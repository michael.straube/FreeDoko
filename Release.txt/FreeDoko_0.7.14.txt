(short english text is below)

Heute veröffentlichen wir die Version 0.7.14 von FreeDoko.

Die InnoCard International GmbH hat unsere Lizenz für den Kartensatz um ein weiteres Jahr bis Ende 2016 verlängert. 

Und natürlich haben wir weiter an der KI gearbeitet und einige Fehler korrigiert.

Viel Spaß beim Spielen vom Doppelkopf
Das FreeDoko Team

----

Today we release the version 0.7.14 of FreeDoko.

Changes:
* expanded license of the InnoCard cardsets to 31. Dezember 2016
* bug fixes

Have fun playing Doppelkopf
Your FreeDoko team
