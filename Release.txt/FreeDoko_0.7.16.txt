Heute veröffentlichen wir die Version 0.7.16 von FreeDoko.

Die InnoCard International GmbH hat die Lizenz für den Kartensatz bis Ende 2017 verlängert. Daher haben wir den Kartensatz wieder aufgenommen. Wir bedanken uns bei der InnoCard International GmbH für die weitere Unterstützung von FreeDoko.

Eine Windows-Version können wir aktuell leider nicht anbieten. Bei Bedarf lässt sich FreeDoko unter msys2 (https://msys2.github.io/) kompilieren. Wir werden voraussichtlich Ende Januar eine Installationsdatei erstellen.


Viel Spaß beim Spielen vom Doppelkopf
Das FreeDoko Team
