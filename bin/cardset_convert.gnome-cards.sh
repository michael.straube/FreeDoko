#!/bin/bash

# script to convert the openclipart cardsets into the FreeDoko one
# uses imagemagick
#
# 12. 11. 2006
# Diether Knof

# height of the destination cards
HEIGHT=138

# the number of rows/columns in the source
ROWS=5
COLS=13

# 1. argument: source (without extension)
# 2. argument: row of the card (start with 0)
# 3. argument: column of the card (start with 0)
# 4. argument: destination file
function convert_card {
  SOURCE=$1.png
  ROW=$2
  COL=$3
  DESTINATION=$4

  total_width=$(identify -format "%w" ${SOURCE})
  total_height=$(identify -format "%h" ${SOURCE})
  WIDTH=$((${total_width} / ${COLS}))

  convert -crop ${WIDTH}x${HEIGHT}+$((${COL}*${WIDTH}))+$((${ROW}*${HEIGHT})) \
	  ${SOURCE} \
	  ${DESTINATION}


  #composite ../mask.${HEIGHT}.png $1 $2
  #mogrify -transparent '#ffff00' $2
} # function convert_card

# first argument: cardset
function convert_cardset
{
  CARDSET=$1
  echo "converting cardset " ${CARDSET}

  # create .png file manually
  #convert -scale $((${ROWS} * ${HEIGHT}))x$((${ROWS} * ${HEIGHT})) ${CARDSET}.svg ${CARDSET}.png
  #echo convert -scale $(( ($(identify -format "%w" ${CARDSET}.png) / ${COLS}) * ${COLS}))x$((${ROWS} * ${HEIGHT})) ${CARDSET}.svg ${CARDSET}.png
  #convert -scale $(( ($(identify -format "%w" ${CARDSET}.png) / ${COLS}) * ${COLS}))x$((${ROWS} * ${HEIGHT})) ${CARDSET}.svg ${CARDSET}.png

  mkdir ${CARDSET}

  echo "  club"
  convert_card ${CARDSET} 0  0 ${CARDSET}/cards/club_ace.png
  convert_card ${CARDSET} 0  8 ${CARDSET}/cards/club_nine.png
  convert_card ${CARDSET} 0  9 ${CARDSET}/cards/club_ten.png
  convert_card ${CARDSET} 0 10 ${CARDSET}/cards/club_jack.png
  convert_card ${CARDSET} 0 11 ${CARDSET}/cards/club_queen.png
  convert_card ${CARDSET} 0 12 ${CARDSET}/cards/club_king.png
  echo "  spade"
  convert_card ${CARDSET} 3  0 ${CARDSET}/cards/spade_ace.png
  convert_card ${CARDSET} 3  8 ${CARDSET}/cards/spade_nine.png
  convert_card ${CARDSET} 3  9 ${CARDSET}/cards/spade_ten.png
  convert_card ${CARDSET} 3 10 ${CARDSET}/cards/spade_jack.png
  convert_card ${CARDSET} 3 11 ${CARDSET}/cards/spade_queen.png
  convert_card ${CARDSET} 3 12 ${CARDSET}/cards/spade_king.png
  echo "  heart"
  convert_card ${CARDSET} 2  0 ${CARDSET}/cards/heart_ace.png
  convert_card ${CARDSET} 2  8 ${CARDSET}/cards/heart_nine.png
  convert_card ${CARDSET} 2  9 ${CARDSET}/cards/heart_ten.png
  convert_card ${CARDSET} 2 10 ${CARDSET}/cards/heart_jack.png
  convert_card ${CARDSET} 2 11 ${CARDSET}/cards/heart_queen.png
  convert_card ${CARDSET} 2 12 ${CARDSET}/cards/heart_king.png
  echo "  diamond"
  convert_card ${CARDSET} 1  0 ${CARDSET}/cards/diamond_ace.png
  convert_card ${CARDSET} 1  8 ${CARDSET}/cards/diamond_nine.png
  convert_card ${CARDSET} 1  9 ${CARDSET}/cards/diamond_ten.png
  convert_card ${CARDSET} 1 10 ${CARDSET}/cards/diamond_jack.png
  convert_card ${CARDSET} 1 11 ${CARDSET}/cards/diamond_queen.png
  convert_card ${CARDSET} 1 12 ${CARDSET}/cards/diamond_king.png

  mkdir ${CARDSET}/backs
  convert_card ${CARDSET} 4 2 ${CARDSET}/backs/back.png
} # function convert_cardset

convert_cardset bellot
#convert_cardset dondorf
#convert_cardset paris
HEIGHT=123
#convert_cardset bonded

#pngcrushmin */*/*.png

