#!/bin/bash

# script to convert the openclipart cardsets into the FreeDoko one
# uses imagemagick
#
# 6. 11. 2006
# Diether Knof

# height of the destination cards
HEIGHT=138
WIDTH=102

rm -rf png.${HEIGHT}

mkdir png.${HEIGHT}



# first argument:  source
# second argument: destination
function convert_card {
  convert -scale x${HEIGHT} \
	  $1 \
	  $2.png

  composite ../mask.${WIDTH}x${HEIGHT}.png $2.png $2
  rm $2.png
  mogrify -transparent '#ffff00' $2
} # function convert_card

function convert_openclipart {
echo "converting openclipart cardset"

mkdir png.${HEIGHT}/cards
echo "  club"
convert_card *c_q.svg  png.${HEIGHT}/cards/club_queen.png
convert_card *c_j.svg  png.${HEIGHT}/cards/club_jack.png
convert_card *c_k.svg  png.${HEIGHT}/cards/club_king.png
convert_card *c_a.svg  png.${HEIGHT}/cards/club_ace.png
convert_card *c_9.svg  png.${HEIGHT}/cards/club_nine.png
convert_card *c_10.svg png.${HEIGHT}/cards/club_ten.png
echo "  spade"
convert_card *s_q.svg  png.${HEIGHT}/cards/spade_queen.png
convert_card *s_j.svg  png.${HEIGHT}/cards/spade_jack.png
convert_card *s_k.svg  png.${HEIGHT}/cards/spade_king.png
convert_card *s_a.svg  png.${HEIGHT}/cards/spade_ace.png
convert_card *s_9.svg  png.${HEIGHT}/cards/spade_nine.png
convert_card *s_10.svg png.${HEIGHT}/cards/spade_ten.png
echo "  heart"
convert_card *h_q.svg  png.${HEIGHT}/cards/heart_queen.png
convert_card *h_j.svg  png.${HEIGHT}/cards/heart_jack.png
convert_card *h_k.svg  png.${HEIGHT}/cards/heart_king.png
convert_card *h_a.svg  png.${HEIGHT}/cards/heart_ace.png
convert_card *h_9.svg  png.${HEIGHT}/cards/heart_nine.png
convert_card *h_10.svg png.${HEIGHT}/cards/heart_ten.png
echo "  diamond"
convert_card *d_q.svg  png.${HEIGHT}/cards/diamond_queen.png
convert_card *d_j.svg  png.${HEIGHT}/cards/diamond_jack.png
convert_card *d_k.svg  png.${HEIGHT}/cards/diamond_king.png
convert_card *d_a.svg  png.${HEIGHT}/cards/diamond_ace.png
convert_card *d_9.svg  png.${HEIGHT}/cards/diamond_nine.png
convert_card *d_10.svg png.${HEIGHT}/cards/diamond_ten.png

mkdir png.${HEIGHT}/backs
for f in ../cardbacks/*.svg; do 
  convert_card $f  png.${HEIGHT}/backs/`basename $f .svg`.png
done

} # function convert_openclipart

convert_openclipart

pngcrushmin */*/*.png
