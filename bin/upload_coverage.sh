#!/bin/zsh

if [ $# -ne 2 ]; then
  echo "Aufruf mit 2 Argumenten:"
  echo "$1: Verzeichnis mit den Coverage-Daten"
  echo "$2: Partition"
  exit
fi

if [[ ! -d "$1" ]]; then
  echo "$1 ist kein Verzeichnis"
  exit
fi
directory="$1"

partition="$2"

file=FreeDoko_`git log --pretty=format:'%H' -n 1`.info

# info-Datei mit den Daten erzeugen
lcov --capture --directory "$directory" --no-external --output-file "$file"

# lcov-Dateien an teamscale versenden
curl -X POST -u build:1bmX1Ti1KamBjbFBHoMx7AoDvMNFxDrY \
  -Freport=@"$file" \
  "http://localhost:8080/p/freedoko/external-report?format=LCOV&partition=$partition&message=$partition%20coverage%20uploaded"
echo
rm "$file"
