#!/bin/zsh

# Skript, um die InnoCard-Kartensätze in das FreeDoko-Format zu konvertieren.
# Größe der Karten
# 90x140 112x175 115x180 122x190 128x200 154x240

# Alle Karten zusammen für die Doko-Lounge
# montage heart_ace.png heart_king.png heart_queen.png heart_jack.png heart_ten.png heart_nine.png diamond_ace.png diamond_king.png diamond_queen.png diamond_jack.png diamond_ten.png diamond_nine.png club_ace.png club_king.png club_queen.png club_jack.png club_ten.png club_nine.png spade_ace.png spade_king.png spade_queen.png spade_jack.png spade_ten.png spade_nine.png -tile x4 -gravity North -geometry +0+0 -background none I.png

if [ $# = 0 ]; then
  echo "Argument(e) erwartet: Kartenhöhe"
  echo "Beispiel: $0 140 185 240"
  exit
fi

RAHMEN="$(pwd)/border.png"
MASKE="$(pwd)/mask.png"

# Dateien mit der Karten
CARDS_FILE_FRENCH=Doppelkopf_franz_Bild_ICI_NEU.pdf
CARDS_FILE_ENGLISH=Bridge_intern_ICI_NEU.pdf
export CARDS_DIR_FRENCH=~/Daten/FreeDoko/orig/InnoCard/Doppelkopf
export CARDS_DIR_ENGLISH=~/Daten/FreeDoko/orig/InnoCard/Bridge

LIZENZ="Verwendung der Spielkartenbilder in FreeDoko bis zum 31. Dezember 2014 mit Genehmigung der InnoCard International GmbH."
LIZENZVERTRAG="Lizenzvertrag_InnoCard_2014.txt"

# Rahmen und Bildmaske erzeugen
# 1: Höhe
# 2: Breite
function create_mask {
local HOEHE=$1
local BREITE=$2
local GROESSE=${BREITE}x${HOEHE}
convert -size $GROESSE \
  xc:transparent -fill transparent -strokewidth 1 -stroke black \
  -draw "roundrectangle 0,0 $(($BREITE-1)),$(($HOEHE-1)) $(($BREITE/10)),$(($BREITE/10))" \
  border.png
convert -size $GROESSE \
  xc:transparent -fill black -stroke black \
  -draw "roundrectangle 0,0 $(($BREITE-1)),$(($HOEHE-1)) $(($BREITE/10)),$(($BREITE/10))" \
  mask.png
}


# Erstes Argument:  Quelle
# Zweites Argument: Ziel
function convert_card {
convert +antialias -background transparent -alpha Set \
   $1 \
  -compose Dst_In "$MASKE" -composite \
  -compose Over "$RAHMEN" -composite \
  -set author "InnoCard International GmbH" \
  -set license "$LIZENZ" \
  $2
} # function convert_card

# Erzeugt die Hintergründe
# 1: Höhe
# 2: Breite
function create_backs {
local HOEHE=$1
local BREITE=$2
../backs/tux/create_tux.sh $BREITE $HOEHE
convert +antialias -background transparent -alpha Set \
  ../backs/tux/tux.png \
  -compose Dst_In "$MASKE" -composite \
  -compose Over "$RAHMEN" -composite \
  "$DIR"/backs/penguin.png
../backs/xaos/create_xaos.sh $BREITE $HOEHE
convert +antialias -background transparent -alpha Set \
  ../backs/xaos/xaos.png \
  -compose Dst_In "$MASKE" -composite \
  -compose Over "$RAHMEN" -composite \
  "$DIR"/backs/xaos.png
}

# 1. Argument: pdf-Datei (Quelle)
# 2. Argument: Zielverzeichnis
# 3. Argument: Höhe der Karten
function convert_french {
local QUELLE="$1"
local DIR="$2"
local HOEHE="$3"
echo "Konvertiere Karten in $DIR/"
mkdir -p "$DIR/cards"

local BREITE=$(identify -format "%w" "$DIR/cards/card-01.png")
create_mask $HOEHE $BREITE

pushd "$DIR/cards" >/dev/null
echo "    Kreuz"
convert_card card-01.png club_queen.png
convert_card card-02.png club_jack.png
convert_card card-03.png club_king.png
convert_card card-04.png club_ace.png
convert_card card-05.png club_ten.png
convert_card card-06.png club_nine.png
echo "    Pik"
convert_card card-07.png spade_queen.png
convert_card card-08.png spade_jack.png
convert_card card-09.png spade_king.png
convert_card card-10.png spade_ace.png
convert_card card-11.png spade_ten.png
convert_card card-12.png spade_nine.png
echo "    Herz"
convert_card card-13.png heart_queen.png
convert_card card-14.png heart_jack.png
convert_card card-15.png heart_king.png
convert_card card-16.png heart_ace.png
convert_card card-17.png heart_ten.png
convert_card card-18.png heart_nine.png
echo "    Karo"
convert_card card-19.png diamond_queen.png
convert_card card-20.png diamond_jack.png
convert_card card-21.png diamond_king.png
convert_card card-22.png diamond_ace.png
convert_card card-23.png diamond_ten.png
convert_card card-24.png diamond_nine.png

rm card-*.png
popd >/dev/null
} # function convert_french

# 1. Argument: pdf-Datei (Quelle)
# 2. Argument: Zielverzeichnis
# 3. Argument: Höhe der Karten
function convert_english {
local QUELLE="$1"
local DIR="$2"
local HOEHE="$3"
echo "Konvertiere Karten in $DIR/"
mkdir -p "$DIR/cards"

echo "  Extrahiere Kartenbilder nach $DIR/cards"
pdftoppm -png -scale-to-y ${HOEHE} -scale-to-x -1 \
  "$QUELLE" "$DIR/cards/card"
#convert -resize ${BREITE}x${HOEHE}^ -gravity center -extent ${BREITE}x${HOEHE} \
  #  -density 1200 $2 \
  #	-comment "Copyright: ${LIZENZ}" \
  #	"$QUELLE" \
  #	"$DIR"/card.png

local BREITE=$(identify -format "%w" "$DIR/cards/card-01.png")
create_mask $HOEHE $BREITE

pushd "$DIR/cards" >/dev/null
echo "    Kreuz"
mkdir -p club
convert_card card-07.png club_queen.png
convert_card card-01.png club_jack.png
convert_card card-06.png club_king.png
convert_card card-05.png club_ace.png
convert_card card-08.png club_ten.png
convert_card card-09.png club_nine.png
echo "    Pik"
mkdir -p spade
convert_card card-19.png spade_queen.png
convert_card card-02.png spade_jack.png
convert_card card-18.png spade_king.png
convert_card card-17.png spade_ace.png
convert_card card-20.png spade_ten.png
convert_card card-21.png spade_nine.png
echo "    Herz"
mkdir -p heart
convert_card card-31.png heart_queen.png
convert_card card-03.png heart_jack.png
convert_card card-30.png heart_king.png
convert_card card-29.png heart_ace.png
convert_card card-32.png heart_ten.png
convert_card card-33.png heart_nine.png
echo "    Karo"
mkdir -p diamond
convert_card card-43.png diamond_queen.png
convert_card card-04.png diamond_jack.png
convert_card card-42.png diamond_king.png
convert_card card-41.png diamond_ace.png
convert_card card-44.png diamond_ten.png
convert_card card-45.png diamond_nine.png

rm card-*.png
popd >/dev/null
} # function convert_english


THEMA=french
#THEMA=english
echo "$THEMA"
local QUELLE=""
if [ $THEMA = french ]; then
  QUELLE=Doppelkopf_franz_Bild_ICI_NEU.pdf
else
  QUELLE=Bridge_intern_ICI_NEU.pdf
fi

# Karten extrahieren
echo "Thema: $THEMA"
echo "  Extrahiere Kartenbilder nach $THEMA/cards"
mkdir -p "$THEMA/cards"
#pdftoppm -png -scale-to-y ${HOEHE} -scale-to-x -1 \
  #  "$QUELLE" "$THEMA/cards/card"
pdfseparate       -l  9 "$QUELLE" "$THEMA/cards/card-0%d.pdf"
pdfseparate -f 10 -l 24 "$QUELLE" "$THEMA/cards/card-%d.pdf"
while [ $# -ge 1 ]; do
  local HOEHE="$1"
  local DIR="$THEMA/$HOEHE"
  echo
  echo "* $DIR *"

  mkdir -p "$DIR/cards"

  # pdf in png umwandeln
  echo "  Extrahiere Kartenbilder nach $DIR/cards"
  for f in $(find "$THEMA/cards/" -name "card-*.pdf"); do
    pdftoppm -png -singlefile -scale-to "$HOEHE" "$f" "$DIR/cards/"$(basename "$f" .pdf) 2>/dev/null
    #gimp -i -b '(pdftopng "'$f'")' -b '(gimp-quit 0)'
    #convert "$f" -size "$GROESSE" "$DIR/cards/"$(basename "$f" .pdf).png 
    #inkscape --export-background=white -d 300 --export-filename "$DIR/cards/"$(basename "$f" .pdf).png -C -h $HOEHE "$f" >/dev/null
  done
  #pdftoppm -png -scale-to-y ${HOEHE} -scale-to-x -1 \
    #  "$QUELLE" "$DIR/cards/card"

  # Karten erzeugen
  convert_$THEMA $QUELLE "$THEMA/$HOEHE"  "$HOEHE"

  # Lizenzvertrag kopieren
  cp "$LIZENZVERTRAG" "$DIR/License.txt"

  # Hintergründe erzeugen
  mkdir -p "$DIR"/backs/
  echo "  Erstelle Hintergründe"
  create_backs $HOEHE $(identify -format "%w" "$DIR/cards/club_queen.png")

  # Symbole erzeugen
  echo "  Erstelle Symbole"
  HOEHE=$(($HOEHE*8/10))
  mkdir -p ./cards
  for f in $(find "$THEMA/cards/" -name "card-*.pdf"); do
    pdftoppm -png -singlefile -scale-to "$HOEHE" "$f" "./cards/"$(basename "$f" .pdf)
    #inkscape --export-background=white --export-filename "./cards/"$(basename "$f" .pdf).png -C -h $HOEHE "$f" >/dev/null
  done
  convert_$THEMA $QUELLE "."  "$HOEHE"
  rm -r cards/

  #rm -rf "$HOEHE"
  #mkdir "$HOEHE"
  #test -d french && mv french "$HOEHE"/
  #test -d english && mv english "$HOEHE"/

  #cp ../copyright.InnoCard License
  #pngcrushmin */*/*/*.png
  shift
done
rm -r "$THEMA/cards"
