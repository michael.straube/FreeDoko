#!/bin/zsh
#
# Erstellt Symbole aus einem Kartensatz
# Verwendet imagemagick
#
# 21. Dezember 2014
# Diether Knof <dknof@gmx.de>

INFO_GROUPS=true
INFO_FILES=false

if [ ! -e ../cards/club_queen.png ]; then
  echo "Kann die Karte '../cards/club_queen.png' nich finden -- falsches Verzeichnis?"
  exit 1
fi
PATH=$PATH:../../../bin/

# get the width and the height of the cards
#width=`xpmtoppm ../cards/club_queen.png | pnmfile | awk '{ print $4 }'`
#height=`xpmtoppm ../cards/club_queen.png | pnmfile | awk '{ print $6 }'`
width=$(identify -format "%w" ../cards/club_queen.png)
height=$(identify -format "%h" ../cards/club_queen.png)
height_half=$(( ${height} / 2 ))

overlap_width=$(( ${width} / 4 ))

FILE_PIXEL=$(tempfile -p pixel. -s .png)
FILE_OVERLAP=$(tempfile -p overlap. -s .png)
FILE_OVERLAP_TMP=$(tempfile -p overlap_tmp. -s .png)

$(${INFO_GROUPS}) && echo "Erstelle Symbole"

overlap() {
  convert ${FILE_PIXEL} -resize $(( ${width} + ($# - 1) * ${overlap_width} ))x${height_half}\! ${FILE_OVERLAP}

  for (( i = 0; $# != 0 ; i++ )); do
    composite -compose src-over -geometry +$(( $i * ${overlap_width} ))+0 ../cards/$1 ${FILE_OVERLAP} ${FILE_OVERLAP_TMP} \
      && mv ${FILE_OVERLAP_TMP} ${FILE_OVERLAP}
    shift
  done # for i

  cat ${FILE_OVERLAP}
}

echo -e "P4\n1 1\n" \
| convert -transparent white - ${FILE_PIXEL}

# Ansagen und Gegenansagen
for limit in 120 90 60 30 0; do
  convert -alpha Set -background transparent \
    -size $((2*${width}))x$((2*$width)) \
    -gravity Center \
    -font Times-Bold \
    -pointsize $((${width}*2/3)) \
    label:$limit \
    -trim \
    +repage \
    -extent ${width}x \
    no_${limit}_reply.png
  w=$(identify -format "%w" no_120_reply.png)
  h=$(identify -format "%h" no_120_reply.png)
  convert no_${limit}_reply.png \
    -alpha Set -background transparent \
    -gravity Center \
    -fill red -stroke red \
    -strokewidth $(($h/5)) \
    -extent $((${w}+2*${h}/5))x \
    -draw "line $(($h/5)),$(($h*4/5)) $(($w+$h/5)),$(($h/5))" \
    +repage \
    no_$limit.png
done

# re.png
$(${INFO_FILES}) && echo "  re.png"
overlap club_queen.png \
  > re.png

$(${INFO_FILES}) && echo "  contra.png"
thickness_x=$(( ${width} / 10 ))
thickness_y=$(( ${height} / 10 ))
convert -fill red -draw "polygon \
  0,$(( $height_half - ${thickness_y} )) \
  0,$(( $height_half )) \
  ${thickness_x},$(( $height_half )) \
  $width,${thickness_y} \
  $width,0 \
  $(( $width - ${thickness_x} )),0 \
  " re.png t.png
composite -compose In t.png re.png contra.png
rm t.png

# Soli
$(${INFO_FILES}) && echo "  solo.jack.png"
overlap club_jack.png spade_jack.png heart_jack.png diamond_jack.png \
  > solo.jack.png
$(${INFO_FILES}) && echo "  solo.queen.png"
overlap club_queen.png spade_queen.png heart_queen.png diamond_queen.png \
  > solo.queen.png
$(${INFO_FILES}) && echo "  solo.king.png"
overlap club_king.png spade_king.png heart_king.png diamond_king.png \
  > solo.king.png
$(${INFO_FILES}) && echo "  solo.queen-jack.png"
overlap club_queen.png spade_queen.png heart_jack.png diamond_jack.png \
  > solo.queen-jack.png
$(${INFO_FILES}) && echo "  solo.king-jack.png"
overlap club_king.png spade_king.png heart_jack.png diamond_jack.png \
  > solo.king-jack.png
$(${INFO_FILES}) && echo "  solo.king-queen.png"
overlap club_king.png spade_king.png heart_queen.png diamond_queen.png \
  > solo.king-queen.png
$(${INFO_FILES}) && echo "  solo.koehler.png"
overlap club_king.png spade_king.png heart_queen.png diamond_jack.png \
  > solo.koehler.png

# Idee: nur Farbsymbol
$(${INFO_FILES}) && echo "  solo.club.png"
overlap spade_queen.png heart_jack.png club_ace.png club_king.png \
  > solo.club.png
$(${INFO_FILES}) && echo "  solo.spade.png"
overlap club_queen.png heart_jack.png spade_ace.png spade_king.png \
  > solo.spade.png
$(${INFO_FILES}) && echo "  solo.heart.png"
overlap club_queen.png diamond_jack.png heart_ace.png heart_king.png \
  > solo.heart.png
$(${INFO_FILES}) && echo "  solo.diamond.png"
overlap club_queen.png heart_jack.png diamond_ace.png diamond_king.png \
  > solo.diamond.png

$(${INFO_FILES}) && echo "  solo.meatless.png"
overlap club_ace.png spade_ace.png heart_ace.png diamond_ace.png \
  > solo.meatless.png

# marriage
# Idee: Hochzeitsringe
$(${INFO_FILES}) && echo "  marriage.png"
overlap club_queen.png club_queen.png \
  > marriage.png
$(${INFO_FILES}) && echo "  marriage.foreign.png"
overlap club_queen.png club_queen.png diamond_jack.png spade_ace.png \
  > marriage.foreign.png
$(${INFO_FILES}) && echo "  marriage.trump.png"
overlap club_queen.png club_queen.png heart_jack.png diamond_ace.png \
  > marriage.trump.png
# Idee: Hochzeitsringe + 3 Farbsymbole
$(${INFO_FILES}) && echo "  marriage.color.png"
overlap club_queen.png club_queen.png heart_ace.png spade_ace.png \
  > marriage.color.png
# Idee: Hochzeitsringe + Farbsymbol
$(${INFO_FILES}) && echo "  marriage.club.png"
overlap club_queen.png club_queen.png club_ace.png club_ace.png \
  > marriage.club.png
$(${INFO_FILES}) && echo "  marriage.spade.png"
overlap club_queen.png club_queen.png spade_ace.png spade_ace.png \
  > marriage.spade.png
$(${INFO_FILES}) && echo "  marriage.heart.png"
overlap club_queen.png club_queen.png heart_ace.png heart_ace.png \
  > marriage.heart.png
$(${INFO_FILES}) && echo "  marriage.partner.png"
overlap club_queen.png \
  > marriage.partner.png

# genscher
$(${INFO_FILES}) && echo "  genscher.png"
overlap diamond_king.png diamond_king.png \
  > genscher.png

# poverty
$(${INFO_FILES}) && echo "  poverty.png"
overlap heart_queen.png spade_jack.png diamond_ten.png \
  > poverty.png
$(${INFO_FILES}) && echo "  poverty.3.png"
overlap heart_queen.png spade_jack.png diamond_ten.png \
  > poverty.3.png
$(${INFO_FILES}) && echo "  poverty.2.png"
overlap heart_queen.png diamond_ten.png club_ace.png \
  > poverty.2.png
$(${INFO_FILES}) && echo "  poverty.1.png"
overlap heart_queen.png club_ace.png heart_king.png \
  > poverty.1.png
$(${INFO_FILES}) && echo "  poverty.0.png"
overlap club_ace.png heart_king.png spade_ten.png \
  > poverty.0.png
$(${INFO_FILES}) && echo "  poverty.partner.png"
overlap club_queen.png \
  > poverty.partner.png

# thrown nines
$(${INFO_FILES}) && echo "  thrown_nines.png"
overlap club_nine.png spade_nine.png heart_nine.png diamond_nine.png \
  > thrown_nines.png

# thrown kings
$(${INFO_FILES}) && echo "  thrown_kings.png"
overlap club_king.png spade_king.png heart_king.png diamond_king.png \
  > thrown_kings.png

# thrown nines and kings
$(${INFO_FILES}) && echo "  thrown_nines_and_kings.png"
overlap club_king.png spade_king.png heart_nine.png diamond_nine.png \
  > thrown_nines_and_kings.png

# thrown richness
# Idee: Viele Münzen
$(${INFO_FILES}) && echo "  thrown_richness.png"
overlap club_ten.png spade_ten.png heart_ten.png diamond_ten.png \
  > thrown_richness.png

# fox highest trump
$(${INFO_FILES}) && echo "  fox_highest_trump.png"
overlap diamond_ace.png diamond_ten.png diamond_king.png diamond_nine.png \
  > fox_highest_trump.png

# swines
# Idee: Schwein
$(${INFO_FILES}) && echo "  swines.club.png"
overlap club_ace.png club_ace.png \
  > swines.club.png
$(${INFO_FILES}) && echo "  swines.spade.png"
overlap spade_ace.png spade_ace.png \
  > swines.spade.png
$(${INFO_FILES}) && echo "  swines.heart.png"
overlap heart_ace.png heart_ace.png \
  > swines.heart.png
$(${INFO_FILES}) && echo "  swines.diamond.png"
overlap diamond_ace.png diamond_ace.png \
  > swines.diamond.png

# hyperswines
# Idee: besonderes Schwein
$(${INFO_FILES}) && echo "  hyperswines.club.png"
overlap club_nine.png club_nine.png \
  > hyperswines.club.png
$(${INFO_FILES}) && echo "  hyperswines.spade.png"
overlap spade_nine.png spade_nine.png \
  > hyperswines.spade.png
$(${INFO_FILES}) && echo "  hyperswines.heart.png"
overlap heart_nine.png heart_nine.png \
  > hyperswines.heart.png
$(${INFO_FILES}) && echo "  hyperswines.diamond.png"
overlap diamond_nine.png diamond_nine.png \
  > hyperswines.diamond.png
# without nines
$(${INFO_FILES}) && echo "  hyperswines.king.club.png"
overlap club_king.png club_king.png \
  > hyperswines.king.club.png
$(${INFO_FILES}) && echo "  hyperswines.king.spade.png"
overlap spade_king.png spade_king.png \
  > hyperswines.king.spade.png
$(${INFO_FILES}) && echo "  hyperswines.king.heart.png"
overlap heart_king.png heart_king.png \
  > hyperswines.king.heart.png
$(${INFO_FILES}) && echo "  hyperswines.king.diamond.png"
overlap diamond_king.png diamond_king.png \
  > hyperswines.king.diamond.png

# swines and hyperswines
$(${INFO_FILES}) && echo "  swines-hyperswines.club.png"
overlap club_ace.png club_nine.png \
  > swines-hyperswines.club.png
$(${INFO_FILES}) && echo "  swines-hyperswines.spade.png"
overlap spade_ace.png spade_nine.png \
  > swines-hyperswines.spade.png
$(${INFO_FILES}) && echo "  swines-hyperswines.heart.png"
overlap heart_ace.png heart_nine.png \
  > swines-hyperswines.heart.png
$(${INFO_FILES}) && echo "  swines-hyperswines.diamond.png"
overlap diamond_ace.png diamond_nine.png \
  > swines-hyperswines.diamond.png
# without nines
$(${INFO_FILES}) && echo "  swines-hyperswines.king.club.png"
overlap club_ace.png club_king.png \
  > swines-hyperswines.king.club.png
$(${INFO_FILES}) && echo "  swines-hyperswines.king.spade.png"
overlap spade_ace.png spade_king.png \
  > swines-hyperswines.king.spade.png
$(${INFO_FILES}) && echo "  swines-hyperswines.king.heart.png"
overlap heart_ace.png heart_king.png \
  > swines-hyperswines.king.heart.png
$(${INFO_FILES}) && echo "  swines-hyperswines.king.diamond.png"
overlap diamond_ace.png diamond_king.png \
  > swines-hyperswines.king.diamond.png

# dullen
$(${INFO_FILES}) && echo "  dullen.png"
overlap heart_ten.png heart_ten.png \
  > dullen.png

# specialpoints
# Idee: Zwei Köpfe
$(${INFO_FILES}) && echo "  doppelkopf.png"
overlap club_ace.png spade_ten.png diamond_ace.png heart_ten.png \
  > doppelkopf.png

exit


$(${INFO_GROUPS}) && echo "Schärfe alle Symbole"
# sharpen and minimize
for f in *.png; do \
  $(${INFO_FILES}) && echo "  $f"
mogrify -sharpen 1 $f
pngcrushmin $f
done

