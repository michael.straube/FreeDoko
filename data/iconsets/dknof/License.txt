http://de.wikipedia.org/wiki/Farbe_%28Kartenspiel%29
Gemeinfrei
* https://commons.wikimedia.org/wiki/File:SuitClubs.svg
* https://commons.wikimedia.org/wiki/File:SuitDiamonds.svg
* https://commons.wikimedia.org/wiki/File:SuitHearts.svg
* https://commons.wikimedia.org/wiki/File:SuitSpades.svg

www.opencliparts.org
Creative Commons CC0 1.0
* http://openclipart.org/detail/142753/gold-rings-by-andy
* http://openclipart.org/detail/2883/running-pig-by-liftarn
* http://openclipart.org/detail/1900/smiling-pig-by-lalolalo
* https://openclipart.org/detail/18670/chess-tile-king-1
* https://openclipart.org/detail/18645/chess-tile-knight-3
* https://openclipart.org/detail/18663/chess-tile-queen-1
* https://openclipart.org/detail/18661/chess-tile-queen-3
* https://openclipart.org/detail/18643/chess-tile-knight-2
* https://openclipart.org/detail/18668/chess-tile-king-3
* https://openclipart.org/detail/18655/chess-tile-rook-3
