= FreeDoko LiesMich
Diether Knof <dknof@posteo.de>


== Installation

Dieser Teil ist nur für die Leute gedacht, die sich das Programm kompilieren wollen, die mit dem vorkompilierten Programm können direkt zu Programmstart weitergehen

Zum Kompilieren einfach im Verzeichnis `src` `make` aufrufen.
Es wird geprüft, ob die benötigten Bibliotheken installiert sind.

Wenn unter Linux eine Fehlermeldung mit `no-cygwin` oder `ms-bitfields` erscheint, dann hat die automatische Erkennung nicht funktioniert.
Geben Sie in der Shell `export OSTYPE=linux-gnu` ein und starten Sie `make` noch einmal.

Einmalig wird die Datei `Makefile.local` erzeugt. Darin können Module an- und abgeschaltet und die Kompilieroptionen angepasst werden. 

== Programmstart

FreeDoko unterstützt einige Kommandozeilenargumente, diese lassen sich mit '--help' anschauen.
Unter Windows erfolgt die Ausgabe nicht direkt in den Microsoft-Terminals, dort kann sie mit '| more' sichtbar gemacht werden.

=== Linux
Das Programm `FreeDoko` starten.

Ist FreeDoko mit `make install` installiert, müssen Sie Mitglied der Gruppe `games` sein, um `FreeDoko` starten zu dürfen.


=== Windows
Die Batch-Datei `FreeDoko.exe` starten.


== Einstellungen

In der Ressourcendatei `FreeDokorc` stehen die Einstellungen für das Programm.

Das Format ist wie folgt:
Leerzeilen und Zeilen, die mit einem `#` beginnen werden ignoriert, Einstellungen beginnen mit dem Schlüsselwort, gefolgt von einem `=` und dem Wert ('1' steht fuer 'ja', '0' fuer 'nein');

Alle Einstellungen können wärend des Spiels geändert werden und werden _sofort_ aktiv.

=== Sprache
Um die Sprache von FreeDoko zu ändern also einfach mit `F2` das Einstellungsfenster öffnen und dann auch die angezeigte Sprache `Standard` klicken und eine andere auswählen. Danach sollte FreeDoko neu gestartet werden.


== Dateien

Zu FreeDoko gehören folgende Dateien und Verzeichnisse:

* Programmdatei:
Unter Linux ist dies `FreeDoko`, unter Windows `FreeDoko.exe`.
* Lizenz: `COPYING` 
* Allgemeine Information: `LIESMICH`, `README`
* Hintergründe: Unterverzeichnis `backgrounds`
* Kartensätze: Unterverzeichnis `cardsets`
* Symbolsätze: Unterverzeichnis `iconsets`
* Sounds: Unterverzeichnis `sounds`
* Textdateien: Unterverzeichnis `po`
* Dokumentation: Unterverzeichnis `doc`
* Handbuch: Unterverzeichnis `manual`
* Regelsätze: Unterverzeichnis `rules`
* Logos: `FreeDoko.png`, `icon.png`
* Programmierer: `AUTHORS` 
* Versionsinformation: `Version` 

== Bemerkungen

* Bei Fehlern bitte eine E-Mail an freedoko@users.sourceforge.net schicken.

// vim: filetype=asciidoc
