DEPTH = .
SUBDIR = .

# for Windows under Linux:
# export OSTYPE=Linux_to_Windows

include $(DEPTH)/Makefile.os
include ../Makefile.install.directories

include Makefile.objects


ifeq ($(OPERATING_SYSTEM), MSYS)
  OBJ_FILES_ALL += windres.o
endif
ifeq ($(OPERATING_SYSTEM), Windows)
  OBJ_FILES_ALL += windres.o
endif
INCLUDE_ALL = $(INCLUDE)
ifeq ($(OPERATING_SYSTEM), MACOSX)
  LIBS_ALL = $(LIBS) -lm -lstdc++
else
  LIBS_ALL = $(LIBS) -lm -lstdc++fs
endif
ifeq ($(USE_UI_GTKMM), true)
  OBJ_FILES_ALL += $(OBJ_FILES_GTKMM)
  INCLUDE_ALL += $(INCLUDE_GTKMM)
  LIBS_ALL += $(LIBS_GTKMM)
endif
ifeq ($(USE_SOUND), true)
  DIRECTORIES += sound
  OBJ_FILES_ALL += $(OBJ_FILES_SOUND)
  ifeq ($(USE_SOUND_ALUT), true)
  INCLUDE_ALL += $(INCLUDE_SOUND_ALUT)
  LIBS_ALL += $(LIBS_SOUND_ALUT)
endif
ifeq ($(USE_SOUND_PLAYSOUND), true)
  LIBS_ALL += $(LIBS_SOUND_PLAYSOUND)
endif
endif

# check the environment and compile the program
.PHONY: all
all : Makefile.local logo.png
	$(MAKE) translation
	$(MAKE) check
	$(MAKE) $(PROGRAM)

include $(DEPTH)/Makefile.rules

.PHONY: test
test :
	echo $(findstring linux,$(OSTYPE))
ifeq ($(findstring linux,$(OSTYPE)), linux)
	echo "linux"
endif
ifeq ($(findstring darwin,$(OSTYPE)), darwin)
	echo "darwin"
endif

# create the file 'Makefile.local' from the template
Makefile.local:
ifeq ($(SHELLTYPE), sh)
	test -f $@ || cp Makefile.local.template $@
endif
ifeq ($(SHELLTYPE), COMMAND.COM)
	if not exist $@ copy Makefile.local.template $@
endif

# create the logo from the free one
logo.png:
ifeq ($(SHELLTYPE), sh)
	test -f $@ || cp logo.free.png $@
endif
ifeq ($(SHELLTYPE), COMMAND.COM)
	if not exist $@ copy logo.free.png $@
endif


# print the targets of this makefile
.PHONY: help
help :
	@echo "make targets"
	@echo "  all               check the environment and compile the program"
	@echo "  help              print the targests of this makefile"
	@echo "  check             check the os and the libraries"
	@echo "  check_os          check, whether the ostype was detected correctly"
	@echo "  check_libraries   check, whether the needed libraries are installed"
	@echo "  watch             automatic compile FreeDoko if a *.cpp-file has changed"
	@echo "  seed.info         create a file with information of the hands for different seeds"
	@echo "  show_settings     show all settings (environment, modules, flags)"
	@echo "  show_environment  show the environment settings"
	@echo "  show_modules      show the modules"
	@echo "  show_flags        show the flags used for compiling"
	@echo "  FreeDoko          compile FreeDoko"
	@echo "  release_bin       compile FreeDoko for a release"
	@echo "  strip             strip the program from all symbols"
	@echo "  objects           compile all cpp files in this directory"
	@echo "  doc               create api overview"
	@echo "  tags              create a tags file"
	@echo "  update_aiconfigs  updates the aiconfigs (from the hardcoded)"
	@echo "  coverage          uploads the result of gcovr to teamscale"
	@$(MAKE) -s rules_help

# check the os and the libraries
.PHONY: check
check : check_os check_libraries

# check, whether the ostype was detected correctly
.PHONY: check_os
check_os :
ifeq ($(OPERATING_SYSTEM), Windows)
 ifneq ($(SHELLTYPE), sh)
	@REM \
	  ; (echo -e \\n\
	  	      "***** Error *****\\n"\
	              "If you see these lines, the operating system was not detected correctly.\\n"\
	              "Detected operating system: $(OPERATING_SYSTEM)\\n"\
	              "Try an 'export OSTYPE' before compiling.\\n"\
	      && false)
endif
else
	@# echo "***** Error ***** If you see this line, the operating system was not detected correctly. Detected operating system: $(OPERATING_SYSTEM)"
endif

# check, whether the needed libraries are installed
# till now only supports 'pkg-config', t.i. unix systems
.PHONY: check_libraries
check_libraries :
ifeq ($(SHELLTYPE), sh)
  ifeq ($(REST_SERVICE), true)
	@pkg-config --exists libmicrohttpd \
	  || echo "libmicrohttpd nicht gefunden. Ist das entsprechende dev-Paket installiert?"
  endif
ifneq ($(OPERATING_SYSTEM), MSYS)
  ifeq ($(WEBSOCKETS_DOKOLOUNGE_SERVICE), true)
	@pkg-config --exists libwebsockets \
	  || echo "libwebsockets nicht gefunden. Ist das entsprechende dev-Paket installiert?"
  endif
endif
  ifeq ($(USE_UI_GTKMM), true)
	@pkg-config --exists gtkmm-3.0 \
	  || echo "gtkmm 3.0 (GUI-Bibliothek) nicht gefunden. Ist das entsprechende dev-Paket installiert?"
  endif
  ifeq ($(USE_SOUND), true)
  ifeq ($(USE_SOUND_ALUT), true)
	@pkg-config --exists freealut \
	  || echo "freealut (Sound-Bibliothek) nicht gefunden. Ist das entsprechende dev-Paket installiert?"
  endif
  endif
	@which xgettext >/dev/null \
	  || echo "Programm xgettext nicht gefunden. Ist gettext installiert?"
	@which msgmerge >/dev/null \
	  || echo "Programm msgmerge nicht gefunden. Ist gettext installiert?"
	@which msgfmt >/dev/null \
	  || echo "Programm msgfmt nicht gefunden. Ist gettext installiert?"
  ifeq ($(REST_SERVICE), true)
	@pkg-config --exists libmicrohttpd
  endif
ifneq ($(OPERATING_SYSTEM), MSYS)
  ifeq ($(WEBSOCKETS_DOKOLOUNGE_SERVICE), true)
	@pkg-config --exists libwebsockets
  endif
endif
  ifeq ($(USE_UI_GTKMM), true)
	@pkg-config --exists gtkmm-3.0
  endif
  ifeq ($(USE_SOUND), true)
  ifeq ($(USE_SOUND_ALUT), true)
	@pkg-config --exists freealut
  endif
  endif
	@which xgettext >/dev/null
	@which msgmerge >/dev/null
	@which msgfmt >/dev/null
endif
ifeq ($(SHELLTYPE), COMMAND.COM)
#       ToDo
endif

# automatic compile FreeDoko if a *.cpp/*.h/Makefile-file has changed
.PHONY: watch
watch :
	@rm -f commands.sh
	@echo '#!/bin/bash' >> commands.sh
	@echo 'if [ -e ./FreeDoko ]; then' >> commands.sh
	@echo '  for f in $$(find . -name "*.cpp"); do' >> commands.sh
	@echo '    if [ $$f -nt ./FreeDoko ]; then rm ./FreeDoko; break; fi' >> commands.sh
	@echo '  done' >> commands.sh
	@echo 'fi' >> commands.sh
	@echo 'if [ $$(grep -c processor /proc/cpuinfo) -gt 1 ]; then' >> commands.sh
	@echo '  nice -n 5 make -s -k --jobs=$$(expr 1 + $$(grep -c processor /proc/cpuinfo) ) --load-average=$$(expr 1 + $$(grep -c processor /proc/cpuinfo) ) $(PROGRAM) && sleep 1' >> commands.sh
	@echo 'else' >> commands.sh
	@echo '  nice -n 5 make -s -k $(PROGRAM) && sleep 1' >> commands.sh
	@echo 'fi' >> commands.sh
	@while true; do \
	  find . -name '*.cpp' -o -name '*.h' -o -name 'Makefile*' | inotifywait -e modify --fromfile -; \
	  sh ./commands.sh; \
	  done
	@rm -f commands.sh

# create a file with the gametype for the different seeds
seed.info : $(PROGRAM)
	./$(PROGRAM) --seed-info --seed=0 --no-automatic-savings ./party.with-nines    | gzip -9 - > seed.info.with-nines.csv.gz &
	./$(PROGRAM) --seed-info --seed=0 --no-automatic-savings ./party.without-nines | gzip -9 - > seed.info.without-nines.csv.gz

# create a file with the gametype for the different seeds
Startwert-Statistiken : $(PROGRAM)
	./$(PROGRAM) --seed-info --seed=0 --no-automatic-savings ./party.with-nines    | gzip -9 - > Startwert-Statistiken_mit_Neunen.csv.gz &
	./$(PROGRAM) --seed-info --seed=0 --no-automatic-savings ./party.without-nines | gzip -9 - > Startwert-Statistiken_ohne_Neunen.csv.gz

# show the settings
.PHONY: show_settings
show_settings :
	$(MAKE) show_environment
	$(MAKE) show_modules
	$(MAKE) show_flags

# show the environment
.PHONY: show_environment
show_environment :
	@echo 'Environment'
	@echo '  OSTYPE                : $(OSTYPE)'
	@echo '  OPERATING_SYSTEM      : $(OPERATING_SYSTEM)'
	@echo '  OPERATING_SYSTEM_NAME : $(OPERATING_SYSTEM_NAME)'
	@echo '  SHELLTYPE             : $(SHELLTYPE)'
ifneq ($(MINGW_DIR),)
	@echo '  MINGW_DIR             : $(MINGW_DIR)'
endif
	@echo '  PROGRAM               : $(PROGRAM)'
	@echo '  OBJDIR                : $(OBJDIR)'

# show the modules used
.PHONY: show_modules
show_modules :
	@echo 'Modules'
	@echo '  REST_SERVICE             : $(REST_SERVICE)'
	@echo '  WEBSOCKETS_DOKOLOUNGE_SERVICE : $(WEBSOCKETS_DOKOLOUNGE_SERVICE)'
	@echo '  USE_THREADS              : $(USE_THREADS)'
	@echo '  USE_UI_GTKMM             : $(USE_UI_GTKMM)'
	@echo '  USE_SOUND                : $(USE_SOUND)'
	@echo '    USE_SOUND_ALUT         : $(USE_SOUND_ALUT)'
	@echo '    USE_SOUND_COMMAND      : $(USE_SOUND_COMMAND)'
	@echo '    USE_SOUND_PLAYSOUND    : $(USE_SOUND_PLAYSOUND)'

# show the flags used for compiling
.PHONY: show_flags
show_flags :
	@echo 'Flags'
	@echo '  CXX      : $(CXX)'
	@echo '  CPPFLAGS : $(CPPFLAGS)'
ifneq ($(CPPFLAGS_FREEDOKO), )
	@echo '  CPPFLAGS_FREEDOKO : $(CPPFLAGS_FREEDOKO)'
endif
	@echo '  CXXFLAGS : $(CXXFLAGS)'
	@echo '  INCLUDE  : $(INCLUDE_ALL)'
	@echo '  LIBS     : $(LIBS_ALL)'
	@echo '  LDFLAGS  : $(LDFLAGS)'

# compile FreeDoko
$(PROGRAM) : show_settings
	./git_commit.update.sh
	./compile_date.update.sh
	$(MAKE) objects directories
	@echo 'compiling $@...'
# Gentoo users do want to see the real compile line.
# So remove the next five lines (the echo lines)
# and remove the '@' in the line after.
	@echo '$(CXX) $$(OBJ_FILES_ALL)'
	@echo '        $(LIBS_ALL)'
	@echo '        $(CXXFLAGS)'
	@echo '        $(LDFLAGS)'
	@echo '        -o $@'
	@$(CXX) \
	  $(OBJ_FILES_ALL:%.o=$(OBJDIR)/%.o) \
	  $(LIBS_ALL) \
	  $(CXXFLAGS) \
	  $(LDFLAGS) \
	  -o $@
	@echo "$@ compiled successfully"

ifeq ($(OPERATING_SYSTEM), Linux)
.PHONY: FreeDoko.exe
FreeDoko.exe :
	$(MAKE) OSTYPE=Linux_to_Windows CXX="ccache i686-w64-mingw32-g++" $@
endif

# compile FreeDoko for a release
.PHONY: release_bin
release_bin : Makefile.local logo.png
	$(MAKE) "RELEASE=true" check
	$(MAKE) "RELEASE=true" \
		CPPFLAGS_FREEDOKO="-DRELEASE \
		-DPUBLIC_DATA_DIRECTORY_VALUE='\"$(DATA_DIRECTORY)\"' \
		-DMANUAL_DIRECTORY_VALUE='\"$(HELP_DIRECTORY)/manual\"' \
		-DLOCALE_DIRECTORY='\"$(LOCALE_DIRECTORY)\"'" \
	  $(PROGRAM)
	$(MAKE) strip

# strip the program from all symbols
.PHONY: strip
strip :
	$(STRIP) $(PROGRAM)

# create a pot file (translations)
.PHONY: pot
pot :
	$(MAKE) -C ../po FreeDoko.pot

# create the translation files
.PHONY: translation
translation :
	$(MAKE) -C ../po

# update the translation files
.PHONY: po
po : pot translation

.PHONY : tags
tags :
ifeq ($(OPERATING_SYSTEM), Linux)
	ctags -R --fields=+lS
endif

ifeq ($(OPERATING_SYSTEM), Linux)
  .PHONY : update_aiconfigs
update_aiconfigs :
	make $(PROGRAM)
	./$(PROGRAM) --save-standard-aiconfig
	cp $(HOME)/.FreeDoko/ai/standard.defensive.saved player/aiconfigs.preset/
endif

ifeq ($(OPERATING_SYSTEM), Windows)
# the program is dependent on the windres object file
$(PROGRAM) : $(TARGET_DIR_LOCAL)windres.o

# create the windres object file
$(TARGET_DIR_LOCAL)windres.o : windres.rc objects_target_dir
	windres $< $@
endif


ifeq ($(OPERATING_SYSTEM), MSYS)
# the program is dependent on the windres object file
$(PROGRAM) : $(TARGET_DIR_LOCAL)windres.o

# create the windres object file
$(TARGET_DIR_LOCAL)windres.o : windres.rc objects_target_dir
	windres $< $@
endif
