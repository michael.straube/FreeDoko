/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "exception.h"

enum class BockTrigger {
  // bock after equal points between re and contra
  equal_points,
  // bock after a lost solo game
  solo_lost,
  // bock after a lost re game (only re is announced)
  re_lost,
  // bock after a lost contra game (only contra is announced)
  contra_lost,
  // bock after a real heart trick
  heart_trick,
  // bock after a black game (a team got no trick)
  black
}; // enum class BockTrigger
constexpr auto bock_trigger_list = array<BockTrigger, 6>{
  BockTrigger::equal_points,
  BockTrigger::solo_lost,
  BockTrigger::re_lost,
  BockTrigger::contra_lost,
  BockTrigger::heart_trick,
  BockTrigger::black
};

auto to_string(BockTrigger bock_trigger) -> string;
auto gettext(BockTrigger bock_trigger)   -> string;
auto operator<<(ostream& ostr, BockTrigger bock_trigger) -> ostream&;
auto bock_trigger_from_string(string const& name) -> BockTrigger;
