/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "fast_play.h"

FastPlay fast_play = {}; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

FastPlay::FastPlay() noexcept = default;

FastPlay::FastPlay(FastPlayBit const v) noexcept
: value(static_cast<int>(v))
{ }

FastPlay::operator bool() const noexcept
{
  return value != 0;
}

auto FastPlay::operator&(FastPlayBit rhs) const noexcept -> FastPlay
{
  if ((value & static_cast<int>(rhs)) != 0) {
    return rhs;
  } else {
    return {};
  }
}

auto FastPlay::operator|=(FastPlayBit rhs) noexcept -> FastPlay&
{
  value |= static_cast<int>(rhs);
  return *this;
}

void FastPlay::add(FastPlayBit rhs) noexcept
{
  value |= static_cast<int>(rhs);
}

void FastPlay::remove(FastPlayBit rhs) noexcept
{
  value &= ~static_cast<int>(rhs);
}

auto operator|(FastPlayBit const lhs, FastPlayBit const rhs) noexcept -> FastPlay
{
  FastPlay res(lhs);
  res.value |= static_cast<int>(rhs);
  return res;
}
