/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "exception.h"

enum class GameType {
  normal,
  thrown_nines,
  thrown_kings,
  thrown_nines_and_kings,
  thrown_richness,
  fox_highest_trump,
  redistribute,
  poverty,
  marriage,
  marriage_solo,
  marriage_silent,
  solo_meatless,
  solo_jack,
  solo_queen,
  solo_king,
  solo_queen_jack,
  solo_king_jack,
  solo_king_queen,
  solo_koehler,
  solo_club,
  solo_spade,
  solo_heart,
  solo_diamond,
}; // enum class GameType

constexpr auto game_type_list = array<GameType, 24>{
  GameType::normal,
  GameType::thrown_nines,
  GameType::thrown_kings,
  GameType::thrown_nines_and_kings,
  GameType::thrown_richness,
  GameType::fox_highest_trump,
  GameType::redistribute,
  GameType::poverty,
  GameType::marriage,
  GameType::marriage_solo,
  GameType::marriage_silent,
  GameType::solo_meatless,
  GameType::solo_jack,
  GameType::solo_queen,
  GameType::solo_king,
  GameType::solo_queen_jack,
  GameType::solo_king_jack,
  GameType::solo_king_queen,
  GameType::solo_koehler,
  GameType::solo_club,
  GameType::solo_spade,
  GameType::solo_heart,
  GameType::solo_diamond,
};
constexpr auto game_type_solo_list = array<GameType, 12>{
  GameType::solo_meatless,
  GameType::solo_jack,
  GameType::solo_queen,
  GameType::solo_king,
  GameType::solo_queen_jack,
  GameType::solo_king_jack,
  GameType::solo_king_queen,
  GameType::solo_koehler,
  GameType::solo_club,
  GameType::solo_spade,
  GameType::solo_heart,
  GameType::solo_diamond,
};

auto to_string(GameType game_type) noexcept -> string;
auto gettext(GameType game_type)            -> string;
auto game_type_from_string(string const& name) -> GameType;
auto operator<<(ostream& ostr, GameType game_type) -> ostream&;

auto is_with_unknown_teams(GameType game_type)       noexcept -> bool;
auto is_with_trump_color(GameType game_type)         noexcept -> bool;
auto is_normal(GameType game_type)                   noexcept -> bool;
auto is_poverty(GameType game_type)                  noexcept -> bool;
auto is_marriage(GameType game_type)                 noexcept -> bool;
auto is_solo(GameType game_type)                     noexcept -> bool;
auto is_real_solo(GameType game_type)                noexcept -> bool;
auto is_color_solo(GameType game_type)               noexcept -> bool;
auto is_picture_solo(GameType game_type)             noexcept -> bool;
auto is_picture_solo_or_meatless(GameType game_type) noexcept -> bool;
auto is_single_picture_solo(GameType game_type)      noexcept -> bool;
auto is_double_picture_solo(GameType game_type)      noexcept -> bool;
