/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "card.h"

#include "hand_card.h"
#include "../game/game.h"
#include "../party/rule.h"

auto Card::color(string const& color_name) -> Color
{
  for (auto const c : {Card::club, Card::spade, Card::heart, Card::diamond}) {
    if (color_name == to_string(c))
      return c;
  }
  if (color_name == "unknown")
    return Card::unknowncardcolor;

  DEBUG_ASSERTION(false,
                  "Card::color(color_name):\n"
                  "  color '" << color_name << "' unknown");

  return Card::nocardcolor;
}

auto Card::value(string const& value_name) -> Value
{
  if (value_name.empty()) {
    cerr << "Card::value("")\n";
    abort();
  }
  for (auto const v : {Card::nine, Card::jack, Card::queen, Card::king, Card::ten, Card::ace}) {
    if (value_name == to_string(v))
      return v;
  }

  if (value_name == "unknown")
    return Card::unknowncardvalue;

  DEBUG_ASSERTION(false,
                  "Card::value(value_name):\n"
                  "  value '" << value_name << "' unknown");

  return nocardvalue;
}

Card::Card(string const& name)
{
  if (name.empty()) {
    cerr << "Card::Card("")\n";
    abort();
  }
  if (name == "unknown") {
    color_ = unknowncardcolor;
    value_ = unknowncardvalue;
    return ;
  }
  istringstream istr(name);
  read(istr);
}

auto Card::read(istream& istr) -> istream&
{
  Color c = Card::nocardcolor;
  istr >> c;
  if (c == Card::unknowncardcolor || c == Card::nocardcolor) {
    *this = Card::unknown;
    return istr;
  }
  Value v = Card::nocardvalue;
  istr >> v;
  *this = Card(c, v);
  return istr;
}

auto Card::tcolor(Game const& game) const -> TColor
{
  if (istrump(game))
    return Card::trump;
  else
    return color();
}

auto Card::istrump(Game const& game) const -> bool
{
  return istrump(game.type(), game.rule(Rule::Type::dullen));
}

auto Card::istrump(GameType const gametype, bool const dullen) const -> bool
{
  if (isdulle(gametype, dullen))
    return true;

  switch (gametype) {
  case GameType::thrown_nines:
    return (value() == nine);
  case GameType::thrown_kings:
    return (value() == king);
  case GameType::thrown_nines_and_kings:
    return (   (value() == nine)
            || (value() == king) );
  case GameType::thrown_richness:
    return true;
  case GameType::normal:
  case GameType::poverty:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
    return (   (color() == diamond)
            || (value() == jack)
            || (value() == queen) );
  case GameType::solo_jack:
    return (value() == jack);
  case GameType::solo_queen:
    return (value() == queen);
  case GameType::solo_king:
    return (value() == king);
  case GameType::solo_queen_jack:
    return (   (value() == jack)
            || (value() == queen) );
    break;
  case GameType::solo_king_jack:
    return (   (value() == jack)
            || (value() == king) );
  case GameType::solo_king_queen:
    return (   (value() == queen)
            || (value() == king) );
  case GameType::solo_koehler:
    return (   (value() == jack)
            || (value() == queen)
            || (value() == king) );
  case GameType::solo_club:
    return (   (color() == club)
            || (value() == jack)
            || (value() == queen) );
  case GameType::solo_heart:
    return (   (color() == heart)
            || (value() == jack)
            || (value() == queen) );
  case GameType::solo_spade:
    return (   (color() == spade)
            || (value() == jack)
            || (value() == queen) );
  case GameType::solo_meatless:
    return false;
  } // switch(gametype)

  return false;
}

auto Card::isdulle(Game const& game) const -> bool
{
  return isdulle(game.type(), game.rule(Rule::Type::dullen));
}

auto Card::isdulle(GameType const gametype, bool const dullen) const -> bool
{
  if (!dullen)
    return false;

  if (*this != Card::dulle)
    return false;

  switch (gametype) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return true;
  case GameType::solo_meatless:
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
    return false;
  } // switch (gametype)

  return false;
}

auto higher_values_descending(Card::Value const value) -> vector<Card::Value>
{
  switch (value) {
  case Card::nine:
    return {Card::ace, Card::ten, Card::king, Card::queen, Card::jack};
  case Card::jack:
    return {Card::ace, Card::ten, Card::king, Card::queen};
  case Card::queen:
    return {Card::ace, Card::ten, Card::king};
  case Card::king:
    return {Card::ace, Card::ten};
  case Card::ten:
    return {Card::ace};
  case Card::ace:
    return {};
  case Card::nocardvalue:
  case Card::unknowncardvalue:
    return {};
  } // switch (value)
  return {};
}

auto is_selector(Card::TColor const tcolor,
                 MarriageSelector const marriage_selector) noexcept -> bool
{
  switch (marriage_selector) {
  case MarriageSelector::team_set:
  case MarriageSelector::silent:
    return false;
  case MarriageSelector::first_foreign:
    return true;
  case MarriageSelector::first_trump:
    return (tcolor == Card::trump);
  case MarriageSelector::first_color:
    return (tcolor != Card::trump);
  case MarriageSelector::first_club:
    return (tcolor == Card::club);
  case MarriageSelector::first_spade:
    return (tcolor == Card::spade);
  case MarriageSelector::first_heart:
    return (tcolor == Card::heart);
  } // switch (marriage_selector)
  return false;
}

auto solo(Card::Value const picture) noexcept -> GameType
{
  switch (picture) {
  case Card::jack:
    return GameType::solo_jack;
  case Card::queen:
    return GameType::solo_queen;
  case Card::king:
    return GameType::solo_king;
  default:
    DEBUG_ASSERTION(false,
                    "solo(picture = " << picture << ")\n"
                    "   no picture");
    return GameType::normal;
  } // switch (picture)
}

auto solo(Card::Value const picture1, Card::Value const picture2) noexcept -> GameType
{
  if (picture1 == Card::jack  && picture2 == Card::queen)
    return GameType::solo_queen_jack;
  if (picture2 == Card::jack  && picture1 == Card::queen)
    return GameType::solo_queen_jack;
  if (picture1 == Card::jack  && picture2 == Card::king)
    return GameType::solo_king_jack;
  if (picture2 == Card::jack && picture1 == Card::king)
    return GameType::solo_king_jack;
  if (picture1 == Card::queen  && picture2 == Card::king)
    return GameType::solo_king_queen;
  if (picture2 == Card::queen && picture1 == Card::king)
    return GameType::solo_king_queen;
  DEBUG_ASSERTION(false,
                  "solo(picture1 = " << picture1 << ", picture2 = " << picture2 << ")\n"
                  "   picture1 and picture2 no pictures or equal");
  return GameType::normal;
}

auto solo(Card::Color const color) noexcept -> GameType
{
  switch (color) {
  case Card::club:
    return GameType::solo_club;
  case Card::spade:
    return GameType::solo_spade;
  case Card::heart:
    return GameType::solo_heart;
  case Card::diamond:
    return GameType::solo_diamond;
  default:
    DEBUG_ASSERTION(false,
                    "solo(color = " << color << ")\n"
                    "   no color");
    return GameType::normal;
  }
}

auto to_string(Card::Value const value) noexcept -> string
{
  switch(value) {
  case Card::nine:
    (void)_("Card::Value::nine");
    return "nine";
  case Card::jack:
    (void)_("Card::Value::jack");
    return "jack";
  case Card::queen:
    (void)_("Card::Value::queen");
    return "queen";
  case Card::king:
    (void)_("Card::Value::king");
    return "king";
  case Card::ten:
    (void)_("Card::Value::ten");
    return "ten";
  case Card::ace:
    (void)_("Card::Value::ace");
    return "ace";
  case Card::nocardvalue:
    (void)_("Card::Value::no card value");
    return "no card value";
  case Card::unknowncardvalue:
    (void)_("Card::Value::unknown card value");
    return "unknown card value";
  } // switch(value)

  return {};
}

auto to_string(Card::TColor const tcolor) noexcept -> string
{
  switch(tcolor) {
  case Card::club:
    (void)_("Card::Color::club");
    return "club";
  case Card::spade:
    (void)_("Card::Color::spade");
    return "spade";
  case Card::heart:
    (void)_("Card::Color::heart");
    return "heart";
  case Card::diamond:
    (void)_("Card::Color::diamond");
    return "diamond";
  case Card::trump:
    (void)_("Card::Color::trump");
    return "trump";
  case Card::nocardcolor:
    (void)_("Card::Color::no card color");
    return "no card color";
  case Card::unknowncardcolor:
    (void)_("Card::Color::unknown card color");
    return "unknown card color";
  } // switch(tcolor)

  return {};
}

auto to_string(Card const& card) noexcept -> string
{
  if (card.is_empty())
    return "empty";
  else if (card.is_unknown())
    return "unknown";
  else
    return (to_string(card.color()) + " " + to_string(card.value()));
}


auto to_unicode(Card const& card) noexcept -> string
{
  switch(card.color()) {
  case Card::club:
    switch (card.value()) {
    case Card::nine:  return "🃙";
    case Card::jack:  return "🃛";
    case Card::queen: return "🃝";
    case Card::king:  return "🃞";
    case Card::ten:   return "🃚";
    case Card::ace:   return "🃑";
    case Card::nocardvalue:      return "🂠";
    case Card::unknowncardvalue: return "🃟";
    }
    return {};
  case Card::spade:
    switch (card.value()) {
    case Card::nine:  return "🂩";
    case Card::jack:  return "🂫";
    case Card::queen: return "🂭";
    case Card::king:  return "🂮";
    case Card::ten:   return "🂪";
    case Card::ace:   return "🂡";
    case Card::nocardvalue:      return "🂠";
    case Card::unknowncardvalue: return "🃟";
    }
    return {};
  case Card::heart:
    switch (card.value()) {
    case Card::nine:  return "🂹";
    case Card::jack:  return "🂻";
    case Card::queen: return "🂽";
    case Card::king:  return "🂾";
    case Card::ten:   return "🂺";
    case Card::ace:   return "🂱";
    case Card::nocardvalue:      return "🂠";
    case Card::unknowncardvalue: return "🃟";
    }
    return {};
  case Card::diamond:
    switch (card.value()) {
    case Card::nine:  return "🃉";
    case Card::jack:  return "🃋";
    case Card::queen: return "🃍";
    case Card::king:  return "🃊";
    case Card::ten:   return "🃊";
    case Card::ace:   return "🃁";
    case Card::nocardvalue:      return "🂠";
    case Card::unknowncardvalue: return "🃟";
    }
    return {};
  case Card::trump:            return "🃏";
  case Card::nocardcolor:      return "🂠";
  case Card::unknowncardcolor: return "🃟";
  } // switch(tcolor)
  return {};
}


auto to_unicode(Card::Color color)  noexcept -> string
{
  switch(color) {
  case Card::club:    return "♣";
  case Card::spade:   return "♠";
  case Card::heart:   return "♥";
  case Card::diamond: return "♦";
  case Card::trump:
  case Card::nocardcolor:
  case Card::unknowncardcolor:
    return "";
  } // switch(tcolor)
  return {};
}


auto gettext(Card::Value const value) -> string
{
  return gettext("Card::Value::" + to_string(value));
}

auto gettext(Card::Color const color) -> string
{
  return gettext("Card::Color::" + to_string(color));
}

auto gettext(Card const& card) -> string
{
  // gettext: %t = color, %t = value
  if (card == Card::unknown)
    return _("Card::unknown");
  return _("Card::%t %t",
           _(card.color()), _(card.value()));
}

auto operator>(Card::Color const lhs, Card::Color const rhs) -> bool
{
  // greater is in the order: 'club', 'spade', 'heart', 'diamond'
  switch (lhs) {
  case Card::club:
    return (   (rhs == Card::spade)
            || (rhs == Card::heart)
            || (rhs == Card::diamond) );
  case Card::spade:
    return (   (rhs == Card::heart)
            || (rhs == Card::diamond) );
  case Card::heart:
    return (rhs == Card::diamond);
  case Card::diamond:
    return false;
  default:
    DEBUG_ASSERTION(false,
                    "operator>(lhs, rhs): illegal value");

    break;
  }

  return false;
}

auto operator<<(ostream& ostr, Card::Value const value) -> ostream&
{
  ostr << to_string(value);
  return ostr;
}

auto operator>>(istream& istr, Card::Value& value) -> istream&
{
  string value_name;
  istr >> value_name;

  value = Card::value(value_name);

  return istr;
}

auto operator<<(ostream& ostr, Card::TColor const tcolor) -> ostream&
{
  ostr << to_string(tcolor);
  return ostr;
}

auto operator>>(istream& istr, Card::Color& color) -> istream&
{
  string color_name;
  istr >> color_name;
  color = Card::color(color_name);

  return istr;
}

auto operator<<(ostream& ostr, Card const& card) -> ostream&
{
  ostr << to_string(card);
  return ostr;
}

auto operator>>(istream& istr, Card& card) -> istream&
{
  return card.read(istr);
}

auto operator<<(ostream& ostr, vector<Card> const& cards) -> ostream&
{
  if (cards.empty())
    return ostr;

  ostr << cards[0];
  for (size_t i = 1; i < cards.size(); ++i)
    ostr << ", " << cards[i];
  return ostr;
}

