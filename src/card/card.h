/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/game_type.h"
#include "../basetypes/marriage_selector.h"

class HandCard;
class Game;

/** Class for a card
 **/
class Card {
public:
  static Card const empty;
  static Card const unknown;

  static Card const fox;
  static Card const dulle;
  static Card const charlie;

  static Card const club_ace;
  static Card const spade_ace;
  static Card const heart_ace;
  static Card const diamond_ace;
  static Card const club_ten;
  static Card const spade_ten;
  static Card const heart_ten;
  static Card const diamond_ten;
  static Card const club_king;
  static Card const spade_king;
  static Card const heart_king;
  static Card const diamond_king;
  static Card const club_queen;
  static Card const spade_queen;
  static Card const heart_queen;
  static Card const diamond_queen;
  static Card const club_jack;
  static Card const spade_jack;
  static Card const heart_jack;
  static Card const diamond_jack;
  static Card const club_nine;
  static Card const spade_nine;
  static Card const heart_nine;
  static Card const diamond_nine;

  static unsigned constexpr number_of_colors  = 4;
  static unsigned constexpr number_of_tcolors = number_of_colors + 1;
  static unsigned constexpr number_of_values  = 6;
  static unsigned constexpr number_of_cards   = number_of_colors * number_of_values;

  enum Value : int8_t {
    nine  =  0,
    jack  =  2,
    queen =  3,
    king  =  4,
    ten   = 10,
    ace   = 11,
    nocardvalue      = 100, // for simpler error-tracking
    unknowncardvalue = 101,
    maxcardvalue     = unknowncardvalue
  };

  enum TColor : int8_t {
    diamond = 0,
    heart,
    spade,
    club,
    trump,
    nocardcolor      = 100, // for simpler error-tracking
    unknowncardcolor = 101,
    maxcardcolor     = unknowncardcolor
  };
  using Color = TColor;

  static auto value(string const& value) -> Value;
  static auto color(string const& color) -> Color ;

public:

  constexpr Card()                 noexcept;
  constexpr Card(Color c, Value v) noexcept;
  Card(string const& name);

  //virtual ~Card() = default;

  auto read(istream& istr) -> istream&;

  constexpr auto color() const noexcept -> Color;
  constexpr auto value() const noexcept -> Value;

  constexpr auto is_empty()          const noexcept -> bool;
  constexpr explicit operator bool() const noexcept;
  constexpr auto is_unknown()        const noexcept -> bool;

  constexpr auto points() const noexcept -> unsigned;

  auto tcolor(Game const& game)                const -> TColor;
  auto istrump(Game const& game)               const -> bool;
  auto istrump(GameType gametype, bool dullen) const -> bool;
  auto isdulle(Game const& game)               const -> bool;
  auto isdulle(GameType gametype, bool dullen) const -> bool;

  constexpr auto operator==(Card const& rhs) const noexcept -> bool;
  constexpr auto operator!=(Card const& rhs) const noexcept -> bool;
private:
  Color color_ = nocardcolor;
  Value value_ = nocardvalue;
}; // class Card

namespace std {
template<> struct less<Card> {
  inline auto operator() (Card const& lhs, Card const& rhs) const -> bool
  {
    return (   lhs.color() < rhs.color()
            || (   lhs.color() == rhs.color()
                && lhs.value() < rhs.value()));
  }
};
} // namespace std

auto higher_values_descending(Card::Value value) -> vector<Card::Value>;

// whether the marriage is determined by a 'tcolor'-trick
auto is_selector(Card::TColor tcolor, MarriageSelector marriage_selector) noexcept -> bool;

auto solo(Card::Value picture)                        noexcept -> GameType;
auto solo(Card::Value picture1, Card::Value picture2) noexcept -> GameType;
auto solo(Card::Color color)                          noexcept -> GameType;

auto to_string(Card::Value value)   noexcept -> string;
auto to_string(Card::TColor tcolor) noexcept -> string;
auto to_string(Card const& card)    noexcept -> string;
auto to_unicode(Card const& card)   noexcept -> string;
auto to_unicode(Card::Color color)  noexcept -> string;
auto gettext(Card::Value value)              -> string;
auto gettext(Card::Color color)              -> string;
auto gettext(Card const& card)               -> string;

auto operator<<(ostream& ostr, Card::Value value)         -> ostream&;
auto operator>>(istream& istr, Card::Value& value)        -> istream&;
auto operator<<(ostream& ostr, Card::TColor tcolor)       -> ostream&;
auto operator>>(istream& istr, Card::Color& color)        -> istream&;
auto operator<<(ostream& ostr, Card const& card)          -> ostream&;
auto operator>>(istream& istr, Card& card)                -> istream&;
auto operator<<(ostream& ostr, vector<Card> const& cards) -> ostream&;

#include "card.hpp"
