/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "card.h"

/** counts the number of the single cards
 ** gives additionally a vector with the cards
 **
 ** @todo   replace map with vector
 **/
class CardCounter : private map<Card, unsigned> {
  using Base = map<Card, unsigned>;
  friend auto operator==(CardCounter const& lhs, CardCounter const& rhs) -> bool;
  friend auto operator!=(CardCounter const& lhs, CardCounter const& rhs) -> bool;

public:
  CardCounter() = default;
  CardCounter(CardCounter const& card_counter) = default;
  auto operator=(CardCounter const& card_counter) -> CardCounter& = default;
  ~CardCounter() = default;

  void clear();

  void write(ostream& ostr) const;

  using Base::const_iterator;
  auto begin() const -> const_iterator;
  auto end()   const -> const_iterator;
  using Base::size;
  using Base::empty;


  auto cards()    const -> vector<Card> const&;
  auto cards_no() const -> unsigned;
  auto operator[](Card card) const -> unsigned;
  auto operator()(Card::TColor tcolor, Game const& game) const -> unsigned;

  auto set(Card card, unsigned n)     -> bool;
  auto min_set(Card card, unsigned n) -> bool;
  auto max_set(Card card, unsigned n) -> bool;

  auto erase(Card card) -> bool;
  void inc(Card card);
  void add(Card card, unsigned n);
  void dec(Card card);

  auto contains(vector<Card> const& cards)            const -> bool;
  auto contains(map<Card, unsigned> const& cards)     const -> bool;
  auto is_contained(vector<Card> const& cards)        const -> bool;
  auto is_contained(map<Card, unsigned> const& cards) const -> bool;

private:
  unsigned cards_no_ = 0;
  vector<Card> cards_;

  vector<unsigned> no_ = vector<unsigned>(Card::number_of_cards + 1, 0);
}; // class CardCounter : private map<Card, unsigned>

auto operator<<(ostream& ostr, CardCounter const& card_counter) -> ostream&;

auto operator==(CardCounter const& lhs, CardCounter const& rhs) -> bool;
auto operator!=(CardCounter const& lhs, CardCounter const& rhs) -> bool;
