/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */


#include "constants.h"
#include "hand.h"

#include "sorted_hand.h"
#include "sorted_hand_const.h"
#include "trick.h"
#include "../player/player.h"
#include "../game/game.h"
#include "../party/rule.h"
#include "../misc/preferences.h"
#include "../utils/string.h"

Hand::Hand() :
  cards_(*this),
  cards_all_(*this)
{ }


Hand::Hand(Player const& player):
  player_(&player),
  cards_(*this),
  cards_all_(*this)
{ }


Hand::Hand(Player const& player, Hand const& hand):
  player_(&player),
  cards_(*this),
  cards_all_(*this),
  requested_position_(hand.requested_position_)
{
  add(hand);
}


Hand::Hand(vector<Card> const& cards) :
  cards_(*this),
  cards_all_(*this)
{
  add(cards);
}


Hand::Hand(Player const& player, vector<Card> const& cards) :
  player_(&player),
  cards_(*this),
  cards_all_(*this)
{
  add(cards);
}


Hand::Hand(Player const& player,
           vector<Card> const& unplayed_cards,
           vector<Card> const& played_cards) :
  player_(&player),
  cards_(*this),
  cards_all_(*this)
{
  add_played(played_cards);
  add(unplayed_cards);
}


Hand::Hand(string const& text) :
  cards_(*this),
  cards_all_(*this)
{
  auto p = text.begin();
  while (p != text.end()) {
    auto const p2 = find(p, text.end(), ',');
    add(Card(string(p, p2)));
    p = p2;
    while (   (p != text.end())
           && (   (*p == ',')
               || (*p == ' '))) {
      ++p;
    }
  }
}


Hand::Hand(istream& istr) :
  cards_(*this),
  cards_all_(*this)
{
  string line;
  while (istr.good()) {
    if (istr.peek() == '}')
      return ;
    if (!istr.good())
      return ;
    // read the lines until there is an empty line or eof
    getline(istr, line);
    if (!line.empty() && (*line.rbegin() == '\r'))
      line.erase(line.end() - 1);
    if (line.empty())
      break;
    if (line[0] == '#')
      continue;

    // the line is one of the following four formats:
    // diamond king
    // diamond king  (played 2)
    // 0  diamond king
    // 0  diamond king  (played 2)
    String::remove_blanks(line);
    if (line[1] == ' ')
      String::word_first_remove(line);

    string card_name;
    if (line == "unknown") {
      card_name = line;
      String::word_first_remove(line);
    } else {
      card_name = String::word_first(line);
      String::word_first_remove(line);
      card_name += " ";
      card_name += String::word_first(line);
      String::word_first_remove(line);
    }

    HandCard card(*this, card_name);
    if (card.is_empty())
      break;
    cards_.push_back(card);
    cards_all_.push_back(card);

    String::remove_blanks(line);
    if (line.empty()) {
      // the card has not been played, yet
      played_.push_back(UINT_MAX);
    } else { // if (line.empty())
      // the card has been played
      // the line is now like: (played 1)
      if (String::word_first(line) != "(played")
        break;
      String::word_first_remove(line);
      String::remove_blanks(line);

      played_.push_back(static_cast<unsigned>(atoi(line.c_str())));
    } // if !(line.empty())
  } // while (istr.good())
}


Hand::Hand(Hand const& hand) :
  Hand(*hand.player_, hand)
{ }


auto Hand::operator=(Hand const& hand) -> Hand&
{
  if (this == &hand)
    return *this;

  cards_.clear();
  cards_all_.clear();
  played_.clear();
  add(hand);
  requested_position_ = hand.requested_position();
  player_ = hand.player_;

  return *this;
}


Hand::operator HandCards() const
{
  return cards_;
}


Hand::operator vector<Card>() const
{
  return cards_;
}

auto Hand::begin() const -> const_iterator
{ return cards_.begin(); }


auto Hand::end() const -> const_iterator
{ return cards_.end(); }


auto Hand::cards() const -> HandCards const&
{ return cards_; }


auto Hand::cards_all() const -> HandCards const&
{ return cards_all_; }


auto Hand::sorted() -> SortedHand
{ return SortedHand(*this); }


auto Hand::sorted() const -> SortedHandConst
{ return SortedHandConst(*this); }


auto Hand::self_check() const -> bool
{
  { // check the cards with cards_all
    unsigned c = 0;
    for (unsigned ca = 0; ca < cardsnumber_all(); ++ca) {
      if (!played(ca)) {
        DEBUG_ASSERTION((card(c) == card_all(ca)),
                        "Hand::self_check():\n"
                        "  card '" << card(c) << "' on position " << c
                        << " is not the same as card all '"
                        << card_all(ca) << "' at position " << ca << "\n"
                        << *this);
        c += 1;
      } // if (!played(ca))
    }
  } // check the cards with cards_all

  { // check the hand of the cards
    for (auto const& c : cards_)
      DEBUG_ASSERTION((&c.hand() == this),
                      "Hand::self_check():\n"
                      "  card '" << c << "' does not belong to this hand.\n"
                      << *this);
    for (auto const& c : cards_all_)
      DEBUG_ASSERTION((&c.hand() == this),
                      "Hand::self_check():\n"
                      "  card_all '" << c
                      << "' does not belong to this hand.\n"
                      << *this);
  } // check the hand of the cards

  return true;
}


auto Hand::empty() const -> bool
{
  return (cardsnumber() == 0);
}


auto Hand::cardsnumber() const -> size_t
{
  return cards_.size();
}


auto Hand::cardsnumber_all() const -> size_t
{
  return cards_all_.size();
}


auto Hand::points() const -> unsigned
{
  unsigned points = 0;

  for (auto const& c : cards_)
    points += c.points();

  return points;
}


void Hand::set_known(Card const card, unsigned n)
{
  if (n == 0)
    return;

  DEBUG_ASSERTION(count(Card::unknown) >= n,
                  "Hand::set_unknown(" << card << ", " << n << ")\n"
                  "  not enough unknown cards:\n"
                  << *this);

  for (; n > 0; --n) {
    auto const pos = getpos(Card::unknown);
    cards_[pos] = card;
    cards_[pos].set_hand(*this);
    cards_all_[pos_to_pos_all(pos)] = card;
    cards_all_[pos_to_pos_all(pos)].set_hand(*this);
  }
}


auto Hand::card(unsigned const i) const -> HandCard
{
#ifdef WORKAROUND
  if (   cardsnumber() < 3
      && i <= 3 ){
    DEBUG_ASSERTION((i < cardsnumber()),
                    "Hand::card(i):\n"
                    "  invalid value 'i' = " << i
                    << " (max = " << cardsnumber() << ")\n"
                    "Hand:\n"
                    << *this
                    << "\n"
                    << "You seem to encounter this bug in a poverty while changing the cards.\n"
                    << "Please write us how the cards are lying (or send us a screenshot from the table) and tell us what your last action was (where you have clicked with which button with your mouse)."
                    << "Thank you\n"
                    << "--\n"
                    << "Sie scheinen diesen Fehler während einer Armut bei dem Auswechseln der Karten zu erhalten.\n"
                    << "Bitte schreiben Sie uns zusätzlich zum Fehlerbericht, wie die Karten liegen (oder machen Sie einen Bildschirmabzug (screenshot) ) und schreiben Sie uns, welche Aktionen Sie zuletzt durchgeführt haben (wohin Sie mit welcher Maustaste geklickt haben).\n"
                    << "Vielen Dank");
  } // if (in poverty)
#endif

  DEBUG_ASSERTION((i < cardsnumber()),
                  "Hand::card(i):\n"
                  "  invalid value 'i' = " << i
                  << " (max = " << cardsnumber() << ")\n"
                  "Hand:\n"
                  << *this);

  DEBUG_ASSERTION(!cards_[i].is_empty(),
                  "Hand::card(i):\n"
                  "  empty card " << i << ":\n"
                  << *this);

  return cards_[i];
}


auto Hand::card_all(unsigned const i) const -> HandCard
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "Error: file " << __FILE__ << ", line " << __LINE__ << ", "
                  << "function Hand::card_all()\n"
                  << "  illegal value " << i
                  << " (maximal value is: " << cardsnumber_all() - 1 << ")");

  DEBUG_ASSERTION(!cards_all_[i].is_empty(),
                  "Hand::card_all(i):\n"
                  "  empty card " << i << ":\n");

  return cards_all_[i];
}


auto Hand::count_played() const -> unsigned
{
  return (cardsnumber_all() - container_algorithm::count(played_, UINT_MAX));
}


auto Hand::count_played(Card::TColor const tcolor) const -> unsigned
{
  auto is_tcolor = [tcolor](auto const card) { return card.tcolor() == tcolor; };
  return (  container_algorithm::count_if(cards_all_, is_tcolor)
          - container_algorithm::count_if(cards_,     is_tcolor));
}


auto Hand::count_played(Card const card) const -> unsigned
{
  return (count_all(card) - count(card));
}


auto Hand::played(unsigned const i) const -> bool
{
  return (played_trick(i) != UINT_MAX);
}


auto Hand::played_trick(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "Hand::played()\n"
                  "  illegal value i = " << i
                  << " (>= " << cardsnumber_all() << ")");

  return played_[i];
}


auto Hand::cards_single() const -> HandCards
{
  HandCards cards_single(*this);

  for (auto const& c : cards_) {
    // search the card in the til now selected cards
    if (!container_algorithm::contains(cards_single, c))
      cards_single.push_back(c);
  } // for (c : cards_)

  return cards_single;
}


auto Hand::counted_cards() const -> map<Card, unsigned>
{
  map<Card, unsigned> counter;

  for (auto c : cards_)
    counter[c] += 1;

  return counter;
}


auto Hand::highest_value() const -> Card::Value
{
  return cards_.highest_value();
}


auto Hand::highest_value(Card::TColor const tcolor) const -> Card::Value
{
  DEBUG_ASSERTION(player_,
                  "Hand:highest_value(tcolor):\n"
                  "  hand does not belongs to a player");

  return cards_.highest_value(tcolor);
}


auto Hand::lowest_value() const -> Card::Value
{
  return cards_.lowest_value();
}


auto Hand::lowest_value(Card::TColor const tcolor) const -> Card::Value
{
  DEBUG_ASSERTION(player_,
                  "Hand:lowest_value(tcolor):\n"
                  "  hand does not belongs to a player");

  return cards_.lowest_value(tcolor);
}


auto Hand::highest_card() const -> HandCard
{
  DEBUG_ASSERTION(player_,
                  "Hand::highest_card():\n"
                  "  hand does not belongs to a player");

  return cards_.highest_card();
}


auto Hand::highest_trump() const -> HandCard
{
  return cards_.highest_trump();
}


auto Hand::highest_trump_till(Card const card_limit) const -> HandCard
{
  return cards_.highest_trump_till(card_limit);
}


auto Hand::highest_card(Card::TColor const tcolor) const -> HandCard
{
  return cards_.highest_card(tcolor);
}


auto Hand::second_highest_card(Card::TColor const tcolor) const -> HandCard
{
  return cards_.second_highest_card(tcolor);
}


auto Hand::highest_card(Card::Value const value) const -> HandCard
{
  return cards_.highest_card(value);
}


auto Hand::lowest_trump() const -> HandCard
{
  return cards_.lowest_trump();
}


auto Hand::lowest_card(Card::TColor const tcolor) const -> HandCard
{
  DEBUG_ASSERTION(player_,
                  "Hand::lowest_card():\n"
                  "  hand does not belongs to a player");

  return cards_.lowest_card(tcolor);
}


auto Hand::lowest_card(Card::Value const value) const -> HandCard
{
  return cards_.lowest_card(value);
}


auto Hand::higher_card_exists(Card const card) const -> bool
{
  return cards_.higher_card_exists(card);
}


auto Hand::lower_card_exists(Card const card) const -> bool
{
  return cards_.lower_card_exists(card);
}


auto Hand::higher_cards_no(Card const card) const -> unsigned
{
  return cards_.higher_cards_no(card);
}


auto Hand::next_jabbing_card(Card const card) const -> HandCard
{
  return cards_.next_jabbing_card(card);
}


auto Hand::next_jabbing_card(Trick const& trick) const -> HandCard
{
  return cards_.next_jabbing_card(trick.winnercard());
}


auto Hand::next_higher_card(Card const card) const -> HandCard
{
  return cards_.next_higher_card(card);
}


auto Hand::same_or_higher_card(Card const card) const -> HandCard
{
  return cards_.same_or_higher_card(card);
}


auto Hand::next_lower_card(Card const card) const -> HandCard
{
  return cards_.next_lower_card(card);
}


auto Hand::same_or_lower_card(Card const card) const -> HandCard
{
  return cards_.same_or_lower_card(card);
}


auto Hand::can_jab(Trick const& trick) const -> bool
{
  return validcards(trick).higher_card_exists(trick.winnercard());
}


auto Hand::can_jab(Card const card) const -> bool
{
  return cards_.higher_card_exists(card);
}


auto Hand::pos_to_pos_all(unsigned pos) const -> unsigned
{
  if (pos == UINT_MAX)
    return UINT_MAX;

  if ( (pos >= cardsnumber())
      || (played_.empty()) ) {
    DEBUG_ASSERTION(false,
                    "Error: file " << __FILE__ << ", line " << __LINE__ << ", "
                    << "function Hand::pos_to_pos_all(" << pos << ")\n"
                    << "  illegal value " << pos
                    << " (max value is: " << cardsnumber() - 1 << ")");
    return UINT_MAX;
  }

  // translate the position
  unsigned n = 0;
  for (pos += 1; pos > 0; n++)
    if (!played(n))
      pos--;

  return (n - 1);
}


auto Hand::pos_all_to_pos(unsigned pos) const -> unsigned
{
  if (pos == UINT_MAX)
    return UINT_MAX;

  if ( (pos >= cardsnumber_all())
      || (played_.empty()) ) {
    DEBUG_ASSERTION(false,
                    "Error: file " << __FILE__ << ", line " << __LINE__ << ", "
                    << "function Hand::pos_all_to_pos(" << pos << ")\n"
                    << "  illegal value " << pos
                    << " (max value is: " << cardsnumber_all() - 1 << ")");
  }

  // translate the position
  unsigned n = 0;
  for (pos += 1; pos > 0; pos--)
    if (!played(pos - 1))
      n++;

  return (n - 1);
}


auto Hand::player() const -> Player const&
{
  DEBUG_ASSERTION(player_,
                  "Hand::player():\n"
                  "  player_ == nullptr");

  return *player_;
}


void Hand::set_player(Player const& player)
{
  player_ = &player;
}


auto Hand::game() const -> Game const&
{
  return player().game();
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Hand::getpos(Card const c) const -> unsigned
{
  // if the card is in the requested position, take that
  if (   (requested_position() < cardsnumber())
      && (card(requested_position()) == c)
     )
    return requested_position();

  // search the card
  for (unsigned i = 0; i < cardsnumber(); i++)
    if (c == card(i))
      // the card has been found
      return i;

  if (   c != Card::unknown
      && contains(Card::unknown))
    return getpos(Card::unknown);

  DEBUG_ASSERTION(false,
                  "Hand::getpos(" << c << ")\n"
                  "  card is not in the hand: " << c << '\n'
                  << *this);

  // the card is not in the hand
  return UINT_MAX;
}


auto Hand::getpos_all(Card const c) const -> unsigned
{
  // if the card is in the requested position, take that
  if (   (requested_position() < cardsnumber())
      && (card_all(requested_position()) == c)
     )
    return requested_position();

  // search the card (in played form)
  for (unsigned i = 0; i < cardsnumber_all(); i++)
    if (played(i))
      if (c == card_all(i))
        // the card has been found
        return i;

  // search the card
  for (unsigned i = 0; i < cardsnumber_all(); i++)
    if (c == card_all(i))
      // the card has been found
      return i;

  // the card is not in the hand
  return UINT_MAX;
}


void Hand::playcard(Card const card)
{
  DEBUG_ASSERTION(contains(card),
                  "Hand::playcard(card = " << card << "):\n"
                  "  the card does not exists in the hand:\n"
                  << *this);


  auto const pos = getpos(card);
  if (cards_[pos] == Card::unknown) {
    cards_[pos] = card;
    cards_all_[pos_to_pos_all(pos)] = card;
  }
  playcard(pos);
}


void Hand::playcard(unsigned const pos)
{
  DEBUG_ASSERTION((pos != UINT_MAX),
                  "function Hand::playcard(pos)\n"
                  "  illegal position 'UINT_MAX'"
                  << *this);

  DEBUG_ASSERTION((pos < cardsnumber()),
                  "function Hand::playcard(" << pos << ")\n"
                  << "  illegal position"
                  << " (maximal value is " << cardsnumber() << ")");

  DEBUG_ASSERTION(!card(pos).is_empty(),
                  "Hand::playcard(" << pos << ")\n"
                  << "  empty card: " << card(pos));

  DEBUG_ASSERTION(player_,
                  "Hand::playcard(" << pos << ")\n"
                  << "  no player set");

  auto const& game = player_->game();

  // mark the card as being played
  if (game.status() == Game::Status::poverty_shift) {
    played_[pos_to_pos_all(pos)] = pos;
  } else { //
    played_[pos_to_pos_all(pos)]
      = cardsnumber_all() - cardsnumber();
  }

  cards_.erase(cards_.begin() + pos);
  requested_position_ = UINT_MAX;
}


void Hand::unplaycard(Card const card)
{
  unsigned trick_max = UINT_MAX;
  unsigned best_cardno = UINT_MAX;
  for (unsigned i = 0; i < cardsnumber_all(); i++) {
    if (   card_all(i) == card
        && played(i)) {
      if (   best_cardno == UINT_MAX
          || trick_max < played_trick(i)) {
        best_cardno = i;
        trick_max = played_trick(i);
      }
    }
  }

  DEBUG_ASSERTION(best_cardno != UINT_MAX,
                  "Hand::unplaycard(" << card << "):\n"
                  "  card not found:\n"
                  << *this);

  // unmark the card and add it to the hand
  played_[best_cardno] = UINT_MAX;
  unsigned j = 0; // position in 'cards_'
  for (; best_cardno > 0; best_cardno--)
    if (!played(best_cardno - 1))
      j += 1;

  cards_.insert(cards_.begin() + j, HandCard(*this, card));
}


void Hand::add(Card const card, unsigned const n)
{
  if (n == 0)
    return;
  cards_.insert(cards_.end(), n, HandCard(*this, card));
  cards_all_.insert(cards_all_.end(), n,HandCard(*this, card));
  played_.insert(played_.end(), n, UINT_MAX);
}


void Hand::add(vector<Card> const& cards)
{
  for (auto const& c : cards)
    add(c);
}


void Hand::add(Hand const& hand)
{
  for (unsigned c = 0; c < hand.cardsnumber_all(); c++) {
    if (!hand.played(c))
      cards_.push_back(HandCard(*this, hand.card_all(c)));
    cards_all_.push_back(HandCard(*this, hand.card_all(c)));
    played_.push_back(hand.played_trick(c));
  } // for (c < hand.cardsnumber_all())
}


void Hand::add_played(Card const card, unsigned const n)
{
  for (unsigned i = 0; i < n; ++i) {
    played_.push_back(cards_all_.size() - cards_.size());
    cards_all_.push_back(HandCard(*this, card));
  } // for (unsigned i = 0; i < n; ++i)
}


void Hand::add_played(vector<Card> const& cards)
{
  for (auto const& c : cards)
    add_played(c);
}


void Hand::remove(Position const position)
{
  DEBUG_ASSERTION(position < cards_all_.size(),
                  "Hand::remove(position = " << position << ")\n"
                  "  Position is not in the hand, the size of the hand is " << cards_all_.size());
  if (!played(position))
    cards_.erase(cards_.begin() + pos_all_to_pos(position));
  cards_all_.erase(cards_all_.begin() + position);
  played_.erase(played_.begin() + position);
}


void Hand::remove(HandCard const card)
{
  DEBUG_ASSERTION((player_ == card.hand().player_),
                  "Hand::remove(card):\n"
                  "  the card does not belong to the same player: " << card << '\n'
                  << *this);

  auto const pos_all = getpos_all(card);
  if (   pos_all == UINT_MAX
      && card.is_empty()) {
    return ;
  }

  DEBUG_ASSERTION((pos_all != UINT_MAX),
                  "Hand::remove(card):"
                  "  card '" << card << "' not in the hand: \n"
                  << *this);

  if (!played(pos_all))
    cards_.erase(cards_.begin() + pos_all_to_pos(pos_all));
  cards_all_.erase(cards_all_.begin() + pos_all);
  played_.erase(played_.begin() + pos_all);
}


void Hand::remove(Card const card)
{
  return remove(HandCard(*this, card));
}


void Hand::remove(vector<Card> const& cards)
{
  for (auto const& c : cards)
    remove(c);
}


void Hand::remove(HandCards const& cards)
{
  for (auto const& c : cards)
    remove(c);
}


void Hand::remove_all(Card const card)
{
  for (auto pos_all = getpos_all(card);
       pos_all != UINT_MAX;
       pos_all = getpos_all(card)) {

    if (!played(pos_all))
      cards_.erase(cards_.begin() + pos_all_to_pos(pos_all));
    cards_all_.erase(cards_all_.begin() + pos_all);
    played_.erase(played_.begin() + pos_all);
  }
}


void Hand::remove_all(std::initializer_list<Card> const& cards)
{
  for (auto const c : cards)
    remove_all(c);
}


void Hand::remove_played_cards()
{
  auto const cards = cards_;
  *this = Hand(player(), cards);
}


auto Hand::requested_position() const -> unsigned
{ return requested_position_; }


auto Hand::requested_card() const -> HandCard
{
  if (requested_position() == UINT_MAX)
    return HandCard(*this, Card());

  return card(requested_position());
}


auto Hand::requested_position_all() const -> unsigned
{
  return pos_to_pos_all(requested_position());
}


void Hand::request_position(unsigned const position)
{
  requested_position_ = position;
}


void Hand::request_position_all(unsigned const position_all)
{
  if (!played(position_all))
    request_position(pos_all_to_pos(position_all));
}


void Hand::request_card(Card const card)
{
  DEBUG_ASSERTION((count(card) > 0),
                  "Hand::request_card(card):\n"
                  "  'card' " << card << " is not in the hand:\n"
                  << *this);

  request_position(getpos(card));
}


void Hand::forget_request()
{
  requested_position_ = UINT_MAX;
}


auto Hand::validcards(Trick const& trick) const -> HandCards
{
  HandCards cards(*this);

  for (auto const& c : cards_)
    if (c.isvalid(trick))
      if (!container_algorithm::contains(cards, c))
        cards.push_back(c);

  return cards;
}


auto Hand::single_cards(Card::TColor const tcolor) const -> HandCards
{
  DEBUG_ASSERTION((cardsnumber() > 0),
                  "Hand::cards(tcolor):\n"
                  "  The hand has no cards");

  HandCards cards(*this);

  for (auto const& c : cards_)
    if (c.tcolor() == tcolor)
      if (!container_algorithm::contains(cards, c))
        cards.push_back(c);

  return cards;
}


auto Hand::cards(Card::TColor const tcolor) const -> HandCards
{
  return cards(tcolor, game().type());
}


auto Hand::cards(Card::TColor const tcolor, GameType const game_type) const -> HandCards
{
  HandCards cards(*this);

  for (auto const& c : cards_) {
    if (c.tcolor(game_type) == tcolor) {
      cards.push_back(c);
    }
  }

  return cards;
}


auto Hand::single_trumps() const -> HandCards
{
  return single_cards(Card::trump);
}


auto Hand::hastrump() const -> bool
{
  return cards_.hastrump();
}


auto Hand::hascolor() const -> bool
{
  return cards_.hascolor();
}


auto Hand::has_swines() const -> bool
{
  auto const& swines = game().swines();

  if (swines.swines_announced())
    return (   swines.swines_owner() == player()
            && contains(Card::trump, Card::ace));

  if (   game().status() == Game::Status::reservation
      && (   !::preferences(Preferences::Type::show_all_hands)
          || player().type() == Player::Type::human)
      && !player().reservation().swines)
    return false;

  return swines.swines_announcement_valid(player());
}


auto Hand::has_hyperswines() const -> bool
{
  auto const& swines = game().swines();

  if (swines.hyperswines_announced())
    return (   swines.hyperswines_owner() == player()
            && (  count(game().cards().hyperswine()) == player().game().rule(Rule::Type::number_of_same_cards)));

  if (   game().status() == Game::Status::reservation
      && (   !::preferences(Preferences::Type::show_all_hands)
          || player().type() == Player::Type::human)
      && player() == game().players().current_player()
      && (   !player().reservation().swines
          || !player().reservation().hyperswines) )
    return false;

  return swines.hyperswines_announcement_valid(player());
}


auto Hand::count(Card::Value const value) const -> unsigned
{
  return cards_.count(value);
}


auto Hand::count_rich_cards() const -> unsigned
{
  return cards_.count_rich_cards();
}


auto Hand::count_trumps() const -> unsigned
{
  return count(Card::trump);
}


auto Hand::count_color_cards() const -> unsigned
{
  return count_if(cards_,
                  [](auto const card) {
                  return !card.istrump();
                  });
}


auto Hand::count(Card::TColor const tcolor) const -> unsigned
{
  return cards_.count(tcolor);
}


auto Hand::count(Card const card) const -> unsigned
{
  return cards_.count(card);
}


auto Hand::count(Card::TColor const tcolor, Card::Value const value) const -> unsigned
{
  return cards_.count(tcolor, value);
}


auto Hand::count(Card::Color const color, Game const& game) const -> unsigned
{
  return count(color, game.type(),
                     game.rule(Rule::Type::dullen));
}


auto Hand::count(Card::Color const color,
            GameType const gametype) const -> unsigned
{
  return cards_.count(color, gametype,
                            game().rule(Rule::Type::dullen));
}


auto Hand::count(Card::Color const color,
            GameType const gametype, bool const dullen) const -> unsigned
{
  return cards_.count(color, gametype, dullen);
}


auto Hand::count_trumps(GameType const game_type) const -> unsigned
{
  return cards_.count_trumps(game_type);
}


auto Hand::count_fehl() const -> unsigned
{
  return (cardsnumber() - count(Card::trump));
}


auto Hand::count_blank_colors(GameType const game_type) const -> unsigned
{
  unsigned n = 0;
  for (auto const c : game().rule().card_colors()) {
    if (   (c != trumpcolor(game_type))
        && !contains(c))
      n += 1;
  }
  return n;
}



auto Hand::count_single_tens(GameType const gametype) const -> unsigned
{
  auto const dullen = game().rule(Rule::Type::dullen);

  unsigned result = 0;
  for (auto const c : game().rule().card_colors()) {
    if (   count(c, gametype) <= 2
        && count(c, Card::ace) == 0
        && !Card(c, Card::ace).istrump(gametype, dullen)
        && !Card(c, Card::ten).istrump(gametype, dullen))
      result += count(c, Card::ten);
  }

  return result;
}


auto Hand::count_all(Card::TColor const tcolor) const -> unsigned
{
  return count_if(cards_all_,
                  [tcolor](auto const card) {
                  return (card.tcolor() == tcolor);
                  });
}


auto Hand::count_all(Card::Value const value) const -> unsigned
{
  return count_if(cards_all_,
                  [value](auto const card) {
                  return (card.value() == value);
                  });
}


auto Hand::count_all(Card::TColor const tcolor, Card::Value const value) const -> unsigned
{
  return count_if(cards_all_,
                  [tcolor, value](auto const card) {
                  return (   card.tcolor() == tcolor
                          && card.value() == value);
                  });
}


auto Hand::count_all(Card const card) const -> unsigned
{
  return count_if(cards_all_,
                  [&card](auto const c) {
                  return (c == card);
                  });
}


auto Hand::count_rich_trumps() const -> unsigned
{
  return count_if(cards_,
                  [](auto const c) {
                  return (c.value() >= 10 && c.istrump());
                  });
}

auto Hand::count_dulle() const -> unsigned
{
  if (!game().rule(Rule::Type::dullen))
    return 0;
  return count(Card::dulle);
}


auto Hand::count_swines() const -> unsigned
{
  if (!has_swines())
    return 0;
  return count(Card::trump, Card::ace);
}


auto Hand::count_hyperswines() const -> unsigned
{
  if (!has_hyperswines())
    return 0;
  return count(Card::trump, game().rule(Rule::Type::with_nines) ? Card::nine : Card::king);
}


auto Hand::count_special() const -> unsigned
{
  return count_dulle() + count_swines() + count_hyperswines();
}


auto Hand::count_trump_aces(GameType const game_type) const -> unsigned
{
  return cards_.count_trump_aces(game_type);
}


auto Hand::count_poverty_cards() const -> unsigned
{
  if (!has_poverty())
    return UINT_MAX;

  auto const& rule = game().rule();

  if (rule(Rule::Type::poverty_shift_only_trump))
    return count(Card::trump);
  else if (!rule(Rule::Type::poverty_fox_do_not_count))
    return rule(Rule::Type::max_number_of_poverty_trumps);
  else if (!rule(Rule::Type::poverty_fox_shift_extra))
    return max(rule(Rule::Type::max_number_of_poverty_trumps),
               count_trumps(GameType::poverty));
  else
    // Cannot use 'has_swines()' in the case that swines can be
    // announced during the game.
    return max(rule(Rule::Type::max_number_of_poverty_trumps)
               + (game().swines().swines_announced()
                  ? 0
                  : count_trump_aces(GameType::poverty)
                 ),
               count_trumps(GameType::poverty));
}


auto Hand::has_poverty() const -> bool
{
  auto const& rule = game().rule();
  if (!rule(Rule::Type::poverty))
    return false;

  return (cards_.count_trumps(GameType::poverty)
          - ( (   rule(Rule::Type::poverty_fox_do_not_count)
               && !game().swines().swines_announced())
             ? count_trump_aces(GameType::poverty)
             : 0)
          <= rule(Rule::Type::max_number_of_poverty_trumps));
}


auto Hand::count_ge(Card const card) const -> unsigned
{
  return cards_.count_ge(card);
}


auto Hand::count_ge_in_color(Card const card) const -> unsigned
{
  if (card.istrump(game()))
    return cards_.count_ge(card);
  return count_if(cards_,
                  [&card](auto const& c) {
                  return (   c.tcolor() == card.color()
                          && c.value() >= card.value());
                  });
}


auto Hand::count_le(Card const card_) const -> unsigned
{
  auto const card = HandCard(*this, card_);
  auto const is_le = [&card](auto const& c) {
    return (c.is_jabbed_by(card) || c == card);
  };
  return count_if(cards_, is_le);
}


auto Hand::count_ge_trump(Card const card_) const -> unsigned
{
  auto const card = HandCard(*this, card_);
  auto const is_ge_trump = [&card](auto const& c) {
    return (c.istrump() && c.jabs(card));
  };
  return count_if(cards_, is_ge_trump);
}


auto Hand::count_le_trump(Card const card_) const -> unsigned
{
  auto const card = HandCard(*this, card_);
  auto const is_le_trump = [&card](auto const& c) {
    return (   c.istrump()
            && (c.is_jabbed_by(card) || c == card));
  };
  return count_if(cards_, is_le_trump);
}


auto Hand::count_lt_trump(Card const card_) const -> unsigned
{
  auto const card = HandCard(*this, card_);
  auto const is_lt_trump = [&card](auto const& c) {
    return (c.istrump() && c.is_jabbed_by(card));
  };
  return count_if(cards_, is_lt_trump);
}


auto Hand::rel_pos_trump(Card const card) const -> double
{
  if (!contains(Card::trump))
    return 0;
  if (!card.istrump(game()))
    return 1;

  auto const le = count_le_trump(card);
  auto const ge = count_ge_trump(card);

  return (static_cast<double>(le) / (le + ge));
}


auto Hand::has_same_cards(Hand const& hand) const -> bool
{
  return hand.cards().equal(cards_);
}


auto Hand::contains(Card::TColor const tcolor) const -> bool
{
  return cards_.contains(tcolor);
}


auto Hand::contains(Card::Value const value) const -> bool
{
  return cards_.contains(value);
}


auto Hand::contains(Card const card) const -> bool
{
  return cards_.contains(card);
}


auto Hand::contains(Card::TColor const tcolor, Card::Value const value) const -> bool
{
  return cards_.contains(tcolor, value);
}


auto Hand::contains(Hand const& hand) const -> bool
{
  for (auto const& c : hand.cards_)
    if (count(c) < hand.count(c))
      return false;

  return true;
}


auto Hand::missing_cards(Hand const& hand) const -> HandCards
{
  HandCards cards(*this);
  for (auto const& c : hand.cards_)
    if (count(c) < hand.count(c))
      cards.push_back(c);

  return cards;
}


auto Hand::contains_ten_points_in_trump() const -> bool
{
  for (auto const& c : cards_) {
    if (   (c.points() >= 10)
        && c.istrump()) {
      return true;
    }
  }
  return false;
}


auto Hand::contains_unknown_or_played() const -> bool
{
  if (cards_all_.size() > cards_.size())
    return true;
  for (auto const& c : cards_) {
    if (c == Card::unknown) {
      return true;
    }
  }
  return false;
}


auto Hand::cards_at_max_fox() const -> HandCards
{
  auto cards = HandCards(*this);
  auto const trumpcolor = game().cards().trumpcolor();
  if (trumpcolor == Card::TColor::nocardcolor)
    return cards;

  if (game().rule(Rule::Type::with_nines)) {
    for (auto const& card : {
         HandCard(*this, trumpcolor, Card::ace),
         HandCard(*this, trumpcolor, Card::ten),
         HandCard(*this, trumpcolor, Card::king)
         }) {
      if (   !card.is_special()
          && contains(card))
        cards.push_back(card);
    }
  } else {
    for (auto const& card : {
         HandCard(*this, trumpcolor, Card::ace),
         HandCard(*this, trumpcolor, Card::ten),
         HandCard(*this, trumpcolor, Card::king),
         HandCard(*this, trumpcolor, Card::nine)
         }) {
      if (   !card.is_special()
          && contains(card))
        cards.push_back(card);
    }
  }

  return cards;
}


auto operator==(Hand const& lhs, Hand const& rhs) -> bool
{
  if ((lhs.cardsnumber_all() != rhs.cardsnumber_all())
      || (lhs.cardsnumber() != rhs.cardsnumber()))
    return false;

  vector<bool>found(lhs.cardsnumber_all(), false);

  for (unsigned c = 0; c < lhs.cardsnumber_all(); c++) {
    unsigned d = 0;
    for (; d < rhs.cardsnumber_all(); d++) {
      if (!found[d]
          && (lhs.card_all(c) == rhs.card_all(d))
          && (lhs.played_trick(c) == rhs.played_trick(d)))
        break;
    } // for (d < rhs.cardsnumber_all())
    if (d < rhs.cardsnumber_all())
      found[d] = true;
    else
      return false;
  } // for (c < lhs.cardsnumber_all())

  if (lhs.requested_position() != rhs.requested_position()) {
    return false;
  }

  return true;
}


auto operator!=(Hand const& lhs, Hand const& rhs) -> bool
{
  return !(lhs == rhs);
}


auto operator<<(ostream& ostr, Hand const& hand) -> ostream&
{
  for (unsigned i = 0; i < hand.cardsnumber_all(); i++) {
    ostr << std::hex << i << std::dec << "  " << hand.card_all(i);
    if (hand.played(i))
      ostr << "\t(played " << setw(2) << hand.played_trick(i) << ")";
    ostr << "\n";
  }

  return ostr;
}


auto operator<<(ostream& ostr, Hands const& hands) -> ostream&
{
  size_t n = 0;
  for (auto const& hand : hands) {
    n = max(n, hand.cardsnumber());
  }
  for (unsigned i = 0; i < n; ++i) {
    for (auto const& hand : hands) {
      ostr << setw(15);
      if (i < hand.cardsnumber())
        ostr << hand.card(i);
      else
        ostr << "";
    }
    ostr << '\n';
  }

  return ostr;
}


auto to_string(Hand const& hand) -> string
{
  ostringstream ostr;
  for (unsigned i = 0; i < hand.cardsnumber(); ++i) {
    ostr << to_string(hand.card(i)) << '\n';
  }

  return ostr.str();
}


auto gettext(Hand const& hand) -> string
{
  ostringstream ostr;
  for (unsigned i = 0; i < hand.cardsnumber(); ++i) {
    ostr << gettext(hand.card(i)) << '\n';
  }

  return ostr.str();
}


auto gettext(Hands const& hands) -> string
{
  ostringstream ostr;
  size_t n = 0;
  for (auto const& hand : hands) {
    n = max(n, hand.cardsnumber());
  }
  for (unsigned i = 0; i < n; ++i) {
    for (auto const& hand : hands) {
      if (i < hand.cardsnumber())
        ostr << gettext(hand.card(i)) << '\t';
      else
        ostr << '\t';
    }
    ostr << '\n';
  }

  return ostr.str();
}
