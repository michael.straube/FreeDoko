/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "hand_cards.h"
class Trick;
class Player;
class SortedHand;
class SortedHandConst;
class Hand;
using Hands = vector<Hand>;

/**
 ** Hand (of a player) with all cards (played and not played)
 **/
class Hand {
public:
  using Position = unsigned;
public:
  using const_iterator = HandCards::const_iterator;

  Hand();
  explicit Hand(Player const& player);
  Hand(Player const& player, Hand const& h);
  explicit Hand(vector<Card> const& cards);
  Hand(Player const& player, vector<Card> const& cards);
  Hand(Player const& player, vector<Card> const& unplayed_cards,
       vector<Card> const& played_cards);
  explicit Hand(string const& text);
  explicit Hand(istream& istr);
  Hand(Hand const& h);
  auto operator=(Hand const& h) -> Hand&;

  ~Hand() = default;

  operator HandCards() const;
  operator vector<Card>() const;
  auto begin() const -> const_iterator;
  auto end()   const -> const_iterator;

  auto cards()     const -> HandCards const&;
  auto cards_all() const -> HandCards const&;
  auto sorted()          -> SortedHand;
  auto sorted()    const -> SortedHandConst;

  auto self_check() const -> bool;

  auto player() const -> Player const&;
  void set_player(Player const& player);

  auto game() const -> Game const&;

  auto empty()           const -> bool;
  auto cardsnumber()     const -> size_t;
  auto cardsnumber_all() const -> size_t;

  auto points() const -> unsigned;

  void set_known(Card card, unsigned n = 1);

  auto card(unsigned i)         const -> HandCard;
  auto card_all(unsigned i)     const -> HandCard;
  auto played(unsigned i)       const -> bool;
  auto played_trick(unsigned i) const -> unsigned;

  auto cards_single()  const -> HandCards;
  auto counted_cards() const -> map<Card, unsigned>;

  auto highest_value()                    const -> Card::Value;
  auto highest_value(Card::TColor tcolor) const -> Card::Value;
  auto lowest_value() const                     -> Card::Value;
  auto lowest_value(Card::TColor tcolor)  const -> Card::Value;

  auto highest_card()                           const -> HandCard;
  auto highest_trump()                          const -> HandCard;
  auto highest_trump_till(Card card_limit)      const -> HandCard;
  auto highest_card(Card::TColor tcolor)        const -> HandCard;
  auto second_highest_card(Card::TColor tcolor) const -> HandCard;
  auto highest_card(Card::Value value)          const -> HandCard;
  auto lowest_trump()                           const -> HandCard;
  auto lowest_card(Card::TColor tcolor)         const -> HandCard;
  auto lowest_card(Card::Value value)           const -> HandCard;
  auto higher_card_exists(Card card)            const -> bool;
  auto lower_card_exists(Card card)             const -> bool;
  auto higher_cards_no(Card card)               const -> unsigned;
  auto next_jabbing_card(Card card)             const -> HandCard;
  auto next_jabbing_card(Trick const& trick)    const -> HandCard;
  auto next_higher_card(Card card)              const -> HandCard;
  auto same_or_higher_card(Card card)           const -> HandCard;
  auto next_lower_card(Card card)               const -> HandCard;
  auto same_or_lower_card(Card card)            const -> HandCard;
  auto can_jab(Trick const& trick)              const -> bool;
  auto can_jab(Card card)                       const -> bool;

  auto getpos(Card card)     const -> unsigned;
  auto getpos_all(Card card) const -> unsigned;

  // transform the position in the unplayed cards to the position in all cards
  auto pos_to_pos_all(unsigned pos) const -> unsigned;
  // transform the position in all cards to the position in the unplayed cards
  auto pos_all_to_pos(unsigned pos) const -> unsigned;



  // changing

  void playcard(Card card);
  void playcard(unsigned pos);
  void unplaycard(Card card);

  void add(Card card, unsigned n = 1);
  void add(vector<Card> const& cards);
  void add(Hand const& hand);

  void add_played(Card card, unsigned n = 1);
  void add_played(vector<Card> const& cards);

  void remove(Position position);
  void remove(HandCard card);
  void remove(Card card);
  void remove(vector<Card> const& cards);
  void remove(HandCards const& cards);
  void remove_all(Card card);
  void remove_all(std::initializer_list<Card> const& cards);
  void remove_played_cards();



  // card request

  auto requested_position()     const -> unsigned;
  auto requested_card()         const -> HandCard;
  auto requested_position_all() const -> unsigned;
  void request_position(unsigned position);
  void request_position_all(unsigned position_all);
  void request_card(Card card);
  void forget_request();



  auto validcards(Trick const& trick)    const -> HandCards;
  auto single_cards(Card::TColor tcolor) const -> HandCards;
  auto cards(Card::TColor tcolor)        const -> HandCards;
  auto cards(Card::TColor tcolor, GameType game_type) const -> HandCards;
  auto single_trumps() const -> HandCards;

  auto hastrump() const -> bool;
  auto hascolor() const -> bool;

  auto has_swines()            const -> bool;
  auto has_hyperswines()       const -> bool;

  auto count(Card::Value value)                      const -> unsigned;
  auto count_rich_cards()                            const -> unsigned;
  auto count_trumps()                                const -> unsigned;
  auto count_color_cards()                           const -> unsigned;
  auto count(Card::TColor tcolor)                    const -> unsigned;
  auto count(Card card)                              const -> unsigned;
  auto count(Card::TColor tcolor, Card::Value value) const -> unsigned;

  auto count(Card::Color color, Game const& game)    const -> unsigned;
  auto count(Card::Color color, GameType gametype)   const -> unsigned;
  auto count(Card::Color color, GameType gametype, bool dullen) const -> unsigned;
  auto count_fehl()                                  const -> unsigned;
  auto count_blank_colors(GameType game_type)        const -> unsigned;

  auto count_played()                                const -> unsigned;
  auto count_played(Card::TColor tcolor)             const -> unsigned;
  auto count_played(Card card)                       const -> unsigned;

  auto count_trumps(GameType game_type)      const -> unsigned;
  auto count_single_tens(GameType game_type) const -> unsigned;
  auto count_trump_aces(GameType game_type)  const -> unsigned;
  auto count_rich_trumps()                   const -> unsigned;
  auto count_dulle()                         const -> unsigned;
  auto count_swines()                        const -> unsigned;
  auto count_hyperswines()                   const -> unsigned;
  auto count_special()                       const -> unsigned;

  auto count_poverty_cards() const -> unsigned;
  auto has_poverty()         const -> bool;

  auto count_all(Card::TColor tcolor) const -> unsigned;
  auto count_all(Card::Value value)   const -> unsigned;
  auto count_all(Card::TColor tcolor, Card::Value value) const -> unsigned;
  auto count_all(Card card)           const -> unsigned;

  auto count_ge(Card card)          const -> unsigned;
  auto count_ge_in_color(Card card) const -> unsigned;
  auto count_le(Card card)          const -> unsigned;
  auto count_ge_trump(Card card)    const -> unsigned;
  auto count_le_trump(Card card)    const -> unsigned;
  auto count_lt_trump(Card card)    const -> unsigned;
  auto rel_pos_trump(Card card)     const -> double;

  // checking

  auto has_same_cards(Hand const& hand) const -> bool;

  auto contains(Card::TColor tcolor)   const -> bool;
  auto contains(Card::Value value)     const -> bool;
  auto contains(Card card)             const -> bool;
  auto contains(Card::TColor tcolor, Card::Value value) const -> bool;
  auto contains(Hand const& hand)      const -> bool;
  auto missing_cards(Hand const& hand) const -> HandCards;

  auto contains_ten_points_in_trump() const -> bool;

  auto contains_unknown_or_played()   const -> bool;

  auto cards_at_max_fox() const -> HandCards;


private:
  Player const* player_ = nullptr;

  HandCards cards_;
  HandCards cards_all_;
  // whether a card is played and in which trick
  vector<unsigned> played_;

  unsigned requested_position_ = UINT_MAX;
};

inline auto begin(Hand const& hand)
{ return hand.begin(); }
inline auto end(Hand const& hand)
{ return hand.end(); }

auto operator<<(ostream& ostr, Hand const& hand) -> ostream&;
auto operator<<(ostream& ostr, Hands const& hands) -> ostream&;

auto to_string(Hand const& hand) -> string;
auto gettext(Hand const& hand)   -> string;
auto gettext(Hands const& hands) -> string;

auto operator==(Hand const& lhs, Hand const& rhs) -> bool;
auto operator!=(Hand const& lhs, Hand const& rhs) -> bool;
