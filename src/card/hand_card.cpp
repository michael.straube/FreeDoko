/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "hand_card.h"
#include "hand.h"

#include "trick.h"
#include "../player/player.h"
#include "../game/game.h"
#include "../party/rule.h"
#include "../party/party.h"
#include "../misc/preferences.h"

// an empty hand card
HandCard const HandCard::empty = HandCard();


HandCard::HandCard(Hand const& hand) :
  hand_(&hand)
{ }


HandCard::HandCard(Card const card) :
  Card(card)
{ }


HandCard::HandCard(Card const card, Hand const& hand) :
  Card(card),
  hand_(&hand)
{ }


HandCard::HandCard(Hand const& hand, Card const card) :
  Card(card),
  hand_(&hand)
{ }


HandCard::HandCard(Hand const& hand,
		   Card::Color const color,
		   Card::Value const value) :
  Card(color, value),
  hand_(&hand)
{ }


HandCard::HandCard(Card::Color const color,
		   Card::Value const value,
                   Hand const& hand) :
  Card(color, value),
  hand_(&hand)
{ }


auto HandCard::operator=(HandCard const& card) -> HandCard&
{
  if (this == &card)
    return *this;
  static_cast<Card&>(*this) = card;
  if (card.hand_)
    hand_ = card.hand_;

  return *this;
}


auto HandCard::operator=(Card const card) -> HandCard&
{
  static_cast<Card&>(*this) = card;

  return *this;
}


void HandCard::set_hand(Hand const& hand)
{
  hand_ = &hand;
}


auto HandCard::hand() const -> Hand const&
{
  return *hand_;
}


auto HandCard::player() const -> Player const&
{
  return hand().player();
}


auto HandCard::game() const -> Game const&
{
  return player().game();
}


auto HandCard::istrump() const -> bool
{
  return Card::istrump(game());
}


auto HandCard::istrump(GameType const game_type) const -> bool
{
  return Card::istrump(game_type, game().rule(Rule::Type::dullen));
}


auto HandCard::tcolor() const -> Card::TColor
{
  if (is_empty())
    return color();
  return (istrump()
          ? Card::trump
          : color());
}


auto HandCard::tcolor(GameType const game_type) const -> Card::TColor
{
  if (is_empty())
    return color();
  return (istrump(game_type)
          ? Card::trump
          : color());
}


auto HandCard::isdulle() const -> bool
{
  return (   *this == Card::dulle
          && Card::isdulle(game()));
}


auto HandCard::isswine() const -> bool
{
  if (is_empty())
    return false;
  return (   *this == game().cards().swine()
          && game().swines().swines_announced());
}


auto HandCard::ishyperswine() const -> bool
{
  if (is_empty())
    return false;
  return (   *this == game().cards().hyperswine()
          && game().swines().hyperswines_announced());
}


auto HandCard::is_special() const -> bool
{
  if (is_empty())
    return false;
  return (   isdulle()
          || isswine()
          || possible_swine()
          || ishyperswine()
          || possible_hyperswine()
         );
}


auto HandCard::is_at_max_fox() const -> bool
{
  if (is_special())
    return false;
  return (   *this == Card::diamond_nine
          || *this == Card::diamond_king
          || *this == Card::diamond_ten
          || *this == Card::diamond_ace);
}


auto HandCard::possible_swine() const -> bool
{
  if (is_empty())
    return false;
  return (   isswine()
          || (   hand().has_swines()
              && *this == game().cards().swine()));
}


auto HandCard::possible_hyperswine() const -> bool
{
  if (is_empty())
    return false;
  return (   ishyperswine()
          || (   hand().has_hyperswines()
              && *this == game().cards().hyperswine()
             )
         );
}


auto HandCard::isfox() const -> bool
{
  if (is_empty())
    return false;
  return (   *this == game().cards().fox()
          && is_normal(game().type())
          && !isswine());
}


auto HandCard::jabs(HandCard const& card) const -> bool
{
  return card.is_jabbed_by(*this);
}


auto HandCard::jabs(Card const card) const -> bool
{
  return jabs({hand(), card});
}


auto HandCard::is_jabbed_by(HandCard const& card) const -> bool
{
  if (card.is_empty())
    return false;
  if (is_empty())
    return true;

  { // hyperswines
    if (possible_hyperswine())
      return false;

    if (card.possible_hyperswine())
      return true;
  } // hyperswines
  { // swines
    if (possible_swine())
      return false;

    if (card.possible_swine())
      return true;
  } // swines

  return game().cards().less(*this, card);
}


auto HandCard::is_jabbed_by(Card const card) const -> bool
{
  return is_jabbed_by({hand(), card});
}


auto HandCard::relative_position(HandCard const a, HandCard const b) -> int
{
  return ::preferences(Preferences::Type::cards_order).relative_position(a, b);
}


auto HandCard::isvalid(Trick const& trick) const -> bool
{
  if (is_unknown())
    return true;

  // test the card
  DEBUG_ASSERTION(!is_empty(),
                  "HandCard::isvalid(Trick):\n"
                  "  empty Card");

  // test whether the card is in the hand
  DEBUG_ASSERTION(hand().getpos(*this) != UINT_MAX,
                  "HandCard::isvalid(Trick):\n"
                  "  Card is not in the hand"
                  ": " << *this << "\n" << hand());

  // The first person can play all cards
  if (trick.isstartcard())
    return true;

  // if there is a card with the same tcolor on the hand as the startcard,
  // the card has to have the same tcolor
  if (hand().contains(trick.startcard().tcolor()))
    return (trick.startcard().tcolor() == tcolor());

  // the tcolor of the startcard is not in the hand
  // the player can play any card
  return true;
}


auto operator<(HandCard const& lhs, HandCard const& rhs) -> bool
{
  return (   lhs != rhs
          && lhs.is_jabbed_by(rhs));
}
