/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "card.h"

class Hand;
class Player;
class Game;
class Trick;

/**
 ** class for a card which belongs to a hand (and so to a player)
 **/
class HandCard : public Card {
public:
  // empty card
  static HandCard const empty;
  // for sorting the hands
  static auto relative_position(HandCard a, HandCard b) -> int;
public:
  HandCard() = default;
  HandCard(Hand const& hand);
  explicit HandCard(Card card);
  HandCard(Card card, Hand const& hand);
  HandCard(Hand const& hand, Card card);
  HandCard(Card::Color color, Card::Value value, Hand const& hand);
  HandCard(Hand const& hand, Card::Color color, Card::Value value);
  HandCard(HandCard const& card) = default;
  auto operator=(HandCard const& card) -> HandCard&;
  auto operator=(Card card) -> HandCard&;

  void set_hand(Hand const& hand);
  auto hand() const -> Hand const&;
  auto player() const -> Player const&;
  auto game() const -> Game const&;

  auto istrump() const -> bool;
  auto istrump(GameType game_type) const -> bool;
  auto tcolor() const -> Card::TColor;
  auto tcolor(GameType game_type) const -> Card::TColor;

  auto isfox()         const -> bool;
  auto isdulle()       const -> bool;
  auto isswine()       const -> bool;
  auto ishyperswine()  const -> bool;
  auto is_special()    const -> bool;
  auto is_at_max_fox() const -> bool;

  auto possible_swine() const -> bool;
  auto possible_hyperswine() const -> bool;

  auto jabs(HandCard const& card)         const -> bool;
  auto jabs(Card card)                    const -> bool;
  auto is_jabbed_by(HandCard const& card) const -> bool;
  auto is_jabbed_by(Card card)            const -> bool;

  // returns whether the card can be played in the trick
  auto isvalid(Trick const& trick) const -> bool;

private:
  // the corresponding hand
  Hand const* hand_ = nullptr;
}; // class HandCard : public Card

auto operator<(HandCard const& lhs, HandCard const& rhs) -> bool;
