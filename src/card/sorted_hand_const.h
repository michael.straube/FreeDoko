/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

class SortedHand;
class Hand;
class HandCard;
class HandCards;
class Card;

/**
 ** A sorted hand.
 ** This is a wrap around ::Hand
 **/
class SortedHandConst {
  friend auto operator==(SortedHandConst const& lhs, SortedHandConst const& rhs) -> bool;
public:
  SortedHandConst() = delete;
  explicit SortedHandConst(Hand const& hand);
  SortedHandConst(SortedHandConst const& hand);
  auto operator=(SortedHandConst const& hand) -> SortedHandConst&;

  ~SortedHandConst();

  auto hand()            const -> Hand const&;
  operator Hand const&() const;

  auto cardsnumber()     const -> unsigned;
  auto cardsnumber_all() const -> unsigned;

  auto card(unsigned i)         const -> HandCard;
  auto card_all(unsigned i)     const -> HandCard;
  auto played(unsigned i)       const -> bool;
  auto played_trick(unsigned i) const -> unsigned;

  auto from_sorted_pos(unsigned i)     const -> unsigned;
  auto from_sorted_pos_all(unsigned i) const -> unsigned;
  auto to_sorted_pos(unsigned i)       const -> unsigned;
  auto to_sorted_pos_all(unsigned i)   const -> unsigned;

  auto pos_to_pos_all(unsigned pos) const -> unsigned;
  auto pos_all_to_pos(unsigned pos) const -> unsigned;

  auto contains_unknown_or_played() const -> bool;

  auto requested_card()         const -> HandCard;
  auto requested_position()     const -> unsigned;
  auto requested_position_all() const -> unsigned;

  void set_sorting();

private:
  auto sort()   -> bool;
  void unsort();

  void set_positions();

private:
  Hand const* hand_ = nullptr;
  vector<unsigned> to_sorted_pos_; // position in the sorted hand
  vector<unsigned> from_sorted_pos_; // original position (SortedHandConst -> Hand)

  bool sorted = false;
}; // class SortedHandConst

auto operator==(SortedHandConst const& lhs, SortedHandConst const& rhs) -> bool;
auto operator!=(SortedHandConst const& lhs, SortedHandConst const& rhs) -> bool;
