/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "tcolor_counter.h"

#include "../game/game.h"

/** constructor
 **/
TColorCounter::TColorCounter() :
  vector<unsigned>(Card::maxcardcolor+1, 0)
{ }

/** clears all data
 **/
void
TColorCounter::clear()
{
  static_cast<vector<unsigned>&>(*this) = TColorCounter();
} // void TColorCounter::clear()

/** writes the data ('card = number') in 'ostr'
 **
 ** @param    ostr   output stream to write the data to
 **/
void
TColorCounter::write(ostream& ostr) const
{
  ostr << "cardno = " << cards_no() << '\n';
  for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
    ostr << setw(14) << static_cast<Card::TColor>(c)
      << " = " << (*this)[static_cast<Card::TColor>(c)] << '\n';
} // void TColorCounter::write(ostream& ostr) const

/** @return   the total number of cards
 **/
unsigned
TColorCounter::cards_no() const
{
  unsigned n = 0;

  for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
    n += (*this)[static_cast<Card::TColor>(c)];
  n += (*this)[Card::unknowncardcolor];

  return n;
} // unsigned TColorCounter::cards_no() const

/** -> result
 **
 ** @param    tcolor   tcolor to return the number of
 **
 ** @return   the number for 'tcolor'
 **/
unsigned const&
TColorCounter::operator[](Card::TColor const tcolor) const
{
  return static_cast<vector<unsigned> const&>(*this)[tcolor];
} // unsigned TColorCounter::operator[](Card::TColor tcolor) const

/** -> result
 **
 ** @param    tcolor   tcolor to return the number of
 **
 ** @return   the number for 'tcolor'
 **/
unsigned&
TColorCounter::operator[](Card::TColor const tcolor)
{
  return static_cast<vector<unsigned>&>(*this)[tcolor];
} // unsigned& TColorCounter::operator[](Card::TColor tcolor)

/** set the counter of 'tcolor' to exactly 'n'
 **
 ** @param    tcolor   tcolor to set the number of
 ** @param    n        new number for 'tcolor'
 **
 ** @return   whether something has changed
 **/
bool
TColorCounter::set(Card::TColor const tcolor, unsigned const n)
{
  unsigned const m = (*this)[tcolor];
  if (n > m)
    return min_set(tcolor, n);
  else if (n < m)
    return max_set(tcolor, n);

  return false;
}

/** set the counter of 'tcolor' to minmal 'n'
 **
 ** @param    tcolor   tcolor to set the number of
 ** @param    n        minimal number for 'tcolor'
 **
 ** @return   whether something has changed
 **/
bool
TColorCounter::min_set(Card::TColor const tcolor, unsigned const n)
{
  unsigned const t = (*this)[tcolor];
  if (t >= n)
    return false;

  (*this)[tcolor] = n;

  return true;
} // bool TColorCounter::min_set(Card::TColor tcolor, unsigned n)

/** set the counter of 'tcolor' to maximal 'n'
 **
 ** @param    tcolor   tcolor to set the number of
 ** @param    n   maximal number for 'tcolor'
 **
 ** @return   whether something has changed
 **/
bool
TColorCounter::max_set(Card::TColor const tcolor, unsigned const n)
{
  unsigned const t = (*this)[tcolor];
  if (t <= n)
    return false;

  (*this)[tcolor] = n;

  return true;
} // bool TColorCounter::max_set(Card::TColor tcolor, unsigned n)

/** increments the counter of 'tcolor'
 **
 ** @param    tcolor   tcolor to increment
 **/
void
TColorCounter::inc(Card::TColor const tcolor)
{
  (*this)[tcolor] += 1;
} // void TColorCounter::inc(Card::TColor tcolor)

/** decrements the counter of 'tcolor'
 **
 ** @param    tcolor   tcolor to decrement
 **/
void
TColorCounter::dec(Card::TColor const tcolor)
{
  (*this)[tcolor] -= 1;
} // void TColorCounter::dec(Card::TColor tcolor)

/** writes the card counter in 'ostr'
 **
 ** @param    ostr   output stream to write into
 ** @param    tcolor_counter   object to write
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, TColorCounter const& tcolor_counter)
{
  tcolor_counter.write(ostr);
  return ostr;
} // ostream& operator<<(ostream& ostr, TColorCounter tcolor_counter)

/** -> result
 **
 ** @param    tcolor_counter_a   first object
 ** @param    tcolor_counter_b   second object
 **
 ** @return   whether the two objects are equal
 **/
bool
operator==(TColorCounter const& tcolor_counter_a,
           TColorCounter const& tcolor_counter_b)
{
  return (static_cast<vector<unsigned> const&>(tcolor_counter_a)
          == static_cast<vector<unsigned> const&>(tcolor_counter_b));
} // bool operator==(TColorCounter tcolor_counter_a, TColorCounter tcolor_counter_b)

/** -> result
 **
 ** @param    tcolor_counter_a   first object
 ** @param    tcolor_counter_b   second object
 **
 ** @return   whether the two objects differ
 **/
bool
operator!=(TColorCounter const& tcolor_counter_a,
           TColorCounter const& tcolor_counter_b)
{
  return (static_cast<vector<unsigned> const&>(tcolor_counter_a)
          != static_cast<vector<unsigned> const&>(tcolor_counter_b));
} // bool operator!=(TColorCounter tcolor_counter_a, TColorCounter tcolor_counter_b)
