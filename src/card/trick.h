/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "trick_cards.h"
#include "../game/specialpoint.h"
class Game;
class Player;
class Hand;

/**
 ** represents a trick (the cards and the startplayer)
 **/
class Trick {
  friend class Game;
  friend auto operator<<(ostream& ostr, Trick const& trick) -> ostream&;
public:
  static Trick const empty;

  using Cards = TrickCards;
  using Points = unsigned;
  using const_iterator = TrickCards::const_iterator;

  explicit Trick(Player const& startplayer);
  Trick(unsigned startplayer, vector<Card> const& cards,
        unsigned winnerplayer = UINT_MAX);
  Trick(istream& istr);
  Trick();
  Trick(Trick const& trick);
  auto operator=(Trick const& trick) -> Trick&;

  auto begin() const -> const_iterator;
  auto end()   const -> const_iterator;

  void set_game(Game const& game);
  auto game() const -> Game const&;

  auto cards() const -> Cards const&;

  auto no()          const -> unsigned;
  auto intrickpile() const -> bool;

  auto self_check() const -> bool;

  void add(HandCard card);
  auto operator+(HandCard card) const -> Trick;

  auto till_card(unsigned c)               const -> Trick;
  auto till_player(Player const& player)   const -> Trick;
  auto before_player(Player const& player) const -> Trick;
  auto before_last_played_card()           const -> Trick;

  auto card(unsigned i)                     const -> HandCard;
  auto card_of_player(Player const& player) const -> HandCard;


  auto player_of_card(unsigned c)             const -> Player const&;
  auto playerno_of_card(unsigned c)           const -> unsigned;
  auto cardno_of_player(Player const& player) const -> unsigned;
  auto has_played(Player const& player)       const -> bool;
  auto playes_before(Player const& lhs, Player const& rhs) const -> bool;
  auto contains(Card card) const -> bool;
  auto contains_trump()    const -> bool;
  auto contains_fox()      const -> bool;
  auto contains_possible_extrapoint(Game const& game) const -> bool;
  auto count(Card::TColor color) const -> unsigned;

  auto startcard()        const -> HandCard;
  auto isstartcard()      const -> bool;
  auto isempty()          const -> bool;
  auto isfull()           const -> bool;
  auto islastcard()       const -> bool;
  auto issecondlastcard() const -> bool;

  void move_in_trickpile();


  auto startplayer()       const -> Player const&;
  auto lastplayer()        const -> Player const&;
  auto secondlastplayer()  const -> Player const&;
  auto remaining_players() const -> vector<std::reference_wrapper<Player const>>;
  auto remaining_following_players() const -> vector<std::reference_wrapper<Player const>>;
  auto startplayerno()     const -> unsigned;
  void set_startplayer(Player const& player);
  void set_startplayer(unsigned startplayerno);
  auto actcardno()           const -> unsigned;
  auto remainingcardno()     const -> unsigned;
  auto actplayer()           const -> Player const&;
  auto actplayerno()         const -> unsigned;
  auto nextplayer()          const -> Player const&;
  auto winnerplayerno()      const -> unsigned;
  auto winnerplayer()        const -> Player const&;
  auto winnerteam()          const -> Team;
  auto winnerplayer_before() const -> Player const&;

  auto points() const -> Points;

  auto isvalid(HandCard card)               const -> bool;
  auto isvalid(Card card, Hand const& hand) const -> bool;

  auto winnercard()        const -> HandCard;
  auto winnercardno()      const -> unsigned;
  auto winnercard_before() const -> HandCard;

  auto jabs_cards_before(unsigned cardno)      const -> bool;
  auto jabs_cards_before(Player const& player) const -> bool;

  auto islast() const -> bool;


  auto isjabbed(HandCard card)               const -> bool;
  auto isjabbed(Card card, Hand const& hand) const -> bool;
  auto less(unsigned lhs, unsigned rhs)      const -> bool; // ToDo: remove

  auto specialpoints()                              const -> Specialpoints;
  auto specialpoints(vector<Team> const& real_team) const -> Specialpoints;

public:
  auto specialpoints_cards()             const -> vector<HandCard>;
  auto is_specialpoints_card(unsigned c) const -> bool;

private:
  Cards cards_; // sorted in the order of playing

  Game const* game_ = nullptr;

  unsigned no_ = UINT_MAX;

  unsigned startplayer_  = UINT_MAX;
  unsigned winnerplayer_ = UINT_MAX;

  bool intrickpile_ = false;
}; // class Trick

auto operator<<(ostream& ostr, Trick const& trick) -> ostream&;
auto gettext(Trick const& trick) -> string;

auto operator==(Trick const& lhs, Trick const& rhs) -> bool;
auto operator!=(Trick const& lhs, Trick const& rhs) -> bool;
