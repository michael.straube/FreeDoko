/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "card.h"
#include <list>

/** a list of hand cards with weighting
 ** the cards are sorted descending according to the weighting
 **
 ** first element of the pair is the weighting, the second the card
 **/
class WeightedCardList : private std::list<pair<int, Card> > {
  using List = std::list<pair<int, Card> >;
  public:
  WeightedCardList() = delete;
  explicit WeightedCardList(Game const& game);
  WeightedCardList(WeightedCardList const& card_list);
  WeightedCardList& operator=(WeightedCardList const& card_list);
  ~WeightedCardList() = default;

  Game const& game() const;

  // clear the list
  void clear();

  // write the list in 'ostr'
  void write(ostream& ostr) const;

  // return the total number of cards
  unsigned cards_no() const;
  using List::empty;

  // add the card
  void add(Card card, int weighting);

  // return the card with the highest weighting
  Card highest() const;
  // return the card with the highest weighting of tcolor
  Card highest(Card::TColor tcolor) const;
  // return the card with the highest weighting not of tcolor
  Card highest_not_of(Card::TColor tcolor) const;
  // return the card with the highest weighting of one of tcolor
  Card highest(Card::TColor tcolor_a,
               Card::TColor tcolor_b) const;
  // return the card with the highest weighting of one of tcolor
  Card highest(Card::TColor tcolor_a,
               Card::TColor tcolor_b,
               Card::TColor tcolor_c) const;
  // return the card with the highest weighting of one of tcolor
  Card highest(Card::TColor tcolor_a,
               Card::TColor tcolor_b,
               Card::TColor tcolor_c,
               Card::TColor tcolor_d) const;
  // return the highest weighting
  int highest_weighting() const;
  // return the highest weighting of the tcolor
  int highest_weighting(Card::TColor tcolor) const;
  // return the highest weighting not of the tcolor
  int highest_weighting_not_of(Card::TColor tcolor) const;
  // return the card with the highest weighting of one of tcolor
  int highest_weighting(Card::TColor tcolor_a,
                        Card::TColor tcolor_b) const;
  // return the card with the highest weighting of one of tcolor
  int highest_weighting(Card::TColor tcolor_a,
                        Card::TColor tcolor_b,
                        Card::TColor tcolor_c) const;
  // return the card with the highest weighting of one of tcolor
  int highest_weighting(Card::TColor tcolor_a,
                        Card::TColor tcolor_b,
                        Card::TColor tcolor_c,
                        Card::TColor tcolor_d) const;


  private:
  // return pointer with the highest weighting
  WeightedCardList::const_iterator highest_ptr() const;
  // return the pointer with the highest weighting of the tcolor
  WeightedCardList::iterator const& highest_ptr(Card::TColor tcolor
                                               ) const;
  // return the pointer with the highest weighting not of the tcolor
  WeightedCardList::iterator const& highest_ptr_not_of(Card::TColor tcolor
                                                       ) const;
  // return the ptr with the highest weighting of one of tcolor
  WeightedCardList::iterator const& highest_ptr(Card::TColor tcolor_a,
                                                Card::TColor tcolor_b
                                               ) const;
  // return the ptr with the highest weighting of one of tcolor
  WeightedCardList::iterator const& highest_ptr(Card::TColor tcolor_a,
                                                Card::TColor tcolor_b,
                                                Card::TColor tcolor_c
                                               ) const;
  // return the ptr with the highest weighting of one of tcolor
  WeightedCardList::iterator const& highest_ptr(Card::TColor tcolor_a,
                                                Card::TColor tcolor_b,
                                                Card::TColor tcolor_c,
                                                Card::TColor tcolor_d
                                               ) const;

  public:

  // return the (highest) weighting of the card
  int weighting(Card card) const;

  // remove the highest card
  void pop_highest();
  // remove the card with the highest weighting of tcolor
  void pop_highest(Card::TColor tcolor);
  // remove the card with the highest weighting not of tcolor
  void pop_highest_not_of(Card::TColor tcolor);
  // remove the given card
  void pop(Card card);

  private:
  // recreate the pointers to the highest card of each tcolor
  void recreate_tcolor_ptrs();
  // recreate the pointer to the highest card of the tcolor
  void recreate_tcolor_ptr(Card::TColor tcolor);

  private:
  // the corresponding game
  Game const* game_;
  // an iterator to the highest card for each tcolor
  vector<WeightedCardList::iterator> tcolor_ptr;
  // the position of the highest card for each tcolor (-1 for none)
  vector<int> tcolor_pos;
}; // class WeightedCardList : private vector<pair<int, Card> >

// write 'card_list' in 'ostr'
ostream& operator<<(ostream& ostr, WeightedCardList const& card_list);
