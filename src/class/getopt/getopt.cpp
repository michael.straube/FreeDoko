/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "getopt.h"

using std::string;
using std::vector;

#include <cstring>

#include <cstdarg>
#include <iostream>

using namespace GetOpt;

// remove the argument 'n'
void remove_argument(int& argc, char* argv[], unsigned n = 1);
// remove 'i' argument characters at position  '[n1][n2]'
// default: 'program -asdf' -> 'program -sdf'
void remove_argument_chars(int& argc, char* argv[], unsigned i = 1,
			   unsigned n1 = 1,  unsigned n2 = 1);

// gets the next option and removes it from the arguments
auto GetOpt::getopt(int& argc, char* argv[], vector<Syntax> const& syntax) -> Option
{
  Option option;

  if (argc == 0) {
    // error -- there must be one argument
    option.error_v = Option::NO_ARGUMENT;
    return option;
  } else if (   argc == 1
	     || (   argc == 2
		 && strncmp(argv[1], "--", 3) == 0 // NOLINT
		)
	    ) {
    // no remaining argument (or only '--')
    option.error_v = Option::NO_OPTION;
    return option;
  } else { // if (argc > 1)
    if ((argv[1][0] != '-') // NOLINT
	|| (argv[1][1] == '\0')) { // NOLINT
      // a sole string
      option.error_v = Option::OK;
      option.name_v = "";
      option.type_v = Syntax::BSTRING;
      option.value_string_v = argv[1]; // NOLINT

      remove_argument(argc, argv);

      return option;
    } // if (argv[1][0] != '-')
    else if (strncmp(argv[1], "--", 3) == 0) { // NOLINT
      // a sole string on the second position
      option.error_v = Option::OK;
      option.name_v = "";
      option.type_v = Syntax::BSTRING;
      option.value_string_v = argv[2]; // NOLINT

      remove_argument(argc, argv, 2);

      return option;
    } // if (strncmp(argv[1], "--", 3) == 0)
    else if (strncmp(argv[1], "--", 2) == 0) { // NOLINT
      // long option
      option.value_string_v = argv[1]; // NOLINT
      option.error_v = Option::UNKNOWN_OPTION;
      for (auto const& s : syntax) {
	// search the option
	if (strncmp(argv[1] + 2, s.name.c_str(), // NOLINT
		    s.name.length()) == 0) {

	  option.type_v = s.type;
	  option.name_v = s.name;

	  if (option.type() == Syntax::BOOL) {
	    if (argv[1][2 + s.name.size()] == '\0') { // NOLINT
	      option.error_v = Option::OK;
	      option.value_v.b = true; // NOLINT
	      remove_argument(argc, argv);

	      return option;
	    } // if (argument = '--name'

	  } else { // if !(option.type() == Syntax::BOOL)

	    if (argv[1][2 + s.name.size()] == '\0') { // NOLINT
	      // only the name of the option is in this argument,
	      // the value (if any) is in the next
	      remove_argument(argc, argv);

	      // test, whether there is a remaining argument
	      if (argc == 1) {
		option.error_v = Option::NO_ARGUMENT;

		return option;

	      } else { // if !(argc == 1)
		// the value of the option is now the argument number 1
		(option.value_string_v += " ") += argv[1]; // NOLINT
		option.value_set(argv[1]); // NOLINT
		if (option.error() == Option::OK)
		  remove_argument(argc, argv);

		return option;

	      } // if !(argc == 1)
	    } // if (value in next argument)
	    else if (argv[1][2 + s.name.size()] == '=') { // NOLINT

	      option.value_set(argv[1] + 2 + s.name.length() + 1); // NOLINT
	      if (option.error() == Option::OK)
		remove_argument(argc, argv);

	      return option;

	    } // if (argument = '--name=value')

	  } // if !(option.type() == Syntax::BOOL)

	} // if (strncmp(argv[1] + 2, syntax, ) == 0)
      } // for (s : syntax)

    } else { // if !(long option)

      // long option
      option.value_string_v = argv[1]; // NOLINT
      option.error_v = Option::UNKNOWN_OPTION;

      // short option
      // ***DK use Iterator
      for (auto const& s : syntax) {
	// search the option
	if (argv[1][1] == s.short_name) { // NOLINT

	  option.type_v = s.type;
	  option.name_v = s.name;

	  if (option.type() == Syntax::BOOL) {
	    // BOOL
	    option.error_v = Option::OK;
	    option.value_v.b = true; // NOLINT
	    remove_argument_chars(argc, argv);
	    if (argv[1][1] == '\0') // argument is empty // NOLINT
	      remove_argument(argc, argv);

	    return option;

	  } else { // if !(option.type() == Syntax::BOOL)

	    if (argv[1][2] == '\0') { // NOLINT
	      // the value is in the next argument
	      remove_argument(argc, argv);
	      (option.value_string_v += " ") += argv[1]; // NOLINT
	      option.value_set(argv[1]); // NOLINT
	      if (option.error() == Option::OK)
		remove_argument(argc, argv);

	      return option;

	    } else { // if !(argv[1][2] == '\0')

	      // the value is in this argument
	      if ((option.type() == Syntax::CHAR)
		  && !(argv[1][2] == '=')) { // NOLINT
		// CHAR
		option.error_v = Option::OK;
		option.value_v.c = argv[1][2]; // NOLINT
		remove_argument_chars(argc, argv, 2);
		if (argv[1][1] == '\0') // argument is empty // NOLINT
		  remove_argument(argc, argv);

		return option;

	      } else { // if !(option.type() == Syntax::BOOL)
		// an '=' is ignored
		option.value_set(argv[1] + 2 + // NOLINT
				 (argv[1][2] == '=' ? 1 : 0)); // NOLINT
		if (option.error() == Option::OK)
		  remove_argument(argc, argv);

		return option;

	      } // if !(option.type() == Syntax::BOOL)

	    } // if (value in this argument)

	  } // if !(option.type() == Syntax::BOOL)
	} // if (argv[1][1] == s.short_name)
      } // for (s : syntax)

    } // if (short option)

  } // if !(argc > 1)

  return option;
}


auto GetOpt::getopt(std::istream& istr, vector<Syntax> const& syntax) -> Option
{
  if (!istr.good())
    return {};

  string line;
  do {
    std::getline(istr, line);
    if (!istr.good())
      return {};

    while (!line.empty() && isspace(line.front()))
      line.erase(0, 1);

    while (!line.empty() && isspace(line.back()))
      line.pop_back();

  } while (line.empty() || line.front() == '#');

  if (line.front() != '-') {
    // a sole string
    Option option;
    option.error_v = Option::OK;
    option.name_v  = "";
    option.type_v  = Syntax::BSTRING;
    option.value_string_v = line; // NOLINT
    return option;
  }

  if (line.size() < 3)
    return {};

  if (line.substr(0, 2) != "--") {
    return {};
  }

  Option option;
  auto const p = line.find(' ');
  option.error_v = Option::UNKNOWN_OPTION;
  option.name_v = line.substr(2, p - 2);
  auto const value = line.substr(p + 1);

  for (auto const& s : syntax) {
    if (s.name == option.name_v) {
      option.error_v = Option::OK;
      option.type_v  = s.type;
      switch (s.type) {
      case Syntax::BOOL:
        option.value_v.b = true;
        break;
      case Syntax::INT:
        if (value.empty()) {
          option.error_v = Option::NO_ARGUMENT;
        } else {
          try {
            option.value_v.i = stoi(value);
          } catch (...) {
            option.error_v = Option::FALSE_ARGUMENT;
          }
        }
        break;
      case Syntax::UNSIGNED:
        if (value.empty()) {
          option.error_v = Option::NO_ARGUMENT;
        } else {
          try {
            option.value_v.u = stoul(value);
          } catch (...) {
            option.error_v = Option::FALSE_ARGUMENT;
          }
        }
        break;
      case Syntax::DOUBLE:
        if (value.empty()) {
          option.error_v = Option::NO_ARGUMENT;
        } else {
          try {
            option.value_v.d = stof(value);
          } catch (...) {
            option.error_v = Option::FALSE_ARGUMENT;
          }
        }
        break;
      case Syntax::CHAR:
        if (value.empty()) {
          option.error_v = Option::NO_ARGUMENT;
        } else if (value.size() > 1) {
          option.error_v = Option::FALSE_ARGUMENT;
        } else {
          option.value_v.c = value[0];
        }
        break;
      case Syntax::BSTRING:
        if (value.empty()) {
          option.error_v = Option::NO_ARGUMENT;
        } else {
          option.value_string_v = value;
        }
        break;
      }
      return option;
    }
  }

  return option;
}


// Description:	remove the 'n'th argument from argc and argv
void remove_argument(int& argc, char* argv[], unsigned const n)
{
  if (n >= unsigned(argc))
    return; // nothing to do (error)

  for (unsigned i = n; i < unsigned(argc) - 1; i++)
    argv[i] = argv[i + 1]; // NOLINT

  argc -= 1;
}

// remove 'i' argument characters at position  '[n1][n2]'
// default: 'program -asdf' -> 'program -sdf'
//		i - the number of characters to remove
//		n1 - the argument number of the character(s)
//		n2 - the position in the argument of the character(s)
void remove_argument_chars(int& argc, char* argv[], unsigned const i,
                           unsigned const n1,  unsigned const n2)
{
  if (n1 >= unsigned(argc))
    return; // nothing to do (error)

  auto const n = strlen(argv[n1]);
  if (n2 + i >= n) { // NOLINT
    // the last argument
    argv[n1][n2] = '\0'; // NOLINT
    return;
  }

  //strcpy(argv[n1] + n2, argv[n1] + n2 + i); // NOLINT
  memmove(argv[n1] + n2 + i, argv[n1] + n2, n - n2 - i);
}
