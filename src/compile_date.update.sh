#!/bin/sh

COMPILE_DATE=\"`date +"%F"`\"

if [ -f compile_date.txt ]; then
  echo "${COMPILE_DATE}" | diff -q - compile_date.txt >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "${COMPILE_DATE}" > compile_date.txt
  fi
else
  echo "${COMPILE_DATE}" > compile_date.txt
fi

