/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "debug.h"

#include <ctime>
#include "utils.h"

#include "ui/ui.h"

Debug debug; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
// Filter
// table: Zeichnen des Tisches
// gameplay: Spielverlauf mit ausgeben
// heuristics

// initialize the pointer to the debug function
DebugFunction debug_function = nullptr; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)


void report_error(string const& message)
{
#ifndef WITH_UI_INTERFACE
  throw message;
#endif
  if (::ui) {
    static bool recursion = false;
    if (recursion) {
      cerr << message << std::flush;
    } else {
      recursion = true;
      auto const backtrace = backtrace_string(2);
      ::ui->error(message, backtrace);
      recursion = false;
    }
  } else {
    cerr << message << std::flush;
  }
#ifdef ASSERTION_GENERATES_SEGFAULT
  SEGFAULT;
#endif
  std::exit(EXIT_FAILURE);
}

Debug::Debug() :
  ostr_(&cerr)
{
  this->add("");
  this->add("codeline");
}

Debug::~Debug() = default;

auto Debug::ostr() -> ostream&
{ return *this->ostr_; }

void Debug::set_ostr(ostream& ostr)
{
  this->ostr_ = &ostr;
}

void Debug::set_file(string const file)
{
  if (   file.empty()
      || file == "-"
      || file == "cerr")
    this->ostr_ = &cerr;
  else
    this->ostr_ = new ofstream(file);
}

void Debug::add(string const filter)
{
  this->filter_.insert(filter);
}

void Debug::remove(string const filter)
{
  this->filter_.erase(filter);
}

auto Debug::filter(string const filter) -> bool
{
  return this->filter_.count(filter);
}

auto Debug::operator()(string const filter) const -> bool
{
  return (filter.empty() || this->filter_.count(filter));
}

auto Debug::ostr(string const filter) -> ostream&
{
  if (filter.empty() || this->filter_.count(filter)) {
    if (this->ostr_)
      return *this->ostr_;
    else
      return cerr;
  } else {
    return ::null_ostr;
  }
}

auto Debug::ostr(string const filter, string const file, long const line, string const function_name) -> ostream&
{
  auto& ostr = this->ostr(filter);
  if (&ostr == &::null_ostr)
    return ostr;
  if (this->filter("time"))
    ostr << Time::current() << ' ';
  if (this->filter("clock")) {
    auto const c = std::clock();
    ostr << Time(c / CLOCKS_PER_SEC, (c / (CLOCKS_PER_SEC / 1000) % 1000)) << ' ';
  }
  if (this->filter("codeline"))
    ostr << file << '#' << line << ' ';
  if (this->filter("function"))
    ostr << function_name << "() ";
  return ostr;
}
