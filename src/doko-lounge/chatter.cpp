/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "chatter.h"
#include "spieltisch.h"
#include "client.h"

#include "random_text.h"
#include "datum.h"

#include "../card/sorted_hand.h"
#include "../player/team_information.h"
#include "../player/cards_information.h"
#include "../player/cards_information.of_player.h"
#include "../game/gameplay.h"
#include "../game/gameplay_actions.h"
#include "../utils/file.h"

#include <fstream>
#include <filesystem>

namespace {
auto ist_mensch(string const& name) -> bool;
} // namespace

namespace DokoLounge {

Chatter::Chatter(Spieltisch& spieltisch_, string spielername) :
  spieltisch(&spieltisch_)
{
  if (!spielername.empty() && spielername[0] == '#')
    spielername.erase(0, 1);
  if (!spielername.empty() && spielername.substr(0, 9) == "FreeDoko#")
    spielername.erase(0, 9);
  for (auto const& datei: {spielername + ".txt", "_Standard.txt"s}) {
    if (std::filesystem::is_regular_file(File::executable_directory/"ChatText"/datei)) {
      lade_reaktionen(File::executable_directory/"ChatText"/datei, spielername);
      break;
    }
  }
}


void Chatter::lade_reaktionen(Path const& datei, string const& spielername)
{
  auto istr = ifstream(datei);
  string zeile;
  while (istr.good() && !istr.eof()) {
    std::getline(istr, zeile);
    if (!istr.good() || istr.eof())
      break;
    if (zeile.empty())
      continue;
    if (zeile[0] == '#')
      continue;
    if (zeile.substr(0, 9) == "!include ") {
      lade_reaktionen(File::executable_directory/"ChatText"/zeile.substr(9), spielername);
      continue;
    }
    reaktionen.push_back(Reaktion(zeile, istr));
  }
}


auto Chatter::kenne_kennwort(string const& kennwort) const -> bool
{
  for (auto const& r : reaktionen)
    if (r.kennwort_match(kennwort))
      return true;
  return false;
}


void Chatter::sage(string const& kennwort)
{
  if (chat_keys.count("still"))
    return ; // Nichts sagen

  auto text1 = text(kennwort);
  auto text2 = text("+" + kennwort);
  if (text2.empty()) {
    spieltisch->socket->sage(text1);
  } else if (!text1.empty()) {
    spieltisch->socket->sage(text1 + "\n" + text2);
  }
}


void Chatter::sage(string const& kennwort, string const& spieler)
{
  if (chat_keys.count("still"))
    return ; // Nichts sagen

  auto text1 = text(kennwort, spieler);
  auto text2 = text("+" + kennwort, spieler);
  if (text2.empty()) {
    spieltisch->socket->sage(text1);
  } else if (!text1.empty()) {
    spieltisch->socket->sage(text1 + "\n" + text2);
  }
}


auto Chatter::text(string const& kennwort) const -> string
{
  RandomText random_text;
  for (auto const& r : reaktionen) {
    r.ergaenze(random_text, kennwort);
  }
  if (kennwort ==  "Hallo" || kennwort ==  "Hallo zusammen") {
    for (auto const& kennwort : kennworte_ergaenzung()) {
      for (auto const& r : reaktionen) {
        r.ergaenze(random_text, kennwort);
      }
    }
  }
  if (kennwort == "+Hallo" || kennwort == "+Hallo zusammen") {
    for (auto const& kennwort : kennworte_ergaenzung()) {
      for (auto const& r : reaktionen) {
        r.ergaenze(random_text, "+" + kennwort);
      }
    }
  }
  auto const t = random_text.get();
  if (t.size() >= 3 && t.front() == '<' && t.back() == '>')
    return text(t);
  return t;
}


auto Chatter::text(string const& kennwort, string const& spieler) const -> string
{
  RandomText random_text;
  for (auto const& r : reaktionen) {
    r.ergaenze(random_text,            kennwort, spieler);
  }
  if (kennwort ==  "Hallo" || kennwort ==  "Hallo zusammen") {
    for (auto const& kennwort : kennworte_ergaenzung()) {
      for (auto const& r : reaktionen) {
        r.ergaenze(random_text, kennwort, spieler);
      }
    }
  }
  if (kennwort == "+Hallo" || kennwort == "+Hallo zusammen") {
    for (auto const& kennwort : kennworte_ergaenzung()) {
      for (auto const& r : reaktionen) {
        r.ergaenze(random_text, "+" + kennwort, spieler);
      }
    }
  }
  auto const t = random_text.get();
  if (t.size() >= 3 && t.front() == '<' && t.back() == '>')
    return text(t, spieler);
  return t;
}


auto Chatter::kennworte_ergaenzung() const -> vector<string>
{
  // Bei "Hallo" noch Text ergänzen
  vector<string> kennworte;
  kennworte.push_back(heute_string());
  kennworte.push_back(heute_string().substr(0, 6)); // Datum ohne Jahr
  if (ist_weihnachtszeit(heute()))
    kennworte.push_back("Weihnachtszeit");
  if (ist_advent_1(heute()))
    kennworte.push_back("1. Advent");
  if (ist_advent_2(heute()))
    kennworte.push_back("2. Advent");
  if (ist_advent_3(heute()))
    kennworte.push_back("3. Advent");
  if (ist_advent_4(heute()))
    kennworte.push_back("4. Advent");
  if (ist_nikolaus(morgen()))
    kennworte.push_back("Nikolaus - 1");
  if (ist_nikolaus(heute()))
    kennworte.push_back("Nikolaus");
  if (ist_heiligabend(morgen()))
    kennworte.push_back("Heiligabend - 1");
  if (ist_heiligabend(heute()))
    kennworte.push_back("Heiligabend");
  if (ist_weihnachten(heute()))
    kennworte.push_back("Weihnachten");
  if (ist_silvester(heute()))
    kennworte.push_back("Silvester");
  if (ist_neujahr(heute()))
    kennworte.push_back("Neujahr");
  kennworte.push_back(to_string(to_wochentag(heute())));

  return kennworte;
}

void Chatter::sage_eintrag(string const& eintrag, string const& spieler)
{
  if (eintrag.empty()) {
    sage_eintrag("Hallo", spieler);
    return ;
  }
  if (eintrag == "heute") {
    sage_eintrag(heute_string(), spieler);
    return ;
  }
  if (eintrag == "gestern") {
    sage_eintrag(gestern_string(), spieler);
    return ;
  }
  if (eintrag == "morgen") {
    sage_eintrag(morgen_string(), spieler);
    return ;
  }
  string text;
  RandomText random_text;
  for (auto const& r : reaktionen) {
    r.ergaenze_immer(random_text, eintrag, spieler);
    r.ergaenze_immer(random_text, "+" + eintrag, spieler);
  }
  text = random_text.all_text();

  if (eintrag == "Hallo" || eintrag == "Hallo zusammen") {
    RandomText random_text;
    for (auto const& kennwort : kennworte_ergaenzung()) {
      for (auto const& r : reaktionen) {
        r.ergaenze_immer(random_text, kennwort, spieler);
        r.ergaenze_immer(random_text, "+" + kennwort, spieler);
      }
    }
    text += random_text.all_text();
  }
  spieltisch->socket->sage(text);
}


void Chatter::sage_zufaellig(vector<string> const& text)
{
  spieltisch->socket->sage_zufaellig(text);
}


void Chatter::sage_zufaellig(vector<string> const& text1, vector<string> const& text2)
{
  spieltisch->socket->sage_zufaellig(text1, text2);
}


void Chatter::sage_zufaellig(vector<string> const& text1, vector<string> const& text2, vector<string> const& text3)
{
  spieltisch->socket->sage_zufaellig(text1, text2, text3);
}


void Chatter::reagiere_auf_chat(string const& name, string const& text)
{
  if (!ist_mensch(name))
    return ;

  auto const spieler = (spieltisch->spieler.substr(0, 9) == "FreeDoko#" ? spieltisch->spieler.substr(9) // Die # nicht beibehalten
                        : spieltisch->spieler.front() == '#' ? spieltisch->spieler.substr(1) // Die # entfernen
                        : spieltisch->spieler);
  // ToDo: Wortanfang, Wortende
  auto const spielerx = "(#?" + spieler.text + "|FreeDoko#" + spieler.text + "|Pinguin|FreeDoko|Doki" + ")";

  // Der Name des FreeDoko ist irgendwo im Text
  if (!regex_matchi(text, "^(|.*[^a-zA-Z])" + spielerx + "([^a-zA-Z].*|)$"))
    return;

  if (text == "#" + spieler.text + " log Reaktionen") {
    for (auto const& r : reaktionen)
      cout << r << '\n';
  } else if (regex_matchi(text, spielerx + " log Reaktion .*")) {
    auto const p = text.find(" log Reaktion ");
    auto kennwort = text.substr(p + sizeof(" log Reaktion ") - 1);
    if (kennwort == "heute")
      kennwort = heute_string();
    if (kennwort == "morgen")
      kennwort = morgen_string();
    if (kennwort == "gestern")
      kennwort = gestern_string();
    for (auto const& r : reaktionen) {
      if (r.kennwort_match(kennwort) || r.kennwort_match("+" + kennwort))
        cout << r << '\n';
    }
  } else if (regex_matchi(text, spielerx + " Eintrag .*")) {
    auto const p = text.find(" Eintrag ");
    sage_eintrag(text.substr(p + sizeof(" Eintrag ") - 1), name);
  } else {
    auto const kennwort = regex_replacei(text, spielerx, "%s");
    if (!kenne_kennwort(kennwort)) {
      auto ostr = ofstream(File::executable_directory / "log/unbekannter_Chat.txt", std::ios::app);
      ostr << name << ": " << text << '\n';
      ostr.close();
    }

    sage(kennwort, name);
  }
}


void Chatter::reagiere_auf_spielerliste(vector<string> const& namen)
{
  auto namen_neu_anz = 0;
  string mensch;
  for (auto const& n : namen) {
    if (ist_mensch(n) && !spieler_begruest.count(n)) {
      namen_neu_anz += 1;
      mensch = n;
      spieler_begruest.insert(mensch);
    }
  }
  if (!chat_keys.count("Hallo"))
    return ;
  if (namen_neu_anz == 0)
    return ;
  if (namen_neu_anz == 1) {
    sage("Hallo", mensch);
    return ;
  }
  sage("Hallo zusammen");
}


void Chatter::spiel(Player const& ai)
{
  string text = "Spiel\n";
  auto const& game = ai.game();
  for (auto const& action : game.gameplay().actions()) {
    switch (action->type()) {
    case GameplayActions::Type::reservation: {
      auto const& a = static_cast<GameplayActions::Reservation const&>(*action);
      text += game.player(a.player).name() + ": " + _(a.reservation.game_type) + "\n";
      break;
    }
    case GameplayActions::Type::card_played: {
      auto const& a = static_cast<GameplayActions::CardPlayed const&>(*action);
      text += game.player(a.player).name() + ": " + _(a.card) + "\n";
      break;
    }
    case GameplayActions::Type::announcement: {
      auto const& a = static_cast<GameplayActions::Announcement const&>(*action);
      text += game.player(a.player).name() + ": " + _(a.announcement) + "\n";
      break;
    }
    case GameplayActions::Type::swines: {
      auto const& a = static_cast<GameplayActions::Swines const&>(*action);
      text += game.player(a.player).name() + ": Schweine\n";
      break;
    }
    case GameplayActions::Type::hyperswines: {
      auto const& a = static_cast<GameplayActions::Hyperswines const&>(*action);
      text += game.player(a.player).name() + ": Hyperschweine\n";
      break;
    }
    case GameplayActions::Type::marriage: {
      auto const& a = static_cast<GameplayActions::Marriage const&>(*action);
      text += game.players().soloplayer().name() + " heiratet " + game.player(a.player).name() + "\n";
      break;
    }
    case GameplayActions::Type::trick_open: {
      text += "--\n";
      break;
    }
    default:
      break;
    }
  }
  spieltisch->socket->sage(text);
}


void Chatter::blatt(Player const& ai)
{
  string text = "Blatt\n";
  auto const& hand = ai.hand();
  auto const type = ai.game().type();
  auto const symbol = [](Card::Color const color) -> string {
    switch (color) {
    case Card::club:
      return "<span style=\"color:black\">♣";
    case Card::spade:
      return "<span style=\"color:black\">♠";
    case Card::heart:
      return "<span style=\"color:red\">♥";
    case Card::diamond:
      return "<span style=\"color:red\">♦";
    default:
      return "";
    }
    return "";
  };
  auto const append_multible = [](string& text, string const text_append, size_t const count) {
    for (size_t i = 0; i < count; ++i)
      text += text_append;
  };
  switch (type) {
  case GameType::normal:
  case GameType::redistribute:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade: {
    auto const trumpcolor = ai.game().cards().trumpcolor();
    auto const color_text = [&](Card::Color const color) -> string {
      string text;
      append_multible(text, "A", hand.count(color, Card::ace));
      if (color != Card::heart || !ai.game().rule(Rule::Type::dullen))
        append_multible(text, "Z", hand.count(color, Card::ten));
      append_multible(text, "K", hand.count(color, Card::king));
      append_multible(text, "9", hand.count(color, Card::nine));
      return text;
    };
    auto const value_text = [&](Card::Value const value) -> string {
      string text;
      text += "<span style=\"color:black\">";
      append_multible(text, "♣", hand.count(Card::club,    value));
      append_multible(text, "♠", hand.count(Card::spade,   value));
      text += "</span>";
      text += "<span style=\"color:red\">";
      append_multible(text, "♥", hand.count(Card::heart,   value));
      append_multible(text, "♦", hand.count(Card::diamond, value));
      text += "</span>";
      return text;
    };
    if (hand.hastrump()) {
      {
        string text2;
        if (hand.has_hyperswines())
          append_multible(text2, "9", hand.count(trumpcolor, Card::nine));
        if (hand.has_swines())
          append_multible(text2, "A", hand.count(trumpcolor, Card::ace));
        if (!text2.empty())
          text += symbol(trumpcolor) + text2 + "</span> ";
      }
      if (ai.game().rule(Rule::Type::dullen) && hand.contains(Card::dulle)) {
        text += symbol(Card::heart);
        append_multible(text, "Z", hand.count(Card::dulle));
        text += "</span> ";
      }
      if (hand.contains(Card::queen))
        text += value_text(Card::queen) + "D ";
      if (hand.contains(Card::jack))
        text += value_text(Card::jack) + "B ";
      {
        string text2;
        if (!hand.has_swines())
          append_multible(text2, "A", hand.count(trumpcolor, Card::ace));
        if (trumpcolor != Card::heart || !ai.game().rule(Rule::Type::dullen))
          append_multible(text2, "Z", hand.count(trumpcolor, Card::ten));
        append_multible(text2, "K", hand.count(trumpcolor, Card::king));
        if (!hand.has_hyperswines())
          append_multible(text2, "9", hand.count(trumpcolor, Card::nine));
        if (!text2.empty())
          text += symbol(trumpcolor) + text2 + "</span>";
      }
      text += "\n";
    }
    if (hand.contains(Card::club)) {
      text += symbol(Card::club) + color_text(Card::club) + "</span>\n";
    }
    if (hand.contains(Card::spade)) {
      text += symbol(Card::spade) + color_text(Card::spade) + "</span>\n";
    }
    if (hand.contains(Card::heart)) {
      text += symbol(Card::heart) + color_text(Card::heart) + "</span>\n";
    }
    if (hand.contains(Card::diamond)) {
      text += symbol(Card::diamond) + color_text(Card::diamond) + "</span>\n";
    }
    break;
  }
  case GameType::solo_meatless: {
    auto const color_text = [&](Card::Color const color) -> string {
      string text;
      append_multible(text, "A", hand.count(color, Card::ace));
      append_multible(text, "Z", hand.count(color, Card::ten));
      append_multible(text, "K", hand.count(color, Card::king));
      append_multible(text, "D", hand.count(color, Card::queen));
      append_multible(text, "B", hand.count(color, Card::jack));
      append_multible(text, "9", hand.count(color, Card::nine));
      return text;
    };
    if (hand.contains(Card::club)) {
      text += symbol(Card::club) + color_text(Card::club) + "</span>\n";
    }
    if (hand.contains(Card::spade)) {
      text += symbol(Card::spade) + color_text(Card::spade) + "</span>\n";
    }
    if (hand.contains(Card::heart)) {
      text += symbol(Card::heart) + color_text(Card::heart) + "</span>\n";
    }
    if (hand.contains(Card::diamond)) {
      text += symbol(Card::diamond) + color_text(Card::diamond) + "</span>\n";
    }
    break;
  }
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king: {
    auto const trumpvalue = (  type == GameType::solo_jack  ? Card::jack
                             : type == GameType::solo_queen ? Card::queen
                             : type == GameType::solo_king  ? Card::king
                             : Card::nocardvalue);
    auto const color_text = [&](Card::Color const color) -> string {
      string text;
      append_multible(text, "A", hand.count(color, Card::ace));
      append_multible(text, "Z", hand.count(color, Card::ten));
      if (trumpvalue != Card::king)
        append_multible(text, "K", hand.count(color, Card::king));
      if (trumpvalue != Card::queen)
        append_multible(text, "D", hand.count(color, Card::queen));
      if (trumpvalue != Card::jack)
        append_multible(text, "B", hand.count(color, Card::jack));
      append_multible(text, "9", hand.count(color, Card::nine));
      return text;
    };
    auto const v = (  type == GameType::solo_jack  ? "B"
                    : type == GameType::solo_queen ? "D"
                    : type == GameType::solo_king  ? "K"
                    : "*");
    if (hand.contains(trumpvalue)) {
      text += "<span style=\"color:black\">";
      append_multible(text, "♣", hand.count(Card::club,    trumpvalue));
      append_multible(text, "♠", hand.count(Card::spade,   trumpvalue));
      text += "</span>";
      text += "<span style=\"color:red\">";
      append_multible(text, "♥", hand.count(Card::heart,   trumpvalue));
      append_multible(text, "♦", hand.count(Card::diamond, trumpvalue));
      text += "</span>";
      text += v;
      text += "\n";
    }
    if (hand.contains(Card::club)) {
      text += symbol(Card::club) + color_text(Card::club) + "</span>\n";
    }
    if (hand.contains(Card::spade)) {
      text += symbol(Card::spade) + color_text(Card::spade) + "</span>\n";
    }
    if (hand.contains(Card::heart)) {
      text += symbol(Card::heart) + color_text(Card::heart) + "</span>\n";
    }
    if (hand.contains(Card::diamond)) {
      text += symbol(Card::diamond) + color_text(Card::diamond) + "</span>\n";
    }

    break;
  }
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
    // gibt es hier nicht
    break;
  }

  spieltisch->socket->sage(text);
}


void Chatter::teaminfo(Player const& ai)
{
  string text = "Teaminfo\n";
  auto const& team_information = ai.team_information();
  for (auto const& player : ai.game().players()) {
    text += player.name() + ": " + _(team_information.team(player)) + " (" + std::to_string(team_information.team_value(player)) + ")\n";
  }
  spieltisch->socket->sage(text);
}


void Chatter::karteninfo(Player const& ai)
{
  string text = "Karteninfo\n";
  auto const& cards_information = ai.cards_information();
  for (auto const& player : ai.game().players()) {
    if (player == ai)
      continue;
    text += player.name() + "\n";
    auto unsorted_hand = cards_information.estimated_hand(player);
    auto const hand = SortedHand(unsorted_hand);
    for (unsigned i = 0; i < hand.cardsnumber(); ++i)
      text += "- " + _(hand.card(i)) + " (" + std::to_string(cards_information.of_player(player).weighting(hand.card(i))) + ")\n";
    text += "\n";
  }
  spieltisch->socket->sage(text);
}


} // namespace WebSockets


namespace {
auto ist_mensch(string const& name) -> bool
{
  return !(   name.substr(0, 8) == "FreeDoko"
           || name.substr(0, 6) == "Dummy "
           || name.front() == '#'
           || name == "fd1"
           || name == "fd2"
           || name == "fd3"
           || name == "fd4"
           || name == "frei"
           || name == "empty"
           || name == ""
          );
}

} // namespace

#endif
