/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "client.h"
#include "element.h"
#include "../utils/random.h"
#include "../utils/string.h"

namespace DokoLounge::WebSockets {

Client::Client(string const login, Passwort const passwort, Tischnummer const tischnummer_) :
  Connection(login, passwort),
  tischnummer(tischnummer_)
{ }


Client::Client(DokoLounge::WebSockets::Context const context,
               URI const uri,
               string const login, Passwort const passwort, Tischnummer const tischnummer_) :
  Connection(context, uri, login, passwort),
  tischnummer(tischnummer_)
{ }


Client::~Client() = default;


auto Client::parse_input() -> bool
{
  // Alles bis zu einem <<CleanTisch>> kann ignoriert werden
  if (auto const p = read_buffer_.rfind("<<CleanTisch>>"); p != string::npos) {
    read_buffer_.erase(0, p);
  }
  return Connection::parse_input();
}


auto Client::parse(Element const& element) -> bool
{
  // Rückgabe: true,  wenn die Verbindung bestehen bleiben soll
  //           false, wenn die Verbindung geschlossen werden soll
  auto const& tag     = element.tag;
  auto const& content = element.content;
  if (tag.empty())
    return true;

  static vector<string> const tag_ignorieren = {
    "alert",
    "spenden",
    "FreundeLobby",
    "FeindeLobby",
    "SpielerInDerLounge",
    "SpielerHatNeuesIcon",
    "LobbyChat",
    "alert2",
    "alleMehrspielertischeUpdate",
    "deineEinstellungen",
    "alleGluckspilze",
    "RegelnFuerLobby",
    "alleMehrspielertische",
    "SpielerInDerLobby",
    "SpielerKommtOderGeht",
    "TischspielerUpdadeFuerLobby",
    "Gluckspilz",
    "Tagessieger",
    "ListenTisch",
    "ListenSpielCount",
    "zuschauer",
    "AlleZuschauer",
    "ZuschauerPlus",
    "ZuschauerMinus",
    "profitisch",
    "ChatEingabeLeeren",
    "CloseArmutFenster",
    "HideArmutsfenster",
    "KartenAufTisch",
    "NeuePunkte",
    "SticheProSpieler",
    "BeutePunkte",
  };
  if (container_algorithm::contains(tag_ignorieren, tag)) {
    if (!login_is_special()) {
      if (debug_in_ostr) {
        *debug_in_ostr << "<<" << tag << ">>wird ignoriert<</" << tag << ">>" << endl;
          //*debug_in_ostr << "<<" << tag << ">>" << content << "<</" << tag << ">>" << endl;
      }
    }
    return true;
  }

  if (!(   tag == "knecht"
       )) {
    if (::debug("in")) {
      cout << "<<" << element.tag << ">>\n";
    }
    if (!login_is_special()) {
      if (debug_in_ostr) {
        if (tag.empty())
          *debug_in_ostr << content << endl;
        else if (!(tag == "tischchat" && content.substr(0, 8) == "<<name>>"))
          *debug_in_ostr << "<<" << tag << ">>" << content << "<</" << tag << ">>" << endl;
      }
    }
  }

  DEBUG("parse") << "> " << element << '\n';

  try {
    if (tag == "verbunden") {
      send("anmelden", (  Element("name", login)
                        + Element("passwort", passwort)
                        + Element("WebClientInfo", "web-version egal")));
    } else if (tag == "falscheversion") {
      write_log("falscheversion empfangen");
      return false;
    } else if (tag == "offline") {
      write_log("offline empfangen");
      write_log_current_time();
      DEBUG("offline") << "Offline empfangen\n";
      throw Offline();
      return false;
    } else if (tag == "TischVerlassenOk") {
      write_log("TischVerlassenOk empfangen, gehe offline");
      write_log_current_time();
      DEBUG("offline") << "Offline: TischVerlassenOK empfangen\n";
      throw Offline();
      return false;
    } else if (tag == "AnmeldungOk") {
      ;
    } else if (tag == "anmeldemess") {
      cerr << login << ": Fehler bei der Anmeldung: " << content << '\n';
    } else if (tag == "deinName") {
      login = content;
      sende_tisch_beitreten(tischnummer);
    } else if (tag == "Lebenszeichen") {
      cout << login << ": Lebenszeichen\n";
      if (content.empty())
        send("LobbyChat", Element("Name", login) + "Lebenszeichen");
      else
        send("LobbyChat", Element("Name", login) + "/f " + content + " Lebenszeichen");
    } else if (tag.substr(0, 6) == "letzte") {
    } else if (tag == "DeinTisch"    && !tisch) {
      tisch_beitreten(stoi(content));
    } else if (tag == "TischErlaubt" && !tisch) {
      tisch_beitreten(stoi(content));
    } else if (tag == "DuBistZuschauer") {
      write_log("Bin Zuschauer, gehe offline");
      write_log_current_time();
      DEBUG("offline") << "Offline: bin Zuschauer\n";
      throw Offline();
      return false;
    } else if (tag == "KI-Schwierigkeit") {
      tisch->setze_ki_schwierigkeit(content);
    } else if (tisch) {
      if (tag == "tischchat") {
        tisch->chat(content);
        if (pruefe_auf_offline_aus_chat(content)) {
          write_log("offline laut chat");
          throw Offline();
          return false;
        }
      } else if (tag == "alleSpielerAmTisch") {
        tisch->setze_spieler(content);
      } else if (tag == "regelinfo1") {
        tisch->setze_regeln(content);
      } else if (tag == "sonderregeln") {
        // ToDo
      } else if (tag == "CleanTisch") {
        tisch->clean();
      } else if (tag == "knecht") {
        tisch->setze_knecht(content);
      } else if (tag == "Ansage") {
        tisch->setze_ansage(content);
      } else if (tag == "Karten") {
        tisch->setze_karten(content);
      } else if (tag == "trumpf") {
        tisch->setze_trumpf(content);
      } else if (tag == "Meldungsfenster") {
        if (!tisch->meldung_gesendet) {
          auto const vorbehalt = tisch->waehle_vorbehalt();
          if (vorbehalt.empty()) {
            DEBUG("refresh") << "Konnte keinen Vorbehalt wählen\n";
            sende_refresh();
          } else {
            sende_meldung(vorbehalt);
          }
        }
      } else if (tag == "Partei") {
        // Wird erst nach dem Vorbehalt gesendet
        tisch->setze_team(content);
      } else if (tag == "SoloInfo") {
        tisch->setze_solo(content);
      } else if (tag == "Hochzeit") {
        tisch->setze_hochzeit(Spielername(content));
      } else if (tag == "Hochzeitspartner") {
        tisch->setze_hochzeitspartner(Spielername(content));
      } else if (tag == "ArmutKartenWaehlen") {
        auto const karten = tisch->waehle_armut_karten();
        if (!karten.empty())
          send("ArmutsKarten", Element("ArmutsName", login) + Element("ArmutsK", karten));
      } else if (tag == "ArmutMitnehmFrage") {
        auto const annahme = tisch->waehle_armut_annahme(content);
        if (!annahme.empty())
          send("ArmutMitnehmen", login + ":" + annahme);
      } else if (tag == "KartenVonArmut") {
        auto const karten = tisch->wechsel_armut_karten(content);
        if (!karten.empty())
          send("MitnehmerKartenBack", Element("MitnehmerName", login) + Element("MitnehmerK", karten));
      } else if (tag == "ArmutIcons") {
        tisch->setze_armut_team(content);
      } else if (tag == "ArmutTauschBeendet") {
      } else if (tag == "Sau") {
        tisch->setze_sau(Spielername(content));
      } else if (tag == "Supersau") {
        tisch->setze_supersau(Spielername(content));
      } else if (tag == "AnsageCount") {
        tisch->setze_spiel_laeuft();
        if (content != "0")
          optional_sende_ansage();
      } else if (tag == "SpielerIstDran") {
        tisch->spieler_ist_dran(Spielername(content));
      } else if (tag == "dubistdran") {
        if (tisch->blatt.empty()) {
          DEBUG("refresh") << "Ich bin dran, kenne aber noch keine Karten\n";
          sende_refresh();
          return true;
        }
        optional_sende_ansage();
        auto const karte = tisch->waehle_karte();
        if (karte == 0) {
          DEBUG("refresh") << "Ich bin dran, habe aber keine Karte ausgewählt.\n";
          sende_refresh();
        } else {
          sende_karte(karte);
        }
      } else if (tag == "KartenLegen") {
        // Die Information der Karten wird von <<kartenauftisch2>> genommen
        tisch->karten_legen(content);
      } else if (tag == "kartenauftisch2") {
        tisch->setze_karten_auf_tisch(content);
      } else if (tag == "StichVomTisch") {
        tisch->setze_stich_vom_tisch(content);
      } else if (tag == "ShowKreuzDame") {
        tisch->setze_ist_re(content);
      } else if (tag == "ShowKreuzDame1") {
        tisch->setze_ist_re(content);
      } else if (tag == "EndabrechnungsInfo") {
        tisch->endabrechnung(content);
      } else if (tag == "KI-Schwierigkeit") {
        tisch->setze_ki_schwierigkeit(content);
      } else if (tag == "log") {
        cout << login << ": Log: " << content << '\n';
      } else if (tag == "debug") {
        tisch->debug(content);
      } else if (tag == "fehlerbericht") {
        tisch->erstelle_fehlerbericht(content);
      } else {
        DEBUG("unknown tag") << login << ": Unbekannte Nachricht: " << element.to_string() << '\n';
      }
    }
  } catch (Offline const&) {
    tischnummer = UINT_MAX;
    throw;
    return true;
  } catch (string const& fehlertext) {
    cerr << __FILE__ << '#' << __LINE__ << "  " << login << "  Fehler: " << fehlertext << '\n';
    cerr << element << '\n';
    write_log("Fehler: " + fehlertext);
    tisch->in_invalid_game = true;
    tischnummer = UINT_MAX;
    return true;
  } catch (...) {
    tisch->in_invalid_game = true;
    tischnummer = UINT_MAX;
    return true;
  }
  return true;
}


void Client::sende_refresh()
{
  send("Refresh", login);
}


void Client::sende_tisch_beitreten(unsigned tischno)
{
  send("TischBeitreten", (  Element("name", login)
                          + Element("tisch", tischno)));
}


void Client::sende_meldung(string const meldung)
{
  send("Meldung", tisch->spieler + ":" + meldung);
  tisch->meldung_gesendet = true;
}


void Client::sende_karte(Kartennummer const karte)
{
  if (karte == 0) {
    return ;
  }
  send("kartenlegen", (  Element("tisch", tisch->nummer)
                       + Element("name",  login)
                       + Element("karte", karte)));
}


void Client::sende_ansage(string const ansage)
{
  send("Ansage", Element("Spieler", login) + Element("Ans", ansage));
}


void Client::optional_sende_ansage()
{
  auto const ansage = tisch->waehle_ansage();
  if (ansage.empty())
    return ;
  sende_ansage(ansage);
}


void Client::sage(string text)
{
  if (text.empty())
    return ;
  while (!text.empty() && isspace(text.back()))
    text.pop_back();
  send(Element("tischchat", Element("name", login) + String::replaced_all(text, "\n", "<br>")));
  if (chat_keys.count("cout"))
    cout << login + ": " << text << '\n';
}


void Client::sage_zufaellig(vector<string> const& text)
{
  sage(random_element(text));
}


void Client::sage_zufaellig(vector<string> const& text1, vector<string> const& text2)
{
  sage(random_element(text1) + random_element(text2));
}


void Client::sage_zufaellig(vector<string> const& text1, vector<string> const& text2, vector<string> const& text3)
{
  sage(random_element(text1) + random_element(text2) + random_element(text3));
}


void Client::fluestere(string name, string text)
{
  send(Element("tischchat", Element("name", login) + "/f " + name + " " + String::replaced_all(text, "\n", "<br>")));
}


void Client::tisch_beitreten(Tischnummer const tischno)
{
  if (tisch && tisch->nummer == tischno)
    return ;
  if (tischno == 0) {
    // Tisch 0 heißt Lobby
    write_log("Tisch 0 erhalten, gehe offline");
    write_log_current_time();
    DEBUG("offline") << "Offline: Tisch 0 erhalten\n";
    throw Offline();
  }
  tisch = make_unique<Spieltisch>(*this, Spielername(login), tischno);
}

auto Client::pruefe_auf_offline_aus_chat(string text) -> bool
{
  while (text.substr(0, 2) == "<<") {
    Element::extract_next_element(text);
  }

  return (   text == "offline: " + login
          || text == "offline:" + login
          || text == login + " offline"
          || text == login + " raus"
          || text == "offline: dummies"
          || text == "dummies offline"
          || text == "dummies raus"
         );
}

} // namespace DokoLounge::WebSockets

#endif
