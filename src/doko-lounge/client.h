/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "connection.h"
#include "spieltisch.h"

namespace DokoLounge {

namespace WebSockets {

class Client : public Connection {
public:
Client(string login, Passwort passwort, Tischnummer tischnummer);
Client(DokoLounge::WebSockets::Context context,
       URI uri, string login, Passwort passwort, Tischnummer tischnummer);
virtual ~Client();

auto parse_input()                 -> bool override;
auto parse(Element const& element) -> bool override;

void sende_refresh();
void sende_tisch_beitreten(unsigned tischno);
void sende_meldung(string meldung);
void sende_karte(Kartennummer karte);
void sende_ansage(string ansage);
void optional_sende_ansage();

void sage(string text);
void sage_zufaellig(vector<string> const& text);
void sage_zufaellig(vector<string> const& text1, vector<string> const& text2);
void sage_zufaellig(vector<string> const& text1, vector<string> const& text2, vector<string> const& text3);
void fluestere(string name, string text);

void tisch_beitreten(Tischnummer tischno);
auto pruefe_auf_offline_aus_chat(string text) -> bool;

public:
unsigned tischnummer = 0;

unique_ptr<Spieltisch> tisch;
};
} // namespace WebSockets
} // namespace DokoLounge
#endif
