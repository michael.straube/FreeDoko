/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "connection.h"
#include "element.h"

#include "../options.h"
#include "../utils.h"
#include "../utils/string.h"
#include "../utils/file.h"
#include "../card/card.h"
#include "../party/status.h"
#include "../game/status.h"

#include <chrono>
#include <filesystem>

namespace {
auto create_log_stream(string login) -> unique_ptr<ostream>;
}

namespace DokoLounge::WebSockets {

int connection_callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len)
{
  switch (reason) {
  case LWS_CALLBACK_CLIENT_ESTABLISHED: {
    DEBUG_ASSERTION(static_cast<Connection*>(user) != nullptr,
                    "LWS_CALLBACK_CLIENT_WRITEABLE: Connection is nullptr\n");
    auto& connection = *static_cast<Connection*>(user);
    DEBUG_ASSERTION(connection.wsi_ == wsi,
                    "connection_callback: wsi entspricht nicht dem der Connection");
    cout << connection.login << " ist verbunden.\n";
    lws_callback_on_writable(wsi);
    break;
  }

  case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: {
    if (in) {
      cerr << "client connection error: " << string(static_cast<char*>(in), len) << '\n';
    } else {
      cerr << "client connection error\n";
    }
    DEBUG_ASSERTION(static_cast<Connection*>(user) != nullptr,
                    "LWS_CALLBACK_CLIENT_WRITEABLE: Connection is nullptr\n");
    auto& connection = *static_cast<Connection*>(user);
    connection.wsi_ = nullptr;
    break;
  }

  case LWS_CALLBACK_CLIENT_RECEIVE: {
    DEBUG_ASSERTION(static_cast<Connection*>(user) != nullptr,
                    "LWS_CALLBACK_CLIENT_WRITEABLE: Connection is nullptr\n");
    auto& connection = *static_cast<Connection*>(user);
    try {
      DEBUG_ASSERTION(static_cast<Connection*>(user) != nullptr,
                      "LWS_CALLBACK_CLIENT_WRITEABLE: Connection is nullptr\n");
      if (in && len > 0) {
        connection.receive(string(static_cast<char*>(in), len));
        return 0;
      }
    } catch (Offline const&) {
      cout << "Beende Verbindung\n";
      return 1;
    } catch (string const& text) {
      cerr << "Fehler: " << text << '\n';
      cerr << "Beende Connection " << connection.login << '\n';
      connection.write_log("Fehler: " + text);
      connection.write_log_current_time();
      return 1;
    } catch (std::exception const& e) {
      cerr << "Exception: " << e.what() << '\n';
      cerr << "Beende Connection " << connection.login << '\n';
      connection.write_log("Exception: "s + e.what());
      connection.write_log_current_time();
      return 1;
    } catch (Card const& card) {
      cerr << "Card-Fehler: " << card << '\n';
      cerr << "Beende Connection " << connection.login << '\n';
      connection.write_log("Card-Fehler: " + _(card));
      connection.write_log_current_time();
      return 1;
    } catch (PartyStatus const status) {
      cerr << "Spielstatus-Fehler: " << status << '\n';
      cerr << "Beende Connection " << connection.login << '\n';
      connection.write_log("Spielstatus-Fehler: " + _(status));
      connection.write_log_current_time();
      return 1;
    } catch (GameStatus const status) {
      cerr << "Spielstatus-Fehler: " << status << '\n';
      cerr << "Beende Connection " << connection.login << '\n';
      connection.write_log("Spielstatus-Fehler: " + _(status));
      connection.write_log_current_time();
      return 1;
    } catch (...) {
      auto e = std::current_exception();
      cerr << "Unbekannter Fehler: " << (e ? e.__cxa_exception_type()->name() : "null");
      cerr << "Beende Connection " << connection.login << '\n';
      connection.write_log("Unbekannter Fehler: "s + (e ? e.__cxa_exception_type()->name() : "null"));
      connection.write_log_current_time();
      return 1;
    }
    break;
  }

  case LWS_CALLBACK_CLIENT_WRITEABLE: {
    lws_callback_on_writable(wsi);
    DEBUG_ASSERTION(static_cast<Connection*>(user) != nullptr,
                    "LWS_CALLBACK_CLIENT_WRITEABLE: Connection is nullptr\n");
    auto& connection = *static_cast<Connection*>(user);
    auto& write_buffer = connection.write_buffer_;
    if (write_buffer.empty())
      break;
    auto const n = write_buffer.size();
    unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + n + LWS_SEND_BUFFER_POST_PADDING];
    unsigned char *p = &buf[LWS_SEND_BUFFER_PRE_PADDING];
    memcpy(p, write_buffer.c_str(), n);
    auto& wsi = connection.wsi_;
    if (wsi)
      lws_write(wsi, p, n, LWS_WRITE_TEXT);
    write_buffer.clear();
    break;
  }

    //case LWS_CALLBACK_CLIENT_CLOSED:
  case LWS_CALLBACK_CLOSED: {
    auto& connection = *static_cast<Connection*>(user);
    cout << connection.login << " wird abgemeldet.\n";
    connection.wsi_ = nullptr;
    connection.write_log("Callback Closed");
    break;
  }

  default:
    break;
  }

  return 0;
}


Connection::Connection() = default;


Connection::Connection(string const login_, Passwort const passwort_) :
  login(login_), passwort(passwort_)
{
  if (!(   login == "file"
        || login == "stdin"
        || login.find('/') != string::npos
        || login.find('\\') != string::npos
       )) {
    debug_in_ostr = create_log_stream(login);
    write_log_current_time();
    write_log("Version: " + version_string());
    //in_traffic_ostr = make_unique<ofstream>("eingehende_Daten_" + String::replaced_all(login, " ", "_") + ".dokolounge");
  }
}


Connection::Connection(DokoLounge::WebSockets::Context const context, URI uri,
                       string const login_, Passwort const passwort_) :
  login(login_), passwort(passwort_)
{
  debug_in_ostr = create_log_stream(login);
  write_log_current_time();
  write_log("Version: " + version_string());
  //in_traffic_ostr = make_unique<ofstream>("eingehende_Daten_" + String::replaced_all(login, " ", "_") + ".dokolounge");

  struct lws_client_connect_info ccinfo = {};
  ccinfo.context = context;

  if (uri.substr(0, 6) == "wss://") {
    ccinfo.ssl_connection = 1;
    ccinfo.port = 443;
    uri.erase(0, 6);
  } else if (uri.substr(0, 5) == "ws://") {
    ccinfo.ssl_connection = 0;
    ccinfo.port = 80;
    uri.erase(0, 5);
  }
  auto const pos_colon = uri.find(':');
  auto const pos_slash = uri.find('/');
  string address;
  if (pos_colon != string::npos) {
    address = uri.substr(0, pos_colon);
    ccinfo.port = stoi(uri.substr(pos_colon + 1, pos_slash));
  } else {
    address = uri.substr(0, pos_slash);
  }
  auto const address_c = new char[address.size() + 1];
  strncpy(address_c, address.c_str(), address.size() + 1);
  ccinfo.address = address_c;

  if (pos_slash != string::npos) {
    auto const pfad = uri.substr(pos_slash);
    if (pfad.empty()) {
      ccinfo.path = "/";
    } else {
      auto const pfad_c = new char[pfad.size() + 1];
      strncpy(pfad_c, pfad.c_str(), pfad.size() + 1);
      ccinfo.path = pfad_c;
    }
  } else {
    ccinfo.path = "/";
  }

  ccinfo.host     = ccinfo.address;
  ccinfo.origin   = lws_canonical_hostname(context);
  ccinfo.protocol = "Doko-Lounge";
  ccinfo.userdata = this;

  cout << "Starte WebSocket-Service auf " << uri << "." << endl;
  wsi_ = lws_client_connect_via_info(&ccinfo);
}


Connection::~Connection()
{
  if (debug_in_ostr) {
    debug_in_ostr->flush();
  }
}


auto Connection::login_is_special() const -> bool
{
  return wsi_ == nullptr;
}


void Connection::receive(string const text)
{
  //if (in_traffic_ostr)
  //*in_traffic_ostr << text << '\n';
  DEBUG("receive") << login << ": > " << text << '\n';
  read_buffer_ += text;
}

auto Connection::parse_input() -> bool
{
  for (auto element = Element::extract_next_element(read_buffer_);
       !element.empty();
       element = Element::extract_next_element(read_buffer_)) {
    DEBUG("parse") << "> " << element << "     -- " << read_buffer_ << '\n';
    if (!parse(element))
      return false;
  }
  return true;
}


void Connection::send(string const& text)
{
  DEBUG("write") << login << ": " << text << '\n';
  write_buffer_ += text;
}


void Connection::send(string const& text, string const& content)
{
  DEBUG("write") << login << ": " << static_cast<string>(Element(text, content)) << '\n';
  write_buffer_ += Element(text, content);
}


void Connection::send(Element const& element)
{
  DEBUG("write") << login << ": " << static_cast<string>(element) << '\n';
  write_buffer_ += element;
}


void Connection::write_log(string const& text)
{
  if (!debug_in_ostr)
    return ;
  *debug_in_ostr << static_cast<string>(Element("log", text)) << '\n';
  debug_in_ostr->flush();
}

void Connection::write_log_current_time()
{
  if (!debug_in_ostr)
    return ;

  *debug_in_ostr << static_cast<string>(Element("Zeit", time_now_string())) << '\n';
  debug_in_ostr->flush();
}

void Connection::clear_write_buffer()
{
  DEBUG("write") << login << ": clear write buffer\n";
  write_buffer_.clear();
}

auto Connection::read_buffer() -> string const&
{
  return read_buffer_;
}

} // namespace DokoLounge::WebSockets

namespace {
auto create_log_stream(string login) -> unique_ptr<ostream>
{
  using namespace std::filesystem;
  auto const now = std::time(nullptr);
  char today[11];
  std::strftime(today, sizeof(today), "%Y-%m-%d", std::localtime(&now));
  auto const dir = File::executable_directory / "log" / today;
  if (!is_directory(dir))
    create_directories(dir);

  String::replace_all(login, " ", "_");
  String::replace_all(login, "FreeDoko", "");
  if (login.front() == '#')
    login.erase(login.begin());
  auto filename = login + ".dokolounge";
  if (!is_regular_file(dir / filename))
    return make_unique<ofstream>(dir / filename);

  for (unsigned n = 1; n < 999; ++n) {
    // Check whether the file does not exists, yet.
    // If the file does already exists,
    // append a number and search the first which does not exists.
    filename = (login
                + "-" + String::to_string(n)
                + ".dokolounge"); // NOLINT performance-inefficient-string-concatenation
    if (!is_regular_file(dir / filename))
      return make_unique<ofstream>(dir / filename);
  }
  return make_unique<ofstream>(dir / filename);
}
}

#endif
