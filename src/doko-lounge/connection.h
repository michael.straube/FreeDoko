/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include <libwebsockets.h>

namespace DokoLounge {

struct Element;

namespace WebSockets {

extern int connection_callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

class Connection {
  friend int connection_callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

public:
  Connection();
  Connection(string login, Passwort passwort);
  Connection(DokoLounge::WebSockets::Context context, URI uri,
             string login, Passwort passwort);
  virtual ~Connection();

  auto login_is_special() const -> bool;

  void receive(string text);
  virtual auto parse_input()                 -> bool;
  virtual auto parse(Element const& element) -> bool = 0;

  void send(string const& text);
  void send(string const& tag, string const& content);
  void send(Element const& element);
  void write_log(string const& text);
  void write_log_current_time();
  template<typename T, typename... Args>
    inline void write_log(string const& text, T const& t, Args... args)
    {
      ostringstream ostr;
      ostr << text << t;
      write_log(ostr.str(), args...);
    }
  void clear_write_buffer();
  auto read_buffer() -> string const&;

protected:
  unique_ptr<ostream> debug_in_ostr;
  //unique_ptr<ostream> in_traffic_ostr;

public:
  string login;
  string const passwort;
  Instance wsi_ = nullptr;
protected:
  string read_buffer_;
  string write_buffer_;
};
} // namespace WebSockets
} // namespace DokoLounge
#endif
