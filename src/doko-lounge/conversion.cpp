/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "conversion.h"

#include "../card/hand.h"
#include "../game/reservation.h"

namespace {
array<Card, 49> const all_cards = {
  Card::unknown,       // 0
  Card::heart_ace,     // 1
  Card::heart_king,    // 2
  Card::heart_queen,   // 3
  Card::heart_jack,    // 4
  Card::heart_ten,     // 5
  Card::heart_nine,    // 6
  Card::diamond_ace,   // 7
  Card::diamond_king,  // 8
  Card::diamond_queen, // 9
  Card::diamond_jack,  // 10
  Card::diamond_ten,   // 11
  Card::diamond_nine,  // 12
  Card::club_ace,      // 13
  Card::club_king,     // 14
  Card::club_queen,    // 15
  Card::club_jack,     // 16
  Card::club_ten,      // 17
  Card::club_nine,     // 18
  Card::spade_ace,     // 19
  Card::spade_king,    // 20
  Card::spade_queen,   // 21
  Card::spade_jack,    // 22
  Card::spade_ten,     // 23
  Card::spade_nine,    // 24
  Card::heart_ace,     // 25
  Card::heart_king,    // 26
  Card::heart_queen,   // 27
  Card::heart_jack,    // 28
  Card::heart_ten,     // 29
  Card::heart_nine,    // 30
  Card::diamond_ace,   // 31
  Card::diamond_king,  // 32
  Card::diamond_queen, // 33
  Card::diamond_jack,  // 34
  Card::diamond_ten,   // 35
  Card::diamond_nine,  // 36
  Card::club_ace,      // 37
  Card::club_king,     // 38
  Card::club_queen,    // 39
  Card::club_jack,     // 40
  Card::club_ten,      // 41
  Card::club_nine,     // 42
  Card::spade_ace,     // 43
  Card::spade_king,    // 44
  Card::spade_queen,   // 45
  Card::spade_jack,    // 46
  Card::spade_ten,     // 47
  Card::spade_nine,    // 48
};
}

namespace DokoLounge {

auto to_game_type(string const& trumpf) -> GameType
{
  if (trumpf == "Karo") {
    return GameType::normal;
  } else if (trumpf == "Armut") {
    return GameType::poverty;
  } else if (trumpf == "Hochzeit") {
    return GameType::marriage;
  } else if (   trumpf == "Fleischlos-Solo"
             || trumpf == "Fleischlos") {
    return GameType::solo_meatless;
  } else if (   trumpf == "Buben-Solo"
             || trumpf == "Bube") {
    return GameType::solo_jack;
  } else if (   trumpf == "Damen-Solo"
             || trumpf == "Dame") {
    return GameType::solo_queen;
  } else if (   trumpf == "Kreuz-Solo"
             || trumpf == "Kreuz") {
    return GameType::solo_club;
  } else if (   trumpf == "Herz-Solo"
             || trumpf == "Herz") {
    return GameType::solo_heart;
  } else if (   trumpf == "Pik-Solo"
             || trumpf == "Pik") {
    return GameType::solo_spade;
  } else if (trumpf == "Karo-Solo") {
    return GameType::solo_diamond;
  } else {
    cerr << "Trumpf „" << trumpf << "“ nicht bekannt.\n";
    SEGFAULT;
  }
  return GameType::normal;
}


auto to_kartennummer(string const& karte) -> Kartennummer
{
  if (karte.size() < 2)
    return 0;
  return std::stoi(karte[0] == '0'
                   ? karte.substr(1, 1)
                   : karte.substr(0, 2));
}


auto to_card(Kartennummer nummer) -> Card
{
  if (!(nummer >= 0 && nummer <= static_cast<int>(all_cards.size() - 1))) {
    cerr << "Karte Nummer " << nummer << " ist gefragt, es gibt aber nur " << all_cards.size() << " Karten\n";
    return {};
  }
  return all_cards[nummer];
}


auto to_card(string const& karte) -> Card
{
  return to_card(to_kartennummer(karte));
}


auto to_cards(string const& karten) -> vector<Card>
{
  vector<Card> cards;
  for (size_t i = 0; i < karten.size(); i += 3) {
    cards.emplace_back(to_card(karten.substr(i, 2)));
  }
  return cards;
}


auto to_blatt(string const& karten, size_t const cardno) -> Blatt
{
  Blatt blatt;
  for (size_t i = 0; i < karten.size(); i += 3) {
    auto const karte = karten[i] == '0' ? karten.substr(i + 1, 1) : karten.substr(i, 2);
    blatt.push_back(std::stoi(karte));
  }
  while (blatt.size() > cardno && blatt.back() == 0) {
    blatt.pop_back();
  }
  return blatt;
}


auto to_hand(Player const& player, Blatt const& blatt) -> Hand
{
  auto hand = Hand(player);
  for (auto const karte : blatt) {
    hand.add(to_card(karte));
  }
  return hand;
}


auto to_hand(Blatt const& blatt) -> Hand
{
  auto hand = Hand();
  for (auto const karte : blatt) {
    hand.add(to_card(karte));
  }
  return hand;
}


auto to_dokolounge_name(Reservation const& reservation) -> string
{
  switch (reservation.game_type) {
  case GameType::normal:
    return "Gesund";
  case GameType::poverty:
    return "Armut";
  case GameType::marriage:
    return "Hochzeit";
  case GameType::solo_meatless:
    return "Fleischlos-Solo";
  case GameType::solo_jack:
    return "Buben-Solo";
  case GameType::solo_queen:
    return "Damen-Solo";
  case GameType::solo_club:
    return "Kreuz-Solo";
  case GameType::solo_heart:
    return "Herz-Solo";
  case GameType::solo_spade:
    return "Pik-Solo";
  case GameType::solo_diamond:
    return "Karo-Solo";
  case GameType::thrown_nines:
  case GameType::thrown_kings:
    return "schmeissen";
  default:
    // Nicht unterstützt
    return "Gesund";
  }
  return {};
}


auto to_dokolounge_name(Announcement const announcement) -> string
{
  switch (announcement) {
  case Announcement::noannouncement:
    return {};
  case Announcement::no120:
    return "keine 120";
  case Announcement::no90:
    return "keine 90";
  case Announcement::no60:
    return "keine 60";
  case Announcement::no30:
    return "keine 30";
  case Announcement::no0:
    return "SCHWARZ";
  default:
    // Gibt es nicht
    return {};
  }
  return {};
}

} // namespace DokoLounge

#endif
