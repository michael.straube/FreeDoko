/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "../basetypes/announcement.h"
#include "../basetypes/game_type.h"
class Card;
class Hand;
class Player;
struct Reservation;

namespace DokoLounge {

auto to_game_type(string const& trumpf)                  -> GameType;
auto to_kartennummer(string const& karte)                -> Kartennummer;
auto to_card(Kartennummer nummer)                        -> Card;
auto to_card(string const& karte)                        -> Card;
auto to_cards(string const& karten)                      -> vector<Card>;
auto to_blatt(string const& karten, size_t cardno)       -> Blatt;
auto to_hand(Player const& player, Blatt const& blatt)   -> Hand;
auto to_hand(Blatt const& blatt)                         -> Hand;
auto to_dokolounge_name(Reservation const& reservation)  -> string;
auto to_dokolounge_name(Announcement announcement)       -> string;
auto to_dokolounge_name(Announcement announcement)       -> string;
} // namespace DokoLounge

#endif
