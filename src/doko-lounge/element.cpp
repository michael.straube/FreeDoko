/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "element.h"

namespace DokoLounge {

auto Element::split_into_elements(string text) -> vector<Element>
{
  if (text.empty())
    return {};
  vector<Element> elements;
  for (auto element = extract_next_element(text); !element.tag.empty(); element = extract_next_element(text))
    elements.push_back(element);
  return elements;
}

auto Element::extract_next_element(string& text) -> Element
{
  if (text.empty())
    return {};
  if (text.substr(0, 2) != "<<") {
    auto p = text.find("<<");
    if (p == string::npos)
      return {};
    auto const content = text.substr(0, p);
    text.erase(0, p);
    return Element("", content);
  }
  auto p = text.find(">>");
  if (p == string::npos)
    return {};
  auto const tag = string(text.substr(2, p - 2));
  p += 2;
  auto const q = text.find("<</" + tag + ">>");
  if (q == string::npos)
    return {};
  auto const content = text.substr(p, q - p);
  text.erase(0, q + 5 + tag.size());
  return Element(tag, content);
}


Element::Element() = default;


Element::Element(string_view text)
{
  if (text.substr(0, 2) != "<<") {
    content = text;
    return;
  }
  auto p = text.find(">>");
  if (p == string::npos)
    return ;
  tag = text.substr(2, p);
  if (text.substr(text.size() - 5 - tag.size()) != "<</" + tag + ">>") {
    tag.clear();
    return ;
  }
  content = text.substr(4 + tag.size(), text.size() - 5 - tag.size()); 
}


Element::Element(string_view tag_, string_view content_) :
  tag(tag_), content(content_)
{ }


Element::Element(string_view tag_, string_lc const& content_) :
  tag(tag_), content(content_.text)
{ }


Element::Element(string_view tag_, int content_) :
  tag(tag_), content(std::to_string(content_))
{ }


auto Element::to_string() const -> string
{
  if (tag.empty())
    return content;
  return "<<" + tag + ">>" + content + "<</" + tag + ">>";
}


Element::operator string() const
{
  return to_string();
}


auto Element::empty() const -> bool
{
  return tag.empty() && content.empty();
}


auto operator+(Element const& lhs, Element const& rhs) -> string
{
  return lhs.to_string() + rhs.to_string();
}


auto operator+(string const& lhs, Element const& rhs) -> string
{
  return lhs + rhs.to_string();
}


auto operator+(Element const& lhs, string const& rhs) -> string
{
  return lhs.to_string() + rhs;
}


auto operator<<(ostream& ostr, Element const& element) -> ostream&
{
  if (!element.tag.empty())
    ostr << element.tag << ": ";
  ostr << element.content;
  return ostr;
}

} // namespace WebSockets
#endif
