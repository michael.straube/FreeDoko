/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

namespace DokoLounge {

struct Element;
using Elements = vector<Element>;

struct Element {
  using Tag     = string_lc;
  using Content = string;

static auto extract_next_element(string& text) -> Element;
static auto split_into_elements(string text)   -> Elements;

Element();
Element(string_view text);
Element(string_view tag, string_view content);
Element(string_view tag, string_lc const& content);
Element(string_view tag, int content);

auto to_string() const -> string;
operator string() const;

auto empty() const -> bool;

Tag     tag;
Content content;
};

auto operator+(Element const& lhs, Element const& rhs) -> string;
auto operator+(string  const& lhs, Element const& rhs) -> string;
auto operator+(Element const& lhs, string  const& rhs) -> string;

auto operator<<(ostream& ostr, Element const& element) -> ostream&;

} // namespace DokoLounge

#endif
