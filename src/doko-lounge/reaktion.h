/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

class RandomText;
#include "datum.h"

namespace DokoLounge {

class Reaktion {
public:
  using Wahrscheinlichkeit = int;

  struct Eintrag {
    friend auto operator<<(ostream& ostr, Reaktion::Eintrag const& eintrag) -> ostream&;

    explicit Eintrag(string zeile);
    explicit Eintrag(vector<vector<string>> text);

    void ergaenze(RandomText& random_text, string const& spieler) const;
    void ergaenze_immer(RandomText& random_text, string const& spieler) const;

    auto text() const -> string;

    Zeit zeit_von = {0, 0};
    Zeit zeit_bis = {24, 0};

  private:
    vector<vector<string>> text_;
  };
public:
  Reaktion(string const& kennwort, istream& istr);
  Reaktion() = delete;
  Reaktion(Reaktion const&);
  auto operator=(Reaktion const&) -> Reaktion& = delete;

  void write(ostream& ostr) const;

  auto kennwort_match(string const& kennwort) const -> bool;

  void ergaenze(RandomText& text, string const& kennwort, string const& spieler = "") const;
  void ergaenze_immer(RandomText& text, string const& kennwort, string const& spieler = "") const;

private:
  Wahrscheinlichkeit wahrscheinlichkeit_ = 100; 
  string kennwort_;
  vector<string> empfaenger_;
  vector<Eintrag> eintrag_;
};

auto operator<<(ostream& ostr, Reaktion::Eintrag const& eintrag) -> ostream&;
auto operator<<(ostream& ostr, Reaktion const& reaktion) -> ostream&;

} // namespace DokoLounge

#endif
