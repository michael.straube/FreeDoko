/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "game.h"
#include "gameplay.h"
#include "gameplay_actions/announcement.h"
#include "../party/rule.h"
#include "../ui/ui.h"

GameAnnouncements::GameAnnouncements(Game& game) :
  game_(&game)
{ }

GameAnnouncements::GameAnnouncements(GameAnnouncements const& announcements,
                                     Game& game) :
  game_(&game),
  announcements_(announcements.announcements_)
{ }

auto GameAnnouncements::last(Team const team) const -> AnnouncementWithTrickno
{
  auto const& game = *game_;
  DEBUG_ASSERTION(   (team == Team::re)
                  || (team == Team::contra)
                  || game.marriage().is_undetermined(),
                  "GameAnnouncements::last(team)\n"
                  "  invalid team = " << team);

  // In an undetermined marriage the contra team cannot have announced.
  // This case is treated here, because in this case there does not
  // exists a player of the team 'contra' so the assertion later on would fail.
  if (   game.marriage().is_undetermined()
      && (team != Team::re) )
    return {};

  // the player of the team, that has made the highest announcement
  Player const* player = nullptr;
  auto const& rule = game.rule();

  // take the highest announcement
  for (auto const& p : game.players()) {
    if (   rule(Rule::Type::knocking)
        && !is_real(game.teaminfo().get(p)))
      continue;

    if (p.team() == team)
      if (!player
          || (last(p).announcement
              > last(*player).announcement))
        player = &p;
  }

  if (   rule(Rule::Type::knocking)
      && !player)
    return {};

  if (!player) {
    return {};
  }

  return last(*player);
}

auto GameAnnouncements::last(Player const& player) const -> AnnouncementWithTrickno
{
  auto const& announcements = announcements_[player.no()];
  if (announcements.empty())
    return {};
  return announcements.back();
}

auto GameAnnouncements::all(Player const& player) const -> vector<AnnouncementWithTrickno> const&
{
  return announcements_[player.no()];
}

void GameAnnouncements::init()
{
  auto const& game = *game_;
  auto const& rule = game.rule();
  auto const number_of_players = rule(Rule::Type::number_of_players_in_game);
  announcements_
    = vector<vector<AnnouncementWithTrickno>>(number_of_players);
}

void GameAnnouncements::remove_all_but_re_contra()
{
  auto const& game = *game_;
  for (unsigned p = 0; p < game.players().size(); p++) {
    auto const& player = game.players().player(p);
    auto& announcements = announcements_[p];
    if (announcements.size() <= 1)
      continue;
    // the player has made an announcement
    announcements.erase(announcements.begin() + 2,
                        announcements.end());
    announcements[1].announcement = Announcement::no120;
    if (!game.isvirtual()) {
      ::ui->announcement_made(Announcement::no120, player);
      signal_announcement_made(Announcement::no120, player);
    }
  } // for (p < playerno())
}

void GameAnnouncements::request_from_players()
{
  auto& game = *game_;
  bool ask_again = true;

  while (ask_again) {
    ask_again = false;

    // ask for announcements
    for (auto& p : game.players()) {
      if (valid_announcement(p) != Announcement::noannouncement) {
        auto const announcement = p.announcement_request();
        if (is_valid(announcement, p)) {
          make_announcement(announcement, p);
          if (last(p) == announcement)
            ask_again = true;
        } // if (announcement_valid(announcement, p))
      } // if (valid_announcement(p))
    } // for (p < playerno())
  } // while (again)
}

void GameAnnouncements::evaluate_trick(Trick const& trick)
{
  auto& game = *game_;
  auto const& rule = game.rule();
  auto const& winnerplayer = trick.winnerplayer();

  // test whether the player has to make an announcement
  if (   (game.type() == GameType::normal)
      && (game.tricks().current_no() == 1)
      && (rule(Rule::Type::announcement_first_trick_thirty_points))
      && (trick.points() >= 30)
      && (last(winnerplayer.team())
          != Announcement::no0)
      && (    !rule(Rule::Type::announcement_first_trick_thirty_points_only_first)
          || (   (last(Team::re) == Announcement::noannouncement)
              && (last(Team::contra) == Announcement::noannouncement) )
         )
      && (   !rule(Rule::Type::announcement_first_trick_thirty_points_only_re_contra)
          || (last(winnerplayer.team())
              == Announcement::noannouncement) )
      && (   !is_marriage(game.type())
          || rule(Rule::Type::announcement_first_trick_thirty_points_in_marriage))
     ) {
    make_announcement(next(last(winnerplayer.team())), winnerplayer);
  } // if (player must make an announcement)
}

auto GameAnnouncements::valid_announcement(Player const& player) const -> Announcement
{
  if (!is_real(player.team()))
    return Announcement::noannouncement;

  if (is_valid(player.next_announcement(), player))
    return player.next_announcement();

  return Announcement::noannouncement;
}

void GameAnnouncements::make_announcement(Announcement const announcement,
                                          Player const& player)
{
  if (!is_valid(announcement, player))
    return ;

  auto& game = *game_;

  // set the announcement
  announcements_[player.no()].push_back(AnnouncementWithTrickno(announcement, game.tricks().current_no()));
  // update the team info
  if (!game.rule(Rule::Type::knocking))
    game.teaminfo().set(player, player.team());

  // tell all players, that the announcement is made
  for (auto& p : game.players())
    p.announcement_made(announcement, player);

  game.gameplay_->add(GameplayActions::Announcement(player.no(), announcement));
  if (!game.isvirtual()) {
    ::ui->announcement_made(announcement, player);
    signal_announcement_made(announcement, player);
    ::ui->gameplay_action(GameplayActions::Announcement(player.no(),
                                                        announcement));
    game.signal_gameplay_action(GameplayActions::Announcement(player.no(),
                                                              announcement));
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
auto GameAnnouncements::is_valid(Announcement const announcement,
                                 Player const& player) const -> bool
{
  auto const& game = *game_;
  auto const& tricks = game.tricks();
  auto const& rule = game.rule();
  // test, whether status is in a game
  if (!game_->in_running_game())
    return false;
  if (tricks.empty())
    return false;
  if (!is_real(player.team()))
    return false;

  // as long as the marriage is not determined, no announcement is valid
  if (   game.marriage().selector() != MarriageSelector::team_set
      && game.marriage().selector() != MarriageSelector::silent)
    return false;

  unsigned const marriage_deferral
    = (  (   game.type() == GameType::marriage
          || game.type() == GameType::marriage_solo)
       ? game.marriage().determination_trickno()
       : 0);

  // Antwort auf eine Ansage/Absage des Gegners
  if (announcement == Announcement::reply) {
    if (last(player.team()).announcement != Announcement::noannouncement)
      return false;
    if (last(opposite(player.team())).announcement == Announcement::noannouncement)
      return false;
    bool const valid = (((rule(Rule::Type::announcement_till_full_trick)
                          ? tricks.real_remaining_no()
                          : player.hand().cardsnumber())
                         + marriage_deferral)
                        + 1 >= rule.remaining_cards(last(opposite(player.team())).announcement));
    return valid;
  }

  auto const last_announcement
    = ( (   rule(Rule::Type::knocking)
         && is_with_unknown_teams(game.type()))
       ? last(player).announcement
       : last(player.team()).announcement);

  // the announcement must be a better one
  if (last_announcement >= announcement) {
    return false;
  }

  if (   announcement == Announcement::no120
      && game.type() == GameType::marriage
      && tricks.current_no() == game.marriage().determination_trickno()
      && tricks.current().isfull()) {
    return true;
  }

  // whether a normal announcement is valid
  bool const valid = (((rule(Rule::Type::announcement_till_full_trick)
                        ? tricks.real_remaining_no()
                        : player.hand().cardsnumber())
                       + marriage_deferral)
                      >= rule.remaining_cards(announcement));

  if (!valid)
    return false;

  // verify that all announcements before are valid
  if (   !rule(Rule::Type::announcement_limit_only_for_current)
      && announcement > Announcement::no120
      && last_announcement != previous(announcement)) {
    return is_valid(previous(announcement), player);
  }

  return valid;
}

auto GameAnnouncements::last_chance_to_announce(Announcement const announcement,
                                                Player const& player) const -> bool
{
  auto const& game = *game_;
  auto const& tricks = game.tricks();
  auto const& rule = game.rule();
  if (!is_valid(announcement, player))
    return false;

  auto const marriage_deferral = (  game.type() == GameType::marriage
                                  ? (game.marriage().determination_trickno()
                                     - 1)
                                  : 0);
  if (   announcement == Announcement::no120
      && game.type() == GameType::marriage
      && tricks.current_no() == game.marriage().determination_trickno()
      && tricks.current().isfull()) {
    return true;
  }

  if (rule(Rule::Type::announcement_till_full_trick)) {
    if (!tricks.current().isfull())
      return false;
    return (tricks.real_remaining_no()
            + marriage_deferral
            == rule.remaining_cards(announcement));
  } else {
    if (game.players().current_player() != player)
      return false;
    if (tricks.current().has_played(player))
      return false;
    return ( player.hand().cardsnumber()
            + marriage_deferral
            == rule.remaining_cards(announcement));
  }
}
