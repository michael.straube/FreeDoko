/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../card/card.h"
class Game;

/**
 ** The cards of a game
 **/
class GameCards {
  friend class Game;
  public:
  explicit GameCards(Game& game);
  GameCards(GameCards const& cards, Game& game);

  GameCards() = delete;
  GameCards(GameCards const&) = delete;
  auto operator=(GameCards const&) = delete;

  void update();
  void update_swines();
  void update_hyperswines();

  auto count()                           const -> unsigned;
  auto count_unique()                    const -> unsigned;
  auto count(Card::TColor tcolor)        const -> unsigned;
  auto count_unique(Card::TColor tcolor) const -> unsigned;
  auto count_trumps()                    const -> unsigned;
  auto count_played_trumps()             const -> unsigned;
  auto count_played()                    const -> unsigned;
  auto count_remaining()                 const -> unsigned;
  auto count_played(Card const& card)    const -> unsigned;
  auto trumpcolor()                      const -> Card::Color;
  auto colors()                          const -> vector<Card::Color>;
  auto cards()                           const -> vector<Card>;
  auto cards(Card::TColor tcolor)        const -> vector<Card> const&;
  auto trumps()                          const -> vector<Card> const&;
  auto sorted_single_cards()             const -> vector<Card>;

  auto less(Card const& lhs, Card const& rhs)          const -> bool;
  auto next_lower_card(Card const& card)               const -> Card;
  auto next_higher_card(Card const& card)              const -> Card;
  auto next_higher_card(Card const& card, unsigned n)  const -> Card;
  auto lower_cards(Card const& card)                   const -> vector<Card>;
  auto higher_cards(Card const& card)                  const -> vector<Card>;
  auto cards_between(Card const& lhs, Card const& rhs) const -> vector<Card>;
  auto lower_trumps(Card const& card)                  const -> vector<Card>;

  auto is_special(HandCard const& card) const -> bool;
  auto fox()        const -> Card;
  auto dulle()      const -> Card;
  auto swine()      const -> Card;
  auto hyperswine() const -> Card;

  private:
  Game* const game_;
  // the cards in the colors (in descending order)
  vector<vector<Card>> cards_;
};

auto trumpcolor(GameType game_type) noexcept -> Card::Color;
