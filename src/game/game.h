/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "status.h"
#include "reservation.h"
#include "players.h"
#include "poverty.h"
#include "marriage.h"
#include "swines.h"
#include "cards.h"
#include "tricks.h"
#include "announcements.h"
#include "teaminfo.h"
#include "../party/rule_types.h"
#include "../player/aiconfig_heuristic.h"
#include "../player/ai/rationale.h"
#include "../card/card.h"
#include "../card/hand_cards.h"
class Party;
class Rule;
class Player;
class Human;
class Trick;
class Hand;

class Gameplay;
#include "gameplay_action.h"

class Game {
  friend class Party;
  friend class GamePlayers;
  friend class GamePoverty;
  friend class GameMarriage;
  friend class GameSwines;
  friend class GameSummary;
  friend class GameAnnouncements;
  friend class GameTeamInfo;
  friend class Ai;
  friend class WVirtualGames;
  friend class WMonteCarlo;
  friend class Gametree;
public:
  using Players       = GamePlayers;
  using Player        = GamePlayer;
  using Poverty       = GamePoverty;
  using Marriage      = GameMarriage;
  using Swines        = GameSwines;
  using Cards         = GameCards;
  using Tricks        = GameTricks;
  using TeamInfo      = GameTeamInfo;
  using Announcements = GameAnnouncements;

  using Status        = GameStatus;

public:
  Game(Party& party, Player& startplayer);
  // constructor (for virtual games)
  //Game(Game const& game, vector<Player*>& players);
  Game(Game const& game, vector<unique_ptr<Player>>& players);
  Game()                           = delete;
  Game(Game const& game)           = delete;
  auto operator=(Game const& game) = delete;
  ~Game();

  auto self_check() const -> bool;
  // resets the game to the beginning of the first trick
  // (that is after the cards distribution and reservation)
  void reset_to_first_trick();

  void write(ostream& ostr)              const;
  void write_hands(ostream& ostr = cout) const;
  auto rest_query()                      const -> string;

  auto status()          const noexcept -> Status;
  void set_status(Status status) noexcept;
  auto in_running_game() const noexcept -> bool;

  auto isvirtual()   const -> bool;
  auto is_finished() const -> bool;

  auto party()                   const          -> Party const&;
  auto rule()                    const noexcept -> Rule const&;
  auto rule(RuleType::Bool type)          const -> bool;
  auto rule(RuleType::Unsigned type)      const -> unsigned;
  auto rule(RuleType::UnsignedExtra type) const -> unsigned;
  auto rule(GameType game_type)           const -> bool;
  auto second_dulle_over_first() const          -> bool;

  auto bock_multiplier()  const -> int;
  auto is_duty_solo()     const noexcept -> bool;

  auto players()          const -> Players const&;
  auto players()                -> Players&;
  auto player(unsigned p) const -> Player const&;
  auto player(unsigned p)       -> Player&;
  auto playerno()         const -> unsigned;

  auto seed()              const noexcept -> Seed;
  void set_seed(Seed seed)       noexcept;

  auto gameplay() const noexcept -> Gameplay const&;

  auto type()                  const noexcept -> GameType;
  void set_type(GameType type)       noexcept;
  void force_set_solo(GameType type, Player const& soloplayer);
  auto is_solo()          const noexcept -> bool;
  auto is_real_solo()     const noexcept -> bool;
  auto set_is_duty_solo()                -> bool;

  void set_startplayer(unsigned startplayer);
  auto startplayer_stays() const -> bool;

  // update the cards (according to the game type)
  void update_cards();


  // players

  auto startplayer() const -> Player const&;

  auto has_made_reservation(Player const& player) const -> bool;
  auto reservation(Player const& player)          const -> Reservation const&;

  // infos of the gameplay

  auto points_of_player(Player const& player)                   const -> unsigned;
  auto points_of_player(unsigned playerno)                      const -> unsigned;
  auto points_of_player(Player const& player, unsigned trickno) const -> unsigned;
  auto points_of_player(unsigned playerno, unsigned trickno)    const -> unsigned;
  auto points_of_team(Team const& team)                         const -> unsigned;
  auto played_points()                                          const -> unsigned;
  auto remaining_points()                                       const -> unsigned;
  auto needed_points_to_win(Team team)                          const -> unsigned;
  auto winnerteam()                                             const -> Team;


  auto cards()         const noexcept -> Cards const&;
  auto cards()               noexcept -> Cards&;
  auto poverty()       const noexcept -> Poverty const&;
  auto poverty()             noexcept -> Poverty&;
  auto marriage()      const noexcept -> Marriage const&;
  auto marriage()            noexcept -> Marriage&;
  auto swines()        const noexcept -> Swines const&;
  auto swines()              noexcept -> Swines&;
  auto tricks()        const noexcept -> Tricks const&;
  auto tricks()              noexcept -> Tricks&;
  auto announcements() const noexcept -> Announcements const&;
  auto announcements()       noexcept -> Announcements&;
  auto teaminfo()      const noexcept -> TeamInfo const&;
  auto teaminfo()            noexcept -> TeamInfo&;


  // team (real team)

  // the team of the player
  auto team(Player const& player) const -> Team;


  // gameplay

  // play the game
  void play();
  // play the game according to the gameplay
  void play(Gameplay const& gameplay);
  void play(GameplayAction const& action);
  void add(GameplayAction const& action); // just add the action but do not play it

  void init();
private:

  void distribute_cards();
  void distribute_cards_by_seed();
  void distribute_cards_manually();
  void distribute_cards_by_bug_report_replay();
public:
  void distribute_cards(Hands hands);
private:
  void fill_up_hands(bool random_seed = false);
  void get_reservations();
  void select_reservation();
protected:
  // the next player has to play a card
  void nextplayer();
public:
  // the player plays a card
  void play_card(Player& player, Card card);
  void play_card(Card card);
  void set_trick_taken();
  void evaluatetrick();
private:
  // finish the game
  void finish();

public:
  void close();

private:
  // print statistics of the seeds
  void print_seed_statistics() const;
  void print_seed_statistics_for_players() const;

private:
  // party, the game belongs to
  Party* party_ = {};
  Status status_ = Status::init;

  unique_ptr<Players> players_;
  Player const* startplayer_ = {};

  unique_ptr<Gameplay> gameplay_;

  Seed seed_ = 0;
  GameType type_ = GameType::normal;
  bool is_duty_solo_ = false;

  Cards cards_;

  Poverty poverty_;
  Marriage marriage_;
  Swines swines_;

  Tricks tricks_;
  Announcements announcements_;

  TeamInfo teaminfo_;

  bool finished_ = false;

public:
  static Signal<void(Game&)>                      signal_open; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
  Signal<void()>                                  signal_cards_distributed;
  Signal<void()>                                  signal_redistribute;
  Signal<void(Player const&)>                     signal_reservation_ask;
  Signal<void(Reservation const&, Player const&)> signal_reservation_got;
  Signal<void()>                                  signal_start;
  Signal<void()>                                  signal_finish;
  Signal<void()>                                  signal_close;

  Signal<void(GameplayAction const&)>             signal_gameplay_action;

  Signal<void(Player const&)>                    signal_get_card;
  Signal<void(HandCard)>                         signal_card_played;

  Signal<void(AiconfigHeuristic, Rationale)>     signal_ai_heuristic_failed;
  Signal<void(Card, unsigned)>                   signal_ai_test_card;
  Signal<void(int)>                              signal_ai_card_weighting;
  Signal<void(Hands const&, int, unsigned, unsigned)> signal_ai_test_hands;
  Signal<void(unsigned)>                         signal_ai_hands_weighting;
  Signal<void(HandCard)>                         signal_virtual_card_played;
  Signal<void()>                                 signal_virtual_trick_full;

  Signal<void()> signal_gametype_changed;
}; // class Game

auto to_string(Game const& game) -> string;
auto operator<<(ostream& ostr, Game const& game) -> ostream&;

auto operator==(Game const& lhs, Game const& rhs) noexcept -> bool;
auto operator!=(Game const& lhs, Game const& rhs) noexcept -> bool;
