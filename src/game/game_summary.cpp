/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game_summary.h"

#include "game.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../card/trick.h"
#include "../utils/string.h"
#include "../class/readconfig/readconfig.h"

GameSummary::GameSummary(Game const& game) :
  rule_(&game.rule()),
  is_replayed_game_(game.party().is_replayed_game()),
  seed_(game.seed()),
  players_number_(game.players().size()),
  party_players_(game.players().size()),
  startplayer_(game.startplayer().no()),
soloplayer_(  game.players().exists_soloplayer()
            ? game.players().soloplayer().no()
            : UINT_MAX),
  game_type_(game.type()),
  duty_solo_(game.is_duty_solo()),
swines_player_(   game.swines().swines_announced()
               ? game.swines().swines_owner().no()
               : UINT_MAX),
hyperswines_player_(  game.swines().hyperswines_announced()
                    ? game.swines().hyperswines_owner().no()
                    : UINT_MAX)
{
  for (auto const& p : game.players())
    party_players_[p.no()] = p.no();
  evaluate(game);
}

GameSummary::GameSummary(Rule const& rule, istream& istr) :
  rule_(&rule)
{
  read(istr);
}

void GameSummary::write(ostream& ostr) const
{
  ostr << "seed = " << Unsigned(seed()) << '\n';
#if 0
  ostr << "players number = " << Unsigned(players_number()) << '\n';
  ostr << "party players =";
  for (auto const& p : party_players_)
    ostr << " " << p;
#endif
  ostr << '\n';
  ostr << "gametype = " << game_type() << '\n';
  ostr << "startplayer number = " << Unsigned(startplayer()) << '\n';
  ostr << "soloplayer number = " << Unsigned(soloplayer()) << '\n';
  ostr << "duty solo = " << is_duty_solo() << '\n';
  ostr << "swines player number = " << Unsigned(swines_player()) << '\n';
  ostr << "hyperswines player number = " << Unsigned(hyperswines_player()) << '\n';
  ostr << "teams =";
  for (auto t = begin(teams_); t != end(teams_); ++t) {
    if (t != begin(teams_))
      ostr << ',';
    ostr << ' ' << *t;
  }
  ostr << '\n';
  ostr << "highest announcement re = " << highest_announcement_re() << '\n';
  ostr << "highest announcement contra = " << highest_announcement_contra() << '\n';
  ostr << "trick points =";
  for (auto p = begin(trick_points_);
       p != end(trick_points_);
       ++p) {
    if (p != begin(trick_points_))
      ostr << ',';
    ostr << ' ' << *p;
  }
  ostr << '\n';
  ostr << "winnerteam = " << winnerteam() << '\n';
  ostr << "points = " << points() << '\n';
  ostr << "special points\n"
    << "{\n"
    << specialpoints()
    << "}\n";
  if (!bock_triggers_.empty()) {
    ostr << "bock triggers\n"
      << "{\n";
    for (auto const& c : bock_triggers_)
      ostr << c << '\n';
    ostr << "}\n";
  } // if (!bock_triggers().empty())
}

void GameSummary::read(istream& istr)
{
  Unsigned seed = UINT_MAX;
  Unsigned players_number = UINT_MAX;
  vector<unsigned> party_players;
  Unsigned startplayer = UINT_MAX;
  Unsigned soloplayer = UINT_MAX;

  GameType game_type = GameType::normal;
  bool duty_solo = false;
  Unsigned swines_player = UINT_MAX;
  Unsigned hyperswines_player = UINT_MAX;
  vector<Team> teams;
  Announcement highest_announcement_re = Announcement::noannouncement;
  Announcement highest_announcement_contra = Announcement::noannouncement;
  vector<unsigned> trick_points;
  Team winnerteam = Team::noteam;
  int points = INT_MAX;
  Specialpoints specialpoints;
  vector<BockTrigger> bock_triggers;

#ifdef WORKAROUND
  // Die Spieleranzahl ist fix 4, daher wird die Anzahl nicht mehr in der Spielzusammenfassung ausgegeben
  players_number = 4;
  if (party_players.empty()) {
    for (unsigned p = 0; p < players_number; ++p)
      party_players.push_back(p);
  }
#endif

  { // read the data
    Config config; // the config read
    int depth = 0; // the depth in the blocks
    do { // while (istr)
      istr >> config;

      if (istr.eof())
        break;

      if (istr.fail())
        break;

      if (config.empty())
        break;

      if (config.value.empty()) {
        if (config.name == "{") {
          depth += 1;
        } else if (config.name == "}") {
          depth -= 1;
          // finished with the game summary
          if (depth == -1)
            istr.putback('}');
          if (depth <= 0)
            break;

        } else if (config.name == "special points") {
          string line;
          line = String::remove_blanks(::read_line(istr));
          if (line != "{")
            throw ReadException("Game summary read: "
                                "special points: expected '{', "
                                " found '" + line + "'");

          while (istr.good()
                 && (istr.peek() != '}')) {
            specialpoints.emplace_back(istr);
            while (   isspace(istr.peek())
                   && istr.good())
              istr.get();
          }

          line = String::remove_blanks(::read_line(istr));
          if (line != "}")
            throw ReadException("Game summary read: "
                                "special points: expected '}', "
                                " found '" + line + "'");
        } else if (config.name == "bock triggers") {
          string line;
          line = String::remove_blanks(::read_line(istr));
          if (line != "{")
            throw ReadException("Game summary read: "
                                "bock triggers: expected '{', "
                                " found '" + line + "'");

          while (istr.good()
                 && (istr.peek() != '}')) {
            line = String::remove_blanks(::read_line(istr));
            bock_triggers.push_back(bock_trigger_from_string(line));
            while (   isspace(istr.peek())
                   && istr.good())
              istr.get();
          }

          line = String::remove_blanks(::read_line(istr));
          if (line != "}")
            throw ReadException("Game summary read: "
                                "bock triggers: expected '}', "
                                " found '" + line + "'");
        } else { // if (config.name == ...)
          throw ReadException("Game summary read: unknown line "
                              "'" + config.name + "'");
        } // if (config.name == ...)
      } else { // if !(config.value.empty())
        istringstream istr(config.value); // cppcheck-suppress shadowArgument
        if (config.name == "seed") {
          istr >> seed;
        } else if (config.name == "players number") {
          istr >> players_number;
          if (party_players.empty()) {
            for (unsigned p = 0; p < players_number; ++p)
              party_players.push_back(p);
          }
        } else if (config.name == "party players") {
          for (auto& p : party_players)
            istr >> p;
        } else if (config.name == "startplayer number") {
          istr >> startplayer;
        } else if (config.name == "soloplayer number") {
          istr >> soloplayer;
        } else if (config.name == "gametype") {
          game_type = game_type_from_string(config.value);
        } else if (config.name == "duty solo") {
          duty_solo = (   (config.value == "true")
                       || (config.value == "yes")
                       || (config.value == "1"));
        } else if (config.name == "swines player number") {
          istr >> swines_player;
        } else if (config.name == "hyperswines player number") {
          istr >> hyperswines_player;
        } else if (config.name == "teams") {
          string::size_type pos_begin = 0;
          while (   pos_begin < config.value.size()
                 && isspace(config.value[pos_begin]))
            ++pos_begin;
          while (pos_begin < config.value.size()) {
            string::size_type const pos_end
              = config.value.find(',', pos_begin);
            auto const team_name = string(config.value,
                                          pos_begin,
                                          ((pos_end == string::npos)
                                           ? string::npos
                                           : (pos_end - pos_begin)));

            teams.push_back(team_from_string(team_name));
            if (pos_end == string::npos)
              break;
            pos_begin = pos_end + 1;
            while (   pos_begin < config.value.size()
                   && isspace(config.value[pos_begin]))
              ++pos_begin;
          } // while (pos_begin < config.value.size())
        } else if (config.name == "highest announcement re") {
          highest_announcement_re = announcement_from_string(config.value);
        } else if (config.name == "highest announcement contra") {
          highest_announcement_contra = announcement_from_string(config.value);
        } else if (config.name == "trick points") {
          string::size_type pos_begin = 0;
          while (   pos_begin < config.value.size()
                 && isspace(config.value[pos_begin]))
            ++pos_begin;
          while (pos_begin < config.value.size()) {
            auto const pos_end = config.value.find(',', pos_begin);
            auto const points_name = string(config.value,
                                            pos_begin,
                                            ((pos_end == string::npos)
                                             ? string::npos
                                             : (pos_end - pos_begin)));

            trick_points.push_back(Unsigned(points_name));
            if (trick_points.back() == UINT_MAX)
              throw ReadException("Game summary read: invalid trickpoints "
                                  "'" + string(config.value,
                                               pos_begin,
                                               pos_end - pos_begin)
                                  + "'");
            if (pos_end == string::npos)
              break;
            pos_begin = pos_end + 1;
            while (   pos_begin < config.value.size()
                   && isspace(config.value[pos_begin]))
              ++pos_begin;
          } // while (pos_begin < config.value.size())
        } else if (config.name == "winnerteam") {
          winnerteam = team_from_string(config.value);
        } else if (config.name == "points") {
          istr >> points;
        } else { // if (config.name == ...)
          throw ReadException("Game summary read: unknown line "
                              "'" + config.name + " = " + config.value + "'");
        } // if (config.name == ...)
        if (!istr.good() && !istr.eof())
          throw ReadException("Game summary read: error loading "
                              "'" + config.name + "'");
      } // if !(config.value.empty())

    } while (istr.good());

    if (!istr.good())
      throw ReadException("Game summary read: unknown error");
  } // read the data

  { // test whether all data was read
    if (seed == UINT_MAX)
      throw ReadException("Game summary read: seed not loaded");
    if (players_number == UINT_MAX)
      throw ReadException("Game summary read: players number not loaded");
    if (startplayer == UINT_MAX)
      throw ReadException("Game summary read: startplayer number not loaded");
    if (teams.size() != players_number)
      throw ReadException("Game summary read: teams not loaded");
    if (trick_points.size() != players_number)
      throw ReadException("Game summary read: trick points not loaded");
    if (points == INT_MAX)
      throw ReadException("Game summary read: points number not loaded");

  } // test whether all data was read

  { // set the data
    // explicit casts of Unsigned because of problems with MinGW g++-3.2
    seed_ = seed;
    players_number_ = players_number;
    party_players_ = party_players;
    startplayer_ = startplayer;
    soloplayer_ = soloplayer;
    game_type_ = game_type;
    duty_solo_ = duty_solo;
    swines_player_ = swines_player;
    hyperswines_player_ = hyperswines_player;
    teams_ = teams;
    highest_announcement_re_ = highest_announcement_re;
    highest_announcement_contra_ = highest_announcement_contra;
    trick_points_ = trick_points;
    winnerteam_ = winnerteam;
    points_ = points;
    specialpoints_ = specialpoints;
    bock_triggers_ = bock_triggers;
  } // set the data
}

auto GameSummary::rule() const -> Rule const&
{
  return *rule_;
}

auto GameSummary::seed() const -> Seed
{
  return seed_;
}

auto GameSummary::players_number() const -> unsigned
{
  return players_number_;
}

auto GameSummary::party_player(unsigned const p) const -> unsigned
{
  DEBUG_ASSERTION(p < party_players_.size(),
                  "GameSummary::party_player(" << p << ")\n"
                  "  There are only " << party_players_.size() << " players");
  return party_players_[p];
}

auto GameSummary::game_player(Player const& player) const -> unsigned
{
  for (unsigned p = 0; p < party_players_.size(); ++p)
    if (party_players_[p] == ::party->players().no(player))
      return p;
  return UINT_MAX;
}

auto GameSummary::has_played(Player const& player) const -> bool
{
  auto const n = ::party->players().no(player);
  auto is_player = [n](auto const p) {
    return p == n;
  };
  return container_algorithm::any_of(party_players_, is_player);
}

auto GameSummary::startplayer() const -> unsigned
{
  return startplayer_;
}

auto GameSummary::soloplayer() const -> unsigned
{
  return soloplayer_;
}

auto GameSummary::game_type() const -> GameType
{
  return game_type_;
}

auto GameSummary::swines_player() const -> unsigned
{
  return swines_player_;
}

auto GameSummary::hyperswines_player() const -> unsigned
{
  return hyperswines_player_;
}

auto GameSummary::highest_announcement_re() const -> Announcement
{
  return highest_announcement_re_;
}

auto GameSummary::highest_announcement_contra() const -> Announcement
{
  return highest_announcement_contra_;
}

auto GameSummary::winnerteam() const -> Team
{
  return winnerteam_;
}

auto GameSummary::is_solo() const -> bool
{
  return ::is_solo(game_type());
}

auto GameSummary::is_duty_solo() const -> bool
{
  return duty_solo_;
}

bool
GameSummary::is_lust_solo() const
{
  return (   is_solo()
          && !is_duty_solo());
}

auto GameSummary::startplayer_stays() const -> bool
{
  // Note: the check of duty solo round must be made by party
  if (!is_real_solo(game_type()))
    return false;
  // some solo
  if (is_duty_solo())
    return true;
  // some lust solo
  return rule()(Rule::Type::lustsolo_player_leads);
}

auto GameSummary::next_startplayer() const -> unsigned
{
  if (startplayer_stays())
    return startplayer();
  else
    return (startplayer() + 1) % players_number();
}

auto GameSummary::is_replayed_game() const -> bool
{
  return is_replayed_game_;
}

auto GameSummary::team_of_points() const -> Team
{
  if (winnerteam() == Team::noteam) {
    switch(rule()(Rule::Type::counting)) {
      case Counting::plus:
        if (points() > 0)
          return Team::re;
        else
          return Team::contra;

      case Counting::minus:
        if (points() < 0)
          return Team::re;
        else
          return Team::contra;

      case Counting::plusminus:
        return Team::re;

    } // switch(rule()(Rule::Type::counting))
  } else { // if !(winnerteam() == Team::noteam)
    switch(rule()(Rule::Type::counting)) {
      case Counting::plus:
        return winnerteam();

      case Counting::minus:
        return opposite(winnerteam());

      case Counting::plusminus:
        return winnerteam();
    } // switch(rule()(Rule::Type::counting))
  } // if !(winnerteam() == Team::noteam)

  return Team::noteam;
}

auto GameSummary::get_points(Team const team) const -> bool
{
  if (team == Team::noteam)
    return false;

  if (winnerteam() == Team::noteam) {
    switch(rule()(Rule::Type::counting)) {
      case Counting::plus:
        return ( ( (points() > 0)
                  && (team == Team::re))
                || ( (points() < 0)
                    && (team == Team::contra)) );
      case Counting::minus:
        return ( ( (points() < 0)
                  && (team == Team::re))
                || ( (points() > 0)
                    && (team == Team::contra)) );

      case Counting::plusminus:
        return (points_ != 0);

    } // switch(rule()(Rule::Type::counting))

  } else { // if !(p.winnerteam() == Team::noteam)
    switch(rule()(Rule::Type::counting)) {
      case Counting::plus:
      case Counting::minus:
        return (team == team_of_points());

      case Counting::plusminus:
        return true;
    } // switch(rule()(Rule::Type::counting))
  } // if !(p.winnerteam() == Team::noteam)

  DEBUG_ASSERT(false);
  return false;
}

auto GameSummary::points() const -> int
{
  return points_;
}

auto GameSummary::points(Team const team) const -> int
{
  if (team == Team::noteam)
    return 0;
  if (!get_points(team))
    return 0;

  // in a solo game 'points()' returns three times the value of 'points_'
  switch(rule().counting()) {
    case Counting::plus:
      if (   is_solo()
          && (   rule()(Rule::Type::solo_always_counts_triple)
              || (winnerteam() == Team::re)) )
        return 3 * points();

      if ( (winnerteam() == Team::noteam)
          && (team == Team::contra) )
        return -points();

      return points();

    case Counting::minus:
      if (   is_solo()
          && (   rule()(Rule::Type::solo_always_counts_triple)
              || (winnerteam() == Team::contra)) )
        return 3 * (-points());

      if ( (winnerteam() == Team::noteam)
          && (team == Team::re) )
        return points();

      return -points();

    case Counting::plusminus:
      if (is_solo()) {
        if (winnerteam() == Team::re) {
          if (team == winnerteam())
            return 3 * points();
          else
            return -points();
        } else { // if !(winnerteam() == Team::re)
          if (team == winnerteam())
            return points();
          else
            return -3 * points();
        } // if !(winnerteam() == Team::re)
      } // if (is_solo())

      if (team == team_of_points())
        return  points();
      else
        return -points();
  } // switch(rule().counting())

  DEBUG_ASSERTION(false,
                  "GameSummary::points(team):\n"
                  "  counting type not known: " << rule().counting());
  return 0;
}

auto GameSummary::get_points(Player const& player) const -> bool
{
  return get_points(team(player));
}

auto GameSummary::points(Player const& player) const -> int
{
  return points(team(player));
}

auto GameSummary::trick_points(Player const& player) const -> unsigned
{
  return (  has_played(player)
          ? trick_points_[game_player(player)]
          : 0);
}

auto GameSummary::trick_points(Team const team) const -> unsigned
{
  unsigned points = 0;
  for (unsigned p = 0; p < trick_points_.size(); ++p)
    if (teams_[p] == team)
      points += trick_points_[p];

  return points;
}

auto GameSummary::team(Player const& player) const -> Team
{
  return (  has_played(player)
          ? teams_[game_player(player)]
          : Team::noteam);
}


auto GameSummary::partner(Player const& player) const -> unsigned
{
  if (is_solo())
    return UINT_MAX;

  auto const myteam = team(player);
  for (auto const& p  : player.game().players()) {
    if (team(p) == myteam && p != player)
      return p.no();
  }
  return UINT_MAX;
}


auto GameSummary::specialpoints() const -> Specialpoints const&
{
  return specialpoints_;
}


auto GameSummary::specialpoint(unsigned const i) const -> Specialpoint const&
{
  DEBUG_ASSERTION(i < specialpoints_.size(),
                  "GameSummary::specialpoints(i = " << i << ")\n"
                  "i is too great, must be less then " << specialpoints_.size());

  return specialpoints_[i];
}


auto GameSummary::bock_multiplier() const -> int
{
  auto const is_bock = [](auto const& specialpoint) {
    return specialpoint.type == Specialpoint::Type::bock;
  };
  auto const multiplier = [](auto const& specialpoint) {
    return specialpoint.value;
  };
  return prod_if(specialpoints_, multiplier, is_bock);
}

auto GameSummary::bock_triggers() const -> vector<BockTrigger> const&
{
  return bock_triggers_;
}

void GameSummary::evaluate(Game const& game)
{
  auto const& rule = game.rule();

  // save the team and the trick points
  for (unsigned p = 0; p < game.players().size(); ++p) {
    auto const& player = game.player(p);
    teams_.push_back(player.team());
    trick_points_.push_back(game.points_of_player(player));
  } // for (p < game.playerno())

  // save the winnerteam
  winnerteam_ = game.winnerteam();

  // calculate highest announcements
  map<Announcement, unsigned> re_announcement_player;
  map<Announcement, unsigned> contra_announcement_player;
  for (unsigned p = 0; p < game.players().size(); ++p) {
    auto const& player = game.player(p);
    auto const& announcements = game.announcements().all(player);

    if (player.team() == Team::re) {
      if (player.announcement() == Announcement::reply)
        highest_announcement_re_ = Announcement::reply;
      if (player.announcement() > highest_announcement_re_)
        highest_announcement_re_ = player.announcement();

      for (auto const& a : announcements)
        re_announcement_player[a.announcement] = p;
    }

    if (player.team() == Team::contra) {
      if (player.announcement() == Announcement::reply)
        highest_announcement_contra_ = Announcement::reply;
      if( player.announcement() > highest_announcement_contra_)
        highest_announcement_contra_ = player.announcement();

      for (auto const& a : announcements)
        contra_announcement_player[a.announcement] = p;
    }

#ifdef WORKAROUND
    // Re/Kontra -> Reply, wenn die andere Partei eine Absage getätigt hat
    // Die Umwandlung no120 -> reply sollte schon früher im Spiel erfolgen
    if (!rule(Rule::Type::knocking)) {
      if (highest_announcement_re_ == Announcement::no120 && highest_announcement_contra_ > Announcement::no120)
        highest_announcement_re_ = Announcement::reply;
      if (highest_announcement_contra_ == Announcement::no120 && highest_announcement_re_ > Announcement::no120)
        highest_announcement_contra_ = Announcement::reply;
    }
#endif
  }

  { // special points
    // Order of the points
    // * won
    // * re no 120 said
    // * contra no 120 said
    // * re no 90
    // * contra no 90
    // * re no 90 said
    // * contra no 90 said
    // * re no 90 said && contra > 120 points
    // * contra no 90 said && re > 120 points
    // * re no 60
    // * contra no 60
    // * re no 60 said
    // * contra no 60 said
    // * re no 60 said && contra > 90 points
    // * contra no 60 said && re > 90 points
    // * re no 30
    // * contra no 30
    // * re no 30 said
    // * contra no 30 said
    // * re no 30 said && contra > 60 points
    // * contra no 30 said && re > 60 points
    // * re no 0
    // * contra no 0
    // * re no 0 said
    // * contra no 0 said
    // * re no 0 said && contra > 30 points
    // * contra no 0 said && re > 30 points
    // * against club queen
    // * multiplicator 3
    // * specialpoints

    { // announcements

      // shortcuts
      // got no 90
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TRICKPOINTS_NO(A) \
      if (   game.points_of_team(Team::re) < A \
          && game.winnerteam() != Team::re) \
      specialpoints_.emplace_back(Specialpoint::Type::no ## A, \
                                        Team::contra); \
      if (   game.points_of_team(Team::contra) < A \
          && game.winnerteam() != Team::contra) \
      specialpoints_.emplace_back(Specialpoint::Type::no ## A, \
                                        Team::re)
      // announced no 60
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TRICKPOINTS_NO_ANNOUNCED(A) \
      if (rule(Rule::Type::knocking)) { \
        for (unsigned p = 0; p < players_number_; ++p) { \
          if (   game.player(p).announcement() >= Announcement::no ## A \
              && game.winnerteam() != Team::noteam) { \
            Specialpoint sp(Specialpoint::Type::no ## A ## _said, \
                            game.player(p).team(), \
                            game.winnerteam()); \
            sp.player_of_no = p; \
            specialpoints_.push_back(sp); \
          } \
        } \
      } else { \
        if (   highest_announcement_re() >= Announcement::no ## A \
            && game.winnerteam() != Team::noteam) { \
          Specialpoint sp(Specialpoint::Type::no ## A ## _said, \
                          Team::re, \
                          game.winnerteam()); \
          sp.player_of_no = re_announcement_player[Announcement::no ## A]; \
          specialpoints_.push_back(sp); \
        } \
        if (   highest_announcement_contra() >= Announcement::no ## A \
            && game.winnerteam() != Team::noteam) { \
          Specialpoint sp(Specialpoint::Type::no ## A ## _said, \
                          Team::contra, \
                          game.winnerteam()); \
          sp.player_of_no = contra_announcement_player[Announcement::no ## A]; \
          specialpoints_.push_back(sp); \
        } \
      }
      // got 120 (against 'no 90')
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TRICKPOINTS_GOT_AGAINST(A, B) \
      if ((highest_announcement_contra() == Announcement::no ## A) \
          && (game.points_of_team(Team::re) >= A + 30)) \
      specialpoints_.emplace_back(Specialpoint::Type::no ## A ## _said_ ## B ## _got, \
                                        Team::re); \
      if ((highest_announcement_re() == Announcement::no ## A) \
          && (game.points_of_team(Team::contra) >= A + 30)) \
      specialpoints_.emplace_back(Specialpoint::Type::no ## A ## _said_ ## B ## _got, \
                                        Team::contra)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define POINTS(A, B) \
      TRICKPOINTS_NO(A); \
      TRICKPOINTS_NO_ANNOUNCED(A); \
      TRICKPOINTS_GOT_AGAINST(A, B);

      { // won
        if (game.winnerteam() != Team::noteam)
          specialpoints_.emplace_back(Specialpoint::Type::won,
                                            game.winnerteam());
      } // won
      if (rule(Rule::Type::announcement_contra_doubles_against_re)) {
        // against club queens
        if (   !::is_solo(game_type())
            && (game.winnerteam() == Team::contra) )
          specialpoints_.emplace_back(Specialpoint::Type::contra_won,
                                            Team::contra);
      } // if (Rule::Type::announcement_contra_doubles_against_re)

      if (rule(Rule::Type::announcement_re_doubles)) {
        POINTS(90, 120);
        POINTS(60, 90);
        POINTS(30, 60);

        { // 0
          // re no 0
          if (( !game.tricks().has_trick(Team::re))
              && (game.winnerteam() != Team::re))
            specialpoints_.emplace_back(Specialpoint::Type::no0,
                                              Team::contra);
          // contra no 0
          if (( !game.tricks().has_trick(Team::contra) )
              && (game.winnerteam() != Team::contra))
            specialpoints_.emplace_back(Specialpoint::Type::no0,
                                              Team::re);
          TRICKPOINTS_NO_ANNOUNCED(0);
          TRICKPOINTS_GOT_AGAINST(0, 30);
        } // 0
      } // if (rule(Rule::Type::announcement_re_doubles))

      if (game.winnerteam() != Team::noteam) {
        if (rule(Rule::Type::knocking)) {
          // no 120 announced
          for (unsigned p = 0; p < game.players().size(); ++p) {
            if (   game.player(p).announcement() >= Announcement::no120
                && game.winnerteam() != Team::noteam) {
              Specialpoint sp(Specialpoint::Type::no120_said,
                              game.player(p).team(),
                              game.winnerteam());
              sp.player_of_no = p;
              specialpoints_.push_back(sp);
            }
          } // for (p)
        } else { // if !(rule(Rule::Type::knocking))
          // re announced
          if (highest_announcement_re() >= Announcement::no120) {
            Specialpoint sp(Specialpoint::Type::no120_said,
                            Team::re,
                            game.winnerteam());
            sp.player_of_no = re_announcement_player[Announcement::no120];
            specialpoints_.push_back(sp);
          }
          if (highest_announcement_re() == Announcement::reply) {
            Specialpoint sp(Specialpoint::Type::announcement_reply,
                            Team::re,
                            game.winnerteam());
            sp.player_of_no = re_announcement_player[Announcement::reply];
            specialpoints_.push_back(sp);
          }
          // contra announced
          if (highest_announcement_contra() >= Announcement::no120) {
            Specialpoint sp(Specialpoint::Type::no120_said,
                            Team::contra,
                            game.winnerteam());
            sp.player_of_no = contra_announcement_player[Announcement::no120];
            specialpoints_.push_back(sp);
          }
          if (highest_announcement_contra() == Announcement::reply) {
            Specialpoint sp(Specialpoint::Type::announcement_reply,
                            Team::contra,
                            game.winnerteam());
            sp.player_of_no = contra_announcement_player[Announcement::reply];
            specialpoints_.push_back(sp);
          }
        } // if !(rule(Rule::Type::knocking))
      } // if (game.winnerteam() != Team::noteam)

      if (!rule(Rule::Type::announcement_re_doubles)) {
        POINTS(90, 120);
        POINTS(60, 90);
        POINTS(30, 60);

        { // 0
          // re no 0
          if (( !game.tricks().has_trick(Team::re))
              && (game.winnerteam() != Team::re))
            specialpoints_.emplace_back(Specialpoint::Type::no0,
                                        Team::contra);
          // contra no 0
          if (( !game.tricks().has_trick(Team::contra) )
              && (game.winnerteam() != Team::contra))
            specialpoints_.emplace_back(Specialpoint::Type::no0,
                                        Team::re);
          TRICKPOINTS_NO_ANNOUNCED(0);
          TRICKPOINTS_GOT_AGAINST(0, 30);
        } // 0
      } // if (!rule(Rule::Type::announcement_re_doubles))

      if (!rule(Rule::Type::announcement_contra_doubles_against_re)) {
        // against club queens
        if (   !::is_solo(game_type())
            && (game.winnerteam() == Team::contra) )
          specialpoints_.emplace_back(Specialpoint::Type::contra_won,
                                      Team::contra);
      } // if (Rule::Type::announcement_contra_doubles_against_re)

#undef TRICKPOINTS_RE_NO
#undef TRICKPOINTS_CONTRA_NO
#undef TRICKPOINTS_RE_GOT
#undef TRICKPOINTS_CONTRA_GOT
#undef ANNOUNCEMENT_REPLY
#undef TRICKPOINTS_RE_NO_ANNOUNCED
#undef TRICKPOINTS_CONTRA_NO_ANNOUNCED
#undef POINTS

    } // announcements

    // multiplicator 3 for solo games
    if (   ::is_solo(game_type())
        && (   rule(Rule::Type::solo_always_counts_triple)
            || (winnerteam() == Team::re) ) ) {
      Specialpoint sp(Specialpoint::Type::solo, winnerteam());
      sp.player_of_no = game.players().soloplayer().no();
      specialpoints_.push_back(sp);
    }

    // last collect all specialpoints of each trick
    for (auto const& t : game.tricks()) {
      for (auto const& s : t.specialpoints()) {
        if (s.is_valid(teams_)) {
          specialpoints_.push_back(s);
        }
      }
    }

    { // bock triggers
      if (rule(Rule::Type::bock_120)) {
        // bock after 120-120 points
        if (game.points_of_team(Team::re) == game.points_of_team(Team::contra))
          bock_triggers_.push_back(BockTrigger::equal_points);
      } // if (rule(Rule::Type::bock_120))

      if (rule(Rule::Type::bock_solo_lost)) {
        // bock after a lost solo game
        if (   ::is_solo(game.type())
            && (game.winnerteam() == Team::contra))
          bock_triggers_.push_back(BockTrigger::solo_lost);
      } // if (rule(Rule::Type::bock_solo_lost))

      if (rule(Rule::Type::bock_re_lost)) {
        // bock after a lost re/contra game (only re/contra is announced)
        if (   (   game.announcements().last(Team::re)
                != Announcement::noannouncement)
            && (game.winnerteam() != Team::re))
          bock_triggers_.push_back(BockTrigger::re_lost);
        if (   (   game.announcements().last(Team::contra)
                != Announcement::noannouncement)
            && (game.winnerteam() != Team::contra))
          bock_triggers_.push_back(BockTrigger::contra_lost);
      } // if (rule(Rule::Type::bock_re_lost))

      if (rule(Rule::Type::bock_heart_trick)) {
        if (!::is_solo(game.type())) {
          // bock after a real heart trick
          for (auto const& t : game.tricks()) {
            auto c = t.cards().begin();
            for (; c != t.cards().end(); ++c) {
              if (c->tcolor() != Card::heart) {
                break;
              }
            }

            if (c == t.cards().end())
              bock_triggers_.push_back(BockTrigger::heart_trick);
          } // for (t : game.tricks())
        } // if (!::is_solo(game.type()))
      } // if (rule(Rule::Type::bock_heart_trick))

      if (rule(Rule::Type::bock_black)) {
        // bock after a black game (a team got no trick)
        if (   !game.tricks().has_trick(Team::re)
            || !game.tricks().has_trick(Team::contra) )
          bock_triggers_.push_back(BockTrigger::black);
      } // if (rule(Rule::Type::bock_black))

      if (game.bock_multiplier() != 1) {
        specialpoints_.emplace_back(Specialpoint::Type::bock,
                                    game.bock_multiplier());
      } // if (game.bock_multiplier())
    } // bock triggers

    // calculating the main game points
    points_ = sum(specialpoints_, winnerteam(), game);

    DEBUG_ASSERTION((   winnerteam() == Team::re
                     || winnerteam() == Team::contra
                     || winnerteam() == Team::noteam),
                    "GameSummary::evaluate(Game):" << '\n'
                    << "  wrong team at end: " << winnerteam());

  } // special points
}

auto operator<<(ostream& ostr, GameSummary const& game_summary) -> ostream&
{
  game_summary.write(ostr);
  return ostr;
}
