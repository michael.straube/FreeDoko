/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/game_type.h"
#include "gameplay_action.h"

class Card;
class Hand;
class Game;
class Player;

// The gameplay, saved as actions
class Gameplay {
  friend class Game;
public:
  Gameplay() = delete;
  explicit Gameplay(Game const& game);
  Gameplay(Game const& game, Gameplay const& gameplay);
  Gameplay(Gameplay const&) = delete;
  auto operator=(Gameplay const&) = delete;
  ~Gameplay();

  auto clone(Game const& game) const -> unique_ptr<Gameplay>;

  auto write(ostream& ostr) const -> ostream&;

  void cards_distributed();
  void redistribute();
  void game_start();

  auto actions() const -> vector<unique_ptr<GameplayAction>> const&;
  void add(GameplayAction const& action);

private:
  Game const* const game_;
  unsigned seed_ = UINT_MAX;
  GameType gametype_ = GameType::normal;
  unsigned soloplayer_ = UINT_MAX;
  vector<Hand> hands_;
  vector<unique_ptr<GameplayAction>> actions_;
}; // class Gameplay

auto operator<<(ostream& ostr, Gameplay const& gameplay) -> ostream&;
