/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once


namespace GameplayActions {

enum class Type {
  // cards_distributed,
  reservation,
  poverty_shift,
  poverty_accepted,
  poverty_returned,
  poverty_denied,
  poverty_denied_by_all,
  card_played,
  announcement,
  swines,
  hyperswines,
  marriage,
  trick_open,
  trick_full,
  trick_taken,
  check,
  print,
  quit
}; // enum class Type
enum class Discrepancy {
  future = -1,
  none = 0,
  skipped = 1,
  player,
  card,
  other
}; // enum class Discrepancy


/** base class for gameplay actions
 **/
class GameplayAction {
public:
  static auto create(string const& line)                -> unique_ptr<GameplayAction>;
  static auto create(string const& line, istream& istr) -> unique_ptr<GameplayAction>;
protected:
  GameplayAction() = default;

public:
  virtual ~GameplayAction() = default;

  auto clone() const -> unique_ptr<GameplayAction>;

  virtual auto equal(GameplayAction const& action) const -> bool;

  virtual void write(ostream& ostr) const = 0;

  virtual auto type() const -> Type = 0;

  virtual auto data_translation() const -> string = 0;
}; // class GameplayAction
auto player(GameplayAction const& action) -> unsigned;

auto to_string(Type type)                 -> string;
auto gettext(Type type)                   -> string;
auto operator<<(ostream& ostr, Type type) -> ostream&;
auto to_string(Discrepancy discrepancy)                 -> string;
auto operator<<(ostream& ostr, Discrepancy discrepancy) -> ostream&;

auto operator!(Discrepancy discrepancy)    -> bool;
auto operator||(Discrepancy lhs, bool rhs) -> bool;

auto to_string(GameplayAction const& action)                 -> string;
auto operator<<(ostream& ostr, GameplayAction const& action) -> ostream&;

auto operator==(GameplayAction const& lhs, GameplayAction const& rhs) -> bool;
auto operator!=(GameplayAction const& lhs, GameplayAction const& rhs) -> bool;
}; // namespace GameplayActions

using GameplayAction = GameplayActions::GameplayAction;
