/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../gameplay_action.h"

namespace GameplayActions {

/**
 ** base class for pure gameplay actions
 ** t.i. there is no player who has initiated the action
 ** example: PovertyDeniedByAll
 **/
template<Type t>
  struct Base : public GameplayAction {
    Base() = default;
    explicit Base(string const& line);
    ~Base() override = default;

    auto equal(GameplayAction const& action) const -> bool override;

    void write(ostream& ostr) const override;

    auto type() const -> Type final;

    auto data_translation() const -> string override;
  }; // struct Base : public GameplayAction

} // namespace GameplayActions

#include "base.hpp"
