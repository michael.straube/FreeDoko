/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "print.h"

#include "../../utils/string.h"

namespace GameplayActions {

Print::Print() = default;

Print::Print(string const& line)
{
  auto pre = to_string(type()) + " = ";
  if (string(line, 0, pre.length()) != pre) {
    pre = to_string(type());
    if (string(line, 0, pre.length()) != pre) {
      return ;
    }
  }

  info = string(line, pre.length());
  String::remove_blanks(info);
}

Print::~Print() = default;

void Print::write(ostream& ostr) const
{
  ostr << type();
  if (!info.empty())
    ostr << " " << info;
}

auto Print::data_translation() const -> string
{
  return info;
}

} // namespace GameplayActions

