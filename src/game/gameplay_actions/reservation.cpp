/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "reservation.h"

#include "../../utils/string.h"

namespace GameplayActions {

Reservation::Reservation(unsigned const player,
                         ::Reservation const& reservation) :
  BasePlayer(player),
  reservation(reservation)
{ }

Reservation::Reservation(string line)
{
  // ex: reservation = 0, marriage, first color, hyperswines
  string const pre = to_string(type()) + " =";
  if (string(line, 0, pre.length()) != pre)
    return ;

  istringstream istr(string(line, pre.length()));
  istr >> player;
  if (istr.get() != ',')
    return ;
  string reservation_text;
  getline(istr, reservation_text);
  reservation = ::Reservation(reservation_text);
}

auto Reservation::equal(GameplayAction const& action) const -> bool
{
  return (   BasePlayer::equal(action)
	  && (reservation == dynamic_cast<Reservation const&>(action).reservation));
}

void Reservation::write(ostream& ostr) const
{
  ostr << type();
  ostr << " = " << player;
  ostr << ", " << reservation.game_type;
  if (reservation.offer_duty_solo)
    ostr << " (offer duty solo)";
  if (reservation.game_type == GameType::marriage)
    ostr << ", " << reservation.marriage_selector;
  if (reservation.swines)
    ostr << ", swines";
  if (reservation.hyperswines)
    ostr << ", hyperswines";
}

auto Reservation::data_translation() const -> string
{
  string translation;

  translation += std::to_string(player);

  translation += ", " + _(reservation.game_type);
  if (reservation.offer_duty_solo)
    translation += " (" + _("offer duty solo") + ")";

  if (reservation.game_type == GameType::marriage)
    translation += ", " + _(reservation.marriage_selector);

  if (reservation.swines)
    translation += ", " + _("swines");
  if (reservation.hyperswines)
    translation += ", " + _("hyperswines");

  return translation;
}

} // namespace GameplayActions
