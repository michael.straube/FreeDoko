/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "base.h"
#include "../../card/trick.h"

namespace GameplayActions {

using TrickOpen = Base<Type::trick_open>;
using TrickTaken = Base<Type::trick_taken>;

struct TrickFull : public Base<Type::trick_full> {
  TrickFull() = delete;
  explicit TrickFull(Trick const& trick);
  explicit TrickFull(string const& line);
  TrickFull(string const& line, istream& istr);

  ~TrickFull() override = default;

  auto equal(GameplayAction const& action) const -> bool override;

  void write(ostream& ostr) const override;

  auto data_translation() const -> string override;

  Trick trick;
};

} // namespace GameplayActions
