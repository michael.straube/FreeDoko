/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

class Game;
class Player;

/**
 ** The marriage part of a game
 **/
class GameMarriage {
  friend class Game;
public:
  explicit GameMarriage(Game& game)                      noexcept;
  GameMarriage(GameMarriage const& marriage, Game& game) noexcept;

  GameMarriage(GameMarriage const&) = delete;

  void reset() noexcept;

  auto is_undetermined()                       const noexcept -> bool;
  auto selector()                              const noexcept -> MarriageSelector;
  void set_selector(MarriageSelector selector);
  auto determination_trickno()                 const noexcept -> unsigned;

  void check_for_silent_marriage();
  void determine_silent_marriage();
  void determine_marriage();

private:
  Game* const game_;
  MarriageSelector selector_ = MarriageSelector::team_set;
  unsigned determination_trickno_ = UINT_MAX;

public:
  Signal<void(Player const&, Player const&)> signal_marriage;
};
