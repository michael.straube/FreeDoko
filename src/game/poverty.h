/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../card/hand_cards.h"
class Game;
class Player;

/**
 ** The poverty part of a game
 **/
class GamePoverty {
  friend class Game;
public:
  using Cards = vector<Card>;

public:
  explicit GamePoverty(Game& game) noexcept;
  GamePoverty(GamePoverty const& poverty, Game& game);

  GamePoverty(GamePoverty const&) = delete;
  auto operator=(GamePoverty const&) = delete;

  auto shifted()          const noexcept -> bool;
  auto shifted_cardno()   const noexcept -> unsigned;
  auto returned_cardno()  const noexcept -> unsigned;
  auto returned_trumpno() const noexcept -> unsigned;
  auto shifted_cards()    const          -> Cards const&;
  auto returned_cards()   const          -> Cards const&;

  void reset() noexcept;
  void shift();

private:
  Game* const game_;
  Cards shifted_cards_;
  Cards returned_cards_;
  bool shifted_ = false;

public:
  Signal<void(Player const&)> signal_shifting;
  Signal<void(Player const&, unsigned)> signal_shift;
  Signal<void(Player const&, unsigned)> signal_ask;
  Signal<void(Player const&)> signal_take_denied;
  Signal<void()> signal_take_denied_by_all;
  Signal<void(Player const&, unsigned, unsigned)> signal_take_accepted;
  Signal<void(Player const&)> signal_get_cards_to_shift;
  Signal<void(Player const&, unsigned)> signal_get_take_accept;
  Signal<void(Player const&, HandCards)> signal_get_exchanged_cards;
  Signal<void(Player const&, HandCards)> signal_cards_get_back;
}; // class GamePoverty
