/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/game_type.h"
#include "../basetypes/marriage_selector.h"

struct Reservation {
  Reservation() = default;
  explicit Reservation(string const& line);
  ~Reservation() = default;;

  GameType game_type                 = GameType::normal;
  MarriageSelector marriage_selector = MarriageSelector::team_set;
  bool offer_duty_solo = false;
  bool swines          = false;
  bool hyperswines     = false;
}; // class Reservation

auto operator==(Reservation lhs, Reservation rhs)     noexcept -> bool;
auto to_string(Reservation const& reservation)                 -> string;
auto to_rest_string(Reservation const& reservation)            -> string;
auto operator<<(ostream& ostr, Reservation const& reservation) -> ostream&;
