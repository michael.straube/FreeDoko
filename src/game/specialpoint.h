/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/team.h"
class Game;

/**
 ** a structure for the management of specialpoints in tricks, gamepoints and
 ** gamepointstable
 **/
struct Specialpoint {
  enum class Type {
    nospecialpoint,
    caught_fox,
    caught_fox_last_trick,
    fox_last_trick,
    //double_fox_last_trick,
    charlie,
    caught_charlie,
    //double_charlie,
    //caught_double_charlie,
    dulle_caught_dulle,
    heart_trick,
    doppelkopf,
    won,
    no90,
    no60,
    no30,
    no0,
    no120_said,
    no90_said,
    no60_said,
    no30_said,
    no0_said,
    no90_said_120_got,
    no60_said_90_got,
    no30_said_60_got,
    no0_said_30_got,
    announcement_reply,
    contra_won,
    solo,
    bock
  }; // enum class Type
  static constexpr auto types = array<Type, 27>{
    Type::nospecialpoint,
    Type::caught_fox,
    Type::caught_fox_last_trick,
    Type::fox_last_trick,
    //Type::double_fox_last_trick,
    Type::charlie,
    Type::caught_charlie,
    //Type::double_charlie,
    //Type::caught_double_charlie,
    Type::dulle_caught_dulle,
    Type::heart_trick,
    Type::doppelkopf,
    Type::won,
    Type::no90,
    Type::no60,
    Type::no30,
    Type::no0,
    Type::no120_said,
    Type::no90_said,
    Type::no60_said,
    Type::no30_said,
    Type::no0_said,
    Type::no90_said_120_got,
    Type::no60_said_90_got,
    Type::no30_said_60_got,
    Type::no0_said_30_got,
    Type::announcement_reply,
    Type::contra_won,
    Type::solo,
    Type::bock
  };
  Specialpoint(Type type, Team team)                     noexcept;
  Specialpoint(Type type, Team team, Team counting_team) noexcept;
  Specialpoint(Type type, int value)                     noexcept;
  explicit Specialpoint(istream& istr);

  auto is_valid(vector<Team> const& team) const -> bool;

  Type type = Type::nospecialpoint;
  Team team = Team::noteam; // team which has made the point
  Team counting_team = Team::noteam; // team, which gets the point
  // the player who get the special point
  // (i.e. the player who has caught a fox)
  unsigned player_get_no = UINT_MAX;
  // the player who has 'lost' the special point
  // (i.e. the player whose fox has been caught)
  unsigned player_of_no = UINT_MAX;
  // the value of the specialpoint (used for bock rounds)
  int value = 0;
}; // struct Specialpoint

using Specialpoints = vector<Specialpoint>;


auto is_winning(Specialpoint::Type sp)      noexcept -> bool;
auto is_announcement(Specialpoint::Type sp) noexcept -> bool;

auto value(Specialpoint::Type sp)     noexcept -> int;
auto to_string(Specialpoint::Type sp) noexcept -> string;
auto specialpoint_type_from_string(string const& name)   -> Specialpoint::Type;
auto operator<<(ostream& ostr, Specialpoint::Type sp)    -> ostream&;
auto operator<<(ostream& ostr, Specialpoint const& sp)   -> ostream&;
auto operator<<(ostream& ostr, Specialpoints const& spv) -> ostream&;

auto sum(Specialpoints const& spv, Team winner, Game const& g) -> int;

auto gettext(Specialpoint::Type sp) -> string;
auto gettext(Specialpoint const& sp) -> string;
