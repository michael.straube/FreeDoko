/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "status.h"


auto to_string(GameStatus const game_status) noexcept -> string
{
  switch (game_status) {
  case GameStatus::init:
    (void)_("GameStatus::init");
    return "init";
  case GameStatus::cards_distribution:
    (void)_("GameStatus::cards distribution");
    return "cards distribution";
  case GameStatus::manual_cards_distribution:
    (void)_("GameStatus::manual cards distribution");
    return "manual cards distribution";
  case GameStatus::reservation:
    (void)_("GameStatus::reservation");
    return "reservation";
  case GameStatus::redistribute:
    (void)_("GameStatus::redistribute");
    return "redistribute";
  case GameStatus::poverty_shift:
    (void)_("GameStatus::poverty shift");
    return "poverty shift";
  case GameStatus::start:
    (void)_("GameStatus::start");
    return "start";
  case GameStatus::play:
    (void)_("GameStatus::play");
    return "play";
  case GameStatus::full_trick:
    (void)_("GameStatus::full trick");
    return "full trick";
  case GameStatus::trick_taken:
    (void)_("GameStatus::trick taken");
    return "trick taken";
  case GameStatus::finished:
    (void)_("GameStatus::finished");
    return "finished";
  } // switch(game_status)

  return {};
}


auto gettext(GameStatus const game_status) -> string
{
  return gettext("GameStatus::" + to_string(game_status));
}


auto operator<<(ostream &ostr, GameStatus const game_status) -> ostream &
{
  ostr << ::to_string(game_status);
  return ostr;
}
