/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game.h"
#include "gameplay.h"
#include "gameplay_actions/swines.h"

#include "../party/rule.h"
#include "../ui/ui.h"

GameSwines::GameSwines(Game& game) noexcept :
  game_(&game)
{ }


GameSwines::GameSwines(GameSwines const& swines, Game& game) :
  game_(&game),
  swines_owner_(swines.swines_owner_
                ? &game.players().player(swines.swines_owner_->no())
                : nullptr),
  first_fox_catcher_(swines.first_fox_catcher_
                     ? &game.players().player(swines.first_fox_catcher_->no())
                     : nullptr),
  swines_announced_(swines.swines_announced_),
  hyperswines_owner_(swines.hyperswines_owner_
                     ? &game.players().player(swines.hyperswines_owner_->no())
                     : nullptr),
  hyperswines_announced_(swines.hyperswines_announced_)
{ }


void GameSwines::self_check() const
{
  if (swines_owner_) {
    DEBUG_ASSERTION((game_ == &swines_owner_->game()),
		    "GameSwines::self_check()\n"
		    "  the swines owner is not of the same game");
  }
  if (first_fox_catcher_) {
    DEBUG_ASSERTION((game_ == &first_fox_catcher_->game()),
		    "GameSwines::self_check()\n"
		    "  the first fox catcher is not of the same game");
  }
  if (hyperswines_owner_) {
    DEBUG_ASSERTION((game_ == &hyperswines_owner_->game()),
		    "GameSwines::self_check()\n"
		    "  the hyperswines owner is not of the same game");
  }
}


void GameSwines::reset() noexcept
{
  swines_owner_          = {};
  first_fox_catcher_     = {};
  swines_announced_      = false;
  hyperswines_owner_     = {};
  hyperswines_announced_ = false;
}


auto GameSwines::first_fox_caught() const noexcept -> bool
{
  return first_fox_catcher_;
}


auto GameSwines::first_fox_catcher() const noexcept -> Player const&
{
  return *first_fox_catcher_;
}


auto GameSwines::swines_announced() const noexcept -> bool
{
  return swines_owner_;
}


auto GameSwines::swines_owner() const noexcept -> Player const&
{
  return *swines_owner_;
}


auto GameSwines::swines_announcement_valid(Player const& player) const -> bool
{
 // for the rules look into the code
 // In a reservation 'true' can be returned although the announcement cannot
 // be made (see GameSwines::swines_announce).
 // This is needed for the sorting in solo games.

  auto const& game = *game_;
  auto const& rule = game.rule();
  auto const& hand = player.hand();

  if (!rule(Rule::Type::swines))
    return false;

  // the swines must not be announced already
  if (   swines_announced()
      && player == swines_owner())
    return false;

  // check game
  switch (game.type()) {
  case GameType::normal:
    break;
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
    return false;
  case GameType::poverty:
    if (!rule(Rule::Type::swines_in_poverty))
      return false;
    break;
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    break;
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
    return false;
  case GameType::solo_club:
  case GameType::solo_spade:
  case GameType::solo_heart:
  case GameType::solo_diamond:
    if (!rule(Rule::Type::swines_in_solo))
      return false;
    break;
  case GameType::solo_meatless:
    return false;
  } // switch (type())

  // check 'fox do not count' in poverty
  if (   game.type() == GameType::poverty
      && (player == (game_->status() == Game::Status::reservation
                     ? game.players().current_player()
                     : game.players().soloplayer()) )
      && (hand.count_poverty_cards()
          > rule(Rule::Type::max_number_of_poverty_trumps)) )
    return false;

  // check cards number
  if (   (rule(Rule::Type::swine_only_second)
          ? (hand.count(Card::trump, Card::ace) != 1)
          : (hand.count(Card::trump, Card::ace)
             != rule(Rule::Type::number_of_same_cards))) )
    return false;

  // special case: shifted swines
  if (   game_->status() == Game::Status::poverty_shift
      && swines_announced())
    return true;

  // 'swines announcement begin': must be in the reservation
  // (in a poverty before the shifting/accepting)
  if (   rule(Rule::Type::swines_announcement_begin)
      && (   game_->status() == Game::Status::start
          || game_->status() == Game::Status::init
          || game_->status() == Game::Status::reservation
          || (   game_->status() == Game::Status::poverty_shift
              && (   game.poverty().shifted_cardno() == 0
                  || player.team() == Team::unknown))
          || (   game.type() != GameType::poverty
              && game.cards().count_played() == 0)
         )
      && !game.poverty().shifted()) {
    return true;
  }

  // normal case, announcement in the game: can announce
  if (   !rule(Rule::Type::swines_announcement_begin)
      && !rule(Rule::Type::swine_only_second))
    return true;

  // 'swine only second': first fox must be caught by the same team
  if (   rule(Rule::Type::swine_only_second)
      && (hand.count_all(Card::trump, Card::ace)
          == rule(Rule::Type::number_of_same_cards))
      && first_fox_catcher_
      && (   (game.teaminfo().get(first_fox_catcher())
              == game.teaminfo().get(player))
          && (   ::is_real(game.teaminfo().get(player))
              || (first_fox_catcher() == player) )
         )
     )
    return true;

  return false;
} // bool GameSwines::swines_announcement_valid(Player player) const


auto GameSwines::swines_announce(Player& player) -> bool
{
  auto& game = *game_;
  auto const& rule = game.rule();

  if (!swines_announcement_valid(player))
    return false;

  // in a reservation no announcement is allowed,
  // but 'valid' returns 'true', because of the cards ordering.
  // This is needed when solo games are selected
  if (game_->status() == Game::Status::reservation)
    return false;

  swines_owner_ = &player;
  swines_announced_ = true;

  if (rule(Rule::Type::swine_only_second)) {
    // the swines owner has to say his team
    game.teaminfo().update();
  }

  game.gameplay_->add(GameplayActions::Swines(swines_owner().no()));
  if (!game.isvirtual()) {
    ::ui->swines_announced(swines_owner());
    signal_swines_announced(swines_owner());
    ::ui->gameplay_action(GameplayActions::Swines(swines_owner().no()));
    game.signal_gameplay_action(GameplayActions::Swines(swines_owner().no()));
  }

  for (auto& p : game.players()) {
    p.swines_announced(player);
  }

  game.cards().update_swines();

  return true;
} // bool GameSwines::swines_announce(Player& player)


auto GameSwines::hyperswines_announced() const noexcept -> bool
{
  return hyperswines_owner_;
}


auto GameSwines::hyperswines_owner() const noexcept -> Player const&
{
  return *hyperswines_owner_;
}


auto GameSwines::hyperswines_announcement_valid(Player const& player) const -> bool
{
  auto const& game = *game_;
  auto const& rule = game.rule();
  auto const& hand = player.hand();

  // check rule 'hyperswines'
  if (!rule(Rule::Type::hyperswines))
    return false;

  // check rule 'hyperswines in solo'
  if (   ::is_solo(game.type())
      && !rule(Rule::Type::hyperswines_in_solo))
    return false;

  // check rule 'hyperswines in poverty'
  if (   ::is_poverty(game.type())
      && !rule(Rule::Type::hyperswines_in_poverty))
    return false;

  // check for enough cards on the hand
  if (hand.count(game.cards().hyperswine())
      != rule(Rule::Type::number_of_same_cards))
    return false;

#ifdef WORKAROUND
#ifndef WEBSOCKETS_SERVICE
  // in the following case we have a problem
  // * virtual games
  // * cards informations
  //   - the player can have diamond ace and diamond nine
  //   - the player cannot have all four cards (2 diamond ace, 2 diamond nines)
  // @todo   use 'remaining_cards_no(player) < 4'
  if (   game.isvirtual()
      && !hyperswines_announced())
    return false;
#endif
#endif

  // swines must be announced or must be valid
  if (!(   swines_announced()
        || swines_announcement_valid(player)))
    return false;

  // check for joint swines and hyperswines
  if (   !rule(Rule::Type::swines_and_hyperswines_joint)
      && (   (swines_owner_ == &player)
          || (   !swines_announced()
              && swines_announcement_valid(player))))
    return false;

  // special case: shifted hyperswines
  if (   game_->status() == Game::Status::poverty_shift
      && (   game.players().current_player().team() == Team::unknown
          || hyperswines_owner_) )
    return true;

  // the hyperswines must not be announced
  if (hyperswines_announced())
    return false;

  // check for right announcement time
  if (   rule(Rule::Type::hyperswines_announcement_begin)
      && (   game_->status() == Game::Status::init
          || game_->status() == Game::Status::reservation
          || (   game_->status() == Game::Status::poverty_shift
              && (   game.poverty().shifted_cardno() == 0
                  || player.team() == Team::unknown))
          || (   game.type() != GameType::poverty
              && game.cards().count_played() == 0)
         )
      && !game.poverty().shifted()) {
    return true;
  }

  // normal case, announcement in the game: can announce
  if (!rule(Rule::Type::hyperswines_announcement_begin))
    return true;

  return false;
}


auto GameSwines::hyperswines_announce(Player& player) -> bool
{
  auto& game = *game_;

  if (!hyperswines_announcement_valid(player))
    return false;

  if (!swines_announced())
    swines_announce(player);

  // this check is needed again, since after the 'swines announce' a player
  // can alread have announced hyperswines
  if (!hyperswines_announcement_valid(player))
    return false;

  hyperswines_owner_ = &player;
  hyperswines_announced_ = true;

  game.gameplay_->add(GameplayActions::Hyperswines(hyperswines_owner().no()));
  if (!game.isvirtual()) {
    ::ui->hyperswines_announced(hyperswines_owner());
    signal_hyperswines_announced(hyperswines_owner());
    ::ui->gameplay_action(GameplayActions::Hyperswines(hyperswines_owner().no()));
    game.signal_gameplay_action(GameplayActions::Hyperswines(hyperswines_owner().no()));
  } // if (!isvirtual())

  for (auto& p : game.players())
    p.hyperswines_announced(player);

  game.cards().update_hyperswines();

  return true;
}


void GameSwines::test_swines_from_reservations()
{
  auto& game = *game_;
  auto& players = game.players();
  auto const& rule = game.rule();

  if (!rule(Rule::Type::swines))
    return ;

  for (unsigned i = 0; i < game.playerno();
       i++, game.players().next_current_player()) {
    if (   game.reservation(game.players().current_player()).swines
        && swines_announcement_valid(game.players().current_player()) )
      swines_announce(game.players().current_player());
  }
  for (unsigned i = 0; i < game.playerno();
       i++, game.players().next_current_player()) {
    if (   game.reservation(game.players().current_player()).hyperswines
        && hyperswines_announcement_valid(game.players().current_player()) )
      hyperswines_announce(game.players().current_player());
  }

  if (rule(Rule::Type::swines_announcement_begin)) {
    for (auto& p : players)
      p.check_swines_announcement_at_game_start();
  }
}


void GameSwines::evaluate_trick(Trick const& trick)
{
  auto& game = *game_;
  auto const& rule = game.rule();

  // test for catcher of the first fox of swines
  if (   rule(Rule::Type::swine_only_second)
      && !first_fox_catcher_) {
    auto const istrumpace = [&game](auto const& card) {
      return card == game.cards().fox();
    };
    if (container_algorithm::any_of(trick, istrumpace))
      first_fox_catcher_ = &trick.winnerplayer();
  }
}
