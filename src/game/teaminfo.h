/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/team.h"

class Game;
class Player;

/**
 ** The teaminfo of a game
 **/
class GameTeamInfo {
  friend class Game;
public:
  explicit GameTeamInfo(Game& game) noexcept;
  GameTeamInfo(GameTeamInfo const& teaminfo, Game& game) noexcept;

  GameTeamInfo(GameTeamInfo const&) = delete;

  void reset();
  void set_at_gamestart();
  void update();

  auto get(Player const& player)                const          -> Team;
  auto get(unsigned playerno)                   const          -> Team;
  auto get_all()                                const noexcept -> vector<Team> const&;
  auto get_human_teaminfo(Player const& player) const          -> Team;
  void set(Player const& player, Team team);
  void set_unknown(Team team)                         noexcept;

  void write(ostream& ostr) const;

private:
  Game* const game_ = nullptr;
  vector<Team> teaminfo_;
  bool all_known_ = false;

public:
  Signal<void(Player const&)> signal_changed;
};

auto operator<<(ostream& ostr, GameTeamInfo const& teaminfo) -> ostream&;
