/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "tricks.h"

GameTricksIterator::GameTricksIterator(GameTricks& tricks,
                                       size_t const p) noexcept :
  tricks_(&tricks),
  p_(p)
{ }


auto GameTricksIterator::operator*() -> Trick&
{
  return tricks_->trick(p_);
}

auto GameTricksIterator::operator*() const -> Trick const&
{
  return tricks_->trick(p_);
}


auto GameTricksIterator::operator->() -> Trick*
{
  return &(**this);
}


auto GameTricksIterator::operator->() const -> Trick const*
{
  return &(**this);
}


auto GameTricksIterator::operator++() noexcept -> GameTricksIterator&
{
  p_ += 1;
  return *this;
}


auto GameTricksIterator::operator==(GameTricksIterator const& i) const noexcept -> bool
{
  return (   p_ == i.p_
          && tricks_ == i.tricks_);
}


auto GameTricksIterator::operator!=(GameTricksIterator const& i) const noexcept -> bool
{
  return !(*this == i);
}


GameTricksConstIterator::GameTricksConstIterator(GameTricks const& tricks,
                                                 size_t const p) noexcept :
  tricks_(&tricks),
  p_(p)
{ }


auto GameTricksConstIterator::operator*() const -> Trick const&
{
  return tricks_->trick(p_);
}


auto GameTricksConstIterator::operator->() const -> Trick const*
{
  return &(**this);
}


auto GameTricksConstIterator::operator++() noexcept -> GameTricksConstIterator&
{
  p_ += 1;
  return *this;
}


auto GameTricksConstIterator::operator==(GameTricksConstIterator const& i) const noexcept -> bool
{
  return (   p_ == i.p_
          && tricks_ == i.tricks_);
}


auto GameTricksConstIterator::operator!=(GameTricksConstIterator const& i) const noexcept -> bool
{
  return !(*this == i);
}
