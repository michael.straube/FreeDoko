/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef BUG_REPORT

class Party;
class Game;

namespace BugReport {
auto create_bug_report(Party const& party, string const& message = "", string const& subdir = ".") -> Path;
void create_from_dokolounge(string erstellername, int tischnummer, string spielername, string message);
void create_from_dokolounge(string erstellername, Game const& game, int tischnummer, string spielername, string message);
void create_assertion_bug_report(string const& message);
auto default_filename(Party const& party) -> string;
} // namespace BugReport

#endif
