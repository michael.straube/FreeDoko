/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "preferences.cardsorder.h"

#include "../utils/string.h"

#include "../party/party.h"
#include "../game/game.h"

#include "../ui/ui.h"

Preferences::CardsOrder::CardsOrder(Preferences& preferences) :
  preferences_(&preferences),
  pos_(Card::number_of_tcolors, Card::nocardcolor),
  direction_(Card::number_of_tcolors, Direction::down)
{
  pos_[Card::trump]   = 0;
  pos_[Card::club]    = 1;
  pos_[Card::heart]   = 2;
  pos_[Card::spade]   = 3;
  pos_[Card::diamond] = 4;
}


Preferences::CardsOrder::CardsOrder(Preferences& preferences,
                                    Card::TColor c0, Direction d0,
                                    Card::TColor c1, Direction d1,
                                    Card::TColor c2, Direction d2,
                                    Card::TColor c3, Direction d3,
                                    Card::TColor c4, Direction d4) :
  preferences_(&preferences),
  sorted_(true),
  pos_(Card::number_of_tcolors, Card::nocardcolor),
  direction_(Card::number_of_tcolors, Direction::down)
{
  // the colors should be different
  pos_[c0] = 0;
  pos_[c1] = 1;
  pos_[c2] = 2;
  pos_[c3] = 3;
  pos_[c4] = 4;

  direction_[c0] = d0;
  direction_[c1] = d1;
  direction_[c2] = d2;
  direction_[c3] = d3;
  direction_[c4] = d4;

  ensure_validity();
}


Preferences::CardsOrder::CardsOrder(Preferences& preferences, string const& value) :
  preferences_(&preferences),
  sorted_(true),
  pos_(Card::number_of_tcolors, Card::nocardcolor),
  direction_(Card::number_of_tcolors, Direction::down)
{
  // *** should work better, when you can get words of the string
  // *** (expand the string-class)
  string remain_string = value;
  string word; // the word of the string
  Card::TColor tcolor = Card::nocardcolor;

  String::remove_blanks(remain_string);
  for (unsigned i = 0; i < Card::number_of_tcolors; i++) {
    if (remain_string.empty())
      break;
    // get the color
    word = string(remain_string, 0, remain_string.find(' '));
    if (remain_string.find(' ') == string::npos)
      word = remain_string;
    if ((word == to_string(Card::trump))
        || (word == _(Card::trump)))
      tcolor = Card::trump;
    else if ((word == to_string(Card::club))
             || (word == _(Card::club)))
      tcolor = Card::club;
    else if ((word == to_string(Card::spade))
             || (word == _(Card::spade)))
      tcolor = Card::spade;
    else if ((word == to_string(Card::heart))
             || (word == _(Card::heart)))
      tcolor = Card::heart;
    else if ((word == to_string(Card::diamond))
             || (word == _(Card::diamond)))
      tcolor = Card::diamond;
    else if ((i == 0)
             && ((word == "unsorted")
                 || (word == _("Preferences::CardsOrder::unsorted"))
                 || (word == "random")
                 || (word == _("Preferences::CardsOrder::random"))
                 || (word == "mixed")
                 || (word == _("Preferences::CardsOrder::mixed"))
                )
            ) {
      // random order
      sorted_ = false;
      break;
    } else {
      cerr << "Unknown TColor \"" << word << "\"" << endl;
      break ;
    }

    if (!sorted())
      break;

    Direction direction;

    // remove the used word from the string
    if (remain_string.length() == 0)
      break;
    remain_string = string(remain_string, word.length(), std::string::npos);
    String::remove_blanks(remain_string);

    // get the direction
    word = string(remain_string, 0, remain_string.find(' '));
    if (remain_string.find(' ') == string::npos)
      word = remain_string;
    if ((word == to_string(Direction::up))
        || (word == _(Direction::up))
        || (word == to_string(Direction::up) + ",")
        || (word == _(Direction::up) + ","))
      direction = Direction::up;
    else if ((word == to_string(Direction::down))
             || (word == _(Direction::down))
             || (word == to_string(Direction::down) + ",")
             || (word == _(Direction::down) + ","))
      direction = Direction::down;
    else {
      cerr << "Unknown direction \"" << word << "\"" << endl;
      break ;
    }

    // removed the used word from the string
    remain_string = string(remain_string, word.length(), std::string::npos);
    String::remove_blanks(remain_string);

    // set the position of the tcolor and the direction
    pos_[tcolor] = i;
    direction_[tcolor] = direction;
  } // for (i < Card::number_of_tcolors)

  ensure_validity();
}


Preferences::CardsOrder::CardsOrder(CardsOrder const& cards_order) = default; // NOLINT (false positive in cppcoreguidelines-pro-type-member-init)


auto Preferences::CardsOrder::operator=(CardsOrder const& cards_order) -> Preferences::CardsOrder&
{
  if (this == &cards_order)
    return *this;
  sorted_    = cards_order.sorted_;
  pos_       = cards_order.pos_;
  direction_ = cards_order.direction_;
  return *this;
}


auto Preferences::CardsOrder::ensure_validity() -> bool
{
  bool result = true;
  { // check for double position
    for (unsigned c = 0; c < Card::number_of_tcolors; c++) {
      if (   (tcolor(c) == Card::nocardcolor)
          || (pos(tcolor(c)) != c) ) {
        result = false;
        break;
      }
    }
    if (!result) {
      pos_[Card::trump]   = 0;
      pos_[Card::club]    = 1;
      pos_[Card::heart]   = 2;
      pos_[Card::spade]   = 3;
      pos_[Card::diamond] = 4;
      direction_[Card::trump]   = Direction::up;
      direction_[Card::club]    = Direction::down;
      direction_[Card::spade]   = Direction::down;
      direction_[Card::heart]   = Direction::down;
      direction_[Card::diamond] = Direction::down;
    }
  } // check for double position
  { // check directions
    if (direction(Card::trump) == Direction::none) {
      direction_set(Card::trump, Direction::up);
      result = false;
    }
    if (direction(Card::club) == Direction::none) {
      direction_set(Card::club, Direction::down);
      result = false;
    }
    if (direction(Card::spade) == Direction::none) {
      direction_set(Card::spade, Direction::down);
      result = false;
    }
    if (direction(Card::heart) == Direction::none) {
      direction_set(Card::heart, Direction::down);
      result = false;
    }
    if (direction(Card::diamond) == Direction::none) {
      direction_set(Card::diamond, Direction::down);
      result = false;
    }
  } // check directions
  return result;
} // bool Preferences::CardsOrder::ensure_validity()


Preferences::CardsOrder::operator bool() const
{
  return sorted_;
}


Preferences::CardsOrder::operator string() const
{
  string value;

  if (sorted_) {
    for (unsigned c = 0; c < Card::number_of_tcolors; c++) {
      value += to_string(tcolor(c)) + " " + to_string(direction(c));
      if (c + 1 < Card::number_of_tcolors)
        value += ", ";
    } // for (c < Card::number_of_tcolors))
  } else { // if !(sorted())
    value = "mixed";
  } // if !(sorted())

  return value;
}


auto Preferences::CardsOrder::operator==(CardsOrder const& order) const -> bool
{
  if (sorted() != order.sorted())
    return false;

  for(unsigned i = 0; i < Card::number_of_tcolors; i++)
    if ( (tcolor(i) != order.tcolor(i))
        || (direction(i) != order.direction(i)) )
      return false;

  return true;
}


auto Preferences::CardsOrder::sorted() const -> bool
{
  return sorted_;
}


auto Preferences::CardsOrder::mixed() const -> bool
{
  return !sorted_;
}


auto Preferences::CardsOrder::relative_position(HandCard const a, HandCard const b) const -> int
{
  if (pos(a.tcolor()) < pos(b.tcolor()))
    return -1;
  if (pos(a.tcolor()) > pos(b.tcolor()))
    return 1;

  // the two cards are the same color
  if (a == b)
    return 0;
  if (a.is_jabbed_by(b))
    return ((direction(a.tcolor()) == Direction::up) ? -1 :  1);
  else
    return ((direction(a.tcolor()) == Direction::up) ?  1 : -1);
}


auto Preferences::CardsOrder::direction(Card::TColor const tcolor) const -> Direction
{
  return direction_[tcolor];
}


auto Preferences::CardsOrder::direction(unsigned const pos_a) const -> Direction
{
  for (unsigned tcolor = 0; tcolor < Card::number_of_tcolors; tcolor++)
    if (pos_[tcolor] == pos_a) {
      return direction_[tcolor];
    }

  return Direction::none;
}


auto Preferences::CardsOrder::pos(const Card::TColor tcolor) const -> unsigned
{
  if (tcolor == Card::unknowncardcolor)
    return 101; // NOLINT
  return pos_[tcolor];
}


auto Preferences::CardsOrder::tcolor(unsigned const pos_a) const -> Card::TColor
{
  for (unsigned tcolor = 0; tcolor < pos_.size(); tcolor++)
    if (pos_[tcolor] == pos_a) {
      return Card::TColor(tcolor);
    }

  return Card::nocardcolor;
}


void Preferences::CardsOrder::direction_toggle(Card::TColor const tcolor)
{
  auto const old_value = *this;

  direction_[tcolor] = ( (direction(tcolor) == Direction::up)
                        ? Direction::down
                        : Direction::up);

  preferences_->signal_changed_(Type::cards_order, &old_value);
  preferences_->signal_changed(Type::cards_order)(old_value);
}


void Preferences::CardsOrder::direction_toggle(unsigned const pos_a)
{
  auto const old_value = *this;

  for (unsigned tcolor = 0; tcolor < pos_.size(); tcolor++)
    if (pos_[tcolor] == pos_a)
      direction_[tcolor] = ( (direction(pos_a) == Direction::up)
                            ? Direction::down
                            : Direction::up);

  preferences_->signal_changed_(Type::cards_order, &old_value);
  preferences_->signal_changed(Type::cards_order)(old_value);
}


void Preferences::CardsOrder::direction_set(Card::TColor const tcolor,
                                            Direction const direction)
{
  auto const old_value = *this;

  if (this->direction(tcolor) == direction)
    return ;

  direction_[tcolor] = direction;

  preferences_->signal_changed_(Type::cards_order, &old_value);
  preferences_->signal_changed(Type::cards_order)(old_value);
}


void Preferences::CardsOrder::direction_set(unsigned const pos,
                                            Direction const direction)
{
  direction_set(tcolor(pos), direction);
}


void Preferences::CardsOrder::pos_to_left(unsigned const pos_a)
{
  auto const old_value = *this;

  for (unsigned c = 0; c < Card::number_of_tcolors; c++) {
    if (pos(Card::TColor(c)) == pos_a)
      pos_[c] = (pos_a - 1 + Card::number_of_tcolors) % Card::number_of_tcolors;
    else if ( ((pos(Card::TColor(c)) + Card::number_of_tcolors)
               % Card::number_of_tcolors)
             == (pos_a - 1 + Card::number_of_tcolors) % Card::number_of_tcolors)
      pos_[c] = pos_a;
  }

  preferences_->signal_changed_(Type::cards_order, &old_value);
  preferences_->signal_changed(Type::cards_order)(old_value);
}


void Preferences::CardsOrder::pos_to_right(unsigned const pos_a)
{
  pos_to_left((pos_a + 1) % Card::number_of_tcolors);
}


void Preferences::CardsOrder::sorted_set(bool const sorted)
{
  if (sorted_ == sorted)
    return ;

  auto const old_value = *this;

  sorted_ = sorted;

  preferences_->signal_changed_(Type::cards_order, &old_value);
  preferences_->signal_changed(Type::cards_order)(old_value);
}


auto to_string(Preferences::CardsOrder::Direction const direction) -> string
{
  switch(direction) {
  case Preferences::CardsOrder::Direction::none:
    (void)_("Direction::none");
    return "none";
  case Preferences::CardsOrder::Direction::up:
    (void)_("Direction::up");
    return "up";
  case Preferences::CardsOrder::Direction::down:
    (void)_("Direction::down");
    return "down";
  }

  return "";
}


auto gettext(Preferences::CardsOrder::Direction const direction) -> string
{
  return gettext("Direction::" + to_string(direction));
}


void Preferences::CardsOrder::write(ostream& ostr) const
{
  if (sorted()) {
    ostr << tcolor(0) << " " << to_string(direction(0));
    for (unsigned i = 1; i < Card::number_of_tcolors; i++)
      ostr << ", " << tcolor(i) << " " << to_string(direction(i));
  } else { // if !(sorted())
    ostr << "mixed";
  } // if !(sorted())
}


auto operator<<(ostream& ostr, Preferences::CardsOrder const& cards_order) -> ostream&
{
  cards_order.write(ostr);
  return ostr;
}
