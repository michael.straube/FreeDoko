/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "preferences_type.h"

constexpr PreferencesType::Bool PreferencesType::bool_list[];
constexpr PreferencesType::Unsigned PreferencesType::unsigned_list[];
constexpr PreferencesType::String PreferencesType::string_list[];
constexpr PreferencesType::CardsOrder PreferencesType::cards_order_list[];


auto to_string(PreferencesType::Bool const type) -> string
{
  switch(type) {
  case PreferencesType::use_threads:
    (void)_("Preferences::use threads");
    return "use threads";
  case PreferencesType::automatic_savings:
    (void)_("Preferences::automatic savings");
    return "automatic savings";
  case PreferencesType::save_party_changes:
    (void)_("Preferences::save party changes");
    return "save party changes";
  case PreferencesType::additional_party_settings:
    (void)_("Preferences::additional party settings");
    return "additional party settings";
  case PreferencesType::show_splash_screen:
    (void)_("Preferences::show splash screen");
    return "show splash screen";
  case PreferencesType::splash_screen_transparent:
    (void)_("Preferences::splash screen transparent");
    return "splash screen transparent";
  case PreferencesType::show_bug_report_button_in_game_finished_window:
    (void)_("Preferences::show bug report button in game finished window");
    return "show bug report button in game finished window";
  case PreferencesType::save_bug_reports_on_desktop:
    (void)_("Preferences::save bug reports on desktop");
    return "save bug reports on desktop";
  case PreferencesType::sound:
    (void)_("Preferences::sound");
    return "sound";
  case PreferencesType::automatic_card_suggestion:
    (void)_("Preferences::automatic card suggestion");
    return "automatic card suggestion";
  case PreferencesType::announce_swines_automatically:
    (void)_("Preferences::announce swines automatically");
    return "announce swines automatically";
  case PreferencesType::shuffle_without_seed:
    (void)_("Preferences::shuffle without seed");
    return "shuffle without seed";
  case PreferencesType::replayed_game_with_random_distribution_of_players:
    (void)_("Preferences::replayed game with random distribution of players");
    return "replayed game with random distribution of players";
  case PreferencesType::show_if_valid:
    (void)_("Preferences::show if valid");
    return "show if valid";
  case PreferencesType::emphasize_valid_cards:
    (void)_("Preferences::emphasize valid cards");
    return "emphasize valid cards";
  case PreferencesType::announce_in_table:
    (void)_("Preferences::announce in table");
    return "announce in table";
  case PreferencesType::show_all_hands:
    (void)_("Preferences::show all hands");
    return "show all hands";
  case PreferencesType::show_all_tricks:
    (void)_("Preferences::show all tricks");
    return "show all tricks";
  case PreferencesType::show_ai_information_hands:
    (void)_("Preferences::show ai information hands");
    return "show ai information hands";
  case PreferencesType::show_ai_information_teams:
    (void)_("Preferences::show ai information teams");
    return "show ai information teams";
  case PreferencesType::show_trickpiles_points:
    (void)_("Preferences::show trickpiles points");
    return "show trickpiles points";
  case PreferencesType::show_known_teams_in_game:
    (void)_("Preferences::show known teams in game");
    return "show known teams in game";
  case PreferencesType::show_soloplayer_in_game:
    (void)_("Preferences::show soloplayer in game");
    return "show soloplayer in game";
  case PreferencesType::show_full_trick_window:
    (void)_("Preferences::show full trick window");
    return "show full trick window";
  case PreferencesType::show_full_trick_window_if_special_points:
    (void)_("Preferences::show full trick window if special points");
    return "show full trick window if special points";
  case PreferencesType::close_full_trick_automatically:
    (void)_("Preferences::close full trick automatically");
    return "close full trick automatically";
  case PreferencesType::show_gametype_window:
    (void)_("Preferences::show gametype window");
    return "show gametype window";
  case PreferencesType::close_gametype_window_automatically:
    (void)_("Preferences::close gametype window automatically");
    return "close gametype window automatically";
  case PreferencesType::show_marriage_window:
    (void)_("Preferences::show marriage window");
    return "show marriage window";
  case PreferencesType::close_marriage_window_automatically:
    (void)_("Preferences::close marriage window automatically");
    return "close marriage window automatically";
  case PreferencesType::show_announcement_window:
    (void)_("Preferences::show announcement window");
    return "show announcement window";
  case PreferencesType::close_announcement_window_automatically:
    (void)_("Preferences::close announcement window automatically");
    return "close announcement window automatically";
  case PreferencesType::show_swines_window:
    (void)_("Preferences::show swines window");
    return "show swines window";
  case PreferencesType::close_swines_window_automatically:
    (void)_("Preferences::close swines window automatically");
    return "close swines window automatically";
  case PreferencesType::rotate_trick_cards:
    (void)_("Preferences::rotate trick cards");
    return "rotate trick cards";
  case PreferencesType::own_hand_on_table_bottom:
    (void)_("Preferences::own hand on table bottom");
    return "own hand on table bottom";
  case PreferencesType::original_cards_size:
    (void)_("Preferences::original cards size");
    return "original cards size";
  case PreferencesType::ui_main_window_fullscreen:
    (void)_("Preferences::ui main window fullscreen");
    return "ui main window fullscreen";
  } // switch(type)

  return "";
}


auto gettext(PreferencesType::Bool const type) -> string
{
  return gettext("Preferences::" + to_string(type));
}


auto operator<<(ostream& ostr, PreferencesType::Bool type) -> ostream&
{
  ostr << to_string(type);
  return ostr;
}


auto to_string(PreferencesType::Unsigned const type) -> string
{
  switch(type) {
  case PreferencesType::max_attempts_sec_for_distribution:
    (void)_("Preferences::max attempts sec for distribution");
    return "max attempts sec for distribution";
  case PreferencesType::min_trumps_to_distribute:
    (void)_("Preferences::min trumps to distribute");
    return "min trumps to distribute";
  case PreferencesType::max_trumps_to_distribute:
    (void)_("Preferences::max trumps to distribute");
    return "max trumps to distribute";
  case PreferencesType::min_queens_to_distribute:
    (void)_("Preferences::min queens to distribute");
    return "min queens to distribute";
  case PreferencesType::max_queens_to_distribute:
    (void)_("Preferences::max queens to distribute");
    return "max queens to distribute";
  case PreferencesType::min_jacks_to_distribute:
    (void)_("Preferences::min club jacks to distribute");
    return "min club jacks to distribute";
  case PreferencesType::max_jacks_to_distribute:
    (void)_("Preferences::max club jacks to distribute");
    return "max club jacks to distribute";
  case PreferencesType::min_club_queens_to_distribute:
    (void)_("Preferences::min club queens to distribute");
    return "min club queens to distribute";
  case PreferencesType::max_club_queens_to_distribute:
    (void)_("Preferences::max club queens to distribute");
    return "max club queens to distribute";
  case PreferencesType::min_heart_tens_to_distribute:
    (void)_("Preferences::min heart tens to distribute");
    return "min heart tens to distribute";
  case PreferencesType::max_heart_tens_to_distribute:
    (void)_("Preferences::max heart tens to distribute");
    return "max heart tens to distribute";
  case PreferencesType::min_diamond_aces_to_distribute:
    (void)_("Preferences::min diamond aces to distribute");
    return "min diamond aces to distribute";
  case PreferencesType::max_diamond_aces_to_distribute:
    (void)_("Preferences::max diamond aces to distribute");
    return "max diamond aces to distribute";
  case PreferencesType::min_color_aces_to_distribute:
    (void)_("Preferences::min color aces to distribute");
    return "min color aces to distribute";
  case PreferencesType::max_color_aces_to_distribute:
    (void)_("Preferences::max color aces to distribute");
    return "max color aces to distribute";
  case PreferencesType::min_blank_colors_to_distribute:
    (void)_("Preferences::min blank colors to distribute");
    return "min blank colors to distribute";
  case PreferencesType::max_blank_colors_to_distribute:
    (void)_("Preferences::max blank colors to distribute");
    return "max blank colors to distribute";
  case PreferencesType::card_play_delay:
    (void)_("Preferences::card play delay");
    return "card play delay";
  case PreferencesType::full_trick_close_delay:
    (void)_("Preferences::full trick close delay");
    return "full trick close delay";
  case PreferencesType::gametype_window_close_delay:
    (void)_("Preferences::gametype window close delay");
    return "gametype window close delay";
  case PreferencesType::marriage_window_close_delay:
    (void)_("Preferences::marriage window close delay");
    return "marriage window close delay";
  case PreferencesType::announcement_window_close_delay:
    (void)_("Preferences::announcement window close delay");
    return "announcement window close delay";
  case PreferencesType::swines_window_close_delay:
    (void)_("Preferences::swines window close delay");
    return "swines window close delay";
  case PreferencesType::table_rotation:
    (void)_("Preferences::table rotation");
    return "table rotation";
  case PreferencesType::cards_height:
    (void)_("Preferences::cards height");
    return "cards height";
  case PreferencesType::ui_main_window_pos_x:
    (void)_("Preferences::ui main window pos x");
    return "ui main window pos x";
  case PreferencesType::ui_main_window_pos_y:
    (void)_("Preferences::ui main window pos y");
    return "ui main window pos y";
  case PreferencesType::ui_main_window_width:
    (void)_("Preferences::ui main window width");
    return "ui main window width";
  case PreferencesType::ui_main_window_height:
    (void)_("Preferences::ui main window height");
    return "ui main window height";
  } // switch(type)

  return "";
}


auto gettext(PreferencesType::Unsigned const type) -> string
{
  return gettext("Preferences::" + to_string(type));
}


auto operator<<(ostream& ostr, PreferencesType::Unsigned type) -> ostream&
{
  ostr << to_string(type);
  return ostr;
}


auto to_string(PreferencesType::String const type) -> string
{
  switch(type) {
  case PreferencesType::name:
    (void)_("Preferences::name");
    return "name";
  case PreferencesType::language:
    (void)_("Preferences::language");
    return "language";
  case PreferencesType::cardset:
    (void)_("Preferences::cardset");
    return "cardset";
  case PreferencesType::cards_back:
    (void)_("Preferences::cards back");
    return "cards back";
  case PreferencesType::iconset:
    (void)_("Preferences::iconset");
    return "iconset";
  case PreferencesType::background:
    (void)_("Preferences::background");
    return "background";
  case PreferencesType::name_font:
    (void)_("Preferences::name font");
    return "name font";
  case PreferencesType::name_font_color:
    (void)_("Preferences::name font color");
    return "name font color";
  case PreferencesType::name_active_font_color:
    (void)_("Preferences::name active font color");
    return "name active font color";
  case PreferencesType::name_reservation_font_color:
    (void)_("Preferences::name reservation font color");
    return "name reservation font color";
  case PreferencesType::trickpile_points_font:
    (void)_("Preferences::trickpile points font");
    return "trickpile points font";
  case PreferencesType::trickpile_points_font_color:
    (void)_("Preferences::trickpile points font color");
    return "trickpile points font color";
  case PreferencesType::poverty_shift_arrow_color:
    (void)_("Preferences::poverty shift arrow color");
    return "poverty shift arrow color";
#ifdef USE_SOUND_COMMAND
  case PreferencesType::play_sound_command:
    (void)_("Preferences::play sound command");
    return "play sound command";
#endif
  case PreferencesType::browser_command:
    (void)_("Preferences::browser command");
    return "browser command";
  case PreferencesType::cardset_license:
    (void)_("Preferences::cardset license");
    return "cardset license";
  case PreferencesType::iconset_license:
    (void)_("Preferences::iconset license");
    return "iconset license";
  } // switch(type)

  return "";
}


auto gettext(PreferencesType::String const type) -> string
{
  return gettext("Preferences::" + to_string(type));
}


auto operator<<(ostream& ostr, PreferencesType::String type) -> ostream&
{
  ostr << to_string(type);
  return ostr;
}


auto to_string(PreferencesPath const type) -> string
{
  switch(type) {
  case PreferencesPath::logo_file:
    (void)_("Preferences::logo file");
    return "logo file";
  case PreferencesPath::icon_file:
    (void)_("Preferences::icon file");
    return "icon file";
  case PreferencesPath::manual_directory:
    (void)_("Preferences::manual directory");
    return "manual directory";
  } // switch(type)

  return "";
}


auto gettext(PreferencesPath const type) -> string
{
  return gettext("Preferences::" + to_string(type));
}


auto operator<<(ostream& ostr, PreferencesPath const type) -> ostream&
{
  ostr << to_string(type);
  return ostr;
}


auto to_string(PreferencesType::CardsOrder const type) -> string
{
  switch(type) {
  case PreferencesType::cards_order:
    return "cards order";
    (void)_("Preferences::cards order");
  } // switch(type)

  return "";
}


auto gettext(PreferencesType::CardsOrder const type) -> string
{
  return gettext("Preferences::" + to_string(type));
}


auto operator<<(ostream& ostr, PreferencesType::CardsOrder type) -> ostream&
{ return (ostr << to_string(type)); }


auto to_string(CardsetFormat const format) -> string
{
  switch (format) {
  case CardsetFormat::unknown:
    (void)_("CardsetFormat::unknown");
    return "unknown";
  case CardsetFormat::freedoko:
    (void)_("CardsetFormat::FreeDoko");
    return "FreeDoko";
  case CardsetFormat::pysol:
    (void)_("CardsetFormat::pysol");
    return "pysol";
  case CardsetFormat::kde:
    (void)_("CardsetFormat::KDE");
    return "KDE";
  case CardsetFormat::gnome:
    (void)_("CardsetFormat::GNOME");
    return "GNOME";
  }
  return "";
}


auto gettext(CardsetFormat const format) -> string
{
  return gettext("CardsetFormat::" + to_string(format));
}


auto operator<<(ostream& ostr, CardsetFormat const format) -> ostream&
{
  ostr << to_string(format);
  return ostr;
}
