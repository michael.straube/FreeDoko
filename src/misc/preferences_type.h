/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

struct PreferencesType {
  ENUM_WITH_STATIC_LIST(Bool, bool_list, 0,
                        use_threads,
                        // save nothing (automatically)
                        automatic_savings,
                        save_party_changes,
                        additional_party_settings,
                        show_splash_screen,
                        splash_screen_transparent,
                        show_bug_report_button_in_game_finished_window,
                        save_bug_reports_on_desktop,
                        sound,
                        automatic_card_suggestion,
                        announce_swines_automatically,

                        shuffle_without_seed,
                        replayed_game_with_random_distribution_of_players,

                        show_if_valid,
                        emphasize_valid_cards,
                        announce_in_table,
                        show_all_hands,
                        show_all_tricks,
                        show_ai_information_hands,
                        show_ai_information_teams,
                        show_trickpiles_points,
                        show_known_teams_in_game,
                        show_soloplayer_in_game,

                        show_full_trick_window,
                        show_full_trick_window_if_special_points,
                        close_full_trick_automatically,

                        show_gametype_window,
                        close_gametype_window_automatically,
                        show_marriage_window,
                        close_marriage_window_automatically,
                        show_announcement_window,
                        close_announcement_window_automatically,
                        show_swines_window,
                        close_swines_window_automatically,

                        rotate_trick_cards,

                        own_hand_on_table_bottom,

                        original_cards_size,

                        ui_main_window_fullscreen,
                        );

  // cppcheck-suppress syntaxError
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,hicpp-avoid-c-arrays,modernize-avoid-c-arrays)
  ENUM_WITH_STATIC_LIST(Unsigned, unsigned_list, ui_main_window_fullscreen + 1,
                        max_attempts_sec_for_distribution,  // default:  2
                        min_trumps_to_distribute,       // default:  0
                        max_trumps_to_distribute,       // default: 12
                        min_queens_to_distribute,       // default:  0
                        max_queens_to_distribute,       // default:  8
                        min_jacks_to_distribute,        // default:  0
                        max_jacks_to_distribute,        // default:  8
                        min_club_queens_to_distribute,  // default:  0
                        max_club_queens_to_distribute,  // default:  2
                        min_heart_tens_to_distribute,   // default:  0
                        max_heart_tens_to_distribute,   // default:  2
                        min_diamond_aces_to_distribute, // default:  0
                        max_diamond_aces_to_distribute, // default:  2
                        min_color_aces_to_distribute,   // default:  0
                        max_color_aces_to_distribute,   // default:  6
                        min_blank_colors_to_distribute, // default:  0
                        max_blank_colors_to_distribute, // default:  3

                        card_play_delay,

                        full_trick_close_delay,

                        gametype_window_close_delay,
                        marriage_window_close_delay,
                        announcement_window_close_delay,
                        swines_window_close_delay,

                        table_rotation,

                        cards_height,

                        ui_main_window_pos_x,
                        ui_main_window_pos_y,
                        ui_main_window_width,
                        ui_main_window_height,
                        );

  ENUM_WITH_STATIC_LIST(String, string_list, ui_main_window_height + 1,
                        name,
                        language,
                        cardset,
                        cards_back,
                        iconset,
                        background,
                        name_font,
                        name_font_color,
                        name_active_font_color,
                        name_reservation_font_color,
                        trickpile_points_font,
                        trickpile_points_font_color,
                        poverty_shift_arrow_color,
#ifdef USE_SOUND_COMMAND
                        play_sound_command,
#endif
                        browser_command,
                        cardset_license,
                        iconset_license,
                       );
  ENUM_WITH_STATIC_LIST(CardsOrder, cards_order_list, iconset_license + 1,
                        cards_order);
}; // struct Type


enum class PreferencesPath {
  logo_file,
  icon_file,
  manual_directory,
};

enum class CardsetFormat {
  unknown,
  freedoko,
  pysol,
  kde,
  gnome,
};

// conversion in a string
auto to_string(PreferencesType::Bool type) -> string;
auto gettext(PreferencesType::Bool type)   -> string;
auto operator<<(ostream& ostr, PreferencesType::Bool type) -> ostream&;

auto to_string(PreferencesType::Unsigned type) -> string;
auto gettext(PreferencesType::Unsigned type)   -> string;
auto operator<<(ostream& ostr, PreferencesType::Unsigned type) -> ostream&;

auto to_string(PreferencesType::String type) -> string;
auto gettext(PreferencesType::String type)   -> string;
auto operator<<(ostream& ostr, PreferencesType::String type) -> ostream&;

auto to_string(PreferencesType::CardsOrder type) -> string;
auto gettext(PreferencesType::CardsOrder type)   -> string;
auto operator<<(ostream& ostr, PreferencesType::CardsOrder type) -> ostream&;

auto to_string(PreferencesPath type) -> string;
auto gettext(PreferencesPath type)   -> string;
auto operator<<(ostream& ostr, PreferencesPath type) -> ostream&;

auto to_string(CardsetFormat format) -> string;
auto gettext(CardsetFormat format)   -> string;
auto operator<<(ostream& ostr, CardsetFormat format) -> ostream&;
