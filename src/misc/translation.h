/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <libintl.h>
#include <clocale>

void translation_init();
auto get_language() -> string;
void set_language(string language);

auto gettext(string text) -> string;

// no argument
auto _(string const& text) -> string; // NOLINT(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
template<typename T>
inline auto _(T const& t) -> string // NOLINT(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
{ return std::string(gettext(t)); }

// replace substrings (by %)
auto replace_substring(string const& text, string const& s) -> string;
auto replace_substring(string const& text, int i) -> string;
auto replace_substring(string const& text, double d) -> string;
auto replace_substring(string const& text, uint32_t u) -> string;
#ifdef OSX
auto replace_substring(string const& text, unsigned long u) -> string;
#else
auto replace_substring(string const& text, uint64_t u) -> string;
#endif
auto replace_substring(string const& text, string const& replacement,
                       char c1, char c2 = '\0') -> string;
auto replace_substrings(string const& text) -> string;
template<typename T, typename... Args>
inline auto replace_substrings(string const& text, T t, Args... args) -> string
{ return replace_substrings(replace_substring(text, t), args...); }

// multible arguments
template<typename... Args>
inline auto _(string const& text, Args... args) -> string // NOLINT(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp,misc-no-recursion)
{ return replace_substrings(_(text), args...); }
