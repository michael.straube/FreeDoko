/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef BUG_REPORT

#include "auto_bug_report.h"
#include "../misc/bug_report.h"

#include "../party/party.h"
#include "../game/game_summary.h"
#include "../game/game.h"
#include "../card/trick.h"
#include "../player/ai/ai.h"

#include "../utils/string.h"

namespace OS_NS {

AutoBugReport::AutoBugReport()
{
  Game::signal_open.connect(*this, &AutoBugReport::game_open, disconnector_);
}

AutoBugReport::~AutoBugReport() = default;

void AutoBugReport::add_condition(string const condition)
{
  if (condition == "all") {
    conditions_.insert("dulle lost");
    conditions_.insert("fox lost");
    conditions_.insert("swine lost");
    conditions_.insert("poverty lost");
    conditions_.insert("solo lost");
    conditions_.insert("bad announcement");
    conditions_.insert("no announcement");
    return;
  }
  conditions_.insert(condition);
}

void AutoBugReport::game_open(Game const& game)
{
  {
    auto& game = ::party->game();
    game.signal_start              .connect(*this, &AutoBugReport::game_start,  disconnector_);
    game.tricks().signal_trick_open.connect(*this, &AutoBugReport::trick_open,  disconnector_);
    game.signal_card_played        .connect(*this, &AutoBugReport::card_played, disconnector_);
    game.tricks().signal_trick_full.connect(*this, &AutoBugReport::trick_full,  disconnector_);
    game.signal_finish             .connect(*this, &AutoBugReport::game_finish, disconnector_);
  }
  if (conditions_.count("game open")) {
    create_report("game open");
  }
}

void AutoBugReport::game_start()
{
  if (conditions_.count("game")) {
    create_report("game start");
  }
}

void AutoBugReport::trick_open(Trick const& trick)
{
  if (conditions_.count("trick")) {
    create_report("trick open:\n" + String::to_string(trick));
  }
}

void AutoBugReport::card_played(HandCard const& card)
{
  if (conditions_.count("card")) {
    create_report("card played: " + to_string(card));
  }
  if (   conditions_.count("no heuristic")
      && dynamic_cast<Ai const*>(&card.player())
      && dynamic_cast<Ai const&>(card.player()).last_heuristic() == Aiconfig::Heuristic::gametree) {
    create_report("no heuristic found for " + card.player().name());
  }
}

void AutoBugReport::trick_full(Trick const& trick)
{
  for (unsigned c = 0; c < trick.actcardno(); ++c) {
    auto const& card = trick.card(c);
    auto const& player = trick.player_of_card(c);

    if (player != trick.winnerplayer()) {
      if (   conditions_.count("dulle lost")
          && card.isdulle()) {
        create_report("dulle lost:\n" + String::to_string(trick));
      }
      if (   conditions_.count("fox lost")
          && card.isfox()
          && (player.team() != trick.winnerplayer().team()) ) {
        create_report("fox lost:\n" + String::to_string(trick));
      }
      if (   conditions_.count("swine lost")
          && card.isswine()) {
        create_report("swine lost:\n" + String::to_string(trick));
      }
    } // if (player != trick.winnerplayer())
  } // for (c)
}

void AutoBugReport::game_finish()
{
  auto const& game_summary = ::party->last_game_summary();
  { // lost poverty
    if (   conditions_.count("poverty lost")
        && game_summary.game_type() == GameType::poverty
        && game_summary.winnerteam() != Team::re)
      create_report("poverty lost by re"
                          " ("
                          + String::to_string(game_summary.trick_points(Team::re))
                          + ")");
  } // lost poverty
  { // lost solo
    if (  conditions_.count("solo lost")
        && is_solo(game_summary.game_type())
        && (game_summary.winnerteam() != Team::re)
        && (game_summary.trick_points(Team::re) < 90) )
      create_report(to_string(game_summary.game_type()) + " lost by re"
                          + " ("
                          + String::to_string(game_summary.trick_points(Team::re))
                          + ")");
  } // lost solo

  { // bad announcement
    if (  conditions_.count("bad announcement")
        && game_summary.highest_announcement_re() != Announcement::noannouncement
        && (game_summary.trick_points(Team::re)
            < (needed_points(game_summary.highest_announcement_re())
               - 2 * 30) )
       )
      create_report("bad annoucement for re: "
                          + to_string(game_summary.highest_announcement_re())
                          + " ("
                          + String::to_string(game_summary.trick_points(Team::contra))
                          + ")");
    if (  conditions_.count("no announcement")
        && (game_summary.highest_announcement_re()
            == Announcement::noannouncement)
        && (game_summary.trick_points(Team::re)
            > (needed_points(Announcement::no120))
            + 2 * 30)
       )
      create_report("no annoucement for re: "
                          + String::to_string(game_summary.trick_points(Team::contra))
                          + " points for contra");

    if (  conditions_.count("bad announcement")
        && game_summary.highest_announcement_contra() != Announcement::noannouncement
        && (game_summary.trick_points(Team::contra)
            < (needed_points(game_summary.highest_announcement_contra())
               - 2 * 30) )
       )
      create_report("bad annoucement for contra: "
                          + to_string(game_summary.highest_announcement_contra())
                          + " ("
                          + String::to_string(game_summary.trick_points(Team::re))
                          + ")");
    if (  conditions_.count("no announcement")
        && (game_summary.highest_announcement_contra()
            == Announcement::noannouncement)
        && (game_summary.trick_points(Team::contra)
            > (needed_points(Announcement::no120))
            + 2 * 30)
       )
      create_report("no annoucement for contra: "
                          + String::to_string(game_summary.trick_points(Team::re))
                          + " points for re");

    if (   (game_summary.game_type() == GameType::poverty)
        && (game_summary.winnerteam() != Team::re) ) {
    } // if (bad announcement)
  } // bad announcement
}

void AutoBugReport::create_report(string const& text) const
{
  cout << "automatic bug report creation\n"
    << "  " << text << '\n';

  auto const path = BugReport::create_bug_report(*::party, text, "automatic");

  cout << "  Path: " << path << '\n'
    << '\n';
}

} // namespace OS_NS

#endif
