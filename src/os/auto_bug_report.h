/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef BUG_REPORT
class Game;
class Trick;
class HandCard;

namespace OS_NS {

/**
 ** create bug reports automatically
 **/
class AutoBugReport {
public:
  AutoBugReport();
  AutoBugReport(AutoBugReport const&) = delete;
  AutoBugReport& operator=(AutoBugReport const&) = delete;
  ~AutoBugReport();

  void add_condition(string condition);

  void game_open(Game const& game);
  void game_start();
  void trick_open(Trick const& trick);
  void card_played(HandCard const& card);
  void trick_full(Trick const& trick);
  void game_finish();

private:
  void create_report(string const& text) const;

private:
  AutoDisconnector disconnector_;
  set<string> conditions_;
};

} // namespace OS_NS
#endif
