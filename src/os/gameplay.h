/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/announcement.h"
class Party;
class Game;
struct Reservation;
class Trick;
class HandCard;
class Player;

namespace OS_NS {

/// The default Gameplay
class Gameplay {
public:
  explicit Gameplay(ostream* ostr = &cout);
  Gameplay(Gameplay const&) = delete;
  Gameplay& operator=(Gameplay const&) = delete;

  ~Gameplay();

  void party_open(Party const& party);
  void party_start();
  void party_finish();
  void party_close();

  void game_open(Game const& game);
  void game_cards_distributed();
  void game_redistribute();
  void reservation_got(Reservation const& reservation, Player const& player);
  void game_start();
  void game_finish();
  void game_close();

  void trick_open(Trick const& trick);
  void trick_full(Trick const& trick);
  void card_played(HandCard card);
  void announcement_made(Announcement announcement, Player const& player);
  void swines_announced(Player const& player);
  void hyperswines_announced(Player const& player);

  void poverty_shift(Player const& player, unsigned cardno);
  void poverty_take_denied_by_all();
  void poverty_take_accepted(Player const& player, unsigned cardno, unsigned trumpno);

  void marriage(Player const& bridegroom, Player const& bride);
private:
  ostream* ostr = {};
  AutoDisconnector disconnector_;

  Party const* party_ = {};
  Game const* game_ = {};
}; // class Gameplay

} // namespace OS_NS
