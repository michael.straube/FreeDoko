/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "seed.h"

#include "../party/party.h"
#include "../game/game.h"
#include "../player/player.h"

#include <chrono>
#include <ctime>
#include <iomanip>

namespace OS_NS {

Seed::Seed(ostream& ostr) :
  ostr(ostr)
{
  Game::signal_open.connect(*this, &Seed::game_open, disconnector_);
}


Seed::~Seed() = default;


void Seed::game_open(Game& game)
{
  game.signal_start.connect(*this, &Seed::game_start, disconnector_);

  auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  ostr << "seed: " << setw(8) << game.seed()
    << " (" << game.startplayer().no() << ")"
    << "\t"
    << ctime(&now)
    << '\n';
}


void Seed::game_start()
{
  auto const& game = ::party->game();
  ostr << "  " << game.type();
  if (is_solo(game.type()))
    ostr << "\t"
      << "(soloplayer: "
      << game.players().soloplayer().no()
      << ")";
  ostr << '\n';
}

} // namespace OS_NS
