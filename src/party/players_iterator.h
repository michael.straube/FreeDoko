/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

// included by players.h

#pragma once

class PartyPlayers;

class PartyPlayersIterator {
  friend class PartyPlayers;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = PartyPlayer;
  using difference_type = std::ptrdiff_t;
  using pointer         = PartyPlayer*;
  using reference       = PartyPlayer&;

private:
  PartyPlayersIterator(PartyPlayers& players, unsigned p);

public:
  auto operator*()        -> PartyPlayer&;
  auto operator*()  const -> PartyPlayer const&;
  auto operator->()       -> PartyPlayer*;
  auto operator->() const -> PartyPlayer const*;
  auto operator++()       -> PartyPlayersIterator&;
  auto operator!=(PartyPlayersIterator const& i) const -> bool;
private:
  PartyPlayers* const players_;
  unsigned p_;
};

class PartyPlayersConstIterator {
  friend class PartyPlayers;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = PartyPlayer;
  using difference_type = std::ptrdiff_t;
  using pointer         = PartyPlayer*;
  using reference       = PartyPlayer&;

private:
  PartyPlayersConstIterator(PartyPlayers const& players, unsigned p);

public:
  auto operator*()  const -> PartyPlayer const&;
  auto operator->() const -> PartyPlayer const*;
  auto operator++()       -> PartyPlayersConstIterator&;
  auto operator!=(PartyPlayersConstIterator const& i) const -> bool;
private:
  PartyPlayers const* const players_;
  unsigned p_;
};
