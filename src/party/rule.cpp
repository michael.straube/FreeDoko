/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "rule.h"

#include "../utils/file.h"
#include "../utils/random.h"

#include "party.h"
#include "../versions.h"
#include "../game/game.h"
#include "../ui/ui.h"
#include "../misc/preferences.h"
#include "../class/readconfig/readconfig.h"
#include "../utils/string.h"


Rule const tournament_rule;

Rule::Rule() :
  bool_(Type::bool_number),
  unsigned_(Type::unsigned_number)
{
  reset_to_tournament();
}

Rule::Rule(Rule const& rule) :
  bool_(rule.bool_),
  unsigned_(rule.unsigned_),
  counting_(rule.counting_)
{ }

auto Rule::operator=(Rule const& rule) -> Rule&
{
  if (this == &rule)
    return *this;
  bool_     = rule.bool_;
  unsigned_ = rule.unsigned_;
  counting_ = rule.counting_;
  return *this;
}

void Rule::reset_to_tournament()
{
  // first set to the ddv rules, then change according to the ruleset
  //
  set(Type::mutate,            false);

  set(Type::with_nines,        true);

  set(Type::bock,              false);
  set(Type::bock_multiplier,       2);
  set(Type::bock_append,       false);
  set(Type::bock_120,          false);
  set(Type::bock_solo_lost,    false);
  set(Type::bock_re_lost,      false);
  set(Type::bock_heart_trick,  false);
  set(Type::bock_black,        false);

  set(Type::throw_with_nines,                       false);
  set(Type::min_number_of_throwing_nines,               5);
  set(Type::throw_with_kings,                       false);
  set(Type::min_number_of_throwing_kings,               5);
  set(Type::throw_with_nines_and_kings,             false);
  set(Type::min_number_of_throwing_nines_and_kings,     7);
  set(Type::throw_with_richness,                    false);
  set(Type::min_richness_for_throwing,                 70);

  set(Type::poverty,                      false);
  set(Type::poverty_shift,                true);
  set(Type::poverty_shift_only_trump,     false);
  set(Type::poverty_fox_do_not_count,     false);
  set(Type::poverty_fox_shift_extra,      false);
  set(Type::poverty_fox_shift_open,       false);
  set(Type::throw_with_one_trump,         false);

  set(Type::throw_when_fox_highest_trump, false);

  set(Type::marriage_determination,         3);
  set(Type::marriage_first_foreign,      true);
  set(Type::marriage_first_color,        false);
  set(Type::marriage_first_single_color, false);
  set(Type::marriage_first_trump,        false);
  set(Type::marriage_first_one_decides,  false);
  set(Type::marriage_before_poverty,     false);

  set(Type::solo,                 true);
  set(Type::solo_jack,            true);
  set(Type::solo_queen,           true);
  set(Type::solo_king,            false);
  set(Type::solo_queen_jack,      false);
  set(Type::solo_king_jack,       false);
  set(Type::solo_king_queen,      false);
  set(Type::solo_koehler,         false);
  set(Type::solo_color,           true);
  set(Type::solo_meatless,        true);

  set(Type::throwing_before_solo, false);

  set(Type::dullen,                               true);
  set(Type::dullen_second_over_first,             false);
  set(Type::dullen_contrary_in_last_trick,        false);
  set(Type::dullen_first_over_second_with_swines, false);

  set(Type::swines,                         false);
  set(Type::swines_announcement_begin,      true);
  set(Type::swine_only_second,              false);
  set(Type::swines_in_solo,                 true);
  set(Type::swines_in_poverty,              true);
  set(Type::hyperswines,                    false);
  set(Type::hyperswines_announcement_begin, true);
  set(Type::hyperswines_in_solo,            true);
  set(Type::hyperswines_in_poverty,         true);
  set(Type::swines_and_hyperswines_joint,   true);

  set(Type::extrapoint_catch_fox,             true);
  set(Type::extrapoint_catch_fox_last_trick,  false);
  set(Type::extrapoint_fox_last_trick,        false);
  set(Type::extrapoint_double_fox_last_trick, false);

  set(Type::extrapoint_charlie,                               true);
  set(Type::extrapoint_catch_charlie,                         false);
  set(Type::extrapoint_double_charlie,                        false);
  set(Type::extrapoint_catch_double_charlie,                  false);
  set(Type::extrapoint_catch_charlie_only_with_diamond_queen, false);

  set(Type::extrapoint_dulle_jabs_dulle,                      false);
  set(Type::extrapoint_heart_trick,                           false);

  set(Type::knocking,                                              false);
  set(Type::announcement_individual_limits,                        false);
  set(Type::announcement_limit_only_for_current,                   false);
  set(Type::announcement_till_full_trick,                          false);
  set(Type::announcement_first_trick_thirty_points,                false);
  set(Type::announcement_first_trick_thirty_points_only_first,     false);
  set(Type::announcement_first_trick_thirty_points_only_re_contra, true);
  set(Type::announcement_first_trick_thirty_points_in_marriage,    true);
  set(Type::announcement_no_120,                                     11);
  set(Type::announcement_no_90,                                      10);
  set(Type::announcement_no_60,                                       9);
  set(Type::announcement_no_30,                                       8);
  set(Type::announcement_no_0,                                        7);
  set(Type::announcement_re_doubles,                               false);
  set(Type::announcement_contra_doubles_against_re,                false);

  set(Type::number_of_rounds_limited,   true);
  set(Type::number_of_rounds,              20);
  set(Type::points_limited,             false);
  set(Type::points,                        40);
  set(Type::number_of_duty_soli,            0);
  set(Type::number_of_duty_color_soli,      0);
  set(Type::number_of_duty_picture_soli,    0);
  set(Type::offer_duty_solo,             true);
  set(Type::lustsolo_player_leads,       false);
  set(Type::solo_always_counts_triple,   false);

  set(Type::number_of_players,              4);
  set(Type::number_of_players_in_game,      4);

  set(Type::counting, Counting::plusminus);
}

void Rule::reset_to_seed_statistics()
{
  reset_to_tournament();

  set(Type::throw_with_nines,                      true);
  set(Type::min_number_of_throwing_nines,              5);
  set(Type::throw_with_kings,                      true);
  set(Type::min_number_of_throwing_kings,              5);
  set(Type::throw_with_nines_and_kings,            false);
  set(Type::min_number_of_throwing_nines_and_kings,    7);
  set(Type::throw_with_richness,                   false);
  set(Type::min_richness_for_throwing,                70);

  set(Type::poverty,                      true);
  set(Type::poverty_shift,                true);
  set(Type::poverty_fox_do_not_count,     true);
  set(Type::poverty_fox_shift_extra,      true);
  set(Type::throw_when_fox_highest_trump, true);

  set(Type::solo,            true);
  set(Type::solo_jack,       true);
  set(Type::solo_queen,      true);
  set(Type::solo_king,       true);
  set(Type::solo_queen_jack, true);
  set(Type::solo_king_jack,  true);
  set(Type::solo_king_queen, true);
  set(Type::solo_koehler,    true);
  set(Type::solo_color,      true);
  set(Type::solo_meatless,   true);

  set(Type::throwing_before_solo, false);

  set(Type::swines,      true);
  set(Type::hyperswines, true);
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Rule::operator()(Type::Bool const type) const -> bool
{
  return (dependencies(type) && value(type));
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Rule::operator()(Type::Unsigned const type) const -> unsigned
{
  switch(type) { // NOLINT(hicpp-multiway-paths-covered)
  case Type::number_of_duty_soli:
    if (dependencies(type))
      return value(type);
    else
      return 0;
  case Type::announcement_no_120:
  case Type::announcement_no_90:
  case Type::announcement_no_60:
  case Type::announcement_no_30:
  case Type::announcement_no_0:
    if ((*this)(Type::announcement_individual_limits)) {
      return value(type);
    } else {
      if ((*this)(Type::with_nines)) {
        switch(type) {
        case Type::announcement_no_120: return 11;
        case Type::announcement_no_90:  return 10;
        case Type::announcement_no_60:  return  9;
        case Type::announcement_no_30:  return  8;
        case Type::announcement_no_0:   return  7;
        default:                        return value(type);
        }
      } else {
        switch(type) {
        case Type::announcement_no_120: return 9;
        case Type::announcement_no_90:  return 8;
        case Type::announcement_no_60:  return 7;
        case Type::announcement_no_30:  return 6;
        case Type::announcement_no_0:   return 5;
        default:                        return value(type);
        }
      }
    }
  default:
    return value(type);
  } // switch(type)

  DEBUG_ASSERT(false);
  return 0;
}

// NOLINTNEXTLINE(misc-no-recursion)
auto Rule::operator()(Type::UnsignedExtra const type) const -> unsigned
{
  switch(type) {
  case Type::number_of_same_cards:
    return ((*this)(Type::number_of_players_in_game) / 2);
  case Type::number_of_card_colors:
    return Card::number_of_colors;
  case Type::number_of_card_tcolors:
    return Card::number_of_tcolors;
  case Type::number_of_card_values:
    return Card::number_of_values + ((*this)(Type::with_nines) ? 0 : -1);
  case Type::number_of_cards_per_value:
    return ((*this)(Type::number_of_same_cards) * (*this)(Type::number_of_card_colors));
  case Type::number_of_cards:
    return ((*this)(Type::number_of_cards_per_value) * (*this)(Type::number_of_card_values));
  case Type::number_of_teams:
    return 2;
  case Type::number_of_players_per_team:
    return (*this)(Type::number_of_players_in_game) / (*this)(Type::number_of_teams);
  case Type::number_of_games_per_round:
    return (*this)(Type::number_of_players);
  case Type::max_number_of_tricks_in_game:
    return (*this)(Type::number_of_same_cards) * Card::number_of_values * Card::number_of_colors / 4;
  case Type::number_of_tricks_in_game:
    return (*this)(Type::number_of_cards) / (*this)(Type::number_of_players_in_game);
  case Type::points_for_doppelkopf:
    return 40;
  case Type::max_number_of_poverty_trumps:
    return 3;
  } // switch(type)

  DEBUG_ASSERTION(false,
                  "Rule::operator()(" << type << ")\n"
                  "  type = " << type << " not in switch");
  return 0;
}

auto Rule::operator()(Type::Counting const type) const -> Counting
{
  return counting();
}

auto Rule::operator()(GameType const game_type) const -> bool
{
  switch(game_type) {
  case GameType::normal:
    return true;
  case GameType::thrown_nines:
    return (*this)(Type::throw_with_nines);
  case GameType::thrown_kings:
    return (*this)(Type::throw_with_kings);
  case GameType::thrown_nines_and_kings:
    return (*this)(Type::throw_with_nines_and_kings);
  case GameType::thrown_richness:
    return (*this)(Type::throw_with_richness);
  case GameType::redistribute:
    return true;
  case GameType::fox_highest_trump:
    return (*this)(Type::throw_when_fox_highest_trump);
  case GameType::poverty:
    return (*this)(Type::poverty);
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    return true;
  case GameType::solo_jack:
    return (*this)(Type::solo_jack);
  case GameType::solo_queen:
    return (*this)(Type::solo_queen);
  case GameType::solo_king:
    return (*this)(Type::solo_king);
  case GameType::solo_queen_jack:
    return (*this)(Type::solo_queen_jack);
  case GameType::solo_king_jack:
    return (*this)(Type::solo_king_jack);
  case GameType::solo_king_queen:
    return (*this)(Type::solo_king_queen);
  case GameType::solo_koehler:
    return (*this)(Type::solo_koehler);
  case GameType::solo_club:
  case GameType::solo_spade:
  case GameType::solo_heart:
  case GameType::solo_diamond:
    return (*this)(Type::solo_color);
  case GameType::solo_meatless:
    return (*this)(Type::solo_meatless);
  } // switch(game_type)

  DEBUG_ASSERT(false);
  return false;
}

auto Rule::operator()(MarriageSelector const marriage_selector) const -> bool
{
  switch(marriage_selector) {
  case MarriageSelector::silent:
  case MarriageSelector::team_set:
    return false;
  case MarriageSelector::first_foreign:
    return (*this)(Type::marriage_first_foreign);
  case MarriageSelector::first_trump:
    return (*this)(Type::marriage_first_trump);
  case MarriageSelector::first_color:
    return (*this)(Type::marriage_first_color);
  case MarriageSelector::first_club:
  case MarriageSelector::first_spade:
  case MarriageSelector::first_heart:
    return (*this)(Type::marriage_first_single_color);
  } // switch(marriage_selector)

  DEBUG_ASSERT(false);
  return false;
}

auto Rule::operator()(Specialpoint::Type const special_point) const -> bool
{
  switch(special_point) {
  case Specialpoint::Type::nospecialpoint:
    return true;
  case Specialpoint::Type::caught_fox:
    return (*this)(Type::extrapoint_catch_fox);
  case Specialpoint::Type::caught_fox_last_trick:
    return (*this)(Type::extrapoint_catch_fox_last_trick);
  case Specialpoint::Type::fox_last_trick:
    return (*this)(Type::extrapoint_fox_last_trick);
  case Specialpoint::Type::charlie:
    return (*this)(Type::extrapoint_charlie);
  case Specialpoint::Type::caught_charlie:
    return (*this)(Type::extrapoint_catch_charlie);
  case Specialpoint::Type::dulle_caught_dulle:
    return (*this)(Type::extrapoint_dulle_jabs_dulle);
  case Specialpoint::Type::heart_trick:
    return (*this)(Type::extrapoint_heart_trick);
  case Specialpoint::Type::doppelkopf:
    return true;

  case Specialpoint::Type::won:
  case Specialpoint::Type::no90:
  case Specialpoint::Type::no60:
  case Specialpoint::Type::no30:
  case Specialpoint::Type::no0:
  case Specialpoint::Type::no120_said:
  case Specialpoint::Type::no90_said:
  case Specialpoint::Type::no60_said:
  case Specialpoint::Type::no30_said:
  case Specialpoint::Type::no0_said:
  case Specialpoint::Type::no90_said_120_got:
  case Specialpoint::Type::no60_said_90_got:
  case Specialpoint::Type::no30_said_60_got:
  case Specialpoint::Type::no0_said_30_got:
  case Specialpoint::Type::announcement_reply:
  case Specialpoint::Type::contra_won:
  case Specialpoint::Type::solo:
    return true;

  case Specialpoint::Type::bock:
    return (*this)(Type::bock);
  } // switch(special_point)

  DEBUG_ASSERT(false);
  return false;
}

auto Rule::remaining_cards(Announcement const announcement) const -> unsigned
{
  switch(announcement) {
  case Announcement::noannouncement:
  case Announcement::no120:
    return std::min((*this)(Type::number_of_tricks_in_game), (*this)(Type::announcement_no_120));
  case Announcement::no90:
    return std::min((*this)(Type::number_of_tricks_in_game), (*this)(Type::announcement_no_90));
  case Announcement::no60:
    return std::min((*this)(Type::number_of_tricks_in_game), (*this)(Type::announcement_no_60));
  case Announcement::no30:
    return std::min((*this)(Type::number_of_tricks_in_game), (*this)(Type::announcement_no_30));
  case Announcement::no0:
    return std::min((*this)(Type::number_of_tricks_in_game), (*this)(Type::announcement_no_0));
  case Announcement::reply:
    return (*this)(Type::number_of_tricks_in_game);
  } // switch(announcement)

  DEBUG_ASSERT(false);
  return 0;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Rule::dependencies(Type::Bool const type) const -> bool
{
  switch(type) {
  case Type::mutate:
#ifdef RELEASE
    return false;
#else
    return true;
#endif

  case Type::with_nines:
    return true;

  case Type::bock:
    return true;
  case Type::bock_append:
    return (*this)(Type::bock);
  case Type::bock_120:
    return (*this)(Type::bock);
  case Type::bock_solo_lost:
    return (*this)(Type::bock);
  case Type::bock_re_lost:
    return (   (*this)(Type::bock)
            && !(*this)(Type::knocking));
  case Type::bock_heart_trick:
    return (*this)(Type::bock);
  case Type::bock_black:
    return (*this)(Type::bock);

  case Type::throw_with_nines:
    return (*this)(Type::with_nines);
  case Type::throw_with_kings:
    return true;
  case Type::throw_with_nines_and_kings:
    return (*this)(Type::with_nines);
  case Type::throw_with_richness:
    return true;

  case Type::poverty:
    return true;
  case Type::poverty_shift:
    return (*this)(Type::poverty);
  case Type::poverty_shift_only_trump:
  case Type::poverty_fox_shift_open:
  case Type::throw_with_one_trump:
    return (*this)(Type::poverty_shift);

  case Type::poverty_fox_do_not_count:
    return (*this)(Type::poverty);

  case Type::poverty_fox_shift_extra:
    return (   (*this)(Type::poverty_fox_do_not_count)
            && !(*this)(Type::poverty_shift_only_trump) );

  case Type::throw_when_fox_highest_trump:
    return true;

  case Type::marriage_first_foreign:
  case Type::marriage_first_color:
  case Type::marriage_first_single_color:
  case Type::marriage_first_trump:
    return true;
  case Type::marriage_first_one_decides:
    return true;
  case Type::marriage_before_poverty:
    return (*this)(Type::poverty);

  case Type::solo:
    return true;
  case Type::solo_jack:
  case Type::solo_queen:
  case Type::solo_king:
  case Type::solo_queen_jack:
  case Type::solo_king_jack:
  case Type::solo_king_queen:
  case Type::solo_koehler:
  case Type::solo_color:
  case Type::solo_meatless:
    return (*this)(Type::solo);

  case Type::throwing_before_solo:
    return (   (*this)(Type::solo)
            && (   (*this)(Type::throw_with_nines)
                || (*this)(Type::throw_with_kings)
                || (*this)(Type::throw_with_nines_and_kings)
                || (*this)(Type::throw_with_richness)
                || (*this)(Type::throw_with_one_trump)
                || (*this)(Type::throw_when_fox_highest_trump)
                || (    (*this)(Type::poverty)
                    && !(*this)(Type::poverty_shift) )
               ) );

  case Type::dullen:
    return true;
  case Type::dullen_second_over_first:
    return (*this)(Type::dullen);
  case Type::dullen_contrary_in_last_trick:
    return (*this)(Type::dullen);
  case Type::dullen_first_over_second_with_swines:
    return ((*this)(Type::dullen)
            && (*this)(Type::swines)
            && ( (*this)(Type::dullen_second_over_first)
                || (*this)(Type::dullen_contrary_in_last_trick)));

  case Type::swines:
    return true;
  case Type::swines_announcement_begin:
    return ((*this)(Type::swines)
            && !(value(Type::swine_only_second)));
  case Type::swine_only_second:
    return ((*this)(Type::swines)
            && !(value(Type::swines_announcement_begin))
            && !(value(Type::hyperswines)));
  case Type::swines_in_solo:
    return ((*this)(Type::swines)
            && (*this)(Type::solo_color));
  case Type::swines_in_poverty:
    return ((*this)(Type::swines)
            && (*this)(Type::poverty_shift));
  case Type::hyperswines:
    return ((*this)(Type::swines)
            && !(*this)(Type::swine_only_second));
  case Type::hyperswines_announcement_begin:
    return ((*this)(Type::hyperswines)
            && (*this)(Type::swines_announcement_begin));
  case Type::hyperswines_in_solo:
    return ((*this)(Type::hyperswines)
            && (*this)(Type::swines_in_solo));
  case Type::hyperswines_in_poverty:
    return ((*this)(Type::hyperswines)
            && (*this)(Type::swines_in_poverty));
  case Type::swines_and_hyperswines_joint:
    return (*this)(Type::hyperswines);

  case Type::extrapoint_catch_fox:
    return true;

  case Type::extrapoint_catch_fox_last_trick:
    return (*this)(Type::extrapoint_catch_fox);

  case Type::extrapoint_fox_last_trick:
    return true;

  case Type::extrapoint_double_fox_last_trick:
    return (*this)(Type::extrapoint_fox_last_trick);

  case Type::extrapoint_charlie:
    return true;
  case Type::extrapoint_catch_charlie:
  case Type::extrapoint_double_charlie:
    return (*this)(Type::extrapoint_charlie);
  case Type::extrapoint_catch_double_charlie:
    return (*this)(Type::extrapoint_double_charlie);
  case Type::extrapoint_catch_charlie_only_with_diamond_queen:
    return (*this)(Type::extrapoint_catch_charlie);

  case Type::extrapoint_dulle_jabs_dulle:
    return ((*this)(Type::dullen)
            && ( (*this)(Type::dullen_second_over_first)
                || (*this)(Type::dullen_contrary_in_last_trick) ));
  case Type::extrapoint_heart_trick:
    return ((*this)(Type::dullen)
            && !(*this)(Type::with_nines));

  case Type::knocking:
  case Type::announcement_individual_limits:
  case Type::announcement_limit_only_for_current:
  case Type::announcement_till_full_trick:
  case Type::announcement_first_trick_thirty_points:
    return true;

  case Type::announcement_first_trick_thirty_points_only_first:
    return (*this)(Type::announcement_first_trick_thirty_points);

  case Type::announcement_first_trick_thirty_points_only_re_contra:
    return (*this)(Type::announcement_first_trick_thirty_points);

  case Type::announcement_first_trick_thirty_points_in_marriage:
    return (*this)(Type::announcement_first_trick_thirty_points);

  case Type::announcement_re_doubles:
    return true;

  case Type::announcement_contra_doubles_against_re:
    return (*this)(Type::announcement_re_doubles);

  case Type::number_of_rounds_limited:
    return true;

  case Type::points_limited:
    return ((*this)(Type::counting) != Counting::plusminus);

  case Type::offer_duty_solo:
    return (*this)(Type::number_of_duty_soli);

  case Type::lustsolo_player_leads:
    return (*this)(Type::solo);

  case Type::solo_always_counts_triple:
    return (   (*this)(Type::solo)
            && ((*this)(Type::counting) != Counting::plusminus) );
  } // switch(type)

  DEBUG_ASSERT(false);
  return false;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Rule::dependencies(Type::Unsigned const type) const -> bool
{
  switch(type) {
  case Type::min_number_of_throwing_nines:
    return (*this)(Type::throw_with_nines);
  case Type::min_number_of_throwing_kings:
    return (*this)(Type::throw_with_kings);
  case Type::min_number_of_throwing_nines_and_kings:
    return (*this)(Type::throw_with_nines_and_kings);
  case Type::min_richness_for_throwing:
    return (*this)(Type::throw_with_richness);

  case Type::bock_multiplier:
    return (*this)(Type::bock);

  case Type::marriage_determination:
  case Type::announcement_no_120:
  case Type::announcement_no_90:
  case Type::announcement_no_60:
  case Type::announcement_no_30:
  case Type::announcement_no_0:
    return (*this)(Type::announcement_individual_limits);

  case Type::number_of_rounds:
    return (*this)(Type::number_of_rounds_limited);

  case Type::points:
    return (*this)(Type::points_limited);

  case Type::number_of_duty_soli:
    return (   (*this)(Type::solo)
            && (   (*this)(Type::solo_jack)
                || (*this)(Type::solo_queen)
                || (*this)(Type::solo_king)
                || (*this)(Type::solo_queen_jack)
                || (*this)(Type::solo_king_jack)
                || (*this)(Type::solo_king_queen)
                || (*this)(Type::solo_koehler)
                || (*this)(Type::solo_color)
                || (*this)(Type::solo_meatless))
            && (   (*this)(Type::number_of_rounds_limited)
                || (*this)(Type::points_limited)));

  case Type::number_of_duty_color_soli:
    return (   ((*this)(Type::number_of_duty_soli) > 0)
            && (*this)(Type::solo_color));

  case Type::number_of_duty_picture_soli:
    return (   ((*this)(Type::number_of_duty_soli) > 0)
            && (   (*this)(Type::solo_jack)
                || (*this)(Type::solo_queen)
                || (*this)(Type::solo_king)
                || (*this)(Type::solo_queen_jack)
                || (*this)(Type::solo_king_jack)
                || (*this)(Type::solo_king_queen)
                || (*this)(Type::solo_koehler) ) );

  case Type::number_of_players:
  case Type::number_of_players_in_game:
    return true;
  } // switch(type)

  DEBUG_ASSERT(false);
  return false;
}

auto Rule::value(Type::Bool const type) const -> bool
{
  return bool_[type - Type::bool_first];
}

auto Rule::value(Type::Unsigned const type) const -> unsigned
{
  return unsigned_[type - Type::unsigned_first];
}

auto Rule::value(Type::Counting const type) const -> Counting
{
  return counting_;
}

auto Rule::counting() const -> Counting const&
{
  return counting_;
}

auto Rule::min(Type::Unsigned const type) const -> unsigned
{
  switch(type) {
  case Type::bock_multiplier:
    return 2;

  case Type::min_number_of_throwing_nines:
  case Type::min_number_of_throwing_kings:
    return ( ( ((*this)(Type::number_of_same_cards) * (*this)(Type::number_of_card_colors))
              / (*this)(Type::number_of_players_in_game))
            + 1);

  case Type::min_number_of_throwing_nines_and_kings:
    return ( ( 2 * ((*this)(Type::number_of_same_cards)
                    * (*this)(Type::number_of_card_colors))
              / (*this)(Type::number_of_players_in_game))
            + 1);

  case Type::min_richness_for_throwing:
    return ( (  (*this)(Type::number_of_same_cards)
              * (*this)(Type::number_of_card_colors)
              * (  Card::ace
                 + Card::ten
                 + Card::king
                 + Card::queen
                 + Card::jack
                 + Card::nine)
             ) / (*this)(Type::number_of_players_in_game)
            + 1);


  case Type::marriage_determination:
    return 1;

  case Type::announcement_no_120:
  case Type::announcement_no_90:
  case Type::announcement_no_60:
  case Type::announcement_no_30:
  case Type::announcement_no_0:
    return 1;

  case Type::number_of_rounds:
    return 1;

  case Type::points:
    return 1;

  case Type::number_of_duty_soli:
    return ((*this)(Type::number_of_duty_color_soli)
            + (*this)(Type::number_of_duty_picture_soli));

  case Type::number_of_duty_color_soli:
    return 0;

  case Type::number_of_duty_picture_soli:
    return 0;

  case Type::number_of_players:
    return 4;

  case Type::number_of_players_in_game:
    return 4;
  } // switch(type)

  DEBUG_ASSERT(false);
  return 0;
}

auto Rule::max(Type::Unsigned const type) const -> unsigned
{
  switch(type) {
  case Type::bock_multiplier:
    return 256;

  case Type::min_number_of_throwing_nines:
  case Type::min_number_of_throwing_kings:
    return ((*this)(Type::number_of_same_cards) * (*this)(Type::number_of_card_colors));

  case Type::min_number_of_throwing_nines_and_kings:
    return 2 * ((*this)(Type::number_of_same_cards) * (*this)(Type::number_of_card_colors));

  case Type::min_richness_for_throwing:
    return (8 * Card::ace + 4 * Card::ten);

  case Type::marriage_determination:
    return (*this)(Type::max_number_of_tricks_in_game);

  case Type::announcement_no_120:
  case Type::announcement_no_90:
  case Type::announcement_no_60:
  case Type::announcement_no_30:
  case Type::announcement_no_0:
    return (*this)(Type::max_number_of_tricks_in_game);

  case Type::number_of_rounds:
    return UINT_MAX - 1;

  case Type::points:
    return UINT_MAX - 1;

  case Type::number_of_duty_soli:
    return ((*this)(Type::number_of_rounds)
            * (*this)(Type::number_of_players));

  case Type::number_of_duty_color_soli:
    return ((*this)(Type::number_of_duty_soli)
            - (*this)(Type::number_of_duty_picture_soli));

  case Type::number_of_duty_picture_soli:
    return ((*this)(Type::number_of_duty_soli)
            - (*this)(Type::number_of_duty_color_soli));

  case Type::number_of_players:
    return 4;

  case Type::number_of_players_in_game:
    return (((*this)(Type::number_of_players) / 2) * 2);
  } // switch(type)

  DEBUG_ASSERT(false);
  return 0;
}

auto Rule::card_colors() const -> vector<Card::Color> const&
{
  static const vector<Card::Color> colors = {Card::club, Card::spade, Card::heart, Card::diamond};

  DEBUG_ASSERTION((colors.size() == (*this)(Type::number_of_card_colors)),
                  "Rule::card_colors():\n"
                  "  Number of card colors is not valid");

  return colors;
}

auto Rule::card_values() const -> vector<Card::Value> const&
{
  static const vector<Card::Value> values_without_nines = {Card::ace, Card::ten, Card::king, Card::queen, Card::jack};
  static const vector<Card::Value> values_with_nines = {Card::ace, Card::ten, Card::king, Card::queen, Card::jack, Card::nine};

  vector<Card::Value> const& values
    = ( (*this)(Type::with_nines)
       ? values_with_nines
       : values_without_nines );

  DEBUG_ASSERTION((values.size() == (*this)(Type::number_of_card_values)),
                  "Rule::card_values():\n"
                  "  Number of card values is not valid");

  return values;
}

auto Rule::cards() const -> vector<Card> const&
{
  static vector<Card> const cards_without_nines
    = { Card::club_ace,
      Card::club_king,
      Card::club_queen,
      Card::club_jack,
      Card::club_ten,
      Card::spade_ace,
      Card::spade_king,
      Card::spade_queen,
      Card::spade_jack,
      Card::spade_ten,
      Card::heart_ace,
      Card::heart_king,
      Card::heart_queen,
      Card::heart_jack,
      Card::heart_ten,
      Card::diamond_ace,
      Card::diamond_king,
      Card::diamond_queen,
      Card::diamond_jack,
      Card::diamond_ten,
    };
  static vector<Card> const cards_with_nines
    = { Card::club_ace,
      Card::club_king,
      Card::club_queen,
      Card::club_jack,
      Card::club_ten,
      Card::spade_ace,
      Card::spade_king,
      Card::spade_queen,
      Card::spade_jack,
      Card::spade_ten,
      Card::heart_ace,
      Card::heart_king,
      Card::heart_queen,
      Card::heart_jack,
      Card::heart_ten,
      Card::diamond_ace,
      Card::diamond_king,
      Card::diamond_queen,
      Card::diamond_jack,
      Card::diamond_ten,
      Card::club_nine,
      Card::spade_nine,
      Card::heart_nine,
      Card::diamond_nine,
    };

  vector<Card> const& cards
    = (  (*this)(Type::with_nines)
       ? cards_with_nines
       : cards_without_nines );

  DEBUG_ASSERTION((cards.size()
                   == ( (*this)(Type::number_of_card_colors)
                       * (*this)(Type::number_of_card_values) ) ),
                  "Rule::cards():\n"
                  "  Number of cards is not valid:\n"
                  "  'cards.size() = " << cards.size()
                  << " != "
                  <<  (*this)(Type::number_of_card_colors)
                  << " * " << (*this)(Type::number_of_card_values) );

  return cards;
}


auto Rule::can_be_changed(Type::Bool type) const -> bool
{
  if (   !::party || this != &::party->rule())
    return true;

  switch (type) {
  case Type::mutate:
#ifdef RELEASE
    return false;
#else
    return true;
#endif

  case Type::with_nines:
    return ::party->status() < Party::Status::play;
  case Type::knocking:
  case Type::marriage_first_foreign:
  case Type::marriage_first_color:
  case Type::marriage_first_single_color:
  case Type::marriage_first_trump:
  case Type::marriage_first_one_decides:
  case Type::marriage_before_poverty:
  case Type::solo:
  case Type::solo_jack:
  case Type::solo_queen:
  case Type::solo_king:
  case Type::solo_queen_jack:
  case Type::solo_king_jack:
  case Type::solo_king_queen:
  case Type::solo_koehler:
  case Type::solo_color:
  case Type::solo_meatless:
  case Type::dullen:
    return (   ::party->status() < Party::Status::play
            || (   ::party->in_game()
                && ::party->game().status() == Game::Status::finished));
  case Type::poverty:
  case Type::poverty_shift:
  case Type::poverty_shift_only_trump:
  case Type::poverty_fox_shift_open:
  case Type::poverty_fox_do_not_count:
  case Type::poverty_fox_shift_extra:
    return (   !::party->in_game()
            || ::party->game().status() != Game::Status::poverty_shift);
  case Type::bock:
  case Type::bock_append:
  case Type::bock_120:
  case Type::bock_solo_lost:
  case Type::bock_re_lost:
  case Type::bock_heart_trick:
  case Type::bock_black:
  case Type::throw_with_nines:
  case Type::throw_with_kings:
  case Type::throw_with_nines_and_kings:
  case Type::throw_with_richness:
  case Type::throw_with_one_trump:
  case Type::throw_when_fox_highest_trump:
  case Type::throwing_before_solo:
  case Type::dullen_second_over_first:
  case Type::dullen_contrary_in_last_trick:
  case Type::dullen_first_over_second_with_swines:
  case Type::swines:
  case Type::swines_announcement_begin:
  case Type::swine_only_second:
  case Type::swines_in_solo:
  case Type::swines_in_poverty:
  case Type::hyperswines:
  case Type::hyperswines_announcement_begin:
  case Type::hyperswines_in_solo:
  case Type::hyperswines_in_poverty:
  case Type::swines_and_hyperswines_joint:
  case Type::extrapoint_catch_fox:
  case Type::extrapoint_catch_fox_last_trick:
  case Type::extrapoint_fox_last_trick:
  case Type::extrapoint_double_fox_last_trick:
  case Type::extrapoint_charlie:
  case Type::extrapoint_catch_charlie:
  case Type::extrapoint_double_charlie:
  case Type::extrapoint_catch_double_charlie:
  case Type::extrapoint_catch_charlie_only_with_diamond_queen:
  case Type::extrapoint_dulle_jabs_dulle:
  case Type::extrapoint_heart_trick:
  case Type::announcement_individual_limits:
  case Type::announcement_limit_only_for_current:
  case Type::announcement_till_full_trick:
  case Type::announcement_first_trick_thirty_points:
  case Type::announcement_first_trick_thirty_points_only_first:
  case Type::announcement_first_trick_thirty_points_only_re_contra:
  case Type::announcement_first_trick_thirty_points_in_marriage:
  case Type::announcement_re_doubles:
  case Type::announcement_contra_doubles_against_re:
  case Type::number_of_rounds_limited:
  case Type::points_limited:
  case Type::offer_duty_solo:
  case Type::lustsolo_player_leads:
  case Type::solo_always_counts_triple:
    return true;
  }
  return false;
}


auto Rule::can_be_changed(Type::Unsigned type) const -> bool
{
  return true;
}


auto Rule::can_be_changed(Type::Counting type) const -> bool
{
  return true;
}


// NOLINTNEXTLINE(misc-no-recursion)
void Rule::set(Type::Bool type, bool value)
{
  if (!can_be_changed(type))
    return ;
  if (this->value(type) == value)
    return ;

  bool const old_value = bool_[type - Type::bool_first];
  bool_[type - Type::bool_first] = value;

  switch (type) {
  case Type::with_nines:
    // Die Ansagezeitpunkte automatisch umändern
    if (   !value
        && this->value(Type::announcement_no_120) == 11
        && this->value(Type::announcement_no_90)  == 10
        && this->value(Type::announcement_no_60)  ==  9
        && this->value(Type::announcement_no_30)  ==  8
        && this->value(Type::announcement_no_0)   ==  7
       ) {
      set(Type::announcement_no_120, 9);
      set(Type::announcement_no_90,  8);
      set(Type::announcement_no_60,  7);
      set(Type::announcement_no_30,  6);
      set(Type::announcement_no_0,   5);
    } else if (   value
               && this->value(Type::announcement_no_120) ==  9
               && this->value(Type::announcement_no_90)  ==  8
               && this->value(Type::announcement_no_60)  ==  7
               && this->value(Type::announcement_no_30)  ==  6
               && this->value(Type::announcement_no_0)   ==  5
              ) {
      set(Type::announcement_no_120, 11);
      set(Type::announcement_no_90,  10);
      set(Type::announcement_no_60,   9);
      set(Type::announcement_no_30,   8);
      set(Type::announcement_no_0,    7);
    }
    break;
  case Type::marriage_first_foreign:
  case Type::marriage_first_color:
  case Type::marriage_first_single_color:
  case Type::marriage_first_trump:
    // if none is set, set 'first color'
    if (   !this->value(Type::marriage_first_foreign)
        && !this->value(Type::marriage_first_color)
        && !this->value(Type::marriage_first_single_color)
        && !this->value(Type::marriage_first_trump)) {
      type = Type::marriage_first_color;
      value = true;
      set(type, value);
    }
    break;
  default:
    break;
  } // switch(type)

  signal_changed(type, &old_value);
  // the set-function has to be called again with the same values,
  // because else the ui could change the values
  DEBUG("rule") << _(type) << " = " << value << '\n';
  set(type, value);
}

void Rule::set(Type::Bool const type, string const& value)
{
  if (   !value.empty()
      && value != "true"
      && value != "false"
      && value != "yes"
      && value != "no"
      && value != "0"
      && value != "1") {
    cerr << "illegal value '" << value << "' for '" << type << "', "
      << "must be a boolean ('true' or 'false' or 'yes' or 'no' or '1' or '0'. "
      << "Ignoring"
      << endl;
  } else {
    set(type, value.empty() ? true : stob(value));
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
void Rule::set(Type::Unsigned const type, unsigned const value)
{
  if (!can_be_changed(type))
    return ;
  if (this->value(type) == value)
    return ;

  unsigned const old_value = unsigned_[type - Type::unsigned_first];
  unsigned_[type - Type::unsigned_first] = value;

  switch(type) {
  case Type::announcement_no_120:
    if (value < this->value(Type::announcement_no_90))
      set(Type::announcement_no_90, value);
    break;
  case Type::announcement_no_90:
    if (value > this->value(Type::announcement_no_120))
      set(Type::announcement_no_120, value);
    if (value < this->value(Type::announcement_no_60))
      set(Type::announcement_no_60, value);
    break;
  case Type::announcement_no_60:
    if (value > this->value(Type::announcement_no_90))
      set(Type::announcement_no_90, value);
    if (value < this->value(Type::announcement_no_30))
      set(Type::announcement_no_30, value);
    break;
  case Type::announcement_no_30:
    if (value > this->value(Type::announcement_no_60))
      set(Type::announcement_no_60, value);
    if (value < this->value(Type::announcement_no_0))
      set(Type::announcement_no_0, value);
    break;
  case Type::announcement_no_0:
    if (value > this->value(Type::announcement_no_30))
      set(Type::announcement_no_30, value);
    break;
  default:
    break;
  } // switch(type)

  signal_changed(type, &old_value);
  // the set-function has to be called again with the same values,
  // because else the ui could change the values
  DEBUG("rule") << _(type) << " = " << value << '\n';
  set(type, value);
}

void Rule::set(Type::Unsigned const type, string const& value)
{
  char* end_ptr = nullptr; // NOLINT(hicpp-vararg)
  auto const number = strtoul(value.c_str(), &end_ptr, 0);
  if (*end_ptr != '\0') {
    cerr << "illegal value '" << value << "' for '" << type << "', "
      << "must be a digit.\n"
      << "Ignoring"
      << endl;
    return ;
  }

  set(type, number);
}


// NOLINTNEXTLINE(misc-no-recursion)
void Rule::set(Type::Counting const type, Counting const value)
{
  if (!can_be_changed(type))
    return ;
  if (counting_ == value)
    return ;

  auto const old_value = counting();

  counting_ = value;

  signal_changed(type, &old_value);
  // the set-function has to be called again with the same values,
  // because else the ui could change the values
  set(type, value);
}


void Rule::set(Type::Counting const type, string const& value)
{
  if (value == to_string(Counting::plus))
    set(type, Counting::plus);
  else if (value == to_string(Counting::minus))
    set(type, Counting::minus);
  else if (value == to_string(Counting::plusminus))
    set(type, Counting::plusminus);
#ifdef BACKWARD_COMPABILITY
  // 0.7.0 name changed
  else if (value == to_string(Counting::plus))
    set(type, Counting::plus);
  else if (value == to_string(Counting::minus))
    set(type, Counting::minus);
  else if (value == to_string(Counting::plusminus))
    set(type, Counting::plusminus);
#endif
  else
    cerr << "illegal value '" << value << "' for '" << type << "', "
      << "must be "
      << "'" << to_string(Counting::plus) << "', "
      << "'" << to_string(Counting::minus) << "' or "
      << "'" << to_string(Counting::plusminus) << ".\n"
      << "Ignoring"
      << endl;
}

auto Rule::set(string const& type, string const& value) -> bool
{
  for (int i = Type::bool_first; i <= Type::bool_last; ++i) {
    if (type == to_string(Rule::Type::Bool(i))) {
      set(Rule::Type::Bool(i), value);
      return true;
    }
    if (type == "no " + to_string(Rule::Type::Bool(i))) {
      set(Rule::Type::Bool(i), value.empty() ? false : !stob(value));
      return true;
    }
    if (type == "without nines") {
      set(Rule::Type::with_nines, value.empty() ? false : !stob(value));
      return true;
    }
  }
  for (int i = Type::unsigned_first; i <= Type::unsigned_last; ++i) {
    if (type == to_string(Rule::Type::Unsigned(i))) {
      set(Rule::Type::Unsigned(i), value);
      return true;
    }
  }

  if (type == to_string(Type::counting)) {
    set(Type::counting, value);
    return true;
  }

#ifndef OUTDATED
  // 0.7.2b -> 0.7.3
  if (type == "throw with five nines") {
    set(Type::throw_with_nines, value);
    set(Type::min_number_of_throwing_nines, 5);
    return true;
  }
  if (type == "throw with six kings") {
    set(Type::throw_with_kings, value);
    set(Type::min_number_of_throwing_kings, 6);
    return true;
  }
#endif
#ifndef OUTDATED
  // 0.7.18 -> 0.7.19
  if (type == "dollen") {
    set(Type::dullen, value);
    return true;
  }
  if (type == "dollen second over first") {
    set(Type::dullen_second_over_first, value);
    return true;
  }
  if (type == "dollen contrary in last trick") {
    set(Type::dullen_contrary_in_last_trick, value);
    return true;
  }
  if (type == "dollen first over second with swines") {
    set(Type::dullen_first_over_second_with_swines, value);
    return true;
  }
  if (type == "extrapoint dolle jabs dolle") {
    set(Type::extrapoint_dulle_jabs_dulle, value);
    return true;
  }
#endif

  return false;
}

void Rule::mutate()
{
  int const r = ::random_value(Type::unsigned_last + 1);

  // do not mutate all rules
  if (   r == Type::mutate
      || r == Type::number_of_players
      || r == Type::number_of_players_in_game
      || r == Type::number_of_rounds_limited
      || r == Type::number_of_rounds
      || r == Type::lustsolo_player_leads
      || r == Type::points_limited
      || r == Type::points
      || r == Type::number_of_duty_soli
      || r == Type::number_of_duty_color_soli
      || r == Type::number_of_duty_picture_soli
      || (   r >= Type::solo
          && r <= Type::solo_meatless)
     )
    return ;

  if (   (r >= Type::bool_first)
      && (r <= Type::bool_last) ) {
    auto const type = static_cast<Type::Bool>(r);
    cout << "mutate " << type
      << ": " << value(type)
      << " (" << (*this)(type) << ")";
    set(type, !value(type));
    cout << " → " << value(type)
      << " (" << (*this)(type) << ")"
      << endl;
  } else if (   (r >= Type::unsigned_first)
             && (r <= Type::unsigned_last) ) {
    auto const type = static_cast<Type::Unsigned>(r);
    cout << "mutate " << type
      << ": " << value(type)
      << " (" << (*this)(type) << ")";
    unsigned value = this->value(type);
    if (value != UINT_MAX) {
      if (::random_value(2) == 0) {
        if (value > min(type))
          value -= 1;
      } else {
        if (value < max(type))
          value += 1;
      }
    }
    set(type, value);
    cout << " → " << this->value(type)
      << " (" << (*this)(type) << ")"
      << endl;
  } else {
    cout << "mutate " << r << endl;
  }
}

auto Rule::load(Path const path) -> bool
{
  ifstream istr(path);
  if (!istr.good())
    return false;

  return read(istr);
}

auto Rule::read(istream& istr) -> bool
{
  reset_to_tournament();

  unsigned depth = 0;

  // load the rules
  while (istr.good()) {
    Config config;
    istr >> config;

    // finished with the config file
    if (config.empty())
      break;

    if (config.separator) {
      // a setting
      if (!set(config.name, config.value)) {
        cerr << "rule file:        "
          << "ignoring unknown rule '" << config.name << "'."
          << endl;
      }
    } else { // if (config.separator)
      // a setting
      // if the value is in parentencies, remove both
      if (config.name == "tournament") {
        reset_to_tournament();
      } else if (config.name == "!end") {
        // ignore the rest of the file
        break;
      } else if(config.name == "!stdout") {
        // output of the data to 'stdout'
        cout << config.value << endl;
      } else if(config.name == "!stderr") {
        // output of the data to 'stderr'
        cerr << config.value << endl;
      } else if(config.name == "{") {
        depth += 1;
      } else if(config.name == "}") {
        if (depth == 0) {
          cerr << "Rulefile: found a '}' without a '{' before.\n"
            << "Finish reading the the file."
            << endl;
          break;
        } // if (depth == 0)
        depth -= 1;
        if (depth == 0)
          break;
      } else if(config.name.empty()) {
        cerr << "Rule file: "
          << "Ignoring line \'" << config.value << "\'.\n";
      } else {
        cerr << "Rule file: "
          << "Rule '" << config.name << "' unknown.\n"
          << "Ignoring it.\n";
      } // if (config.name == .)
    } // config.separator
  } // while (istr.good())

  return true;
}

auto Rule::save(Path const path) const -> bool
{
  auto path_tmp = path;
  path_tmp += ".tmp";
  ofstream ostr(path_tmp);
  if (!ostr.good()) {
    ::ui->information(_("Error::Rule::save: Error opening temporary file %s. Aborting saving.", path_tmp.string()), InformationType::problem);
    return false;
  }

  ostr << "# FreeDoko rules (version " << *::version << ")\n"
    << '\n';
  write(ostr);

  if (!ostr.good()) {
    ::ui->information(_("Error::Rule::save: Error saving in temporary file %s. Keeping temporary file (for bug tracking).", path_tmp.string()), InformationType::problem);
    return false;
  }
  ostr.close();

  try {
    if (is_regular_file(path))
      remove(path);
    rename(path_tmp, path);
  } catch (std::filesystem::filesystem_error const& error) {
    ::ui->information(_("Error::Rule::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string())
                      + "\n"
                      + _("Error message: %s", error.what()),
                      InformationType::problem);
    return false;
  } catch(...) {
    ::ui->information(_("Error::Rule::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string()), InformationType::problem);
    return false;
  }

  return true;
}

void Rule::write(ostream& ostr) const
{
  std::ios_base::fmtflags const flags = ostr.flags();
  ostr << std::boolalpha;

  if (value(Type::mutate)) {
    ostr << Type::mutate << " = "
      << value(Type::mutate) << '\n'
      << '\n';
  }

  ostr << Type::with_nines << " = "
    << value(Type::with_nines) << '\n'

    << '\n'

    << Type::number_of_rounds_limited << " = "
    << value(Type::number_of_rounds_limited) << '\n'
    << "  "
    << Type::number_of_rounds << " = "
    << value(Type::number_of_rounds) << '\n'

    << Type::points_limited << " = "
    << value(Type::points_limited) << '\n'
    << "  "
    << Type::points << " = "
    << value(Type::points) << '\n'

    << Type::number_of_duty_soli << " = "
    << value(Type::number_of_duty_soli) << '\n'
    << "  "
    << Type::number_of_duty_color_soli << " = "
    << value(Type::number_of_duty_color_soli) << '\n'
    << "  "
    << Type::number_of_duty_picture_soli << " = "
    << value(Type::number_of_duty_picture_soli) << '\n'
    << Type::offer_duty_solo << " = "
    << value(Type::offer_duty_solo) << '\n'
    << Type::lustsolo_player_leads << " = "
    << value(Type::lustsolo_player_leads) << '\n'
    << Type::solo_always_counts_triple << " = "
    << value(Type::solo_always_counts_triple) << '\n'

    // *** << Type::party_points_SUMMING << " = "
    // *** << value(Type::party_points_SUMMING) << '\n'

    << Type::number_of_players << " = "
    << value(Type::number_of_players) << '\n'

    << Type::number_of_players_in_game << " = "
    << value(Type::number_of_players_in_game) << '\n'

    << '\n'

    << Type::bock << " = "
    << value(Type::bock) << '\n'
    << "  "
    << Type::bock_multiplier << " = "
    << value(Type::bock_multiplier) << '\n'
    << "  "
    << Type::bock_append << " = "
    << value(Type::bock_append) << '\n'
    << "  "
    << Type::bock_120 << " = "
    << value(Type::bock_120) << '\n'
    << "  "
    << Type::bock_solo_lost << " = "
    << value(Type::bock_solo_lost) << '\n'
    << "  "
    << Type::bock_re_lost << " = "
    << value(Type::bock_re_lost) << '\n'
    << "  "
    << Type::bock_heart_trick << " = "
    << value(Type::bock_heart_trick) << '\n'
    << "  "
    << Type::bock_black << " = "
    << value(Type::bock_black) << '\n'

    << '\n'


    << Type::throw_with_nines << " = "
    << value(Type::throw_with_nines) << '\n'

    << Type::min_number_of_throwing_nines << " = "
    << value(Type::min_number_of_throwing_nines) << '\n'

    << Type::throw_with_kings << " = "
    << value(Type::throw_with_kings) << '\n'

    << Type::min_number_of_throwing_kings << " = "
    << value(Type::min_number_of_throwing_kings) << '\n'

    << Type::throw_with_nines_and_kings << " = "
    << value(Type::throw_with_nines_and_kings) << '\n'

    << Type::min_number_of_throwing_nines_and_kings << " = "
    << value(Type::min_number_of_throwing_nines_and_kings) << '\n'

    << Type::throw_with_richness << " = "
    << value(Type::throw_with_richness) << '\n'

    << Type::min_richness_for_throwing << " = "
    << value(Type::min_richness_for_throwing) << '\n'

    << '\n'

    << Type::poverty << " = "
    << value(Type::poverty) << '\n'
    << "  "
    << Type::poverty_shift << " = "
    << value(Type::poverty_shift) << '\n'
    << "  "
    << Type::poverty_shift_only_trump << " = "
    << value(Type::poverty_shift_only_trump) << '\n'
    << "  "
    << Type::poverty_fox_do_not_count << " = "
    << value(Type::poverty_fox_do_not_count) << '\n'
    << "  "
    << Type::poverty_fox_shift_extra << " = "
    << value(Type::poverty_fox_shift_extra) << '\n'
    << "  "
    << Type::poverty_fox_shift_open << " = "
    << value(Type::poverty_fox_shift_open) << '\n'
    << "  "
    << Type::throw_with_one_trump << " = "
    << value(Type::throw_with_one_trump) << '\n'
    << "  "
    << Type::throw_when_fox_highest_trump << " = "
    << value(Type::throw_when_fox_highest_trump) << '\n'

    << '\n'

    << Type::marriage_determination << " = "
    << value(Type::marriage_determination) << '\n'
    << "  "
    << Type::marriage_first_foreign << " = "
    << value(Type::marriage_first_foreign) << '\n'
    << "  "
    << Type::marriage_first_color << " = "
    << value(Type::marriage_first_color) << '\n'
    << "  "
    << Type::marriage_first_single_color << " = "
    << value(Type::marriage_first_single_color) << '\n'
    << "  "
    << Type::marriage_first_trump << " = "
    << value(Type::marriage_first_trump) << '\n'
    << "  "
    << Type::marriage_first_one_decides << " = "
    << value(Type::marriage_first_one_decides) << '\n'
    << "  "
    << Type::marriage_before_poverty << " = "
    << value(Type::marriage_before_poverty) << '\n'

    << '\n'

    << Type::solo << " = "
    << value(Type::solo) << '\n'
    << "  "
    << Type::solo_jack << " = "
    << value(Type::solo_jack) << '\n'
    << "  "
    << Type::solo_queen << " = "
    << value(Type::solo_queen) << '\n'
    << "  "
    << Type::solo_king << " = "
    << value(Type::solo_king) << '\n'
    << "  "
    << Type::solo_queen_jack << " = "
    << value(Type::solo_queen_jack) << '\n'
    << "  "
    << Type::solo_king_jack << " = "
    << value(Type::solo_king_jack) << '\n'
    << "  "
    << Type::solo_king_queen << " = "
    << value(Type::solo_king_queen) << '\n'
    << "  "
    << Type::solo_koehler << " = "
    << value(Type::solo_koehler) << '\n'
    << "  "
    << Type::solo_color << " = "
    << value(Type::solo_color) << '\n'
    << "  "
    << Type::solo_meatless << " = "
    << value(Type::solo_meatless) << '\n'
    << "  "
    << Type::throwing_before_solo << " = "
    << value(Type::throwing_before_solo) << '\n'

    << '\n'

    << Type::dullen << " = "
    << value(Type::dullen) << '\n'
    << "  "
    << Type::dullen_second_over_first << " = "
    << value(Type::dullen_second_over_first) << '\n'
    << "  "
    << Type::dullen_contrary_in_last_trick << " = "
    << value(Type::dullen_contrary_in_last_trick) << '\n'
    << "  "
    << Type::dullen_first_over_second_with_swines << " = "
    << value(Type::dullen_first_over_second_with_swines) << '\n'

    << '\n'

    << Type::swines << " = "
    << value(Type::swines) << '\n'
    << "  "
    << Type::swines_in_solo << " = "
    << value(Type::swines_in_solo) << '\n'
    << "  "
    << Type::swines_in_poverty << " = "
    << value(Type::swines_in_poverty) << '\n'
    << "  "
    << Type::swines_announcement_begin << " = "
    << value(Type::swines_announcement_begin) << '\n'
    << "  "
    << Type::swine_only_second << " = "
    << value(Type::swine_only_second) << '\n'

    << Type::hyperswines << " = "
    << value(Type::hyperswines) << '\n'
    << "  "
    << Type::hyperswines_in_solo << " = "
    << value(Type::hyperswines_in_solo) << '\n'
    << "  "
    << Type::hyperswines_in_poverty << " = "
    << value(Type::hyperswines_in_poverty) << '\n'
    << "  "
    << Type::hyperswines_announcement_begin << " = "
    << value(Type::hyperswines_announcement_begin) << '\n'
    << "  "
    << Type::swines_and_hyperswines_joint << " = "
    << value(Type::swines_and_hyperswines_joint) << '\n'

    << '\n'

    << "# Extrapoints:\n"

    << Type::extrapoint_catch_fox << " = "
    << value(Type::extrapoint_catch_fox) << '\n'
    << Type::extrapoint_catch_fox_last_trick << " = "
    << value(Type::extrapoint_catch_fox_last_trick) << '\n'
    << Type::extrapoint_fox_last_trick << " = "
    << value(Type::extrapoint_fox_last_trick) << '\n'
    << Type::extrapoint_double_fox_last_trick << " = "
    << value(Type::extrapoint_double_fox_last_trick) << '\n'

    << Type::extrapoint_dulle_jabs_dulle << " = "
    << value(Type::extrapoint_dulle_jabs_dulle) << '\n'
    << Type::extrapoint_heart_trick << " = "
    << value(Type::extrapoint_heart_trick) << '\n'

    << Type::extrapoint_charlie << " = "
    << value(Type::extrapoint_charlie) << '\n'
    << Type::extrapoint_catch_charlie << " = "
    << value(Type::extrapoint_catch_charlie) << '\n'
    << Type::extrapoint_double_charlie << " = "
    << value(Type::extrapoint_double_charlie) << '\n'
    << Type::extrapoint_catch_double_charlie << " = "
    << value(Type::extrapoint_catch_double_charlie) << '\n'
    << Type::extrapoint_catch_charlie_only_with_diamond_queen << " = "
    << value(Type::extrapoint_catch_charlie_only_with_diamond_queen) << '\n'


    << '\n'

    << Type::announcement_first_trick_thirty_points << " = "
    << value(Type::announcement_first_trick_thirty_points) << '\n'

    << Type::announcement_first_trick_thirty_points_only_first << " = "
    << value(Type::announcement_first_trick_thirty_points_only_first) << '\n'

    << Type::announcement_first_trick_thirty_points_only_re_contra << " = "
    << value(Type::announcement_first_trick_thirty_points_only_re_contra) << '\n'

    << Type::announcement_first_trick_thirty_points_in_marriage << " = "
    << value(Type::announcement_first_trick_thirty_points_in_marriage) << '\n'

    << Type::announcement_re_doubles << " = "
    << value(Type::announcement_re_doubles) << '\n'

    << Type::announcement_contra_doubles_against_re << " = "
    << value(Type::announcement_contra_doubles_against_re) << '\n'

    << '\n'

    << "# till when to make the announcements:\n"

    << Type::knocking << " = "
    << value(Type::knocking) << '\n'

    << Type::announcement_no_120 << " = "
    << value(Type::announcement_no_120) << '\n'
    << Type::announcement_no_90 << " = "
    << value(Type::announcement_no_90) << '\n'
    << Type::announcement_no_60 << " = "
    << value(Type::announcement_no_60) << '\n'
    << Type::announcement_no_30 << " = "
    << value(Type::announcement_no_30) << '\n'
    << Type::announcement_no_0 << " = "
    << value(Type::announcement_no_0) << '\n'

    << Type::announcement_individual_limits << " = "
    << value(Type::announcement_individual_limits) << '\n'
    << Type::announcement_limit_only_for_current << " = "
    << value(Type::announcement_limit_only_for_current) << '\n'
    << Type::announcement_till_full_trick << " = "
    << value(Type::announcement_till_full_trick) << '\n'

    << '\n'

    << "# "
    << Type::number_of_same_cards << " = "
    << (*this)(Type::number_of_same_cards) << '\n'
    << "# "
    << Type::number_of_card_colors << " = "
    << (*this)(Type::number_of_card_colors) << '\n'
    << "# "
    << Type::number_of_card_tcolors << " = "
    << (*this)(Type::number_of_card_tcolors) << '\n'
    << "# "
    << Type::number_of_card_values << " = "
    << (*this)(Type::number_of_card_values) << '\n'
    << "# "
    << Type::number_of_cards << " = "
    << (*this)(Type::number_of_cards) << '\n'
    << "# "
    << Type::number_of_teams << " = "
    << (*this)(Type::number_of_teams) << '\n'
    << "# "
    << Type::number_of_players_per_team << " = "
    << (*this)(Type::number_of_players_per_team) << '\n'
    << "# "
    << Type::number_of_tricks_in_game << " = "
    << (*this)(Type::number_of_tricks_in_game) << '\n'

    << '\n'

    << Type::counting << " = "
    << (*this)(Type::counting) << '\n'
    ;

  ostr.flags(flags);
}


auto Rule::rest_query() const -> string
{
  string query;
  for (int i = Rule::Type::bool_first; i <= Rule::Type::bool_last; ++i) {
    auto const type = static_cast<Rule::Type::Bool>(i);
    if (value(type) != tournament_rule.value(type)) {
      if (type == Rule::Type::with_nines && value(type) == false) 
        query += "without nines;";
      else
        query += (value(type) ? "" : "no ") + to_string(type) + ";";
    }
  }

  for (int i = Rule::Type::unsigned_first; i <= Rule::Type::unsigned_last; ++i) {
    auto const type = static_cast<Rule::Type::Unsigned>(i);
    if (value(type) != tournament_rule.value(type))
      query += to_string(type) + "=" + std::to_string(value(type)) + ";";
  }
  // letztes Komma entfernen
  if (!query.empty())
    query.pop_back();
  return query;
}

auto operator<<(ostream& ostr, Rule const& rule) -> ostream&
{
  rule.write(ostr);
  return ostr;
}

auto operator==(Rule const& lhs, Rule const& rhs) -> bool
{
  for (int i = Rule::Type::bool_first; i <= Rule::Type::bool_last; ++i)
    if (lhs.value(static_cast<Rule::Type::Bool>(i))
        != rhs.value(static_cast<Rule::Type::Bool>(i)))
      return false;

  for (int i = Rule::Type::unsigned_first; i <= Rule::Type::unsigned_last; ++i)
    if (lhs.value(static_cast<Rule::Type::Unsigned>(i))
        != rhs.value(static_cast<Rule::Type::Unsigned>(i)))
      return false;

  return true;
}

auto operator!=(Rule const& lhs, Rule const& rhs) -> bool
{
  return !(lhs == rhs);
}
