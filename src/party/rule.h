/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "rule_types.h"
#include "../basetypes/game_type.h"
#include "../basetypes/marriage_selector.h"
#include "../basetypes/announcement.h"
#include "../game/specialpoint.h"
#include "../card/card.h"

class Rule;
extern Rule const tournament_rule;

/**
 ** controls the rules of the game/party
 **/
class Rule {
public:
  using Type = RuleType;

public:
  Rule();
  Rule(Rule const& rule);
  auto operator=(Rule const& rule) -> Rule&;

  auto operator()(Type::Bool type)          const -> bool;
  auto operator()(Type::Unsigned type)      const -> unsigned;
  auto operator()(Type::UnsignedExtra type) const -> unsigned;
  auto operator()(Type::Counting type)      const -> Counting;

  auto operator()(GameType game_type)                 const -> bool;
  auto operator()(MarriageSelector marriage_selector) const -> bool;
  auto operator()(Specialpoint::Type special_point)   const -> bool;
  auto remaining_cards(Announcement announcement)     const -> unsigned;

  auto dependencies(Type::Bool type)     const -> bool;
  auto dependencies(Type::Unsigned type) const -> bool;

  auto value(Type::Bool type)     const -> bool;
  auto value(Type::Unsigned type) const -> unsigned;
  auto value(Type::Counting type) const -> Counting;
  auto counting()                 const -> Counting const& ;

  auto min(Type::Unsigned type) const -> unsigned;
  auto max(Type::Unsigned type) const -> unsigned;

  auto card_colors() const -> vector<Card::Color> const&;
  auto card_values() const -> vector<Card::Value> const&; // descending order
  auto cards()       const -> vector<Card>        const&;

  auto can_be_changed(Type::Bool type)     const -> bool;
  auto can_be_changed(Type::Unsigned type) const -> bool;
  auto can_be_changed(Type::Counting type) const -> bool;

  void set(Type::Bool type, bool value);
  void set(Type::Bool type, string const& value);
  void set(Type::Unsigned type, unsigned value);
  void set(Type::Unsigned type, string const& value);
  void set(Type::Counting type, Counting value);
  void set(Type::Counting type, string const& value);

  auto set(string const& type, string const& value) -> bool;

  // mutate the rule
  void mutate();

  auto load(Path path)       -> bool;
  auto read(istream& istr)   -> bool;
  auto save(Path path) const -> bool;

  void write(ostream& ostr) const;

  auto rest_query() const -> string;

  void reset_to_tournament();
  void reset_to_seed_statistics();

private:
  vector<bool> bool_;
  vector<unsigned> unsigned_;
  Counting counting_ = Counting::plus;

public:
  Signal<void(int, void const*)> signal_changed;
};

auto operator<<(ostream& ostr, Rule const& rule) -> ostream&;

auto operator==(Rule const& lhs, Rule const& rhs) -> bool;
auto operator!=(Rule const& lhs, Rule const& rhs) -> bool;
