/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once


enum class Counting {
  plus,
  minus,
  plusminus
}; // enum class Counting

struct RuleType {
  enum Bool {
    first,
    bool_first = first,

    // mutate the rules after each game
    // default: false
    // is always 'false' in a release
    mutate = bool_first,

    // with or without 9
    // default: true
    with_nines,

    // bock rounds (t.i. the points are doubled)
    // default: true
    bock,
    // append bock rounds (or sum them up)
    // default: false
    bock_append,
    // bock after 120-120 points
    // default: true
    bock_120,
    // bock after a lost solo game
    // default: false
    bock_solo_lost,
    // bock after a lost re/contra game (only re/contra is announced)
    // default: false
    bock_re_lost,
    // bock after a real heart trick
    // default: false
    bock_heart_trick,
    // bock after a black game (a team got no trick)
    // default: false
    bock_black,

    // throw with nines (-> min_number_of_throwing_nines)
    // default: true
    throw_with_nines,
    // throw with kings (-> min_number_of_throwing_kings)
    // default: false
    throw_with_kings,
    // throw with nines and kings (-> min_number_of_throwing_nines_and_kings)
    // default: false
    throw_with_nines_and_kings,
    // throw with too many points (-> min_richness_for_throwing)
    // default: false
    throw_with_richness,

    // poverty (<= 3 trump)
    // default: true
    poverty,
    // to shift the trump (or give the cards again)
    // default: true
    poverty_shift,
    // to shift only the trump
    // default: false
    poverty_shift_only_trump,
    // fox do not count for the poverty trump number
    // default: false
    poverty_fox_do_not_count,
    // shift the fox extra
    // default: false
    poverty_fox_shift_extra,
    // shift the fox open
    // default: false
    poverty_fox_shift_open,
    // to throw with at max one trump
    // default: false
    throw_with_one_trump,

    // to throw when the diamond ace is the highest trump
    // default: false
    throw_when_fox_highest_trump,

    // whether you can make the announcement
    // default: true
    marriage_first_foreign,
    // whether you can make the announcement
    // default: true
    marriage_first_color,
    // whether you can make the announcement
    // default: true
    marriage_first_single_color,
    // whether you can make the announcement
    // default: true
    marriage_first_trump,
    // whether the first trick decides the marriage, also if it goes to the bride
    // default: false
    marriage_first_one_decides,
    // whether the marriage goes before the poverty
    // default: false
    marriage_before_poverty,

    // if the solo games are allowed
    // default: true
    solo,
    // default: true
    solo_jack,
    // default: true
    solo_queen,
    // default: true
    solo_king,
    // default: true
    solo_queen_jack,
    // default: true
    solo_king_jack,
    // default: true
    solo_king_queen,
    // default: true
    solo_koehler,
    // default: true
    solo_color,
    // default: true
    solo_meatless,
    // default: false
    throwing_before_solo,

    // the hearts tens are trump
    // default: true
    dullen,
    // in the same trick is the second hearts ten is greater than the first
    // default: true
    dullen_second_over_first,
    // ... but not in the last trick
    // default: false
    dullen_contrary_in_last_trick,
    // if swines are announced the first dulle goes over the second always
    // default: false
    dullen_first_over_second_with_swines,

    // both diamond ace are the highest
    // default: true
    swines,
    // when to make the announcement
    // (start of the game, or when the first is played)
    // dafault: true
    swines_announcement_begin,
    // when the first diamond ace is home, the second becomes a Schwein
    // default: false
    swine_only_second,
    // when there are swines in a solo
    // default: true
    swines_in_solo,
    // when there are swines in a poverty
    // default: true
    swines_in_poverty,
    // both diamond nine are above the Schweine
    // default: false
    hyperswines,
    // when to make the announcement
    // (start of the game, or when the first is played)
    // default: true
    hyperswines_announcement_begin,
    // when there are hyperswines in a solo
    // default: true
    hyperswines_in_solo,
    // when there are hyperswines in a povert
    // default: true
    hyperswines_in_poverty,
    // whether it is allowed that a player has swines and hyperswines
    // default: false
    swines_and_hyperswines_joint,

    // catching the diamond ace of the other team: +1 point
    // default: true
    extrapoint_catch_fox,
    // catching the diamond ace of the other team in the last trick: +1 point
    // default: false
    extrapoint_catch_fox_last_trick,
    // the fox gets the last trick: +1 point
    // default: false
    extrapoint_fox_last_trick,
    // the double_fox gets the last trick: +1 points
    // (+ the point from 'fox_last_trick')
    // default: false
    extrapoint_double_fox_last_trick,

    // club jack gets the last trick: +1 point
    // default: true
    extrapoint_charlie,
    // club jack of the other team is catched in the last trick: +1 point
    // default: true
    extrapoint_catch_charlie,
    // both club jack of the same team get the last trick: +2 point
    // default: false
    extrapoint_double_charlie,
    // both club jack of the same team is catched in the last trick: +2 point
    // default: false
    extrapoint_catch_double_charlie,
    // jab a charlie only with a diamond queen
    // default: false
    extrapoint_catch_charlie_only_with_diamond_queen,

    // the second dulle jabs the first
    // default: false
    extrapoint_dulle_jabs_dulle,
    // if there is a heart trick
    // default: false
    extrapoint_heart_trick,

    // whether an announcement gives the information, which team the player is
    // default: false
    knocking,
    // whether individual announcement limits can be made
    // default: false
    announcement_individual_limits,
    // whether an announcement can be made without the ones before being
    // in the limit
    // default: false
    announcement_limit_only_for_current,
    // whether the announcement can be made till the trick is full
    // that is the played (open) card does count, too
    // default: false
    announcement_till_full_trick,
    // if the winner of the first trick has to make an announcement,
    // when the trick has at least thirty points
    // default: true
    announcement_first_trick_thirty_points,
    // the duty announcement in the first trick has only got to be made
    // when no announcement has been made so far
    // default: false
    announcement_first_trick_thirty_points_only_first,
    // the duty announcement in the first trick has only got to be made
    // when it is a re/contra
    // default: true
    announcement_first_trick_thirty_points_only_re_contra,
    // the duty announcement in the first trick has also to be made
    // in a marriage
    // default: true
    announcement_first_trick_thirty_points_in_marriage,

    // whether the announcement re/contra doubles the points
    // default: true
    announcement_re_doubles,

    // whether the announcement contra doubles the point 'against re'
    // default: false
    announcement_contra_doubles_against_re,

    // whether the numbe of rounds is limited
    // default: true
    number_of_rounds_limited,
    // whether the points (for the tournament) are limited
    // default: false
    points_limited,

    // offer the duty solo to the other players
    // default: false
    offer_duty_solo,

    // whether the soloplayer of a lust-solo plays the first card
    // default: true
    lustsolo_player_leads,

    // whether solo games always count 3* points (also when lost)
    // default: false
    solo_always_counts_triple,

    bool_last = solo_always_counts_triple
  }; // enum Bool
  static unsigned constexpr bool_number = bool_last - bool_first + 1;

  enum Unsigned {
    unsigned_first = bool_last + 1,

    // bock multiplier
    // default: 2
    bock_multiplier = unsigned_first,

    // minimum number of nines for throwing
    // default: 5
    min_number_of_throwing_nines,
    // minimum number of kings for throwing
    // default: 5
    min_number_of_throwing_kings,
    // minimum number of nines and kings for throwing
    // default: 7
    min_number_of_throwing_nines_and_kings,

    // minimum number of points for throwing
    // default: 70
    min_richness_for_throwing,

    // the number of tricks, after which you have to play alone
    // if you have not found a partner
    marriage_determination,

    // announcements:
    // -1 for not allowed,
    // 1-12: as long as you have at least so many cards on the hand
    // default: 10
    announcement_no_120,
    // default: 9
    announcement_no_90,
    // default: 6
    announcement_no_60,
    // default: 3
    announcement_no_30,
    // default: 3
    announcement_no_0,

    // the number of rounds to play
    // default: 20
    number_of_rounds,

    // the number of points in a tournament
    // default: 40
    points,
    // the number of duty soli
    // default: 0
    number_of_duty_soli,
    // the number of duty color soli
    // default: 0
    number_of_duty_color_soli,
    // the number of duty picture soli
    // default: 0
    number_of_duty_picture_soli,

    // number of players
    // default: 4
    number_of_players,

    // number of players in a game
    // default: 4
    number_of_players_in_game,

    unsigned_last = number_of_players_in_game,
  }; // enum Unsigned
  static unsigned constexpr unsigned_number = unsigned_last - unsigned_first + 1;

  // these values are calculated, not set
  enum UnsignedExtra {
    unsigned_extra_first = unsigned_last + 1,

    // number of cards colors, values and cards
    // how often a card is in the game
    // 2
    number_of_same_cards = unsigned_extra_first,
    // the number of colors
    // 4
    number_of_card_colors,
    // the number of colors, with trump as an extra color
    // 5
    number_of_card_tcolors,
    // the number of card values
    // 5/6
    number_of_card_values,
    // the number of cards per value
    // 8
    number_of_cards_per_value,
    // the number of cards (each (color, value) exists two times)
    // 24
    number_of_cards,

    // the number of teams (two)
    // 2
    number_of_teams,
    // the number of players in a team
    // (number_of_players_in_game / number_of_teams)
    number_of_players_per_team,

    // the number of games per round
    // equals the number of players
    number_of_games_per_round,

    max_number_of_tricks_in_game,
    // number_of_cards / number_of_players
    number_of_tricks_in_game,
    number_of_cards_on_hand = number_of_tricks_in_game,

    // the points needed for a 'Doppelkopf' extrapoint
    // 40
    points_for_doppelkopf,

    // the maximum number of trumps for a poverty
    // 3
    max_number_of_poverty_trumps,

    unsigned_extra_last = max_number_of_poverty_trumps
  }; // enum UnsignedExtra

  // ToDo: Type::Card (p.e. for swines card)

  enum Counting {
    counting = unsigned_extra_last + 1,
    last = counting
  }; // enum Counting
}; // struct RuleType

auto to_string(RuleType::Bool type) -> string;
auto to_string(RuleType::Unsigned type) -> string;
auto to_string(RuleType::UnsignedExtra type) -> string;
auto to_string(RuleType::Counting type) -> string;
auto to_string(Counting counting) -> string;

auto gettext(RuleType::Bool type) -> string;
auto gettext(RuleType::Bool type, string const& text) -> string;
auto gettext(RuleType::Unsigned type) -> string;
auto gettext(RuleType::Unsigned type, string const& text) -> string;
auto gettext(RuleType::Counting type) -> string;
auto gettext(Counting counting) -> string;

auto operator<<(ostream& ostr, RuleType::Bool type) -> ostream&;
auto operator<<(ostream& ostr, RuleType::Unsigned type) -> ostream&;
auto operator<<(ostream& ostr, RuleType::UnsignedExtra type) -> ostream&;
auto operator<<(ostream& ostr, RuleType::Counting type) -> ostream&;
auto operator<<(ostream& ostr, Counting counting) -> ostream&;

