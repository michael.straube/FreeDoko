/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "status.h"


auto to_string(PartyStatus const party_status) noexcept -> string
{
  switch (party_status) {
  case PartyStatus::programstart:
    (void)_("PartyStatus::programstart");
    return "programstart";
  case PartyStatus::quit:
    (void)_("PartyStatus::quit");
    return "quit";
  case PartyStatus::init:
    (void)_("PartyStatus::init");
    return "init";
  case PartyStatus::initial_loaded:
    (void)_("PartyStatus::initial loaded");
    return "initial loaded";
  case PartyStatus::loaded:
    (void)_("PartyStatus::loaded");
    return "loaded";
  case PartyStatus::play:
    (void)_("PartyStatus::play");
    return "play";
  case PartyStatus::finished:
    (void)_("PartyStatus::finished");
    return "finished";
  } // switch(party_status)

  return {};
}


auto gettext(PartyStatus const party_status) -> string
{
  return gettext("PartyStatus::" + to_string(party_status));
}


auto operator<<(ostream &ostr, PartyStatus const party_status) -> ostream &
{
  ostr << ::to_string(party_status);
  return ostr;
}
