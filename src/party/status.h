/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

enum class PartyStatus {
  programstart,
  init,
  initial_loaded,
  loaded,
  play,
  finished,
  quit
}; // enum class PartyStatus

auto to_string(PartyStatus party_status) noexcept -> string;
auto gettext(PartyStatus party_status)            -> string;
auto operator<<(ostream& ostr, PartyStatus party_status) -> ostream&;


