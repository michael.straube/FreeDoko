/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "ai.h"

#include "../../game/game.h"
#include "../../party/rule.h"
#include "../../card/trick.h"


AiRandom::AiRandom(Name const name) :
  Player(Player::Type::ai_random, name)
{ }


AiRandom::AiRandom(Player const& player) :
  Player(player)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai_random);
}


AiRandom::AiRandom(Player const& player, Aiconfig const& aiconfig) :
  Player(player),
  Aiconfig(aiconfig)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai_random);
}

auto AiRandom::clone() const -> unique_ptr<Player>
{
  return make_unique<AiRandom>(*this);
}


auto AiRandom::get_reservation() -> Reservation const&
{
  reservation() = Player::get_default_reservation();

  if (   (   game().is_duty_solo()
          && *this == game().startplayer())
      || ::random_value(game().playerno() * game().playerno()) == 0) {
    vector<GameType> soli;
    for (auto t : game_type_list) {
      if (is_real_solo(t)
          && game().rule(t) ) {
        soli.push_back(t);
      }
    }
    if (!soli.empty()) {
      reservation().game_type = soli[::random_value(soli.size())];
    }
  }

  return reservation();
}


auto AiRandom::card_get() -> HandCard
{
  HandCard card;
  do {
    card = hand().card(::random_value(hand().cardsnumber()));
  } while (!game().tricks().current().isvalid(card));

  return card;
}


auto AiRandom::announcement_request() const -> Announcement
{
  // points of the own team
  unsigned points = 0;

  // count the points, the own team has made
  for(auto const& player : game().players()) {
    if (game().teaminfo().get(player) == team())
      points += game().points_of_player(player);
  }

  if (points == 240)
    return Announcement::no0;
  if (points > 240 - 30)
    return Announcement::no30;
  if (points > 240 - 60)
    return Announcement::no60;
  if (points > 240 - 90)
    return Announcement::no90;
  if (points > 240 - 120)
    return Announcement::no120;

  return Announcement::noannouncement;
}


auto AiRandom::poverty_shift() -> HandCards
{
  HandCards hand_cards = hand().cards();
  HandCards cards_to_shift(hand());

  // first add all trump
  for (auto c = hand_cards.begin(); c != hand_cards.end(); ) {
    if (c->istrump()) {
      cards_to_shift.push_back(*c);
      c = hand_cards.erase(c);
    } else {
      ++c;
    }
  }

  // then fill up with cards randomly chosen
  while (cards_to_shift.size() < hand().count_poverty_cards()) {
    auto const i = ::random_value(hand_cards.size());
    cards_to_shift.push_back(hand_cards[i]);
    hand_cards.erase(hand_cards.begin() + i);
  }

  hand().remove(cards_to_shift);

  return cards_to_shift;
}


auto AiRandom::poverty_take_accept(unsigned const cardno) -> bool
{
  return false;
}


auto AiRandom::poverty_cards_change(vector<Card> const& cards) -> HandCards
{
  DEBUG_ASSERTION(false,
                  "AiRandom::poverty_cards_change(cards)\n"
                  "  function should not be called!");
  return HandCards(hand());
}


void AiRandom::poverty_cards_get_back(vector<Card> const& cards)
{
  hand().add(cards);
}
