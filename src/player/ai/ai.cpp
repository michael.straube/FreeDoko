/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "ai.h"
#include "solo_decision.h"

#include "announcement.h"
#include "heuristic.h"
#include "../team_information.h"
#include "../../basetypes/program_flow_exception.h"
#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../card/trick.h"
#include "../../game/gameplay_actions.h"
#include "../../os/bug_report_replay.h"
#ifdef CHECK_RUNTIME
#include "../../runtime.h"
#endif

/** standard constructor
 **/
Ai::Ai() :
  Player(Player::Type::ai, "")
{
  db_ = make_unique<AiDb>();
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);
  for (auto const game_type : ::game_type_solo_list) {
    solo_decisions_.emplace_back(SoloDecision::create(game_type, *this));
  }
}

/** constructor
 **/
Ai::Ai(Name const name) :
  Player(Player::Type::ai, name)
{
  db_ = make_unique<AiDb>();
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);
  for (auto const game_type : ::game_type_solo_list) {
    solo_decisions_.emplace_back(SoloDecision::create(game_type, *this));
  }
}

/** constructor
 **
 ** @param    aiconfig   ai configuration
 **
 ** @result    -
 **/
Ai::Ai(Aiconfig const& aiconfig) :
  Player(Player::Type::ai, ""),
  Aiconfig(aiconfig)
{
  db_ = make_unique<AiDb>();
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);
  for (auto const game_type : ::game_type_solo_list) {
    solo_decisions_.emplace_back(SoloDecision::create(game_type, *this));
  }
}

/** copy constructor
 **
 ** @param    player   player to copy
 **/
Ai::Ai(Player const& player) :
  Player(player)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai);
  db_ = make_unique<AiDb>(player.db());
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);
  for (auto const game_type : ::game_type_solo_list) {
    solo_decisions_.emplace_back(SoloDecision::create(game_type, *this));
  }
}

/** copy constructor
 **
 ** @param    player   player to copy
 ** @param    aiconfig   configuration to copy
 **/
Ai::Ai(Player const& player, Aiconfig const& aiconfig) :
  Player(player),
  Aiconfig(aiconfig)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai);

  if (::party->in_game()) {
    DEBUG_ASSERTION(false,
                    "Ai::Ai(player):\n"
                    "  in game");
  }

  db_ = make_unique<AiDb>(player.db());
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);
  for (auto const game_type : ::game_type_solo_list) {
    solo_decisions_.emplace_back(SoloDecision::create(game_type, *this));
  }
}

/** construktor
 **
 ** @param    istr   stream with the infos
 **
 ** @todo      using constructor Aiconfig(istr)
 ** @todo      update to the new format
 **/
Ai::Ai(istream& istr) :
  Player(Player::Type::ai, "")
{
  db_ = make_unique<AiDb>();
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);

  while (istr.good()) {
    string line;
    getline(istr, line);
    if (istr.fail() || istr.eof())
      break;

    if (*line.rbegin() == '\r')
      line.erase(line.end() - 1);

    if (   line.empty()
        || line[0] == '#') {
      continue;
    }
    if (   line == "Aiconfig"
        || line == "aiconfig") {
      // this is the last entry
      Aiconfig::read(istr);
      break;
    } else if (line == "Database") {
      db_->read(istr);
    } else {
      cerr << "Reading the ai:\n"
        << "found following unknown line:\n"
        << line << '\n'
        << "ignoring it."
        << endl;
#ifndef RELEASE
    std::exit(EXIT_FAILURE);
#endif
    }
  }; // while (istr.good())
}

/** copy constructor
 **/
Ai::Ai(Ai const& ai) :
  Player(ai),
  Aiconfig(ai),
  silent_marriage_(ai.silent_marriage_),
  last_heuristic_(ai.last_heuristic_),
  last_heuristic_rationale_(ai.last_heuristic_rationale_)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai);
  db_ = make_unique<AiDb>(ai.db());
  team_information_  = make_unique<TeamInformation>(ai.team_information());
  cards_information_ = make_unique<CardsInformation>(ai.cards_information());
  for (auto const game_type : ::game_type_solo_list) {
    solo_decisions_.emplace_back(SoloDecision::create(game_type, *this));
  }
}

/** destructor
 **/
Ai::~Ai() = default;

/** clone the player
 **
 ** @return   pointer of a clone
 **/
unique_ptr<Player>
Ai::clone() const
{
  return make_unique<Ai>(*this);
}

/** @return   the database
 **/
auto Ai::db() const noexcept -> AiDb const&
{ return dynamic_cast<AiDb const&>(*db_); }

/** @return   the database
 **/
auto Ai::db() noexcept -> AiDb&
{ return dynamic_cast<AiDb&>(*db_); }

/** writes the ai into the stream
 **
 ** @param    ostr   stream the ai is to be written in
 **
 ** @return   the output stream
 **/
ostream&
Ai::write(ostream& ostr) const
{
  // output of the name, type and database
  Player::write(ostr);

  if (difficulty() != Difficulty::custom) {
    ostr << "difficulty = " << difficulty() << '\n';
    return ostr;
  }

  // output of the configuration
  ostr << '\n'
    << "Aiconfig\n"
    << "{\n";
  Aiconfig::write(ostr);
  ostr << "}\n";

  return ostr;
}

/** read the config of the player
 **
 ** @param    config   configuration to read
 ** @param    istr   input stream
 **
 ** @return   whether the configuration was valid
 **/
bool
Ai::read(Config const& config, istream& istr)
{
  if (   !config.separator
      && (   (config.name == "Aiconfig")
          || (config.name == "aiconfig") ) ) {
    return Aiconfig::read(istr);
  }
  if (config.name == "difficulty") {
    set_to_difficulty(aiconfig_difficulty_from_string(config.value));
    return true;
  }

  return Player::read(config, istr);
}

/** sets the game of the ai
 **
 ** @param    game   the new game for the ai
 **/
void
Ai::set_game(Game& game)
{
  //bool const old_isvirtual = game().isvirtual();
  Player::set_game(game);
}

/** sets the hand of the ai
 **
 ** @param    hand   new hand
 **
 ** @return   the hand
 **/
void Ai::set_hand(const Hand& hand)
{
  Player::set_hand(hand);
  if (game().status() <= Game::Status::init)
    return ;
  cards_information().set_hand(*this, hand);
  team_information().update_teams();
}

/** the rule has changed
 **
 ** @param    type        rule type to have changed
 ** @param    old_value   old value of the rule
 **/
void
Ai::rule_changed(int const type, void const* const old_value)
{
  Aiconfig::rule_changed(type, old_value);
}

/** the game 'game' is opened
 **
 ** @param    game    opened game
 **/
void
Ai::game_open(Game& game)
{
  Player::game_open(game);
  team_information().reset();
  cards_information().reset();
}

/** the game is started
 **/
void
Ai::game_start()
{
  if (game().status() == Game::Status::redistribute)
    return ;

  Player::game_start();

  { // in a poverty the hand can have changed
    cards_information().reset();
    cards_information().set_hand(*this, hand());
  }

  team_information().game_start();
  cards_information().game_start();
  team_information().update_teams();
}

#if 0
/** -> result
 ** - if 'Aiconfig::hands_known' the real hand of the player is returned
 **   (independent on 'Rule::Type::show_all_hands')
 ** - if not 'Aiconfig::hands_known' the hand is calculated,
 **   see 'CardsInformation'
 **
 ** @param    player   player whose hand is returned
 **
 ** @result   hand of 'player'
 **/
Hand const&
Ai::handofplayer(Player const& player) const
{
  if (   !game().isvirtual()
      && (player == *this))
    return hand();

  if (Aiconfig::value(Aiconfig::Type::hands_known))
    return player.hand();

  // If it is the last trick and the player has already played a card,
  // his hand is empty.
  if (   (::game_status == Game::Status::play)
      && (game().tricks().real_remaining_no() == 1)
      && (game().tricks().current().cardno_of_player(player)
          < game().tricks().current().actcardno()))
    return player.hand();

#ifndef RELEASE
  Hand const& hand = cards_information().estimated_hand(player);
  hand.self_check();
  return hand;
#else
  return cards_information().estimated_hand(player);
#endif
}

/** the known team of the player
 **/
Team
Ai::teamofplayer(Player const& player) const
{
  // In the marriage case, as long as the teams are not determined,
  // the player shall think that every other player is against him.
  if (game().type() == GameType::marriage)
    return teaminfo(player);

  if (Aiconfig::value(Aiconfig::Type::teams_known))
    return player.team();

  return teaminfo( player );
}
#endif


void Ai::set_teams(vector<Team> const& teams)
{
  team_information().set_teams(teams);
  set_team(teams[no()]);
}


/** @return   card played by the ai
 **/
HandCard
Ai::card_get()
{
  auto const& current_trick = game().tricks().current();
  auto card = HandCard(hand(), nextcard(current_trick));

  auto& swines = game().swines();
  // Bevor ein Hyperschwein ausgespielt werden darf, müssen Schweine angesagt/ausgespielt sein
  if (   card.possible_hyperswine()
      && swines.hyperswines_announcement_valid(*this)
      && !swines.swines_announced()
      && swines.swines_announcement_valid(*this)) {
    card = game().cards().swine();
  }
  if (   card.possible_swine()
      && swines.swines_announcement_valid(*this))
    swines.swines_announce(*this);
  if (   card.possible_hyperswine()
      && swines.hyperswines_announcement_valid(*this))
    swines.hyperswines_announce(*this);

#ifdef BUG_REPORT_REPLAY
  if (   ::bug_report_replay
      && ::bug_report_replay->auto_action())
    return card;
#endif

  if (game().announcements().is_valid(Announcement::no120, *this)) {
    auto announcement_heuristic = AnnouncementHeuristic::create_to_show_team(*this, *this, current_trick, card);
    if (announcement_heuristic) {
      auto const make_announcement = announcement_heuristic->make_announcement(current_trick);
      auto const announcement = announcement_heuristic->announcement();
      last_announcement_rationale_ = announcement_heuristic->rationale();
      if (   ::debug(name())
          || ::debug("announcement")
          || ::debug(name() + " announcement")
          || ::debug(std::to_string(no()) + " announcement")
          || ::debug(to_string(announcement))
          || ::debug(name() + " " + to_string(announcement))
          || ::debug(std::to_string(no()) + " " + to_string(announcement))
          || ::debug(_(announcement))
          || ::debug(name() + " " + _(announcement))
          || ::debug(std::to_string(no()) + " " + _(announcement))
         ) {
        DEBUG("") << name() << ": " << announcement << "\n"
          << announcement_heuristic->rationale() << '\n';
      }
      if (make_announcement) {
        game().announcements().make_announcement(announcement, *this);
      }
    }
  }

  return card;
}

/** @return   suggested card
 **/
HandCard
Ai::card_suggestion()
{
#ifdef BUG_REPORT_REPLAY
  if (::bug_report_replay) {
    static bool bug_report_flip = false;
    if (last_heuristic_ != Aiconfig::Heuristic::bug_report)
      bug_report_flip = true;
    else
      bug_report_flip ^= true;
    if (bug_report_flip) {
      set_last_heuristic_to_bug_report();
      auto const card = ::bug_report_replay->next_card(*this);
      if (   !card.is_empty()
          && hand().contains(card)) {
        return HandCard(hand(), card);
      }
    }
  }
#endif
  auto const& current_trick = game().tricks().current();
  auto const card = nextcard(current_trick);
#ifndef RELEASE
#ifdef DKNOF
  cout << "card suggestion: " << card << '\n'
    << last_heuristic() << '\n'
    << last_heuristic_rationale() << '\n';
#endif
#endif
  return HandCard(hand(), card);
}

/** check a swines announcement at the game start
 **/
void
Ai::check_swines_announcement_at_game_start()
{
#ifdef BUG_REPORT_REPLAY
  if (   ::bug_report_replay
      && ::bug_report_replay->auto_action()
      && (::bug_report_replay->current_action().type()
          == GameplayActions::Type::swines)
      && (dynamic_cast<GameplayActions::Swines const&>(::bug_report_replay->current_action()).player
          == no())
     ) {
    game().swines().swines_announce(*this);
  }
#endif

  if (type() == Player::Type::ai) {
    // announce swines
    if (game().rule(Rule::Type::swines_announcement_begin))
      if (game().swines().swines_announcement_valid(*this))
        game().swines().swines_announce(*this);

    // hyperswines are announced by 'Ai::swines_announced(player)'
  } // if (type() == Player::Type::ai)
}

/** 'player' has announce swines
 ** if possible announce hyperswines
 **
 ** @param    player   player that has announced the swines
 **/
void
Ai::swines_announced(Player const& player)
{
  this->Player::swines_announced(player);

#ifdef BUG_REPORT_REPLAY
  if (   ::bug_report_replay
      && ::bug_report_replay->auto_action()
      && (::bug_report_replay->current_action().type()
          == GameplayActions::Type::hyperswines)
      && (dynamic_cast<GameplayActions::Hyperswines const&>(::bug_report_replay->current_action()).player
          == no())
     ) {
    game().swines().hyperswines_announce(*this);
  }
#endif

  if (type() == Player::Type::ai) {
    if (   game().swines().hyperswines_announcement_valid(*this)
        && game().rule(Rule::Type::hyperswines_announcement_begin))
      game().swines().hyperswines_announce(*this);
  } // if (type() == Player::Type::ai)
}

/** selects a card to play
 **
 ** @param    trick   current trick
 **
 ** @return   card to play
 **/
Card
Ai::nextcard(Trick const& trick)
{
  if (!game().isvirtual())
    db().increase(Aiconfig::Heuristic::no_heuristic);

  DEBUG_ASSERTION(!hand().empty(),
                  "Ai::nextcard(trick)\n"
                  "  The hand is empty");

#ifdef CHECK_RUNTIME
  auto const ssp = (  !game().isvirtual()
                    ? ::runtime["ai heuristics"].start_stop_proxy()
                    : nullptr);
#endif

  auto heuristics_vector = (  silent_marriage_
                            ? (heuristic_states(HeuristicsMap::GameTypeGroup::marriage_silent,
                                                      HeuristicsMap::PlayerTypeGroup::re))
                            : heuristic_states(HeuristicsMap::Key(*this)));
  heuristics_vector.insert(heuristics_vector.begin(),
                           {Heuristic::only_one_valid_card, true});
  heuristics_vector.insert(heuristics_vector.begin(),
                           {Heuristic::bug_report, true});
  for (auto const& heuristic_state : heuristics_vector) {
    if (!heuristic_state.active)
      continue;
    auto const heuristic = heuristic_state.heuristic;
    if (heuristic == Aiconfig::Heuristic::error) {
      continue;
    }

#ifdef RELEASE
    if (!::Heuristic::is_valid(heuristic, *this))
      continue;
#else
    DEBUG_ASSERTION(::Heuristic::is_valid(heuristic, *this),
                    "Heuristic " << heuristic << " is not valid for " << name()
                    << " (" << HeuristicsMap::group(game()) << " / " << HeuristicsMap::group(*this) << ")");
#endif
    auto heuristic_logic = ::Heuristic::create(heuristic, *this);
    DEBUG_ASSERTION(heuristic_logic->heuristic() == heuristic,
                    "Heuristic " << heuristic << " is of the wrong type: " << heuristic_logic->heuristic());
    heuristic_logic->set_player(*this);
    auto const card = heuristic_logic->get_card(trick);
    if (   !game().isvirtual()
        && (   ::debug(name())
            || ::debug("heuristics")
            || ::debug(name() + " heuristics")
            || ::debug(std::to_string(no()) + " heuristics")
            || ::debug(to_string(heuristic))
            || ::debug(name() + " " + to_string(heuristic))
            || ::debug(std::to_string(no()) + " " + to_string(heuristic))
            || ::debug(_(heuristic))
            || ::debug(name() + " " + _(heuristic))
            || ::debug(std::to_string(no()) + " " + _(heuristic))
           )) {
      DEBUG("") << no() << ' ' << name() << ": " << card << " (" << heuristic << ")\n"
        << heuristic_logic->rationale();
    }

    if (!card) {
      if (is_real(heuristic))
        game().signal_ai_heuristic_failed(heuristic, heuristic_logic->rationale());
      continue;
    }

    if (   heuristic == Aiconfig::Heuristic::try_for_doppelkopf
        && announcement() == Announcement::noannouncement
        && !trick.islastcard()
        && !(teaminfo(trick.player_of_card(game().playerno())) >= maybe(opposite(team())))) {
      // make an announcement so that the player behind can give points
      game().announcements().make_announcement(Announcement::no120, *this);
    }

    last_heuristic_ = heuristic;
    last_heuristic_rationale_ = heuristic_logic->rationale();
    db().increase(heuristic);

    return card;
  } // for (heuristic_state : heuristics_vector)

  last_heuristic_ = Aiconfig::Heuristic::no_heuristic;
  last_heuristic_rationale_.clear();
  return hand().validcards(trick)[0];
}

/** @return   the last heuristic
 **/
Aiconfig::Heuristic
Ai::last_heuristic() const
{
  return last_heuristic_;
}

/** @return   the rationale of the last heuristic
 **/
Rationale const&
Ai::last_heuristic_rationale() const
{
  return last_heuristic_rationale_;
}

/** set the last heuristic to manual
 **/
void
Ai::set_last_heuristic_to_manual()
{
  last_heuristic_ = Aiconfig::Heuristic::manual;
  last_heuristic_rationale_.clear();
}

/** set the last heuristic to bug report
 **/
void
Ai::set_last_heuristic_to_bug_report()
{
  last_heuristic_ = Aiconfig::Heuristic::bug_report;
  last_heuristic_rationale_.clear();
}

/** @return   the rationale of the last announcement
 **/
Rationale const&
Ai::last_announcement_rationale() const
{
  return last_announcement_rationale_;
}

/** initialize the ai for a new game
 **/
void
Ai::new_game(Game& game)
{
  Player::new_game(game);

  silent_marriage_ = false;
}

/** @return   tricktype which determines teams for a marraige
 **/
MarriageSelector
Ai::marriage_selector() const
{
  auto const& rule = game().rule();
  auto const& hand = this->hand();

  if (rule(Rule::Type::marriage_first_foreign))
    return MarriageSelector::first_foreign;

  if (   rule(Rule::Type::marriage_first_trump)
      && !rule(Rule::Type::marriage_first_color)
      && !rule(Rule::Type::marriage_first_single_color)) {
    return MarriageSelector::first_trump;
  }
  if (   !rule(Rule::Type::marriage_first_trump)
      && rule(Rule::Type::marriage_first_color)) {
    return MarriageSelector::first_color;
  }

  if (   rule(Rule::Type::marriage_first_color)
      && hand.count(Card::trump) + 4 <= hand.cardsnumber()) {
    return MarriageSelector::first_color;
  }
  if (   rule(Rule::Type::marriage_first_trump)
      && hand.count(Card::trump) + 4 > hand.cardsnumber()) {
    return MarriageSelector::first_trump;
  }

  auto const club_no  = hand.count(Card::club)  - hand.count(Card::club_ace);
  auto const spade_no = hand.count(Card::spade) - hand.count(Card::spade_ace);
  auto const heart_no = hand.count(Card::heart) - hand.count(Card::heart_ace);
  if (   !rule(Rule::Type::marriage_first_single_color)) {
    if (club_no + spade_no + heart_no > 0) {
      return MarriageSelector::first_color;
    }
    return MarriageSelector::first_trump;
  }

  if (club_no + spade_no + heart_no > 0) {
    if (club_no >= spade_no && club_no >= heart_no) {
      return MarriageSelector::first_club;
    }
    if (spade_no >= club_no && spade_no >= heart_no) {
      return MarriageSelector::first_spade;
    }
    if (heart_no >= club_no && heart_no >= spade_no) {
      return MarriageSelector::first_heart;
    }
  }

  if (rule(Rule::Type::marriage_first_trump)) {
    return MarriageSelector::first_trump;
  }

  if (club_no >= spade_no && club_no >= heart_no) {
    return MarriageSelector::first_club;
  }
  if (spade_no >= club_no && spade_no >= heart_no) {
    return MarriageSelector::first_spade;
  }
  if (heart_no >= club_no && heart_no >= spade_no) {
    return MarriageSelector::first_heart;
  }

  return MarriageSelector::first_foreign;
}

/** -> result
 **
 ** @param    isDuty   whether the player must play a duty solo
 **                     (has no effect, yet)
 **
 ** @result    the reservation of the ai
 **/
Reservation const&
Ai::get_reservation()
{
  auto& reservation = this->reservation();

#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()
        && (::bug_report_replay->current_action().type()
            == GameplayActions::Type::reservation)
        && (dynamic_cast<GameplayActions::Reservation const&>(::bug_report_replay->current_action()).player
            == no())
       ) {
      return (reservation = dynamic_cast<GameplayActions::Reservation const&>(::bug_report_replay->current_action()).reservation);
    } // if (auto execute)
  }
#endif

  reservation = get_default_reservation();

  auto const& rule = game().rule();

#ifdef TODO
  // ToDo: auf die neue Schnittstelle umstellen
  // check, whether to announce 'richness'
  if (reservation.game_type == GameType::thrown_richness) {
    if (Heuristics::make_announcement(*this))
      reservation.game_type = GameType::normal;
  }
#endif

  { // decide solo game
    SoloDecision::Points max_points = 120;
    if (   game().is_duty_solo()
        && *this == game().startplayer()) {
      max_points = INT_MIN;
    }
    GameType best_game_type = GameType::normal;
    // first all duty soli
    for (auto& decision : solo_decisions_) {
      auto const game_type = decision->game_type();
      if (!rule(game_type))
        continue;
      if (!is_remaining_duty_solo(game_type))
        continue;
      decision->set_player(*this);
      auto const points = decision->estimated_points();
      if (   ::debug(name())
          || ::debug("solo")
          || ::debug(name() + " solo")
          || ::debug(std::to_string(no()) + " solo")) {
        DEBUG("") << name() << ": " << decision->game_type() << ": " << points << " points\n"
          << decision->rationale() << '\n';
      }
      if (points > max_points) {
        best_game_type = game_type;
        max_points = points;
        if (   ::debug(name())
            || ::debug("solo")
            || ::debug(name() + " solo")
            || ::debug(std::to_string(no()) + " solo")) {
          DEBUG("") << name() << ": " << decision->game_type() << ": " << points << " points\n"
            << decision->rationale() << '\n';
        }
      }
    }
    max_points = 120;
    if (best_game_type == GameType::normal) {
      // lust solo games
      for (auto& decision : solo_decisions_) {
        auto const game_type = decision->game_type();
        if (!rule(game_type))
          continue;
        if (is_remaining_duty_solo(game_type))
          continue;
        decision->set_player(*this);
        auto const points = decision->estimated_points();
        if (   ::debug(name())
            || ::debug("solo")
            || ::debug(name() + " solo")
            || ::debug(std::to_string(no()) + " solo")
            || ::debug(name() + " " + to_string(decision->game_type()))
           ) {
          DEBUG("") << name() << ": " << decision->game_type() << ": " << points << " points\n"
            << decision->rationale() << '\n';
        }
        if (points > max_points) {
          best_game_type = game_type;
          max_points = points;
        }
      }
    }
    if (best_game_type != GameType::normal) {
      reservation.game_type = best_game_type;
      if (is_color_solo(best_game_type)) {
        if (   rule(Rule::Type::swines_in_solo)
            && rule(Rule::Type::swines_announcement_begin)
            && hand().count(Card(trumpcolor(best_game_type), Card::ace)) == 2) {
          reservation.swines = true;
          if (   rule(Rule::Type::hyperswines_in_solo)
              && rule(Rule::Type::hyperswines_announcement_begin)
              && hand().count(Card(trumpcolor(best_game_type),
                                         rule(Rule::Type::with_nines) ? Card::nine : Card::king)
                                   ) == 2) {
            reservation.hyperswines = true;
          }
        }
      }
    }
  }

  if (reservation.game_type == GameType::marriage) {
    reservation.marriage_selector = marriage_selector();
    DEBUG_ASSERTION(rule(reservation.marriage_selector),
                    "Ai::reservation()\n"
                    "   marriage selector " << reservation.marriage_selector << " is not valid");
  }

  // special case: with swines announced the player does not have a poverty
  if (   reservation.game_type == GameType::poverty
      && (hand().count_poverty_cards()
          > rule(Rule::Type::max_number_of_poverty_trumps)) ) {
    reservation.swines = false;
  }

  return reservation;
}

/** @return   the points the team has made
 **/
unsigned
Ai::points_of_team() const
{
  unsigned points = game().points_of_player(*this);

  if (team_information().all_known()) {
    for (unsigned p = 0; p < game().playerno(); ++p)
      if (   p != no()
          && team_information().team(p) >= surely(team()))
        points += game().points_of_player(p);
  } else if (team_information().guessed_partner() != nullptr) {
    // normal game, partner guessed
    points += game().points_of_player(*team_information().guessed_partner());
  } else { // if (no partner known)
    // add the lowest points of the players
    unsigned lowest_points = 240;
    for (unsigned p = 0; p < game().playerno(); ++p)
      if (   (p != no())
          && (game().points_of_player(p) < lowest_points))
        lowest_points = game().points_of_player(p);
    points += lowest_points;
  } // if (no partner known)

  return points;
}

/** -> result
 ** (uses Heuristics)
 **
 ** @see        Heuristics::make_announcement()
 ** @see        Heuristics::say_no90
 ** @see        Heuristics::say_no60
 ** @see        Heuristics::say_no30
 ** @see        Heuristics::say_no0
 **
 ** @return   announcement of the player
 **/
Announcement
Ai::announcement_request() const
{
#ifdef BUG_REPORT_REPLAY
  if (   ::bug_report_replay
      && ::bug_report_replay->auto_action()) {
    if (   (::bug_report_replay->current_action().type()
            == GameplayActions::Type::announcement)
        && (dynamic_cast<GameplayActions::Announcement const&>(::bug_report_replay->current_action()).player
            == no())
       ) {
      last_announcement_rationale_ = _("Announcement::bug report");
      return dynamic_cast<GameplayActions::Announcement const&>(::bug_report_replay->current_action()).announcement;
    } // if (current action = announcement)
    return Announcement::noannouncement;
  }
#endif

  if (!is_real(team()))
    return Announcement::noannouncement;

  auto announcement_heuristic = AnnouncementHeuristic::create(*this, *this, game().tricks().current());
  if (!announcement_heuristic)
    return Announcement::noannouncement;
  auto const announcement = announcement_heuristic->announcement();
  auto const make_announcement = announcement_heuristic->make_announcement(game().tricks().current());
  last_announcement_rationale_ = announcement_heuristic->rationale();
  if (   ::debug(name())
      || ::debug("announcement")
      || ::debug(name() + " announcement")
      || ::debug(std::to_string(no()) + " announcement")
      || ::debug(to_string(announcement))
      || ::debug(name() + " " + to_string(announcement))
      || ::debug(std::to_string(no()) + " " + to_string(announcement))
      || ::debug(_(announcement))
      || ::debug(name() + " " + _(announcement))
      || ::debug(std::to_string(no()) + " " + _(announcement))
     ) {
    DEBUG("") << name() << ": " << announcement << "\n"
      << announcement_heuristic->rationale() << '\n';
  }
  if (make_announcement)
    return announcement;
  return Announcement::noannouncement;
}

auto Ai::trump_card_limit() const -> HandCard
{
  if (silent_marriage_)
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_solocolor)};

  switch (game().type()) {
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_solocolor)};
  case GameType::solo_jack:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_solojack)};
  case GameType::solo_queen:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_soloqueen)};
  case GameType::solo_king:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_soloking)};
  case GameType::solo_queen_jack:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_solojackqueen)};
  case GameType::solo_king_jack:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_solojackking)};
  case GameType::solo_king_queen:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_soloqueenking)};
  case GameType::solo_koehler:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_solokoehler)};
  case GameType::solo_meatless:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_meatless)};
  default:
    return {hand(), Aiconfig::value(Aiconfig::Type::trumplimit_normal)};
  }
}

auto Ai::lowest_trump_card_limit() const -> HandCard
{
  if (silent_marriage_)
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_solocolor)};

  switch (game().type()) {
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_solocolor)};
  case GameType::solo_jack:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_solojack)};
  case GameType::solo_queen:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_soloqueen)};
  case GameType::solo_king:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_soloking)};
  case GameType::solo_queen_jack:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_solojackqueen)};
  case GameType::solo_king_jack:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_solojackking)};
  case GameType::solo_king_queen:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_soloqueenking)};
  case GameType::solo_koehler:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_solokoehler)};
  case GameType::solo_meatless:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_meatless)};
  default:
    return {hand(), Aiconfig::value(Aiconfig::Type::lowest_trumplimit_normal)};
  }
}
