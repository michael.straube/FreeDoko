/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../playersDb.h"
#include "../aiconfig.h"

class AiDb : public PlayersDb {
public:
  AiDb();
  explicit AiDb(istream& istr);
  explicit AiDb(PlayersDb const& players_db);
  AiDb(AiDb const& aidb);
  ~AiDb() override;

  void write(ostream& ostr) const override;
private:
  auto read_group(string const& name, istream& istr) -> bool override ;
  void read_group_heuristics(istream& istr);

public:
  auto heuristics_group_general()       const -> unsigned;
  auto heuristics_group_marriage()      const -> unsigned;
  auto heuristics_group_poverty()       const -> unsigned;
  auto heuristics_group_solo()          const -> unsigned;
  auto heuristics_group_solo_color()    const -> unsigned;
  auto heuristics_group_solo_picture()  const -> unsigned;
  auto heuristics_group_solo_meatless() const -> unsigned;

  /// how often each statistic was used
  auto heuristic(Aiconfig::Heuristic heuristic) const -> unsigned;

  void increase(Aiconfig::Heuristic heuristic);

  void clear() override;

private:
  mutable std::map<Aiconfig::Heuristic, unsigned> heuristics_;
}; // class AiDb : public PlayersDb
