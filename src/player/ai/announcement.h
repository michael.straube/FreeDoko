/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "decision_in_game.h"

/** Base class for announcement heuristics
 **/
class AnnouncementHeuristic : public DecisionInGame {
public:
static unique_ptr<AnnouncementHeuristic> create(Ai const& ai, Player const& player, Trick const& trick);
static unique_ptr<AnnouncementHeuristic> create_to_show_team(Ai const& ai, Player const& player, Trick const& trick, HandCard card_to_play);

private:
static unique_ptr<AnnouncementHeuristic> create(Ai const& ai, Player const& player, Announcement announcement);

public:
AnnouncementHeuristic(Ai const& ai, Player const& player, Announcement announcement);

~AnnouncementHeuristic() override;

Announcement announcement() const;

bool make_announcement(Trick const& trick);
virtual bool conditions_met(Trick const& trick) = 0;

protected:
bool make_announcement();
bool say_no90();
bool say_no60();
bool say_no30();
bool say_no0();

int hand_value();

bool opposite_team_can_win_trick(Trick const& trick);
HandCard highest_trump_of_opposite_team();

private:
bool jabbed_by_guessed_partner(Card::TColor color) const;
bool jabbed_by_not_guessed_partner(Card::TColor color) const;

protected:
Announcement const announcement_;
};
