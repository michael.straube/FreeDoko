/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../announcement.h"

namespace AnnouncementHeuristics::Normal {

class Base;

auto create(Ai const& ai, Player const& player, Announcement announcement) -> unique_ptr<Base>;

/** Base class for announcement heuristics for normal games
 **/
class Base : public AnnouncementHeuristic {
public:
  Base(Ai const& ai, Player const& player, Announcement announcement);

  ~Base() override;

  auto conditions_met(Trick const& trick) -> bool override = 0;

protected:
  auto re_announced()     -> bool;
  auto contra_announced() -> bool;

  auto other_has_hyperswines() -> bool;

  auto count_blank_colors(Trick const& trick) const  -> unsigned;
  auto count_color_aces(Trick const& trick)          -> unsigned;
  auto count_color_tricks_jabbed(Trick const& trick) -> unsigned;
  auto can_jab_current_trick(Trick const& trick)     -> bool;
  auto queen_value()                                 -> double;
};
} // namespace AnnouncementHeuristics::Normal
