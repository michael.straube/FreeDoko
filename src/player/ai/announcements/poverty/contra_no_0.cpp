/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "contra_no_0.h"

#include "../../ai.h"

namespace AnnouncementHeuristics::Poverty {

/** constructor
 **/
ContraNo0::ContraNo0(Ai const& ai,
       Player const& player) :
  Base(ai, player, Announcement::no0)
{ }

/** destructor
 **/
ContraNo0::~ContraNo0() = default;

/** whether the announcement should be made
 **/
bool
ContraNo0::conditions_met(Trick const& trick)
{
#ifndef OUTDATED
  if (!say_no0())
    return false;
  rationale_.add(_("Announcement::old logic"));
  return true;
#else
  // ToDo
  return false;
#endif
}

} // namespace AnnouncementHeuristics::Poverty
