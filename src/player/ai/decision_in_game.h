/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "decision.h"

class TeamInformation;
#include "../cards_information.h"
#include "../guessed_team.h"
#include "../../card/trick.h"
#include "../../basetypes/team.h"
#include "../../basetypes/announcement.h"

/** Base class for decisions in a running game
 **/
class DecisionInGame : public Decision {
public:
  explicit DecisionInGame(Ai const& ai);
  DecisionInGame(Decision const&) = delete;
  auto operator=(DecisionInGame const&) = delete;

  ~DecisionInGame() override;

  auto trump_card_limit()                       const -> Card;
  auto lowest_trump_card_limit()                const -> Card;
  auto below_trump_card_limit(Card card)        const -> bool;
  auto below_lowest_trump_card_limit(Card card) const -> bool;

  auto team_information()                          const -> TeamInformation const&;
  auto team()                                      const -> Team;
  auto is_soloplayer()                             const -> bool;
  auto own_team_known()                            const -> bool;
  auto own_team_guessed()                          const -> bool;
  auto partner()                                   const -> Player const&;
  auto guessed_team(Player const& player)          const -> GuessedTeam;
  auto same_team(Player const& player)             const -> bool;
  auto surely_same_team(Player const& player)      const -> bool;
  auto opposite_team(Player const& player)         const -> bool;
  auto guessed_same_team(Player const& player)     const -> bool;
  auto guessed_opposite_team(Player const& player) const -> bool;

  auto cards_information()                        const -> CardsInformation const& ;
  auto cards_information(Player const& player)    const -> CardsInformation::OfPlayer const&;
  auto color_runs(Card::TColor color)             const -> unsigned;
  auto color_runs()                               const -> unsigned;
  auto count_swines_and_hyperswines_of_own_team() const -> unsigned;
  auto guessed_hand(Player const& player)         const -> Hand const&;
  auto guessed_hand_of_soloplayer()               const -> Hand const&;

  auto is_winnerplayer(Trick const& trick) const -> bool;

  auto only_own_team_behind(Trick const& trick) const -> bool;

  auto points_of_own_team(bool with_current_trick = true)      -> Trick::Points;
  auto points_of_opposite_team(bool with_current_trick = true) -> Trick::Points;
  auto max_points_for_opposite_team()            -> Trick::Points;
  auto max_points_for_opposite_team_soloplayer() -> Trick::Points;
};
