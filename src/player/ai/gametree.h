/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "decision_in_game.h"

#include "../../card/hand.h"
#include "../aiconfig.h"
#include "../rating/rating.h"

#include <chrono>
using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;

/**
 ** tries all possibilities to play and take the card with the maximal worst card (max-min).
 ** In contrary to 'VirtualGames' a following player does not decide his
 ** card but all cards are tested.
 ** When used with heuristics, it is first testet, whether a heuristic does
 ** match, else all cards are tested.
 **
 ** @author     Diether Knof
 **
 ** @todo	use 'CardsInformation' for setting the hands in the recursion
 **/
class Gametree : public DecisionInGame {
  public:
    Gametree() = delete;
    explicit Gametree(Ai const& ai);
    Gametree(Gametree const&) = delete;
    Gametree& operator=(Gametree const&) = delete;

    ~Gametree() override;

    Rating::Type rating() const;

    // the best card according to the weighting
    Card best_card() const;

  private:
    int weighting(Card card, TimePoint time_limit) const;
    int weighting_of_distribution(Card card, Hands const& hands) const;

    int rated_modus(Game& virt_game) const;
    int full_trick_modi(Game& virt_game) const;
    int game_finished_modus(Game& virt_game) const;

    Card choose_best_prefiltered_card(Trick const& trick, Hand const& hand, HandCards const& cards) const;
    Card choose_best_prefiltered_color_card(Trick const& trick, Hand const& hand, HandCards const& cards) const;
    Card choose_best_prefiltered_trump_card(Trick const& trick, Hand const& hand, HandCards const& cards) const;

  private:
    Rating::Type rating_ = Rating::Type::max;
};
