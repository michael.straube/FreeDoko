/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "heuristic.h"
#include "ai.h"
#include "../player.h"
#include "../../party/rule.h"
#include "../team_information.h"
#include "../cards_information.h"
#include "../cards_information.of_player.h"

/** @return   whether a soloplayer is behind
 **/
bool
Heuristic::condition_soloplayer_behind(Trick const& trick)
{
  if (!is_solo(game().type())) {
    rationale_.add(_("Heuristic::condition::no solo game"));
    return false;
  } else if (soloplayer_behind(trick)) {
    rationale_.add(_("Heuristic::condition::solo player behind"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::solo player has already played"));
    return false;
  }
}

/** @return   whether the soloplayer has already played
 **/
bool
Heuristic::condition_soloplayer_has_played(Trick const& trick)
{
  if (!game().is_solo()) {
    rationale_.add(_("Heuristic::condition::no solo game"));
    return false;
  }
  auto const& soloplayer = game().players().soloplayer();
  if (trick.has_played(soloplayer)) {
    rationale_.add(_("Heuristic::condition::the soloplayer has already played"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::the soloplayer has not already played"));
    return false;
  }
}

/** @return   whether this is a marriage determination trick
 **/
bool
Heuristic::condition_is_marriage_determination_trick(Trick const& trick)
{
  auto const& game = this->game();
  if (!game.marriage().is_undetermined()) {
    rationale_.add(_("Heuristic::condition::no undetermined marriage"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::undetermined marriage"));
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  if (!is_selector(trick.startcard().tcolor(), game.marriage().selector())) {
    rationale_.add(_("Heuristic::condition::startcard is no marriage selector"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::startcard is marriage selector"));
  return true;
}

/** @return  whether this is the marriage decision trick and I am the soloplayer
 **/
bool
Heuristic::condition_marriage_decision_for_me(Trick const& trick)
{
  if (!condition_is_marriage_determination_trick(trick))
    return false;
  if (player().team() == Team::re) {
    rationale_.add(_("Heuristic::condition::marriage player"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no marriage player"));
    return false;
  }
}

/** @return   whether the player is of the given team
 **/
bool
Heuristic::condition(Team const team)
{
  if (player().team() == team) {
    rationale_.add(_("Heuristic::condition::team %s", _(team)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::not team %s", _(team)));
    return false;
  }
}

/** @return   whether this is the first trick
 **/
bool
Heuristic::condition_first_trick()
{
  if (game().tricks().current_no() == 0) {
    rationale_.add(_("Heuristic::condition::first trick"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::not first trick"));
    return false;
  }
}

/** @return   whether this is the startcard of the trick
 **/
bool
Heuristic::condition_startcard(Trick const& trick)
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no empty trick"));
    return false;
  }
}

/** @return   whether this is the second card in the trick
 **/
bool
Heuristic::condition_second_card(Trick const& trick)
{
  if (trick.actcardno() == 1) {
    rationale_.add(_("Heuristic::condition::second card"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::not second card"));
    return false;
  }
}

/** @return   whether this is the second last card in the trick
 **/
bool
Heuristic::condition_second_last_card(Trick const& trick)
{
  if (trick.remainingcardno() == 2) {
    rationale_.add(_("Heuristic::condition::second last card"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::not second last card"));
    return false;
  }
}

/** @return   whether this is the last card of the trick
 **/
bool
Heuristic::condition_last_card(Trick const& trick)
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no last card"));
    return false;
  }
}

/** @return   whether this is the first color run
 **/
bool
Heuristic::condition_first_color_run(Trick const& trick)
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  } else if (trick.startcard().istrump()) {
    rationale_.add(_("Heuristic::condition::trump trick"));
    return false;
  }
  auto const color = trick.startcard().tcolor();
  if (cards_information().color_runs(color) > 0) {
    rationale_.add(_("Heuristic::condition::color was already played"));
    return false;
  } else if (cards_information().played(color) > hand().played(color) + trick.count(color)) {
    rationale_.add(_("Heuristic::condition::somebody already has played %s", _(color)));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::first color run"));
    return true;
  }
}

/** @return   whether the trick is a trump trick
 **/
bool
Heuristic::condition_trump_trick(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  } else if (trick.startcard().istrump()) {
    rationale_.add(_("Heuristic::condition::trump trick"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::color trick"));
    return false;
  }
}

/** @return   whether this is a color trick
 **/
bool
Heuristic::condition_color_trick(Trick const& trick)
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  } else if (trick.startcard().istrump()) {
    rationale_.add(_("Heuristic::condition::trump trick"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::color trick"));
    return true;
  }
}

/** @return   whether this is a pure color trick
 **/
bool
Heuristic::condition_pure_color_trick(Trick const& trick)
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }

  for (auto const& card : trick) {
    if (card.istrump()) {
      rationale_.add(_("Heuristic::condition::trump in trick"));
      return false;
    }
  }
  rationale_.add(_("Heuristic::condition::pure color trick"));
  return true;
}

/** @return   whether the startcard is 'card'
 **/
bool
Heuristic::condition_startcard_is(Trick const& trick, Card const card)
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  if (trick.startcard() == card) {
    rationale_.add(_("Heuristic::condition::startcard is %s", _(card)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::startcard is not %s", _(card)));
    return false;
  }
}

/** @return   whether the partner is guessed
 **/
bool
Heuristic::condition_partner_guessed()
{
  if (team_information().maybe_partner()) {
    rationale_.add(_("Heuristic::condition::partner guessed"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no partner guessed"));
    return false;
  }
}

/** @return   whether the partner is behind the player
 **/
bool
Heuristic::condition_partner_behind(Trick const& trick)
{
  if (trick.game().is_solo() && team() == Team::re) {
    rationale_.add(_("Heuristic::condition::solo player"));
    return false;
  }
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return true;
  }
  for (auto const& player : trick.remaining_following_players()) {
    if (same_team(player)) {
      rationale_.add(_("Heuristic::condition::partner behind"));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no known partner behind"));
  return false;
}

/** @return   whether the guessed partner is behind the player
 **/
bool
Heuristic::condition_guessed_partner_behind(Trick const& trick)
{
  if (trick.game().is_solo()) {
    if (team() == Team::re) {
      rationale_.add(_("Heuristic::condition::solo player"));
      return false;
    }
    return condition_partner_behind(trick);
  }
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return true;
  }
  auto const partner = team_information().maybe_partner();
  if (!partner) {
    rationale_.add(_("Heuristic::condition::no partner guessed"));
    return false;
  }
  if (trick.cardno_of_player(*partner) < trick.actcardno()) {
    rationale_.add(_("Heuristic::condition::guessed partner in front"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::guessed partner behind"));
  return true;
}

/** @return   whether the next player is of the same team
 **/
bool
Heuristic::condition_next_player_same_team(Trick const& trick)
{
  if (trick.remainingcardno() == 0) {
    rationale_.add(_("Heuristic::condition::trick is full"));
    return false;
  }

  auto const actcardno = trick.actcardno();
  auto const& player = trick.player_of_card(actcardno + 1);
  if (same_team(player)) {
    rationale_.add(_("Heuristic::condition::next player of the same team"));
    return true;
  } else if (opposite_team(player)) {
    rationale_.add(_("Heuristic::condition::next player of the opposite team"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::team of the next player not known"));
    return false;
  }
}

/** @return   whether the next player is guessed to be of the same team
 **/
bool
Heuristic::condition_next_player_guessed_same_team(Trick const& trick)
{
  if (trick.remainingcardno() == 0) {
    rationale_.add(_("Heuristic::condition::trick is full"));
    return false;
  }

  auto const actcardno = trick.actcardno();
  auto const& player = trick.player_of_card(actcardno + 1);
  if (same_team(player)) {
    rationale_.add(_("Heuristic::condition::next player of the same team"));
    return true;
  } else if (opposite_team(player)) {
    rationale_.add(_("Heuristic::condition::next player of the opposite team"));
    return false;
  } else if (guessed_same_team(player)) {
    rationale_.add(_("Heuristic::condition::next player guessed of the same team"));
    return true;
  } else if (guessed_opposite_team(player)) {
    rationale_.add(_("Heuristic::condition::next player guessed of the opposite team"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::team of the next player not guessed"));
    return false;
  }
}

/** @return   whether the next player is guessed to be of the opposite team
 **/
bool
Heuristic::condition_next_player_guessed_opposite_team(Trick const& trick)
{
  if (trick.remainingcardno() == 0) {
    rationale_.add(_("Heuristic::condition::trick is full"));
    return false;
  } else if (trick.remainingcardno() == 1) {
    rationale_.add(_("Heuristic::condition::only one player behind"));
    return false;
  }

  auto const actcardno = trick.actcardno();
  auto const& player = trick.player_of_card(actcardno + 1);
  if (guessed_opposite_team(player)) {
    rationale_.add(_("Heuristic::condition::next player of the opposite team"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::last player not of the opposite team"));
    return false;
  }
}

/** @return   whether the last player is of the same team
 **/
bool
Heuristic::condition_last_player_same_team(Trick const& trick)
{
  if (same_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player of the same team"));
    return true;
  } else if (opposite_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player of the opposite team"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::team of the last player not known"));
    return false;
  }
}

/** @return   whether the last player is guessed to be of the same team
 **/
bool
Heuristic::condition_last_player_guessed_same_team(Trick const& trick)
{
  if (same_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player of the same team"));
    return true;
  } else if (opposite_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player of the opposite team"));
    return false;
  } else if (guessed_same_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player guessed of the same team"));
    return true;
  } else if (guessed_opposite_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player guessed of the opposite team"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::team of the last player not guessed"));
    return false;
  }
}

/** @return   whether the opposite team has made an announcement
 **/
bool
Heuristic::condition_opposite_team_announcement()
{
  auto const team = this->team();
  if (!is_real(team)) {
    rationale_.add(_("Heuristic::condition::team not set"));
    return false;
  }
  if (!!player().game().announcements().last(opposite(team))) {
    rationale_.add(_("Heuristic::condition::announcement of the opposite team"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::no announcement of the opposite team"));
  return false;
}

/** @return   whether the opposite team has made an announcement
 **/
bool
Heuristic::condition_partner_announcement()
{
  if (!is_real(team())) {
    rationale_.add(_("Heuristic::condition::team not set"));
    return false;
  }
  for (auto const& p : player().game().players()) {
    if (p == player())
      continue;
    if (!same_team(p))
      continue;
    if (!!p.announcement()) {
      rationale_.add(_("Heuristic::condition::announcement of the partner"));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no announcement of the partner"));
  return false;
}

/** @return   whether the opposite team has made an announcement
 **/
bool
Heuristic::condition_own_team_announcement()
{
  auto const team = this->team();
  if (!is_real(team)) {
    rationale_.add(_("Heuristic::condition::team not set"));
    return false;
  }
  if (!player().game().announcements().last(team)) {
    rationale_.add(_("Heuristic::condition::no announcement of the own team"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::announcement of the own team"));
  return true;
}

/** @return   whether the opposite team has made an announcement
 **/
bool
Heuristic::condition_own_team_announcement(Announcement const announcement)
{
  auto const team = this->team();
  if (!is_real(team)) {
    rationale_.add(_("Heuristic::condition::team not set"));
    return false;
  }
  if (!player().game().announcements().last(team)) {
    rationale_.add(_("Heuristic::condition::no announcement of the own team"));
    return false;
  }
  if (player().game().announcements().last(team) < announcement) {
    rationale_.add(_("Heuristic::condition::%s not announced by the own team", _(announcement)));
    return false;
  }
  rationale_.add(_("Heuristic::condition::%s announced by the own team", _(announcement)));
  return true;
}

auto Heuristic::condition_partner_has_swine_or_hyperswine() -> bool
{
  auto const& game = this->game();
  auto const team = this->team();
  auto const& cards_information = this->cards_information();
  if (!is_real(team)) {
    rationale_.add(_("Heuristic::condition::team not set"));
    return false;
  }
  if (   game.swines().hyperswines_announced()
      && cards_information.remaining_others(game.cards().hyperswine())) {
    if (guessed_same_team(game.swines().hyperswines_owner())) {
      rationale_.add(_("Heuristic::condition::my partner still has a hyperswine"));
      return true;
    }
    return false;
  }
  if (   game.swines().swines_announced()
      && cards_information.remaining_others(game.cards().swine())) {
    if (guessed_same_team(game.swines().swines_owner())) {
      rationale_.add(_("Heuristic::condition::my partner still has a swine"));
      return true;
    }
    return false;
  }
  return false;
}

auto Heuristic::condition_partner_has_dulle() -> bool
{
  auto const& game = this->game();
  auto const team = this->team();
  auto const& cards_information = this->cards_information();
  if (!game.rule(Rule::Type::dullen)) {
    return false;
  }
  if (!is_with_trump_color(game.type())) {
    return false;
  }
  if (!is_real(team)) {
    return false;
  }

  if (!cards_information.remaining_others(Card::dulle)) {
    rationale_.add(_("Heuristic::condition::no dulle still out"));
    return false;
  }
  for (auto const& p : game.players()) {
    if (   p != player()
        && guessed_same_team(p)
        && guessed_hand(p).contains(Card::dulle)) {
      rationale_.add(_("Heuristic::condition::player %s probably still has a dulle"));
      return true;
    }
  }

  return false;
}

auto Heuristic::condition_partner_has_club_queen() -> bool
{
  auto const& game = this->game();
  auto const team = this->team();
  auto const& cards_information = this->cards_information();
  if (   game.type() != GameType::normal
      && game.type() != GameType::marriage) {
    rationale_.add(_("Heuristic::condition::no normal game or marriage"));
    return false;
  }
  if (team != Team::re) {
    rationale_.add(_("Heuristic::condition::team not re"));
    return false;
  }
  if (cards_information.remaining_others(Card::club_queen)) {
    rationale_.add(_("Heuristic::condition::club queen still out"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::club queen already played"));
  return false;
}

/** @return   whether the player has color on the hand
 **/
bool
Heuristic::condition_has_color()
{
  if (hand().hascolor()) {
    rationale_.add(_("Heuristic::condition::have color"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::only trump on the hand"));
    return false;
  }
}

/** @return   whether the player has trump on the hand
 **/
bool
Heuristic::condition_has_trump()
{
  if (hand().hastrump()) {
    rationale_.add(_("Heuristic::condition::have trump"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no trump on the hand"));
    return false;
  }
}


auto Heuristic::condition_has_highest_trump() -> bool
{
  if (!hand().hastrump()) {
    rationale_.add(_("Heuristic::condition::no trump on the hand"));
    return false;
  }
  auto const& card = hand().highest_trump();
  if (cards_information().highest_remaining_trump() == card) {
    rationale_.add(_("Heuristic::condition::have the highest trump"));
    return true;
  }

  rationale_.add(_("Heuristic::condition::have not the highest trump"));
  return false;
}


/** @return   whether 'card' is trump
 **/
bool
Heuristic::condition_is_trump(Card card)
{
  if (card.istrump(game())) {
    rationale_.add(_("Heuristic::condition::%s is trump", _(card)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::%s is no trump", _(card)));
    return false;
  }
}

/** @return   whether the player has the color on the hand
 **/
bool
Heuristic::condition_has(Card::Color const color)
{
  if (hand().contains(color)) {
    rationale_.add(_("Heuristic::condition::have %s", _(color)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::have not %s", _(color)));
    return false;
  }
}

/** @return   whether the player has the card on the hand
 **/
bool
Heuristic::condition_has(Card const card)
{
  if (hand().contains(card)) {
    rationale_.add(_("Heuristic::condition::have %s", _(card)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::have not %s", _(card)));
    return false;
  }
}

/** @return   whether the player has color on the hand
 **/
bool
Heuristic::condition_has_ten_points_in_trump()
{
  if (hand().contains_ten_points_in_trump()) {
    rationale_.add(_("Heuristic::condition::have ten points in trump"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::have no ten points in trump"));
    return false;
  }
}

/** @return   whether the player has a dulle
 **/
bool
Heuristic::condition_has_dulle()
{
  if (hand().count_dulle()) {
    rationale_.add(_("Heuristic::condition::have dulle"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::have no dulle"));
    return false;
  }
}

/** @return   whether another player still has trump
 **/
bool
Heuristic::condition_noone_else_has_trump()
{
  if (cards_information().remaining_trumps_others()) {
    rationale_.add(_("Heuristic::condition::the other players still have trump"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::the other players do not have trump"));
    return true;
  }
}

/** @return   whether another player still has 'color'
 **/
bool
Heuristic::condition_noone_else_has(Card::Color const color)
{
  if (cards_information().remaining_others(color)) {
    rationale_.add(_("Heuristic::condition::the other players still have color %s", _(color)));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::the other players do not have color %s", _(color)));
    return true;
  }
}

/** @return   whether another player still has 'color'
 **/
bool
Heuristic::condition_noone_else_has(Card const card)
{
  if (cards_information().remaining_others(card)) {
    rationale_.add(_("Heuristic::condition::the other players still have card %s", _(card)));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::the other players do not have card %s", _(card)));
    return true;
  }
}

/** @return  whether all players behind not of the own team have the color
 **/
bool
Heuristic::condition_opposite_players_behind_have_color(Trick const& trick,
                                                        Card::Color const color)
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    return false;
  }
  for (auto const& player : trick.remaining_following_players()) {
    if (   !guessed_same_team(player)
        && !guessed_hand(player).contains(color)) {
      rationale_.add(_("Heuristic::condition::player %s behind is supposed to not have color %s", player.get().name(), _(color)));
      return false;
    }
  }
  rationale_.add(_("Heuristic::condition::all opposite players behind can have color %s", _(color)));
  return true;
}

/** @return  whether all players behind not of the own team must serve the color
 **/
bool
Heuristic::condition_opposite_players_behind_must_serve(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const tcolor = trick.startcard().tcolor();
  auto const& cards_information = this->cards_information();
  for (auto const& player : trick.remaining_following_players()) {
    if (   !guessed_same_team(player)
        && !cards_information.of_player(player).must_have(tcolor)) {
      rationale_.add(_("Heuristic::condition::player %s behind must possible not serve color %s", player.get().name(), _(tcolor)));
      return false;
    }
  }
  rationale_.add(_("Heuristic::condition::all opposite players behind must serve color %s", _(tcolor)));
  return true;
}

/** @return   whether an opposite player is behind
 **/
bool
Heuristic::condition_opposite_or_unknown_player_behind(Trick const& trick)
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    return false;
  }

  auto const& game = this->game();
  if (   game.type() == GameType::marriage
      && game.marriage().is_undetermined()
      && team() != Team::re) {
    rationale_.add(_("Heuristic::condition::undetermined marriage"));
    return true;
  }

  for (Player const& player : trick.remaining_following_players()) {
    if (opposite_team(player)) {
      rationale_.add(_("Heuristic::condition::player %s behind of opposite team", player.name()));
      return true;
    }
  }
  for (Player const& player : trick.remaining_following_players()) {
    if (guessed_opposite_team(player)) {
      rationale_.add(_("Heuristic::condition::player %s behind guessed of opposite team", player.name()));
      return true;
    }
  }
  for (Player const& player : trick.remaining_following_players()) {
    if (!guessed_same_team(player)) {
      rationale_.add(_("Heuristic::condition::player behind not guessed to be of same team"));
      return true;
    }
  }

  rationale_.add(_("Heuristic::condition::only own team behind"));
  return false;
}

/** @return   whether only opposite players are behind
 **/
bool
Heuristic::condition_only_opposite_or_unknown_player_behind(Trick const& trick)
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    return false;
  }

  auto const& game = this->game();
  if (   game.type() == GameType::marriage
      && game.marriage().is_undetermined()
      && team() != Team::re) {
    rationale_.add(_("Heuristic::condition::undetermined marriage"));
    return true;
  }

  for (Player const& player : trick.remaining_following_players()) {
    if (same_team(player)) {
      rationale_.add(_("Heuristic::condition::player %s behind of same team", player.name()));
      return false;
    }
  }
  for (Player const& player : trick.remaining_following_players()) {
    if (guessed_same_team(player)) {
      rationale_.add(_("Heuristic::condition::player %s behind guessed of same team", player.name()));
      return false;
    }
  }

  rationale_.add(_("Heuristic::condition::no own team behind"));
  return true;
}

/** @return   whether an opposite player who has made an announcement is behind
 **/
bool
Heuristic::condition_opposite_player_with_announcement_behind(Trick const& trick)
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    return false;
  }

  auto const& game = this->game();
  if (   (game.type() == GameType::marriage)
      && game.marriage().is_undetermined()) {
    rationale_.add(_("Heuristic::condition::undetermined marriage"));
    return false;
  }

  for (Player const& player : trick.remaining_following_players()) {
    if (   !guessed_same_team(player)
        && player.announcement() != Announcement::noannouncement) {
      rationale_.add(_("Heuristic::condition::opposite player behind with announcement"));
      return true;
    }
  }

  rationale_.add(_("Heuristic::condition::no opposite player behind with announcement"));
  return false;
}


auto Heuristic::condition_opposite_swine() -> bool
{
  auto const& cards_information = this->cards_information();
  for (auto const& player : game().players()) {
    if (guessed_same_team(player))
      continue;

    auto const& hand_of_player = cards_information.estimated_hand(player);
    if (hand_of_player.count_swines()) {
      rationale_.add(_("Heuristic::condition::%s has a swine", player.name()));
      return true;
    }
  }

  rationale_.add(_("Heuristic::condition::no opposite swine"));
  return false;
}


auto Heuristic::condition_opposite_hyperswine() -> bool
{
  auto const& cards_information = this->cards_information();
  for (auto const& player : game().players()) {
    if (guessed_same_team(player))
      continue;

    auto const& hand_of_player = cards_information.estimated_hand(player);
    if (hand_of_player.count_hyperswines()) {
      rationale_.add(_("Heuristic::condition::%s has a hyperswine", player.name()));
      return true;
    }
  }

  rationale_.add(_("Heuristic::condition::no opposite hyperswine"));
  return false;
}


auto Heuristic::condition_opposite_swine_or_hyperswine() -> bool
{
  return condition_opposite_swine() || condition_opposite_hyperswine();
}


/** @return   whether an opposite player behind has swine or hyperswine
 **/
bool
Heuristic::condition_opposite_swine_or_hyperswine_behind(Trick const& trick)
{
  auto const& cards_information = this->cards_information();
  for (Player const& player : trick.remaining_following_players()) {
    if (guessed_same_team(player))
      continue;
    auto const& hand_of_player = cards_information.estimated_hand(player);
    if (hand_of_player.count_swines()) {
      rationale_.add(_("Heuristic::condition::%s has a swine", player.name()));
      return true;
    } else if (hand_of_player.count_hyperswines()) {
      rationale_.add(_("Heuristic::condition::%s has a hyperswine", player.name()));
      return true;
    }
  }

  rationale_.add(_("Heuristic::condition::no opposite swine or hyperswine behind"));
  return false;
}

/** @return   whether the card can be jabbed by the opposite team
 **/
bool
Heuristic::condition_can_be_jabbed_by_opposite_team(Trick const& trick,
                                                    Card const card)
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    return false;
  }

  if (   HandCard(hand(), card).possible_swine()
      && !game().swines().swines_announced()
      && !(   game().swines().hyperswines_announced()
           && trick.has_played(game().swines().hyperswines_owner())
           && cards_information().remaining_others(game().cards().hyperswine())
          )) {
    rationale_.add(_("Heuristic::condition::no opposite player behind can jab the trick"));
    return false;
  }

  bool only_own_team_behind = true;
  for (Player const& player : trick.remaining_following_players()) {
    if (guessed_same_team(player))
      continue;
    only_own_team_behind = false;
    Trick trick2 = trick;
    trick2.add(HandCard(hand(), card));
    Hand const& hand2 = guessed_hand(player);
    if (hand2.can_jab(trick2)) {
      rationale_.add(_("Heuristic::condition::opposite team behind can jab %s", _(card)));
      return true;
    }
    if (   hand2.higher_card_exists(card)
        && !trick2.startcard().istrump()
        && !cards_information().of_player(player).must_have(trick2.startcard().color())
        && (   value(Aiconfig::Type::cautious)
            || (   cards_information().remaining_others(trick2.startcard().color())
                <= trick.remainingcardno() - (value(Aiconfig::Type::chancy) ? 1 : 0)))
       ) {
      rationale_.add(_("Heuristic::condition::opposite team behind can possible jab %s", _(card)));
      return true;
    }
  }

  if (only_own_team_behind) {
    rationale_.add(_("Heuristic::condition::only own team behind"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::no opposite player behind can jab the trick"));
  return false;
}

/** @return   whether the opposite team has trump
 **/
bool
Heuristic::condition_opposite_team_has_trump()
{
  auto const& players = player().game().players();
  for (auto const& p : players) {
    if (   !guessed_same_team(p)
        && guessed_hand(p).hastrump()) {
      rationale_.add(_("Heuristic::condition::the other team has trumps"));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::the other team has no trumps"));
  return false;
}

/** @return   whether the opposite team can have the card
 **/
bool
Heuristic::condition_opposite_team_can_have(Card const card)
{
  auto const is_opposite_with_card = [this, card](Player const& player) {
    return (   !guessed_same_team(player)
            && guessed_hand(player).contains(card));
  };
  if (any_of(player().game().players(), is_opposite_with_card)) {
    rationale_.add(_("Heuristic::condition::the other team can have %s", _(card)));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the other team does not have %s", _(card)));
  return false;
}

/** @return   whether there is still trump behind
 **/
bool
Heuristic::condition_opposite_trump_behind(Trick const& trick)
{
  auto is_opposite_with_trump_behind = [this, &trick](Player const& player) {
    return (   !trick.has_played(player)
            && !guessed_same_team(player)
            && guessed_hand(player).hastrump());
  };
  if (any_of(player().game().players(), is_opposite_with_trump_behind)) {
    rationale_.add(_("Heuristic::condition::there is still trump of the opposite team behind"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::there is no trump of the opposite team behind"));
  return false;
}

/** @return   whether an opposite player behind has the card 'card'
 **/
bool
Heuristic::condition_opposite_players_behind_have(Trick const& trick, Card const card)
{
  for (auto const& player : trick.remaining_following_players()) {
    if (   !guessed_same_team(player)
        && guessed_hand(player).contains(card)) {
      rationale_.add(_("Heuristic::condition::player %s has card %s", player.get().name(), _(card)));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no following opposite player has card %s", _(card)));
  return false;
}

/** @return   whether the winnerplayer is the soloplayer
 **/
bool
Heuristic::condition_winnerplayer_soloplayer(Trick const& trick)
{
  if (!is_solo(game().type())) {
    rationale_.add(_("Heuristic::condition::no solo game"));
    return false;
  }
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  if (trick.winnerplayer().team() == Team::re) {
    rationale_.add(_("Heuristic::condition::winnerplayer is the soloplayer"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::winnerplayer is not the soloplayer"));
    return false;
  }
}

/** @return   whether the winnerplayer is of the same team as the player
 **/
bool
Heuristic::condition_winnerplayer_same_team(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const& game = this->game();
  auto const& winnerplayer = trick.winnerplayer();
  if (   game.type() == GameType::marriage
      && game.marriage().is_undetermined()) {
    if (   (team() != Team::re)
        || !is_selector(trick.startcard().tcolor(),
                        game.marriage().selector())) {
      rationale_.add(_("Heuristic::condition::undetermined marriage"));
      return false;
    }
  }
  if (same_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer same team"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::winnerplayer not of same team"));
    return false;
  }
}

/** @return   whether the winnerplayer is of the same team as the player
 **/
bool
Heuristic::condition_winnerplayer_surely_same_team(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const& game = this->game();
  auto const& winnerplayer = trick.winnerplayer();
  if (   game.type() == GameType::marriage
      && game.marriage().is_undetermined()) {
    if (   (team() != Team::re)
        || !is_selector(trick.startcard().tcolor(),
                        game.marriage().selector())) {
      rationale_.add(_("Heuristic::condition::undetermined marriage"));
      return false;
    }
  }
  if (same_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer same team"));
    return true;
  } else if (surely_same_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer surely same team"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::winnerplayer not surely of same team"));
    return false;
  }
}

/** @return   whether the winnerplayer is guessed to be of the same team as the player
 **/
bool
Heuristic::condition_winnerplayer_guessed_same_team(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const& game = this->game();
  auto const& winnerplayer = trick.winnerplayer();
  if (   (game.type() == GameType::marriage)
      && game.marriage().is_undetermined()) {
    if (   (team() != Team::re)
        || !is_selector(trick.startcard().tcolor(),
                        game.marriage().selector())) {
      rationale_.add(_("Heuristic::condition::undetermined marriage"));
      return false;
    }
  }
  if (same_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer same team"));
    return true;
  } else if (guessed_same_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer guessed same team"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::winnerplayer not of same team"));
    return false;
  }
}

/** @return   whether the winnerplayer is guessed to be of the opposite team as the player
 **/
bool
Heuristic::condition_winnerplayer_guessed_opposite_team(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const& game = this->game();
  auto const& winnerplayer = trick.winnerplayer();
  if (   (game.type() == GameType::marriage)
      && game.marriage().is_undetermined()) {
    if (   (team() != Team::re)
        || !is_selector(trick.startcard().tcolor(),
                        game.marriage().selector())) {
      rationale_.add(_("Heuristic::condition::undetermined marriage"));
      return false;
    }
  }
  if (opposite_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer opposite team"));
    return true;
  } else if (guessed_opposite_team(winnerplayer)) {
    rationale_.add(_("Heuristic::condition::winnerplayer guessed opposite team"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::winnerplayer not of opposite team"));
    return false;
  }
}

/** @return   whether the winnercard is trump
 **/
bool
Heuristic::condition_winnercard_trump(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  } else if (trick.winnercard().istrump()) {
    rationale_.add(_("Heuristic::condition::winnercard is trump"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::winnercard is no trump"));
    return false;
  }
}

/** @return   whether the own team has jabbed with at least trump card limit
 **/
bool
Heuristic::condition_own_team_jabbed_with_trump_card_limit(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  } else if (!guessed_same_team(trick.winnerplayer())) {
    rationale_.add(_("Heuristic::condition::winnerplayer not of own team"));
    return false;
  } else if (!trick.winnercard().istrump()) {
    rationale_.add(_("Heuristic::condition::winnercard is color card"));
    return false;
  } else if (trick.isjabbed({hand(), trump_card_limit()})) {
    rationale_.add(_("Heuristic::condition::winnercard below trump card limit"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::winnercard at least trump card limit"));
    return true;
  }
}

/** @return   whether the player must serve
 **/
bool
Heuristic::condition_must_serve(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto tcolor = trick.startcard().tcolor();
  if (hand().contains(tcolor)) {
    rationale_.add(_("Heuristic::condition::must serve %s", _(tcolor)));
    return true;
  }
  rationale_.add(_("Heuristic::condition::must not serve %s", _(tcolor)));
  return false;
}

/** @return   whether 'card' is valid in this trick
 **/
bool
Heuristic::condition_is_valid(Trick const& trick, Card card)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return true;
  }

  if (!hand().contains(card)) {
    rationale_.add(_("Heuristic::condition::the hand does not contain card %s", _(card)));
    return false;
  }
  if (trick.isvalid(card, hand())) {
    rationale_.add(_("Heuristic::condition::can play %s", _(card)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::must serve %s", _(trick.startcard().tcolor())));
    return false;
  }
}


/** @return   whether the player can jab
 **/
bool
Heuristic::condition_can_jab(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }

  if (hand().can_jab(trick)) {
    rationale_.add(_("Heuristic::condition::can jab"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::cannot jab"));
    return false;
  }
}

/** @return   whether the player can jab with trump
 **/
bool
Heuristic::condition_can_jab_with_trump(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }

  auto const color = trick.startcard().tcolor();
  if (   (color != Card::TColor::trump)
      && hand().contains(color)) {
    rationale_.add(_("Heuristic::condition::still have color %s", _(color)));
    return false;
  } else if (hand().can_jab(trick)) {
    rationale_.add(_("Heuristic::condition::can jab with trump"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::have no trump high enough to jab"));
    return false;
  }
}

/** @return   whether the player can jab with trump
 **/
bool
Heuristic::condition_can_jab_with(Trick const& trick, Card const card)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }

  if (!hand().contains(card)) {
    rationale_.add(_("Heuristic::condition::have not %s", _(card)));
    return false;
  } else if (!trick.isvalid(card, hand())) {
    rationale_.add(_("Heuristic::condition::%s not valid", _(card)));
    return false;
  } else if (trick.isjabbed(HandCard(hand(), card))) {
    rationale_.add(_("Heuristic::condition::trick is jabbed by %s", _(card)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::trick is not jabbed by %s", _(card)));
    return false;
  }
}

/** @return   whether there are enough points for a doppelkopf in the trick so far
 **/
bool
Heuristic::condition_enough_trick_points_for_doppelkopf(Trick const& trick)
{
  if (trick.points() >= 10 * trick.actcardno()) {
    rationale_.add(_("Heuristic::condition::enough trick points so far (%u points for %u players)", trick.points(), trick.actcardno()));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::not enough points"));
    return false;
  }
}

/** @return   whether the winnercard is not less then limithigh
 **/
bool
Heuristic::condition_high_winnercard(Trick const& trick)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const winnercard = trick.winnercard();
  if (!winnercard.istrump()) {
    rationale_.add(_("Heuristic::condition::the winnercard is no trump"));
    return false;
  }
  auto const limithigh = HandCard(hand(), ai().value(Aiconfig::Type::limithigh));
  if (trick.isjabbed(limithigh)) {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is less then limithigh (%s)", _(winnercard), _(limithigh)));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is not less then limithigh (%s)", _(winnercard), _(limithigh)));
    return true;
  }
}

/** @return   whether the winnercard is not less then limithigh
 **/
bool
Heuristic::condition_winnercard_ge(Trick const& trick, Card const card_)
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const card = HandCard(hand(), card_);
  auto const& winnercard = trick.winnercard();
  if (trick.isjabbed(card)) {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is not greater then %s", _(winnercard), _(card)));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is greater then %s", _(winnercard), _(card)));
    return true;
  }
}

/** @return   whether the partner has jabbed with a high trump
 **/
bool
Heuristic::condition_partner_jabbed_with_high_trump(Trick const& trick)
{
  auto const& ai = this->ai();
  if (!same_team(trick.winnerplayer())) {
    rationale_.add(_("Heuristic::condition::the winnerplayer is not of the own team"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::the winnerplayer is of the own team"));
  if (trick.isjabbed({hand(), ai.value(Aiconfig::Type::limithigh)})) {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is less then limithigh (%s)",
                     _(trick.winnercard()), _(ai.value(Aiconfig::Type::limithigh))));
    return false;
  }
  rationale_.add(_("Heuristic::condition::the winnercard (%s) is not less then limithigh (%s)",
                   _(trick.winnercard()), _(ai.value(Aiconfig::Type::limithigh))));
  return true;
}

/** @return   whether the trick has x points so far
 **/
bool
Heuristic::condition_trick_points_ge(Trick const& trick, Trick::Points const points)
{
  if (trick.points() >= points) {
    rationale_.add(_("Heuristic::condition::the trick has at least %u points", points));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::the trick has less then %u points", points));
    return false;
  }
}

/** @return   whether a guessed partner can jab the trick
 **/
bool
Heuristic::condition_own_team_can_jab(Trick const& trick)
{
  for (auto const& p : trick.remaining_players()) {
    auto const& player = p.get();
    if (player == trick.actplayer())
      continue;
    if (!same_team(player))
      continue;
    if (guessed_hand(player).can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::%s can jab the trick", player.name()));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no partner found who can jab the trick"));
  return false;
}

/** @return   whether a guessed partner can jab the trick
 **/
bool
Heuristic::condition_guessed_own_team_can_jab(Trick const& trick)
{
  for (auto const& p : trick.remaining_players()) {
    auto const& player = p.get();
    if (player == trick.actplayer())
      continue;
    if (!guessed_same_team(player))
      continue;
    if (guessed_hand(player).can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::%s can jab the trick", player.name()));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no guessed partner found who can jab the trick"));
  return false;
}

/** @return   whether an opposite team partner / soloplayer / player of unknown team can jab the trick
 **/
bool
Heuristic::condition_opposite_or_unknown_team_can_jab(Trick const& trick)
{
  if (trick.isempty())
    return true;

  if (trick.startcard().istrump()) {
    for (auto const& p : trick.remaining_players()) {
      auto const& player = p.get();
      if (player == trick.actplayer())
        continue;
      if (surely_same_team(player))
        continue;
      if (guessed_hand(player).can_jab(trick)) {
        rationale_.add(_("Heuristic::condition::%s can jab the trick", player.name()));
        return true;
      }
    }
    rationale_.add(_("Heuristic::condition::no opponent found who can jab the trick"));
    return false;
  }
  auto const color = trick.startcard().color();
  for (auto const& p : trick.remaining_players()) {
    auto const& player = p.get();
    if (player == trick.actplayer())
      continue;
    if (surely_same_team(player))
      continue;
    if (guessed_hand(player).can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::%s can jab the trick", player.name()));
      return true;
    }
    if (   !cards_information().of_player(player).must_have(color)
        && guessed_hand(player).higher_card_exists(trick.winnercard())) {
      if (   cards_information().remaining_others(color) + 1 < trick.remainingcardno()
          && !cards_information().of_player(player).must_have(color)) {
        rationale_.add(_("Heuristic::condition::the player %s possible has no color %s", player.name(), _(color)));
        return true;
      }
      if (!game().is_solo()) {
        if (cards_information().color_runs(color) > 1
            || (   cards_information().color_runs(color) == 1
                && (   this->player().hand().contains(color)
                    || !trick.winnercard().istrump()))) {
          rationale_.add(_("Heuristic::condition::not the first color run"));
          return true;
        }
        if (cards_information().of_player(player).played(color) > cards_information().color_runs(color)) {
          rationale_.add(_("Heuristic::condition::the player %s has already thrown the color %s", player.name(), _(color)));
          return true;
        }
      }
    }
  }
  rationale_.add(_("Heuristic::condition::no opponent found who can jab the trick"));
  return false;
}

auto Heuristic::condition_opposite_or_unknown_team_sure_can_jab(Trick const& trick) -> bool
{
  if (trick.isempty())
    return true;
  auto const is_contra = (   game().type() == GameType::normal && player().team() == Team::contra);
  auto const tcolor = trick.isempty() ? Card::trump : trick.startcard().tcolor();
  for (auto const& p : trick.remaining_players()) {
    auto const& player = p.get();
    if (player == trick.actplayer())
      continue;
    if (surely_same_team(player))
      continue;
    auto const must_have_cards = cards_information().of_player(player).must_have_cards();
    if (   (   tcolor == Card::trump
            || !guessed_hand(player).contains(tcolor))
        && must_have_cards.can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::%s can jab the trick for sure with high trump", player.name()));
      return true;
    }
    if (is_contra && guessed_hand(player).contains(Card::club_queen)) {
      rationale_.add(_("Heuristic::condition::%s can jab the trick with card %s", player.name(), _(Card::club_queen)));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no opponent found who surely can jab the trick"));
  return false;
}

/** @return   whether there are enough remaining color cards for the reamining players
 **/
bool
Heuristic::condition_enough_remaining_color_cards(Trick const& trick, Card::Color const color)
{
  // the number of remaining cards for the other players
  auto const& game = trick.game();
  auto const& cards = game.cards();
  auto const& cards_information = this->cards_information();
  auto const& hand = this->hand();
  unsigned const remaining_cards
    = (cards.count(color)
       - (cards_information.played(color) + hand.count(color)));

  // there are enough cards for all players
  if (   remaining_cards < trick.remainingcardno() - 1
      && (trick.isempty() || trick.startcard().tcolor() == color)
      && !condition_opposite_players_behind_must_serve(trick)) {
    rationale_.add(_("Heuristic::condition::not enough remaining cards of %s for all other players", _(color)));
    return false;
  }
  rationale_.add(_("Heuristic::condition::enough remaining cards of %s for all other players", _(color)));
  return true;
}

/** @return   whether the partner has already jabbed high
 **/
bool
Heuristic::condition_backhand_can_pfund(Trick const& trick)
{
  auto const& backhand = trick.lastplayer();
  auto const& hand = guessed_hand(backhand);
  auto const& cards_information = this->cards_information().of_player(trick.lastplayer());
  if (hand.count(Card::ace) + hand.count(Card::ten) == 0) {
    rationale_.add(_("Heuristic::condition::player %s has neither ace nor ten", backhand.name()));
    return false;
  }

  if (!trick.startcard().istrump()) {
    if (cards_information.must_have(trick.startcard().tcolor())) {
      auto const color = trick.startcard().tcolor();
      auto const ace = Card(color, Card::ace);
      if (hand.contains(ace)) {
        rationale_.add(_("Heuristic::condition::player %s possible has card %s to pfund", backhand.name(), _(ace)));
        return true;
      }
      auto const ten = HandCard(hand, Card(color, Card::ten));
      if (   hand.contains(ten)
          && !ten.is_special()) {
        rationale_.add(_("Heuristic::condition::player %s possible has card %s to pfund", backhand.name(), _(ten)));
        return true;

      }
      return false;
    }
  } else {
    auto const color = game().cards().trumpcolor();
    auto const ace = HandCard(hand, color, Card::ace);
    if (hand.contains(ace)) {
      rationale_.add(_("Heuristic::condition::player %s possible has card %s to pfund", backhand.name(), _(ace)));
      return true;
    }
    auto const ten = HandCard(hand, color, Card::ten);
    if (   hand.contains(ten)
        && !ten.is_special()) {
      rationale_.add(_("Heuristic::condition::player %s possible has card %s to pfund", backhand.name(), _(ten)));
      return true;

    }
    return false;
  }

  return true;
}

