/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "decision_in_game.h"

#include "../../card/trick.h"

/** Base class for heuristics
 **/
class Heuristic : public DecisionInGame {
public:
  static auto create(Aiconfig::Heuristic heuristic,
                     Ai const& ai) -> unique_ptr<Heuristic>;
  static auto is_valid(Aiconfig::Heuristic heuristic, Player const& player) -> bool;
  static auto is_valid(Aiconfig::Heuristic heuristic,
                       GameTypeGroup game_type, PlayerTypeGroup player_type) -> bool;

  Heuristic(Ai const& ai, Aiconfig::Heuristic heuristic);
  Heuristic(Heuristic const&) = delete;
  auto operator=(Heuristic const&) = delete;

  ~Heuristic() override;

  auto heuristic()                 const -> Aiconfig::Heuristic;
  auto name()                      const -> string const&;
  auto get_card(Trick const& trick)      -> Card;

  virtual auto conditions_met(Trick const& trick) -> bool = 0;
  virtual auto card_to_play(Trick const& trick)   -> Card = 0;

protected: // Hilfsfunktionen
  auto soloplayer_behind(Trick const& trick)                        const -> bool;
  auto can_be_jabbed_by_others(Card::Color color)                   const -> bool;
  auto following_opposite_player_has(Trick const& trick, Card card) const -> bool;

  auto highest_trump_of_opposite_team()                         const -> HandCard;
  auto highest_card_behind_of_opposite_team(Trick const& trick) const -> HandCard;
  auto valid_cards_behind_of_opposite_team(Trick const& trick)  const -> vector<HandCard>;

  auto remaining_tricks_go_to_own_team() const -> bool;

  auto condition_soloplayer_behind(Trick const& trick)     -> bool;
  auto condition_soloplayer_has_played(Trick const& trick) -> bool;

  auto condition_is_marriage_determination_trick(Trick const& trick) -> bool;
  auto condition_marriage_decision_for_me(Trick const& trick)        -> bool;

  auto condition(Team team) -> bool;

  auto condition_first_trick()                               -> bool;
  auto condition_startcard(Trick const& trick)               -> bool;
  auto condition_second_card(Trick const& trick)             -> bool;
  auto condition_second_last_card(Trick const& trick)        -> bool;
  auto condition_last_card(Trick const& trick)               -> bool;
  auto condition_first_color_run(Trick const& trick)         -> bool;
  auto condition_trump_trick(Trick const& trick)             -> bool;
  auto condition_color_trick(Trick const& trick)             -> bool;
  auto condition_pure_color_trick(Trick const& trick)        -> bool;
  auto condition_startcard_is(Trick const& trick, Card card) -> bool;

  auto condition_partner_guessed()                                     -> bool;
  auto condition_partner_behind(Trick const& trick)                    -> bool;
  auto condition_guessed_partner_behind(Trick const& trick)            -> bool;
  auto condition_next_player_same_team(Trick const& trick)             -> bool;
  auto condition_next_player_guessed_same_team(Trick const& trick)     -> bool;
  auto condition_next_player_guessed_opposite_team(Trick const& trick) -> bool;
  auto condition_last_player_same_team(Trick const& trick)             -> bool;
  auto condition_last_player_guessed_same_team(Trick const& trick)     -> bool;

  auto condition_opposite_team_announcement()                     -> bool;
  auto condition_partner_announcement()                           -> bool;
  auto condition_own_team_announcement()                          -> bool;
  auto condition_own_team_announcement(Announcement announcement) -> bool;
  auto condition_partner_has_swine_or_hyperswine()                -> bool;
  auto condition_partner_has_dulle()                              -> bool;
  auto condition_partner_has_club_queen()                         -> bool;

  auto condition_has_color()                       -> bool;
  auto condition_has_trump()                       -> bool;
  auto condition_has_highest_trump()               -> bool;
  auto condition_is_trump(Card card)               -> bool;
  auto condition_has(Card::Color color)            -> bool;
  auto condition_has(Card card)                    -> bool;
  auto condition_has_ten_points_in_trump()         -> bool;
  auto condition_has_dulle()                       -> bool;
  auto condition_noone_else_has_trump()            -> bool;
  auto condition_noone_else_has(Card::Color color) -> bool;
  auto condition_noone_else_has(Card card)         -> bool;

  auto condition_opposite_players_behind_have_color(Trick const& trick, Card::Color color) -> bool;
  auto condition_opposite_players_behind_must_serve(Trick const& trick)                    -> bool;

  auto condition_opposite_or_unknown_player_behind(Trick const& trick)         -> bool;
  auto condition_only_opposite_or_unknown_player_behind(Trick const& trick)    -> bool;
  auto condition_opposite_player_with_announcement_behind(Trick const& trick)  -> bool;
  auto condition_opposite_swine()                                              -> bool;
  auto condition_opposite_hyperswine()                                         -> bool;
  auto condition_opposite_swine_or_hyperswine()                                -> bool;
  auto condition_opposite_swine_or_hyperswine_behind(Trick const& trick)       -> bool;
  auto condition_can_be_jabbed_by_opposite_team(Trick const& trick, Card card) -> bool;
  auto condition_opposite_team_has_trump()                                     -> bool;
  auto condition_opposite_team_can_have(Card card)                             -> bool;
  auto condition_opposite_trump_behind(Trick const& trick)                     -> bool;
  auto condition_opposite_players_behind_have(Trick const& trick, Card card)   -> bool;

  auto condition_winnerplayer_soloplayer(Trick const& trick)               -> bool;
  auto condition_winnerplayer_same_team(Trick const& trick)                -> bool;
  auto condition_winnerplayer_surely_same_team(Trick const& trick)         -> bool;
  auto condition_winnerplayer_guessed_same_team(Trick const& trick)        -> bool;
  auto condition_winnerplayer_guessed_opposite_team(Trick const& trick)    -> bool;
  auto condition_winnercard_trump(Trick const& trick)                      -> bool;
  auto condition_own_team_jabbed_with_trump_card_limit(Trick const& trick) -> bool;
  auto condition_must_serve(Trick const& trick)                            -> bool;
  auto condition_is_valid(Trick const& trick, Card card)                   -> bool;
  auto condition_can_jab(Trick const& trick)                               -> bool;
  auto condition_can_jab_with_trump(Trick const& trick)                    -> bool;
  auto condition_can_jab_with(Trick const& trick, Card card)               -> bool;
  auto condition_enough_trick_points_for_doppelkopf(Trick const& trick)    -> bool;
  auto condition_high_winnercard(Trick const& trick)                       -> bool;
  auto condition_winnercard_ge(Trick const& trick, Card card)              -> bool;
  auto condition_partner_jabbed_with_high_trump(Trick const& trick)        -> bool;

  auto condition_trick_points_ge(Trick const& trick, Trick::Points points) -> bool;

  auto condition_own_team_can_jab(Trick const& trick)                                -> bool;
  auto condition_guessed_own_team_can_jab(Trick const& trick)                        -> bool;
  auto condition_opposite_or_unknown_team_can_jab(Trick const& trick)                -> bool;
  auto condition_opposite_or_unknown_team_sure_can_jab(Trick const& trick)           -> bool;
  auto condition_enough_remaining_color_cards(Trick const& trick, Card::Color color) -> bool;

  auto condition_backhand_can_pfund(Trick const& trick) -> bool;

  auto lowest_best_trump_card(Trick const& trick) const -> HandCard;

  auto colors_starting_with(Card::Color color)            -> vector<Card::Color>;
  auto sure_jabbing_card(Trick const& trick)              -> HandCard;
  auto result_high_trump_jabbing_card(Trick const& trick) -> HandCard;
  auto lowest_highest_trump(Trick const& trick)           -> HandCard;

  auto optimized_jabbing_card(Trick const& trick, HandCard card)                            -> HandCard;
  auto optimized_trump_to_jab(Card card, Trick const& trick, bool only_same_points = false) -> HandCard;
  auto best_jabbing_card(Trick const& trick, bool jab_always = false)                       -> HandCard;
  auto jabbing_card_for_last_player(Trick const& trick)                                     -> HandCard;
private:
  auto jabbing_card_for_last_player(Trick const& trick, Card::Color color) -> HandCard;
  auto jabbing_card_for_last_player_trump(Trick const& trick)              -> HandCard;

protected:
  Aiconfig::Heuristic heuristic_ = Aiconfig::Heuristic::no_heuristic;
  string const name_;
};


auto best_of_if(vector<Card::Color> container,
                std::function<bool(Card::Color)> predicate,
                std::function<int(Card::Color)> value_function) -> Card::Color;
