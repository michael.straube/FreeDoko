/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "bug_report.h"

#include "../ai.h"
#include "../../../os/bug_report_replay.h"
#include "../../../game/gameplay_actions.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool BugReport::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
BugReport::BugReport(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::bug_report)
{ }

/** destructor
 **/
BugReport::~BugReport() = default;

/** @return  whether the conditions are met
 **/
bool
BugReport::conditions_met(Trick const& trick)
{
  return (   !condition_virtual_game(trick)
          && condition_auto_action());
}

/** @return   card to play
 **/
Card
BugReport::card_to_play(Trick const& trick)
{
#ifdef BUG_REPORT_REPLAY
  auto& game = const_cast<Game&>(this->game());
  auto& player = const_cast<Player&>(this->player());

  if (   (::bug_report_replay->current_action().type()
          == GameplayActions::Type::swines)
      && (dynamic_cast<GameplayActions::Swines const&>(::bug_report_replay->current_action()).player == player.no()) ) {
    game.swines().swines_announce(player);
  }

  if (   (::bug_report_replay->current_action().type()
          == GameplayActions::Type::hyperswines)
      && (dynamic_cast<GameplayActions::Hyperswines const&>(::bug_report_replay->current_action()).player == player.no()) ) {
    game.swines().hyperswines_announce(player);
  }

  if (::bug_report_replay->current_action().type()
      != GameplayActions::Type::card_played) {
    rationale_.add(_("Heuristic::reject::next action is not card played"));
    return {};
  }
  if (dynamic_cast<GameplayActions::CardPlayed const&>(::bug_report_replay->current_action()).player != player.no()) {
    rationale_.add(_("Heuristic::reject::the next player is not this one"));
    return {};
  }

  auto const card = dynamic_cast<GameplayActions::CardPlayed const&>(::bug_report_replay->current_action()).card;
  auto const& hand = this->hand();
  if (!hand.contains(card)) {
    rationale_.add(_("Heuristic::reject::card %s is not in the hand", _(card)));
    return {};
  }
  if (!trick.isvalid(card, hand)) {
    rationale_.add(_("Heuristic::reject::card %s is not valid", _(card)));
    return {};
  }

  rationale_.add(_("Heuristic::return::card from bug report: %s", _(card)));
  return card;
#else
  return {};
#endif
}

/** @return   whether this is a virtual game
 **/
bool
BugReport::condition_virtual_game(Trick const& trick)
{
  if (trick.game().isvirtual()) {
    rationale_.add(_("Heuristic::condition::virtual game"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no virtual game"));
    return false;
  }
}

/** @return   whether this is a virtual game
 **/
bool
BugReport::condition_auto_action()
{
#ifdef BUG_REPORT_REPLAY
  if (!::bug_report_replay) {
    rationale_.add(_("Heuristic::condition::no bug report replay"));
    return false;
  }
  if (::bug_report_replay->auto_action()) {
    rationale_.add(_("Heuristic::condition::auto action in bug report replay"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no auto action in bug report replay"));
    return false;
  }
#else
  return false;
#endif
}

} // namespace Heuristics
