/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "create_blank_color.h"

#include "../ai.h"

#include "../../../card/trick.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool CreateBlankColor::is_valid(GameTypeGroup const game_type,
                          PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
CreateBlankColor::CreateBlankColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::create_blank_color)
{ }

/** destructor
 **/
CreateBlankColor::~CreateBlankColor() = default;

/** @return  whether the conditions are met
 **/
bool
CreateBlankColor::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_color_trick(trick)
          && !condition_first_color_run(trick)
          && !condition_must_serve(trick)
          && (   condition_few_trick_points(trick)
              || condition_trick_jabbed_high(trick))
         );
}

/** @return   card to play
 **/
Card
CreateBlankColor::card_to_play(Trick const& trick)
{
  auto const card = best_fehl_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to create blank color"));
    return {};
  }

  rationale_.add(_("Heuristic::return::best card to create blank color: %s", _(card)));
  return card;
}

// @return   whether in the trick there are only few points
bool
CreateBlankColor::condition_few_trick_points(Trick const& trick)
{
  if (static_cast<int>(trick.points())
      > value(Aiconfig::Type::limit_throw_fehl)) {
    rationale_.add(_("Heuristic::condition::too many trick points"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::few trick points"));
  auto const color = trick.startcard().color();
  if (   !trick.islastcard()
      && (       cards_information().remaining_others(color, Card::ace)
          || (  !HandCard(hand(), color, Card::ten).is_special()
              && cards_information().remaining_others(color, Card::ten))
         )) {
    rationale_.add(_("Heuristic::condition::still remaining ten or ace in color %s", _(color)));
    return false;
  }
  return true;
}

/** @return   whether the trick is jabbed with a high (trump) card
 **/
bool
CreateBlankColor::condition_trick_jabbed_high(Trick const& trick)
{
  if (!trick.winnercard().istrump()) {
    rationale_.add(_("Heuristic::condition::not jabbed with trump"));
    return false;
  }

  if (trick.winnercard().is_jabbed_by(ai().lowest_trump_card_limit())) {
    rationale_.add(_("Heuristic::condition::winnercard is a low trump"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::winnercard is a high trump"));
  return true;
}

/** @return   a good color card to throw in order to create a blank color
 **/
Card
CreateBlankColor::best_fehl_card(Trick const& trick)
{
  auto const& cards_information = this->cards_information();
  auto const& hand = this->hand();

  auto p = max_if(hand, [&hand](auto const card) {
                  return (   !card.istrump()
                          && hand.count(card.color()) <= 1
                          && (   card.value() == Card::king
                              || card.value() == Card::nine )); },
                  [&cards_information](Card const lhs, Card const rhs) {
                  return (cards_information.remaining_others(lhs.color())
                          < cards_information.remaining_others(rhs.color()));
                  });
  if (p)
    return *p;
  return {};
}

} // namespace Heuristics
