/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "draw_remaining_trump.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

auto DrawRemainingTrump::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump(game_type);
}


DrawRemainingTrump::DrawRemainingTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::draw_remaining_trump)
{ }


DrawRemainingTrump::~DrawRemainingTrump() = default;

auto DrawRemainingTrump::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_opposite_team_has_trump()
          && condition_can_possible_draw_remaining_trumps()
         );
}


auto DrawRemainingTrump::card_to_play(Trick const& trick) -> Card
{
  auto c = hand().highest_trump();
  while (   c.points() >= 10
         && condition_can_be_jabbed_by_opposite_team(trick, c))
    c = hand().next_lower_card(c);
  rationale_.add(_("Heuristic::return::highest trump: %s", _(c)));
  return c;
}


auto DrawRemainingTrump::condition_can_possible_draw_remaining_trumps() -> bool
{
  auto const& hand = this->hand();
  auto const& cards_information = ai().cards_information();
  auto const other_trumpno = cards_information.remaining_trumps_others();

  auto const other_highest_trump = highest_trump_of_opposite_team();
  auto high_trumpno = hand.count_ge(other_highest_trump);

  if (game().is_solo() && player().team() == Team::contra) {
    if (!guessed_hand_of_soloplayer().hastrump()) {
      rationale_.add(_("Heuristic::condition::the soloplayer has no trump any more"));
      return false;
    }
    if (high_trumpno >= other_trumpno) {
      rationale_.add(_("Heuristic::condition::have enough high trumps to possible draw all remaining ones"));
      return true;
    }
  } else if (game().is_solo() && player().team() == Team::re && high_trumpno >= other_trumpno / 2.0 + (ai().difficulty() == AiconfigDifficulty::chancy ? 0 : 1)) {
    rationale_.add(_("Heuristic::condition::have enough high trumps to possible draw all remaining ones"));
    return true;
  } else if (high_trumpno >= other_trumpno / 2.0) {
    rationale_.add(_("Heuristic::condition::have enough high trumps to possible draw all remaining ones"));
    return true;
  }

  rationale_.add(_("Heuristic::condition::have not enough high trumps to possible draw all remaining ones"));
  return false;
}

} // namespace Heuristics
