/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "play_color_for_last_player_to_jab.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayColorForLastPlayerToJab::is_valid(GameTypeGroup const game_type,
                                           PlayerTypeGroup const player_group)
{
  return (   is_with_trump_color(game_type)
          && is_with_partner(game_type, player_group)
          && !(   game_type == GameTypeGroup::poverty
               && player_group == PlayerTypeGroup::re));
}

/** constructor
 **/
PlayColorForLastPlayerToJab::PlayColorForLastPlayerToJab(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_color_for_last_player_to_jab)
{ }

/** destructor
 **/
PlayColorForLastPlayerToJab::~PlayColorForLastPlayerToJab() = default;

/** @return  whether the conditions are met
 **/
bool
PlayColorForLastPlayerToJab::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_last_player_guessed_same_team(trick)
         );
}

/** @return   card to play
 **/
Card
PlayColorForLastPlayerToJab::card_to_play(Trick const& trick)
{
  auto const& partner = trick.lastplayer();
  auto const& hand_of_partner = cards_information().estimated_hand(partner);
  if (!hand_of_partner.hastrump()) {
    rationale_.add(_("Heuristic::reject::the partner has no trump"));
    return {};
  }
  auto card = best_color_card_for_partner_to_jab(partner);
  if (!card) {
    rationale_.add(_("Heuristic::reject::the partner has still all colors I have"));
    return {};
  }
  rationale_.add(_("Heuristic::return::highest card from the best color: %s", _(card)));
  return card;
}

/** @return   best color card for the partner to jab
 **/
Card
PlayColorForLastPlayerToJab::best_color_card_for_partner_to_jab(Player const& partner)
{
  auto const& hand_of_partner = cards_information().estimated_hand(partner);
  auto best_color = Card::nocardcolor;
  int longest_color_count = -1;
  for (auto const color : game().cards().colors()) {
    if (hand_of_partner.contains(color))
      continue;
    int const color_count = cards_information().remaining_others(color);
    if (   color_count > longest_color_count
        || (   color_count == longest_color_count
            && (  hand().highest_card(color).value()
                > hand().highest_card(best_color).value()))) {
      best_color = color;
      longest_color_count = color_count;
    }
  }

  if (!best_color)
    return {};

  return hand().highest_card(best_color);
}

} // namespace Heuristics
