/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "play_color_for_partner_to_jab.h"

#include "../../../../game/game.h"

namespace Heuristics {

auto PlayColorForPartnerToJab::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_trump_color(game_type)
          && is_with_partner(game_type, player_group)
          && !(   game_type == GameTypeGroup::poverty
               && player_group == PlayerTypeGroup::re));
}


PlayColorForPartnerToJab::PlayColorForPartnerToJab(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_color_for_partner_to_jab)
{ }


PlayColorForPartnerToJab::~PlayColorForPartnerToJab() = default;


auto PlayColorForPartnerToJab::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_partner_guessed()
         );
}


auto PlayColorForPartnerToJab::card_to_play(Trick const& trick) -> Card
{
  auto const color = best_color_to_play(trick);

  if (!color) {
    rationale_.add(_("Heuristic::reject::no suitable color found"));
    return {};
  }

  auto const& hand = this->hand();
  if (cards_information().remaining_others(color) > 2) {
    auto const card = hand.highest_card(color);
    rationale_.add(_("Heuristic::return::highest card from color %s: %s", _(color), _(card)));
    return card;
  } else {
    auto const card = hand.lowest_card(color);
    rationale_.add(_("Heuristic::return::lowest card of color %s: %s", _(color), _(card)));
    return card;
  }
}


auto PlayColorForPartnerToJab::best_color_to_play(Trick const& trick) -> Card::Color
{
  auto const& cards_information = this->cards_information();
  return best_of_if(game().cards().colors(),
                    [this](Card::Color const color) {
                    return condition_suitable_color(color);
                    },
                    [&cards_information](Card::Color const color) {
                    return cards_information.remaining_others(color);
                    }
                   );
}


auto PlayColorForPartnerToJab::condition_suitable_color(Card::Color const color) -> bool
{
  if (!condition_has(color))
    return false;
  auto const highest_trump_p = highest_trump_of_partners(color);
  if (!highest_trump_p) {
    rationale_.add(_("Heuristic::Decision::the partner has the color %s", _(color)));
    return false;
  }
  rationale_.add(_("Heuristic::Decision::the partner does not have the color %s", _(color)));
  if (   cards_information().remaining_others(color) > 2
      && !game().cards().less(highest_trump_p, trump_card_limit())) {
    rationale_.add(_("Heuristic::Decision::the partner has a high trump"));
    return true;
  }
  auto const highest_trump_o = highest_trump_of_opponents(color);
  if (    highest_trump_p != highest_trump_o
      && !game().cards().less(highest_trump_p, highest_trump_o)) {
    rationale_.add(_("Heuristic::Decision::the partner has the highest trump"));
    return true;
  }
  rationale_.add(_("Heuristic::Decision::the partner has no trump high enough"));
  return false;
}


auto PlayColorForPartnerToJab::highest_trump_of_partners(Card::Color const color) -> Card
{
  Card highest_trump= Card::empty;
  for (auto const& player : game().players()) {
    if (!guessed_same_team(player))
      continue;
    auto const hand = guessed_hand(player);
    if (hand.contains(color))
      continue;
    highest_trump = hand.highest_trump();
  }
  return highest_trump;
}


auto PlayColorForPartnerToJab::highest_trump_of_opponents(Card::Color const color) -> Card
{
  Card highest_trump= Card::empty;
  for (auto const& player : game().players()) {
    if (guessed_same_team(player))
      continue;
    auto const hand = guessed_hand(player);
    if (hand.contains(color))
      continue;
    highest_trump = hand.highest_trump();
  }
  return highest_trump;
}

} // namespace Heuristics
