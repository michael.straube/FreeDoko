/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "play_to_jab_later.h"

#include "../../../../card/hand.h"
#include "../../../../card/trick.h"
#include "../../../../game/game.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayToJabLater::is_valid(GameTypeGroup const game_type,
                              PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

/** constructor
 **/
PlayToJabLater::PlayToJabLater(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_to_jab_later)
{ }

/** destructor
 **/
PlayToJabLater::~PlayToJabLater() = default;

/** @return  whether the conditions are met
 **/
bool
PlayToJabLater::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && !condition_has_color());
}

/** @return   card to play
 **/
Card
PlayToJabLater::card_to_play(Trick const& trick)
{
  auto const& game = trick.game();
  auto const& hand = this->hand();

  static auto const cards_to_play_with_nines
    = vector<Card>({Card::diamond_jack,
                   Card::heart_jack,
                   Card::diamond_nine,
                   Card::diamond_king});
  static auto const cards_to_play_without_nines
    = vector<Card>({Card::diamond_jack,
                   Card::heart_jack,
                   Card::diamond_king});

  auto const& cards_to_play
    = (   (rule()(Rule::Type::with_nines))
       ? cards_to_play_with_nines
       : cards_to_play_without_nines);

  // @heuristic::action      play a low trump
  for (auto const& c : cards_to_play) {
    if (   hand.contains(c)
        && !game.cards().is_special(HandCard(hand, c))) {
      rationale_.add(_("Heuristic::return::low trump: %s", _(c)));
      return c;
    }
  }

  rationale_.add(_("Heuristic::reject::no low trump in hand"));
  return Card::empty;
}

} // namespace Heuristics
