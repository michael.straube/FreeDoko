/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "remaining_tricks_to_me.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool RemainingTricksToMe::is_valid(GameTypeGroup const game_type,
                                   PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
RemainingTricksToMe::RemainingTricksToMe(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::remaining_tricks_to_me)
{ }

/** destructor
 **/
RemainingTricksToMe::~RemainingTricksToMe() = default;

/** @return  whether the conditions are met
 **/
bool
RemainingTricksToMe::conditions_met(Trick const& trick)
{
  if (!condition_startcard(trick))
    return false;
  if (condition_special_point_possible())
    return false;
  if (!condition_remaining_trump_tricks_to_me())
    return false;

  for (auto const color : game().cards().colors()) {
    if (!condition_remaining_tricks_to_me(color))
      return false;
  }
  return true;
}

/** @return   card to play
 **/
Card
RemainingTricksToMe::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  if (hand.hastrump()) {
    auto const card = hand.highest_trump();
    rationale_.add(_("Heuristic::return::highest trump: %s", _(card)));
    return card;
  }
  for (auto const color : game().cards().colors()) {
    if (hand.contains(color)) {
      auto const card = hand.highest_card(color);
      rationale_.add(_("Heuristic::return::highest color card: %s", _(card)));
      return card;
    }
  }
  return {};
}

/** @return   whether a special point is possible
 **/
bool
RemainingTricksToMe::condition_special_point_possible()
{
  auto const& game = this->game();
  auto const& rule = this->rule();
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  if (game.is_solo()) {
    rationale_.add(_("Heuristic::condition::solo game -- no special points possible"));
    return false;
  }
  if (   (hand.contains(Card::ace) || hand.contains(Card::ten))
      && cards_information.remaining_others(Card::ten) + cards_information.remaining_others(Card::ace) >= 3) {
    rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::doppelkopf)));
    return true;
  }
  if (rule(Rule::Type::extrapoint_charlie)) {
    if (hand.contains(Card::charlie)
        && (hand.hascolor() || hand.lowest_trump() != Card::charlie)) {
      rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::charlie)));
      return true;
    }
  }
  if (rule(Rule::Type::extrapoint_fox_last_trick)) {
    if (hand.contains(Card::fox)) {
      rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::fox_last_trick)));
      return true;
    }
  }
  if (rule(Rule::Type::extrapoint_dulle_jabs_dulle)) {
    if (   hand.contains(Card::dulle)
        && cards_information.remaining_others(Card::dulle)) {
      rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::dulle_caught_dulle)));
      return true;
    }
  }
  if (rule(Rule::Type::extrapoint_heart_trick)) {
    if (   hand.contains(Card::heart)
        && cards_information.remaining_others(Card::heart) >= 3) {
      rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::heart_trick)));
      return true;
    }
  }
  if (rule(Rule::Type::extrapoint_catch_charlie)) {
    if (cards_information.remaining_others(Card::charlie)) {
      rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::caught_charlie)));
      return true;
    }
  }
  if (rule(Rule::Type::extrapoint_catch_fox_last_trick)) {
    if (cards_information.remaining_others(game.cards().fox())) {
      rationale_.add(_("Heuristic::condition::special point %s possible", _(Specialpoint::Type::caught_fox_last_trick)));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no special point possible"));
  return false;
}

/** @return   whether the player gets all remaining tricks for trump
 **/
bool
RemainingTricksToMe::condition_remaining_trump_tricks_to_me()
{
  auto const& hand = this->hand();
  if (cards_information().remaining_trumps_others() == 0) {
    rationale_.add(_("Heuristic::condition::the other players have no trump"));
    return true;
  }
  if (!hand.hastrump()) {
    rationale_.add(_("Heuristic::condition::have no trump"));
    return false;
  }
  if (   hand.hascolor()
      && hand.count(Card::trump) < cards_information().remaining_trumps_others()) {
    rationale_.add(_("Heuristic::condition::have less trumps then the other players together"));
    return false;
  }

  if (hand.lowest_trump().jabs(cards_information().highest_remaining_trump_of_others())) {
    rationale_.add(_("Heuristic::condition::my lowest trump %s is not jabbed by the other players with %s",
                           _(hand.lowest_trump()),
                           _(cards_information().highest_remaining_trump_of_others())));
    return true;
  }

  rationale_.add(_("Heuristic::condition::not sure whether all remaining trumps goes to me"));
  return false;
}

/** @return   whether the player gets all remaining tricks for the tcolor
 **/
bool
RemainingTricksToMe::condition_remaining_tricks_to_me(Card::TColor const color)
{
  auto const& hand = this->hand();
  if (!hand.contains(color)) {
    rationale_.add(_("Heuristic::condition::have no tolor %s", _(color)));
    return true;
  }
  if (cards_information().remaining_others(color) == 0) {
    rationale_.add(_("Heuristic::condition::the other players have no tcolor %s", _(color)));
    return true;
  }

  if (!hand.lowest_card(color).is_jabbed_by(cards_information().highest_remaining_card_of_others(color))) {
    rationale_.add(_("Heuristic::condition::my lowest card %s of color %s is not jabbed by the other players with %s",
                           _(hand.lowest_card(color)),
                           _(color),
                           _(cards_information().highest_remaining_card_of_others(color))));
    return true;
  }

  rationale_.add(_("Heuristic::condition::not sure whether all remaining cards of %s goes to me", _(color)));
  return false;
}

} // namespace Heuristics
