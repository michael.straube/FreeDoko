/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_color.h"

#include "../../ai.h"
#include "../../../cards_information.of_player.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool StartWithColor::is_valid(GameTypeGroup const game_type,
                              PlayerTypeGroup const player_group)
{
  return is_with_partner(game_type, player_group);
}

/** constructor
 **/
StartWithColor::StartWithColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_color)
{ }

/** destructor
 **/
StartWithColor::~StartWithColor() = default;

/** @return  whether the conditions are met
 **/
bool
StartWithColor::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && !condition_next_player_guessed_opposite_team(trick)
         );
}

/** @return   card to play
 **/
Card
StartWithColor::card_to_play(Trick const& trick)
{
  auto const& player            = this->player();
  auto const& hand              = this->hand();
  auto const& cards_information = this->cards_information();
  auto const& game              = this->game();

  // best card so far to play
  HandCard best_card;
  auto const& next_player = trick.player_of_card(1);
  auto const& hand_of_next_player = guessed_hand(next_player);

  // maximal remaining cards of the selected color
  // @heuristic::condition   or there are enough remaining cards for all players
  //                         (players - me)
  auto max_remaining_cards_no = game.playerno() - 1;

  for (auto const color : game.cards().colors()) {
    if (color == game.cards().trumpcolor())
      continue;

    if (!hand.contains(color)) {
      continue;
    }

    if (cards_information.played(color)
        > (cards_information(player).played(color)
           + cards_information(next_player).played(color))) {
      rationale_.add(_("Heuristic::condition::another player has already played %s", _(color)));
      continue;
    }

    if (   hand_of_next_player.contains(color)
        && !hand_of_next_player.contains(color, Card::ace)) {
      rationale_.add(_("Heuristic::condition::the next player has %s, but not the ace", _(color)));
      continue;
    }

    auto const remaining_cards_no
      = cards_information.remaining_others(color);
    // there are at least as many remaining cards as the best so far
    if (remaining_cards_no < max_remaining_cards_no) {
      rationale_.add(_("Heuristic::condition::less remaining cards of %s then %s", _(color), _(best_card.color())));
      continue;
    }

    // search the card with the highest value
    auto const& card = hand.highest_card(color);

    if (   remaining_cards_no > max_remaining_cards_no
        || card.value() > best_card.value() ) {
      if (best_card)
        rationale_.add(_("Heuristic::condition::prefer %s to %s", _(color), _(best_card.color())));
      else
        rationale_.add(_("Heuristic::condition::select %s", _(color)));
      best_card = card;
      max_remaining_cards_no = remaining_cards_no;
    } // if (found better card)
  }

  if (!best_card) {
    rationale_.add(_("Heuristic::reject::no fitting color found"));
    return {};
  }
  rationale_.add(_("Heuristic::return::smallest color, highest value: %s", _(best_card)));
  return best_card;
} // Card Heuristics::start_with_color(Trick trick, HeuristicInterface hi)

} // namespace Heuristics
