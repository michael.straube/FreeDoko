/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_color_double_ace.h"

#include "../../ai.h"
#include "../../../../game/game.h"


namespace Heuristics {

auto StartWithColorDoubleAce::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return true;
}


StartWithColorDoubleAce::StartWithColorDoubleAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_color_double_ace)
{ }


StartWithColorDoubleAce::~StartWithColorDoubleAce() = default;


auto StartWithColorDoubleAce::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_have_color_double_ace()
         );
}


auto StartWithColorDoubleAce::card_to_play(Trick const& trick) -> Card
{
  auto const ace = get_double_ace(trick);
  if (!ace) {
    rationale_.add(_("Heuristic::reject::no suitable double color ace found"));
    return {};
  }

  rationale_.add(_("Heuristic::return::ace found: %s", _(ace)));
  return ace;
}


auto StartWithColorDoubleAce::get_double_ace(Trick const& trick) -> Card
{
  auto const& game = trick.game();
  auto const opposite_trump_behind = condition_opposite_trump_behind(trick);
  auto const single_picture_solo_re = (is_single_picture_solo(game.type()) && player().team() == Team::re);
  auto const solo_contra = (is_solo(game.type()) && player().team() == Team::contra);

  auto color_valid
    = [&](Card::Color const color) {
      return (   condition_have_color_double_ace(color)
              && (   single_picture_solo_re
                  || !opposite_trump_behind
                  || (   condition_opposite_players_behind_have_color(trick, color)
                      && (   solo_contra
                          || condition_enough_remaining_color_cards(trick, color)))));
    };
  auto const color = best_of_if(game.cards().colors(),
                                color_valid,
                                [this](Card::Color const color) {
                                return cards_information().remaining_others(color);
                                }
                               );

  if (color == Card::nocardcolor)
    return {};
  return Card(color, Card::ace);
}


auto StartWithColorDoubleAce::condition_have_color_double_ace() -> bool
{
  auto const& hand = this->hand();
  for (auto const& card : hand) {
    if (   card.value() == Card::ace
        && !card.istrump()
        && hand.count(card) == 2
       ) {
      rationale_.add(_("Heuristic::condition::have a color double ace"));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::have no color double ace"));
  return false;
}


auto StartWithColorDoubleAce::condition_have_color_double_ace(Card::Color const color) -> bool
{
  auto const ace = Card(color, Card::ace);
  auto const& hand = this->hand();
  auto const& game = this->game();
  if (ace.istrump(game)) {
    rationale_.add(_("Heuristic::condition::card %s is trump", _(ace)));
    return false;
  }
  auto const count = hand.count(ace);
  if (count == 0) {
    rationale_.add(_("Heuristic::condition::have no card %s", _(ace)));
    return false;
  }
  if (count == 1) {
    rationale_.add(_("Heuristic::condition::have %u of %s", count, _(ace)));
    return false;
  }
  rationale_.add(_("Heuristic::condition::have a double card %s", _(ace)));
  return true;
}

} // namespace Heuristics
