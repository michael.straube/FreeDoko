/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_lowest_color.h"

#include "../../../player.h"

namespace Heuristics {


auto StartWithLowestColor::is_valid(GameTypeGroup const game_type,
                                    PlayerTypeGroup const player_group) -> bool
{
  return true;
}


StartWithLowestColor::StartWithLowestColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_lowest_color)
{ }


StartWithLowestColor::~StartWithLowestColor() = default;


auto StartWithLowestColor::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_has_color());
}


auto StartWithLowestColor::card_to_play(Trick const& trick) -> Card
{
  auto const& hand = this->hand();
  HandCard best_card;

  for (auto const& card : hand) {
    if (card.istrump())
      continue;

    if (   !best_card
        || card.value() < best_card.value()) {
      best_card = card;
    }
  }
  if (!best_card) {
    rationale_.add(_("Heuristic::reject::no color card found"));
    return {};
  }

  rationale_.add(_("Heuristic::return::lowest color card: %s", _(best_card)));
  return best_card;
}

} // namespace Heuristics
