/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_lowest_trump.h"

#include "../../ai.h"
#include "../../../../game/game.h"
#include "../../../../game/cards.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool StartWithLowestTrump::is_valid(GameTypeGroup const game_type,
                                    PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

/** constructor
 **/
StartWithLowestTrump::StartWithLowestTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_lowest_trump)
{ }

/** destructor
 **/
StartWithLowestTrump::~StartWithLowestTrump() = default;

/** @return  whether the conditions are met
 **/
bool
StartWithLowestTrump::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_has_trump());
}

/** @return   card to play
 **/
Card
StartWithLowestTrump::card_to_play(Trick const& trick)
{
  return (  is_picture_solo(game().type())
          ? lowest_trump()
          : best_lowest_trump_in_trumpcolor());
}

/** @return   the lowest trump in a game with a trumpcolor
 **/
Card
StartWithLowestTrump::best_lowest_trump_in_trumpcolor()
{
  auto const& hand = this->hand();
  auto const trumpcolor = game().cards().trumpcolor();

  auto const cards_ascending = {
    Card(trumpcolor, Card::nine),
    Card(trumpcolor, Card::king),
    Card::diamond_jack,
    Card::heart_jack,
    Card::spade_jack,
    Card::club_jack,
    Card::diamond_queen,
    Card::heart_queen,
    Card::spade_queen,
    Card(trumpcolor, Card::ten),
    Card::club_queen,
    Card::club_queen,
  };
  for (auto const card : cards_ascending) {
    if (   !HandCard(hand, card).is_special()
        && hand.contains(card)) {
      rationale_.add(_("Heuristic::return::lowest trump: %s", _(card)));
      return card;
    }
  }
  // remaining cards: fox, dulle, swines, hyperswines

  auto const dulle = game().cards().dulle();
  if (hand.contains(dulle)) {
    if (   rule()(Rule::Type::dullen_second_over_first)
        && cards_information().remaining_others(dulle)) {
      if (hand.has_swines()) {
        auto const swine = game().cards().swine();
        rationale_.add(_("Heuristic::return::prefer %s to dulle", _(swine)));
        return swine;
      }
      if (hand.has_hyperswines()) {
        auto const hyperswine = game().cards().hyperswine();
        rationale_.add(_("Heuristic::return::prefer %s to dulle", _(hyperswine)));
        return hyperswine;
      }
    }

    rationale_.add(_("Heuristic::return::lowest trump: %s", _(dulle)));
    return dulle;
  }

  if (hand.has_swines()) {
    auto const swine = game().cards().swine();
    rationale_.add(_("Heuristic::return::swine %s", _(swine)));
    return swine;
  }
  if (hand.has_hyperswines()) {
    auto const hyperswine = game().cards().hyperswine();
    rationale_.add(_("Heuristic::return::hyperswine %s", _(hyperswine)));
    return hyperswine;
  }

  return lowest_trump();
}

/** @return   the lowest trump in a game with a trumpcolor
 **/
Card
StartWithLowestTrump::lowest_trump()
{
  auto const card = hand().lowest_trump();
  rationale_.add(_("Heuristic::return::lowest trump: %s", _(card)));
  return card;
}

} // namespace Heuristics
