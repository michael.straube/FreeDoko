/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_trump_for_partner_color_ace.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool StartWithTrumpForPartnerColorAce::is_valid(GameTypeGroup const game_type,
                                                PlayerTypeGroup const player_group)
{
  return (   is_with_trump_color(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
StartWithTrumpForPartnerColorAce::StartWithTrumpForPartnerColorAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_trump_for_partner_color_ace)
{ }

/** destructor
 **/
StartWithTrumpForPartnerColorAce::~StartWithTrumpForPartnerColorAce() = default;

/** @return  whether the conditions are met
 **/
bool
StartWithTrumpForPartnerColorAce::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_last_player_same_team(trick)
          && condition_partner_can_have_color_ace()
          && condition_partner_can_have_high_trump()
         );
}

/** @return   card to play
 **/
Card
StartWithTrumpForPartnerColorAce::card_to_play(Trick const& trick)
{
  auto const king = Card(game().cards().trumpcolor(), Card::king);
  if (   hand().contains(king)
      && !HandCard(hand(), king).is_special()) {
    rationale_.add(_("Heuristic::return::low trump: %s", _(king)));
    return king;
  }

  auto const card = lowest_best_trump_card(trick);
  rationale_.add(_("Heuristic::return::low trump: %s", _(card)));
  return card;
}

/** @return   whether the partner in the backhand can have a color ace to start with
 **/
bool
StartWithTrumpForPartnerColorAce::condition_partner_can_have_color_ace()
{
  auto const& game = this->game();
  auto const& cards_information = this->cards_information();
  auto const& partner = game.players().previous(player());
  for (auto const color : game.cards().colors()) {
    if (color == game.cards().trumpcolor())
      continue;
    if (cards_information.played(color))
      continue;
    auto const ace = Card(color, Card::ace);
    if (guessed_hand(partner).contains(ace)) {
      rationale_.add(_("Heuristic::condition::%s can have card %s", partner.name(), _(ace)));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::%s has no color ace", partner.name()));
  return false;
}

/** @return   whether the partner can have a high trump to jab
 **/
bool
StartWithTrumpForPartnerColorAce::condition_partner_can_have_high_trump()
{
  auto const& game = this->game();
  auto const& rule = this->rule();
  auto const& partner = game.players().previous(player());
  auto const& swines = game.swines();
  auto const& cards_information = this->cards_information();
  if (swines.hyperswines_announced()) {
    if (swines.hyperswines_owner() == partner) {
      rationale_.add(_("Heuristic::condition::%s has hyperswines", partner.name()));
      return true;
    } else {
      rationale_.add(_("Heuristic::condition::%s has not the hyperswines", partner.name()));
      return false;
    }
  }
  if (swines.swines_announced()) {
    if (swines.swines_owner() == partner) {
      rationale_.add(_("Heuristic::condition::%s has swines", partner.name()));
      return true;
    } else {
      rationale_.add(_("Heuristic::condition::%s has not the swines", partner.name()));
      return false;
    }
  }
  if (partner.announcement() != Announcement::noannouncement) {
    rationale_.add(_("Heuristic::condition::%s has made an announcement", partner.name()));
    return true;
  }
  auto const& hand_of_partner = guessed_hand(partner);
  if (rule(Rule::Type::dullen)) {
    if (!rule(Rule::Type::dullen_second_over_first)) {
      rationale_.add(_("Heuristic::condition::not rule %s", _(Rule::Type::dullen_second_over_first)));
      return false;
    }
    if (hand_of_partner.contains(Card::dulle)) {
      rationale_.add(_("Heuristic::condition::%s can have a dulle", partner.name()));
      return true;
    } else if (cards_information.remaining_others(Card::dulle)) {
      rationale_.add(_("Heuristic::condition::%s does not have a dulle", partner.name()));
      return false;
    }
  }
  if (hand_of_partner.contains(Card::club_queen)) {
    rationale_.add(_("Heuristic::condition::%s can have card %s", partner.name(), _(Card::club_queen)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::%s has no card %s", partner.name(), _(Card::club_queen)));
    return false;
  }
}

} // namespace Heuristics
