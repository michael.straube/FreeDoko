/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "gametree.h"

#include "../ai.h"
#include "../gametree.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool Gametree::is_valid(GameTypeGroup const game_type,
                        PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
Gametree::Gametree(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::gametree)
{ }

/** destructor
 **/
Gametree::~Gametree() = default;

/** @return  whether the conditions are met
 **/
bool
Gametree::conditions_met(Trick const& trick)
{
  return (   condition_card_forgotten() == 0
          && !condition_future_limit_exceeded(trick));
}

/** @return   card to play
 **/
Card
Gametree::card_to_play(Trick const& trick)
{
  auto const card = ::Gametree(ai()).best_card();

  rationale_.add(_("Heuristic::return::best card found with gametree: %s", _(card)));
  return card;
}

/** @return   whether the future limit is exceeded by the remaining tricks
 **/
bool
Gametree::condition_card_forgotten()
{
  return cards_information().forgotten_cards_no() > 0;
}

/** @return   whether the partner has the highest trump in the game
 **/
bool
Gametree::condition_future_limit_exceeded(Trick const& trick)
{
  auto future_limit = value(Aiconfig::Type::future_limit);

  auto const& game = this->game();
  size_t remaining_tricks_no = game.tricks().remaining_no();
  if (!trick.isempty()) {
    for (auto const& player : trick.remaining_players()) {
      future_limit /= guessed_hand(player).validcards(trick).size();
      if (future_limit < 1) {
        rationale_.add(_("Heuristic::condition::future limit is exceeded"));
        return true;
      }
    }
    remaining_tricks_no -= 1;
  }
  if (   remaining_tricks_no <= 5
      && future_limit / factorial(remaining_tricks_no) / factorial(3 * remaining_tricks_no) >= 1) {
    rationale_.add(_("Heuristic::condition::future limit is not exceeded"));
    return false;
  }

  for (auto t = remaining_tricks_no; t > 0; --t) {
    for (auto const& player : game.players()) {
      future_limit /= min(min(guessed_hand(player).cards_single().size(),
                              guessed_hand(player).cardsnumber() - (remaining_tricks_no - t)),
                          3 * remaining_tricks_no);
      if (future_limit < 1) {
        rationale_.add(_("Heuristic::condition::future limit is exceeded"));
        return true;
      }
    }
  }
  rationale_.add(_("Heuristic::condition::future limit is not exceeded"));
  return false;
}

} // namespace Heuristics
