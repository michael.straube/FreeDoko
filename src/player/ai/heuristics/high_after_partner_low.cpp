/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "high_after_partner_low.h"

#include "../ai.h"
#include "../../../card/hand.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool HighAfterPartnerLow::is_valid(GameTypeGroup const game_type,
                                   PlayerTypeGroup const player_group)
{
  return (   is_with_partner(game_type, player_group)
          && is_with_trump_color(game_type));
}

/** constructor
 **/
HighAfterPartnerLow::HighAfterPartnerLow(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::high_after_partner_low)
{ }

/** destructor
 **/
HighAfterPartnerLow::~HighAfterPartnerLow() = default;

/** @return  whether the conditions are met
 **/
bool
HighAfterPartnerLow::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && condition_trump_trick(trick)
          && condition_partner_played_small_card(trick)
          && !condition_partner_announcement()
         );
}

/** @return   card to play
 **/
Card
HighAfterPartnerLow::card_to_play(Trick const& trick)
{
  auto const card = jabbing_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  if (card.points() >= 10 && guessed_hand(trick.lastplayer()).can_jab(trick + card)) {
    rationale_.add(_("Heuristic::reject::do not jab with card %s", _(card)));
    return {};
  }
  if (   is_with_trump_color(game().type())
      && card.value() == Card::nine) {
    for (auto const card : {Card::diamond_jack, Card::heart_jack}) {
      if (   hand().contains(card)
          && trick.isjabbed(card, hand())) {
        rationale_.add(_("Heuristic::return::jab with %s", _(card)));
        return card;
      }
    }
  }
  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}

bool HighAfterPartnerLow::condition_partner_played_small_card(Trick const& trick)
{
  Card card;
  if (guessed_same_team(trick.startplayer())) {
    card = trick.startcard();
  }
  if (trick.actcardno() >= 2 && guessed_same_team(trick.player_of_card(1))) {
    card = trick.card(1);
  }
  if (guessed_opposite_team(trick.lastplayer())) {
    card = trick.winnercard();
  }
  if (!card) {
    rationale_.add(_("Heuristic::condition::guessed partner behind"));
    return false;
  }
  auto const trump_card_limit = ai().trump_card_limit();
  if (trump_card_limit.jabs(card)) {
    rationale_.add(_("Heuristic::condition::the card of the partner (%s) is less then trump card limit (%s)", _(trick.winnercard()), _(trump_card_limit)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::the card of the partner (%s) is not less then trump card limit (%s)", _(trick.winnercard()), _(trump_card_limit)));
    return false;
  }
}


HandCard HighAfterPartnerLow::jabbing_card(Trick const& trick)
{
  if (false)
  {
    auto const card = hand().highest_trump();
    if (card.is_special()) {
      if (card.ishyperswine()) {
        rationale_.add(_("Heuristic::result::jab with highest trump %s", _(card)));
        return card;
      }
      if (condition_opposite_swine_or_hyperswine_behind(trick)) {
        rationale_.add(_("Heuristic::condition::swine or hyperswine behind"));
        // take a queen
      } else {
        if (card.isswine()) {
          rationale_.add(_("Heuristic::result::jab with highest trump %s", _(card)));
          return card;
        }
        if (card.isdulle()) {
          if (   rule()(Rule::Type::dullen_second_over_first)
              && condition_opposite_players_behind_have(trick, Card::dulle)) {
            rationale_.add(_("Heuristic::condition::dulle could get jabbed"));
            // take a queen
          } else {
            rationale_.add(_("Heuristic::result::jab with highest trump %s", _(card)));
            return card;
          }
        }
      }
    }
  }
  auto const card = hand().same_or_lower_card(Card::club_queen);
  if (!trick.isjabbed(card)) {
    rationale_.add(_("Heuristic::reject::cannot jab trick with queen"));
    return {};
  }
  if (ai().trump_card_limit().jabs(card)) {
    rationale_.add(_("Heuristic::reject::jabbing card %s below trump card limit"));
    return {};
  }
  rationale_.add(_("Heuristic::result::jab with high trump %s", _(card)));
  return card;
}

} // namespace Heuristics
