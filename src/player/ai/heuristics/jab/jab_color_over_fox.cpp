/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_color_over_fox.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabColorOverFox::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}

/** constructor
 **/
JabColorOverFox::JabColorOverFox(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_color_over_fox)
{ }

/** destructor
 **/
JabColorOverFox::~JabColorOverFox() = default;

/** @return  whether the conditions are met
 **/
bool
JabColorOverFox::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && condition_color_trick(trick)
          && condition_can_jab_with_trump(trick)
          && !condition_winnercard_trump(trick));
}

/** @return   card to play
 **/
Card
JabColorOverFox::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  auto const& game = this->game();

  auto const card = hand.same_or_higher_card(Card::diamond_jack);
  if (!card) {
    rationale_.add(_("Heuristic::condition::no card found over the fox"));
    return {};
  }
  // check to play a club queen
  if (   game.type() == GameType::normal
      // ToDo: check the own gameplay for team informations
      && game.teaminfo().get(player()) == Team::unknown
      && team() == Team::re
      && hand.contains(Card::club_queen)
      && card.value() == Card::queen) {
    if (card.color() == Card::spade) {
      rationale_.add(_("Heuristic::return::%s", _(Card::club_queen)));
      return Card::club_queen;
    }
    if (   condition_opposite_or_unknown_player_behind(trick)
        && (   trick.points() >= 8
            || trick.actcardno() == 1)) {
      rationale_.add(_("Heuristic::return::%s", _(Card::club_queen)));
      return Card::club_queen;
    }
  }
  if (card.is_special()) {
    rationale_.add(_("Heuristic::reject::can only jab with a special card"));
    return {};
  }
  rationale_.add(_("Heuristic::return::next trump over fox: %s", _(card)));
  return card;
}

} // namespace Heuristics
