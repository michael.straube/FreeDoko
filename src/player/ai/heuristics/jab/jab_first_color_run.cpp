/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_first_color_run.h"

#include "../../../../card/trick.h"
#include "../../../../game/game.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabFirstColorRun::is_valid(GameTypeGroup const game_type,
                                PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

/** constructor
 **/
JabFirstColorRun::JabFirstColorRun(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_first_color_run)
{ }

/** destructor
 **/
JabFirstColorRun::~JabFirstColorRun() = default;

/** @return  whether the conditions are met
 **/
bool
JabFirstColorRun::conditions_met(Trick const& trick)
{
  return (   condition_first_color_run(trick)
          && condition_can_jab_with_trump(trick));
}

/** @return   card to play
 **/
Card
JabFirstColorRun::card_to_play(Trick const& trick)
{
  if (   following_opposite_players_serve(trick)
      && cards_information().remaining(trick.startcard().color()) > trick.remainingcardno()) {
    auto const card = get_card_at_most_fox(trick);
    if (card)
      return card;
  }

  Card card;
  if (cards_information().remaining(trick.startcard().color()) == trick.remainingcardno() - 1) {
    card = hand().same_or_higher_card(  trick.points() >= 10
                                      ? Card::diamond_queen
                                      : Card::diamond_jack);
  }

  if (!card)
    card = get_card_above_fox(trick);

  if (   card == Card::spade_queen
      && team() == Team::re
      && game().teaminfo().get(player()) != Team::re) {
    rationale_.add(_("Heuristic::jab::prefer club queen in order to show my team"));
    return Card::club_queen;
  }
  return card;

}

/** @return   whether the following opposite players serve the color trick
 **/
bool
JabFirstColorRun::following_opposite_players_serve(Trick const& trick)
{
  auto const color = trick.startcard().color();
  for (auto const& player : trick.remaining_following_players()) {
    if (   !guessed_same_team(player)
        && !guessed_hand(player).contains(color)) {
      return false;
    }
  }
  return true;
}

/** @return  jabbing card that is at most the fox
 **/
Card
JabFirstColorRun::get_card_at_most_fox(Trick const& trick)
{
  for (auto const& card : hand().cards_at_max_fox()) {
    if (trick.isjabbed(card)) {
      rationale_.add(_("Heuristic::return::jab with %s", _(card)));
      return card;
    }
  }
  return {};
}

/** @return  jabbing card that is above the fox
 **/
Card
JabFirstColorRun::get_card_above_fox(Trick const& trick)
{
  auto const card = hand().same_or_higher_card(Card::diamond_jack);
  if (!trick.isjabbed(card)) {
    auto const card = hand().next_jabbing_card(trick.winnercard());
    rationale_.add(_("Heuristic::condition::next jabbing card: %s", _(card)));
    return card;
  }
  if (!card) {
    rationale_.add(_("Heuristic::condition::no card found over the fox"));
    auto const card = hand().highest_trump();
    rationale_.add(_("Heuristic::return::highest trump: %s", _(card)));
    return card;
  }
  rationale_.add(_("Heuristic::return::next trump over fox: %s", _(card)));
  return card;
}

} // namespace Heuristics
