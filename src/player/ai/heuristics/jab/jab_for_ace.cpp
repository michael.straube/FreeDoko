/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_for_ace.h"
#include "../first_card/start_with_color_single_ace.h"

#include "../../ai.h"

namespace Heuristics {


auto JabForAce::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump(game_type);
}


JabForAce::JabForAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_for_ace)
{ }


JabForAce::~JabForAce() = default;


auto JabForAce::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_marriage_decision_for_me(trick)
          && condition_have_single_color_ace_to_start_with()
          && condition_can_jab_with_trump(trick)
          && !condition_partner_already_jabbed_high(trick)
         );
}


auto JabForAce::card_to_play(Trick const& trick) -> Card
{
  auto const card = jabbing_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  rationale_.add(_("Heuristic::return::card to jab with: %s", _(card)));
  return card;
}


auto JabForAce::condition_partner_already_jabbed_high(Trick const& trick) -> bool
{
  if (!condition_winnerplayer_guessed_same_team(trick))
    return false;
  auto const& winnercard = trick.winnercard();
  if (!winnercard.istrump()) {
    rationale_.add(_("Heuristic::condition::winnercard is no trump"));
    return false;
  }
  if (below_lowest_trump_card_limit(winnercard)) {
    rationale_.add(_("Heuristic::condition::winnercard is a low trump"));
    return false;
  }
  if (   below_trump_card_limit(winnercard)
      && (   trick.points() > 6
          || trick.startcard().istrump()
          || cards_information().color_runs(trick.startcard().tcolor()) >= 2)) {
    rationale_.add(_("Heuristic::condition::winnercard is no high trump"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::winnercard is a high trump"));
  return true;
}


auto JabForAce::condition_have_single_color_ace_to_start_with() -> bool
{
  auto start_with_color_single_ace = StartWithColorSingleAce(ai());
  start_with_color_single_ace.set_player(player());
  auto const new_trick = Trick(player());
  auto const ace = start_with_color_single_ace.get_card(new_trick);

  if (!ace) {
    rationale_.add(_("Heuristic::condition::no suitable single color ace found to play"));
    return false;
  }
  if (cards_information().played(ace.color()) > 0) {
    rationale_.add(_("Heuristic::condition::not first color run in %s", _(ace.color())));
    return false;
  }
  rationale_.add(_("Heuristic::condition::found single ace %s to play", _(ace)));
  return true;
}


auto JabForAce::jabbing_card(Trick const& trick) -> HandCard
{
  if (   trick.startcard().istrump()
      && !trick.islastcard()
      && !only_own_team_behind(trick)) {
    auto const card = jabbing_card_for_first_trick(trick);
    if (!trick.isjabbed(card))
      return {};
    rationale_.add(_("Heuristic::info::no color trick so far -> take high trump %s", _(card)));
    return card;

  }
  auto const card = best_jabbing_card(trick);
  rationale_.add(_("Heuristic::info::best jabbing card: %s", _(card)));
  return card;
}


auto JabForAce::jabbing_card_for_first_trick(Trick trick) -> HandCard
{
  auto start_with_color_single_ace = StartWithColorSingleAce(ai());
  start_with_color_single_ace.set_player(player());
  auto const new_trick = Trick(player());
  auto const ace = start_with_color_single_ace.get_card(new_trick);
  if (!ace)
    return {};
  if (cards_information().remaining_others(ace.color()) == 3) {
    rationale_.add(_("Heuristic::info::only 3 other cards in color %s", _(ace.color())));
    auto const card = hand().highest_trump_till(Card::club_queen);
    if (!trick.isjabbed(card))
      return {};
    rationale_.add(_("Heuristic::return::highest trump till club queen: %s", _(card)));
    return card;
  }

  auto const card = hand().highest_trump();
  if (   card.is_special()
      && condition_can_be_jabbed_by_opposite_team(trick, card)) {
    auto const card = hand().highest_trump_till(Card::club_queen);
    if (!trick.isjabbed(card))
      return {};
    rationale_.add(_("Heuristic::return::highest trump till club queen: %s", _(card)));
    return card;
  }
    rationale_.add(_("Heuristic::return::highest trump: %s", _(card)));
  return card;
}

} // namespace Heuristics
