/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_rich_trick.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabRichTrick::is_valid(GameTypeGroup const game_type,
                            PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}

/** constructor
 **/
JabRichTrick::JabRichTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_rich_trick)
{ }

/** destructor
 **/
JabRichTrick::~JabRichTrick() = default;

/** @return  whether the conditions are met
 **/
bool
JabRichTrick::conditions_met(Trick const& trick)
{
  return (   condition_trick_is_rich(trick)
          && !condition_is_marriage_determination_trick(trick)
          && condition_can_jab_with_trump(trick)
          && !(   condition_winnerplayer_same_team(trick)
               && !condition_opposite_or_unknown_team_can_jab(trick))
          && !condition_partner_jabbed_with_high_trump(trick)
          );
}

/** @return   card to play
 **/
Card
JabRichTrick::card_to_play(Trick const& trick)
{
  if (!condition_opposite_or_unknown_player_behind(trick)) {
    auto const card = jabbing_card_for_last_player(trick);
    rationale_.add(_("Heuristic::return::next jabbing card: %s", _(card)));
    return card;
  }
  auto const card = HandCard(hand(), result_high_trump_jabbing_card(trick));
  if (card.is_special()) {
    auto const trick_points_modifier = (  (aggressive() ? -2 : 0)
                                        + (cautious()   ?  2 : 0)
                                        + (game().type() == GameType::poverty ? 6 : 0));
    if (trick.points() + 4 * trick.remainingcardno()
        < static_cast<Trick::Points>(12 + trick_points_modifier)) {
      rationale_.add(_("Heuristic::reject::too few points for a special card"));
      return {};
    }
  }
  return card;
}

/** @return   whether the partner has jabbed with a high trump
 **/
bool
JabRichTrick::condition_trick_is_rich(Trick const& trick)
{
  if (trick.points() + 2 * trick.remainingcardno()
      >= static_cast<Trick::Points>(14
                                    - (aggressive() ? 2 : 0)
                                    + (cautious() ? 2 : 0))) {
    rationale_.add(_("Heuristic::condition::the trick is rich"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the trick is not rich"));
  return false;
}

} // namespace Heuristics
