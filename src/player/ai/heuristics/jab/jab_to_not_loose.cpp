/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_to_not_loose.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabToNotLoose::is_valid(GameTypeGroup const game_type,
                        PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
JabToNotLoose::JabToNotLoose(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_to_not_loose)
{ }

/** destructor
 **/
JabToNotLoose::~JabToNotLoose() = default;

/** @return  whether the conditions are met
 **/
bool
JabToNotLoose::conditions_met(Trick const& trick)
{
  return (   !condition_own_team_lost()
          && condition_own_team_can_loose(trick)
          && !condition_winnerplayer_same_team(trick)
          && condition_can_jab(trick)
         );
}

/** @return   card to play
 **/
Card
JabToNotLoose::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  auto const card = best_jabbing_card(trick);

  if (   hand.count(Card::trump) >= 4
      && trick.points() <= 10
      && card.jabs(trump_card_limit())
      && (   hand.count_fehl() > 0
          || hand.count_le(trump_card_limit()) > 2)
      ) {
    rationale_.add(_("Heuristic::reject::jabbing card is too high for this small trick"));
    return {};
  }

  rationale_.add(_("Heuristic::return::jab to not loose: %s", _(card)));
  return card;
}

/** @return   whether the own team can loose with this trick
 **/
bool
JabToNotLoose::condition_own_team_lost()
{
  auto const team = this->team();
  auto const& game = this->game();
  auto const opposite_points = points_of_opposite_team(false);
  auto const needed_points = game.needed_points_to_win(opposite(team));
  if (opposite_points >= needed_points) {
    rationale_.add(_("Heuristic::condition::opposite team has enough points to win: %u >= %u", opposite_points, needed_points));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::opposite team has not enough points to have won: %u < %u", opposite_points, needed_points));
    return false;
  }
}

/** @return   whether the own team can loose with this trick
 **/
bool
JabToNotLoose::condition_own_team_can_loose(Trick const& trick)
{
  auto const team = this->team();
  auto const& game = this->game();
  auto const opposite_points = points_of_opposite_team(false);
  auto const needed_points = game.needed_points_to_win(opposite(team));
  auto points = trick.points();
  for (auto const& player : trick.remaining_players()) {
    points += guessed_hand(player).lowest_value();
  }
  if (opposite_points >= needed_points) {
    rationale_.add(_("Heuristic::condition::opposite team has enough points to win: %u >= %u", opposite_points, needed_points));
    return true;
  } else if (opposite_points + points >= needed_points) {
    rationale_.add(_("Heuristic::condition::opposite team can win with this trick"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::opposite team cannot win with this trick"));
    return false;
  }
}

} // namespace Heuristics
