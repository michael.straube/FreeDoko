/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_to_win.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabToWin::is_valid(GameTypeGroup const game_type,
                        PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
JabToWin::JabToWin(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_to_win)
{ }

/** destructor
 **/
JabToWin::~JabToWin() = default;

/** @return  whether the conditions are met
 **/
bool
JabToWin::conditions_met(Trick const& trick)
{
  return (   !condition_own_team_won()
          && !condition_winnerplayer_same_team(trick)
          && condition_can_jab(trick)
         );
}

/** @return   card to play
 **/
Card
JabToWin::card_to_play(Trick const& trick)
{
  auto const team = this->team();
  auto const& game = this->game();

  auto const card = best_jabbing_card(trick);

  auto const own_points = points_of_own_team(false);
  auto const needed_points = game.needed_points_to_win(team);

  if (own_points + trick.points() + card.value() >= needed_points) {
    rationale_.add(_("Heuristic::return::jab to win: %s", _(card)));
    return card;
  }

  rationale_.add(_("Heuristic::reject::cannot jab to get all points to win"));
  return {};
}

/** @return   whether the own team already has one
 **/
bool
JabToWin::condition_own_team_won()
{
  auto const team = this->team();
  auto const& game = this->game();
  auto const own_points = points_of_own_team(false);
  auto const needed_points = game.needed_points_to_win(team);
  if (own_points >= needed_points) {
    rationale_.add(_("Heuristic::condition::own team has enough points to win: %u >= %u", own_points, needed_points));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::own team has not enough points to win: %u < %u", own_points, needed_points));
    return true;
  }
}

} // namespace Heuristics
