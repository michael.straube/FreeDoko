/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_trump_because_of_partner_announcement.h"

#include "../../ai.h"

namespace Heuristics {

auto JabTrumpBecauseOfPartnerAnnouncement::is_valid(GameTypeGroup const game_type,
                                                    PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_trump(game_type)
          && is_with_partner(game_type, player_group));
}


JabTrumpBecauseOfPartnerAnnouncement::JabTrumpBecauseOfPartnerAnnouncement(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_trump_because_of_partner_announcement)
{ }


JabTrumpBecauseOfPartnerAnnouncement::~JabTrumpBecauseOfPartnerAnnouncement() = default;


auto JabTrumpBecauseOfPartnerAnnouncement::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_winnerplayer_same_team(trick)
          && condition_trump_trick(trick)
          && condition_can_jab(trick)
          && !condition_partner_behind(trick)
          && condition_partner_announcement()
         );
}


auto JabTrumpBecauseOfPartnerAnnouncement::card_to_play(Trick const& trick) -> Card
{
  auto const& hand = this->hand();

  if (   game().announcements().last(team()) >= Announcement::no60
      && condition_opposite_trump_behind(trick)) {
    return result_high_trump_jabbing_card(trick);
  }

  auto const card = hand.next_jabbing_card(trick.winnercard());
  if (   card.value() >= 10
      && !card.is_special()) {
    rationale_.add(_("Heuristic::reject::next jabbing card at most the fox"));
    return {};
  }

  if (card.is_special()) {
    if (condition_can_be_jabbed_by_opposite_team(trick, card)) {
      rationale_.add(_("Heuristic::reject::next jabbing card is special and can be jabbed"));
      return {};
    }
    rationale_.add(_("Heuristic::return::next jabbing card: %s", _(card)));
    return card;
  }

  rationale_.add(_("Heuristic::return::next jabbing card: %s", _(card)));
  return card;
}

} // namespace Heuristics
