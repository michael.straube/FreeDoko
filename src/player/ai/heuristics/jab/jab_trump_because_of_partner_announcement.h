/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../heuristic.h"

namespace Heuristics {

/** Jab a trump trick when the partner has made an announcement
 **/
class JabTrumpBecauseOfPartnerAnnouncement : public Heuristic {
public:
static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
explicit JabTrumpBecauseOfPartnerAnnouncement(Ai const& ai);
JabTrumpBecauseOfPartnerAnnouncement(JabTrumpBecauseOfPartnerAnnouncement const&) = delete;
JabTrumpBecauseOfPartnerAnnouncement& operator=(JabTrumpBecauseOfPartnerAnnouncement const&) = delete;

~JabTrumpBecauseOfPartnerAnnouncement() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;
};
} // namespace Heuristics
