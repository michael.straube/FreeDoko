/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_trump_over_fox.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabTrumpOverFox::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}

/** constructor
 **/
JabTrumpOverFox::JabTrumpOverFox(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_trump_over_fox)
{ }

/** destructor
 **/
JabTrumpOverFox::~JabTrumpOverFox() = default;

/** @return  whether the conditions are met
 **/
bool
JabTrumpOverFox::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && condition_trump_trick(trick)
          && !condition_next_player_guessed_same_team(trick)
          && condition_winnercard_below_fox(trick)
          && condition_remaining_fox_in_game(trick)
          && !condition_last_player_guessed_same_team(trick)
         );
}

/** @return   card to play
 **/
Card
JabTrumpOverFox::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();

  auto const card = hand.same_or_higher_card(Card::diamond_jack);
  if (!card) {
    rationale_.add(_("Heuristic::condition::no card found over the fox"));
    return {};
  }

  if (card.is_special()) {
    rationale_.add(_("Heuristic::reject::next card over fox is special"));
    return {};
  }

  if (!below_trump_card_limit(card)) {
    rationale_.add(_("Heuristic::reject::next card over fox is not below trump card limit"));
    return {};
  }

  rationale_.add(_("Heuristic::return::next card over fox: %s", _(card)));
  return card;
}

/** @return   whether the winnercard is below the fox
 **/
bool
JabTrumpOverFox::condition_winnercard_below_fox(Trick const& trick)
{
  if (trick.winnercard() == game().cards().fox()) {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is not below the fox", _(trick.winnercard())));
    return false;
  } else if (trick.isjabbed({hand(), Card::diamond_jack})) {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is below the fox", _(trick.winnercard())));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::the winnercard (%s) is not below the fox", _(trick.winnercard())));
    return false;
  }
}

/** @return   whether a player behind, not of the same team, still has a fox
 **/
bool
JabTrumpOverFox::condition_remaining_fox_in_game(Trick const& trick)
{
  auto const fox = game().cards().fox();
  auto const ten = Card(game().cards().trumpcolor(), Card::ten);
  for (Player const& player : trick.remaining_following_players()) {
    if (guessed_same_team(player))
      continue;
    auto const& hand = guessed_hand(player);
    if (hand.contains(fox)) {
      rationale_.add(_("Heuristic::condition::%s still can have a fox", player.name()));
      return true;
    }
    if (   hand.contains(ten)
        && !HandCard(hand, ten).is_special()) {
      rationale_.add(_("Heuristic::condition::%s still can have a %s", player.name(), _(ten)));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::no other player has a fox or a trump ten"));
  return false;
}

} // namespace Heuristics
