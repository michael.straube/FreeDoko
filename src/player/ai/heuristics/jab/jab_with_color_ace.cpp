/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_with_color_ace.h"

#include "../../ai.h"
#include "../../../../game/game.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabWithColorAce::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
JabWithColorAce::JabWithColorAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_color_ace)
{ }

/** destructor
 **/
JabWithColorAce::~JabWithColorAce() = default;

/** @return  whether the conditions are met
 **/
bool
JabWithColorAce::conditions_met(Trick const& trick)
{
  if (!condition_color_trick(trick))
    return false;

  auto const color = trick.startcard().color();
  auto const ace = Card(color, Card::ace);
  return (   condition_has(ace)
          && condition_can_jab_with(trick, ace)
          && (   condition_first_color_run(trick)
              || !condition_winnerplayer_guessed_same_team(trick)
              || (   condition_winnerplayer_guessed_same_team(trick)
                  && condition_opposite_or_unknown_team_can_jab(trick)))
          && condition_enough_remaining_color_cards(trick)
         );
}

/** @return   card to play
 **/
Card
JabWithColorAce::card_to_play(Trick const& trick)
{
  auto const color = trick.startcard().color();
  auto const ace = Card(color, Card::ace);
  // in solo games: check whether a ten is sufficient
  if (prefer_ten(trick)) {
    auto const ten = Card(color, Card::ten);
    rationale_.add(_("Heuristic::return::jab with %s in favour of the ace", _(ten)));
    return ten;
  }

  rationale_.add(_("Heuristic::return::jab with %s", _(ace)));
  return ace;
}

/** @return   whether there are enough remaining color cards for the reamining players
 **/
bool
JabWithColorAce::condition_enough_remaining_color_cards(Trick const& trick)
{
  // the number of remaining cards for the other players
  auto const color = trick.startcard().color();
  auto const& game = trick.game();
  auto const& cards = game.cards();
  auto const& hand = this->hand();
  unsigned const remaining_cards
    = (cards.count(color)
       - (cards_information().played(color) + hand.count(color)));

  // there are enough cards for all players
  if (remaining_cards < trick.remainingcardno() - 1) {
    if (!condition_opposite_players_behind_must_serve(trick)) {
      rationale_.add(_("Heuristic::condition::not enough remaining cards of %s for all other players", _(color)));
      return false;
    }
  }
  rationale_.add(_("Heuristic::condition::enough remaining cards of %s for all other players", _(color)));
  return true;
}

/** @return  whether to prefer a ten to jab
 **/
bool
JabWithColorAce::prefer_ten(Trick const& trick)
{
  auto const& game = trick.game();
  if (!(   game.is_solo()
        && team() == Team::contra))
    return false;

  auto const color = trick.startcard().color();
  auto const ten = Card(color, Card::ten);
  auto const& hand = this->hand();
  if (hand.count(color, Card::ace) == 2)
    return false;
  if (hand.count(color, Card::ten) == 2)
    return false;
  if (!hand.contains(ten))
    return false;
  if (!condition_can_jab_with(trick, ten))
    return false;

  auto const& soloplayer = game.players().soloplayer();
  auto const& soloplayer_hand = guessed_hand(soloplayer);
  auto const ace = Card(trick.startcard().color(), Card::ace);
  if (soloplayer_hand.count(color) <= 1)
    return false;

  if (trick.has_played(soloplayer)) {
    if (soloplayer_hand.contains(ace)) {
      rationale_.add(_("Heuristic::info::solo game, contra, ten is sufficient, the soloplayer still has the ace"));
      return true;
    }
    if (soloplayer_hand.contains(ten)) {
      rationale_.add(_("Heuristic::info::solo game, contra, ten is sufficient, the soloplayer still has the ten"));
      return true;
    }
  } else {
    if (   !soloplayer_hand.contains(ace)
        && soloplayer_hand.contains(ten)) {
      rationale_.add(_("Heuristic::info::solo game, contra, ten is sufficient, the soloplayer still has the ten"));
      return true;
    }
  }
  return false;
}

} // namespace Heuristics
