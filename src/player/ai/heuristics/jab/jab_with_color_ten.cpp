/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_with_color_ten.h"

#include "../../ai.h"
#include "../../../../game/game.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabWithColorTen::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
JabWithColorTen::JabWithColorTen(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_color_ten)
{ }

/** destructor
 **/
JabWithColorTen::~JabWithColorTen() = default;

/** @return  whether the conditions are met
 **/
bool
JabWithColorTen::conditions_met(Trick const& trick)
{
  if (!condition_color_trick(trick))
    return false;

  auto const color = trick.startcard().color();
  auto const ten = Card(color, Card::ten);
  return (   condition_has(ten)
          && !condition_is_trump(ten)
          && condition_can_jab_with(trick, ten)
          && condition_opposite_players_behind_have_color(trick, color)
          && !condition_opposite_players_behind_have(trick, Card(color, Card::ace))
          && condition_enough_remaining_color_cards(trick, color)
         );
}

/** @return   card to play
 **/
Card
JabWithColorTen::card_to_play(Trick const& trick)
{
  auto const color = trick.startcard().color();
  auto const ten = Card(color, Card::ten);
  rationale_.add(_("Heuristic::return::jab with color ten %s", _(ten)));
  return ten;
}

} // namespace Heuristics
