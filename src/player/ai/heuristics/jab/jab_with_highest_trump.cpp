/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_with_highest_trump.h"

#include "../../../player.h"
#include "../../../../card/hand.h"
#include "../../../../game/game.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabWithHighestTrump::is_valid(GameTypeGroup const game_type,
                                   PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

/** constructor
 **/
JabWithHighestTrump::JabWithHighestTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_highest_trump)
{ }

/** destructor
 **/
JabWithHighestTrump::~JabWithHighestTrump() = default;

/** @return  whether the conditions are met
 **/
bool
JabWithHighestTrump::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && !condition_marriage_decision_for_me(trick)
          && condition_can_jab_with_trump(trick)
          && !condition_winnerplayer_same_team(trick)
          && condition_opposite_or_unknown_player_behind(trick)
          && condition_many_points(trick)
          && condition_have_highest_trump(trick)
         );
}

/** @return   card to play
 **/
Card
JabWithHighestTrump::card_to_play(Trick const& trick)
{
  auto const card = jabbing_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  rationale_.add(_("Heuristic::return::jab with highest trump: %s", _(card)));
  return card;
}

/** @return   whether the partner has already jabbed high
 **/
bool
JabWithHighestTrump::condition_many_points(Trick const& trick)
{
  unsigned const min_points = (game().type() == GameType::poverty
                               ? 14
                               : 10);
  if (trick.points() >= min_points) {
    rationale_.add(_("Heuristic::condition::the trick has at least %u points", min_points));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the trick has less then %u points", min_points));
  return false;
}

/** @return   whether the player has the highest trump
 **/
bool
JabWithHighestTrump::condition_have_highest_trump(Trick const& trick)
{
  auto const& trump = hand().highest_trump();
  if (!trump) {
    rationale_.add(_("Heuristic::condition::have no trump"));
    return false;
  }
  Trick trick2 = trick;
  trick2.add(trump);
  for (auto const& player : trick2.remaining_players()) {
    if (guessed_hand(player).can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::%s can jab the highest trump", player.get().name()));
      return false;
    }
  }
  rationale_.add(_("Heuristic::condition::have the highest trump"));
  return true;
}

/** @return   the card to jab with
 **/
Card
JabWithHighestTrump::jabbing_card(Trick const& trick)
{
  auto const card = hand().highest_trump();
  return optimized_jabbing_card(trick, card);
}

} // namespace Heuristics
