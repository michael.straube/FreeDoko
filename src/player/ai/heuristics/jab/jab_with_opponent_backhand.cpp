/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_with_opponent_backhand.h"

#include "../../../player.h"
#include "../../../../card/hand.h"
#include "../../../../game/game.h"

namespace Heuristics {

auto JabWithOpponentBackhand::is_valid(GameTypeGroup const game_type,
                                   PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_trump_color(game_type)
          && !(is_solo(game_type) && player_group == PlayerTypeGroup::contra));
}


JabWithOpponentBackhand::JabWithOpponentBackhand(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_opponent_backhand)
{ }


JabWithOpponentBackhand::~JabWithOpponentBackhand() = default;


auto JabWithOpponentBackhand::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && condition_winnerplayer_guessed_opposite_team(trick)
          && !condition_last_player_guessed_same_team(trick)
          && condition_backhand_can_pfund(trick)
          && condition_can_jab(trick)
         );
}


auto JabWithOpponentBackhand::card_to_play(Trick const& trick) -> Card
{
  auto const card = jabbing_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  if (card.points() >= 10 && guessed_hand(trick.lastplayer()).can_jab(trick + card)) {
    rationale_.add(_("Heuristic::reject::do not jab with card %s", _(card)));
    return {};
  }
  rationale_.add(_("Heuristic::return::jab with next higher trump: %s", _(card)));
  return card;
}


auto JabWithOpponentBackhand::jabbing_card(Trick const& trick) -> HandCard
{
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (!trick.isvalid(card))
    return {};
  return card;
}

} // namespace Heuristics
