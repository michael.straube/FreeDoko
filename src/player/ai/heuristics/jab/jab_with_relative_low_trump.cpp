/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_with_relative_low_trump.h"

#include "../../../player.h"
#include "../../../../card/hand.h"
#include "../../../../game/game.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabWithRelativeLowTrump::is_valid(GameTypeGroup const game_type,
                                       PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}

/** constructor
 **/
JabWithRelativeLowTrump::JabWithRelativeLowTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_relative_low_trump)
{ }

/** destructor
 **/
JabWithRelativeLowTrump::~JabWithRelativeLowTrump() = default;

/** @return  whether the conditions are met
 **/
bool
JabWithRelativeLowTrump::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_marriage_decision_for_me(trick)
          && condition_can_jab_with_trump(trick)
          && !condition_winnerplayer_same_team(trick)
          && condition_jabbing_card_is_relative_low(trick)
         );
}

/** @return   card to play
 **/
Card
JabWithRelativeLowTrump::card_to_play(Trick const& trick)
{
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  if (   game().type() == GameType::normal
      && team() == Team::re
      && !player().announcement()
      && card.value() == Card::queen
      && hand().contains(Card::club_queen)
      && condition_guessed_partner_behind(trick)) {
    auto const card = Card::club_queen;
    rationale_.add(_("Heuristic::return::jab with club queen to show the team"));
    return card;
  }
  rationale_.add(_("Heuristic::return::jab with next higher trump: %s", _(card)));
  return card;
}

/** @return   whether the jabbing card is relative low in the hand
 **/
bool
JabWithRelativeLowTrump::condition_jabbing_card_is_relative_low(Trick const& trick)
{
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (card.is_special()) {
    rationale_.add(_("Heuristic::condition::the jabbing card %s is special", _(card)));
    return false;
  }
  if (card.points() >= 10) {
    rationale_.add(_("Heuristic::condition::the jabbing card %s has too many points", _(card)));
    return false;
  }
  auto const num_lt_trumps = hand().count_lt_trump(card);
  if (num_lt_trumps == 0) {
    rationale_.add(_("Heuristic::condition::the jabbing card %s is the smallest trump", _(card)));
    return true;
  }
  if (hand().count(Card::trump) < 4) {
    rationale_.add(_("Heuristic::condition::too few trumps"));
    return false;
  }
  if (num_lt_trumps == 1) {
    rationale_.add(_("Heuristic::condition::only one trump below the jabbing card %s", _(card)));
    return true;
  }
  if (hand().count(Card::trump) < 6) {
    rationale_.add(_("Heuristic::condition::relative position of jabbing card %s is too great", _(card)));
    return false;
  }
  if (num_lt_trumps == 2) {
    rationale_.add(_("Heuristic::condition::only two trumps below the jabbing card %s", _(card)));
    return true;
  }
  rationale_.add(_("Heuristic::condition::relative position of jabbing card %s is too great", _(card)));
  return false;
}

} // namespace Heuristics
