/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_for_points.h"

#include "../../../../game/game.h"
#include "../../../../card/hand.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool LastPlayerJabForPoints::is_valid(GameTypeGroup const game_type,
                                      PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

/** constructor
 **/
LastPlayerJabForPoints::LastPlayerJabForPoints(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::last_player_jab_for_points)
{ }

/** destructor
 **/
LastPlayerJabForPoints::~LastPlayerJabForPoints() = default;

/** @return  whether the conditions are met
 **/
bool
LastPlayerJabForPoints::conditions_met(Trick const& trick)
{
  return (   condition_last_card(trick)
          && !condition_winnerplayer_guessed_same_team(trick)
          && condition_can_jab_with_trump(trick)
         );
}

/** @return   card to play
 **/
Card
LastPlayerJabForPoints::card_to_play(Trick const& trick)
{
  auto const& game = trick.game();

  auto const card = jabbing_card_for_last_player(trick);
  if (::is_picture_solo(game.type())) {
    if (trick.points() < 10) {
      rationale_.add(_("Heuristic::condition::not enough points in the trick"));
      return {};
    }
    rationale_.add(_("Heuristic::condition::enough points in the trick"));
    return card;
  }

  if (below_trump_card_limit(card)) {
    rationale_.add(_("Heuristic::return::jab below trump card limit: %s", _(card)));
    return card;
  }


  if (trick.contains_fox()) {
    rationale_.add(_("Heuristic::condition::the trick contains a fox"));
    rationale_.add(_("Heuristic::return::jab the trick: %s", _(card)));
    return card;
  }
  if (game.rule(RuleType::dullen) && trick.contains(game.cards().dulle())) {
    rationale_.add(_("Heuristic::condition::the trick contains a dulle"));
    rationale_.add(_("Heuristic::return::jab the trick: %s", _(card)));
    return card;
  }
  if (game.rule(RuleType::swines) && game.swines().swines_announced() && trick.contains(game.cards().swine())) {
    rationale_.add(_("Heuristic::condition::the trick contains a swine"));
    rationale_.add(_("Heuristic::return::jab the trick: %s", _(card)));
    return card;
  }

  auto const trick_points_modifier = (  (aggressive() ? -2 : 0)
                                      + (cautious()   ?  2 : 0)
                                      + (game.type() == GameType::poverty && !game.players().is_soloplayer(player())
                                         ? 10 : 0));
  if (trick.points() < static_cast<Trick::Points>(3 + hand().count_trumps()
                                                  + (HandCard(hand(), card).is_special() ? 4 : 0)
                                                  + trick_points_modifier)) {
    rationale_.add(_("Heuristic::condition::not enough points in the trick"));
    return {};
  }
  rationale_.add(_("Heuristic::condition::enough points in the trick"));
  rationale_.add(_("Heuristic::return::jab the trick: %s", _(card)));
  return card;
}

} // namespace Heuristics
