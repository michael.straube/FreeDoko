/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_with_trump_ace_or_ten.h"

#include "../../../../game/game.h"
#include "../../../../card/hand.h"

namespace Heuristics {

bool LastPlayerJabWithTrumpAceOrTen::is_valid(GameTypeGroup const game_type,
                                              PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}


LastPlayerJabWithTrumpAceOrTen::LastPlayerJabWithTrumpAceOrTen(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::last_player_jab_with_trump_ace_or_ten)
{ }


LastPlayerJabWithTrumpAceOrTen::~LastPlayerJabWithTrumpAceOrTen() = default;


bool
LastPlayerJabWithTrumpAceOrTen::conditions_met(Trick const& trick)
{
  auto const trumpcolor = trick.game().cards().trumpcolor();
  auto const ace = Card(trumpcolor, Card::ace);
  auto const ten = Card(trumpcolor, Card::ten);
  return (   condition_last_card(trick)
          && (   (   hand().has_swines()
                  ? false
                  : condition_can_jab_with(trick, ace))
              || condition_can_jab_with(trick, ten))
         );
}


Card
LastPlayerJabWithTrumpAceOrTen::card_to_play(Trick const& trick)
{
  auto const trumpcolor = trick.game().cards().trumpcolor();

  if (!hand().has_swines()) {
    auto const ace = Card(trumpcolor, Card::ace);
    if (hand().contains(ace)) {
      rationale_.add(_("Heuristic::condition::jab trick with %s", _(ace)));
      return ace;
    }
  }
  auto const ten = Card(trumpcolor, Card::ten);
  if (hand().contains(ten)) {
    rationale_.add(_("Heuristic::condition::jab trick with %s", _(ten)));
    return ten;
  }
  return {};
}

} // namespace Heuristics
