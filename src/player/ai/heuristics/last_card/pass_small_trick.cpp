/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pass_small_trick.h"

#include "../../ai.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool LastPlayerPassSmallTrick::is_valid(GameTypeGroup const game_type,
                                        PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
LastPlayerPassSmallTrick::LastPlayerPassSmallTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::last_player_pass_small_trick)
{ }

/** destructor
 **/
LastPlayerPassSmallTrick::~LastPlayerPassSmallTrick() = default;

/** @return  whether the conditions are met
 **/
bool
LastPlayerPassSmallTrick::conditions_met(Trick const& trick)
{
  return (   condition_last_card(trick)
          && condition_color_trick(trick)
          && !condition_must_serve(trick)
          && condition_few_points_in_trick(trick)
         );
}

/** @return   card to play
 **/
Card
LastPlayerPassSmallTrick::card_to_play(Trick const& trick)
{
  auto const card = best_fehl_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no low color card found"));
    return {};
  }

  rationale_.add(_("Heuristic::return::best low color card: %s", _(card)));
  return card;
}

/** @return  whether the guessed partner has worries
 **/
bool
LastPlayerPassSmallTrick::condition_few_points_in_trick(Trick const& trick)
{
  if (static_cast<int>(trick.points()) > ai().value(Aiconfig::Type::limit_throw_fehl) ) {
    rationale_.add(_("Heuristic::condition::many points in the trick"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::few trick points"));
  return true;
}

/** @return   a good color card to throw in order to create a fehl color
 **/
Card
LastPlayerPassSmallTrick::best_fehl_card(Trick const& trick)
{
  auto const& cards_information = this->cards_information();
  auto const& hand = this->hand();

  auto p = max_if(hand, [&hand](auto const card) {
                  return (   !card.istrump()
                          && hand.count(card.color()) <= 1
                          && (   card.value() == Card::king
                              || card.value() == Card::nine )); },
                  [&cards_information](Card const lhs, Card const rhs) {
                  return (cards_information.remaining_others(lhs.color())
                          < cards_information.remaining_others(rhs.color()));
                  });
  if (p)
    return *p;
  return {};
}

} // namespace Heuristics
