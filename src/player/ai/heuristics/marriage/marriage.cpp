/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "marriage.h"

#include "../../../../party/rule.h"
#include "../../../../game/game.h"

namespace Heuristics {

HeuristicMarriage::HeuristicMarriage(Ai const& ai, Aiconfig::Heuristic const heuristic) :
  Heuristic(ai, heuristic)
{ }


HeuristicMarriage::~HeuristicMarriage() = default;


auto HeuristicMarriage::condition_unresolved_marriage() -> bool
{
  if (   game().type() == GameType::marriage_solo
      || game().type() == GameType::marriage_silent) {
    rationale_.add(_("Heuristic::condition::game type %s", _(game().type())));
    return false;
  } else if (game().type() == GameType::marriage) {
    rationale_.add(_("Heuristic::condition::undetermined marriage"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::no marriage"));
    return false;
  }
}


auto HeuristicMarriage::highest_trump_in_game() -> Card
{
  auto const& game = this->game();
  auto const& rule = this->rule();
  auto const& cards = game.cards();
  auto const& hand = this->hand();
  Card card;

  if (rule(Rule::Type::hyperswines)) {
    if (   (   game.swines().hyperswines_announced()
            || game.swines().hyperswines_announcement_valid(player()))
        && hand.contains(cards.hyperswine())) {
      rationale_.add(_("Heuristic::info::have hyperswines"));
      card = cards.hyperswine();
    } else if (game.swines().hyperswines_announced()) {
      rationale_.add(_("Heuristic::info::other player has hyperswines"));
      return card;
    }
  }

  if (rule(Rule::Type::swines)) {
    if (   (   game.swines().swines_announced()
            || game.swines().swines_announcement_valid(player()))
        && hand.contains(cards.swine())) {
      rationale_.add(_("Heuristic::info::have swines"));
      card = cards.swine();
    } else if (game.swines().swines_announced()) {
      rationale_.add(_("Heuristic::info::other player has swines"));
      return card;
    }
  }

  if (rule(Rule::Type::dullen)) {
    if (!hand.contains(Card::dulle)) {
      rationale_.add(_("Heuristic::info::do not have dulle"));
      return card;
    } else if (hand.count(Card::dulle) < rule(Rule::Type::number_of_same_cards)) {
      if (rule(Rule::Type::dullen_second_over_first)) {
        rationale_.add(_("Heuristic::info::rule: %s", _(Rule::Type::dullen_second_over_first)));
        return card;
      } else {
        rationale_.add(_("Heuristic::info::have dulle"));
        return Card::dulle;
      }
    }
    rationale_.add(_("Heuristic::info::have both dulle"));
    card = Card::dulle;
  }

  {
    if (!hand.contains(Card::spade_queen)) {
      if (!card)
        rationale_.add(_("Heuristic::info::do not have highest trump %s", _(card)));
      return card;
    }
    card = Card::spade_queen;
    if (hand.count(card) < rule(Rule::Type::number_of_same_cards)) {
      rationale_.add(_("Heuristic::info::have highest trump %s", _(card)));
      return card;
    }

    if (!hand.contains(Card::heart_queen)) {
      rationale_.add(_("Heuristic::info::have highest trump %s", _(card)));
      return card;
    }
    card = Card::heart_queen;
    if (hand.count(card) < rule(Rule::Type::number_of_same_cards)) {
      rationale_.add(_("Heuristic::info::have highest trump %s", _(card)));
      return card;
    }

    if (!hand.contains(Card::diamond_queen)) {
      rationale_.add(_("Heuristic::info::have highest trump %s", _(card)));
      return card;
    }
    card = Card::diamond_queen;
    if (hand.count(card) < rule(Rule::Type::number_of_same_cards)) {
      rationale_.add(_("Heuristic::info::have highest trump %s", _(card)));
      return card;
    }
    rationale_.add(_("Heuristic::info::have highest trump %s", _(card)));
    return card;
  }
}

} // namespace Heuristics
