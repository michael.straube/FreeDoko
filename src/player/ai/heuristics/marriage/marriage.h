/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../heuristic.h"

namespace Heuristics {

/** Base class for marriage heuristics
 **/
class HeuristicMarriage : public Heuristic {
public:
  HeuristicMarriage(Ai const& ai, Aiconfig::Heuristic heuristic);
  HeuristicMarriage(HeuristicMarriage const&) = delete;
  auto operator=(HeuristicMarriage const&) -> HeuristicMarriage& = delete;

  ~HeuristicMarriage() override;

protected: // Hilfsfunktionen
  auto condition_unresolved_marriage()                   -> bool;
  auto condition_last_trick_for_marriage_determination() -> bool;

  auto highest_trump_in_game() -> Card;
};

} // namespace Heuristics
