/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_to_marry.h"

#include "../../../../party/rule.h"
#include "../../../../game/game.h"

namespace Heuristics {


auto StartToMarry::is_valid(GameTypeGroup const game_type,
                            PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::marriage_undetermined
          && player_group == PlayerTypeGroup::contra);
}


StartToMarry::StartToMarry(Ai const& ai) :
  HeuristicMarriage(ai, Aiconfig::Heuristic::start_to_marry)
{ }


StartToMarry::~StartToMarry() = default;


auto StartToMarry::conditions_met(Trick const& trick) -> bool
{
  return (   condition_unresolved_marriage()
          && !condition(Team::re)
          && condition_startcard(trick));
}


auto StartToMarry::card_to_play(Trick const& trick) -> Card
{
  switch (game().marriage().selector()) {
  case MarriageSelector::silent:
  case MarriageSelector::team_set:
    return Card::empty;

  case MarriageSelector::first_foreign: {
    auto const card = highest_trump_in_game();
    if (card) {
      rationale_.add(_("Heuristic::return::play highest trump in game: %s", _(card)));
      return card;
    }
    rationale_.add(_("Heuristic::reject::do not have the highest trump in game"));
  } {
    auto const card = best_color_ace();
    if (card) {
      rationale_.add(_("Heuristic::return::play color ace: %s", _(card)));
      return card;
    }
    rationale_.add(_("Heuristic::reject::have no color ace"));
    return card;
  }

  case MarriageSelector::first_trump: {
    auto const card = highest_trump_in_game();
    if (card) {
      rationale_.add(_("Heuristic::return::play highest trump in game: %s", _(card)));
      return card;
    }
    rationale_.add(_("Heuristic::reject::do not have the highest trump in game"));
    return {};
  }

  case MarriageSelector::first_club: {
    auto const card = get_ace(Card::club);
    if (!card) {
      return {};
    }
    rationale_.add(_("Heuristic::return::play %s", _(card)));
    return card;
  }
  case MarriageSelector::first_spade: {
    auto const card = get_ace(Card::spade);
    if (!card) {
      return {};
    }
    rationale_.add(_("Heuristic::return::play %s", _(card)));
    return card;
  }
  case MarriageSelector::first_heart: {
    auto const card = get_ace(Card::heart);
    if (!card) {
      return {};
    }
    rationale_.add(_("Heuristic::return::play %s", _(card)));
    return card;
  }

  case MarriageSelector::first_color: {
    auto const card = best_color_ace();
    if (card) {
      rationale_.add(_("Heuristic::return::play color ace: %s", _(card)));
      return card;
    }
    rationale_.add(_("Heuristic::reject::have no color ace"));
    return card;
  }
  } // switch (hi.game().marriage().selector())

  return {};
}


auto StartToMarry::best_color_ace() -> Card
{
  auto const& hand = this->hand();
  auto best_ace = Card::empty;
  unsigned max_remaining_cards = 2;
  for (auto const color : game().cards().colors()) {
    auto const ace = Card(color, Card::ace);
    if (!hand.contains(ace))
      continue;
    auto const remaining_others = cards_information().remaining_others(color);
    if (remaining_others > max_remaining_cards) {
      if (!best_ace) {
        rationale_.add(_("Heuristic::info::select %s", _(ace)));
      } else {
        rationale_.add(_("Heuristic::info::prefer %s (more remaining cards)", _(ace)));
      }
      best_ace = ace;
      max_remaining_cards = remaining_others;
    }
  }
  return {};
}


auto StartToMarry::get_ace(Card::Color const color) -> Card
{
  auto const ace = Card(color, Card::ace);
  auto const& hand = this->hand();

  if (!hand.contains(ace)) {
    rationale_.add(_("Heuristic::info::have no %s", _(ace)));
    return {};
  }
  if (cards_information().remaining_others(color) >= game().playerno() - 1) {
    rationale_.add(_("Heuristic::info::not enough remaining of %s", _(color)));
    return Card(color, Card::ace);
  }

  return ace;
}

} // namespace Heuristics
