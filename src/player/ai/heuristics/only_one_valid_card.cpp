/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "only_one_valid_card.h"

#include "../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool OnlyOneValidCard::is_valid(GameTypeGroup const game_type,
                                PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
OnlyOneValidCard::OnlyOneValidCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::only_one_valid_card)
{ }

/** destructor
 **/
OnlyOneValidCard::~OnlyOneValidCard() = default;

/** @return  whether the conditions are met
 **/
bool
OnlyOneValidCard::conditions_met(Trick const& trick)
{
  return condition_only_one_card_valid(trick);
}

/** @return   card to play
 **/
Card
OnlyOneValidCard::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  for (auto const& card : hand) {
    if (trick.isvalid(card, hand)) {
      rationale_.add(_("Heuristic::return::only valid card: %s", _(card)));
      return card;
    }
  }
  DEBUG_ASSERTION(false,
                  "Heuristic::OnlyOneValidCard::get_card(trick)\n"
                  "  no valid card found.\n"
                  "Hand:\n"
                  << hand
                  << "Trick:\n"
                  << trick);
  return {};
}

/** @return  whether only one card is valid
 **/
bool
OnlyOneValidCard::condition_only_one_card_valid(Trick const& trick)
{
  auto const& hand = this->hand();
  Card valid_card;
  for (auto const& card : hand) {
    if (trick.isvalid(card, hand)) {
      if (!valid_card) {
        valid_card = card;
      } else if (valid_card != card) {
        rationale_.add(_("Heuristic::condition::more then one card is valid"));
        return false;
      }
    }
  }
  DEBUG_ASSERTION(valid_card,
                  "Heuristic::OnlyOneValidCard::condition_only_one_card_valid(trick)\n"
                  "  no card is valid.\n"
                  "Hand:\n"
                  << hand
                  << "Trick:\n"
                  << trick);
  rationale_.add(_("Heuristic::condition::only one card is valid"));
  return true;
}

} // namespace Heuristics
