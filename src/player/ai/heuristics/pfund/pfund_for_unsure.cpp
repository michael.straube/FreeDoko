/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pfund_for_unsure.h"

#include "../../ai.h"

#include "../../../../card/trick.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PfundForUnsure::is_valid(GameTypeGroup const game_type,
                              PlayerTypeGroup const player_group)
{
  return is_with_partner(game_type, player_group);
}

/** constructor
 **/
PfundForUnsure::PfundForUnsure(Ai const& ai) :
  Pfund(ai, Aiconfig::Heuristic::pfund_for_unsure)
{ }

/** destructor
 **/
PfundForUnsure::~PfundForUnsure() = default;

/** @return  whether the conditions are met
 **/
bool
PfundForUnsure::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_winnerplayer_guessed_same_team(trick)
          && (    !condition_opposite_or_unknown_team_can_jab(trick)
              ||  condition_high_winnercard(trick))
          && condition_have_many_points());
}

/** @return   card to play
 **/
Card
PfundForUnsure::card_to_play(Trick const& trick)
{
  if (!condition_partner_has_swine_or_hyperswine()) {
    auto const pfund = high_pfund(trick);
    if (pfund) {
      if (pfund.isfox()) {
        auto const ten = Card(pfund.color(), Card::ten);
        if (   hand().contains(ten)
            && !HandCard(hand(), ten).is_special()) {
          rationale_.add(_("Heuristic::return::high pfund: %s", _(ten)));
          return ten;
        }
      } else {
        rationale_.add(_("Heuristic::return::high pfund: %s", _(pfund)));
        return pfund;
      }
    }
  }
  {
    auto const pfund = small_pfund(trick);
    if (!pfund) {
      rationale_.add(_("Heuristic::reject::no pfund found"));
      return {};
    }
    if (pfund.points() == 0) {
      rationale_.add(_("Heuristic::reject::no pfund found"));
      return {};
    }
    rationale_.add(_("Heuristic::return::small pfund: %s", _(pfund)));
    return pfund;
  }
}

/** @return   whether the player has many points
 **/
bool
PfundForUnsure::condition_have_many_points()
{
  auto const& hand = this->hand();
  if (hand.count(Card::ace) + hand.count(Card::ten) >= hand.cardsnumber() / 2 + 1) {
    rationale_.add(_("Heuristic::condition::have many points, so try a pfund"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::have not many points"));
    return false;
  }
}

} // namespace Heauristics
