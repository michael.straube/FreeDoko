/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_trump_pfund.h"

#include "../../ai.h"

#include "../../../../card/trick.h"
#include "../../../../party/rule.h"

namespace Heuristics {

auto StartWithTrumpPfund::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_trump_color(game_type)
          && is_with_partner(game_type, player_group));
}


StartWithTrumpPfund::StartWithTrumpPfund(Ai const& ai) :
  Pfund(ai, Aiconfig::Heuristic::start_with_trump_pfund)
{ }


StartWithTrumpPfund::~StartWithTrumpPfund() = default;


auto StartWithTrumpPfund::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_guessed_partner_behind_can_win_trick(trick));
}


auto StartWithTrumpPfund::card_to_play(Trick const& trick) -> Card
{
  {
    auto const pfund = high_trump_pfund();
    if (pfund) {
      rationale_.add(_("Heuristic::return::high pfund: %s", _(pfund)));
      return pfund;
    }
  }
  {
    auto const pfund = small_trump_pfund();
    if (pfund.value() >= 4) {
      rationale_.add(_("Heuristic::return::small pfund: %s", _(pfund)));
      return pfund;
    }
  }
  rationale_.add(_("Heuristic::return::no pfund found"));
  return {};
}


auto StartWithTrumpPfund::condition_guessed_partner_behind_can_win_trick(Trick const& trick) -> bool
{
  if (   game().swines().hyperswines_announced()
      && cards_information().remaining_others(game().cards().hyperswine())) {
    rationale_.add(_("Heuristic::condition::player %s with hyperswines is behind", game().swines().hyperswines_owner().name()));
    if (same_team(game().swines().hyperswines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with hyperswines is a partner", game().swines().hyperswines_owner().name()));
      return true;
    } else if (guessed_same_team(game().swines().hyperswines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with hyperswines is guessed to be a partner", game().swines().hyperswines_owner().name()));
      return true;
    } else {
      return false;
    }
  }
  if (   game().swines().swines_announced()
      && cards_information().remaining_others(game().cards().swine())) {
    rationale_.add(_("Heuristic::condition::player %s with swines is behind", game().swines().swines_owner().name()));
    if (same_team(game().swines().swines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with swines is a partner", game().swines().swines_owner().name()));
      return true;
    } else if (guessed_same_team(game().swines().swines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with swines is guessed to be a partner", game().swines().swines_owner().name()));
      return true;
    } else {
      return false;
    }
  }
  if (   (game().type() == GameType::normal || game().type() == GameType::marriage)
      && team() == Team::re
      && cards_information().remaining_others(Card::club_queen)
      && !(   rule()(Rule::Type::dullen)
           && cards_information().remaining_others(Card::dulle))) {
    if (   (trick.actcardno() >= 1 ? guessed_team(trick.player_of_card(0)) == Team::contra : true)
        && (trick.actcardno() >= 2 ? guessed_team(trick.player_of_card(1)) == Team::contra : true)) {
      rationale_.add(_("Heuristic::condition::partner can jab with the club queen"));
      return true;
    }
  }

  return false;
}

} // namespace Heuristics
