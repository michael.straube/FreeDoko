/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "play_for_partner_worries.h"

#include "../ai.h"
#include "../../cards_information.of_player.h"
#include "../../team_information.h"

namespace Heuristics {

auto PlayForPartnerWorries::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_trump_color(game_type)
          && is_with_partner(game_type, player_group));
}


PlayForPartnerWorries::PlayForPartnerWorries(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_for_partner_worries)
{ }


PlayForPartnerWorries::~PlayForPartnerWorries() = default;


auto PlayForPartnerWorries::conditions_met(Trick const& trick) -> bool
{
  return (   condition_color_trick(trick)
          && condition_partner_guessed()
          && condition_guessed_partner_behind(trick)
          && condition_can_jab(trick)
          && condition_guessed_partner_has_worries(trick)
          && condition_guessed_partner_has_mostly_trump()
         );
}


auto PlayForPartnerWorries::card_to_play(Trick const& trick) -> Card
{
  auto const card = best_winning_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no good winning card found"));
    return {};
  }

  rationale_.add(_("Heuristic::return::best winning card: %s", _(card)));
  return card;
}


auto PlayForPartnerWorries::condition_guessed_partner_has_worries(Trick const& trick) -> bool
{
  auto const partner = team_information().guessed_partner();
  if (!partner) {
    rationale_.add(_("Heuristic::condition::no partner guessed"));
    return false;
  }
  auto const validcards = partner->hand().validcards(trick);
  // the player has worries
  auto const is_rich_card = [](auto const& card) {
    return (   card.value() >= 10
            && !card.isdulle()
            && !card.is_special());
  };
  if (!any_of(validcards, is_rich_card)) {
    rationale_.add(_("Heuristic::condition::the partner has no worries"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::the partner can have worries"));
  return true;
}


auto PlayForPartnerWorries::condition_guessed_partner_has_mostly_trump() -> bool
{
  auto const partner = team_information().guessed_partner();
  auto const& cards_information = this->cards_information();
  auto const& hand_of_partner = guessed_hand(*partner);
  if (   (hand_of_partner.count(Card::trump) + 1
          >= hand_of_partner.cardsnumber())
      || (cards_information.of_player(*partner).must_have(Card::trump) + 1
          >= partner->cards_to_play()) ) {
    rationale_.add(_("Heuristic::condition::the partner has mostly trump"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the partner still has some color cards"));
  return false;
}


auto PlayForPartnerWorries::best_winning_card(Trick const& trick) -> Card
{
  Card winning_card;

  HandCards const validcards = hand().validcards(trick);
  for (auto const& c : validcards) {
    if (   trick.isjabbed(c)
        && (   winning_card.is_empty()
            || c.is_jabbed_by(winning_card))) {
      winning_card = c;
    }
  }

  return winning_card;
}

} // namespace Heuristics
