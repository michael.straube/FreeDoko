/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_color_trick_with_trump.h"

#include "../../../../../card/hand.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyContraJabColorTrickWithTrump::is_valid(GameTypeGroup const game_type,
                                                   PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
PovertyContraJabColorTrickWithTrump::PovertyContraJabColorTrickWithTrump(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_contra_jab_color_trick_with_trump)
{ }

/** destructor
 **/
PovertyContraJabColorTrickWithTrump::~PovertyContraJabColorTrickWithTrump() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyContraJabColorTrickWithTrump::conditions_met(Trick const& trick)
{
  return (   condition_poverty()
          && condition(Team::contra)
          && condition_color_trick(trick)
          && condition_can_jab_with_trump(trick)
          && condition_poverty_partner_player_behind(trick)
          && condition_trick_points_ge(trick, 8)
          && !(   condition_winnerplayer_same_team(trick)
               && condition_winnercard_ge(trick, Card::diamond_jack))
         );
}

/** @return   card to play
 **/
Card
PovertyContraJabColorTrickWithTrump::card_to_play(Trick const& trick)
{
  auto const card = trump_to_jab(trick);

  if (!card) {
    rationale_.add(_("Heuristic::reject::cannot jab with a jack or queen below club queen"));
    return {};
  }

  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}

/** @return   high trump to jab the trick with
 **/
Card
PovertyContraJabColorTrickWithTrump::trump_to_jab(Trick const& trick)
{
  for (auto const card : {
       Card::heart_queen,
       Card::diamond_queen,
       Card::club_jack,
       Card::spade_jack,
       Card::spade_queen,
       Card::heart_jack,
       Card::diamond_jack,
       }) {
    if (   hand().contains(card)
        && trick.isjabbed({hand(), card})) {
      return card;
    }
  }

  return {};
}

} // namespace Heuristics
