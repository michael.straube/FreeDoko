/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pfund_high_for_unsure.h"

#include "../../../ai.h"

#include "../../../../../card/trick.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyContraPfundHighForUnsure::is_valid(GameTypeGroup const game_type,
                                               PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
PovertyContraPfundHighForUnsure::PovertyContraPfundHighForUnsure(Ai const& ai) :
  PovertyContraPfund(ai, Aiconfig::Heuristic::poverty_contra_pfund_high_for_unsure)
{ }

/** destructor
 **/
PovertyContraPfundHighForUnsure::~PovertyContraPfundHighForUnsure() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyContraPfundHighForUnsure::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_winnerplayer_guessed_same_team(trick)
          && !condition_opposite_or_unknown_team_can_jab(trick));
}

/** @return   card to play
 **/
Card
PovertyContraPfundHighForUnsure::card_to_play(Trick const& trick)
{
  auto const pfund = high_pfund(trick);
  if (pfund) {
    rationale_.add(_("Heuristic::return::high pfund: %s", _(pfund)));
    return pfund;
  }
  rationale_.add(_("Heuristic::return::no high pfund found"));
  return {};
}

} // namespace Heuristics
