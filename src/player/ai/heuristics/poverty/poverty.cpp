/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "poverty.h"

#include "../../../../game/game.h"

/** constructor
 **/
HeuristicPoverty::HeuristicPoverty(Ai const& ai, Aiconfig::Heuristic const heuristic) :
  Heuristic(ai, heuristic)
{ }

/** destructor
 **/
HeuristicPoverty::~HeuristicPoverty() = default;

/** @return   whether this is a poverty
 **/
bool
HeuristicPoverty::condition_poverty()
{
  if (game().type() != GameType::poverty) {
    rationale_.add(_("Heuristic::condition::no poverty"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::poverty"));
  return true;
}

/** @return   the poverty player
 **/
Player const&
HeuristicPoverty::poverty_player()
{
  return game().players().soloplayer();
}

/** @return   the poverty partner
 **/
Player const&
HeuristicPoverty::poverty_partner()
{
  return game().players().poverty_partner();
}

/** @return   the contra partner (the player must be contra, also)
 **/
Player const&
HeuristicPoverty::contra_partner()
{
  for (auto const& p : game().players()) {
    if (   p.team() == Team::contra
        && p != player())
      return p;
  }
  DEBUG_ASSERTION(false,
                  "HeuristicPoverty::contra_partner()\n"
                  "  not found");
  return player();
}

/** @return   whether the player is the poverty player
 **/
bool
HeuristicPoverty::condition_poverty_player()
{
  if (game().type() != GameType::poverty) {
    rationale_.add(_("Heuristic::condition::no poverty"));
    return false;
  }
  if (player().team() != Team::re) {
    rationale_.add(_("Heuristic::condition::Team is contra"));
    return false;
  }
  if (!game().players().is_soloplayer(player())) {
    rationale_.add(_("Heuristic::condition::poverty partner"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::poverty player"));
  return true;
}

/** @return   whether the player is the poverty partner
 **/
bool
HeuristicPoverty::condition_poverty_partner()
{
  if (game().type() != GameType::poverty) {
    rationale_.add(_("Heuristic::condition::no poverty"));
    return false;
  }
  if (player().team() != Team::re) {
    rationale_.add(_("Heuristic::condition::Team is contra"));
    return false;
  }
  if (game().players().is_soloplayer(player())) {
    rationale_.add(_("Heuristic::condition::poverty player"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::poverty partner"));
  return true;
}

/** @return   whether the poverty partner is behind
 **/
bool
HeuristicPoverty::condition_poverty_partner_player_behind(Trick const& trick)
{
  auto const& game = this->game();
  if (game.type() != GameType::poverty) {
    rationale_.add(_("Heuristic::condition::no poverty"));
    return false;
  }
  if (trick.has_played(game.players().poverty_partner())) {
    rationale_.add(_("Heuristic::condition::the poverty partner has still got to play"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::the poverty partner has not played"));
    return true;
  }
}
