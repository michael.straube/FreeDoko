/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../heuristic.h"

/** Base class for poverty heuristics
 **/
class HeuristicPoverty : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
HeuristicPoverty(Ai const& ai, Aiconfig::Heuristic heuristic);
HeuristicPoverty(HeuristicPoverty const&) = delete;
HeuristicPoverty& operator=(HeuristicPoverty const&) = delete;

~HeuristicPoverty() override;

protected: // Hilfsfunktionen
Player const& poverty_player();
Player const& poverty_partner();
Player const& contra_partner();

bool condition_poverty();
bool condition_poverty_player();
bool condition_poverty_partner();
bool condition_poverty_partner_player_behind(Trick const& trick);
};
