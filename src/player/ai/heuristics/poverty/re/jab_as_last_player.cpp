/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_as_last_player.h"

#include "../../../../../card/hand.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyReJabAsLastPlayer::is_valid(GameTypeGroup const game_type,
                                        PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::re);
}

/** constructor
 **/
PovertyReJabAsLastPlayer::PovertyReJabAsLastPlayer(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_re_jab_as_last_player)
{ }

/** destructor
 **/
PovertyReJabAsLastPlayer::~PovertyReJabAsLastPlayer() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyReJabAsLastPlayer::conditions_met(Trick const& trick)
{
  return (   condition_poverty_partner()
          && !condition_opposite_or_unknown_player_behind(trick)
          && condition_can_jab(trick)
         );
}

/** @return   card to play
 **/
Card
PovertyReJabAsLastPlayer::card_to_play(Trick const& trick)
{
  auto const card = card_to_jab(trick);

  if (!card) {
    rationale_.add(_("Heuristic::reject::cannot jab"));
    return {};
  }

  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}

/** @return   high trump to jab the trick with
 **/
Card
PovertyReJabAsLastPlayer::card_to_jab(Trick const& trick)
{
  for (auto const& card : hand().cards_at_max_fox()) {
    if (trick.isjabbed(card)) {
      return card;
    }
  }
  return hand().next_jabbing_card(trick.winnercard());
}

} // namespace Heuristics
