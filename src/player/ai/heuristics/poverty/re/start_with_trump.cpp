/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_trump.h"

#include "../../../../../game/game.h"
#include "../../../../../card/hand.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyReStartWithTrump::is_valid(GameTypeGroup const game_type,
                                       PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::re);
}

/** constructor
 **/
PovertyReStartWithTrump::PovertyReStartWithTrump(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_re_start_with_trump)
{ }

/** destructor
 **/
PovertyReStartWithTrump::~PovertyReStartWithTrump() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyReStartWithTrump::conditions_met(Trick const& trick)
{
  return (   condition_poverty_partner()
          && condition_startcard(trick)
         );
}

/** @return   card to play
 **/
Card
PovertyReStartWithTrump::card_to_play(Trick const& trick)
{
  auto const card = trump_to_start_with();

  if (!card) {
    rationale_.add(_("Heuristic::reject::no trump found to start with"));
    return {};
  }

  rationale_.add(_("Heuristic::return::start with %s", _(card)));
  return card;
}

/** @return   trump to start the trick with
 **/
Card
PovertyReStartWithTrump::trump_to_start_with()
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  // @heuristic::action   play a small jack
  bool const jacks_first
    = (   game.tricks().remaining_no() > game.tricks().max_size() / 2
       || hand.count(Card::jack) > hand.count(Card::queen));

  static auto const cards_to_play_jacks_first = {
    Card::club_jack,
    Card::spade_jack,
    Card::heart_jack,
    Card::diamond_jack,
    Card::diamond_queen,
    Card::heart_queen,
    Card::spade_queen,
    Card::club_queen,
  };
  static auto const cards_to_play_queens_first = {
    Card::diamond_queen,
    Card::heart_queen,
    Card::spade_queen,
    Card::club_queen,
    Card::club_jack,
    Card::spade_jack,
    Card::heart_jack,
    Card::diamond_jack,
  };

  for (auto const card : (jacks_first ? cards_to_play_jacks_first : cards_to_play_queens_first))
    if (hand.contains(card))
      return card;
  return {};
}

} // namespace Heuristics
