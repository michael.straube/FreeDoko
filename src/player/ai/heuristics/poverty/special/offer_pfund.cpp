/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "offer_pfund.h"

#include "../../../../../card/trick.h"
#include "../../../../../game/game.h"
#include "../../../../../game/players.h"
#include "../../../../../party/rule.h"

#include "../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertySpecialOfferPfund::is_valid(GameTypeGroup const game_type,
                                        PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::special);
}

/** constructor
 **/
PovertySpecialOfferPfund::PovertySpecialOfferPfund(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_special_offer_pfund)
{ }

/** destructor
 **/
PovertySpecialOfferPfund::~PovertySpecialOfferPfund() = default;

/** @return  whether the conditions are met
 **/
bool
PovertySpecialOfferPfund::conditions_met(Trick const& trick)
{
  return (   condition_poverty_player()
          && !condition_startcard(trick)
          && !(   condition_trump_trick(trick)
               && condition_must_serve(trick))
          && !condition_high_winnercard(trick)
          && condition_partner_behind(trick)
          && condition_many_tens_and_aces()
         );
}

/** @return   card to play
 **/
Card
PovertySpecialOfferPfund::card_to_play(Trick const& trick)
{
  {
    auto const card = serve_color(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::serve color: %s", _(card)));
      return card;
    }
  }
  {
    auto const card = single_ten(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::offer single ten: %s", _(card)));
      return card;
    }
  }
  {
    auto const card = single_ace(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::offer single ace: %s", _(card)));
      return card;
    }
  }
  {
    auto const card = double_ten(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::offer single ten: %s", _(card)));
      return card;
    }
  }
  auto const card = best_ten_or_ace(trick);
  rationale_.add(_("Heuristic::return::offer %s", _(card)));
  return card;
}

/** @return   whether the player has many tens and aces
 **/
bool
PovertySpecialOfferPfund::condition_many_tens_and_aces()
{
  auto const& hand = this->hand();
  // @heuristic::condition   the player has many high cards
  if (hand.count(Card::ten) + hand.count(Card::ace)
      < (hand.cardsnumber() + 1) / 2) {
    rationale_.add(_("Heuristic::condition::less then half of the cards are tens and aces"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::at least half of the cards are tens and aces"));
    return true;
  }
}

/** @return    ten/ace to serve the color
 **/
Card
PovertySpecialOfferPfund::serve_color(Trick const& trick)
{
  if (trick.startcard().istrump())
    return {};
  auto const color = trick.startcard().tcolor();
  auto const& hand = this->hand();

  if (hand.contains(color, Card::ten))
    return Card(color, Card::ten);
  if (hand.contains(color, Card::ace))
    return Card(color, Card::ace);
  return {};
}

/** @return    single ten of a color
 **/
Card
PovertySpecialOfferPfund::single_ten(Trick const& trick)
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  for (auto const color : game.cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (!hand.contains(ten))
      continue;
    if (ten.istrump(game))
      continue;
    if (hand.count(color) != 1)
      continue;
    return ten;
  }
  return {};
}

/** @return    single ace of a color
 **/
Card
PovertySpecialOfferPfund::single_ace(Trick const& trick)
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  for (auto const color : game.cards().colors()) {
    auto const ace = Card(color, Card::ace);
    if (!hand.contains(ace))
      continue;
    if (ace.istrump(game))
      continue;
    if (hand.count(color) != 1)
      continue;
    return ace;
  }
  return {};
}

/** @return    double ten of a color
 **/
Card
PovertySpecialOfferPfund::double_ten(Trick const& trick)
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  for (auto const color : game.cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (!hand.contains(ten))
      continue;
    if (ten.istrump(game))
      continue;
    if (   hand.count(color) == 2
        && hand.count(ten) == 2)
      continue;
    return ten;
  }
  return {};
}

/** @return    the best ten or ace to play
 **/
Card
PovertySpecialOfferPfund::best_ten_or_ace(Trick const& trick)
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  auto has_ten_or_ace = [&hand, &game](Card::Color const color) {
    return (   (   hand.contains(Card(color, Card::ten))
                && !Card(color, Card::ten).istrump(game))
            || (   hand.contains(Card(color, Card::ace))
                && !Card(color, Card::ace).istrump(game))
           );
  };
  auto const color = best_of_if(game.cards().colors(),
                                has_ten_or_ace,
                                [&hand](Card::Color const color) {
                                return hand.count(color);
                                });
  if (hand.contains(color, Card::ten))
    return Card(color, Card::ten);
  if (hand.contains(color, Card::ace))
    return Card(color, Card::ace);
  return {};
}

} // namespace Heuristics
