/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../poverty.h"

namespace Heuristics {

/** Play a high color card in a trick the partner can overjab
 **/
class PovertySpecialOfferPfund : public HeuristicPoverty {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit PovertySpecialOfferPfund(Ai const& ai);
PovertySpecialOfferPfund(PovertySpecialOfferPfund const&) = delete;
PovertySpecialOfferPfund& operator=(PovertySpecialOfferPfund const&) = delete;

~PovertySpecialOfferPfund() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;

private:
bool condition_many_tens_and_aces();
Card serve_color(Trick const& trick);
Card single_ten(Trick const& trick);
Card single_ace(Trick const& trick);
Card double_ten(Trick const& trick);
Card best_ten_or_ace(Trick const& trick);
};
} // namespace Heuristics
