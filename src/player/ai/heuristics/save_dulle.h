/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../heuristic.h"

namespace Heuristics {

/** Jab with a dulle to save it from the other dulle
 **/
class SaveDulle : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit SaveDulle(Ai const& ai);
SaveDulle(SaveDulle const&) = delete;
SaveDulle& operator=(SaveDulle const&) = delete;

~SaveDulle() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;

private:
bool condition_opposite_dulle();
bool condition_partner_swine_or_hyperswine();
bool condition_have_few_trumps();
};
} // namespace Heuristics
