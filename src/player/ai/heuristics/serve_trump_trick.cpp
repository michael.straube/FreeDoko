/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "serve_trump_trick.h"

#include "../ai.h"

namespace Heuristics {


auto ServeTrumpTrick::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump(game_type);
}


ServeTrumpTrick::ServeTrumpTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::serve_trump_trick)
{ }


ServeTrumpTrick::~ServeTrumpTrick() = default;


auto ServeTrumpTrick::conditions_met(Trick const& trick) -> bool
{
  return (   condition_trump_trick(trick)
          && (   !condition_can_jab(trick)
              || condition_jabbing_is_not_worth(trick))
          && (  is_solo(trick.game().type()) 
              ? !condition_soloplayer_behind(trick)
              : true)
         );
}


auto ServeTrumpTrick::card_to_play(Trick const& trick) -> Card
{
  if (hand().hastrump()) {
    auto const card = serve_with_trump(trick);
    return optimize_trump(trick, card);
  } else {
    return serve_with_color(trick);
  }
}


auto ServeTrumpTrick::condition_jabbing_is_not_worth(Trick const& trick) -> bool
{
  if (trick.points() >= min_points_for_jabbing()) {
    rationale_.add(_("Heuristic::condition::trick has many points"));
    return false;
  }
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (below_trump_card_limit(card)) {
    rationale_.add(_("Heuristic::condition::next jabbing card is below trump card limit"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::next jabbing card is at least trump card limit"));
  return true;
}


auto ServeTrumpTrick::min_points_for_jabbing() const -> Trick::Points
{
  if (game().type() == GameType::poverty)
    return 13;
  return 8;
}


auto ServeTrumpTrick::serve_with_trump(Trick const& trick) -> Card
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  auto const card = hand.lowest_trump();
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable trump to serve found"));
    return {};
  }
  if (trick.isjabbed(card)) {
    rationale_.add(_("Heuristic::reject::trick is jabbed by lowest trump"));
    return {};
  }
  if (card.points() < 10) {
    rationale_.add(_("Heuristic::return::play low trump %s", _(card)));
    return card;
  }

  for (auto c : {Card::diamond_jack, Card::heart_jack, Card::spade_jack}) {
    if (   hand.contains(c)
        && c.istrump(game)) {
      rationale_.add(_("Heuristic::return::play low trump %s", _(c)));
      return c;
    }
  }
  rationale_.add(_("Heuristic::reject::no suitable trump to serve found"));
  return {};
}


auto ServeTrumpTrick::serve_with_color(Trick const& trick) -> Card
{
  auto const& colors = game().cards().colors();
  auto const& hand = this->hand();
  for (auto const v : {Card::nine, Card::jack, Card::queen, Card::king}) {
    for (auto const c : colors) {
      auto const card = Card(c, v);
      if (   hand.contains(card)
          && hand.count(c) == 1) {
        rationale_.add(_("Heuristic::return::single %s", _(card)));
        return card;
      }
    }
  }
  for (auto const c : colors) {
    auto const card = Card(c, Card::nine);
    if (   hand.contains(card)
        && hand.count(c) == hand.count(card)) {
      rationale_.add(_("Heuristic::return::low color card: %s", _(card)));
      return card;
    }
  }
  for (auto const v : {Card::nine, Card::jack, Card::queen, Card::king}) {
    for (auto const c : colors) {
      auto const card = Card(c, v);
      if (   hand.contains(card)
          && hand.highest_card(c).points() <= 4) {
        rationale_.add(_("Heuristic::return::low color card: %s", _(card)));
        return card;
      }
    }
  }
  rationale_.add(_("Heuristic::reject::no suitable color card to serve found"));
  return {};
}


auto ServeTrumpTrick::optimize_trump(Trick const& trick, Card card) -> Card
{
  auto const& cards_information = this->cards_information();
  auto const& cards = game().cards();
  auto const& hand = this->hand();
  for (auto c = card; cards_information.remaining_others(c) <= 1; c = cards.next_higher_card(c)) {
    if (   hand.contains(c)
        && (   !HandCard(hand, c).is_special()
            || !cards_information.highest_remaining_trump_of_others().jabs(card))
        && trick.isjabbed(c, hand)) {
      rationale_.add(_("Heuristic::return::prefer jabbing with %s", _(c)));
      return c;
    }
    if (cards_information.remaining_others(c))
      break;
  }
  return card;
}


} // namespace Heuristics
