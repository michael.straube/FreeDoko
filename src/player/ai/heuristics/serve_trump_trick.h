/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../heuristic.h"

namespace Heuristics {

/** Serve a trump trick the player cannot get or it is not worth it
 **/
class ServeTrumpTrick : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit ServeTrumpTrick(Ai const& ai);
ServeTrumpTrick(ServeTrumpTrick const&) = delete;
ServeTrumpTrick& operator=(ServeTrumpTrick const&) = delete;

~ServeTrumpTrick() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;

private:
auto condition_jabbing_is_not_worth(Trick const& trick) -> bool;
auto min_points_for_jabbing() const -> Trick::Points;
auto serve_with_trump(Trick const& trick) -> Card;
auto serve_with_color(Trick const& trick) -> Card;
auto optimize_trump(Trick const& trick, Card card) -> Card;
};
} // namespace Heuristics
