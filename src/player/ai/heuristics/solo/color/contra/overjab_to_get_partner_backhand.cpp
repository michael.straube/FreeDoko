/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "overjab_to_get_partner_backhand.h"

#include "../../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool ColorOverjabToGetPartnerBackhand::is_valid(GameTypeGroup const game_type,
                                                PlayerTypeGroup const player_group)
{
  return (   is_color_solo(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
ColorOverjabToGetPartnerBackhand::ColorOverjabToGetPartnerBackhand(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::color_overjab_to_get_partner_backhand)
{ }

/** destructor
 **/
ColorOverjabToGetPartnerBackhand::~ColorOverjabToGetPartnerBackhand() = default;

/** @return  whether the conditions are met
 **/
bool
ColorOverjabToGetPartnerBackhand::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_soloplayer_will_be_backhand(trick)
          && condition_soloplayer_has_played(trick)
          && !condition_high_winnercard(trick)
          && condition_can_jab(trick));
}

/** @return   card to play
 **/
Card
ColorOverjabToGetPartnerBackhand::card_to_play(Trick const& trick)
{
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (!card.istrump()) {
    rationale_.add(_("Heuristic::reject::cannot jab with trump"));
    return {};
  }
  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}

/** @return   whether the soloplayer will be backhand
 **/
bool
ColorOverjabToGetPartnerBackhand::condition_soloplayer_will_be_backhand(Trick const& trick)
{
  auto const& winnerplayer = trick.winnerplayer();
  auto const& players = game().players();
  auto const& player_backhand = players.previous(winnerplayer);
  if (players.is_soloplayer(player_backhand)) {
    rationale_.add(_("Heuristic::condition::the soloplayer will be backhand"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::the soloplayer will not be backhand"));
    return false;
  }
}

} // namespace Heuristics
