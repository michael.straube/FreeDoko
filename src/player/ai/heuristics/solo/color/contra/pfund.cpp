/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pfund.h"

#include "../../../../ai.h"

namespace Heuristics {

/** constructor
 **/
ColorPfund::ColorPfund(Ai const& ai, Aiconfig::Heuristic heuristic) :
  Heuristic(ai, heuristic)
{ }

/** destructor
 **/
ColorPfund::~ColorPfund() = default;

/** @return   a high pfund
 **/
auto ColorPfund::high_pfund(Trick const& trick) -> HandCard
{
  auto const color = trick.startcard().tcolor();
  auto const card = (hand().contains(color)
                     ? (color == Card::trump
                        ? high_pfund_trump()
                        : pfund(color))
                     : arbitrary_pfund());
  if (card.value() < 10) {
    rationale_.add(_("Heuristic::reject::pfund card %s has less then 10 points", _(card)));
    return {};
  }
  return card;
}

/** @return   a pfund
 **/
auto ColorPfund::pfund(Trick const& trick) -> HandCard
{
  auto const color = trick.startcard().tcolor();
  auto const card = (hand().contains(color)
                     ? (color == Card::trump
                        ? pfund_trump()
                        : pfund(color))
                     : arbitrary_pfund());
  return card;
}

/** @return    pfund card of trump
 **/
auto ColorPfund::high_pfund_trump() -> HandCard
{
  auto const trumpcolor = game().cards().trumpcolor();
  auto const hand = this->hand();
  for (auto const card : {Card(trumpcolor, Card::ace),
       Card(trumpcolor, Card::ten),
       }) {
    if (!hand.contains(card))
      continue;
    if (HandCard(hand, card).is_special())
      continue;

    rationale_.add(_("Heuristic::return::trump pfund: %s", _(card)));
    return {hand, card};
  }
  return {};
}

/** @return    pfund card of trump
 **/
auto ColorPfund::pfund_trump() -> HandCard
{
  auto const trumpcolor = game().cards().trumpcolor();
  auto const hand = this->hand();
  for (auto const card : {Card(trumpcolor, Card::ace),
       Card(trumpcolor, Card::ten),
       Card(trumpcolor, Card::king),
       Card::diamond_jack,
       Card::heart_jack,
       Card(trumpcolor, Card::nine),
       Card::spade_jack,
       Card::club_jack,
       Card::diamond_queen,
       Card::heart_queen,
       }) {
    if (!hand.contains(card))
      continue;
    if (HandCard(hand, card).is_special())
      continue;

    rationale_.add(_("Heuristic::return::trump pfund: %s", _(card)));
    return {hand, card};
  }
  return {};
}

/** @return    highest card of color
 **/
auto ColorPfund::pfund(Card::Color const color) -> HandCard
{
  auto const card = hand().highest_card(color);
  rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(card)));
  return card;
}

/** @return    good high card of any color
 **/
auto ColorPfund::arbitrary_pfund() -> HandCard
{
  {
    auto const card = pfund_blank_ten();
    if (card) {
      return card;
    }
  }
  {
    auto const card = pfund_double_ten();
    if (card) {
      return card;
    }
  }
  {
    auto const card = pfund_single_ten();
    if (card) {
      return card;
    }
  }
  {
    auto const card = pfund_ten();
    if (card) {
      return card;
    }
  }
  {
    auto const card = pfund_ace();
    if (card)
      return card;
  }
  {
    auto const card = pfund_blank_king();
    if (card)
      return card;
  }
  auto card = HandCard(hand());
  for (auto const& c : hand()) {
    if (c.value() < 10 && (!card || c.value() >= card.value()))
      card = c;
  }
  if (card) {
    rationale_.add(_("Heuristic::return::pfund %s", _(card)));
    return card;
  }
  return {};
}

/** @return   whether this is the trumpcolor
 **/
bool
ColorPfund::is_trumpcolor(Card::Color const color)
{
  return color == game().cards().trumpcolor();
}

auto ColorPfund::pfund_blank_ten() -> HandCard
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    if (is_trumpcolor(color))
      continue;
    auto const ten = HandCard(hand, color, Card::ten);
    if (   !ten.istrump()
        && hand.count(color) == 1
        && hand.contains(ten)) {
      rationale_.add(_("Heuristic::return::blank ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no blank ten"));
  return {};
}

auto ColorPfund::pfund_blank_king() -> HandCard
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    if (is_trumpcolor(color))
      continue;
    auto const king = HandCard(hand, color, Card::king);
    if (   !king.istrump()
        && hand.count(color) == 1
        && hand.contains(king)) {
      rationale_.add(_("Heuristic::return::blank king: %s", _(king)));
      return king;
    }
  }
  rationale_.add(_("Heuristic::reject::have no blank king"));
  return {};
}

/** @return    a double ten
 **/
auto ColorPfund::pfund_double_ten() -> HandCard
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    if (is_trumpcolor(color))
      continue;
    auto const ten = HandCard(hand, color, Card::ten);
    if (   !ten.istrump()
        && hand.count(ten) == 2) {
      rationale_.add(_("Heuristic::return::double ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no double ten"));
  return {};
}

/** @return    a single ten
 **/
auto ColorPfund::pfund_single_ten() -> HandCard
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    if (is_trumpcolor(color))
      continue;
    auto const ten = HandCard(hand, color, Card::ten);
    if (   !ten.istrump()
        && hand.count(ten) == 1) {
      rationale_.add(_("Heuristic::return::single ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no single ten"));
  return {};
}

/** @return    a ten
 **/
auto ColorPfund::pfund_ten() -> HandCard
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    if (is_trumpcolor(color))
      continue;
    auto const ten = HandCard(hand, color, Card::ten);
    if (   !ten.istrump()
        && hand.count(ten) == 1) {
      rationale_.add(_("Heuristic::return::ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no ten"));
  return {};
}

/** @return    an ace
 **/
auto ColorPfund::pfund_ace() -> HandCard
{
  auto const& hand = this->hand();
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  vector<Card::Color> colors;
  for (auto const color : game().cards().colors()) {
    if (hand.contains(color, Card::ace))
      colors.push_back(color);
  }
  if (colors.empty()) {
    rationale_.add(_("Heuristic::reject::have no ace"));
    return {};
  }
  if (colors.size() == 1) {
    auto const ace = HandCard(hand, colors[0], Card::ace);
    rationale_.add(_("Heuristic::return::ace: %s", _(ace)));
    return ace;
  }
  for (auto const color : colors) {
    if (    hand.count(color) == 1
        && !hand_of_soloplayer.contains(color)) {
      auto const ace = HandCard(hand, color, Card::ace);
      rationale_.add(_("Heuristic::condition::the soloplayer does not have color %s", _(color)));
      rationale_.add(_("Heuristic::return::single ace: %s", _(ace)));
      return ace;
    }
  }
  for (auto const color : colors) {
    if (!hand_of_soloplayer.contains(color)) {
      auto const ace = HandCard(hand, color, Card::ace);
      rationale_.add(_("Heuristic::condition::the soloplayer does not have color %s", _(color)));
      rationale_.add(_("Heuristic::return::ace: %s", _(ace)));
      return ace;
    }
  }
  for (auto const color : colors) {
    if (    hand.count(color) == 1
        && !hand_of_soloplayer.contains(color)) {
      auto const ace = HandCard(hand, color, Card::ace);
      rationale_.add(_("Heuristic::return::single ace: %s", _(ace)));
      return ace;
    }
  }
  auto const ace = HandCard(hand, colors[0], Card::ace);
  rationale_.add(_("Heuristic::return::ace: %s", _(ace)));
  return ace;
}

} // namespace Heuristics
