/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../../../heuristic.h"

namespace Heuristics {

/** Base class for pfund heuristics
 **/
class ColorPfund : public Heuristic {
public:
ColorPfund(Ai const& ai, Aiconfig::Heuristic heuristic);
ColorPfund(ColorPfund const&) = delete;
ColorPfund& operator=(ColorPfund const&) = delete;

~ColorPfund() override;

protected:
auto high_pfund(Trick const& trick)   -> HandCard;
auto pfund(Trick const& trick)        -> HandCard;

auto high_pfund_trump()               -> HandCard;
auto pfund_trump()                    -> HandCard;
auto pfund(Card::Color color)         -> HandCard;
auto arbitrary_pfund()                -> HandCard;

auto is_trumpcolor(Card::Color color) -> bool;
auto pfund_blank_ten()                -> HandCard;
auto pfund_blank_king()               -> HandCard;
auto pfund_single_ten()               -> HandCard;
auto pfund_double_ten()               -> HandCard;
auto pfund_ten()                      -> HandCard;
auto pfund_ace()                      -> HandCard;
};
} // namespace Heuristics
