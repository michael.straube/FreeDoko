/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "pfund.h"

namespace Heuristics {

/** pfund in a trick that goes for sure to the own team
 **/
class ColorPfundForSure : public ColorPfund {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit ColorPfundForSure(Ai const& ai);
ColorPfundForSure(ColorPfundForSure const&) = delete;
ColorPfundForSure& operator=(ColorPfundForSure const&) = delete;

~ColorPfundForSure() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;
};
} // namespace Heuristics
