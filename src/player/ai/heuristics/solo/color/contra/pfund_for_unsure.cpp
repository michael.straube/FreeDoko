/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pfund_for_unsure.h"

#include "../../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool ColorPfundForUnsure::is_valid(GameTypeGroup const game_type,
                                   PlayerTypeGroup const player_group)
{
  return (   is_color_solo(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
ColorPfundForUnsure::ColorPfundForUnsure(Ai const& ai) :
  ColorPfund(ai, Aiconfig::Heuristic::color_pfund_for_unsure)
{ }

/** destructor
 **/
ColorPfundForUnsure::~ColorPfundForUnsure() = default;

/** @return  whether the conditions are met
 **/
bool
ColorPfundForUnsure::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_has(trick.startcard().tcolor())
          && !condition_high_winnercard(trick)
          && !condition_can_jab(trick)
          && (   !condition_winnerplayer_same_team(trick)
              || condition_opposite_or_unknown_team_can_jab(trick))
          && condition_last_player_same_team(trick)
          && condition_own_team_can_jab(trick)
         );
}

/** @return  card to play
 **/
Card
ColorPfundForUnsure::card_to_play(Trick const& trick)
{
  return pfund(trick);
}

} // namespace Heuristics
