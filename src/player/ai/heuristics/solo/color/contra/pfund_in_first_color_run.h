/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "pfund.h"

namespace Heuristics {

/** ColorPfund in the first color run, if the trick goes to the partner
 **/
class ColorPfundInFirstColorRun : public ColorPfund {
public:
static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
explicit ColorPfundInFirstColorRun(Ai const& ai);
ColorPfundInFirstColorRun(ColorPfundInFirstColorRun const&) = delete;
ColorPfundInFirstColorRun& operator=(ColorPfundInFirstColorRun const&) = delete;

~ColorPfundInFirstColorRun() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;

private:
auto condition_winnercard_ace_or_trump(Trick const& trick) -> bool;
auto choose_best_fehl(Trick const& trick) -> Card;
};
} // namespace Heuristics
