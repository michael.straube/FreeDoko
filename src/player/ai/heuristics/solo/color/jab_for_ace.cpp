/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_for_ace.h"
#include "../../jab/jab_for_ace.h"

#include "../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool ColorJabForAce::is_valid(GameTypeGroup const game_type,
                              PlayerTypeGroup const player_group)
{
  return is_color_solo(game_type);
}

/** constructor
 **/
ColorJabForAce::ColorJabForAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::color_jab_for_ace)
{ }

/** destructor
 **/
ColorJabForAce::~ColorJabForAce() = default;

/** @return  whether the conditions are met
 **/
bool
ColorJabForAce::conditions_met(Trick const& trick)
{
  return true;
}

/** @return   card to play
 **/
Card
ColorJabForAce::card_to_play(Trick const& trick)
{
  auto jab_for_ace = JabForAce(ai());
  jab_for_ace.set_player(player());
  return jab_for_ace.get_card(trick);
}

} // namespace Heuristics
