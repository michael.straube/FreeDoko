/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "overjab_re.h"

#include "../../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool MeatlessOverjabRe::is_valid(GameTypeGroup const game_type,
                                 PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::solo_meatless
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
MeatlessOverjabRe::MeatlessOverjabRe(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_overjab_re)
{ }

/** destructor
 **/
MeatlessOverjabRe::~MeatlessOverjabRe() = default;

/** @return  whether the conditions are met
 **/
bool
MeatlessOverjabRe::conditions_met(Trick const& trick)
{
  return (   condition_winnerplayer_soloplayer(trick)
          && condition_can_jab(trick)
         );
}

/** @return  card to play
 **/
Card
MeatlessOverjabRe::card_to_play(Trick const& trick)
{
  auto const card = best_card_to_jab(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no card found to jab"));
    return {};
  }

  if (!guessed_hand_of_soloplayer().can_jab(card)) {
    rationale_.add(_("Heuristic::condition::the soloplayer cannot jab my next higher card %s", _(card)));
    auto const color = trick.startcard().color();
    auto const card = hand().highest_card(color);
    rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(card)));
    return card;
  }
  rationale_.add(_("Heuristic::return::jab with the next higher card %s", _(card)));
  return card;
}


/** @return   this card or a higher one as long as the soloplayer has no lower one
 **/
Card
MeatlessOverjabRe::best_card_to_jab(Trick const& trick)
{
  auto const color = trick.startcard().color();
  auto const& hand = this->hand();
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  Card best_card;
  for (auto const value : higher_values_descending(trick.startcard().value())) {
    auto const card = Card(color, value);
    if (hand.contains(card))
      best_card = card;
    if (hand_of_soloplayer.contains(card))
      break;
  }
  auto const card = hand.next_lower_card(best_card);
  if (card && trick.isjabbed(card))
    return card;
  return best_card;
}

} // namespace Heuristics
