/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../../../heuristic.h"

namespace Heuristics {

/** Base class for pfund heuristics
 **/
class MeatlessPfund : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
MeatlessPfund(Ai const& ai, Aiconfig::Heuristic heuristic);
MeatlessPfund(MeatlessPfund const&)            = delete;
MeatlessPfund& operator=(MeatlessPfund const&) = delete;

~MeatlessPfund() override;

Card card_to_play(Trick const& trick) override;

private:
Card get_pfund_card();
Card get_pfund_card(Card::Color color);
Card get_highest_card(Card::Color color);

Card pfund_soloplayer_has_only_greater_cards(vector<Card> const& pfunds);
Card pfund_soloplayer_has_too_many_greater_cards(vector<Card> const& pfunds);
Card pfund_soloplayer_guessed_has_not_color(vector<Card> const& pfunds);
Card pfund_soloplayer_has_not_color(vector<Card> const& pfunds);
Card pfund_with_highest_points(vector<Card> const& pfunds);
};
} // namespace Heuristics
