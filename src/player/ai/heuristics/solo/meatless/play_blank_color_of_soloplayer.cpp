/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "play_blank_color_of_soloplayer.h"

#include "../../../ai.h"
#include "../../../../cards_information.of_player.h"

namespace Heuristics {

auto MeatlessPlayBlankColorOfSoloplayer::is_valid(GameTypeGroup const game_type,
                                     PlayerTypeGroup const player_group) -> bool
{
  return (   game_type    == GameTypeGroup::solo_meatless
          && player_group == PlayerTypeGroup::contra);
}


MeatlessPlayBlankColorOfSoloplayer::MeatlessPlayBlankColorOfSoloplayer(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_play_blank_color_of_soloplayer)
{ }


MeatlessPlayBlankColorOfSoloplayer::~MeatlessPlayBlankColorOfSoloplayer() = default;


auto MeatlessPlayBlankColorOfSoloplayer::conditions_met(Trick const& trick) -> bool
{
  return condition_startcard(trick);
}


auto MeatlessPlayBlankColorOfSoloplayer::card_to_play(Trick const& trick) -> Card
{
  auto color = Card::nocardcolor;
  auto const& hand = this->hand();
  auto const& cards_information_of_soloplayer = cards_information().of_player(game().players().soloplayer());

  for (auto const c : game().cards().colors()) {
    if (!hand.contains(c))
      continue;
    if (cards_information_of_soloplayer.can_have(c))
      continue;
    rationale_.add(_("Heuristic::condition::the soloplayer does not have the color %s", _(c)));
    if (color == Card::nocardcolor) {
      color = c;
    } else if (hand.count(c) > hand.count(color)) {
    rationale_.add(_("Heuristic::condition::prefer color %s to %s", _(c), _(color)));
    }
  }

  if (color == Card::nocardcolor) {
    rationale_.add(_("Heuristic::reject::no color found to the soloplayer does not have"));
    return {};
  }

  auto const card = hand.highest_card(color);
  rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(card)));
  return card;
}

} // namespace Heuristics
