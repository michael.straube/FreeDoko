/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "serve_color_trick.h"

#include "../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool MeatlessServeColorTrick::is_valid(GameTypeGroup const game_type,
                                       PlayerTypeGroup const player_group)
{
  return  game_type == GameTypeGroup::solo_meatless;
}

/** constructor
 **/
MeatlessServeColorTrick::MeatlessServeColorTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_serve_color_trick)
{ }

/** destructor
 **/
MeatlessServeColorTrick::~MeatlessServeColorTrick() = default;

/** @return  whether the conditions are met
 **/
bool
MeatlessServeColorTrick::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_has(trick.startcard().tcolor())
          && !condition_can_jab(trick)
          && (   !condition_winnerplayer_guessed_same_team(trick)
              || condition_opposite_or_unknown_team_can_jab(trick))
          && !condition_guessed_own_team_can_jab(trick)
         );
}

/** @return  card to play
 **/
Card
MeatlessServeColorTrick::card_to_play(Trick const& trick)
{
  return get_lowest_card(trick.startcard().color());
}

/** @return    smallest card of color
 **/
Card
MeatlessServeColorTrick::get_lowest_card(Card::Color color)
{
  auto const card = hand().lowest_card(color);
  rationale_.add(_("Heuristic::return::lowest card of color %s: %s", _(color), _(card)));
  return card;
}

} // namespace Heuristics
