/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "start_with_best_color.h"

#include "../../../ai.h"

namespace Heuristics {

auto MeatlessStartWithBestColor::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (game_type == GameTypeGroup::solo_meatless);
}


MeatlessStartWithBestColor::MeatlessStartWithBestColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_start_with_best_color)
{ }


MeatlessStartWithBestColor::~MeatlessStartWithBestColor() = default;


auto MeatlessStartWithBestColor::conditions_met(Trick const& trick) -> bool
{
  return condition_startcard(trick);
}


auto MeatlessStartWithBestColor::card_to_play(Trick const& trick) -> Card
{
  auto color = last_color();
  if (condition_have_highest_card(color)) {
    rationale_.add(_("Heuristic::condition::have highest card of last color %s", _(color)));
  } else {
    color = color_with_most_sure_tricks();
    if (color == Card::Color::nocardcolor) {
      rationale_.add(_("Heuristic::reject::no color with sure tricks found"));
      return {};
    }
  }

  auto const card = hand().highest_card(color);

  rationale_.add(_("Heuristic::return::highest card of %s: %s", _(color), _(card)));
  return card;
}


auto MeatlessStartWithBestColor::last_color() -> Card::Color
{
  auto const& tricks = game().tricks();
  if (tricks.current_no() == 0)
    return Card::Color::nocardcolor;
  return tricks.trick(tricks.current_no() - 1).startcard().tcolor();
}


auto MeatlessStartWithBestColor::color_with_most_sure_tricks() -> Card::Color
{
  auto const& hand = this->hand();
  auto best_color = Card::Color::nocardcolor;
  unsigned best_sure_tricks_count = 0;
  for (auto const color : game().cards().colors()) {
    if (!hand.contains(color))
      continue;
    auto const sure_tricks_count = count_sure_tricks(color);
    rationale_.add(_("Heuristic::info::%s: %u sure tricks", _(color), sure_tricks_count));
    if (sure_tricks_count > best_sure_tricks_count) {
      best_color = color;
      best_sure_tricks_count = sure_tricks_count;
    }
  }
  return best_color;
}


auto MeatlessStartWithBestColor::count_sure_tricks(Card::Color const color) -> unsigned
{
  auto const cards = hand().cards(color);
  auto other_cards = cards_information().remaining_cards_of_others(color);
  if (other_cards.empty())
    return cards.size();

  auto const& all_cards = game().cards().cards(color);
  unsigned winning_tricks = 0;

  for (auto const card : all_cards) {
    auto const n = cards.count(card);
    winning_tricks += n;
    for (auto i = 0U; i < n; ++i) {
      other_cards.pop_back();
      if (other_cards.empty())
        break;
    }
    if (other_cards.empty())
      break;
    if (card == other_cards.front())
      break;
  }
  if (other_cards.empty())
    return cards.size();
  return winning_tricks;
}


auto MeatlessStartWithBestColor::condition_have_highest_card(Card::Color const color) -> bool
{
  auto const& hand = this->hand();
  if (!hand.contains(color)) {
    rationale_.add(_("Heuristic::condition::do not have %s", _(color)));
    return false;
  }
  auto const card = hand.highest_card(color);
  if (cards_information().higher_card_exists(card)) {
    rationale_.add(_("Heuristic::condition::do not have the highest card of %s", _(color)));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::have the highest card of %s: %s", _(color), _(card)));
    return true;
  }
}

} // namespace Heuristics
