/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../../../../heuristic.h"

namespace Heuristics {

/** Base class for pfund heuristics
 **/
class PicturePfund : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
PicturePfund(Ai const& ai, Aiconfig::Heuristic heuristic);
PicturePfund(PicturePfund const&) = delete;
PicturePfund& operator=(PicturePfund const&) = delete;

~PicturePfund() override;

Card get_highest_card(Card::Color color);
Card get_blank_ten();
Card get_single_ten();
Card get_solo_double_ten();
Card get_double_ten();
Card get_ten();
Card get_blank_ace();
Card get_double_ace();
Card get_ace();
Card get_high_card_from_solo_color();
Card get_highest_card_from_solo_color();
Card get_medium_card();

private:
bool is_solo_color(Card::Color color) const;
};
} // namespace Heuristics
