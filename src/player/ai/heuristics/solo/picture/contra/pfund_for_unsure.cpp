/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pfund_for_unsure.h"

#include "../../../../ai.h"
#include "../../../../../../game/game.h"


namespace Heuristics {

bool PicturePfundForUnsure::is_valid(GameTypeGroup const game_type,
                                     PlayerTypeGroup const player_group)
{
  return (   is_picture_solo(game_type)
          && is_with_partner(game_type, player_group));
}

PicturePfundForUnsure::PicturePfundForUnsure(Ai const& ai) :
  PicturePfund(ai, Aiconfig::Heuristic::picture_pfund_for_unsure)
{ }

PicturePfundForUnsure::~PicturePfundForUnsure() = default;


auto PicturePfundForUnsure::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_can_jab(trick)
          && (   !condition_winnerplayer_same_team(trick)
              || condition_opposite_or_unknown_team_can_jab(trick))
          && condition_last_player_same_team(trick)
          && condition_own_team_can_jab(trick)
         );
}


auto PicturePfundForUnsure::card_to_play(Trick const& trick) -> Card
{
  auto const color = trick.startcard().tcolor();
  return (hand().contains(color)
          ? get_pfund_card(color)
          : get_pfund_card());
}


auto PicturePfundForUnsure::get_pfund_card(Card::Color const color) -> Card
{
  if (color == Card::trump) {
    rationale_.add(_("Heuristic::reject::must serve trump trick"));
    return {};
  }
  auto const& hand = this->hand();
  auto const card = hand.highest_card(color);
  auto const second_highest_card = hand.second_highest_card(color);
  auto const hand_soloplayer = guessed_hand_of_soloplayer();
  if (hand_soloplayer.contains(color)) {
    auto const highest_card_of_soloplayer = hand_soloplayer.highest_card(color);
    if (   highest_card_of_soloplayer.is_jabbed_by(card)
        && !highest_card_of_soloplayer.is_jabbed_by(second_highest_card)) {
      rationale_.add(_("Heuristic::condition::keep highest card of color %s: %s for later jab", _(color), _(card)));
      rationale_.add(_("Heuristic::return::second highest card of color %s: %s", _(color), _(card)));
      return second_highest_card;
    }
    if (   card.value() == Card::ace
        && hand.count(color) > 2) {
      rationale_.add(_("Heuristic::condition::keep %s for later jab", _(card)));
      rationale_.add(_("Heuristic::return::second highest card of color %s: %s", _(color), _(card)));
      return second_highest_card;
    }
  }
  rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(card)));
  return card;
}


auto PicturePfundForUnsure::get_pfund_card() -> Card
{
  for (auto function : {
       &PicturePfund::get_blank_ten,
       &PicturePfund::get_solo_double_ten,
       &PicturePfund::get_high_card_from_solo_color,
       &PicturePfund::get_double_ten,
       &PicturePfund::get_single_ten,
       &PicturePfund::get_ten,
       &PicturePfund::get_blank_ace,
       &PicturePfund::get_double_ace,
       &PicturePfund::get_highest_card_from_solo_color,
       &PicturePfund::get_medium_card,
       }) {
    auto const card = std::invoke(function, *this);
    if (card)
      return card;
  }
  return {};
}

} // namespace Heuristics
