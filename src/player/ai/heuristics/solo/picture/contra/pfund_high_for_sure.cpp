/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pfund_high_for_sure.h"

#include "../../../../ai.h"
#include "../../../../../../game/game.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PicturePfundHighForSure::is_valid(GameTypeGroup const game_type,
                                       PlayerTypeGroup const player_group)
{
  return (   is_picture_solo(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
PicturePfundHighForSure::PicturePfundHighForSure(Ai const& ai) :
  PicturePfund(ai, Aiconfig::Heuristic::picture_pfund_high_for_sure)
{ }

/** destructor
 **/
PicturePfundHighForSure::~PicturePfundHighForSure() = default;

/** @return  whether the conditions are met
 **/
bool
PicturePfundHighForSure::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_winnerplayer_same_team(trick)
          && !condition_opposite_or_unknown_team_can_jab(trick)
         );
}

/** @return  card to play
 **/
Card
PicturePfundHighForSure::card_to_play(Trick const& trick)
{
  auto const color = trick.startcard().tcolor();
  auto const card = (hand().contains(color)
                     ? get_pfund_card(color)
                     : get_pfund_card());
  if (card.value() < 10) {
    rationale_.add(_("Heuristic::reject::pfund card %s has less then 10 points", _(card)));
    return {};
  }
  return card;
}

/** @return    highest card of color
 **/
Card
PicturePfundHighForSure::get_pfund_card(Card::Color const color)
{
  if (color == Card::trump) {
    rationale_.add(_("Heuristic::reject::must serve trump trick"));
    return {};
  }
  auto const card = get_highest_card(color);
  if (card.value() < 10) {
    rationale_.add(_("Heuristic::reject::pfund card %s has less then 10 points", _(card)));
    return {};
  }
  return card;
}

/** @return    good high card of any color
 **/
Card
PicturePfundHighForSure::get_pfund_card()
{
  for (auto function : {
       &PicturePfund::get_blank_ten,
       &PicturePfund::get_solo_double_ten,
       &PicturePfund::get_high_card_from_solo_color,
       &PicturePfund::get_double_ten,
       &PicturePfund::get_single_ten,
       &PicturePfund::get_ten,
       &PicturePfund::get_blank_ace,
       &PicturePfund::get_double_ace,
       }) {
    auto const card = std::invoke(function, *this);
    if (card)
      return card;
  }
  return {};
}

} // namespace Heuristics
