/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "pfund.h"

namespace Heuristics {

/** pfund many points in a trick that is supposed to go to the own team
 **/
class PicturePfundHighForUnsure : public PicturePfund {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit PicturePfundHighForUnsure(Ai const& ai);
PicturePfundHighForUnsure(PicturePfundHighForUnsure const&) = delete;
PicturePfundHighForUnsure& operator=(PicturePfundHighForUnsure const&) = delete;

~PicturePfundHighForUnsure() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;

private:
Card get_pfund_card(Card::Color color);
Card get_pfund_card();
};
} // namespace Heuristics
