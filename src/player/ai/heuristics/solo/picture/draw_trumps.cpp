/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "draw_trumps.h"

#include "../../../ai.h"

namespace Heuristics {


auto PictureDrawTrumps::is_valid(GameTypeGroup const game_type,
                                 PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureDrawTrumps::PictureDrawTrumps(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_draw_trumps)
{ }


PictureDrawTrumps::~PictureDrawTrumps() = default;


auto PictureDrawTrumps::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_has_trump_majority()
          && !condition_noone_else_has_trump()
         );
}


auto PictureDrawTrumps::card_to_play(Trick const& trick) -> Card
{
  auto const card = next_smaller_trump_then_opponents_highest();
  rationale_.add(_("Heuristic::return::play next smaller trump then opponents highest: %s", _(card)));
  return card;
}


auto PictureDrawTrumps::condition_has_trump_majority() -> bool
{
  auto const trumps_no = hand().count_trumps();
  auto const remaining_trumps_others = cards_information().remaining_trumps_others();

  if (trumps_no == 0) {
    rationale_.add(_("Heuristic::condition::have no trump"));
    return false;
  }

  if (trumps_no > remaining_trumps_others) {
    rationale_.add(_("Heuristic::condition::have more trumps then the others"));
    return true;
  }

  auto const has_trump = [this](Player const& player) {
    return (   player.team() != team()
            && guessed_hand(player).hastrump());
  };
  unsigned const players_with_trump_no = count_if(game().players(), has_trump);

  if (   players_with_trump_no > 1
      && trumps_no > 1
      && (trumps_no == 2 ? players_with_trump_no > 2 : true)
      && (trumps_no == 3 ? players_with_trump_no > 2 : true)
     ) {
    rationale_.add(_("Heuristic::condition::have enough trumps"));
    return true;
  }
  return false;
}


auto PictureDrawTrumps::next_smaller_trump_then_opponents_highest() const -> Card
{
  auto const& hand = this->hand();
  auto const highest_trump = highest_trump_of_opponents();
  auto card = hand.next_lower_card(highest_trump);
  if (!card)
    card = hand.same_or_lower_card(highest_trump);
  if (!card)
    card = hand.next_higher_card(highest_trump);
  return card;
}


auto PictureDrawTrumps::highest_trump_of_opponents() const -> Card
{
  HandCard trump;
  for (auto const& p : game().players()) {
    if (!opposite_team(p))
      continue;

    auto const& t = guessed_hand(p).highest_trump();
    if (trump.is_jabbed_by(t))
      trump = t;
  }
  return trump;
}

} // namespace Heuristics
