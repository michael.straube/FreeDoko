/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "jab_color_trick_for_sure.h"

#include "../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PictureJabColorTrickForSure::is_valid(GameTypeGroup const game_type,
                                           PlayerTypeGroup const player_group)
{
  return is_picture_solo(game_type);
}

/** constructor
 **/
PictureJabColorTrickForSure::PictureJabColorTrickForSure(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_jab_color_trick_for_sure)
{ }

/** destructor
 **/
PictureJabColorTrickForSure::~PictureJabColorTrickForSure() = default;

/** @return  whether the conditions are met
 **/
bool
PictureJabColorTrickForSure::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_color_trick(trick)
          && condition_can_jab_with_trump(trick)
          && !condition_opposite_trump_behind(trick)
         );
}

/** @return   card to play
 **/
Card
PictureJabColorTrickForSure::card_to_play(Trick const& trick)
{
  {
    auto const card = hand().lowest_trump();
    if (trick.isjabbed(card)) {
      rationale_.add(_("Heuristic::return::jab with lowest trump %s", _(card)));
      return card;
    }
  }
  {
    auto const card = hand().next_jabbing_card(trick.winnercard());
    if (!card) {
      rationale_.add(_("Heuristic::reject::cannot jab, winnercard is too high"));
      return {};
    }
    rationale_.add(_("Heuristic::return::jab with trump %s", _(card)));
    return card;
  }
}

} // namespace Heuristics
