/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "play_last_trumps.h"

#include "../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PicturePlayLastTrumps::is_valid(GameTypeGroup const game_type,
                                     PlayerTypeGroup const player_group)
{
  return is_picture_solo(game_type);
}

/** constructor
 **/
PicturePlayLastTrumps::PicturePlayLastTrumps(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_play_last_trumps)
{ }

/** destructor
 **/
PicturePlayLastTrumps::~PicturePlayLastTrumps() = default;

/** @return  whether the conditions are met
 **/
bool
PicturePlayLastTrumps::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_has_trump()
          && condition_noone_else_has_trump()
          && condition_many_trump_cards()
         );
}

/** @return   card to play
 **/
Card
PicturePlayLastTrumps::card_to_play(Trick const& trick)
{
  auto const card = hand().highest_trump();
  rationale_.add(_("Heuristic::return::play highest trump %s", _(card)));
  return card;
}

/** @return   whether the player has at least as many trump cards as othe cards
 **/
bool
PicturePlayLastTrumps::condition_many_trump_cards()
{
  if (hand().count(Card::trump) * 2 >= hand().cardsnumber()) {
    rationale_.add(_("Heuristic::condition::have many trumps"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::less then half of the cards are trump"));
  return false;
}

} // namespace Heuristics
