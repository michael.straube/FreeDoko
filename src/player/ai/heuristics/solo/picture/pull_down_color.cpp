/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "pull_down_color.h"

#include "../../../ai.h"

namespace Heuristics {

auto PicturePullDownColor::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PicturePullDownColor::PicturePullDownColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_pull_down_color)
{ }


PicturePullDownColor::~PicturePullDownColor() = default;


auto PicturePullDownColor::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
         );
}


auto PicturePullDownColor::card_to_play(Trick const& trick) -> Card
{
  auto const color = color_to_pull_down();
  if (color == Card::nocardcolor) {
    rationale_.add(_("Heuristic::reject::no color found to pull down"));
    return {};
  }

  auto const card = hand().highest_card(color);
  rationale_.add(_("Heuristic::return::pull down %s, highest card is %s", _(color), _(card)));
  return card;
}


auto PicturePullDownColor::color_to_pull_down() -> Card::Color
{
  auto const& hand = this->hand();

  for (auto const color : colors_starting_with(last_color())) {
    if (   hand.contains(color)
        && condition_all_tricks_to_me(color)) {
      rationale_.add(_("Heuristic::reject::do not have the highest card of %s", _(color)));
      return color;
    }
  }
  return Card::nocardcolor;
}


auto PicturePullDownColor::last_color() -> Card::Color
{
  auto const& tricks = game().tricks();
  if (tricks.current_no() == 0)
    return Card::Color::nocardcolor;
  return tricks.trick(tricks.current_no() - 1).startcard().tcolor();
}


auto PicturePullDownColor::condition_all_tricks_to_me(Card::Color const color) -> bool
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  if (!cards_information.remaining_others(color)) {
    rationale_.add(_("Heuristic::condition::the other players do not have %s", _(color)));
    return true;
  }

  auto const sort_cards_ascending = [](Card const lhs, Card const rhs) {
    return (lhs.value() < rhs.value());
  };
  auto const sort_cards_descending = [](Card const lhs, Card const rhs) {
    return (lhs.value() > rhs.value());
  };
  auto cards = hand.cards(color);
  auto remaining_cards = cards_information.remaining_cards_of_others(color);
  if (cards.size() < remaining_cards.size()) {
    rationale_.add(_("Heuristic::condition::the other players have more cards of %s then me", _(color)));
    return false;
  }

  sort(cards, sort_cards_descending);
  sort(remaining_cards, sort_cards_ascending);
  auto j = remaining_cards.begin();
  for (auto i = cards.begin(); j != remaining_cards.end(); ++i, ++j) {
    if (i->value() < j->value()) {
      rationale_.add(_("Heuristic::condition::the other players can get a trick with %s", _(color)));
      return false;
    }
  }

  rationale_.add(_("Heuristic::condition::get all tricks of %s", _(color)));
  return true;
}

} // namespace Heuristics
