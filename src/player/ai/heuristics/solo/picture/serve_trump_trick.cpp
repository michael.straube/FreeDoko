/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "serve_trump_trick.h"

#include "../../../ai.h"

namespace Heuristics {

bool PictureServeTrumpTrick::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return is_picture_solo(game_type);
}


PictureServeTrumpTrick::PictureServeTrumpTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_serve_trump_trick)
{ }


PictureServeTrumpTrick::~PictureServeTrumpTrick() = default;


auto PictureServeTrumpTrick::conditions_met(Trick const& trick) -> bool
{
  return (   condition_trump_trick(trick)
          && condition_has_trump()
          && !condition_can_jab(trick));
}


auto PictureServeTrumpTrick::card_to_play(Trick const& trick) -> Card
{
  auto const card = hand().lowest_trump();
  rationale_.add(_("Heuristic::return::play low trump %s", _(card)));
  return card;
}

} // namespace Heuristics
