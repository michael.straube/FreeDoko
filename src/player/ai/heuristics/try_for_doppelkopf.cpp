/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "try_for_doppelkopf.h"

#include "../../../card/trick.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"

#include "../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool TryForDoppelkopf::is_valid(GameTypeGroup const game_type,
                                PlayerTypeGroup const player_group)
{
  return is_with_special_points(game_type);
}

/** constructor
 **/
TryForDoppelkopf::TryForDoppelkopf(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::try_for_doppelkopf)
{ }

/** destructor
 **/
TryForDoppelkopf::~TryForDoppelkopf() = default;

/** @return  whether the conditions are met
 **/
bool
TryForDoppelkopf::conditions_met(Trick const& trick)
{
  return (   condition_second_last_card(trick)
          && condition_can_jab_with_trump(trick)
          && condition_enough_trick_points_for_doppelkopf(trick)
          && condition_has_ten_points_in_trump()
          && condition_last_player_has_10_points(trick)
          && (   condition_last_player_guessed_same_team(trick)
              || condition_last_player_must_play_10_points(trick))
          && condition_can_announce_no_120()
         );
}

/** @return   card to play
 **/
Card
TryForDoppelkopf::card_to_play(Trick const& trick)
{
  { // non special cards
    auto const try_cards = {
      Card(game().cards().trumpcolor(), Card::ace),
      Card(game().cards().trumpcolor(), Card::ten),
    };
    for (auto const card_ : try_cards) {
      auto const card = HandCard(hand(), card_);
      if (!hand().contains(card))
        continue;
      if (!trick.isjabbed(card))
        continue;

      if (card.is_special())
        continue;

      rationale_.add(_("Heuristic::return::no special card - %s", _(card)));
      return card;
    }
  }

  { // special cards
    auto const try_cards = {
      Card(game().cards().trumpcolor(), Card::ten),
      Card(game().cards().trumpcolor(), Card::ace),
      Card::dulle
    };

    for (auto const card_ : try_cards) {
      auto const card = HandCard(hand(), card_);
      if (!hand().contains(card))
        continue;
      if (!trick.isjabbed(card))
        continue;

      if (!card.is_special())
        continue;

      if (   !guessed_same_team(trick.winnerplayer())
          && !trick.isjabbed({hand(), trump_card_limit()})) {
        rationale_.add(_("Heuristic::return::special card needed for winning the trick - %s", _(card)));
        return card;
      }

      if (remaining_tricks_go_to_own_team()) {
        rationale_.add(_("Heuristic::return::special card not needed for an announcement - %s", _(card)));
        return card;
      }

#ifdef OUTDATED
      // 2020-05-03  Umstrukturierung der Heuristiken
      auto const announcement = game.announcements().last(team());
      if (  announcement == Announcement::no60
          ? Heuristics::say_no30(ai())
          : announcement == Announcement::no90
          ? Heuristics::say_no60(ai())
          : announcement == Announcement::no120
          ? Heuristics::say_no90(ai())
          : is_reply(announcement)
          ? Heuristics::say_no90(ai())
          : announcement == Announcement::noannouncement
          ? Heuristics::make_announcement(ai())
          : false) {
        rationale_.add(_("Heuristic::return::announcement can be improved - %s", _(card)));
        return card;
      }
#endif
    } // for (card)
  }

  rationale_.add(_("Heuristic::reject::no card found to jab"));
  return Card::empty;
}

/** @return   whether the last player has ten points
 **/
bool
TryForDoppelkopf::condition_last_player_has_10_points(Trick const& trick)
{
  auto const& hand = guessed_hand(trick.lastplayer());
  auto const cards = hand.validcards(trick);
  if (cards.highest_value() >= 10) {
    rationale_.add(_("Heuristic::condition::last player has 10 points"));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::last player does not have 10 points"));
    return false;
  }
}

/** @return   whether the last player must play 10 points
 **/
bool
TryForDoppelkopf::condition_last_player_must_play_10_points(Trick const& trick)
{
  auto const color = trick.startcard().tcolor();
  auto const& hand = guessed_hand(trick.lastplayer());
  auto const cards = hand.validcards(trick);
  auto const lowest_value = cards.lowest_value(color);
  if (lowest_value >= 10) {
    rationale_.add(_("Heuristic::condition::last player must play 10 points"));
    return false;
  } else {
    rationale_.add(_("Heuristic::condition::last player has other then 10 points to play"));
    return false;
  }
}

/** @return   whether the player can announce no 120 or already has
 **/
bool
TryForDoppelkopf::condition_can_announce_no_120()
{
  auto const& player = this->player();
  auto const team = player.team();
  auto const& game = player.game();
  if (player.announcement() == Announcement::noannouncement) {
    rationale_.add(_("Heuristic::condition::the player has announced %s", _(team)));
    return true;
  } else if (game.announcements().is_valid(Announcement::no120, player)
            ) {
    rationale_.add(_("Heuristic::condition::the player can announce %s", _(team)));
    return true;
  } else {
    rationale_.add(_("Heuristic::condition::the player cannot announce %s", _(team)));
    return false;
  }
}

} // namespace Heuristics
