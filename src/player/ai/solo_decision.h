/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "decision.h"

/** Base class for solo decision
 **/
class SoloDecision : public Decision {
public:
  using Points = int;
public:
  static auto create(GameType game_type, Ai const& ai) -> unique_ptr<SoloDecision>;

  SoloDecision(Ai const& ai, GameType game_type);
  SoloDecision(SoloDecision const&) = delete;
  SoloDecision& operator=(SoloDecision const&) = delete;

  ~SoloDecision() override;

  auto game_type() const -> GameType;

  virtual auto estimated_points() -> Points = 0;

protected: // Hilfsfunktionen
  auto is_duty_solo()   const -> bool;
  auto is_startplayer() const -> bool;

private:
  GameType game_type_;
};

namespace SoloDecisions {
using Points = SoloDecision::Points;
} // namespace SoloDecisions
