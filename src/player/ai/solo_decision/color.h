/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../solo_decision.h"
#include "../../../card/card.h"

namespace SoloDecisions {
/** Decision for color solo
 **/
class Color : public SoloDecision {
public:
  Color(Ai const& ai, Card::Color color);
  Color(Color const&) = delete;
  Color& operator=(Color const&) = delete;

  ~Color() override;

  auto color() const -> Card::Color;

  auto estimated_points() -> Points override;

private:
  auto estimated_points_for_special_cards() -> Points;
  auto estimated_points_for_queens() -> Points;
  auto estimated_points_for_blank_colors() -> Points;
  auto estimated_points_for_color_aces() -> Points;
  auto estimated_points_for_color_aces_with_nines(Card::Color color) -> Points;
  auto estimated_points_for_color_aces_without_nines(Card::Color color) -> Points;
  auto estimated_points_for_color_cards() -> Points;
  auto modifier_for_trumps() -> Points;

  auto hand_contains(Card::Color color) const -> bool;

private:
  Card::Color color_;
};
} // namespace SoloDecisions
