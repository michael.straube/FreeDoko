/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "meatless.h"

#include "../../../game/game.h"
#include "../../../party/rule.h"

namespace SoloDecisions {

Meatless::Meatless(Ai const& ai) :
  SoloDecision(ai, GameType::solo_meatless)
{ }


Meatless::~Meatless() = default;


auto Meatless::estimated_points() -> Points
{
  return lower_points_bound();
}


auto Meatless::lower_points_bound() -> Points
{
  if (   !is_startplayer()
      && !can_jab_each_color()) {
    return 0;
  }

  HandCards loosing_cards(hand());
  for (auto const color : rule().card_colors()) {
    auto const cards = this->loosing_cards(color);
    loosing_cards.add(cards);
    rationale_.add(_("SoloDecision::information::%s: %u loosing cards", _(color), cards.size()));
  }

  Points loosing_points = container_algorithm::sum(loosing_cards, [](Card const card) {
                                                   return card.points();
                                                   });
  int remaining_cards = loosing_cards.size() * (rule()(Rule::Type::number_of_players_in_game) - 1);
  for (auto const value : rule().card_values()) {
    auto const n = std::min(remaining_cards,
                            static_cast<int>(rule()(Rule::Type::number_of_cards_per_value))
                            - static_cast<int>(hand().count(value)));
    loosing_points += Card(Card::club, value).points() * n;
    remaining_cards -= n;
    if (remaining_cards <= 0)
      break;
  }
  return 240 - loosing_points;
}


auto Meatless::can_jab_each_color() -> bool
{
  for (auto const color : rule().card_colors()) {
    if (!can_jab(color)) {
      rationale_.add(_("SoloDecision::information::cannot jab %s", _(color)));
      return false;
    }
  }
  rationale_.add(_("SoloDecision::information::can jab each color"));
  return true;
}


auto Meatless::can_jab(Card::Color const color) -> bool
{
  auto const cards = hand().cards(color, game_type());
  {
    auto const ace = Card(color, Card::ace);
    if (   cards.contains(ace)
        && cards.size() > 2 - cards.count(ace)) {
      rationale_.add(_("SoloDecision::information::can jab %s with ace", _(color)));
      return true;
    }
  }
  {
    auto const ten = Card(color, Card::ten);
    if (   cards.contains(ten)
        && cards.size() > 4 - cards.count(ten)) {
      rationale_.add(_("SoloDecision::information::can jab %s with ten", _(color)));
      return true;
    }
  }
  {
    auto const king = Card(color, Card::king);
    if (   cards.contains(king)
        && cards.size() > 6 - cards.count(king)) {
      rationale_.add(_("SoloDecision::information::can jab %s with king", _(color)));
      return true;
    }
  }
  return false;
}


auto Meatless::loosing_cards(Card::Color const color) -> HandCards
{
  if (hand().count(color, game_type()) == 0)
    return HandCards(hand());
  auto cards = hand().cards(color, game_type());
  int remaining_cards_others = ( rule()(Rule::Type::number_of_same_cards)
                                * rule()(Rule::Type::number_of_card_values)
                                - cards.size());

  for (auto const value : rule().card_values()) {
    auto const card = Card(color, value);
    auto const n = cards.count(card);
    cards.remove_all(card);
    if (n == 0)
      break;
    if (n == 1) {
      remaining_cards_others -= 1;
      if (remaining_cards_others <= 0) {
        cards.clear();
        break;
      }
      break;
    }
    if (remaining_cards_others >= 7)
      remaining_cards_others -= 4;
    else if (remaining_cards_others >= 5)
      remaining_cards_others -= 3;
    else
      remaining_cards_others -= 2;

    if (remaining_cards_others <= 0) {
      cards.clear();
      break;
    }
  }
  return cards;
}


auto Meatless::sum_lowest_points(Card::Color color, unsigned cardno) -> unsigned
{
  unsigned points = 0;
  auto const& cards = hand();
  for (auto const card : {Card(color, Card::nine), Card(color, Card::jack), Card(color, Card::queen), Card(color, Card::king), Card(color, Card::ten)}) {
    auto const n = min(cardno, cards.count(card));
    points += n * card.points();
    cardno -= n;
    if (cardno == 0)
      break;
  }
  return points;
}

} // namespace SoloDecisions
