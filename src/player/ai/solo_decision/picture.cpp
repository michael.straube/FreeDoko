/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "picture.h"

#include "../../../party/rule.h"
#include "../../../game/game.h"

namespace SoloDecisions {

Picture::Picture(Ai const& ai,
                 Card::Value const picture) :
  SoloDecision(ai, solo(picture)),
  picture_({picture})
  {
    DEBUG_ASSERTION((   picture == Card::jack
                     || picture == Card::queen
                     || picture == Card::king),
                    "SoloDecision::Picture::Picture(ai, picture = " << picture << ")\n"
                    "   picture is not valid, must either be jack or queen or king");
  }


Picture::Picture(Ai const& ai,
                 Card::Value const picture1,
                 Card::Value const picture2) :
  SoloDecision(ai, solo(picture1, picture2)),
  picture_({picture1, picture2})
  {
    DEBUG_ASSERTION(picture1 != picture2,
                    "SoloDecision::Picture::Picture(ai, picture1 = " << picture1 << ", picture2 = " << picture2 << ")\n"
                    "   picture1 and picture2 must be different");
    DEBUG_ASSERTION((   picture1 == Card::queen
                     || picture1 == Card::king),
                    "SoloDecision::Picture::Picture(ai, picture1 = " << picture1 << ", picture2 = " << picture2 << ")\n"
                    "   picture1 is not valid, must either be queen or king");
    DEBUG_ASSERTION((   picture2 == Card::jack
                     || picture2 == Card::queen),
                    "SoloDecision::Picture::Picture(ai, picture1 = " << picture1 << ", picture2 = " << picture2 << ")\n"
                    "   picture2 is not valid, must either be jack or queen");
  }


Picture::Picture(Ai const& ai,
                 Card::Value const picture1,
                 Card::Value const picture2,
                 Card::Value const picture3) :
  SoloDecision(ai, GameType::solo_koehler),
  picture_({picture1, picture2, picture3})
  {
    DEBUG_ASSERTION(   picture1 == Card::king
                    && picture2 == Card::queen
                    && picture3 == Card::jack,
                    "SoloDecision::Picture::Picture(ai, picture1 = " << picture1 << ", picture2 = " << picture2 << ", picture3 = " << picture3 << ")\n"
                    "   must be king, queen, jack in this order");
  }


Picture::~Picture() = default;


auto Picture::all_single_cards(Card::Color color) const -> vector<Card>
{
  vector<Card> cards = {Card(color, Card::ace),
    Card(color, Card::ten)};
  if (!is_trump(Card::king))
    cards.emplace_back(color, Card::king);
  if (!is_trump(Card::queen))
    cards.emplace_back(color, Card::queen);
  if (!is_trump(Card::jack))
    cards.emplace_back(color, Card::jack);
  if (rule()(Rule::Type::with_nines))
    cards.emplace_back(color, Card::nine);
  return cards;
}


auto Picture::is_trump(Card::Value const value) const -> bool
{
  return contains(picture_, value);
}


auto Picture::is_trump(Card const card) const -> bool
{
  return is_trump(card.value());
}


auto Picture::count_trumps() const -> unsigned
{
  size_t n = 0;
  for (auto p : picture_)
    n += hand().count(p);
  return n;
}


auto Picture::count_aces() const -> unsigned
{
  return hand().count(Card::ace);
}


auto Picture::count_aces_and_tens() const -> unsigned
{
  auto const& hand = this->hand();
  auto n = hand.count(Card::ace);
  for (auto const color : rule().card_colors()) {
    if (hand.count(color, Card::ace) == 2)
      n += hand.count(color, Card::ten);
  }
  return n;
}


auto Picture::count_blank_aces() const -> unsigned
{
  auto const& hand = this->hand();
  unsigned n = 0;
  for (auto const color : rule().card_colors()) {
    if (   hand.count(color, game_type()) == 1
        && hand.contains(color, Card::ace)) {
      n += 1;
    }
  }
  return n;
}


auto Picture::count_highest_colors() const -> unsigned
{
  auto const& hand = this->hand();
  unsigned n = 0;
  for (auto const color : rule().card_colors()) {
    for (auto const card : all_single_cards(color)) {
      auto const i = hand.count(card);
      n += i;
      if (i < 2) {
        break;
      }
    }
  }
  return n;
}


auto Picture::sum_lowest_points(Card::Color const color,
                                unsigned cardno) const -> unsigned
{
  unsigned points = 0;
  auto const& cards = hand();
  for (auto const card : {Card(color, Card::nine), Card(color, Card::jack), Card(color, Card::queen), Card(color, Card::king), Card(color, Card::ten)}) {
    if (is_trump(card))
      continue;
    auto const n = min(cardno, cards.count(card));
    points += n * card.points();
    cardno -= n;
    if (cardno == 0)
      break;
  }
  return points;
}

} // namespace SoloDecisions
