/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../solo_decision.h"
#include "../../../card/card.h"

namespace SoloDecisions {
/** Decision for picture solo and meatless
 **/
class Picture : public SoloDecision {
public:
  Picture(Ai const& ai, Card::Value picture);
  Picture(Ai const& ai, Card::Value picture1, Card::Value picture2);
  Picture(Ai const& ai, Card::Value picture1, Card::Value picture2, Card::Value picture3);
  Picture(Picture const&) = delete;
  Picture& operator=(Picture const&) = delete;

  ~Picture() override;

protected: // Hilfsfunktionen
  auto all_single_cards(Card::Color color) const -> vector<Card>;

  auto is_trump(Card::Value value) const -> bool;
  auto is_trump(Card card)         const -> bool;
  auto count_trumps()         const -> unsigned;
  auto count_aces()           const -> unsigned;
  auto count_aces_and_tens()  const -> unsigned;
  auto count_blank_aces()     const -> unsigned;
  auto count_highest_colors() const -> unsigned;

  auto sum_lowest_points(Card::Color color, unsigned cardno) const -> unsigned;

protected:
  vector<Card::Value> picture_;
};
} // namespace SoloDecisions
