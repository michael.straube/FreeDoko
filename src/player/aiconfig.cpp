/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "aiconfig.h"

#include "ai/heuristic.h"

#include "../versions.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../misc/preferences.h"
#include "../ui/ui.h"


// NOLINTNEXTLINE(misc-no-recursion)
auto Aiconfig::preset(Difficulty const difficulty) -> Aiconfig const&
{
  DEBUG_ASSERTION( (difficulty != Difficulty::custom),
                  "Aiconfig::preset(Type::difficulty)\n"
                  "  difficulty is 'custom'");

  // just to get a warning when a new difficulty has been added
  switch (difficulty) {
  case Difficulty::custom:
  case Difficulty::standard:
  case Difficulty::cautious:
  case Difficulty::chancy:
  case Difficulty::unfair:
    break;
  } // switch (difficulty)

  static map<Difficulty, unique_ptr<Aiconfig>> aiconfigs;
  if (aiconfigs.empty()) {
    { // standard
      aiconfigs[Difficulty::standard] = make_unique<Aiconfig>();
    } // standard
    { // cautious
      aiconfigs[Difficulty::cautious] = make_unique<Aiconfig>();
      auto& aiconfig = *aiconfigs[Difficulty::cautious];
      aiconfig.bool_[Type::cautious] = true;
      aiconfig.int_[Type::limitqueen] = 9;
      aiconfig.int_[Type::limitdulle] = 12;
      aiconfig.int_[Type::first_trick_for_trump_points_optimization] = 5;
      aiconfig.int_[Type::announcelimit] = 11;
      aiconfig.int_[Type::announcelimitreply] = 10;
    } // cautious
    { // chancy
      aiconfigs[Difficulty::chancy] = make_unique<Aiconfig>();
      auto& aiconfig = *aiconfigs[Difficulty::chancy];
      aiconfig.bool_[Type::trusting] = true;
      aiconfig.bool_[Type::aggressive] = true;
      aiconfig.bool_[Type::chancy] = true;
      aiconfig.int_[Type::limitqueen] = 7;
      aiconfig.int_[Type::limitdulle] = 10;
      aiconfig.int_[Type::first_trick_for_trump_points_optimization] = 5;
      aiconfig.int_[Type::announcelimit] = 10;
      aiconfig.int_[Type::announcelimitreply] = 9;

      for (auto& states : aiconfig.heuristic_states_) {
        for (auto& state : states.second) {
          if (state.heuristic == Heuristic::grab_trick) {
            state.active = true;
          }
        }
      }
    } // chancy
    { // unfair
      aiconfigs[Difficulty::unfair] = make_unique<Aiconfig>();
      auto& aiconfig = *aiconfigs[Difficulty::unfair];
      aiconfig.bool_[Type::hands_known] = true;
    } // unfair
  } // if (aiconfigs.empty())

  return *aiconfigs[difficulty];
}


auto Aiconfig::keys() -> vector<HeuristicsMap::Key> const&
{
  static vector<HeuristicsMap::Key> const keys = {
    {HeuristicsMap::GameTypeGroup::normal, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::normal, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::marriage_undetermined, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::marriage_undetermined, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::marriage_silent, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::marriage_silent, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::poverty, HeuristicsMap::PlayerTypeGroup::special},
    {HeuristicsMap::GameTypeGroup::poverty, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::poverty, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::soli_color, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::soli_color, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::soli_single_picture, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::soli_single_picture, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::soli_double_picture, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::soli_double_picture, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::solo_koehler, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::solo_koehler, HeuristicsMap::PlayerTypeGroup::contra},
    {HeuristicsMap::GameTypeGroup::solo_meatless, HeuristicsMap::PlayerTypeGroup::re},
    {HeuristicsMap::GameTypeGroup::solo_meatless, HeuristicsMap::PlayerTypeGroup::contra},
  };

  return keys;
}


// NOLINTNEXTLINE(misc-no-recursion)
Aiconfig::Aiconfig()
{
  reset_to_hardcoded();
}


Aiconfig::Aiconfig(Difficulty const difficulty)
{
  reset_to_hardcoded();
  set_to_difficulty(difficulty);
}


Aiconfig::Aiconfig(istream& istr)
{
  (void)heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::normal,
                                             HeuristicsMap::PlayerTypeGroup::re)];
  reset_to_hardcoded();
  read(istr);
}


Aiconfig::Aiconfig(istream& istr, Difficulty const difficulty) :
  difficulty_(difficulty)
{
  (void)heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::normal,
                                             HeuristicsMap::PlayerTypeGroup::re)];
  read(istr);
  DEBUG_ASSERTION(update_level_ == 1,
                  "Aiconfig::Aiconfig(istr, difficulty)\n"
                  "  update_level_ = " << update_level_ << " is not 1");
  update_level_ = 0;

  difficulty_ = difficulty;
}


Aiconfig::Aiconfig(Aiconfig const& aiconfig) :
  difficulty_(aiconfig.difficulty_),
  heuristic_states_(aiconfig.heuristic_states_),
  bool_(aiconfig.bool_),
  int_(aiconfig.int_),
  card_(aiconfig.card_)
{ }


// NOLINTNEXTLINE(misc-no-recursion)
auto Aiconfig::operator=(Aiconfig const& aiconfig) -> Aiconfig&
{
  if (this == &aiconfig)
    return *this;

  enter_update();
  difficulty_ = aiconfig.difficulty_;
  rating_ = aiconfig.rating_;
  heuristic_states_ = aiconfig.heuristic_states_;
  for (auto const type : Aiconfig::Type::bool_list)
    set(type, aiconfig.value(type));
  for (auto const type : Aiconfig::Type::int_list)
    set(type, aiconfig.value(type));
  for (auto const type : Aiconfig::Type::card_list)
    set(type, aiconfig.value(type));
  difficulty_ = aiconfig.difficulty();

  leave_update(difficulty());

  return *this;
}


Aiconfig::~Aiconfig() = default;


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::reset_to_hardcoded(unsigned const no)
{

  if (no != UINT_MAX) {
    set_to_difficulty(Difficulty::standard);
    return ;
  }

  enter_update();

  set(Type::trusting,                               false);
  set(Type::aggressive,                             false);
  set(Type::cautious,                               false);
  set(Type::chancy,                                 false);
  set(Type::hands_known,                            false);
  set(Type::teams_known,                            false);

  set(Type::future_limit,                           10000);
  set(Type::max_sec_wait_for_gametree,                 10);
  set_rating(Rating::Type::max);

  set(Type::limit_throw_fehl,                          8);
  set(Type::limitqueen,                                9);
  set(Type::limitdulle,                               12);

  set(Type::last_tricks_without_heuristics,            3);
  set(Type::first_trick_for_trump_points_optimization, 4);

  set(Type::announcelimit,                            11);
  set(Type::announcelimitdec,                          1);
  set(Type::announceconfig,                            1);
  set(Type::announcelimitreply,                       10);
  set(Type::announceconfigreply,                       1);

  set(Type::limitthrowing,                   Card::diamond_queen);

  set(Type::trumplimit_solocolor,            Card::diamond_queen);
  set(Type::trumplimit_solojack,             Card::club_jack);
  set(Type::trumplimit_soloqueen,            Card::club_queen);
  set(Type::trumplimit_soloking,             Card::club_king);
  set(Type::trumplimit_solojackking,         Card::heart_king);
  set(Type::trumplimit_solojackqueen,        Card::heart_queen);
  set(Type::trumplimit_soloqueenking,        Card::heart_king);
  set(Type::trumplimit_solokoehler,          Card::club_queen);
  set(Type::trumplimit_meatless,             Card::diamond_ten);
  set(Type::trumplimit_normal,               Card::heart_queen);

  set(Type::lowest_trumplimit_solocolor,     Card::heart_jack);
  set(Type::lowest_trumplimit_solojack,      Card::heart_jack);
  set(Type::lowest_trumplimit_soloqueen,     Card::heart_queen);
  set(Type::lowest_trumplimit_soloking,      Card::heart_king);
  set(Type::lowest_trumplimit_solojackking,  Card::spade_jack);
  set(Type::lowest_trumplimit_solojackqueen, Card::spade_jack);
  set(Type::lowest_trumplimit_soloqueenking, Card::spade_queen);
  set(Type::lowest_trumplimit_solokoehler,   Card::spade_queen);
  set(Type::lowest_trumplimit_meatless,      Card::diamond_queen);
  set(Type::lowest_trumplimit_normal,        Card::diamond_jack);

  set(Type::limithigh,                       Card::heart_queen);

  init_heuristic_states();

  difficulty_ = Difficulty::standard;
  leave_update(Difficulty::standard);
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::set_to_difficulty(Difficulty const difficulty)
{
  if (difficulty == Difficulty::custom)
    return ;

  *this = Aiconfig::preset(difficulty);
  difficulty_ = difficulty;

  if (::ui != nullptr)
    ::ui->aiconfig_changed(*this);
}


void Aiconfig::rule_changed(int const type, void const* const old_value)
{
  if (type == Rule::Type::with_nines) {
    if (difficulty() != Difficulty::custom) {
      auto const difficulty_bak = difficulty();
      difficulty_ = Difficulty::custom;
      set_to_difficulty(difficulty_bak);
    }
  } // if (type == Rule::Type::with_nines)
}


auto Aiconfig::difficulty() const -> Aiconfig::Difficulty
{ return difficulty_; }


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::update_difficulty()
{
  if (in_update())
    return ;

  auto difficulty = Difficulty::custom;
  for (auto const d : aiconfig_difficulty_list) {
    if (   (d != Difficulty::custom)
        && equal(Aiconfig::preset(d))) {
      difficulty = d;
      break;
    }
  }

  if (difficulty == this->difficulty())
    return ;

  difficulty_ = difficulty;
  if (::ui)
    ::ui->aiconfig_changed(*this);
}


auto Aiconfig::in_update() const -> bool
{
  return (update_level_ > 0);
}


void Aiconfig::enter_update()
{
  update_level_ += 1;
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::leave_update()
{
  DEBUG_ASSERTION((update_level_ > 0) && (update_level_ < 9999),
                  "Aiconfig::leave_update()\n"
                  "  update_level is not > 0 / < 9999: " << update_level_);
  update_level_ -= 1;
  if (update_level_ == 0)
    update_difficulty();
}


void Aiconfig::leave_update(Difficulty const difficulty)
{
  DEBUG_ASSERTION(update_level_ > 0,
                  "Aiconfig::leave_update()\n"
                  "  update_level is not > 0: " << update_level_);
  update_level_ -= 1;
  if (update_level_ == 0)
    difficulty_ = difficulty;
}


auto Aiconfig::equal(Aiconfig const& aiconfig) const -> bool
{
  return (   rating_           == aiconfig.rating_
          && heuristic_states_ == aiconfig.heuristic_states_
          && bool_             == aiconfig.bool_
          && int_              == aiconfig.int_
          && card_             == aiconfig.card_);
}


auto Aiconfig::rating() const -> Rating::Type
{
  return rating_;
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::set_rating(Rating::Type const rating)
{
  enter_update();
  rating_ = rating;
  leave_update();
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Aiconfig::heuristic_states(HeuristicsMap::Key const key) const -> vector<Aiconfig::HeuristicState> const&
{
  auto states = heuristic_states_.find(key);

  // if there is no special take a default one
  if (   states == heuristic_states_.end()
      || states->second.empty()) {
    if (   key.gametype_group == HeuristicsMap::GameTypeGroup::solo_meatless
        && key.playertype_group == HeuristicsMap::PlayerTypeGroup::contra)
      return heuristic_states(HeuristicsMap::GameTypeGroup::solo_meatless,
                              HeuristicsMap::PlayerTypeGroup::re);

    if (key.gametype_group == HeuristicsMap::GameTypeGroup::normal)
      return heuristic_states(HeuristicsMap::GameTypeGroup::normal, HeuristicsMap::PlayerTypeGroup::re);

    return heuristic_states(HeuristicsMap::GameTypeGroup::normal, key.playertype_group);
  } // if (states.empty())

  return states->second;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Aiconfig::heuristic_states(HeuristicsMap::GameTypeGroup const gametype_group,
                                HeuristicsMap::PlayerTypeGroup const playertype_group
                               ) const -> vector<Aiconfig::HeuristicState> const&
{
  return heuristic_states(HeuristicsMap::Key(gametype_group,
                                             playertype_group));
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Aiconfig::heuristic_states(HeuristicsMap::Key const key) -> vector<Aiconfig::HeuristicState>&
{
  auto states = heuristic_states_.find(key);

  // if there is no special take a default one
  if (states == heuristic_states_.end()) {
    if (   (key.gametype_group == HeuristicsMap::GameTypeGroup::solo_meatless)
        && (key.playertype_group == HeuristicsMap::PlayerTypeGroup::contra) )
      return heuristic_states(HeuristicsMap::GameTypeGroup::solo_meatless,
                              HeuristicsMap::PlayerTypeGroup::re);

    if (key.gametype_group == HeuristicsMap::GameTypeGroup::normal)
      return heuristic_states(HeuristicsMap::GameTypeGroup::normal, HeuristicsMap::PlayerTypeGroup::re);

    return heuristic_states(HeuristicsMap::GameTypeGroup::normal, key.playertype_group);
  } // if (states.empty())

  return states->second;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Aiconfig::heuristic_states(HeuristicsMap::GameTypeGroup const gametype_group, HeuristicsMap::PlayerTypeGroup const playertype_group) -> vector<Aiconfig::HeuristicState>&
{
  return heuristic_states(HeuristicsMap::Key(gametype_group,
                                             playertype_group));
}


auto Aiconfig::heuristics(HeuristicsMap::GameTypeGroup const gametype_group,
                          HeuristicsMap::PlayerTypeGroup const playertype_group
                         ) const -> vector<Aiconfig::Heuristic>
{
  auto const& states = heuristic_states(gametype_group, playertype_group);

  vector<Heuristic> heuristics;
  heuristics.push_back(Heuristic::only_one_valid_card);
  for (auto const& state : states) {
    if (state.active)
      heuristics.push_back(state.heuristic);
  }

  heuristics.push_back(Heuristic::valid_card);
  return heuristics;
}


auto Aiconfig::heuristics(Player const& player) const -> vector<Aiconfig::Heuristic>
{
  return heuristics(HeuristicsMap::group(player.game()),
                    HeuristicsMap::group(player));
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::init_heuristic_states()
{
  enter_update();
  heuristic_states_.clear();
  (void)heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::normal,
                                             HeuristicsMap::PlayerTypeGroup::re)];


  { // normal
    heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::normal,
                                         HeuristicsMap::PlayerTypeGroup::re)]
      = { Heuristic::remaining_tricks_to_me,
        Heuristic::play_last_trumps,
        Heuristic::gametree,
        Heuristic::start_with_color_single_ace,
        Heuristic::start_with_color_double_ace,
        Heuristic::jab_with_color_ace,
        Heuristic::jab_with_color_ten,
        Heuristic::play_color_for_partner_ace,
        Heuristic::play_color_for_last_player_to_jab,
        Heuristic::start_with_color_ten,
        Heuristic::pfund_high_for_sure,
        Heuristic::jab_for_doppelkopf,
        Heuristic::try_for_doppelkopf,
        Heuristic::pfund_in_first_color_run,
        Heuristic::serve_color_trick,
        Heuristic::pfund_high_for_unsure,
        Heuristic::pfund_for_sure,
        Heuristic::last_player_jab_with_trump_ace_or_ten,
        Heuristic::jab_fox,
        Heuristic::jab_for_ace,
        Heuristic::save_dulle,
        Heuristic::save_dulle_from_swines,
        Heuristic::pfund_for_unsure,
        Heuristic::start_with_trump_pfund,
        Heuristic::pfund_before_partner,
        Heuristic::play_highest_color_card_in_game,
        Heuristic::jab_small_color_card,
        Heuristic::start_with_trump_for_partner_color_ace,
        Heuristic::play_color_for_partner_to_jab,
        {Heuristic::start_with_color,               false},
        Heuristic::retry_color,
        Heuristic::jab_first_color_run,
        Heuristic::last_player_pass_small_trick,
        Heuristic::last_player_jab_for_points,
        Heuristic::partner_backhand_draw_trump,
        Heuristic::jab_to_win,
        Heuristic::jab_to_not_loose,
        Heuristic::cannot_jab,
        Heuristic::jab_trump_high_because_of_partner_announcement,
        Heuristic::jab_trump_high_after_opponent_announcement,
        Heuristic::second_last_jab_for_following_pfund,
        Heuristic::jab_second_color_run,
        Heuristic::jab_color_trick_high,
        Heuristic::jab_rich_trick,
        Heuristic::create_blank_color,
        Heuristic::jab_trump_over_fox,
        Heuristic::jab_color_over_fox,
        Heuristic::jab_trump_because_of_partner_announcement,
        Heuristic::high_after_partner_low,
        Heuristic::jab_with_relative_low_trump,
        Heuristic::jab_with_opponent_backhand,
        Heuristic::serve_trump_trick,
        Heuristic::jab_with_highest_trump,
        Heuristic::draw_trump_for_team_announcement,
        Heuristic::draw_remaining_trump,
        Heuristic::draw_trump,
        Heuristic::play_to_jab_later,
        {Heuristic::start_with_low_color,           false},
        Heuristic::start_with_low_trump,
        Heuristic::play_for_partner_worries,
        Heuristic::play_trump,
        Heuristic::play_bad_color,
        Heuristic::start_with_lowest_color,
        Heuristic::start_with_lowest_trump,
        Heuristic::serve_partner_with_lowest_card,
        Heuristic::jab_with_lowest_card_below_trump_card_limit,
        Heuristic::serve_with_lowest_card,
        Heuristic::jab_with_lowest_card,
      };

    { // contra
      auto& states
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::normal,
                                               HeuristicsMap::PlayerTypeGroup::contra)]
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::normal,
                                               HeuristicsMap::PlayerTypeGroup::re)];
      for (auto& state : states) {
        switch (state.heuristic) {
        case Heuristic::start_with_color:
          state.active = true;
          break;
        case Heuristic::start_with_low_color:
          state.active = true;
          break;
        case Heuristic::start_with_low_trump:
          state.active = false;
          break;
        default:
          break;
        } // switch (s->heuristic)
      } // for (s)
    } // contra
  } // normal
  { // marriage
    { // undetermined marriage
      { // re
        heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::marriage_undetermined,
                                             HeuristicsMap::PlayerTypeGroup::re)]
          = { Heuristic::start_to_get_married_last_chance,
            Heuristic::start_with_color_single_ace,
            Heuristic::start_to_get_married,
            Heuristic::pfund_in_marriage_determination_trick,
            Heuristic::remaining_tricks_to_me,
            Heuristic::play_last_trumps,
            Heuristic::gametree,
            Heuristic::start_with_color_single_ace,
            Heuristic::start_with_color_double_ace,
            Heuristic::jab_with_color_ace,
            Heuristic::jab_with_color_ten,
            Heuristic::start_with_color_ten,
            Heuristic::jab_for_doppelkopf,
            Heuristic::try_for_doppelkopf,
            Heuristic::serve_color_trick,
            Heuristic::jab_fox,
            Heuristic::jab_for_ace,
            Heuristic::save_dulle,
            Heuristic::save_dulle_from_swines,
            Heuristic::play_highest_color_card_in_game,
            Heuristic::jab_small_color_card,
            Heuristic::retry_color,
            Heuristic::jab_first_color_run,
            Heuristic::last_player_pass_small_trick,
            Heuristic::last_player_jab_for_points,
            Heuristic::jab_second_color_run,
            Heuristic::jab_color_trick_high,
            Heuristic::create_blank_color,
            Heuristic::jab_to_win,
            Heuristic::jab_to_not_loose,
            Heuristic::cannot_jab,
            Heuristic::jab_rich_trick,
            Heuristic::jab_trump_over_fox,
            Heuristic::jab_color_over_fox,
            Heuristic::jab_with_relative_low_trump,
            Heuristic::serve_trump_trick,
            Heuristic::jab_with_highest_trump,
            Heuristic::draw_remaining_trump,
            Heuristic::draw_trump,
            Heuristic::play_to_jab_later,
            Heuristic::play_trump,
            Heuristic::start_with_lowest_color,
            Heuristic::start_with_lowest_trump,
            Heuristic::jab_with_lowest_card_below_trump_card_limit,
            Heuristic::serve_with_lowest_card,
            Heuristic::jab_with_lowest_card,
          };
      }

      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::marriage_undetermined,
                                           HeuristicsMap::PlayerTypeGroup::contra)]
        = { Heuristic::remaining_tricks_to_me,
          Heuristic::gametree,
          Heuristic::start_to_marry,
          Heuristic::jab_to_marry,
          Heuristic::start_with_color_single_ace,
          Heuristic::start_with_color_double_ace,
          Heuristic::jab_with_color_ace,
          Heuristic::jab_with_color_ten,
          Heuristic::start_with_color_ten,
          Heuristic::jab_for_doppelkopf,
          Heuristic::try_for_doppelkopf,
          Heuristic::jab_small_color_card,
          Heuristic::serve_color_trick,
          Heuristic::jab_fox,
          Heuristic::last_player_jab_with_trump_ace_or_ten,
          Heuristic::jab_for_ace,
          Heuristic::retry_color,
          Heuristic::save_dulle,
          Heuristic::save_dulle_from_swines,
          Heuristic::jab_first_color_run,
          Heuristic::jab_second_color_run,
          Heuristic::jab_color_trick_high,
          Heuristic::create_blank_color,
          Heuristic::jab_color_over_fox,
          Heuristic::jab_with_highest_trump,
          Heuristic::draw_remaining_trump,
          Heuristic::draw_trump,
          Heuristic::play_to_jab_later,
          Heuristic::jab_to_win,
          Heuristic::jab_to_not_loose,
          Heuristic::last_player_jab_for_points,
          Heuristic::jab_with_relative_low_trump,
          Heuristic::serve_trump_trick,
          Heuristic::start_with_lowest_color,
          Heuristic::start_with_lowest_trump,
          Heuristic::jab_with_lowest_card_below_trump_card_limit,
          Heuristic::serve_with_lowest_card,
          Heuristic::jab_with_lowest_card,
        };
    } // undetermined marriage
    { // silent marriage
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::marriage_silent,
                                           HeuristicsMap::PlayerTypeGroup::re)]
        = { Heuristic::remaining_tricks_to_me,
          Heuristic::play_last_trumps,
          Heuristic::gametree,
          Heuristic::start_with_color_single_ace,
          Heuristic::start_with_color_double_ace,
          Heuristic::start_with_color_ten,
          Heuristic::jab_with_color_ace,
          Heuristic::jab_with_color_ten,
          Heuristic::last_player_jab_with_trump_ace_or_ten,
          Heuristic::color_jab_for_ace,
          Heuristic::jab_with_highest_trump,
          Heuristic::draw_remaining_trump,
          Heuristic::draw_trump,
          Heuristic::play_to_jab_later,
          Heuristic::play_highest_color_card_in_game,
          Heuristic::jab_to_win,
          Heuristic::jab_to_not_loose,
          Heuristic::last_player_jab_for_points,
          Heuristic::start_with_lowest_color,
          Heuristic::start_with_lowest_trump,
          Heuristic::jab_with_lowest_card_below_trump_card_limit,
          Heuristic::serve_with_lowest_card,
          Heuristic::jab_with_lowest_card,
        };
    } // silent marriage
  } // marriage
  { // poverty
    heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::poverty,
                                         HeuristicsMap::PlayerTypeGroup::special)]
      = { Heuristic::remaining_tricks_to_me,
        Heuristic::gametree,
        Heuristic::start_with_color_blank_ace,
        Heuristic::start_with_color_single_ace,
        Heuristic::start_with_color_double_ace,
        Heuristic::play_color_for_last_player_to_jab,
        Heuristic::poverty_special_start_with_trump,
        Heuristic::jab_with_color_ace,
        Heuristic::jab_with_color_ten,
        Heuristic::play_color_for_partner_ace,
        Heuristic::start_with_color_ten,
        Heuristic::play_color_for_partner_to_jab,
        Heuristic::last_player_jab_with_trump_ace_or_ten,
        Heuristic::last_player_jab_for_points,
        Heuristic::poverty_special_play_pfund,
        Heuristic::poverty_special_give_no_points,
        Heuristic::poverty_special_offer_pfund,
        Heuristic::jab_with_opponent_backhand,
        Heuristic::jab_small_color_card,
        Heuristic::serve_color_trick,
        Heuristic::jab_with_relative_low_trump,
        Heuristic::serve_trump_trick,
        Heuristic::play_bad_color,
        Heuristic::serve_with_lowest_card,
      };
    heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::poverty,
                                         HeuristicsMap::PlayerTypeGroup::re)]
      = { Heuristic::remaining_tricks_to_me,
        Heuristic::play_last_trumps,
        Heuristic::gametree,
        Heuristic::start_with_color_single_ace,
        Heuristic::start_with_color_double_ace,
        Heuristic::start_with_color_ten,
        Heuristic::jab_with_color_ace,
        Heuristic::jab_with_color_ten,
        Heuristic::jab_for_doppelkopf,
        Heuristic::create_blank_color,
        Heuristic::pfund_in_first_color_run,
        Heuristic::last_player_jab_with_trump_ace_or_ten,
        Heuristic::jab_first_color_run,
        Heuristic::serve_color_trick,
        Heuristic::jab_to_win,
        Heuristic::jab_to_not_loose,
        Heuristic::serve_trump_trick,
        Heuristic::draw_remaining_trump,
        Heuristic::draw_trump,
        Heuristic::poverty_re_start_with_trump,
        Heuristic::poverty_re_jab_as_last_player,
        Heuristic::poverty_re_jab_color_trick_with_trump,
        Heuristic::jab_with_opponent_backhand,
        Heuristic::jab_with_highest_trump,
        Heuristic::play_to_jab_later,
        Heuristic::play_highest_color_card_in_game,
        Heuristic::jab_small_color_card,
        Heuristic::play_trump,
        Heuristic::jab_color_over_fox,
        Heuristic::last_player_jab_for_points,
        Heuristic::start_with_lowest_trump,
        Heuristic::jab_with_lowest_card_below_trump_card_limit,
        Heuristic::serve_with_lowest_card,
        Heuristic::jab_with_lowest_card,
      };
    heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::poverty,
                                         HeuristicsMap::PlayerTypeGroup::contra)]
      = { Heuristic::remaining_tricks_to_me,
        Heuristic::play_last_trumps,
        Heuristic::gametree,
        Heuristic::play_color_for_last_player_to_jab,
        Heuristic::play_color_for_partner_to_jab,
        Heuristic::start_with_color_single_ace,
        Heuristic::start_with_color_double_ace,
        Heuristic::start_with_color_ten,
        Heuristic::jab_with_color_ace,
        Heuristic::jab_with_color_ten,
        Heuristic::last_player_jab_with_trump_ace_or_ten,
        Heuristic::poverty_contra_start_with_color,
        Heuristic::poverty_contra_jab_color_trick_with_trump,
        Heuristic::serve_color_trick,
        Heuristic::pfund_in_first_color_run,
        Heuristic::poverty_contra_pfund_high_for_sure,
        Heuristic::poverty_contra_pfund_high_for_unsure,
        Heuristic::save_dulle,
        Heuristic::save_dulle_from_swines,
        Heuristic::poverty_contra_pfund_for_sure,
        Heuristic::poverty_contra_pfund_for_unsure,
        Heuristic::poverty_leave_to_partner,
        Heuristic::poverty_overjab_re,
        Heuristic::play_highest_color_card_in_game,
        Heuristic::jab_small_color_card,
        Heuristic::partner_backhand_draw_trump,
        Heuristic::jab_fox,
        Heuristic::play_for_partner_worries,
        Heuristic::start_with_trump_pfund,
        Heuristic::pfund_before_partner,
        Heuristic::last_player_jab_for_points,
        Heuristic::jab_color_over_fox,
        Heuristic::jab_with_relative_low_trump,
        Heuristic::serve_trump_trick,
        Heuristic::jab_with_highest_trump,
        Heuristic::serve_partner_with_lowest_card,
        Heuristic::start_with_lowest_color,
        Heuristic::start_with_lowest_trump,
        Heuristic::jab_with_lowest_card_below_trump_card_limit,
        Heuristic::serve_with_lowest_card,
        Heuristic::jab_with_lowest_card,
      };
  } // poverty

  { // solo
    { // color
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_color,
                                           HeuristicsMap::PlayerTypeGroup::re)]
        = { Heuristic::remaining_tricks_to_me,
          Heuristic::gametree,
          Heuristic::start_with_color_single_ace,
          Heuristic::start_with_color_double_ace,
          Heuristic::start_with_color_ten,
          Heuristic::jab_with_color_ace,
          Heuristic::jab_with_color_ten,
          Heuristic::color_jab_for_ace,
          Heuristic::serve_color_trick,
          Heuristic::play_highest_color_card_in_game,
          Heuristic::last_player_jab_with_trump_ace_or_ten,
          Heuristic::jab_small_color_card,
          Heuristic::save_dulle,
          Heuristic::save_dulle_from_swines,
          Heuristic::jab_first_color_run,
          Heuristic::jab_with_highest_trump,
          Heuristic::draw_remaining_trump,
          Heuristic::draw_trump,
          Heuristic::play_to_jab_later,
          Heuristic::jab_to_win,
          Heuristic::jab_to_not_loose,
          Heuristic::jab_with_relative_low_trump,
          Heuristic::serve_trump_trick,
          Heuristic::last_player_jab_for_points,
          Heuristic::start_with_lowest_trump,
          Heuristic::start_with_lowest_color,
          Heuristic::jab_with_lowest_card_below_trump_card_limit,
          Heuristic::jab_with_lowest_card,
          Heuristic::serve_with_lowest_card,
        };
      { // contra
        heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_color,
                                             HeuristicsMap::PlayerTypeGroup::contra)]
          = { Heuristic::remaining_tricks_to_me,
            Heuristic::play_last_trumps,
            Heuristic::gametree,
            Heuristic::start_with_color_single_ace,
            Heuristic::start_with_color_double_ace,
            Heuristic::jab_with_color_ace,
            Heuristic::jab_with_color_ten,
            Heuristic::play_color_for_partner_ace,
            Heuristic::start_with_color_ten,
            Heuristic::color_pfund_in_first_color_run,
            Heuristic::serve_color_trick,
            Heuristic::color_pfund_high_for_sure,
            Heuristic::color_overjab_to_get_partner_backhand,
            Heuristic::color_pfund_high_for_unsure,
            Heuristic::start_with_trump_pfund,
            Heuristic::last_player_jab_with_trump_ace_or_ten,
            Heuristic::jab_first_color_run,
            Heuristic::jab_second_color_run,
            Heuristic::save_dulle,
            Heuristic::save_dulle_from_swines,
            Heuristic::color_pfund_for_sure,
            Heuristic::pfund_before_partner,
            Heuristic::color_pfund_for_unsure,
            Heuristic::color_jab_for_ace,
            Heuristic::play_highest_color_card_in_game,
            Heuristic::jab_small_color_card,
            Heuristic::play_color_for_last_player_to_jab,
            Heuristic::play_color_for_partner_to_jab,
            Heuristic::play_color_in_solo,
            Heuristic::retry_color,
            Heuristic::jab_color_trick_high,
            Heuristic::create_blank_color,
            Heuristic::jab_to_win,
            Heuristic::jab_to_not_loose,
            Heuristic::last_player_pass_small_trick,
            Heuristic::cannot_jab,
            Heuristic::jab_with_highest_trump,
            Heuristic::partner_backhand_draw_trump,
            Heuristic::draw_remaining_trump,
            Heuristic::draw_trump,
            Heuristic::play_to_jab_later,
            Heuristic::start_with_low_color,
            Heuristic::play_for_partner_worries,
            Heuristic::play_trump,
            Heuristic::play_bad_color,
            Heuristic::last_player_jab_for_points,
            Heuristic::jab_color_over_fox,
            Heuristic::jab_with_relative_low_trump,
            Heuristic::serve_trump_trick,
            Heuristic::serve_partner_with_lowest_card,
            Heuristic::start_with_lowest_color,
            Heuristic::start_with_lowest_trump,
            Heuristic::jab_with_lowest_card_below_trump_card_limit,
            Heuristic::serve_with_lowest_card,
            Heuristic::jab_with_lowest_card,
          };
      } // contra

      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::marriage_silent,
                                           HeuristicsMap::PlayerTypeGroup::contra)]
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_color,
                                               HeuristicsMap::PlayerTypeGroup::contra)];
    } // color
    { // single picture
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_single_picture,
                                           HeuristicsMap::PlayerTypeGroup::re)]
        = { Heuristic::remaining_tricks_to_me,
          Heuristic::picture_get_first_trumps,
          Heuristic::picture_get_last_trumps,
          Heuristic::start_with_color_blank_ace,
          Heuristic::picture_draw_trumps,
          Heuristic::picture_serve_trump_trick,
          Heuristic::picture_pull_down_color,
          Heuristic::play_highest_color_card_in_game,
          Heuristic::start_with_color_double_ace,
          Heuristic::start_with_color_single_ace,
          Heuristic::start_with_color_ten,
          Heuristic::jab_with_color_ace,
          Heuristic::jab_with_color_ten,
          Heuristic::picture_start_with_highest_color,
          Heuristic::picture_play_last_trumps,
          Heuristic::jab_small_color_card,
          Heuristic::serve_color_trick,
          Heuristic::picture_jab_color_trick_for_sure,
          Heuristic::gametree,
          Heuristic::jab_to_win,
          Heuristic::jab_to_not_loose,
          Heuristic::last_player_jab_for_points,
          Heuristic::play_trump,
          Heuristic::create_blank_color,
          Heuristic::start_with_lowest_color,
          Heuristic::start_with_lowest_trump,
          Heuristic::jab_with_lowest_card_below_trump_card_limit,
          Heuristic::jab_with_lowest_card,
          Heuristic::serve_with_lowest_card,
        };
      { // contra
        heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_single_picture,
                                             HeuristicsMap::PlayerTypeGroup::contra)]
          = { Heuristic::remaining_tricks_to_me,
            Heuristic::picture_pull_down_color,
            Heuristic::gametree,
            Heuristic::start_with_color_single_ace,
            Heuristic::start_with_color_double_ace,
            Heuristic::jab_with_color_ace,
            Heuristic::jab_with_color_ten,
            Heuristic::play_color_for_partner_ace,
            Heuristic::start_with_color_ten,
            Heuristic::serve_color_trick,
            Heuristic::picture_serve_trump_trick,
            Heuristic::picture_jab_trump_trick,
            Heuristic::picture_pfund_high_for_sure,
            Heuristic::picture_pfund_for_sure,
            Heuristic::picture_pfund_high_for_unsure,
            Heuristic::picture_pfund_for_unsure,
            Heuristic::pfund_before_partner,
            Heuristic::play_highest_color_card_in_game,
            Heuristic::picture_play_last_trumps,
            Heuristic::jab_small_color_card,
            Heuristic::retry_color,
            Heuristic::jab_to_win,
            Heuristic::jab_to_not_loose,
            Heuristic::last_player_pass_small_trick,
            Heuristic::cannot_jab,
            Heuristic::draw_remaining_trump,
            Heuristic::draw_trump,
            Heuristic::play_to_jab_later,
            Heuristic::start_with_low_color,
            Heuristic::serve_trump_trick,
            Heuristic::play_trump,
            Heuristic::last_player_jab_for_points,
            Heuristic::serve_partner_with_lowest_card,
            Heuristic::start_with_lowest_color,
            Heuristic::start_with_lowest_trump,
            Heuristic::jab_with_lowest_card_below_trump_card_limit,
            Heuristic::serve_with_lowest_card,
            Heuristic::jab_with_lowest_card,
          };
      } // contra
    } // single picture
    { // double picture
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_double_picture,
                                           HeuristicsMap::PlayerTypeGroup::re)]
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_single_picture,
                                               HeuristicsMap::PlayerTypeGroup::re)];
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_double_picture,
                                           HeuristicsMap::PlayerTypeGroup::contra)]
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_single_picture,
                                               HeuristicsMap::PlayerTypeGroup::contra)];
    } // double picture
    { // koehler
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::solo_koehler,
                                           HeuristicsMap::PlayerTypeGroup::re)]
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_single_picture,
                                               HeuristicsMap::PlayerTypeGroup::re)];
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::solo_koehler,
                                           HeuristicsMap::PlayerTypeGroup::contra)]
        = heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::soli_single_picture,
                                               HeuristicsMap::PlayerTypeGroup::contra)];
    } // koehler
    { // meatless
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::solo_meatless,
                                           HeuristicsMap::PlayerTypeGroup::re)]
        = { Heuristic::remaining_tricks_to_me,
          Heuristic::meatless_pull_down_color,
          Heuristic::meatless_start_with_best_color,
          Heuristic::meatless_serve_color_trick,
          Heuristic::meatless_serve_trick_re,
          Heuristic::meatless_play_highest_color_card_in_game,
          Heuristic::jab_small_color_card,
          Heuristic::start_with_lowest_color,
          Heuristic::jab_with_lowest_card,
          Heuristic::serve_with_lowest_card,
        };
      heuristic_states_[HeuristicsMap::Key(HeuristicsMap::GameTypeGroup::solo_meatless,
                                           HeuristicsMap::PlayerTypeGroup::contra)]
        = { Heuristic::remaining_tricks_to_me,
          Heuristic::meatless_pull_down_color,
          Heuristic::meatless_play_blank_color_of_soloplayer,
          Heuristic::meatless_play_color_for_partner_ace,
          Heuristic::meatless_start_with_best_color,
          Heuristic::meatless_retry_last_color,
          Heuristic::meatless_serve_color_trick,
          Heuristic::meatless_serve_trick_contra,
          Heuristic::meatless_overjab_re,
          Heuristic::meatless_pfund_for_sure,
          Heuristic::meatless_pfund_before_partner,
          Heuristic::meatless_play_highest_color_card_in_game,
          Heuristic::jab_small_color_card,
          Heuristic::serve_partner_with_lowest_card,
          Heuristic::start_with_lowest_color,
          Heuristic::jab_with_lowest_card,
          Heuristic::serve_with_lowest_card,
        };
    } // meatless
  } // solo

#ifndef RELEASE
  for (auto const& heuristic_state : heuristic_states_) {
    auto const& key = heuristic_state.first;
    for (auto const& state : heuristic_state.second) {
      if (!::Heuristic::is_valid(state.heuristic, key.gametype_group, key.playertype_group)) {
        cerr << key.gametype_group << " / " << key.playertype_group << ": heuristic '" << state.heuristic << "' is not valid\n";
      }
    }
  }
#endif

  fill_up_heuristic_states();
  leave_update();
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::fill_up_heuristic_states()
{
  enter_update();

  for (auto& heuristic_state : heuristic_states_) {
    auto const& key = heuristic_state.first;
    auto& states = heuristic_state.second;

    for (auto const heuristic : aiconfig_heuristic_list) {
      auto const is_heuristic = [heuristic](HeuristicState const state) {
        return state.heuristic == heuristic;
      };
      if (   is_real(heuristic)
          && ::Heuristic::is_valid(heuristic, key.gametype_group, key.playertype_group)
          && !any_of(states, is_heuristic)) {
        states.emplace_back(heuristic, false);
      }
    }
  }

  leave_update();
}


auto Aiconfig::value(HeuristicsMap::Key const key,
                     Heuristic const heuristic) const -> bool
{
  auto const& states = heuristic_states(key);

  for (auto const& state : states) {
    if (state.heuristic == heuristic)
      return state.active;
  }

  return false;
}


auto Aiconfig::value(HeuristicsMap::GameTypeGroup const gametype_group,
                     HeuristicsMap::PlayerTypeGroup const playertype_group,
                     Heuristic const heuristic) const -> bool
{
  return value(HeuristicsMap::Key(gametype_group, playertype_group),
               heuristic);
}


auto Aiconfig::value(Type::Bool const type) const -> bool
{
  return bool_[type];
}


auto Aiconfig::value(Type::Int const type) const -> int
{
  return int_[type];
}


auto Aiconfig::value(Type::Card const type) const -> Card
{
  return card_[type];
}


auto Aiconfig::min(Type::Int const type) const -> int
{
  switch(type) {
  case Type::future_limit:
  case Type::last_tricks_without_heuristics:
  case Type::first_trick_for_trump_points_optimization:
    return 0;

  case Type::max_sec_wait_for_gametree:
    return 1;

  case Type::limit_throw_fehl:
  case Type::limitqueen:
  case Type::limitdulle:
    return 0;

  case Type::announcelimit:
  case Type::announcelimitdec:
  case Type::announceconfig:
  case Type::announcelimitreply:
  case Type::announceconfigreply:
    return 0;
  } // switch(type)

  return INT_MIN;
}


auto Aiconfig::max(Type::Int const type) const -> int
{
  switch(type) {
  case Type::last_tricks_without_heuristics:
  case Type::first_trick_for_trump_points_optimization:
    // when changing 'with nines' -> 'without nines' -> 'with nines'
    // it should keep '12'
    return static_cast<int>(::party->rule()(Rule::Type::max_number_of_tricks_in_game));

  case Type::limit_throw_fehl:
  case Type::limitqueen:
  case Type::limitdulle:
    return 33;

  case Type::announcelimit:
  case Type::announcelimitdec:
  case Type::announceconfig:
  case Type::announcelimitreply:
  case Type::announceconfigreply:
    return 1000;

  case Type::future_limit:
    return 1e9;
  case Type::max_sec_wait_for_gametree:
    return 5 * 60;
  } // switch(type)

  return INT_MAX;
}


auto Aiconfig::valid_cards(Type::Card const type) const -> vector<Card> const&
{
  switch(type) {
  case Type::limitthrowing:
  case Type::limithigh:

  case Type::trumplimit_normal:
  case Type::lowest_trumplimit_normal:
  case Type::trumplimit_solocolor:
  case Type::lowest_trumplimit_solocolor:
    {
      static vector<Card> valid_cards
        = { Card::club_queen,
          Card::spade_queen,
          Card::heart_queen,
          Card::diamond_queen,
          Card::club_jack,
          Card::spade_jack,
          Card::heart_jack,
          Card::diamond_jack,
        };

      return valid_cards;
    }
  case Type::trumplimit_solojack:
  case Type::lowest_trumplimit_solojack:
    {
      static vector<Card> valid_cards
        = { Card::club_jack,
          Card::spade_jack,
          Card::heart_jack,
          Card::diamond_jack,
        };

      return valid_cards;
    }
  case Type::trumplimit_soloqueen:
  case Type::lowest_trumplimit_soloqueen:
    {
      static vector<Card> valid_cards
        = { Card::club_queen,
          Card::spade_queen,
          Card::heart_queen,
          Card::diamond_queen,
        };

      return valid_cards;
    }
  case Type::trumplimit_soloking:
  case Type::lowest_trumplimit_soloking:
    {
      static vector<Card> valid_cards
        = { Card::club_king,
          Card::spade_king,
          Card::heart_king,
          Card::diamond_king,
        };

      return valid_cards;
    }
  case Type::trumplimit_solojackking:
  case Type::lowest_trumplimit_solojackking:
    {
      static vector<Card> valid_cards
        = { Card::club_king,
          Card::spade_king,
          Card::heart_king,
          Card::diamond_king,
          Card::club_jack,
          Card::spade_jack,
          Card::heart_jack,
          Card::diamond_jack,
        };

      return valid_cards;
    }
  case Type::trumplimit_solojackqueen:
  case Type::lowest_trumplimit_solojackqueen:
    {
      static vector<Card> valid_cards
        = { Card::club_queen,
          Card::spade_queen,
          Card::heart_queen,
          Card::diamond_queen,
          Card::club_jack,
          Card::spade_jack,
          Card::heart_jack,
          Card::diamond_jack,
        };

      return valid_cards;
    }
  case Type::trumplimit_soloqueenking:
  case Type::lowest_trumplimit_soloqueenking:
    {
      static vector<Card> valid_cards
        = { Card::club_king,
          Card::spade_king,
          Card::heart_king,
          Card::diamond_king,
          Card::club_queen,
          Card::spade_queen,
          Card::heart_queen,
          Card::diamond_queen,
        };

      return valid_cards;
    }
  case Type::trumplimit_solokoehler:
  case Type::lowest_trumplimit_solokoehler:
    {
      static vector<Card> valid_cards
        = { Card::club_king,
          Card::spade_king,
          Card::heart_king,
          Card::diamond_king,
          Card::club_queen,
          Card::spade_queen,
          Card::heart_queen,
          Card::diamond_queen,
          Card::club_jack,
          Card::spade_jack,
          Card::heart_jack,
          Card::diamond_jack,
        };

      return valid_cards;
    }
  case Type::trumplimit_meatless:
  case Type::lowest_trumplimit_meatless:
    {
      static vector<Card> valid_cards
        = { Card::diamond_ace,
          Card::diamond_ten,
          Card::diamond_king,
          Card::diamond_queen,
          Card::diamond_jack,
          Card::diamond_nine,
        };

      return valid_cards;
    }
  } // switch(type)

  {
    static vector<Card> valid_cards;
    return valid_cards;
  }
}


auto Aiconfig::set(string const type, string const value) -> bool
{
#ifdef DEPRECATED
  // 0.7.6 replacing
  if (type == "fairplay hands") {
    set(Type::teams_known, !(   (value == "true")
                             || (value == "yes")
                             || (value == "1") ) );
    return true;
  }
  if (type == "fairplay teams") {
    set(Type::hands_known, !(   (value == "true")
                             || (value == "yes")
                             || (value == "1")) );
    return true;
  }
#endif
  for (auto const t : Aiconfig::Type::bool_list) {
    if (type == to_string(t)) {
      set(t, value);
      return true;
    }
  }
  for (auto const t : Aiconfig::Type::int_list) {
    if (type == to_string(t)) {
      set(t, value);
      return true;
    }
  }
  for (auto const t : Aiconfig::Type::card_list) {
    if (type == to_string(t)) {
      set(t, value);
      return true;
    }
  }

  return false;
}


void Aiconfig::set(HeuristicsMap::GameTypeGroup const gametype_group,
                   HeuristicsMap::PlayerTypeGroup const playertype_group,
                   Heuristic const heuristic,
                   string const value)
{
  if (   (value != "true")
      && (value != "false")
      && (value != "yes")
      && (value != "no")
      && (value != "0")
      && (value != "1")) {
    cerr << "Aiconfig::set(GameTypeGroup, PlayerTypeGroup, Heuristic, value)\n"
      << "  illegal value '" << value << "' for '" << heuristic << "', "
      << "must be a boolean ('true' or 'false' or 'yes' or 'no' or '1' or '0')."
      << '\n'
      << "  Taking 'false'."
      << endl;
  }
  set(gametype_group, playertype_group, heuristic,
      (   (value == "true")
       || (value == "yes")
       || (value == "1")) );
}


void Aiconfig::set(Type::Bool const type, string const value)
{
  if (   (value != "true")
      && (value != "false")
      && (value != "yes")
      && (value != "no")
      && (value != "0")
      && (value != "1")) {
    cerr << "Aiconfig::set(Bool, value)\n"
      << "  illegal value '" << value << "' for '" << type << "', "
      << "must be a boolean ('true' or 'false' or 'yes' or 'no' or '1' or '0')."
      << '\n'
      << "  Taking 'false'."
      << endl;
  }
  set(type, (  (   (value == "false")
                || (value == "no")
                || (value == "0") )
             ? false
             : true) );
}


void Aiconfig::set(Type::Int const type, string const value)
{
  char* end_ptr = nullptr;
  unsigned number = strtoul(value.c_str(), &end_ptr, 0);
  if (*end_ptr != '\0') {
    cerr << "Aiconfig::set(Int, value)\n"
      << "illegal value '" << value << "' for '" << type << "', "
      << "must be a digit.\n"
      << "  Taking " << number << "."
      << endl;
  }

  set(type, number);
}


void Aiconfig::set(Type::Card const type, string const value)
{
  set(type, Card(value));
}



void Aiconfig::set(HeuristicsMap::Key const key,
                   Heuristic const heuristic,
                   bool const value)
{
  set(key.gametype_group, key.playertype_group, heuristic, value);
}


void Aiconfig::set(HeuristicsMap::GameTypeGroup const gametype_group,
                   HeuristicsMap::PlayerTypeGroup const playertype_group,
                   Heuristic const heuristic,
                   bool const value)
{
  enter_update();
  auto& states = heuristic_states(gametype_group, playertype_group);

  for (auto& state : states)
    if (state.heuristic == heuristic) {
      state.active = value;
      return;
    }
  states.emplace_back(heuristic, value);

  leave_update();
}


void Aiconfig::set_to_default(HeuristicsMap::Key const key)
{
  enter_update();
  heuristic_states(key)
    = heuristic_states(HeuristicsMap::Key());
  leave_update();
}


void Aiconfig::move(HeuristicsMap::Key const key,
                    Heuristic const heuristic,
                    unsigned const pos)
{
  auto& states = heuristic_states(key);

  if (states[pos].heuristic == heuristic)
    return ;

  if (pos > states.size())
    return ;

  enter_update();

  vector<HeuristicState>::iterator s;
  for (s = states.begin();
       s != states.end();
       ++s)
    if (s->heuristic == heuristic)
      break;

  if (s == states.end()) {
    states.insert(states.begin() + pos, HeuristicState(heuristic, false));
  } else { // if !(s == states.end())
    HeuristicState const state = *s;
    states.erase(s);
    states.insert(states.begin() + pos, state);
  } // if !(s == states.end())

  leave_update();
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::set(Type::Bool const type, bool const value)
{
  enter_update();
  bool_[type] = value;
  leave_update();
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::set(Type::Int const type, int const value)
{
  enter_update();
  int_[type] = value;
  leave_update();
}


// NOLINTNEXTLINE(misc-no-recursion)
void Aiconfig::set(Type::Card const type, Card const value)
{
  enter_update();
  card_[type] = value;
  leave_update();
}


void Aiconfig::write(ostream& ostr) const
{
  if (difficulty() != Difficulty::custom) {
    ostr << "difficulty = " << to_string(difficulty()) << '\n';
    return ;
  } // if (difficulty() != custom)

  auto const difficulty = difficulty_with_min_differences();
  ostr << "difficulty = " << to_string(difficulty) << '\n';
  write_differences(ostr, difficulty);
}


auto Aiconfig::difficulty_with_min_differences() const -> Aiconfig::Difficulty
{
  Difficulty difficulty_min = Difficulty::custom;
  unsigned differences_min = UINT_MAX;
  for (auto const difficulty : aiconfig_difficulty_list) {
    if (difficulty == Difficulty::custom)
      continue;
    auto const differences = count_differences(difficulty);
    if (differences < differences_min) {
      difficulty_min = difficulty;
      differences_min = differences;
    }
  }
  return difficulty_min;
}


auto Aiconfig::count_differences(Difficulty difficulty) const -> unsigned
{
  Aiconfig const& preset_aiconfig = Aiconfig::preset(difficulty);
  // the number of differences
  unsigned differences = 0;

  { // check the values
    for (auto const type : Aiconfig::Type::bool_list) {
      if (value(type) != preset_aiconfig.value(type)) {
        differences += 1;
      }
    }
    for (auto const type : Aiconfig::Type::int_list) {
      if (value(type) != preset_aiconfig.value(type)) {
        differences += 1;
      }
    }
    for (auto const type : Aiconfig::Type::card_list) {
      if (value(type) != preset_aiconfig.value(type)) {
        differences += 1;
      }
    }
  } // check the values
  if (rating() != preset_aiconfig.rating()) {
    differences += 1;
  }
  { // check the heuristics
    for (auto const key : Aiconfig::keys()) {
      if (heuristic_states(key)
          != preset_aiconfig.heuristic_states(key)) {
        differences += 5;
      }
    }
  } // check the heuristics
  return differences;
}


void Aiconfig::write_differences(ostream& ostr,
                                 Aiconfig::Difficulty const difficulty) const
{
  std::ios_base::fmtflags const flags = ostr.flags();
  ostr << std::boolalpha;

  Aiconfig const& preset_aiconfig = Aiconfig::preset(difficulty);

  { // check the values
    for (auto const type : Aiconfig::Type::bool_list) {
      if (value(type) != preset_aiconfig.value(type)) {
        ostr << "  "
          << type << " = "
          << (value(type) ? "true" : "false")
          << '\n';
      }
    }
    for (auto const type : Aiconfig::Type::int_list) {
      if (value(type) != preset_aiconfig.value(type)) {
        ostr << "  "
          << type << " = "
          << value(type)
          << '\n';
      }
    }
    for (auto const type : Aiconfig::Type::card_list) {
      if (value(type) != preset_aiconfig.value(type)) {
        ostr << "  "
          << type << " = "
          << value(type)
          << '\n';
      }
    }
  } // check the values
  if (rating() != preset_aiconfig.rating()) {
    ostr << "  "
      << "rating       = " << rating() << '\n';
  }
  { // check the heuristics
    auto const& keys = Aiconfig::keys();
    bool first_difference = true;

    for (auto const key : keys) {
      if (heuristic_states(key)
          != preset_aiconfig.heuristic_states(key)) {
        { // write the differences
          if (first_difference) {
            ostr << "heuristics" << '\n'
              << "{\n";
            first_difference = false;
          } // if (first_difference)

          ostr << "  "
            << key.gametype_group << " - "
            << key.playertype_group << '\n'
            << "  {\n";
          for (auto const h : heuristic_states(key)) {
            ostr
              << "    "
              << h.heuristic << " = "
              << (h.active ? "true" : "false") << '\n';
          }
          ostr << "  }\n";
        } // write the differences
      } // if (heuristic states differ)
    } // for (key : keys)

    if (!first_difference) {
      // there was a difference
      ostr << "}\n";
    } // if (!first_difference)

  } // check the heuristics

  ostr.flags(flags);
}


void Aiconfig::write_full(ostream& ostr) const
{
  ostr << "# difficulty = " << to_string(difficulty()) << '\n';
  std::ios_base::fmtflags const flags = ostr.flags();
  ostr << std::boolalpha;

  ostr << "rating       = " << rating() << '\n';
  ostr << '\n';
  ostr << '\n';

  ostr << "# bool\n";
  for (auto const type : Aiconfig::Type::bool_list) {
    ostr << type << " = " << (value(type) ? "true" : "false") << '\n';
  }
  ostr << '\n';

  ostr << "# unsigned\n";
  for (auto const type : Aiconfig::Type::int_list) {
    ostr << type << " = " << value(type) << '\n';
  }
  ostr << '\n';

  ostr << "# card\n";
  for (auto const type : Aiconfig::Type::card_list) {
    ostr << type << " = " << value(type) << '\n';
  }

  ostr << '\n';
  ostr << "#heuristics" << '\n';
  ostr << "heuristics" << '\n'
    << "{\n";
  for (auto const& heuristic_state : heuristic_states_) {
    ostr << "  "
      << heuristic_state.first.gametype_group << " - " << heuristic_state.first.playertype_group << '\n'
      << "  {\n";
    for (auto const& h : heuristic_state.second)
      ostr << "    " << h.heuristic << " = " << (h.active ? "true" : "false") << '\n';
    ostr << "  }\n";
  } // for (s \in heuristic_states_)
  ostr << "}\n";

  ostr.flags(flags);
}


auto Aiconfig::load(Path const path) -> bool
{
  ifstream istr(path);
  if (!istr.good())
    return false;
  return read(istr);
}


auto Aiconfig::read(istream& istr) -> bool
{
  enter_update();

  unsigned depth = 0;

  reset_to_hardcoded();

  // read the configuration
  while (istr.good()) {
    Config config;
    istr >> config;
    if (!istr.good())
      break;

    // finished with the config file
    if (config.empty())
      break;

    if (config.separator) {
      // a setting
      if (config.name == "difficulty") {
        try {
          set_to_difficulty(aiconfig_difficulty_from_string(config.value));
        } catch (std::ios_base::failure const& error) {
          cerr << "Aiconfig::read()\n"
            << error.what()
            << endl;
        }
      } else if (config.name == "rating") {
        try {
          auto const r = Rating::type_from_string(config.value);
          set_rating(r);
        } catch (string const& value) {
          cerr << "rating type: "
            << "unknown rating type '" << value << "'."
            << endl;
        }
      } else {
        if (!set(config.name, config.value)) {
          cerr << "aiconfig: "
            << "unknown config '" << config.name << "'."
            << endl;
        }
      } // if (config.name == "type")
    } else { // if (config.separator)
      // a setting
      // if the value is in parentencies, remove both
      if(config.name == "!end") {
        // ignore the rest of the file
        break;
      } else if(config.name == "!stdout") {
        // output of the data to 'stdout'
        cout << config.value << endl;
      } else if(config.name == "!stderr") {
        // output of the data to 'stderr'
        cerr << config.value << endl;
      } else if(config.name == "{") {
        depth += 1;
      } else if(config.name == "}") {
        if (depth == 0) {
          cerr << "Aiconfig: found a '}' without a '{' before.\n"
            << "Finish reading the the file."
            << endl;
          break;
        } // if (depth == 0)
        depth -= 1;
        if (depth == 0)
          break;
      } else if (config.name == "heuristics") {
        read_heuristics(istr);
      } else if (config.name.empty()) {
        cerr << "Aiconfig: "
          << "Ignoring line \'" << config.value << "\'.\n";
      } else {
        cerr << "Aiconfig: "
          << "type '" << config.name << "' unknown.\n"
          << "Ignoring it.\n";
      } // if (config.name == .)
    } // config.separator
  } // while (istr.good())

  leave_update();
  return istr.good();
}


auto Aiconfig::read_heuristics(istream& istr) -> bool
{
  enter_update();
  unsigned depth = 0;
  string line;
  while (istr.good()) {
    std::getline(istr, line);
    if (!istr.good())
      break;

    while (   !line.empty()
           && isspace(*line.begin()))
      line.erase(line.begin());

    if (*(line.end() - 1) == '\r')
      line.erase(line.end() - 1);

    if (line.empty())          { // empty line
    } else if (line[0] == '#') { // comment
    } else if (line == "{")    { // block open
      depth += 1;
    } else if (line == "}")    { // block close
      if (depth == 0) {
        cerr << "Aiconfig: found a '}' without a '{' before.\n"
          << "Finish reading the the file."
          << endl;
        break;
      } // if (depth == 0)
      depth -= 1;
      if (depth == 0)
        break;
    } else                     { // gametype group - playertype group
      string::size_type const pos = line.find('-');
      if (pos == string::npos) {
        cerr << "unknown line '" << line << "'\n"
          << "ignoring it" << endl;
        continue;
      } // if (pos == string::npos)

      string gametype_group_string(line, 0, pos);
      while (   !gametype_group_string.empty()
             && isspace(*gametype_group_string.rbegin()))
        gametype_group_string.erase(gametype_group_string.size() - 1);
      string playertype_group_string(line, pos + 1, string::npos);
      while (   !playertype_group_string.empty()
             && isspace(*playertype_group_string.begin()))
        playertype_group_string.erase(playertype_group_string.begin());

      HeuristicsMap::GameTypeGroup const gametype_group
        = HeuristicsMap::GameTypeGroup_from_string(gametype_group_string);
      HeuristicsMap::PlayerTypeGroup const playertype_group
        = HeuristicsMap::PlayerTypeGroup_from_string(playertype_group_string);

      unsigned depth2 = 0;
      vector<HeuristicState>& states
        = heuristic_states_[HeuristicsMap::Key(gametype_group,
                                               playertype_group)];
      states.clear();
      while (istr.good()) {
        Config config;
        istr >> config;
        if (!istr.good())
          break;

        if (config.separator) {
          Heuristic const heuristic = Heuristic_from_name(config.name);
          if (is_real(heuristic)) {
            bool const active = (   (config.value == "true")
                                 || (config.value == "yes")
                                 || (config.value == "1"));
            states.emplace_back(heuristic, active);
          } // if (is_real(heuristic))
        } else { // if (config.separator)
          if(config.name == "{") {
            depth2 += 1;
          } else if(config.name == "}") {
            if (depth2 == 0) {
              cerr << "Aiconfig: found a '}' without a '{' before.\n"
                << "Finish reading the the file."
                << endl;
              break;
            } // if (depth2 == 0)
            depth2 -= 1;
            if (depth2 == 0)
              break;
          } else if (config.name.empty()) {
            cerr << "Aiconfig: "
              << "Ignoring line \'" << config.value << "\'.\n";
          } else {
            cerr << "Aiconfig: "
              << "type '" << config.name << "' unknown.\n"
              << "Ignoring it.\n";
          } // if (config.name == .)
        } // config.separator
      } // while (istr.good())
    } // if (line == ...)
  } // while (istr.good())

  fill_up_heuristic_states();

  leave_update();
  return istr.good();
}


auto Aiconfig::save(Path const path) const -> bool
{
  auto path_tmp = path;
  path_tmp += ".tmp";
  ofstream ostr(path_tmp);
  if (!ostr.good()) {
    ::ui->information(_("Error::Aiconfig::save: Error opening temporary file %s. Aborting saving.", path_tmp.string()), InformationType::problem);
    return false;
  }

  ostr << "# FreeDoko ai configuration (" << *::version << ")\n"
    << '\n';
  write(ostr);

  if (!ostr.good()) {
    ::ui->information(_("Error::Aiconfig::save: Error saving in temporary file %s. Keeping temporary file (for bug tracking).", path_tmp.string()), InformationType::problem);
    return false;
  }
  ostr.close();

  try {
    if (is_regular_file(path))
      remove(path);
    rename(path_tmp, path);
  } catch (std::filesystem::filesystem_error const& error) {
    ::ui->information(_("Error::Aiconfig::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string())
                      + "\n"
                      + _("Error message: %s", error.what()),
                      InformationType::problem);
    return false;
  } catch (...) {
    ::ui->information(_("Error::Aiconfig::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string()), InformationType::problem);
    return false;
  }

  return true;
}


auto operator==(Aiconfig const& lhs, Aiconfig const& rhs) -> bool
{
  return lhs.equal(rhs);
}


auto operator!=(Aiconfig const& lhs, Aiconfig const& rhs) -> bool
{
  return !(lhs == rhs);
}


auto operator==(Aiconfig::HeuristicState const lhs,
                Aiconfig::HeuristicState const rhs) -> bool
{
  return (   (lhs.heuristic == rhs.heuristic)
          && (lhs.active == rhs.active) );
}


auto operator<<(ostream& ostr, Aiconfig const& aiconfig) -> ostream&
{
  aiconfig.write(ostr);

  return ostr;
}
