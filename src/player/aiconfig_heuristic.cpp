/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "aiconfig_heuristic.h"


auto to_string(AiconfigHeuristic const heuristic) -> string
{
  switch(heuristic) {
  case AiconfigHeuristic::error:
    (void)_("AiConfig::Heuristic::error");
    (void)_("AiConfig::Heuristic::Description::error");
    return "error";
  case AiconfigHeuristic::no_heuristic:
    (void)_("AiConfig::Heuristic::no heuristic");
    (void)_("AiConfig::Heuristic::Description::no heuristic");
    return "no heuristic";
  case AiconfigHeuristic::manual:
    (void)_("AiConfig::Heuristic::manual");
    (void)_("AiConfig::Heuristic::Description::manual");
    return "manual";
  case AiconfigHeuristic::bug_report:
    (void)_("AiConfig::Heuristic::bug report");
    (void)_("AiConfig::Heuristic::Description::bug report");
    return "bug report";
  case AiconfigHeuristic::only_one_valid_card:
    (void)_("AiConfig::Heuristic::only one valid card");
    (void)_("AiConfig::Heuristic::Description::only one valid card");
    return "only one valid card";
  case AiconfigHeuristic::valid_card:
    (void)_("AiConfig::Heuristic::valid card");
    (void)_("AiConfig::Heuristic::Description::valid card");
    return "valid card";
  case AiconfigHeuristic::gametree:
    (void)_("AiConfig::Heuristic::gametree");
    (void)_("AiConfig::Heuristic::Description::gametree");
    return "gametree";
  case AiconfigHeuristic::remaining_tricks_to_me:
    (void)_("AiConfig::Heuristic::remaining tricks to me");
    (void)_("AiConfig::Heuristic::Description::remaining tricks to me");
    return "remaining tricks to me";
  case AiconfigHeuristic::start_with_color_blank_ace:
    (void)_("AiConfig::Heuristic::start with color blank ace");
    (void)_("AiConfig::Heuristic::Description::start with color blank ace");
    return "start with color blank ace";
  case AiconfigHeuristic::start_with_color_single_ace:
    (void)_("AiConfig::Heuristic::start with color single ace");
    (void)_("AiConfig::Heuristic::Description::start with color single ace");
    return "start with color single ace";
  case AiconfigHeuristic::start_with_color_double_ace:
    (void)_("AiConfig::Heuristic::start with color double ace");
    (void)_("AiConfig::Heuristic::Description::start with color double ace");
    return "start with color double ace";
  case AiconfigHeuristic::start_with_color_ten:
    (void)_("AiConfig::Heuristic::start with color ten");
    (void)_("AiConfig::Heuristic::Description::start with color ten");
    return "start with color ten";
  case AiconfigHeuristic::start_with_lowest_color:
    (void)_("AiConfig::Heuristic::start with lowest color");
    (void)_("AiConfig::Heuristic::Description::start with lowest color");
    return "start with lowest color";
  case AiconfigHeuristic::start_with_lowest_trump:
    (void)_("AiConfig::Heuristic::start with lowest trump");
    (void)_("AiConfig::Heuristic::Description::start with lowest trump");
    return "start with lowest trump";
  case AiconfigHeuristic::jab_with_color_ace:
    (void)_("AiConfig::Heuristic::jab with color ace");
    (void)_("AiConfig::Heuristic::Description::jab with color ace");
    return "jab with color ace";
  case AiconfigHeuristic::jab_with_color_ten:
    (void)_("AiConfig::Heuristic::jab with color ten");
    (void)_("AiConfig::Heuristic::Description::jab with color ten");
    return "jab with color ten";
  case AiconfigHeuristic::jab_first_color_run:
    (void)_("AiConfig::Heuristic::jab first color run");
    (void)_("AiConfig::Heuristic::Description::jab first color run");
    return "jab first color run";
  case AiconfigHeuristic::jab_second_color_run:
    (void)_("AiConfig::Heuristic::jab second color run");
    (void)_("AiConfig::Heuristic::Description::jab second color run");
    return "jab second color run";
  case AiconfigHeuristic::jab_small_color_card:
    (void)_("AiConfig::Heuristic::jab small color card");
    (void)_("AiConfig::Heuristic::Description::jab small color card");
    return "jab small color card";
  case AiconfigHeuristic::jab_color_over_fox:
    (void)_("AiConfig::Heuristic::jab color over fox");
    (void)_("AiConfig::Heuristic::Description::jab color over fox");
    return "jab color over fox";
  case AiconfigHeuristic::jab_color_trick_high:
    (void)_("AiConfig::Heuristic::jab color trick high");
    (void)_("AiConfig::Heuristic::Description::jab color trick high");
    return "jab color trick high";
  case AiconfigHeuristic::jab_rich_trick:
    (void)_("AiConfig::Heuristic::jab rich trick");
    (void)_("AiConfig::Heuristic::Description::jab rich trick");
    return "jab rich trick";
  case AiconfigHeuristic::jab_trump_over_fox:
    (void)_("AiConfig::Heuristic::jab trump over fox");
    (void)_("AiConfig::Heuristic::Description::jab trump over fox");
    return "jab trump over fox";
  case AiconfigHeuristic::jab_trump_because_of_partner_announcement:
    (void)_("AiConfig::Heuristic::jab trump because of partner announcement");
    (void)_("AiConfig::Heuristic::Description::jab trump because of partner announcement");
    return "jab trump because of partner announcement";
  case AiconfigHeuristic::jab_trump_high_because_of_partner_announcement:
    (void)_("AiConfig::Heuristic::jab trump high because of partner announcement");
    (void)_("AiConfig::Heuristic::Description::jab trump high because of partner announcement");
    return "jab trump high because of partner announcement";
  case AiconfigHeuristic::jab_trump_high_after_opponent_announcement:
    (void)_("AiConfig::Heuristic::jab trump high after opponent announcement");
    (void)_("AiConfig::Heuristic::Description::jab trump high after opponent announcement");
    return "jab trump high after opponent announcement";
  case AiconfigHeuristic::jab_with_highest_trump:
    (void)_("AiConfig::Heuristic::jab with highest trump");
    (void)_("AiConfig::Heuristic::Description::jab with highest trump");
    return "jab with highest trump";
  case AiconfigHeuristic::jab_with_lowest_card:
    (void)_("AiConfig::Heuristic::jab with lowest card");
    (void)_("AiConfig::Heuristic::Description::jab with lowest card");
    return "jab with lowest card";
  case AiconfigHeuristic::jab_with_lowest_card_below_trump_card_limit:
    (void)_("AiConfig::Heuristic::jab with lowest card below trump card limit");
    (void)_("AiConfig::Heuristic::Description::jab with lowest card below trump card limit");
    return "jab with lowest card below trump card limit";
  case AiconfigHeuristic::jab_with_relative_low_trump:
    (void)_("AiConfig::Heuristic::jab with relative low trump");
    (void)_("AiConfig::Heuristic::Description::jab with relative low trump");
    return "jab with relative low trump";
  case AiconfigHeuristic::high_after_partner_low:
    (void)_("AiConfig::Heuristic::high after partner low");
    (void)_("AiConfig::Heuristic::Description::high after partner low");
    return "high after partner low";
  case AiconfigHeuristic::second_last_jab_for_following_pfund:
    (void)_("AiConfig::Heuristic::second last jab for following pfund");
    (void)_("AiConfig::Heuristic::Description::second last jab for following pfund");
    return "second last jab for following pfund";
  case AiconfigHeuristic::last_player_jab_with_trump_ace_or_ten:
    (void)_("AiConfig::Heuristic::last player jab with trump ace or ten");
    (void)_("AiConfig::Heuristic::Description::last player jab with trump ace or ten");
    return "last player jab with trump ace or ten";
  case AiconfigHeuristic::last_player_jab_for_points:
    (void)_("AiConfig::Heuristic::last player jab for points");
    (void)_("AiConfig::Heuristic::Description::last player jab for points");
    return "last player jab for points";
  case AiconfigHeuristic::jab_with_opponent_backhand:
    (void)_("AiConfig::Heuristic::jab with opponent backhand");
    (void)_("AiConfig::Heuristic::Description::jab with opponent backhand");
    return "jab with opponent backhand";
  case AiconfigHeuristic::serve_with_lowest_card:
    (void)_("AiConfig::Heuristic::serve with lowest card");
    (void)_("AiConfig::Heuristic::Description::serve with lowest card");
    return "serve with lowest card";
  case AiconfigHeuristic::serve_partner_with_lowest_card:
    (void)_("AiConfig::Heuristic::serve partner with lowest card");
    (void)_("AiConfig::Heuristic::Description::serve partner with lowest card");
    return "serve partner with lowest card";
  case AiconfigHeuristic::start_with_color:
    (void)_("AiConfig::Heuristic::start with color");
    (void)_("AiConfig::Heuristic::Description::start with color");
    return "start with color";
  case AiconfigHeuristic::start_with_low_color:
    (void)_("AiConfig::Heuristic::start with low color");
    (void)_("AiConfig::Heuristic::Description::start with low color");
    return "start with low color";
  case AiconfigHeuristic::start_with_low_trump:
    (void)_("AiConfig::Heuristic::start with low trump");
    (void)_("AiConfig::Heuristic::Description::start with low trump");
    return "start with low trump";
  case AiconfigHeuristic::retry_color:
    (void)_("AiConfig::Heuristic::retry color");
    (void)_("AiConfig::Heuristic::Description::retry color");
    return "retry color";
  case AiconfigHeuristic::start_with_trump_for_partner_color_ace:
    (void)_("AiConfig::Heuristic::start with trump for partner color ace");
    (void)_("AiConfig::Heuristic::Description::start with trump for partner color ace");
    return "start with trump for partner color ace";
  case AiconfigHeuristic::play_color_for_partner_to_jab:
    (void)_("AiConfig::Heuristic::play color for partner to jab");
    (void)_("AiConfig::Heuristic::Description::play color for partner to jab");
    return "play color for partner to jab";
  case AiconfigHeuristic::play_color_for_last_player_to_jab:
    (void)_("AiConfig::Heuristic::play color for last player to jab");
    (void)_("AiConfig::Heuristic::Description::play color for last player to jab");
    return "play color for last player to jab";
  case AiconfigHeuristic::play_color_for_partner_ace:
    (void)_("AiConfig::Heuristic::play color for partner ace");
    (void)_("AiConfig::Heuristic::Description::play color for partner ace");
    return "play color for partner ace";
  case AiconfigHeuristic::play_bad_color:
    (void)_("AiConfig::Heuristic::play bad color");
    (void)_("AiConfig::Heuristic::Description::play bad color");
    return "play bad color";
  case AiconfigHeuristic::save_dulle:
    (void)_("AiConfig::Heuristic::save dulle");
    (void)_("AiConfig::Heuristic::Description::save dulle");
    return "save dulle";
  case AiconfigHeuristic::save_dulle_from_swines:
    (void)_("AiConfig::Heuristic::save dulle from swines");
    (void)_("AiConfig::Heuristic::Description::save dulle from swines");
    return "save dulle from swines";
  case AiconfigHeuristic::pfund_in_first_color_run:
    (void)_("AiConfig::Heuristic::pfund in first color run");
    (void)_("AiConfig::Heuristic::Description::pfund in first color run");
    return "pfund in first color run";
  case AiconfigHeuristic::serve_color_trick:
    (void)_("AiConfig::Heuristic::serve color trick");
    (void)_("AiConfig::Heuristic::Description::serve color trick");
    return "serve color trick";
  case AiconfigHeuristic::serve_trump_trick:
    (void)_("AiConfig::Heuristic::serve trump trick");
    (void)_("AiConfig::Heuristic::Description::serve trump trick");
    return "serve trump trick";
  case AiconfigHeuristic::pfund_high_for_sure:
    (void)_("AiConfig::Heuristic::pfund high for sure");
    (void)_("AiConfig::Heuristic::Description::pfund high for sure");
    return "pfund high for sure";
  case AiconfigHeuristic::pfund_for_sure:
    (void)_("AiConfig::Heuristic::pfund for sure");
    (void)_("AiConfig::Heuristic::Description::pfund for sure");
    return "pfund for sure";
  case AiconfigHeuristic::pfund_high_for_unsure:
    (void)_("AiConfig::Heuristic::pfund high for unsure");
    (void)_("AiConfig::Heuristic::Description::pfund high for unsure");
    return "pfund high for unsure";
  case AiconfigHeuristic::pfund_for_unsure:
    (void)_("AiConfig::Heuristic::pfund for unsure");
    (void)_("AiConfig::Heuristic::Description::pfund for unsure");
    return "pfund for unsure";
  case AiconfigHeuristic::start_with_trump_pfund:
    (void)_("AiConfig::Heuristic::start with trump pfund");
    (void)_("AiConfig::Heuristic::Description::start with trump pfund");
    return "start with trump pfund";
  case AiconfigHeuristic::pfund_before_partner:
    (void)_("AiConfig::Heuristic::choose pfund before partner");
    (void)_("AiConfig::Heuristic::Description::choose pfund before partner");
    return "choose pfund before partner";
  case AiconfigHeuristic::jab_for_ace:
    (void)_("AiConfig::Heuristic::jab for ace");
    (void)_("AiConfig::Heuristic::Description::jab for ace");
    return "jab for ace";
  case AiconfigHeuristic::create_blank_color:
    (void)_("AiConfig::Heuristic::create blank color");
    (void)_("AiConfig::Heuristic::Description::create blank color");
    return "create blank color";
  case AiconfigHeuristic::play_trump:
    (void)_("AiConfig::Heuristic::play trump");
    (void)_("AiConfig::Heuristic::Description::play trump");
    return "play trump";
  case AiconfigHeuristic::jab_fox:
    (void)_("AiConfig::Heuristic::jab fox");
    (void)_("AiConfig::Heuristic::Description::jab fox");
    return "jab fox";
  case AiconfigHeuristic::jab_for_doppelkopf:
    (void)_("AiConfig::Heuristic::jab for doppelkopf");
    (void)_("AiConfig::Heuristic::Description::jab for doppelkopf");
    return "jab for doppelkopf";
  case AiconfigHeuristic::try_for_doppelkopf:
    (void)_("AiConfig::Heuristic::try for doppelkopf");
    (void)_("AiConfig::Heuristic::Description::try for doppelkopf");
    return "try for doppelkopf";
  case AiconfigHeuristic::play_for_partner_worries:
    (void)_("AiConfig::Heuristic::play for partner worries");
    (void)_("AiConfig::Heuristic::Description::play for partner worries");
    return "play for partner worries";
  case AiconfigHeuristic::partner_backhand_draw_trump:
    (void)_("AiConfig::Heuristic::partner backhand draw trump");
    (void)_("AiConfig::Heuristic::Description::partner backhand draw trump");
    return "partner backhand draw trump";
  case AiconfigHeuristic::draw_trump:
    (void)_("AiConfig::Heuristic::draw trump");
    (void)_("AiConfig::Heuristic::Description::draw trump");
    return "draw trump";
  case AiconfigHeuristic::draw_remaining_trump:
    (void)_("AiConfig::Heuristic::draw remaining trump");
    (void)_("AiConfig::Heuristic::Description::draw remaining trump");
    return "draw remaining trump";
  case AiconfigHeuristic::draw_trump_for_team_announcement:
    (void)_("AiConfig::Heuristic::draw trump for team announcement");
    (void)_("AiConfig::Heuristic::Description::draw trump for team announcement");
    return "draw trump for team announcement";
  case AiconfigHeuristic::play_to_jab_later:
    (void)_("AiConfig::Heuristic::play to jab later");
    (void)_("AiConfig::Heuristic::Description::play to jab later");
    return "play to jab later";
  case AiconfigHeuristic::play_highest_color_card_in_game:
    (void)_("AiConfig::Heuristic::play highest color card in game");
    (void)_("AiConfig::Heuristic::Description::play highest color card in game");
    return "play highest color card in game";
  case AiconfigHeuristic::jab_to_win:
    (void)_("AiConfig::Heuristic::jab to win");
    (void)_("AiConfig::Heuristic::Description::jab to win");
    return "jab to win";
  case AiconfigHeuristic::jab_to_not_loose:
    (void)_("AiConfig::Heuristic::jab to not loose");
    (void)_("AiConfig::Heuristic::Description::jab to not loose");
    return "jab to not loose";
  case AiconfigHeuristic::grab_trick:
    (void)_("AiConfig::Heuristic::grab trick");
    (void)_("AiConfig::Heuristic::Description::grab trick");
    return "grab trick";
  case AiconfigHeuristic::last_player_pass_small_trick:
    (void)_("AiConfig::Heuristic::last player pass small trick");
    (void)_("AiConfig::Heuristic::Description::last player pass small trick");
    return "last player pass small trick";

  case AiconfigHeuristic::cannot_jab:
    (void)_("AiConfig::Heuristic::cannot jab");
    (void)_("AiConfig::Heuristic::Description::cannot jab");
    return "cannot jab";
  case AiconfigHeuristic::play_last_trumps:
    (void)_("AiConfig::Heuristic::play last trumps");
    (void)_("AiConfig::Heuristic::Description::play last trumps");
    return "play last trumps";

  case AiconfigHeuristic::start_to_marry:
    (void)_("AiConfig::Heuristic::start to marry");
    (void)_("AiConfig::Heuristic::Description::start to marry");
    return "start to marry";
  case AiconfigHeuristic::jab_to_marry:
    (void)_("AiConfig::Heuristic::jab to marry");
    (void)_("AiConfig::Heuristic::Description::jab to marry");
    return "jab to marry";
  case AiconfigHeuristic::start_to_get_married:
    (void)_("AiConfig::Heuristic::start to get married");
    (void)_("AiConfig::Heuristic::Description::start to get married");
    return "start to get married";
  case AiconfigHeuristic::start_to_get_married_last_chance:
    (void)_("AiConfig::Heuristic::start to get married last chance");
    (void)_("AiConfig::Heuristic::Description::start to get married last chance");
    return "start to get married last chance";
  case AiconfigHeuristic::pfund_in_marriage_determination_trick:
    (void)_("AiConfig::Heuristic::pfund in marriage determination trick");
    (void)_("AiConfig::Heuristic::Description::pfund in marriage determination trick");
    return "pfund in marriage determination trick";

  case AiconfigHeuristic::poverty_contra_pfund_high_for_sure:
    (void)_("AiConfig::Heuristic::poverty: contra: pfund high for sure");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: pfund high for sure");
    return "poverty: contra: pfund high for sure";
  case AiconfigHeuristic::poverty_contra_pfund_for_sure:
    (void)_("AiConfig::Heuristic::poverty: contra: pfund for sure");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: pfund for sure");
    return "poverty: contra: pfund for sure";
  case AiconfigHeuristic::poverty_contra_pfund_high_for_unsure:
    (void)_("AiConfig::Heuristic::poverty: contra: pfund high for unsure");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: pfund high for unsure");
    return "poverty: contra: pfund high for unsure";
  case AiconfigHeuristic::poverty_contra_pfund_for_unsure:
    (void)_("AiConfig::Heuristic::poverty: contra: pfund for unsure");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: pfund for unsure");
    return "poverty: contra: pfund for unsure";
  case AiconfigHeuristic::poverty_contra_pfund_before_partner:
    (void)_("AiConfig::Heuristic::poverty: choose pfund before partner");
    (void)_("AiConfig::Heuristic::Description::poverty: choose pfund before partner");
    return "poverty: choose pfund before partner";
  case AiconfigHeuristic::poverty_special_start_with_trump:
    (void)_("AiConfig::Heuristic::poverty: special: start with trump");
    (void)_("AiConfig::Heuristic::Description::poverty: special: start with trump");
    return "poverty: special: start with trump";
  case AiconfigHeuristic::poverty_special_play_pfund:
    (void)_("AiConfig::Heuristic::poverty: special: play pfund");
    (void)_("AiConfig::Heuristic::Description::poverty: special: play pfund");
    return "poverty: special: play pfund";
  case AiconfigHeuristic::poverty_special_give_no_points:
    (void)_("AiConfig::Heuristic::poverty: special: give no points");
    (void)_("AiConfig::Heuristic::Description::poverty: special: give no points");
    return "poverty: special: give no points";
  case AiconfigHeuristic::poverty_special_offer_pfund:
    (void)_("AiConfig::Heuristic::poverty: special: offer pfund");
    (void)_("AiConfig::Heuristic::Description::poverty: special: offer pfund");
    return "poverty: special: offer pfund";
  case AiconfigHeuristic::poverty_re_jab_color_trick_with_trump:
    (void)_("AiConfig::Heuristic::poverty: re: jab color trick with trump");
    (void)_("AiConfig::Heuristic::Description::poverty: re: jab color trick with trump");
    return "poverty: re: jab color trick with trump";
  case AiconfigHeuristic::poverty_re_start_with_trump:
    (void)_("AiConfig::Heuristic::poverty: re: start with trump");
    (void)_("AiConfig::Heuristic::Description::poverty: re: start with trump");
    return "poverty: re: start with trump";
  case AiconfigHeuristic::poverty_contra_start_with_color:
    (void)_("AiConfig::Heuristic::poverty: contra: start with color");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: start with color");
    return "poverty: contra: start with color";
  case AiconfigHeuristic::poverty_contra_jab_color_trick_with_trump:
    (void)_("AiConfig::Heuristic::poverty: contra: jab color trick with trump");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: jab color trick with trump");
    return "poverty: contra: jab color trick with trump";
  case AiconfigHeuristic::poverty_leave_to_partner:
    (void)_("AiConfig::Heuristic::poverty: contra: leave to partner");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: leave to partner");
    return "poverty: contra: leave to partner";
  case AiconfigHeuristic::poverty_overjab_re:
    (void)_("AiConfig::Heuristic::poverty: contra: overjab re");
    (void)_("AiConfig::Heuristic::Description::poverty: contra: overjab re");
    return "poverty: contra: overjab re";
  case AiconfigHeuristic::poverty_re_jab_as_last_player:
    (void)_("AiConfig::Heuristic::poverty: re: jab as last player");
    (void)_("AiConfig::Heuristic::Description::poverty: re: jab as last player");
    return "poverty: re: jab as last player";

  case AiconfigHeuristic::play_color_in_solo:
    (void)_("AiConfig::Heuristic::play color in solo");
    (void)_("AiConfig::Heuristic::Description::play color in solo");
    return "play color in solo";

  case AiconfigHeuristic::color_jab_for_ace:
    (void)_("AiConfig::Heuristic::color: jab for ace");
    (void)_("AiConfig::Heuristic::Description::color: jab for ace");
    return "color: jab for ace";
  case AiconfigHeuristic::color_pfund_in_first_color_run:
    (void)_("AiConfig::Heuristic::color pfund in first color run");
    (void)_("AiConfig::Heuristic::Description::color pfund in first color run");
    return "color pfund in first color run";
  case AiconfigHeuristic::color_pfund_high_for_sure:
    (void)_("AiConfig::Heuristic::color pfund high for sure");
    (void)_("AiConfig::Heuristic::Description::color pfund high for sure");
    return "color pfund high for sure";
  case AiconfigHeuristic::color_pfund_for_sure:
    (void)_("AiConfig::Heuristic::color pfund for sure");
    (void)_("AiConfig::Heuristic::Description::color pfund for sure");
    return "color pfund for sure";
  case AiconfigHeuristic::color_pfund_high_for_unsure:
    (void)_("AiConfig::Heuristic::color pfund high for unsure");
    (void)_("AiConfig::Heuristic::Description::color pfund high for unsure");
    return "color pfund high for unsure";
  case AiconfigHeuristic::color_pfund_for_unsure:
    (void)_("AiConfig::Heuristic::color pfund for unsure");
    (void)_("AiConfig::Heuristic::Description::color pfund for unsure");
    return "color pfund for unsure";
  case AiconfigHeuristic::color_overjab_to_get_partner_backhand:
    (void)_("AiConfig::Heuristic::color: overjab to get partner backhand");
    (void)_("AiConfig::Heuristic::Description::color: overjab to get partner backhand");
    return "color: overjab to get partner backhand";

  case AiconfigHeuristic::picture_pull_down_color:
    (void)_("AiConfig::Heuristic::picture pull down color");
    (void)_("AiConfig::Heuristic::Description::picture pull down color");
    return "picture pull down color";
  case AiconfigHeuristic::picture_start_with_highest_color:
    (void)_("AiConfig::Heuristic::picture start with highest color");
    (void)_("AiConfig::Heuristic::Description::picture start with highest color");
    return "picture start with highest color";
  case AiconfigHeuristic::picture_get_last_trumps:
    (void)_("AiConfig::Heuristic::picture get last trumps");
    (void)_("AiConfig::Heuristic::Description::picture get last trumps");
    return "picture get last trumps";
  case AiconfigHeuristic::picture_get_first_trumps:
    (void)_("AiConfig::Heuristic::picture get first trumps");
    (void)_("AiConfig::Heuristic::Description::picture get first trumps");
    return "picture get first trumps";
  case AiconfigHeuristic::picture_draw_trumps:
    (void)_("AiConfig::Heuristic::picture draw trumps");
    (void)_("AiConfig::Heuristic::Description::picture draw trumps");
    return "picture draw trumps";
  case AiconfigHeuristic::picture_serve_trump_trick:
    (void)_("AiConfig::Heuristic::picture serve trump trick");
    (void)_("AiConfig::Heuristic::Description::picture serve trump trick");
    return "picture serve trump trick";
  case AiconfigHeuristic::picture_jab_trump_trick:
    (void)_("AiConfig::Heuristic::picture jab trump trick");
    (void)_("AiConfig::Heuristic::Description::picture jab trump trick");
    return "picture jab trump trick";
  case AiconfigHeuristic::picture_play_last_trumps:
    (void)_("AiConfig::Heuristic::picture play last trumps");
    (void)_("AiConfig::Heuristic::Description::picture play last trumps");
    return "picture play last trumps";
  case AiconfigHeuristic::picture_jab_color_trick_for_sure:
    (void)_("AiConfig::Heuristic::picture jab color trick for sure");
    (void)_("AiConfig::Heuristic::Description::picture jab color trick for sure");
    return "picture jab color trick for sure";
  case AiconfigHeuristic::picture_pfund_high_for_sure:
    (void)_("AiConfig::Heuristic::picture pfund high for sure");
    (void)_("AiConfig::Heuristic::Description::picture pfund high for sure");
    return "picture pfund high for sure";
  case AiconfigHeuristic::picture_pfund_for_sure:
    (void)_("AiConfig::Heuristic::picture pfund for sure");
    (void)_("AiConfig::Heuristic::Description::picture pfund for sure");
    return "picture pfund for sure";
  case AiconfigHeuristic::picture_pfund_high_for_unsure:
    (void)_("AiConfig::Heuristic::picture pfund high for unsure");
    (void)_("AiConfig::Heuristic::Description::picture pfund high for unsure");
    return "picture pfund high for unsure";
  case AiconfigHeuristic::picture_pfund_for_unsure:
    (void)_("AiConfig::Heuristic::picture pfund for unsure");
    (void)_("AiConfig::Heuristic::Description::picture pfund for unsure");
    return "picture pfund for unsure";

  case AiconfigHeuristic::meatless_start_with_best_color:
    (void)_("AiConfig::Heuristic::meatless: start with best color");
    (void)_("AiConfig::Heuristic::Description::meatless: start with best color");
    return "meatless: start with best color";
  case AiconfigHeuristic::meatless_pull_down_color:
    (void)_("AiConfig::Heuristic::meatless: pull down color");
    (void)_("AiConfig::Heuristic::Description::meatless: pull down color");
    return "meatless: pull down color";
  case AiconfigHeuristic::meatless_play_blank_color_of_soloplayer:
    (void)_("AiConfig::Heuristic::meatless: play blank color of soloplayer");
    (void)_("AiConfig::Heuristic::Description::meatless: play blank color of soloplayer");
    return "meatless: play blank color of soloplayer";
  case AiconfigHeuristic::meatless_retry_last_color:
    (void)_("AiConfig::Heuristic::meatless: retry last color");
    (void)_("AiConfig::Heuristic::Description::meatless: retry last color");
    return "meatless: retry last color";
  case AiconfigHeuristic::meatless_overjab_re:
    (void)_("AiConfig::Heuristic::meatless overjab re");
    (void)_("AiConfig::Heuristic::Description::meatless overjab re");
    return "meatless overjab re";
  case AiconfigHeuristic::meatless_pfund_for_sure:
    (void)_("AiConfig::Heuristic::meatless pfund for sure");
    (void)_("AiConfig::Heuristic::Description::meatless pfund for sure");
    return "meatless pfund for sure";
  case AiconfigHeuristic::meatless_pfund_before_partner:
    (void)_("AiConfig::Heuristic::meatless pfund before partner");
    (void)_("AiConfig::Heuristic::Description::meatless pfund before partner");
    return "meatless pfund before partner";
  case AiconfigHeuristic::meatless_play_color_for_partner_ace:
    (void)_("AiConfig::Heuristic::meatless: play color for partner ace");
    (void)_("AiConfig::Heuristic::Description::meatless: play color for partner ace");
    return "meatless: play color for partner ace";
  case AiconfigHeuristic::meatless_serve_color_trick:
    (void)_("AiConfig::Heuristic::meatless: serve color trick");
    (void)_("AiConfig::Heuristic::Description::meatless: serve color trick");
    return "meatless: serve color trick";
  case AiconfigHeuristic::meatless_serve_trick_contra:
    (void)_("AiConfig::Heuristic::meatless: serve trick contra");
    (void)_("AiConfig::Heuristic::Description::meatless: serve trick contra");
    return "meatless: serve trick contra";
  case AiconfigHeuristic::meatless_serve_trick_re:
    (void)_("AiConfig::Heuristic::meatless: serve trick re");
    (void)_("AiConfig::Heuristic::Description::meatless: serve trick re");
    return "meatless: serve trick re";
  case AiconfigHeuristic::meatless_play_highest_color_card_in_game:
    (void)_("AiConfig::Heuristic::meatless: play highest color card in game");
    (void)_("AiConfig::Heuristic::Description::meatless: play highest color card in game");
    return "meatless: play highest color card in game";
  } // switch(heuristic)

  return "";
}


auto is_real(AiconfigHeuristic const heuristic) -> bool
{
  switch (heuristic) {
  case AiconfigHeuristic::error:
  case AiconfigHeuristic::no_heuristic:
  case AiconfigHeuristic::manual:
  case AiconfigHeuristic::bug_report:
  case AiconfigHeuristic::gametree:
  case AiconfigHeuristic::only_one_valid_card:
  case AiconfigHeuristic::valid_card:
    return false;
  default:
    return true;
  }
}


auto is_general(AiconfigHeuristic const heuristic) -> bool
{
  return (    is_real(heuristic)
          && !for_marriage(heuristic)
          && !for_poverty(heuristic)
          && !for_solo(heuristic) );
}


auto for_marriage(AiconfigHeuristic const heuristic) -> bool
{
  switch (heuristic) {
  case AiconfigHeuristic::start_to_marry:
  case AiconfigHeuristic::jab_to_marry:
  case AiconfigHeuristic::start_to_get_married:
  case AiconfigHeuristic::start_to_get_married_last_chance:
  case AiconfigHeuristic::pfund_in_marriage_determination_trick:
    return true;
  default:
    return false;
  };
}


auto for_poverty(AiconfigHeuristic const heuristic) -> bool
{
  switch (heuristic) {
  case AiconfigHeuristic::poverty_contra_pfund_high_for_sure:
  case AiconfigHeuristic::poverty_contra_pfund_for_sure:
  case AiconfigHeuristic::poverty_contra_pfund_high_for_unsure:
  case AiconfigHeuristic::poverty_contra_pfund_for_unsure:
  case AiconfigHeuristic::poverty_contra_pfund_before_partner:
  case AiconfigHeuristic::poverty_special_play_pfund:
  case AiconfigHeuristic::poverty_special_give_no_points:
  case AiconfigHeuristic::poverty_special_offer_pfund:
  case AiconfigHeuristic::poverty_re_jab_as_last_player:
  case AiconfigHeuristic::poverty_re_jab_color_trick_with_trump:
  case AiconfigHeuristic::poverty_re_start_with_trump:
  case AiconfigHeuristic::poverty_contra_start_with_color:
  case AiconfigHeuristic::poverty_contra_jab_color_trick_with_trump:
  case AiconfigHeuristic::poverty_leave_to_partner:
  case AiconfigHeuristic::poverty_overjab_re:
    return true;
  default:
    return false;
  };
}


auto for_solo(AiconfigHeuristic const heuristic) -> bool
{
  return (   for_color_solo(heuristic)
          || for_picture_solo(heuristic)
          || for_meatless_solo(heuristic));
}


auto for_color_solo(AiconfigHeuristic const heuristic) -> bool
{
  switch (heuristic) {
  case AiconfigHeuristic::color_jab_for_ace:
  case AiconfigHeuristic::color_pfund_in_first_color_run:
  case AiconfigHeuristic::color_pfund_high_for_sure:
  case AiconfigHeuristic::color_pfund_for_sure:
  case AiconfigHeuristic::color_pfund_high_for_unsure:
  case AiconfigHeuristic::color_pfund_for_unsure:
  case AiconfigHeuristic::color_overjab_to_get_partner_backhand:
    return true;
  default:
    return false;
  };
}


auto for_picture_solo(AiconfigHeuristic const heuristic) -> bool
{
  switch (heuristic) {
  case AiconfigHeuristic::picture_pull_down_color:
  case AiconfigHeuristic::picture_start_with_highest_color:
  case AiconfigHeuristic::picture_get_last_trumps:
  case AiconfigHeuristic::picture_get_first_trumps:
  case AiconfigHeuristic::picture_draw_trumps:
  case AiconfigHeuristic::picture_serve_trump_trick:
  case AiconfigHeuristic::picture_jab_trump_trick:
  case AiconfigHeuristic::picture_play_last_trumps:
  case AiconfigHeuristic::picture_pfund_high_for_sure:
  case AiconfigHeuristic::picture_pfund_for_sure:
  case AiconfigHeuristic::picture_pfund_high_for_unsure:
  case AiconfigHeuristic::picture_pfund_for_unsure:
  case AiconfigHeuristic::picture_jab_color_trick_for_sure:
    return true;
  default:
    return false;
  };
}


auto for_meatless_solo(AiconfigHeuristic const heuristic) -> bool
{
  switch (heuristic) {
  case AiconfigHeuristic::meatless_start_with_best_color:
  case AiconfigHeuristic::meatless_pull_down_color:
  case AiconfigHeuristic::meatless_play_blank_color_of_soloplayer:
  case AiconfigHeuristic::meatless_retry_last_color:
  case AiconfigHeuristic::meatless_overjab_re:
  case AiconfigHeuristic::meatless_pfund_for_sure:
  case AiconfigHeuristic::meatless_pfund_before_partner:
  case AiconfigHeuristic::meatless_play_color_for_partner_ace:
  case AiconfigHeuristic::meatless_serve_color_trick:
  case AiconfigHeuristic::meatless_serve_trick_contra:
  case AiconfigHeuristic::meatless_serve_trick_re:
  case AiconfigHeuristic::meatless_play_highest_color_card_in_game:
    return true;
  default:
    return false;
  };
}


auto AiconfigHeuristic_from_name(string const& name) -> AiconfigHeuristic
{
  for (auto const h : aiconfig_heuristic_list) {
    if (name == to_string(h)) {
      return h;
    }
  }

  return AiconfigHeuristic::error;
}


auto operator<<(ostream& ostr, AiconfigHeuristic const heuristic) -> ostream&
{
  return (ostr << to_string(heuristic));
}


auto gettext(AiconfigHeuristic const heuristic) -> string
{
  return gettext("AiConfig::Heuristic::" + to_string(heuristic));
}


auto gettext_description(AiconfigHeuristic const heuristic) -> string
{
  return gettext("AiConfig::Heuristic::Description::" + to_string(heuristic));
}
