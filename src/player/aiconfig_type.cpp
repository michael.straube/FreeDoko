/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "aiconfig_type.h"

constexpr AiconfigType::Bool AiconfigType::bool_list[];
constexpr AiconfigType::Int AiconfigType::int_list[];
constexpr AiconfigType::Card AiconfigType::card_list[];


auto to_string(AiconfigDifficulty const difficulty) -> string
{
  switch(difficulty) {
  case AiconfigDifficulty::custom:
    (void)_("AiConfig::Difficulty::custom");
    return "custom";
  case AiconfigDifficulty::standard:
    (void)_("AiConfig::Difficulty::standard");
    return "standard";
  case AiconfigDifficulty::cautious:
    (void)_("AiConfig::Difficulty::cautious");
    return "cautious";
  case AiconfigDifficulty::chancy:
    (void)_("AiConfig::Difficulty::chancy");
    return "chancy";
  case AiconfigDifficulty::unfair:
    (void)_("AiConfig::Difficulty::unfair");
    return "unfair";
  } // switch(difficulty)

  return "";
}


auto gettext(AiconfigDifficulty const difficulty) -> string
{
  return gettext("AiConfig::Difficulty::" + to_string(difficulty));
}


auto operator<<(ostream& ostr, AiconfigDifficulty const difficulty) -> ostream&
{
  return (ostr << to_string(difficulty));
}


auto aiconfig_difficulty_from_string(string const name) -> AiconfigDifficulty
{
  for (auto const d : aiconfig_difficulty_list) {
    if (name == to_string(d)) {
      return d;
    }
  }
#ifndef OUTDATED
  // Änderung 2019-07-09
  if (name == "normal") {
    return AiconfigDifficulty::standard;
  } else if (name == "defensive") {
    return AiconfigDifficulty::cautious;
  } else if (name == "offensive") {
    return AiconfigDifficulty::chancy;
  }
#endif
#ifndef OUTDATED
  // Änderung, 2018-08-19
  if (name == "novice") {
    return AiconfigDifficulty::standard;
  } else if (name == "standard defensive") {
    return AiconfigDifficulty::cautious;
  } else if (name == "standard offensive") {
    return AiconfigDifficulty::chancy;
  } else if (name == "profi") {
    return AiconfigDifficulty::standard;
  } else if (name == "profi aggressive") {
    return AiconfigDifficulty::chancy;
  } else if (name == "profi unfair") {
    return AiconfigDifficulty::unfair;
  }
#endif
  throw std::ios_base::failure("unknown difficulty '" + name + "'");
}


auto to_string(AiconfigType::Bool const type) -> string
{
  switch(type) {
  case AiconfigType::trusting:
    (void)_("AiConfig::trusting");
    return "trusting";
  case AiconfigType::aggressive:
    (void)_("AiConfig::aggressive");
    return "aggressive";
  case AiconfigType::cautious:
    (void)_("AiConfig::cautious");
    return "cautious";
  case AiconfigType::chancy:
    (void)_("AiConfig::chancy");
    return "chancy";
  case AiconfigType::teams_known:
    (void)_("AiConfig::teams known");
    return "teams known";
  case AiconfigType::hands_known:
    (void)_("AiConfig::hands known");
    return "hands known";
  } // switch(type)

  return "";
}


auto to_string(AiconfigType::Int const type) -> string
{
  switch(type) {
  case AiconfigType::max_sec_wait_for_gametree:
    (void)_("AiConfig::max sec wait for gametree");
    return "max sec wait for gametree";
  case AiconfigType::future_limit:
    (void)_("AiConfig::future limit");
    return "future limit";
  case AiconfigType::limit_throw_fehl:
    (void)_("AiConfig::limit throw fehl");
    return "limit throw fehl";
  case AiconfigType::limitqueen:
    (void)_("AiConfig::limit queen");
    return "limit queen";
  case AiconfigType::limitdulle:
    (void)_("AiConfig::limit dulle");
    return "limit dulle";
  case AiconfigType::last_tricks_without_heuristics:
    (void)_("AiConfig::last tricks without heuristics");
    return "last tricks without heuristics";
  case AiconfigType::first_trick_for_trump_points_optimization:
    (void)_("AiConfig::first trick for trump points optimization");
    return "first trick for trump points optimization";
  case AiconfigType::announcelimit:
    (void)_("AiConfig::announce limit");
    return "announce limit";
  case AiconfigType::announcelimitdec:
    (void)_("AiConfig::announce limit decrement");
    return "announce limit decrement";
  case AiconfigType::announceconfig:
    (void)_("AiConfig::announce config");
    return "announce config";
  case AiconfigType::announcelimitreply:
    (void)_("AiConfig::announce limit reply");
    return "announce limit reply";
  case AiconfigType::announceconfigreply:
    (void)_("AiConfig::announce config reply");
    return "announce config reply";
  } // switch(type)

  return "";
}


auto to_string(AiconfigType::Card const type) -> string
{
  switch(type) {
  case AiconfigType::limitthrowing:
    (void)_("AiConfig::limit throwing");
    return "limit throwing";
  case AiconfigType::limithigh:
    (void)_("AiConfig::limit high");
    return "limit high";
  case AiconfigType::trumplimit_solocolor:
    (void)_("AiConfig::trump limit color");
    return "trump limit color";
  case AiconfigType::trumplimit_solojack:
    (void)_("AiConfig::trump limit jack");
    return "trump limit jack";
  case AiconfigType::trumplimit_soloqueen:
    (void)_("AiConfig::trump limit queen");
    return "trump limit queen";
  case AiconfigType::trumplimit_soloking:
    (void)_("AiConfig::trump limit king");
    return "trump limit king";
  case AiconfigType::trumplimit_solojackking:
    (void)_("AiConfig::trump limit jack king");
    return "trump limit jack king";
  case AiconfigType::trumplimit_solojackqueen:
    (void)_("AiConfig::trump limit jack queen");
    return "trump limit jack queen";
  case AiconfigType::trumplimit_soloqueenking:
    (void)_("AiConfig::trump limit queen king");
    return "trump limit queen king";
  case AiconfigType::trumplimit_solokoehler:
    (void)_("AiConfig::trump limit koehler");
    return "trump limit koehler";
  case AiconfigType::trumplimit_meatless:
    (void)_("AiConfig::trump limit meatless");
    return "trump limit meatless";
  case AiconfigType::trumplimit_normal:
    (void)_("AiConfig::trump limit normal");
    return "trump limit normal";
  case AiconfigType::lowest_trumplimit_solocolor:
    (void)_("AiConfig::lowest trump limit color");
    return "lowest trump limit color";
  case AiconfigType::lowest_trumplimit_solojack:
    (void)_("AiConfig::lowest trump limit jack");
    return "lowest trump limit jack";
  case AiconfigType::lowest_trumplimit_soloqueen:
    (void)_("AiConfig::lowest trump limit queen");
    return "lowest trump limit queen";
  case AiconfigType::lowest_trumplimit_soloking:
    (void)_("AiConfig::lowest trump limit king");
    return "lowest trump limit king";
  case AiconfigType::lowest_trumplimit_solojackking:
    (void)_("AiConfig::lowest trump limit jack king");
    return "lowest trump limit jack king";
  case AiconfigType::lowest_trumplimit_solojackqueen:
    (void)_("AiConfig::lowest trump limit jack queen");
    return "lowest trump limit jack queen";
  case AiconfigType::lowest_trumplimit_soloqueenking:
    (void)_("AiConfig::lowest trump limit queen king");
    return "lowest trump limit queen king";
  case AiconfigType::lowest_trumplimit_solokoehler:
    (void)_("AiConfig::lowest trump limit koehler");
    return "lowest trump limit koehler";
  case AiconfigType::lowest_trumplimit_meatless:
    (void)_("AiConfig::lowest trump limit meatless");
    return "lowest trump limit meatless";
  case AiconfigType::lowest_trumplimit_normal:
    (void)_("AiConfig::lowest trump limit normal");
    return "lowest trump limit normal";
  } // switch(type)

  return "";
}


auto gettext(AiconfigType::Bool const type) -> string
{
  return gettext("AiConfig::" + to_string(type));
}


auto operator<<(ostream& ostr, AiconfigType::Bool const type) -> ostream&
{
  return (ostr << to_string(type));
}


auto gettext(AiconfigType::Int const type) -> string
{
  return gettext("AiConfig::" + to_string(type));
}


auto operator<<(ostream& ostr, AiconfigType::Int const type) -> ostream&
{
  return (ostr << to_string(type));
}


auto gettext(AiconfigType::Card const type) -> string
{
  return gettext("AiConfig::" + to_string(type));
}


auto operator<<(ostream& ostr, AiconfigType::Card const type) -> ostream&
{
  return (ostr << to_string(type));
}
