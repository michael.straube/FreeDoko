/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "cards_information.h"
#include "cards_information.of_player.h"
#include "cards_information.heuristics.h"

#include "player.h"
#include "team_information.h"
#include "ai/ai.h"

#include "../card/trick.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"

// whether to update only when a hand is requested
#define UPDATE_ON_DEMAND
#ifdef NO_UPDATE_ON_DEMAND
#undef UPDATE_ON_DEMAND
#endif
#ifdef CHECK_RUNTIME
#include "../../runtime.h"
#endif

// whether to make self checks
// local macro
#ifndef RELEASE
#define SELF_CHECK
#endif

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define THROW_INVALID_GAME_IF(predicate, text) \
  do {                                   \
    if (predicate) {                  \
      DEBUG("throw") << text << '\n'     \
                     << *this; \
      ostringstream ostr;                \
      ostr << "ASSERT(" << __FILE__ << '#' << __LINE__ << "): " \
           << text << '\n'               \
           << *this;       \
      if (!is_virtual()) { \
        report_error(ostr.str());        \
      }                                  \
      throw InvalidGameException(ostr.str()); \
    } \
  } while(false)

/*
 * CardsInformation contains the information a player has of the distribution
 * of the cards in a game. The player does not necessarily be an active player,
 * The 'user' of this class has to call the methods corresponding to the
 * gameplay.
 * Information can be contained from the methods 'possible_hand(Player)',
 * 'estimated_hand(Player) and with 'of_player(Player)' specific information
 * about a player.
 *
 * The idea is to take all information from the gameplay to tell which cards
 * - are played
 * - a specific player must have
 * - a specific player cannot have
 * Merging the informations between the players (see CardsInformation::OfPlayer)
 * more data can be created (if par example three players do not have spade,
 * then the last one must have all remaining spade cards).
 *
 * The collected informations are
 * - the hand of the ai is set
 *   the other players cannot have the cards
 *   (in virtual games a bit more complicated, since the hand can contain more
 *    cards than the ai has to play)
 * - a card is played
 *   the card does not longer remain, it is marked as played by the player
 * - a player has not served a color
 *   the player has no more card of the color
 * - announcement re (normal game)
 *   the player has at least one club queen (can be already played)
 * - announcement contra (normal game)
 *   the player has no club queen
 * - marriage
 *   the player has both club queens
 * - poverty
 *   the poverty player has as many trumps as he has gotten back
 * - announcement swines / hyperswines
 *   the player has both trump aces / nines / kings
 *
 * When more information was collected the information about a player is
 * updated as good as I could program it (see 'OfPlayer').
 *
 * 'CardsInformation' gets noted about changed cards ('queue_update()')
 * Then the 'of_player's are informed about the changes ('do_update()') till no
 * changed card remains (the 'of_player's can add cards again).
 *
 */

/*
 * Implemention remark
 * the class cannot be an 'OS' because then the functions are not called in
 * virtual games
 */

/** constructor
 **
 ** @param    player         corresponding player
 **/
CardsInformation::CardsInformation(Player const& player) :
  player_(&player)
{ }

/** copy constructor
 **
 ** @param    cards_information   object to be copied
 **/
CardsInformation::CardsInformation(CardsInformation const& cards_information) :
  player_(cards_information.player_),
  of_player_(cards_information.of_player_),
  color_runs_(cards_information.color_runs_),
  played_(cards_information.played_),
  tcolor_played_(cards_information.tcolor_played_),
  in_recalcing(cards_information.in_recalcing),
  in_update(cards_information.in_update),
  possible_hands_(cards_information.possible_hands_),
  estimated_hands_(cards_information.estimated_hands_),
  auto_update_(cards_information.auto_update_),
  cards_to_update(cards_information.cards_to_update),
  hands_outdated(cards_information.hands_outdated)
{
  for (auto& p : of_player_)
    p.set_cards_information(*this);
} // CardsInformation::CardsInformation(CardsInformation cards_information)

/** destructor
 **/
CardsInformation::~CardsInformation() = default;

/** writes 'cards_information' in 'ostr'
 **
 ** @param    ostr         output stream
 ** @param    cards_information   object to write into 'ostr'
 **
 ** @return   the output stream
 **/
ostream&
operator<<(ostream& ostr, CardsInformation const& cards_information)
{
  cards_information.write_summary(ostr);
  return ostr;
} // ostream& operator<<(ostream& ostr, CardsInformation cards_information)

/** writes 'cards_information' in 'ostr'
 **
 ** @param    ostr      output stream
 **/
void
CardsInformation::write(ostream& ostr) const
{
  ostr << "cards information:\n"
    << "{\n";

  ostr << "played:\n"
    << "{\n"
    << played_
    << "}\n"
    << "tcolor played:\n"
    << "{\n"
    << tcolor_played_
    << "}\n"
    << '\n';

  for (auto const& p : of_player_)
    ostr << p << '\n';

  ostr << "}\n";
} // void CardsInformation::write(ostream& ostr) const


/** writes the summary of 'cards_information' in 'ostr'
 **
 ** @param    ostr      output stream
 **/
void
CardsInformation::write_summary(ostream& ostr) const
{
  ostr << "cards information summary:\n"
    << "  {\n";
  ostr << setw(19) << "|";
  for (unsigned p = 0; p < game().playerno(); ++p)
    ostr << " " << p
      << (p == player().no() ? '*' : ' ')
      << " "
      << game().player(p).name() << setw(10 - game().player(p).name().length()) << "|";
  ostr << '\n';
  ostr << "-------------+----+-------------+-------------+-------------+-------------+\n";
  { // tcolors
    vector<Card::TColor> tcolors;
    tcolors.push_back(Card::trump);
    for (auto c : game().rule().card_colors())
      tcolors.push_back(c);
    for (auto c : tcolors) {
      if (remaining(c)) {
        ostr << setw(13) << c << ":" << setw(3) << remaining(c) << " |";
        for (unsigned p = 0; p < game().playerno(); ++p) {
          if (of_player(p).can_have(c) == 0) {
            ostr << setw(14) << "|";
          } else {
            if (of_player(p).must_have(c))
              ostr << setw(2) << of_player(p).must_have(c);
            else
              ostr << " -";
            ostr << " ";
            if (  of_player(p).can_have(c)
                > of_player(p).must_have(c)) {
              ostr << setw(2) << (  of_player(p).can_have(c)
                                  - of_player(p).must_have(c))
                << "  "
                << setw(5) << ""
                << " |";
            } else {
              ostr << " -" << setw(9) << "|";
            }
          }

        } // for (p)
        ostr << '\n';
      } // if (remaining(c))
    } // for (c : card_colors)
  } // tcolors
  ostr << "-------------+----+-------------+-------------+-------------+-------------+\n";
  vector<Card> cards = game().cards().sorted_single_cards();
  cards.push_back(Card::unknown);
  for (auto c : cards) {
    if (remaining(c)) {
      ostr << setw(13) << c << ":" << setw(3) << remaining(c) << " |";
      for (unsigned p = 0; p < game().playerno(); ++p) {
        ostr << " ";
        if (of_player(p).can_have(c) == 0) {
          ostr << setw(13) << "|";
        } else {
          if (of_player(p).must_have(c))
            ostr << of_player(p).must_have(c);
          else
            ostr << "-";
          ostr << "  ";
          if (  of_player(p).can_have(c)
              > of_player(p).must_have(c)) {
            ostr << (  of_player(p).can_have(c)
                     - of_player(p).must_have(c))
              << "  "
              << setw(5) << of_player(p).weighting(c)
              << " |";
          } else {
            ostr << "-" << setw(9) << "|";
          }
        }

      } // for (p)
      ostr << '\n';
    } // if (remaining(c))
  } // for (c : cards)
  { // sum
    ostr << "-------------+----+-------------+-------------+-------------+-------------+\n";
    ostr << setw(14) << "sum:" << setw(3) << remaining_cards_no() << " |";
    for (unsigned p = 0; p < game().playerno(); ++p) {
      if (of_player(p).can_have_.cards_no() == 0) {
        ostr << setw(13) << "|";
      } else {
        ostr << setw(2);
        if (of_player(p).must_have_.cards_no())
          ostr << of_player(p).must_have_.cards_no();
        else
          ostr << "-";
        ostr << " ";
        ostr << setw(2);
        if (  of_player(p).can_have_.cards_no()
            > of_player(p).must_have_.cards_no()) {
          ostr << (  of_player(p).can_have_.cards_no()
                   - of_player(p).must_have_.cards_no());
        } else {
          ostr << "-";
        }
        ostr << " / " << setw(2) << cardno_to_play(game().player(p))
          << setw(4) << "|";
      }
    } // for (p)
    ostr << '\n';
  } // sum
  ostr << "  must have     can have  /  cardno to play\n";
  ostr << "  }\n";
} // void CardsInformation::write_summary(ostream& ostr) const

/** @return   whether the cards information are virtual
 **/
bool
CardsInformation::is_virtual() const
{
  return (   game().isvirtual()
          || (this != &player().cards_information())
          || (player() != game().player(player().no()))
         );
} // bool CardsInformation::is_virtual() const

/** resets all information
 **/
void
CardsInformation::reset()
{
  played_.clear();
  tcolor_played_.clear();

  of_player_.clear();
  of_player_.reserve(4); // ToDo: game().playerno()
  for (unsigned p = 0; p < 4; ++p) { // ToDo: game().playerno()
    of_player_.emplace_back(*this, p);
    of_player_.back().reset();
  }

  color_runs_ = vector<unsigned>(Card::maxcardcolor+1, 0);

  possible_hands_.clear();
  possible_hands_.resize(4); // ToDo: game().playerno()
  estimated_hands_.clear();
  estimated_hands_.resize(4); // ToDo: game().playerno()

  hands_outdated = true;
} // void CardsInformation::reset()

/** recalcs all information
 ** does remember all color runs and all 'important' cards
 **/
void
CardsInformation::recalc()
{
#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai cards information"].start_stop_proxy();
#endif

  try {
    if (   dynamic_cast<Ai const&>(player()).value(Aiconfig::Type::hands_known)
        && game().status() >= Game::Status::reservation
        && game().status() != Game::Status::redistribute) {
      for (auto const& player : game().players()) {
        set_hand(player, player.hand());
      }
      return ;
    }
  } catch (std::bad_cast const&) {
  }

  in_recalcing = true;
  (void)in_recalcing;

  reset();
  auto const& game = this->game();

  if (game.status() >= Game::Status::play) {
    { // mark the played cards
      for (unsigned t = 0; t < game.tricks().current_no(); ++t) {
        auto const& trick = game.tricks().trick(t);
        auto trick2 = Trick(trick.startplayer());
        for (unsigned c = 0; c < trick.actcardno(); ++c) {
          auto const& card = trick.card(c);
          trick2.add(card);
          card_played(card, trick2);
        } // for (c < trick.actcardno())
        color_runs_[trick.startcard().tcolor()] += 1;

        // check for colors the players do not have
        auto const tcolor = trick2.startcard().tcolor();
        if (tcolor != Card::unknowncardcolor) {
          for (unsigned c = 1; c < trick2.actcardno(); ++c) {
            if (   (trick.card(c).tcolor() != tcolor)
                && (trick.card(c).tcolor() != Card::unknowncardcolor) )
              of_player(trick2.card(c).player()
                       ).add_cannot_have(tcolor);
          } // for (c < trick.actcardno())
        } // if (tcolor != Card::unknowncardcolor)
      } // for (t < game.tricks().current_no())
      if (   !game.tricks().current().isfull()
          && !game.tricks().current().isempty())
        color_runs_[game.tricks().current().startcard().tcolor()]
          -= 1;
    } // mark the played cards

    { // add announcements info
      for (auto& p : of_player_) {
        if (!!game.announcements().last(p.player()))
          announcement_made(game.announcements().last(p.player()), p.player());
      } // for (p \in of_player_)
    } // add announcements info
  }

  { // add marriage info
    if (is_marriage(game.type())) {
      auto n = (game.rule(Rule::Type::number_of_same_cards)
                - played(Card::club_queen));
      for (auto& p : of_player_) {
        if (p.player() == game.players().soloplayer()) {
          if (n >= p.played(Card::unknown))
            p.add_must_have(Card::club_queen,
                            n - p.played(Card::unknown));
        } else {
          p.add_cannot_have(Card::club_queen);
        }
      }
    } // if (is marriage)
  } // add marriage info
#ifndef WITH_DOKOLOUNGE
  // In der Doko-Lounge wird nur bekannt gegeben, ob die Armut überhaupt Trumpf erhalten hat, aber nicht wie viele Trumpf.
  // Die Dokolounge kümmert sich selber um die Informationen zu einer Armut
  { // add poverty info
    if (   game.status() >= Game::Status::start
        && game.type() == GameType::poverty) {
      auto& of_player = this->of_player(game.players().soloplayer());
      auto n = (game.poverty().returned_trumpno()
                - of_player.played(Card::trump));
      of_player.add_can_have(Card::trump, n);
      if (of_player.played(Card::unknown) < n)
        of_player.add_must_have(Card::trump,
                                n - of_player.played(Card::unknown));
    } // if (game.type() == GameType::poverty)
  } // add poverty info
#endif
  { // add swines info
    if (game.swines().swines_announced()) {
      for (auto& p : of_player_) {
        if (p.player() != game.swines().swines_owner())
          p.add_cannot_have(game.cards().swine());
      }

      if (game.swines().hyperswines_announced()) {
        for (auto& p : of_player_) {
          if (p.player() != game.swines().hyperswines_owner())
            p.add_cannot_have(game.cards().hyperswine());
        }
      } // if (game.hyperswines_owner())
    } // if (game.swines_owner())
  } // add swines info

  if (player_)
    set_hand(player(), player().hand());

  in_recalcing = false;

  // this must be called here because it is skipped being in recalcing
  update_remaining_cards();

  queue_update_all();

  if (auto_update())
    do_update();
} // void CardsInformation::recalc()

/** @return   the corresponding player
 **/
Player const&
CardsInformation::player() const
{ return *player_; }

/** @return   whether to auto update
 **/
bool
CardsInformation::auto_update() const
{ return auto_update_; }

/** sets the auto update
 **
 ** @param    value   new value
 **/
void
CardsInformation::set_auto_update(bool const value)
{ auto_update_ = value; }


auto CardsInformation::operator()(Player const& player) const -> OfPlayer const&
{
  return of_player(player);
}


auto CardsInformation::of_player(Player const& player) const -> OfPlayer const&
{
  return of_player(player.no());
}


auto CardsInformation::of_player(unsigned const playerno) const -> OfPlayer const&
{
  return of_player_[playerno];
}


auto CardsInformation::of_player(Player const& player) -> OfPlayer&
{
  return of_player(player.no());
}


auto CardsInformation::of_player(unsigned const playerno) -> OfPlayer&
{
  return of_player_[playerno];
}


/** @return   how many of the players still have unknown cards
 **/
unsigned
CardsInformation::remaining_unknown_players() const
{
  unsigned n = 0;
  for (auto const& p : of_player_)
    if (!p.all_known())
      n += 1;
  return n;
} // unsigned CardsInformation::remaining_unknown_players() const

/** -> result
 **
 ** @param    tcolor   trump color
 **
 ** @return   the number of runs of tcolor (without the current trick)
 **/
unsigned
CardsInformation::color_runs(Card::TColor const tcolor) const
{
  DEBUG_ASSERTION(tcolor != Card::unknowncardcolor,
                  "CardsInformation::color_runs(" << tcolor << ")\n"
                  "  tcolor 'unknown' is forbidden");
  return color_runs_[tcolor];
} // CardsInformation::OfPlayer CardsInformation::color_runs(Card::TColor tcolor) const

/** @return   the number of runs of any color (without trump, without the current trick)
 **/
unsigned
CardsInformation::color_runs() const
{
  unsigned n = 0;
  for (auto const color : game().cards().colors())
    n += color_runs_[color];
  return n;
}

/** @return   the number of runs of trump (without the current trick)
 **/
unsigned
CardsInformation::trump_runs() const
{
  unsigned runs = 0;
  for (unsigned t = 0; t < game().tricks().current_no(); ++t)
    if (game().tricks().trick(t).startcard().istrump())
      runs += 1;

  return runs;
} // CardsInformation::OfPlayer CardsInformation::trump_runs() const

/** -> result
 **
 ** @param    player   the player whose hand is asked for
 **
 ** @return   hand with all cards the player can have
 **/
Hand const&
CardsInformation::possible_hand(Player const& player) const
{
  DEBUG_ASSERTION((&game() == &player.game()),
                  "CardsInformation::possible_hand(player):\n"
                  "  the game of the player is not the game of the player");

  return possible_hand(player.no());
} // Hand CardsInformation::possible_hand(Player player) const

/** -> result
 **
 ** @param    playerno   the number of the player
 **
 ** @return   hand with all cards the player can have
 **/
Hand const&
CardsInformation::possible_hand(unsigned const playerno) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  if (hands_outdated)
    recalc_hands();
  return possible_hands_[playerno];
} // Hand CardsInformation::possible_hand(unsigned playerno) const


// NOLINTNEXTLINE(misc-no-recursion)
auto CardsInformation::estimated_hand(Player const& player) const -> Hand const&
{
  DEBUG_ASSERTION((&game() == &player.game()),
                  "CardsInformation::estimated_hand(player):\n"
                  "  the game of the player is not the game of the player");

  return estimated_hand(player.no());
}


// NOLINTNEXTLINE(misc-no-recursion)
auto CardsInformation::estimated_hand(unsigned const playerno) const -> Hand const&
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  if (hands_outdated)
    recalc_hands();
  return estimated_hands_[playerno];
}

/** @return   alle combination of estimated hands
 **/
vector<Hands>
CardsInformation::estimated_hands_combinations() const
{
  vector<Hands> estimated_hands_combinations;

  auto const playerno = player().game().rule(Rule::Type::number_of_players_in_game);

  Hands const base_hands = [&]() {
    Hands hands;
    for (unsigned p = 0; p < playerno; ++p) {
      hands.push_back(of_player(p).must_have_cards());
    }
    return hands;
  }();
  auto const free_cards = [&]() {
    vector<Card> cards;
    for (auto const card : game().cards().sorted_single_cards()) {
      auto n = played(card);
      for (auto const& hand : base_hands)
        n += hand.count(card);
      DEBUG_ASSERTION(n <= game().rule(Rule::Type::number_of_same_cards),
                      "CardsInformation::estimated_hand_combinations()\n"
                      "  Wrong count played + must have for card " << card << ": "
                      << n << " > " << game().rule(Rule::Type::number_of_same_cards));
      cards.insert(cards.end(), game().rule(Rule::Type::number_of_same_cards) - n, card);
    }
    return cards;
  }();
  auto free_players = [&]() {
    vector<unsigned> players;
    for (unsigned p = 0; p < playerno; ++p) {
      players.insert(players.end(), of_player(p).remaining_cards_no() - base_hands[p].cardsnumber(), p);
    }
    return players;
  }();

  DEBUG_ASSERTION(free_cards.size() == free_players.size(),
                  "CardsInformation::estimated_hand_combinations()\n"
                  "   size of free_cards = " << free_cards.size() << " != " << free_players.size() << " = size of free_players");

  auto const add_cards = [](Hands& hands, vector<Card> const& free_cards, vector<unsigned> const& free_players) {
    for (size_t i = 0; i < free_cards.size(); ++i) {
      hands[free_players[i]].add(free_cards[i]);
    }
  };
  auto const hands_valid = [this,playerno](Hands const& hands) {
    for (unsigned p = 0; p < playerno; ++p)
      if (!of_player(p).is_valid(hands[p]))
        return false;
    return true;
  };

  do {
    Hands hands = base_hands;
    add_cards(hands, free_cards, free_players);
    if (hands_valid(hands))
      estimated_hands_combinations.push_back(hands);
  } while (std::next_permutation(free_players.begin(), free_players.end()));

  int max_weighting = INT_MIN;
  for (auto const& hands : estimated_hands_combinations) {
    max_weighting = std::max(max_weighting, weighting(hands));
  }
  remove_if(estimated_hands_combinations, [this,max_weighting](Hands const& hands) {
            return weighting(hands) < max_weighting - 80;
            });

  container_algorithm::sort(estimated_hands_combinations,
                            [this](Hands const& lhs, Hands const& rhs) {
                            return weighting(lhs) > weighting(rhs);
                            });

  return estimated_hands_combinations;
}

/** @return   the weighting of the hands
 **/
int
CardsInformation::weighting(Hands const& hands) const
{
  int weighting = INT_MAX;
  DEBUG_ASSERT(hands.size() == of_player_.size());
  for (unsigned p = 0; p < hands.size(); ++p) {
    if (weighting < 0)
      weighting += std::min(0, of_player(p).weighting(hands[p]));
    else
      weighting = std::min(weighting, of_player(p).weighting(hands[p]));
  }
  return weighting;
}


HandCard
CardsInformation::highest_remaining_trump() const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  HandCard card;
  for (auto const& p : game().players()) {
    // note: possible_hand() is a temporary hand,
    // so 'c' needs another hand
    auto const c
      = HandCard(p.hand(),
                 possible_hand(p).highest_card(Card::trump));
    if (card.is_jabbed_by(c))
      card = c;
  } // for (p)

  return card;
} // Card CardsInformation::highest_remaining_trump() const


HandCard
CardsInformation::highest_remaining_trump_of_others() const
{
  return highest_remaining_card_of_others(Card::trump);
}


HandCard
CardsInformation::highest_remaining_card_of_others(Card::TColor const tcolor) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  HandCard card;
  for (auto const& p : game().players()) {
    if (p == player())
      continue;
    // note: possible_hand() is a temporary hand,
    // so 'c' needs another hand
    auto const c
      = HandCard(p.hand(),
                 possible_hand(p).highest_card(tcolor));
    if (card.is_jabbed_by(c))
      card = c;
  }

  return card;
}

/** -> result
 **
 ** @param    card   card to compare with
 **
 ** @return   whether there exists a higher card than 'card' on the other hands
 **/
bool
CardsInformation::higher_card_exists(HandCard const card) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  for (auto const& p : game().players()) {
    if (p == card.player())
      continue;
    if (possible_hand(p).higher_card_exists(card))
      return true;
  } // for (p)

  return false;
} // bool CardsInformation::higher_card_exists(HandCard card) const

/** -> result
 **
 ** @param    card   card to compare with
 **
 ** @return   number of higher cards then 'card'
 **/
unsigned
CardsInformation::higher_cards_no_of_others(HandCard const card) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  unsigned n = 0;
  for (auto const& c : game().rule().cards()) {
    if (card.is_jabbed_by({card.hand(), c}))
      n += remaining_others(c);
  }

  return n;
} // unsigned CardsInformation::higher_cards_no_of_others(HandCard card) const


/** @return   the corresponding game
 **/
Game const&
CardsInformation::game() const
{
  return player().game();
}

/** sets (changes) the player
 **
 ** @param    player   new player
 **/
void
CardsInformation::set_player(Player const& player)
{
  player_ = &player;
} // void CardsInformation::set_player(Ai player)

/** sets the hand of the player
 **
 ** @param    player   player whose hand is set
 ** @param    hand   the hand of the player
 **/
void
CardsInformation::set_hand(Player const& player, Hand const& hand)
{
  if (game().status() == Game::Status::poverty_shift)
    return ;
  if (game().status() == Game::Status::cards_distribution)
    return ;

#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai cards information"].start_stop_proxy();
#endif

  of_player(player).add_can_only_have(hand.cards());

  if (   !in_recalcing
      && auto_update())
    do_update();
} // void CardsInformation::set_hand(Player player, Hand hand)

/** queue to update the information of the cards
 **
 ** @param    cards   cards to update the information of
 **/
void
CardsInformation::queue_update(vector<Card> const& cards)
{
  for (auto const c : cards)
    queue_update(c);
} // void CardsInformation::queue_update(vector<Card> cards)

/** queue to update the information of the card
 **
 ** @param    card   card to update the information of
 **/
void
CardsInformation::queue_update(Card const card)
{
  cards_to_update.insert(card);
} // void CardsInformation::queue_update(Card card)

/** queue to update the information of the cards of tcolor
 **
 ** @param    tcolor   tcolor of the cards to update the information of
 **/
void
CardsInformation::queue_update(Card::TColor const tcolor)
{
  DEBUG_ASSERTION(tcolor != Card::unknowncardcolor,
                  "CardsInformation::queue_update(" << tcolor << ")\n"
                  "  tcolor 'unknown' is forbidden");
  queue_update(game().cards().cards(tcolor));
} // void CardsInformation::queue_update(Card::TColor tcolor)


// NOLINTNEXTLINE(misc-no-recursion)
void CardsInformation::do_update() const
{
  if (in_update)
    return ;
  if (cards_to_update.empty())
    return ;

  in_update = true;

  while (!cards_to_update.empty()) {
    while (!cards_to_update.empty()) {
      auto const card = *cards_to_update.begin();
      cards_to_update.erase(cards_to_update.begin());
      if (card.is_unknown())
        continue;
      for (auto& p : of_player_)
        p.update_information(card);
    } // while (!cards_to_update().empty())

    for (auto& p : of_player_) {
      p.check_can_is_must();
      p.check_must_is_can();
      p.update_tcolor_information();
    } // for (p : of_player_)
  } // while (!cards_to_update().empty())

#ifdef DKNOF
  const_cast<CardsInformation*>(this)->check_joined_hands();
#endif
  if (!cards_to_update.empty())
    do_update();

#ifdef SELF_CHECK
  self_check();
#endif
  in_update = false;

#ifndef UPDATE_ON_DEMAND
  // ToDo: recalc the weighting for all cards
  recalc_hands();
#else
  hands_outdated = true;
#endif
} // void CardsInformation::do_update() const

/** updates the information of all cards
 **/
void
CardsInformation::queue_update_all()
{
  for (auto const& c : game().rule().cards())
    queue_update(c);
} // void CardsInformation::queue_update_all()


// NOLINTNEXTLINE(misc-no-recursion)
void CardsInformation::check_joined_hands()
{
  // Check whether two players can have so many cards between them as they still have to play. Then the other players cannot have the cards.
  if (remaining_unknown_players() <= 2)
    return;

  for (auto p1 = of_player_.begin();
       p1 != of_player_.end();
       ++p1) {
    if (p1->all_known())
      continue;
    auto p2 = p1;
    ++p2;
    for (; p2 != of_player_.end(); ++p2) {
      if (p2->all_known())
        continue;
      if (   (p1->can_have_.cards_no()
              > cardno_to_play(p1->player())
              + cardno_to_play(p2->player()))
          || (p2->can_have_.cards_no()
              > cardno_to_play(p1->player())
              + cardno_to_play(p2->player())))
        continue;

      // check the joined cards
      auto const can_have = joined_can_have(*p1, *p2);
      THROW_INVALID_GAME_IF(can_have.cards_no()
                            < cardno_to_play(p1->player())
                            + cardno_to_play(p2->player()),
                            "check_joined_hands()\n"
                            "  can_have(p1 + p2).cards_no() = " << can_have.cards_no() << " < "
                            << cardno_to_play(p1->player()) << " + " << cardno_to_play(p2->player())
                            << " = cardno_to_play(p1->player()) + cardno_to_play(p2->player())");

      if (can_have.cards_no()
          == cardno_to_play(p1->player())
          + cardno_to_play(p2->player())) {
        for (auto p = of_player_.begin();
             p != of_player_.end();
             ++p) {
          if ((p == p1) || (p == p2))
            continue;
          if (p->all_known())
            continue;
          // all cards of can_have must be shared between p1 and p2
          for (auto const& c : can_have)
            p->add_can_have(c.first,
                            game().rule(Rule::Type::number_of_same_cards)
                            - played(c.first) - c.second);
        } // for (p)
        if (   !in_recalcing
            && auto_update())
          do_update();

      } // if (cards shared between p1 and p2)
    } // for (p2)
  } // for (p1)
} // void CardsInformation::check_joined_hands()

/** updates 'can have' according to 'remaining cards'
 **/
void
CardsInformation::update_remaining_cards()
{
  for (auto& p : of_player_)
    p.update_remaining_cards();
} // void CardsInformation::update_remaining_cards()


// NOLINTNEXTLINE(misc-no-recursion)
void CardsInformation::recalc_hands() const
{
  recalc_possible_hands();
  recalc_estimated_hands();
  hands_outdated = false;
}

/** updates the possible hands
 **/
void
CardsInformation::recalc_possible_hands() const
{
  for (auto const& p : of_player_)
    possible_hands_[p.playerno()] = p.possible_hand();
} // void CardsInformation::recalc_possible_hands()


// NOLINTNEXTLINE(misc-no-recursion)
void CardsInformation::recalc_estimated_hands() const
{
#ifdef CHECK_RUNTIME
  auto const ssp = (   (this == &player().cards_information())
                    ? ::runtime["ai recalc estimated hands"].start_stop_proxy()
                    : nullptr);
#endif
  // overview
  // 1) estimated for each player the hand, according to the weighting
  // 2) make sure, that all cards are distributed
  // 3a) check that all pairs of players have enough cards between them
  // 3b) check that no player has more cards the other players do not have,
  //     than he can have
  // 4) check for full estimated hands
  // 5) check, that the estimated hands do not differ if the hands are set
  //    (this uses recursion till depth 1)
  // If no valid estimation is found, the possible hands are used.

  try {
    { // 1) estimated for each player the hand, according to the weighting
      for (auto const& p : of_player_)
        estimated_hands_[p.playerno()] = p.estimated_hand();
    } // 1) estimated for each player the hand, according to the weighting

    { // 2) make sure, that all cards are distributed
      for (auto card : game().rule().cards()) {
        auto sum = played(card);
        for (auto const& p : game().players()) {
          sum += estimated_hands_[p.no()].count(card);
        }

        for ( ;sum < game().rule(Rule::Type::number_of_same_cards); ++sum) {
          // maximal weighting
          int max_weighting = INT_MIN;
          // player with the maximum weighting
          Player const* max_player = nullptr;
          for (auto const& p : game().players()) {
            if (estimated_hands_[p.no()].count(card)
                < of_player(p).can_have(card)) {
              if (   (of_player(p).weighting(card) > max_weighting)
                  && (cardno_to_play(p)
                      > estimated_hands_[p.no()].count(card)) ) {
                max_weighting = of_player(p).weighting(card);
                max_player = &p;
              }
            } // if (player can have another card)
          } // for (p \in game.players)

          // This can happen when tricks are forgotten.
          // P.e. if only one player still has 'card' and still has one card to play
          // he cannot have both forgotten club aces.
          if (!max_player)
            break;

          estimated_hands_[max_player->no()].add(card);
          // ToDo: check, how many times this loop is used more than ones
        } // for (sum < game().rule(Rule::Type::number_of_same_cards))

      } // for (card : rule.cards())
    } // 2) make sure, that all cards are distributed

    { // 3a) check that no player has more cards the other players do not have, than he can have
      for (auto const& player : game().players()) {
        if (of_player(player).all_known())
          continue;
        unsigned sum = 0;
        for (auto const& card : game().rule().cards()) {
          int s = remaining(card);
          for (auto const& p : game().players()) {
            if (p != player)
              s -= estimated_hands_[p.no()].count(card);
          }
          if (s > 0)
            sum += s;
        } // for (card)

        // ToDo: add cards to the other players
        if (sum > cardno_to_play(player)) {
#ifdef WORKAROUND
          THROW_INVALID_GAME_IF(false,
                                "recalc_estimated_hands()\n"
                                "  sum = " << sum << " > " << cardno_to_play(player) << " = cardno_to_play(player)");
#else
          // add cards to the other players
#endif
        }
      } // for (player)
    } // 3a) check that no player has more cards the other players do not have, than he can have
    { // 3b) check that all pairs of players have enough cards between them
      for (auto p1 = game().players().begin();
           p1 != game().players().end();
           ++p1) {
        if (of_player(*p1).all_known())
          continue;
        auto p2 = p1;
        ++p2;
        for (; p2 != game().players().end(); ++p2) {
          if (of_player(*p2).all_known())
            continue;
          unsigned sum = 0;
          for (auto const& card : game().rule().cards()) {
            sum += min(estimated_hands_[p1->no()].count(card)
                       + estimated_hands_[p2->no()].count(card),
                       remaining(card));
          } // for (card)
          if (sum < cardno_to_play(*p1) + cardno_to_play(*p2)) {
            // ToDo: add cards until it matches
            // for that, we also need the WeightedCardList for the estimated hand of the OfPlayer
#ifdef WORKAROUND
            THROW_INVALID_GAME_IF(false,
                                  "recalc_estimated_hands()\n"
                                  "sum = " << sum << " < " << cardno_to_play(*p1) << " + " << cardno_to_play(*p2)
                                  << " = cardno_to_play(*p1) + cardno_to_play(*p2)");
#endif
          }
        } // for (p2)
      } // for (p1)

    } // 3b) check that all pairs of players have enough cards between them

    { // 4) check for full estimated hands
      // if all cards of a player are estimated,
      // the other players cannot have the cards

      // the number of known distributed cards
      map<Card, unsigned> known_cards; // Replace with a vector
      for (auto const& p : game().players()) {
        auto const& hand = estimated_hands_[p.no()];
        if (hand.cardsnumber() == cardno_to_play(p)) {
          for (auto const& c : hand.cards())
            known_cards[c] += 1;
        } // if (full hand estimated
      } // for (p \in game().players)

      // whether the estimated hands will change
      bool changed = false;
      // cards information which takes the full estimated hands into account
      CardsInformation cards_information(*this);
      cards_information.set_auto_update(false);

      // check, that a card is not in two full estimated hands
      for (auto const& c : known_cards) {
        // ToDo: move in upper loop
        auto const& card = c.first;
        // number of known distributed cards of 'c'
        auto const n = (played(card)
                        + c.second);
        if (n > game().rule(Rule::Type::number_of_same_cards)) {
          changed = true;
          // decrease the weighting for the card
          for (auto const& p : game().players()) {
            cards_information.of_player(p).cards_weighting_[card] -= 10; // *Value*
          }
        } // if (n > game().rule(Rule::Type::number_of_same_cards))
      } // for (c : known_cards)

      if (!changed) {
        // remove the known cards in the new cards information
        for (auto const& p : game().players()) {
          auto const& hand = estimated_hands_[p.no()];
          if (hand.cardsnumber() != cardno_to_play(p)) {
            // ToDo: replace by 'set_hand()' ?
            for (auto const& c : known_cards) {
              Card const card = c.first;
              // number of known distributed cards of 'c'
              unsigned const n = (played(card)
                                  + c.second);
              cards_information.of_player(p).add_can_have(card, game().rule(Rule::Type::number_of_same_cards) - n + of_player(p).must_have(card));
              if (hand.count(card) + n > game().rule(Rule::Type::number_of_same_cards))
                changed = true;
            } // for (c \in known_cards)
          } // if (full hand not known)
        } // if (!changed)
      } // for (p : game().players())

      if (changed) {
        // recursion

        // only take one recursion depth
        THROW_INVALID_GAME_IF(this != &player().cards_information(),
                              "recalc_estimated_hands(): recursion depth > 1");

        // use the estimated hands from the modified cards information
        cards_information.do_update();
        for (auto const& p : game().players()) {
          estimated_hands_[p.no()]
            = cards_information.estimated_hand(p);
        } // for (p \in game().players)
        return ;
      } // if (changed)

    } // 4) check for full estimated hands

    { // 5) check, that the estimated hands do not differ if the hands are set
      CardsInformation cards_information(*this);
      cards_information.set_auto_update(false);
      for (auto const& p : game().players()) {
        cards_information.set_hand(p, estimated_hands_[p.no()]);
      }
      // estimate the hand (only in first recursion depth)
      /// @todo   if the do_update throws an exception, normalize the weightings
      if (this == &player().cards_information())
        cards_information.do_update();

      // whether the estimated hands differ
      bool differ = false;
      for (auto const& p : game().players()) {
        if (cards_information.of_player(p).estimated_hand()
            != estimated_hands_[p.no()]) {
          differ = true;
          break;
        }
      }

      if (differ) {
        // recursion

        // only take one recursion depth
        THROW_INVALID_GAME_IF(this != &player().cards_information(),
                              "recalc_estimated_hands(): recursion depth > 1");

        // use the estimated hands from the modified cards information
        cards_information.do_update();
        for (auto const& p : game().players()) {
          estimated_hands_[p.no()]
            = cards_information.estimated_hand(p);
        } // for (p \in game().players)
      } // if (differ)

    } // 5) check, that the estimated hands do not differ if the hands are set

  } catch (InvalidGameException const& exception) {
    // could not create estimated hands -- do no estimation
    estimated_hands_ = possible_hands_;
  } // try
} // void CardsInformation::recalc_estimated_hands()

/** updates the estimated hands
 **
 ** @todo      work in progress
 **/
void
CardsInformation::recalc_weightings() const
{
#ifdef CHECK_RUNTIME
  auto const ssp = (  (this == &player().cards_information())
                    ? ::runtime["ai recalc weightings"].start_stop_proxy()
                    : nullptr);
#endif
  // overview
  // * Rebuild a new cards information for the start of the game with 'must have' and 'cannot have' from the gameplay.
  // * Replay the game and update the weighting (using the information of further played cards)

  return ;

  // create a virtual game

  // virtual players
  vector<unique_ptr<Player>> player_virt;
  {
    // Create new players.
    // The hand is set during the recursion.
    for (auto const& player : game().players()) {
      player_virt.push_back(this->player().clone());
      static_cast<Person&>(*player_virt.back()).set_name(player.name());
      player_virt.back()->set_team(maybe_to_real(this->player().team_information().team(player_virt.size() - 1)));
    } // for (player)

  } // create a virtual game
  Game virt_game(game(), player_virt);
  virt_game.reset_to_first_trick();

  // build a new cards information for the start of the game
  CardsInformation cards_information(*player_virt[player().no()]);

  // replay the game (with announcements!)
  // for each game action do game action

  // copy the cards information
} // void CardsInformation::recalc_weightings() const

/** @return   the number of forgotten (unknown) cards
 **/
unsigned
CardsInformation::forgotten_cards_no() const
{
  return played(Card::unknown);
}

/** @return   how many cards have been played in total in the game
 **/
unsigned
CardsInformation::played_cards_no() const
{
  return played_.cards_no();
} // unsigned CardsInformation::played_cards_no() const

/** -> result
 **
 ** @param    card   card to look at
 **
 ** @return   how many of 'card' have been played
 **/
unsigned
CardsInformation::played(Card const card) const
{
  return (played_)[card];
} // unsigned CardsInformation::played(Card card) const

/** -> result
 **
 ** @param    color   color of the card
 ** @param    value   color of the card
 **
 ** @return   how many of 'Card(color, value)' have been played
 **/
unsigned
CardsInformation::played(Card::Color const color,
                         Card::Value const value) const
{
  return played(Card(color, value));
} // unsigned CardsInformation::played(Card::Color color, Card::Value value) const

/** @return   how many cards of 'tcolor' have been played
 **/
unsigned
CardsInformation::played(Card::TColor const tcolor) const
{
  DEBUG_ASSERTION(tcolor != Card::unknowncardcolor,
                  "CardsInformation::played(" << tcolor << ")\n"
                  "  tcolor 'unknown' is forbidden");
  if (tcolor == Card::trump) {
    unsigned n = 0;
    for (auto const& c : game().rule().cards())
      if (c.istrump(game()))
        n += played(c);
    return n;
  }
  unsigned n = 0;
  for (auto const v : game().rule().card_values())
    if (!Card(tcolor, v).istrump(game()))
      n += played(Card(tcolor, v));
  return n;
}

/** @return   how many cards of 'value' have been played
 **/
unsigned
CardsInformation::played(Card::Value const value) const
{
  DEBUG_ASSERTION(value != Card::unknowncardvalue,
                  "CardsInformation::played(" << value << ")\n"
                  "  value 'unknown' is forbidden");
  unsigned n = 0;
  for (auto color : game().rule().card_colors())
    n += played(Card(color, value));
  return n;
}

/** -> result
 **
 ** @param    player   player
 **
 ** @return   how many cards the player still has to play
 **/
unsigned
CardsInformation::cardno_to_play(Player const& player) const
{
  if (game().tricks().empty())
    return game().tricks().max_size();

  return (game().tricks().max_size() - game().tricks().real_current_no()
          - (game().tricks().current().has_played(player) ? 1 : 0));
} // unsigned CardsInformation::cardno_to_play(Player player) const

/** @return   how many cards are still to be played
 **/
unsigned
CardsInformation::remaining_cards_no() const
{
  return (game().tricks().max_size() * game().playerno()
          - played_cards_no());
} // unsigned CardsInformation::remaining_cards_no() const

/** -> result
 **
 ** @param    card   card to check
 **
 ** @return   how many of 'card' are still to be played
 **/
unsigned
CardsInformation::remaining(Card const card) const
{
  if (card == Card::unknown)
    return 0;
  return (game().rule(Rule::Type::number_of_same_cards)
          - played(card));
}

/** @return   how many of 'color', 'value' are still to be played
 **/
unsigned
CardsInformation::remaining(Card::Color const color, Card::Value const value) const
{
  return remaining(Card(color, value));
}

/** @return   how many trumps are still in the game
 **/
unsigned
CardsInformation::remaining_trumps() const
{
  return remaining(Card::trump);
} // unsigned CardsInformation::remaining_trumps() const

/** @return   how many cards of 'tcolor' are still to be played
 **/
unsigned
CardsInformation::remaining(Card::TColor const tcolor) const
{
  return ( (game().cards().count_unique(tcolor)
            * game().rule(Rule::Type::number_of_same_cards))
          - played(tcolor));
}

/** @return   how many cards of 'value' are still to be played
 **/
unsigned
CardsInformation::remaining(Card::Value const value) const
{
  return ( (game().rule(Rule::Type::number_of_card_colors)
            * game().rule(Rule::Type::number_of_same_cards))
          - played(value));
}

/** @return   the remaining cards of the other players of 'tcolor'
 **/
vector<Card>
CardsInformation::remaining_cards_of_others(Card::TColor const tcolor) const
{
  vector<Card> cards;
  auto const& hand = player().hand();

  for (auto const card : game().cards().cards(tcolor)) {
    int n = 2;
    n -= played(card);
    n -= hand.count(card);
    for (int i = 0; i < n; ++i)
      cards.push_back(card);
  }

  return cards;
}

/** @return   how many cards the other players do have
 **/
unsigned
CardsInformation::remaining_others_cards_no() const
{
  if (cardno_to_play(player())
      == player().hand().cardsnumber())
    return (remaining_cards_no()
            - player().hand().cardsnumber());
  else
    return remaining_cards_no();
} // unsigned CardsInformation::remaining_others_cards_no() const

/** -> result
 **
 ** @param    card   the card
 **
 ** @return   how many cards 'card' are played or known
 **/
unsigned
CardsInformation::known(Card const card) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  unsigned result = played(card);
  for (auto const& p : of_player_)
    result += p.must_have(card);

  return result;
} // unsigned CardsInformation::known(Card card) const

/** -> result
 **
 ** @param    card   the card
 **
 ** @return   how many cards 'card' are remaining and unknown
 **/
unsigned
CardsInformation::remaining_unknown(Card const card) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  return (game().rule(Rule::Type::number_of_same_cards)
          - known(card));
} // unsigned CardsInformation::remaining_unknown(Card card) const

/** -> result
 **
 ** @param    card   the card
 **
 ** @return   the joined "can have" information of both players
 **/
CardCounter
CardsInformation::joined_can_have(OfPlayer const& p1, OfPlayer const& p2) const
{
  auto can_have = p1.can_have_;
  for (auto const& c : p2.can_have_) {
    auto const& card = c.first;
    can_have.add(card, c.second);
    can_have.max_set(card,
                     game().rule(Rule::Type::number_of_same_cards)
                     - played(card));
    // ToDo: check must have of other players
  } // for (c)

  return can_have;
} // CardCounter CardsInformation::joined_can_have(OfPlayer p1, OfPlayer p2) const

/** @return   how many trumps the other players do still have
 **/
unsigned
CardsInformation::remaining_trumps_others() const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  return remaining_others(Card::trump);
} // unsigned CardsInformation::remaining_trumps_others() const

/** -> result
 **
 ** @param    card   card to check
 **
 ** @return   how many of 'card' the other players do have
 **/
unsigned
CardsInformation::remaining_others(Card const card) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  if (cardno_to_play(player())
      == player().hand().cardsnumber())
    return (remaining(card)
            - player().hand().count(card));
  else
    return remaining(card);
} // unsigned CardsInformation::remaining_others(Card card) const

/** -> result
 **
 ** @param    color   color of the card to check
 ** @param    value   value of the card to check
 **
 ** @return   how many of Card(color, value) the other players do have
 **/
unsigned
CardsInformation::remaining_others(Card::Color const color,
                                   Card::Value const value) const
{
  return remaining_others(Card(color, value));
} // unsigned CardsInformation::remaining_others(Card::Color color, Card::Value value) const

/** -> result
 **
 ** @param    tcolor   tcolor to check
 **
 ** @return   how many cards of 'tcolor' the other players do have
 **/
unsigned
CardsInformation::remaining_others(Card::TColor const tcolor) const
{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  if (cardno_to_play(player())
      == player().hand().cardsnumber())
    return (remaining(tcolor)
            - player().hand().count(tcolor));
  else
    return remaining(tcolor);
}

/** @return   how many cards of 'value' the other players do have
 **/
unsigned
CardsInformation::remaining_others(Card::Value const value) const

{
#ifdef UPDATE_ON_DEMAND
  do_update();
#endif

  if (cardno_to_play(player())
      == player().hand().cardsnumber())
    return (remaining(value)
            - player().hand().count(value));
  else
    return remaining(value);
}

/** the game starts
 **/
void
CardsInformation::game_start()
{
#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai cards information"].start_stop_proxy();
#endif

  // reset (needed in poverty: swines owner can change)
  reset();

  if (player_)
    set_hand(player(), player().hand());

  try {
    if (   dynamic_cast<Ai const&>(player()).value(Aiconfig::Type::hands_known)
        && game().status() >= Game::Status::reservation
        && game().status() != Game::Status::redistribute) {
      for (auto const& player : game().players()) {
        set_hand(player, player.hand());
      }
    }
  } catch (std::bad_cast const&) {
  }

#ifndef WITH_DOKOLOUNGE
  // In der Doko-Lounge wird nur bekannt gegeben, ob die Armut überhaupt Trumpf erhalten hat, aber nicht wie viele Trumpf.
  // Die Dokolounge kümmert sich selber um die Informationen zu einer Armut
  if (game().type() == GameType::poverty) {
    if (player() == game().players().soloplayer()) {
      // the soloplayer knows his shifted cards
      auto known_cards = game().poverty().shifted_cards();
      for (auto const& c : game().poverty().returned_cards()) {
        if (std::find(known_cards.begin(), known_cards.end(), c)
            != known_cards.end())
          known_cards.erase(std::find(known_cards.begin(), known_cards.end(),
                                      c));
      }
      of_player(game().players().poverty_partner()).add_must_have(known_cards);
    } else if (player() == game().players().poverty_partner()) {
      // the poverty partner knows the returned cards
      auto& of_player = this->of_player(game().players().soloplayer());
      of_player.add_must_have(game().poverty().returned_cards());
      // the poverty player cannot have any other card
      for (auto const& c : game().cards().trumps())
        of_player.add_can_have(c, of_player.must_have(c));

    } else {
      // the other players know the returned trumps
      of_player(game().players().soloplayer()).add_must_exactly_have(Card::trump, game().poverty().returned_trumpno());
    }
  } // if (game().type() == GameType::poverty)
#endif

  // Take care of swines when the game is started,
  // else I get problems with shifted swines.
  if (game().swines().swines_announced())
    swines_announced(game().swines().swines_owner());
  if (game().swines().hyperswines_announced())
    hyperswines_announced(game().swines().hyperswines_owner());

  for (auto& p : of_player_)
    p.game_start();

  if (auto_update())
    do_update();
} // void CardsInformation::game_start()

/** the trick is full
 ** update the color runs
 **
 ** @param    trick   full trick
 **/
void
CardsInformation::trick_full(Trick const& trick)
{
  color_runs_[trick.startcard().tcolor()] += 1;
} // void CardsInformation::trick_full(Trick trick)

/** 'card' has been played - update the information
 **
 ** @param    card   played card
 **/
void
CardsInformation::card_played(HandCard const card)
{
  card_played(card, card.game().tricks().current());
} //  void CardsInformation::card_played(HandCard card)

/** 'card' has been played - update the information
 **
 ** @param    card   played card
 ** @param    trick   current trick
 **/
void
CardsInformation::card_played(HandCard const card, Trick const& trick)
{
#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai cards information"].start_stop_proxy();
#endif

  played_.inc(card);
  tcolor_played_.inc(card.tcolor());
  DEBUG_ASSERTION(card.is_unknown()
                  || (played(card)
                      <= game().rule(Rule::Type::number_of_same_cards)),
                  "CardsInformation::card_played(" << card << ")\n"
                  "  card is played more often than it is in the game:\n"
                  << "  " << played(card) << " > "
                  << game().rule(Rule::Type::number_of_same_cards));

  of_player(card.player()).card_played(card, trick);

  if (card.is_unknown()) {
    return ;
  }

  if (in_recalcing) {
    return ;
  }

  if (card.tcolor() != game().tricks().current().startcard().tcolor()) {
    of_player(card.player()).add_cannot_have(game().tricks().current().startcard().tcolor());
  }

  queue_update(card);
  queue_update(card.tcolor());

  if (auto_update())
    do_update();

  of_player(card.player()).weight_played_card(card, trick);
} // void CardsInformation::card_played(HandCard card, Trick trick)

/** an announcement has been made - update the information
 **
 ** @param    announcement   made announcement
 ** @param    player      player who has made the announcement
 **/
void
CardsInformation::announcement_made(Announcement const announcement,
                                    Player const& player)
{
  // in a virtual game the distribution of the remaining club queens does not
  // depend on the team, so we can get no more information
  if (game().isvirtual())
    return ;

  if (game().type() != GameType::normal)
    return ;

  if (game().rule(Rule::Type::knocking))
    return ;

#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai cards information"].start_stop_proxy();
#endif

  auto const team = game().teaminfo().get(player);
  if (team == Team::re) {
    if (   of_player(player).played(Card::club_queen) == 0
        && forgotten_cards_no() == 0) {
      of_player(player).add_must_have(Card::club_queen);
    }
  } else if (team == Team::contra) {
    of_player(player).add_cannot_have(Card::club_queen);
  } else {
    DEBUG_ASSERTION(false,
                    "CardsInformation::announcement_made("
                    << announcement << ", " << player.no() << "):\n"
                    "  team of the player not known in the game: "
                    << team);
  }

  queue_update(Card::club_queen);

  if (auto_update())
    do_update();
} // void CardsInformation::announcement_made(Announcement announcement, Player player)

/** a player has announced swines - update the information
 **
 ** @param    player   player who has swines
 **/
void
CardsInformation::swines_announced(Player const& player)
{
#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai cards information"].start_stop_proxy();
#endif

  if (game().status() <= Game::Status::start) {
    // this is needed in the case that the swines are shifted or when 'game_start' is not called in a color solo
    recalc();
  }

  auto const swine = game().cards().swine();

  if (game().rule(Rule::Type::swine_only_second))
    of_player(player).add_must_have(swine, 1);
  else
    of_player(player).add_must_have(swine, 2);

  for (auto& p : of_player_)
    if (p.player() != player)
      p.add_cannot_have(swine);

  if (auto_update())
    do_update();
} // void CardsInformation::swines_announced(Player player)

/** a player has announced hyperswines - update the information
 **
 ** @param    player   player who has hyperswines
 **/
void
CardsInformation::hyperswines_announced(Player const& player)
{
  // Wait till the game is started, else I get problems with shifted swines.
  if (game().status() <= Game::Status::reservation)
    return ;

#ifdef CHECK_RUNTIME
  auto x = ::runtime["ai cards information"].start_stop_proxy();
#endif

  if (game().status() <= Game::Status::start) {
    // this is needed in the case that the swines are shifted or when 'game_start' is not called in a color solo
    recalc();
  }

  auto const hyperswine = game().cards().hyperswine();
  of_player(player).add_must_have(hyperswine, 2);
  for (auto& p : of_player_)
    if (p.player() != player)
      p.add_cannot_have(hyperswine);

  if (auto_update())
    do_update();
} // void CardsInformation::hyperswines_announced(Player player)


/** self check
 ** when an error is found, an ASSERTION is created
 **
 ** @return   whether the self-check was successful (no error)
 **/
bool
CardsInformation::self_check() const
{
  if (in_recalcing)
    return true;
  if (!cards_to_update.empty())
    return false;

#ifdef WORKAROUND
  // We get problems with 'forgotten cards no'
  // because the last trick is closed but no further cards are forgotten.
  if (game().is_finished())
    return true;
#endif

  { // the number of played cards must not be
    // greater than the number of played cards in the game and
    // less than the number of played cards in the game
    //      minus the number of forgotten cards
    if (played_.cards_no() != game().cards().count_played()) {
      DEBUG_ASSERTION(false,
                      "CardsInformation::self_check():\n"
                      "  played cards no "
                      << " = " << played_.cards_no()
                      << " != " << game().cards().count_played()
                      << " = game.cards().count_played()\n"
                      << "  trick = " << game().tricks().real_current_no());
      return false;
    }
  } // the number of played cards
  { // the number of played tcolors must not be
    // greater than the number of played tcolors in the game and
    // less than the number of played tcolors in the game
    //      minus the number of forgotten tcolors
    if (tcolor_played_.cards_no() != game().cards().count_played()) {
      DEBUG_ASSERTION(false,
                      "CardsInformation::self_check():\n"
                      "  played tcolor no + forgotten tcolor no"
                      << " = " << tcolor_played_.cards_no()
                      << " != " << game().cards().count_played()
                      << " = game.cards().count_played()\n"
                      << "  trick = " << game().tricks().real_current_no() << '\n'
                      << tcolor_played_
                     );
      return false;
    }
  } // the number of played tcolors

  // the number of played cards in the game
  for (auto const& p : played_) {
    { // every card is at max played as many times as it is in the game
      if (   !p.first.is_unknown()
          && (p.second > game().rule(Rule::Type::number_of_same_cards)) ) {
        DEBUG_ASSERTION(p.first.is_unknown(),
                        "CardsInformation::self_check():\n"
                        "  card '" << p.first << "' has been played "
                        << p.second << " times.\n"
                       );
        return false;
      }
    } // every card is at max played as many times as it is in the game
    { // the number of played in total is the sum of played over the players
      unsigned n = 0;
      for (auto const& f : of_player_)
        n += f.played(p.first);

      if (p.second != n) {
        DEBUG_ASSERTION(false,
                        "CardsInformation::self_check():\n"
                        "  card '" << p.first << "' has been played "
                        << p.second << " times, but the players say "
                        << n << " times.\n"
                       );
        return false;
      }
    } // the number of played in total is the sum of played over the players
  } // for (p : played_)

  { // the number of played cards of a tcolors does not exceed the number in the game
    for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
      DEBUG_ASSERTION(tcolor_played_[static_cast<Card::TColor>(c)]
                      <= game().cards().count(static_cast<Card::TColor>(c)),
                      "CardsInformation::self_check():\n"
                      "  tcolor '" << static_cast<Card::TColor>(c) << "' has been played "
                      << game().cards().count(static_cast<Card::TColor>(c)) << " times.\n"
                     );
  } // the number of played cards of a tcolors does not exceed the number in the game

  { // every card is either played or a player can have it or it is forgotten
    auto const& cards = game().rule().cards();

    for (auto const c : cards) {
      auto n = played(c);
      for (auto const& f : of_player_)
        n += f.can_have(c);

      n += forgotten_cards_no();

#ifdef DEBUG_ASSERT
      if (   n < game().rule(Rule::Type::number_of_same_cards)
          && game().tricks().current_no() != UINT_MAX) {
        cerr << "current trick\n";
        cerr << game().tricks().current() << endl;
        cerr << *this << endl;
      }
#endif
      DEBUG_ASSERTION((n >= game().rule(Rule::Type::number_of_same_cards)),
                      "CardsInformation::self_check():\n"
                      "  card '" << c << "' is only distributed "
                      << n << " times, must be "
                      << game().rule(Rule::Type::number_of_same_cards)
                      << " times.\n"
                      << "  played: " << played(c)
                      << ", sum can_have = " << (n - played(c)));
    } // for (c \in cards)
  } // every card is either played or a player can have it

  // ToDo: check there are at most as many cards missing as are forgotten

  { // the information of the single players is valid
    for (auto const& f : of_player_)
      if (!f.self_check())
        return false;
  } // the information of the single players is valid

  return true;
} // bool CardsInformation::self_check() const

/** -> result
 **
 ** @param    lhs   first object
 ** @param    rhs   second object
 **
 ** @return   whether the two objects are equal (the player may differ)
 **/
bool
operator==(CardsInformation const& lhs,
           CardsInformation const& rhs)
{
  return ( (   &lhs.player_
            == &rhs.player_)
          && (   lhs.of_player_
              == rhs.of_player_) );
} // bool operator==(CardsInformation lhs, CardsInformation rhs)

/** -> result
 **
 ** @param    lhs   first object
 ** @param    rhs   second object
 **
 ** @return   whether the two objects are different
 **/
bool
operator!=(CardsInformation const& lhs,
           CardsInformation const& rhs)
{
  return !(lhs == rhs);
} // bool operator!=(CardsInformation rhs, CardsInformation rhs)

#ifdef SELF_CHECK
#undef SELF_CHECK
#endif
