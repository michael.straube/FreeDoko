/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../card/card.h"

#include "../basetypes/announcement.h"
#include "../card/hand.h"
#include "../card/card_counter.h"
#include "../card/tcolor_counter.h"

#include "../game/exception.h"

class Player;
class Game;

/**
 ** Contains and analyses the information, who has played which card
 ** and gives the information which cards a player can have.
 **
 ** @author	Diether Knof
 **/
class CardsInformation {
  friend auto operator==(CardsInformation const& lhs, CardsInformation const& rhs) -> bool;
  friend class Player;

  public:
  class OfPlayer;

  public:
  CardsInformation(Player const& player);
  CardsInformation(CardsInformation const& cards_information);
  auto operator=(CardsInformation const& cards_information) = delete;
  ~CardsInformation();


  void write(ostream& ostr) const;
  void write_summary(ostream& ostr) const;

  auto is_virtual() const -> bool;

  void reset();
  void recalc();

  // the corresponding player
  auto player() const -> Player const&;
  auto auto_update() const -> bool;
  void set_auto_update(bool a);

  // the information of single players
  auto operator()(Player const& player) const -> OfPlayer const&;
  auto of_player(Player const& player) const -> OfPlayer const&;
  auto of_player(unsigned playerno)    const -> OfPlayer const&;
  auto of_player(Player const& player)       -> OfPlayer&;
  auto of_player(unsigned playerno)          -> OfPlayer&;

  auto remaining_unknown_players() const -> unsigned;

  auto color_runs(Card::TColor tcolor) const -> unsigned;
  auto color_runs()                    const -> unsigned;
  auto trump_runs()                    const -> unsigned;

  auto possible_hand(Player const& player)  const -> Hand const&;
  auto possible_hand(unsigned playerno)     const -> Hand const&;
  auto estimated_hand(Player const& player) const -> Hand const&;
  auto estimated_hand(unsigned playerno)    const -> Hand const&;

  auto estimated_hands_combinations() const -> vector<Hands>;
  auto weighting(Hands const& hands) const -> int;

  auto highest_remaining_trump()                             const -> HandCard;
  auto highest_remaining_trump_of_others()                   const -> HandCard;
  auto highest_remaining_card_of_others(Card::TColor tcolor) const -> HandCard;
  auto higher_card_exists(HandCard card)                     const -> bool;
  auto higher_cards_no_of_others(HandCard card)              const -> unsigned;

  auto game() const -> Game const&;

  // some settings

  void set_player(Player const& player);
  void set_hand(Player const& player, Hand const& hand);


  // intern methods

  private:
  void queue_update(vector<Card> const& cards);
  void queue_update(Card card);
  void queue_update(Card::TColor tcolor);
  void queue_update_all();

  // the following functions are const, but the variables to update are mutable
  void do_update() const;
  void update_remaining_cards();
  void check_joined_hands();
#ifdef DKNOF
  public:
#endif
  void recalc_hands() const;
  void recalc_possible_hands() const;
  void recalc_estimated_hands() const;
  void recalc_weightings() const;

  public:

  // information

  auto forgotten_tricks_no()                 const -> unsigned;
  auto forgotten_cards_no()                  const -> unsigned;
  auto played_cards_no()                     const -> unsigned;
  auto played(Card card)                     const -> unsigned;
  auto played(Card::Color color, Card::Value value) const -> unsigned;
  auto played(Card::TColor tcolor)           const -> unsigned;
  auto played(Card::Value value)             const -> unsigned;
  auto cardno_to_play(Player const& player)  const -> unsigned;
  auto remaining_cards_no()                  const -> unsigned;
  auto remaining(Card card)                  const -> unsigned;
  auto remaining(Card::Color card, Card::Value value) const -> unsigned;
  auto remaining_trumps()                    const -> unsigned;
  auto remaining(Card::TColor tcolor)        const -> unsigned;
  auto remaining(Card::Value value)          const -> unsigned;
  auto remaining_cards_of_others(Card::TColor tcolor) const -> vector<Card>;
  auto remaining_others_cards_no()           const -> unsigned;
  auto remaining_others(Card card)           const -> unsigned;
  auto remaining_others(Card::Color color, Card::Value value) const -> unsigned;
  auto remaining_trumps_others()             const -> unsigned;
  auto remaining_others(Card::TColor tcolor) const -> unsigned;
  auto remaining_others(Card::Value value)   const -> unsigned;

  auto known(Card card)             const -> unsigned;
  auto remaining_unknown(Card card) const -> unsigned;

  auto joined_can_have(OfPlayer const& p1, OfPlayer const& p2) const -> CardCounter;

  // the information from the game

  void game_start();
  void trick_open(Trick const& trick);
  void trick_full(Trick const& trick);
  void card_played(HandCard card);
  void card_played(HandCard card, Trick const& trick);
  void announcement_made(Announcement announcement, Player const& player);
  void swines_announced(Player const& player);
  void hyperswines_announced(Player const& player);

  private:
#ifdef DKNOF
  public:
#endif
  // checks the data for error
  auto self_check() const -> bool;

  private:
  Player const* player_ = nullptr;
  mutable vector<OfPlayer> of_player_;
  vector<unsigned> color_runs_;
  CardCounter played_;
  TColorCounter tcolor_played_;
  bool in_recalcing = false;
  mutable bool in_update = false;
  mutable vector<Hand> possible_hands_;
  mutable vector<Hand> estimated_hands_;

  // whether to update the dependend cards and recalc the hands automatically
  bool auto_update_ = true;

  mutable set<Card> cards_to_update;

  mutable bool hands_outdated = false;

}; // class CardsInformation

auto operator<<(ostream& ostr, CardsInformation const& cards_information) -> ostream&;
auto operator==(CardsInformation const& lhs, CardsInformation const& rhs) -> bool;
auto operator!=(CardsInformation const& lhs, CardsInformation const& rhs) -> bool;
