/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "cards_information.heuristics.h"
#include "cards_information.of_player.h"
#include "team_information.h"

#include "../card/hand_card.h"
#include "../card/trick.h"
#include "player.h"
#include "team_information.h"
#include "ai/ai.h"

#include "../game/game.h"
#include "../party/rule.h"

// For configuration values search for '*Value*'
// -100 means: shall not have it (p.e. second fox, second queen, color ace when not played)
//  100 means: should have it    (p.e. second dulle)
//
// Info
// Ai::handofplayer() or CardsInformation::estimated_hand() should not be called because of logical (but not technical) recursion. But CardsInformation::possible_hand() can be used.

#ifndef RELEASE
#ifdef DKNOF
// the ai whose information are written
// (what the ai assumes of the other players)
// undefine for no output
#define DEBUG_AI 4
#endif
#endif


#ifdef DEBUG_AI
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CHANGE_WEIGHTING(card, value) \
  (((self.no() == DEBUG_AI) && !self.game().isvirtual()) \
   ? (cout << "weighting change: " << played_card.player().no() << ": " << setw(13) << card << ": " << setw(4) << value << " -> " << (weightings[card] + value) << "   (" << heuristic_name << ")\n") \
   : cout), \
   (weightings[card] += value)
#else
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CHANGE_WEIGHTING(card, value) \
  (void)heuristic_name, \
  weightings[card] += value
#endif

namespace CardsInformationHeuristics {

using CardPlayedHeuristic = void(*)(HandCard, Trick const&, Player const&, map<Card, int>&);

auto trump_card_limit(Player const& self) -> HandCard;

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CARD_PLAYED_HEURISTIC(name) \
void name(HandCard played_card, \
          Trick const& trick, \
          Player const& self, \
          map<Card, int>& weightings)

// not started with a color ace
CARD_PLAYED_HEURISTIC(no_color_ace);
// a color trick has been served
CARD_PLAYED_HEURISTIC(served_color);
// a color trick has been jabbed (first run)
CARD_PLAYED_HEURISTIC(color_trick_jabbed_first_run);
// the last player has played a pfund
CARD_PLAYED_HEURISTIC(last_player_played_pfund);

// the last player in a trump trick (player has jabbed)
CARD_PLAYED_HEURISTIC(last_player_jabbed_trump);
// in a trump trick (player has not jabbed)
CARD_PLAYED_HEURISTIC(not_jabbed_trump);
// the player has served a trump trick
CARD_PLAYED_HEURISTIC(served_trump);

// the player has thrown a trump in a color trick
CARD_PLAYED_HEURISTIC(threw_trump_in_color_trick);
// the player has thrown a color in a color trick
CARD_PLAYED_HEURISTIC(threw_color_in_color_trick);

// the first player has played a pfund
CARD_PLAYED_HEURISTIC(first_player_played_pfund);
// the first player has played a club queen
CARD_PLAYED_HEURISTIC(first_player_played_club_queen);

// the bride in the marriage plays a pfund in the decision trick
CARD_PLAYED_HEURISTIC(bride_pfunds_in_decision_trick);

// the player has no second diamond ace because he has not announced swines
CARD_PLAYED_HEURISTIC(no_swines);
// assumption: no silent marriage
CARD_PLAYED_HEURISTIC(no_silent_marriage);

// the player will start with a dulle, if he has the second one
CARD_PLAYED_HEURISTIC(both_dullen);

// the re player in a poverty starts with a trump
CARD_PLAYED_HEURISTIC(poverty_re_starts_with_trump);

// the soloplayer will probably start with a ten when he has both aces
CARD_PLAYED_HEURISTIC(soloplayer_startcard_color);
// the soloplayer will probably start with a ten when he has both aces
CARD_PLAYED_HEURISTIC(soloplayer_startcard_color);

// the re player has played the second best trump in a picture solo
CARD_PLAYED_HEURISTIC(picture_solo_re_played_second_best_trump);
CARD_PLAYED_HEURISTIC(picture_solo_re_not_jabbed_trick_with_points);
// a contra player has played a trump in a picture solo
CARD_PLAYED_HEURISTIC(picture_solo_contra_played_trump);

#undef CARD_PLAYED_HEURISTIC


void poverty_returned(Player const& player,
                      Player const& self,
                      map<Card, int>& weightings,
                      unsigned const trumpno)
{
  // Idea:
  // the player who accepts the poverty will have only trump

  auto constexpr heuristic_name = "poverty returned";

  auto const& game = player.game();

  for (auto const& c : game.rule().cards()) {
    if (!c.istrump(game)) {
      HandCard const played_card(player.hand(), c);
      CHANGE_WEIGHTING(c, -80 - 40 * static_cast<int>(trumpno)); // *Value*
    }
  } // for (c : cards)
}


void game_start_picture_solo(Player const& player,
                             Player const& self,
                             map<Card, int>& weightings)
  // picture solo: the contra players will not have much trump
{
  auto const& game = self.game();
  DEBUG_ASSERTION(is_picture_solo(game.type()),
                  "game_start_picture_solo: no picture solo: " << game.type());
  if (!(   player.team() == Team::contra
        && self.team() == Team::contra))
    return ;

  auto const n = self.hand().count(Card::trump);
  auto const& trumps = game.cards().trumps();
  int const trumpno = trumps.size();
  for (auto const& card : trumps) {
    weightings[card] -= n * (40 * 4 / trumpno);
  }
}


void card_played(HandCard const played_card,
                 Trick const& trick,
                 Player const& self,
                 map<Card, int>& weightings)
{
  static vector<CardPlayedHeuristic> heuristic_list = {
    no_color_ace,
    served_color,
    color_trick_jabbed_first_run,
    last_player_played_pfund,
    last_player_jabbed_trump,
    not_jabbed_trump,
    served_trump,
    threw_trump_in_color_trick,
    threw_color_in_color_trick,
    first_player_played_pfund,
    first_player_played_club_queen,
    bride_pfunds_in_decision_trick,
    no_swines,
    no_silent_marriage,
    both_dullen,
    poverty_re_starts_with_trump,
    soloplayer_startcard_color,
    picture_solo_re_played_second_best_trump,
    picture_solo_re_not_jabbed_trick_with_points,
    picture_solo_contra_played_trump
  };

  for (auto const& f : heuristic_list)
    (*f)(played_card, trick, self, weightings);
}


void no_color_ace(HandCard const played_card,
                  Trick const& trick,
                  Player const& self,
                  map<Card, int>& weightings)
{
  // Idea:
  // A player would start with a color ace if he has one.
  // And in the first run of a color he would play his ace.

  // the name of the heuristic
  auto constexpr heuristic_name = "no color ace";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  // a normal game or a marriage
  if (!(   game.type() == GameType::normal
        || game.type() == GameType::marriage))
    return ;

  // startplayer
  if (trick.startplayer() == player) {

    // In a marriage not the solo player,
    // if he determines the marriage with his card.
    if (   (game.type() == GameType::marriage)
        && (player == game.players().soloplayer())
        && is_selector(played_card.tcolor(),
                       game.marriage().selector()) )
      return ;

    // has not played a color ace
    if (played_card.value() == Card::ace)
      return ;

    // If it is the solo player, he will have the color ace, if he played a ten
    if (   game.is_solo()
        && (player == game.players().soloplayer())
        && (played_card.value() == Card::ten)) {
      CHANGE_WEIGHTING(Card(played_card.color(), Card::ace), 50); // *Value*
      return ;
    }

    // For all colors which have not run, yet, the player is assumed to not
    // have the ace.
    // ToDo: check also, that noone has thrown the color
    for (auto const& c : self.game().rule().card_colors())
      if (!Card(c, Card::ace).istrump(game))
        if (!self.cards_information().played(Card(c, Card::ace)))
          CHANGE_WEIGHTING(Card(c, Card::ace),
                           -50 * static_cast<int>(game.cards().count(c)
                                                  - (game.rule(Rule::Type::number_of_same_cards))
                                                 )); // *Value*
  } else { // if !(startplayer)
    // color trick
    if (trick.startcard().istrump())
      return ;

    auto const color = trick.startcard().tcolor();

    // first color run
    if (!(self.cards_information().color_runs(color) == 0))
      return ;

    // player has not played an ace
    if (played_card.value() == Card::ace)
      return ;

    // winner card is no ace and no trump
    if (   (trick.winnercard().value() == Card::ace)
        || trick.winnercard().istrump())
      return ;

    CHANGE_WEIGHTING(Card(color, Card::ace), -100); // *Value*
  } // if !(startplayer)
}


void served_color(HandCard const played_card,
                  Trick const& trick,
                  Player const& self,
                  map<Card, int>& weightings)
{
  // Idea:
  // If no team is known, the player plays the lowest card in a color trick

  // the name of the heuristic
  auto constexpr heuristic_name = "served color";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (trick.startplayer() == player)
    return ;

  if (trick.startcard().istrump())
    return ;

  // color trick served
  if (played_card.tcolor() != trick.startcard().tcolor())
    return ;

  // it is not the winnerplayer
  if (trick.winnerplayer() == player)
    return ;

  // the winnerplayer is not of the team as the player
  // or a higher card (an ace) then the winnercard exists
  // note: 'TeamInfo' makes the opposite assumption:
  //       a high card played is a sign for the same team
  if (   maybe_equal(self.teaminfo(trick.winnerplayer()), self.teaminfo(player))
      && (   trick.winnercard().istrump()
          || trick.winnercard() == Card(played_card.tcolor(), Card::ace)
          || (self.cards_information().played(played_card.tcolor(), Card::ace)
              < game.rule(Rule::Type::number_of_same_cards)) )
     )
    return ;

  // special case: undetermined marriage
  if (   game.marriage().is_undetermined()
      && player == game.players().soloplayer())
    return ;

  // in a poverty: this is the poverty player
  // and the soloplayer is behind
  if (   game.type() == GameType::poverty
      && player == game.players().soloplayer()
      && !trick.has_played(game.players().poverty_partner())) {
    return ;
  }

  // no team of the players behind is known to be of the own team
  for (Player const& p : trick.remaining_following_players()) {
    if (   maybe_equal(self.team_information().team(p), self.team_information().team(player))
        && self.cards_information().estimated_hand(p).can_jab(trick)) {
      return ;
    }
  }

  switch (game.type()) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    switch (played_card.value()) {
    case Card::ace:
      // add value: 80 + 10 * point difference
      if (!Card(played_card.color(), Card::ten).istrump(game))
        CHANGE_WEIGHTING(Card(played_card.color(), Card::ten), -90); // *Value*
      if (!Card(played_card.color(), Card::king).istrump(game))
        CHANGE_WEIGHTING(Card(played_card.color(), Card::king), -150); // *Value*
      if (game.rule(Rule::Type::with_nines))
        if (!Card(played_card.color(), Card::nine).istrump(game))
          CHANGE_WEIGHTING(Card(played_card.color(), Card::nine), -190); // *Value*
      break;
    case Card::ten:
      // add value: 40 + 10 * point difference
      if (!Card(played_card.color(), Card::king).istrump(game))
        CHANGE_WEIGHTING(Card(played_card.color(), Card::king), -140); // *Value*
      if (game.rule(Rule::Type::with_nines))
        if (!Card(played_card.color(), Card::nine).istrump(game))
          CHANGE_WEIGHTING(Card(played_card.color(), Card::nine), -140); // *Value*
      break;
    case Card::king:
      // add value: 40 + 10 * point difference
      if (game.rule(Rule::Type::with_nines))
        if (!Card(played_card.color(), Card::nine).istrump(game))
          CHANGE_WEIGHTING(Card(played_card.color(), Card::nine), -80); // *Value*
      break;
    default:
      break;
    } // switch (played_card.value())
    break;

  default:
    // ToDo
    break;
  } // switch (game.type())
}


void color_trick_jabbed_first_run(HandCard const played_card,
                                  Trick const& trick,
                                  Player const& self,
                                  map<Card, int>& weightings)
{
  // Idea:
  // If the first color run is jabbed, the player will take low trump.

  // the name of the heuristic
  auto constexpr heuristic_name = "color trick jabbed first run";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  auto const color = trick.startcard().color();

  // normal game
  if (   is_solo(game.type())
      || game.type() == GameType::poverty)
    return ;

  // not startplayer
  if (trick.startplayer() == player)
    return ;

  // color trick
  if (trick.startcard().istrump())
    return ;

  // first run or last card
  if (   (self.cards_information().color_runs(color) > 0)
      && !trick.isfull())
    return ;

  // no (following) player has played/thrown the color
  for (unsigned c = trick.actcardno() + 1; c < game.playerno(); ++c)
    if (self.cards_information().of_player(trick.player_of_card(c)).played(color))
      return ;

  // color trick jabbed
  if (!played_card.istrump())
    return ;

  // the cards in the order to play
  vector<pair<Card, int> > cards; // *Value*
  if (!game.swines().swines_announced())
    cards.emplace_back(Card(game.cards().trumpcolor(), Card::ace), -200);
  if (!(   (game.cards().trumpcolor() == Card::heart)
        && game.rule(Rule::Type::dullen)))
    cards.emplace_back(Card(game.cards().trumpcolor(), Card::ten), -100);
  if (!(   !game.rule(Rule::Type::with_nines)
        && game.swines().hyperswines_announced()) )
    cards.emplace_back(Card(game.cards().trumpcolor(), Card::king), -90);
  cards.emplace_back(Card::diamond_jack, -60);
  cards.emplace_back(Card::heart_jack,   -60);
  cards.emplace_back(Card::spade_jack,   -60);
  cards.emplace_back(Card::club_jack,    -40);
  if (   game.rule(Rule::Type::with_nines)
      && !game.swines().hyperswines_announced())
    cards.emplace_back(Card(game.cards().trumpcolor(), Card::nine), -50);
  cards.emplace_back(Card::diamond_queen, -50);
  cards.emplace_back(Card::heart_queen,   -50);
  cards.emplace_back(Card::spade_queen,   -50);

  // search the former winnercard
  Trick former_trick(trick.startplayer());
  for (unsigned c = 0; c < trick.actcardno() - 1; ++c)
    former_trick.add(trick.card(c));

  // the former winnercard
  auto const& former_winnercard = former_trick.winnercard();

  // the weighting
  double weighting = 1;
  // there must be at least one more card remaining
  switch (self.cards_information().remaining(color)
          - (trick.remainingcardno() - 1)) {
  case 0:
  case 1:
    weighting = 0;
    break;
  case 2:
    switch (trick.remainingcardno()) {
    case 0:
      weighting = 1;
      break;
    case 1:
      weighting = 0.9;
      break;
    case 2:
      weighting = 0.7;
      break;
    default:
      weighting = 1;
      break;
    } // switch (trick.remainingcardno())
    break;
  default:
    weighting = 1;
    break;
  } // switch(free color cards)

  for (auto const& card : cards) {
    if (played_card == card.first)
      break;
    else if (former_winnercard.is_jabbed_by({player.hand(), card.first}))
      CHANGE_WEIGHTING(card.first,
                       static_cast<int>(weighting * card.second));
  }
}


void last_player_played_pfund(HandCard const played_card,
                              Trick const& trick,
                              Player const& self,
                              map<Card, int>& weightings)
{
  // Idea:
  // The last player will have played his best pfund if the trick goes to his partner

  // the name of the heuristic
  auto constexpr heuristic_name = "last player played pfund";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (!trick.isfull())
    return ;

  if (trick.winnerplayer() == trick.lastplayer())
    return ;

  // winnerplayer is of the team as the last player
  if (   self.teaminfo(player) == Team::unknown
      || game.marriage().is_undetermined()
      || !maybe_equal(self.team_information().team(player), self.team_information().team(trick.winnerplayer())))
    return ;

  if (played_card.istrump()) {
    auto const trumpcolor = game.cards().trumpcolor();
    if (trumpcolor != Card::nocardcolor) {
      if (played_card != Card(trumpcolor, Card::ace)) {
        if (!HandCard(self.hand(), trumpcolor, Card::ace).is_special())
          CHANGE_WEIGHTING(Card(trumpcolor, Card::ace), -100); // *Value*
        if (played_card != Card(trumpcolor, Card::ten)) {
          if (!HandCard(self.hand(), trumpcolor, Card::ten).is_special())
            CHANGE_WEIGHTING(Card(trumpcolor, Card::ten), -90); // *Value*
        } // if (played_card != ten)
      } // if (played_card != ace)
    } // if (trumpcolor)
  } else { // if !(played_card.tcolor() == Card::trump)
    for (auto v : game.rule().card_values()) {
      if (   (v > played_card.value())
          && !Card(played_card.color(), v).istrump(game))
        CHANGE_WEIGHTING(Card(played_card.color(), v), -100); // *Value*
    }
  } // if !(played_card.tcolor() == Card::trump)
}


void last_player_jabbed_trump(HandCard const played_card,
                              Trick const& trick,
                              Player const& self,
                              map<Card, int>& weightings)
{
  // Idea:
  // If the player has jabbed a trick,
  // he will not have a cards between the former winnercard and his card.

  // the name of the heuristic
  auto constexpr heuristic_name = "last player jabbed trump";

  auto const& player = played_card.player();

  if (!trick.isfull())
    return ;

  if (!(trick.winnerplayer() == player))
    return ;

  // the player does not show himself with a club queen
  if (   (played_card == Card::club_queen)
      && (player.game().type() == GameType::normal)
      && (player.announcement() == Announcement::noannouncement) )
    return ;

  // search the former winnercard
  Trick former_trick(trick.startplayer());
  for (unsigned c = 0; c < trick.actcardno() - 1; ++c)
    former_trick.add(trick.card(c));

  // the former winnercard
  auto const& former_winnercard = former_trick.winnercard();

  // the former winnercard is a trump card
  if (!former_winnercard.istrump())
    return ;

  // the cards between
  auto const cards_between
    = trick.game().cards().cards_between(former_winnercard, played_card);

  for (auto const& c : cards_between)
    CHANGE_WEIGHTING(c, -40); // *Value*
}


void not_jabbed_trump(HandCard const played_card,
                      Trick const& trick,
                      Player const& self,
                      map<Card, int>& weightings)
{
  // Idea:
  // If the player has not jabbed a trick with many points,
  // he will not have a card higher than the winnercard.

  // the name of the heuristic
  auto constexpr heuristic_name = "not jabbed trump";

  auto const& player = played_card.player();

  if (trick.winnerplayer() == player)
    return ;

  // last player: team not totally unknown
  if (   trick.isfull()
      && self.teaminfo(player) == Team::unknown)
    return ;

  if (maybe_equal(self.teaminfo(trick.winnerplayer()), self.teaminfo(player)))
    return ;

  // not: only partners behind, one has a higher card
  if (self.teaminfo(player) != GuessedTeam::unknown) {
    auto const team = self.team_information().team(player);
    bool let_partner = false;
    for (unsigned c = trick.actcardno() + 1; c < trick.game().playerno(); ++c) {
      auto const& p = trick.player_of_card(c);
      if (!maybe_equal(self.team_information().team(p), team)) {
        let_partner = false;
        break;
      }
      if (self.cards_information().possible_hand(p).higher_card_exists(trick.winnercard()))
        let_partner = true;
    } // for (c)

    if (let_partner)
      return ;
  }

  // not poverty, contra player, partner is behind, partner can have higher card
  if (   trick.game().type() == GameType::poverty
      && player.team() == Team::contra
      && !trick.has_played(*self.team_information().partner(player))
      && self.cards_information().possible_hand(*self.team_information().partner(player)).higher_card_exists(trick.winnercard()))
    return ;

  // enough points in the trick
  if (!(trick.points() + 2 * trick.remainingcardno() - played_card.value()
        >= 10)) // *Value*
    return ;

  // the player has not played a pfund
  if (played_card.value() >= 10)
    return ;

  // trump trick
  if (!trick.startcard().istrump())
    return ;

  // winnercard small enough
  if (trick.isfull()) {
    // always take the trick (10 points)
  } else if (trick.islastcard()) {
    // take the trick (6 points), but not with a very high trump
    if (self.trump_card_limit().is_jabbed_by(trick.winnercard()))

      return ;
  } else {
    // take the trick (10 points), but not with a high trump
    if (!trick.isjabbed(self.trump_card_limit()))
      return ;
  }

  // the higher cards
  auto const higher_cards
    = trick.game().cards().higher_cards(trick.winnercard());
  // the modification value
  auto value = -((static_cast<int>(trick.points()
                                   + 5 * trick.remainingcardno())
                  - 6) * 10); // *Value*
  value += -150 * trick.specialpoints().size();

  // ToDo: use 'TrickWeighting' for the value (if > 0)
  for (auto const& c : higher_cards)
    CHANGE_WEIGHTING(c, value);
}


void served_trump(HandCard const played_card,
                  Trick const& trick,
                  Player const& self,
                  map<Card, int>& weightings)
{
  // Idea:
  // If the player has served a trump trick,
  // he will have played his lowest trump.

  // the name of the heuristic
  auto constexpr heuristic_name = "served trump";

  auto const& player = played_card.player();

  if (trick.actcardno() == 1)
    return ;

  if (trick.winnerplayer() == player)
    return ;

  auto const winnerteam = self.team_information().team(trick.winnerplayer());

  // whether the trick goes to the team of the player
  if (   trick.winnerteam() != Team::unknown
      && (winnerteam & self.teaminfo(player)))
    return ;

  // trump trick
  if (!trick.startcard().istrump())
    return ;

  // the lower cards
  auto const lower_cards = trick.game().cards().lower_trumps(played_card);

  // ToDo: use 'TrickWeighting' for the value (if > 0)
  for (auto const& c : lower_cards) {
    if (   (c.value() < played_card.value())
        && (   (c.value() < 10)
            || trick.isfull() ) )
      CHANGE_WEIGHTING(c,
                       -3 * static_cast<int>(c.value()) // *Value*
                       - (HandCard(played_card.hand(), c).isfox()
                          ? 100 : 0)); // *Value*
  } // for (c : lower_cards)
}


void threw_trump_in_color_trick(HandCard const played_card,
                                Trick const& trick,
                                Player const& self,
                                map<Card, int>& weightings)
{
  // Idea:
  // If the player has thrown a trump in a color trick without jabbing,
  // he will not have a color card of such a value or less.

  // the name of the heuristic
  auto constexpr heuristic_name = "threw trump in color trick";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (!(   !trick.startcard().istrump()
        && played_card.istrump()
        && trick.winnercardno() != trick.actcardno() - 1))
    return ;

  { // first: the player will not have a color card with the same value
    for (auto const c : self.game().rule().card_colors()) {
      if (!Card(c, played_card.value()).istrump(game))
        CHANGE_WEIGHTING(Card(c, played_card.value()), -120); // *Value*
    }
  } // first: the player will not have a color card with the same value

  // now check whether the trick goes the the same team as the player

  // last player: the player is not of the same team as the winnerplayer
  if (   trick.isfull()
      && maybe_equal(self.teaminfo(player), self.teaminfo(trick.winnercard().player())))
    return ;

  // third player: the player is not of the same team as the winnerplayer
  // ToDo: check, that the last player cannot overjab
  if (   trick.islastcard()
      && maybe_equal(self.teaminfo(player), self.teaminfo(trick.winnercard().player())))
    return ;

  { // second: the player will not have a color card with a smaller value
    for (auto const& c : game.rule().cards()) {
      if (   c.value() < played_card.value()
          && !c.istrump(game)) {
        CHANGE_WEIGHTING(c, -100); // *Value*
      }
    }
  } // second: the player will not have a color card with a smaller value
}


void threw_color_in_color_trick(HandCard const played_card,
                                Trick const& trick,
                                Player const& self,
                                map<Card, int>& weightings)
{
  // Idea:
  // If the player has thrown a color in a color trick
  // he will not have the color any more

  // the name of the heuristic
  auto constexpr heuristic_name = "threw color in color trick";

  auto const& game = trick.game();

  if (!(   !(   game.type() == GameType::solo_meatless
             || is_picture_solo(game.type())
            )
        && !trick.startcard().istrump()
        && !played_card.istrump()
        && played_card.tcolor() != trick.startcard().tcolor()
        && game.tricks().remaining_no() > 4))
    return ;

  auto const color = played_card.tcolor();
  for (auto const& v : game.rule().card_values()) {
    auto const card = Card(color, v);
    if (!card.istrump(game))  {
      CHANGE_WEIGHTING(card, -100); // *Value*
    }
  }
}


void first_player_played_pfund(HandCard const played_card,
                               Trick const& trick,
                               Player const& self,
                               map<Card, int>& weightings)
{
  // Idea::
  // If the first player has played a pfund, his partner will have the
  // highest card.
  // If there is a club queen remaining and there are no swines/hyperswines,
  // the first player will be re and he has all cards over the club queen.

  // the name of the heuristic
  auto constexpr heuristic_name = "first player played pfund";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  // normal game or marriage
  if (!(   (game.type() == GameType::normal)
        || (game.type() == GameType::marriage) ) )
    return ;

  // the first player in the trick
  if (trick.actcardno() != 1)
    return ;

  // the first player is not contra
  if (self.teaminfo(player) == Team::contra)
    return ;

  // he has played a trump pfund
  if (!(   (played_card.value() >= 10)
        && !game.cards().is_special(played_card)
        && played_card.istrump()) )
    return ;

  // another player has swines / hyperswines
  if (   game.swines().hyperswines_announced()
      && (game.swines().hyperswines_owner() != player) )
    return ;
  if (   game.swines().swines_announced()
      && (game.swines().swines_owner() != player) )
    return ;

  // there is a club queen remaining
  if (self.cards_information().played(Card::club_queen)
      + self.cards_information().of_player(self).must_have(Card::club_queen)
      == game.rule(Rule::Type::number_of_same_cards))
    return ;

  // if a club queen is played, it must be from the player himself
  if (self.cards_information().of_player(player).played(Card::club_queen)
      < self.cards_information().played(Card::club_queen))
    return ;

  // the player has all higher trumps from the club queen ...
  auto const higher_trumps
    = trick.game().cards().higher_cards(Card::spade_queen);

  // ... if the ai does not have it
  for (auto const& c : higher_trumps) {
    // ToDo: check for announced swines/hyperswines, ...
    if (   (c != Card::club_queen)
        && (self.hand().contains(c)) )
      return ;
  }

  for (auto const& c : higher_trumps)
    CHANGE_WEIGHTING(c, 1000); // *Value*
}


void first_player_played_club_queen(HandCard const played_card,
                                    Trick const& trick,
                                    Player const& self,
                                    map<Card, int>& weightings)
{
  // Idea:
  // If the first player has played a club queen, he will have all higher
  // cards and -- if the other club queen has not been played -- no trump
  // pfund.

  // the name of the heuristic
  auto constexpr heuristic_name = "first player played club queen";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (game.type() != GameType::normal)
    return ;

  if (trick.actcardno() != 1)
    return ;

  if (played_card != Card::club_queen)
    return ;

  if (   game.swines().hyperswines_announced()
      && game.swines().hyperswines_owner() != player)
    return ;
  if (   game.swines().swines_announced()
      && game.swines().swines_owner() != player)
    return ;

  // the re partner is not the last player
  if (self.team_information().team(trick.lastplayer()) >= GuessedTeam::maybe_re)
    return ;

  // the player has all higher trumps from the club queen ...
  auto const higher_trumps
    = trick.game().cards().higher_cards(Card::spade_queen);

  // ... if the ai does not have it
  for (auto const& c : higher_trumps) {
    // ToDo: check for announced swines/hyperswines, ...
    if (   c != Card::club_queen
        && self.hand().contains(c))
      return ;
  }

  for (auto const& c : higher_trumps)
    CHANGE_WEIGHTING(c, 1000); // *Value*

  // there is a club queen remaining -> the player does not have a trump pfund
  if (self.cards_information().played(Card::club_queen)
      + self.cards_information().of_player(self).must_have(Card::club_queen)
      < game.rule(Rule::Type::number_of_same_cards)) {
    if (!HandCard(self.hand(), Card::diamond_ace).is_special())
      CHANGE_WEIGHTING(Card::diamond_ace, -500);
    if (!HandCard(self.hand(), Card::diamond_ten).is_special())
      CHANGE_WEIGHTING(Card::diamond_ten, -400);
    if (!HandCard(self.hand(), Card::diamond_king).is_special())
      CHANGE_WEIGHTING(Card::diamond_king, -100);
  }
}


void bride_pfunds_in_decision_trick(HandCard const played_card,
                                    Trick const& trick,
                                    Player const& self,
                                    map<Card, int>& weightings)
{
  // Idea:
  // In the decision trick of a marriage, the bride plays a pfund.

  // the name of the heuristic
  auto constexpr heuristic_name = "bride pfunds in decision trick";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  // the player is a bride in an undetermined marriage and this is the marriage decision trick
  if (!(   game.marriage().is_undetermined()
        && game.players().is_soloplayer(player)
        && is_selector(trick.startcard().tcolor(), game.marriage().selector())
       ) )
    return ;

  // the player has not played a color ace
  if (   !played_card.istrump()
      && (played_card.value() == Card::ace))
    return ;

  // the player (bride) has no better pfund
  if (played_card.istrump()) {
    auto const trumpcolor = game.cards().trumpcolor();
    if (played_card != Card(trumpcolor, Card::ace)) {
      if (!HandCard(self.hand(), trumpcolor, Card::ace).is_special())
        CHANGE_WEIGHTING(Card(trumpcolor, Card::ace), -100); // *Value*
      if (played_card != Card(trumpcolor, Card::ten)) {
        if (!HandCard(self.hand(), trumpcolor, Card::ten).is_special())
          CHANGE_WEIGHTING(Card(trumpcolor, Card::ten), -90); // *Value*
      } // if (played_card != ten)
    } // if (played_card != ace)
  } else {
    for (auto const v : game.rule().card_values()) {
      if (   (v > played_card.value())
          && !Card(played_card.color(), v).istrump(game))
        CHANGE_WEIGHTING(Card(played_card.color(), v), -100); // *Value*
    }

    if (played_card.value() == Card::nine)
      CHANGE_WEIGHTING(Card(played_card.color(), Card::nine), -100); // *Value*
  } // if !(played_card.tcolor() == Card::trump)
}


void no_swines(HandCard const played_card,
               Trick const& trick,
               Player const& self,
               map<Card, int>& weightings)
{
  // Idea:
  // If the player has played a fox and has not announced swines,
  // he has no second fox.
  // Same idea for hyperswines

  // the name of the heuristic
  auto constexpr heuristic_name = "no swines";

  auto const& game = trick.game();

  if (   game.rule(Rule::Type::swines)
      && (played_card == game.cards().swine())
      && !game.rule(Rule::Type::swine_only_second)
      && (self.cards_information().of_player(self).played(played_card) == 1)
      && (  (game.type() == GameType::poverty)
          ? game.rule(Rule::Type::swines_in_poverty)
          : is_solo(game.type())
          ? game.rule(Rule::Type::swines_in_solo)
          : true)) {
    CHANGE_WEIGHTING(played_card, -300); // *Value*
  }

  {
    // the name of the heuristic
    auto constexpr heuristic_name = "no hyperswines";
    if (   game.rule(Rule::Type::hyperswines)
        && (played_card == game.cards().hyperswine())
        && (self.cards_information().of_player(self).played(played_card) == 1)
        && game.swines().swines_announced()
        && (   game.rule(Rule::Type::swines_and_hyperswines_joint)
            || (game.swines().swines_owner() != self))
        && (  (game.type() == GameType::poverty)
            ? game.rule(Rule::Type::hyperswines_in_poverty)
            : is_solo(game.type())
            ? game.rule(Rule::Type::hyperswines_in_solo)
            : true)) {
      CHANGE_WEIGHTING(played_card, -300); // *Value*
    }
  }
}


void no_silent_marriage(HandCard const played_card,
                        Trick const& trick,
                        Player const& self,
                        map<Card, int>& weightings)
{
  // Idea:
  // If the player has played one club queen he will not have a second.
  // We assume he does not play a silent marriage

  // the name of the heuristic
  auto constexpr heuristic_name = "no silent marriage";

  if (!(   trick.game().type() == GameType::normal
        && played_card == Card::club_queen))
    return ;

  CHANGE_WEIGHTING(played_card, -200); // *Value*
}


void both_dullen(HandCard const played_card,
                 Trick const& trick,
                 Player const& self,
                 map<Card, int>& weightings)
{
  // Idea:
  // second dulle over first: the player will start with the first dulle only if he has the second

  // the name of the heuristic
  auto constexpr heuristic_name = "both dullen";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  // second dulle over first
  if (!game.rule(Rule::Type::dullen_second_over_first))
    return ;

  if (!played_card.isdulle())
    return ;

  // first card
  if (trick.actcardno() == 1) {
    if (player.cards_to_play() <= 4)
      CHANGE_WEIGHTING(played_card, 40 + 40 * player.cards_to_play()); // *Value*
    else
      CHANGE_WEIGHTING(played_card, 200); // *Value*
  }

  // second card
  if (trick.actcardno() == 2) {
    if (player.cards_to_play() <= 4)
      CHANGE_WEIGHTING(played_card, 40 * player.cards_to_play() - 20); // *Value*
    else
      CHANGE_WEIGHTING(played_card, 150); // *Value*
  }

  // third card
  if (trick.actcardno() == 3) {
    if (!maybe_equal(self.team_information().team(trick.player_of_card(2)),
                     self.team_information().team(trick.player_of_card(3)))) {
      if (player.cards_to_play() < 3)
        CHANGE_WEIGHTING(played_card, 20 * player.cards_to_play()); // *Value*
      else
        CHANGE_WEIGHTING(played_card, 50); // *Value*
    }
    else
      // if the player plays the dulle at the second last position, assume, he does not have the other dulle, else he would have played it before
      CHANGE_WEIGHTING(played_card, -20); // *Value*
  } // if (trick.actcardno() == 3)

  if (trick.actcardno() == 4)
    // if the player plays the dulle at the last position, assume, he does not have the other dulle, else he would have played it before
    CHANGE_WEIGHTING(played_card, -50); // *Value*
}


void poverty_re_starts_with_trump(HandCard const played_card,
                                  Trick const& trick,
                                  Player const& self,
                                  map<Card, int>& weightings)
{
  // Idea:
  // The re player in a poverty starts with a trump and the poverty
  // player is behind.
  // If he plays a high card (queen or above) the poverty player will not
  // have a high card.

  // the name of the heuristic
  auto constexpr heuristic_name = "poverty re starts with trump";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (game.type() != GameType::poverty)
    return ;

  // only the re player
  if (   (player.team() != Team::re)
      || (player == game.players().soloplayer()) )
    return ;

  // the poverty player is behind
  if (trick.has_played(game.players().soloplayer()))
    return ;

  if (played_card.value() == Card::queen || played_card.is_special()) {
    // the poverty player has no queen or higher card
    auto c = game.cards().trumps().begin();
    for ( ;
         c != game.cards().trumps().end();
         ++c)
      if (*c == Card::diamond_queen)
        break;
    for ( ;
         c != game.cards().trumps().end();
         ++c) {
#ifdef POSTPONED
      // ToDo: change the information of the poverty player
      CHANGE_WEIGHTING(*c, -70); // *Value*
#else
      CHANGE_WEIGHTING(*c, 0); // *Value*
#endif
    }
  } // if (Card::charlie.less(card))
}


void soloplayer_startcard_color(HandCard const played_card,
                                Trick const& trick,
                                Player const& self,
                                map<Card, int>& weightings)
{
  // Idea:
  // If the soloplayer in a solo starts with a lower color card,
  // he probably will have the higher ones.

  // the name of the heuristic
  auto constexpr heuristic_name = "soloplayer startcard color";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  // sologame
  if (!is_real_solo(game.type()))
    return ;

  if (player != game.players().soloplayer())
    return ;

  if (trick.startplayer() != player)
    return ;

  if (played_card.istrump())
    return ;

  if (played_card.value() == Card::ten) {
    CHANGE_WEIGHTING(Card(played_card.color(), Card::ace), 110); // *Value*
  } else {
    for (auto const v : game.rule().card_values()) {
      if (v > played_card.value()) {
        CHANGE_WEIGHTING(Card(played_card.color(), v), -20); // *Value*
      }
    }
  }
}


void picture_solo_re_played_second_best_trump(HandCard const played_card,
                                              Trick const& trick,
                                              Player const& self,
                                              map<Card, int>& weightings)
{
  // Idea:
  // If the re player starts with the second best trump, he will probably have the best one, also.

  // the name of the heuristic
  auto constexpr heuristic_name = "picture solo re played second best trump";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (!is_picture_solo(game.type()))
    return ;
  if (player != game.players().soloplayer())
    return ;
  if (self.cards_information().trump_runs())
    return ;

  Card::Value value = Card::nocardvalue;
  switch (game.type()) {
  case GameType::solo_jack:
    value = Card::jack;
    break;
  case GameType::solo_queen:
  case GameType::solo_queen_jack:
    value = Card::queen;
    break;
  case GameType::solo_king:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
    value = Card::king;
    break;
  case GameType::solo_meatless:
    value = Card::ten;
    break;
  default:
    break;
  } // switch (game.type())
  if (played_card.value() != value)
    return ;
  if (played_card.color() != Card::spade)
    return ;
  if (game.type() == GameType::solo_meatless)
    CHANGE_WEIGHTING(Card(played_card.color(), Card::ace),
                     200); // *Value*
  else
    CHANGE_WEIGHTING(Card(Card::club, value),
                     200); // *Value*
}


void picture_solo_re_not_jabbed_trick_with_points(HandCard const played_card,
                                                  Trick const& trick,
                                                  Player const& self,
                                                  map<Card, int>& weightings)
{
  // Idea:
  // If the re player starts with the second best trump, he will probably have the best one, also.

  // the name of the heuristic
  auto constexpr heuristic_name = "picture solo re not jabbed trick with points";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (!is_picture_solo(game.type()))
    return ;
  if (player != game.players().soloplayer())
    return ;

  if (trick.points() < 10)
    return ;
  if (played_card.tcolor() == trick.startcard().tcolor())
    return ;
  if (played_card.istrump())
    return ;

  for (auto const& card : game.cards().trumps()) {
    CHANGE_WEIGHTING(card, -200); // *Value*
  }
}


void picture_solo_contra_played_trump(HandCard const played_card,
                                      Trick const& trick,
                                      Player const& self,
                                      map<Card, int>& weightings)
{
  // Idea:
  // The contra players will most probably only have this one trump.

  // the name of the heuristic
  auto constexpr heuristic_name = "picture solo contra played trump";

  auto const& player = played_card.player();
  auto const& game = trick.game();

  if (!is_picture_solo(game.type()))
    return ;

  if (player == game.players().soloplayer())
    return ;

  if (!played_card.istrump())
    return ;

  auto const& trumps = game.cards().trumps();
  int const trumpno = trumps.size();
  for (auto const& card : trumps) {
    CHANGE_WEIGHTING(card, -(40 * 4 / trumpno)); // *Value*
  }
  if (trick.winnerplayer() != self) {
    for (auto const& card : trumps) {
      if (played_card.jabs(card))
        CHANGE_WEIGHTING(card, -40); // *Value*
    }
  }
}

} // namespace CardsInformationHeuristics
