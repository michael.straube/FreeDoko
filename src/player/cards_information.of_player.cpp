/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "cards_information.of_player.h"
#include "cards_information.heuristics.h"
#include "team_information.h"
#include "player.h"
#include "ai/ai.h"

#include "../party/rule.h"
#ifdef DEBUG_ASSERT
#include "../card/trick.h"
#endif
#include "../card/weighted_card_list.h"

#ifdef CHECK_RUNTIME
#include "../runtime.h"
#endif

// whether to print information of the throwing
#ifndef RELEASE
#ifdef DKNOF
// whether to print information of the information flow
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CONDITION_INFORMATION_FLOW \
  false \
  && (cards_information().player().no() == 1) \
  && (playerno() == 3)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CONDITION_INFORMATION_ESTIMATED_HAND \
  false \
  && (cards_information().player().no() == 1) \
  && (playerno() == 3)

#endif
#endif


// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define THROW_INVALID_GAME_IF(predicate, text) \
  do {                                   \
    if (predicate) {                  \
      DEBUG("throw") << text << '\n'     \
                     << cards_information(); \
      ostringstream ostr;                \
      ostr << "ASSERT(" << __FILE__ << '#' << __LINE__ << "): " \
           << text << '\n'               \
           << "of player " << playerno_ << '\n'               \
           << cards_information();       \
      if (!cards_information().is_virtual()) { \
        report_error(ostr.str());        \
      }                                  \
      throw InvalidGameException(ostr.str()); \
    } \
  } while(false)


/*
 * CardsInformation::OfPlayer contains the information an ai has about the
 * cards a specific player can / must / cannot have.
 * The class contains four counters and a weighting
 * - played
 *   what cards the player has played
 * - must have
 *   which and how many cards the player must have
 *   (p.e. if he has announced 're' and not played a club queen, he must
 *    have one (but can have two) )
 * - can have
 *   which and how many cards the player can have at max
 *   (p.e. if another player has annouced 're' he can only have one club queen)
 * - does not have tcolor
 *   just remembers the colors the player cannot have anymore
 *   (because he could not serve).
 *   This should not really be necessary anymore, but I feel more sure with this
 *   code (should check someday)
 * - cards weighting
 *   According to the gameplay the cards are weighted.
 *   positive weight means higher probability for the card,
 *   negative weight means lower probability for the card.
 *
 * The easy part of the code is to add the information gotten by the game
 * (see CardsInformation).
 * Complicated is to update the information over the players.
 * - if the cards a player has to play == the number he must have
 *   the player can only have the cards he must have
 *   (must have --> can have)
 *   A bit more information is gained, like:
 *     If the player has only one free card (the rest to play are 'must') he
 *     can only have at most one card from each card he must not have).
 * - if the cards a player has to play == the number he can have
 *   the player must have the cards he can have
 *   (can have --> must have)
 *   This is analogous to the previous case, so here also a bit more information
 *   can be gained (and yes, there was at least one case I needed this), p.e.
 *     The player has to play two cards
 *     and he can have one diamond jack and two heart jacks,
 *     he must have at least one heart jack.
 * - count data for a single card
 *   Sum up how many times the card was played and the other players must have
 *   it. Then the player can at max have the remaining number of cards.
 *   If p.e. a player has announced 're' he must have a club queen. So the other
 *     players can only have at max one club queen in their hands.
 *   And again this can be viewed from the opposite side:
 *   If for a card the sum of 'played' and 'can have' of the other players
 *   are less than two, then the player must have the remaining ones.
 *   A simple (and most common) example is that three players do not have
 *   spade, so the fourth player must have all remaining spade cards.
 */

/** constructor
 **
 ** @param    cards_information   corresponding cards information
 ** @param    playerno   number of the player of this information
 **/
CardsInformation::OfPlayer::OfPlayer(CardsInformation& cards_information,
                                     unsigned const playerno) :
  cards_information_(&cards_information),
  playerno_(playerno)
{ }

/** copy constructor
 **
 ** @param    of_player   object to be copied
 **/
CardsInformation::OfPlayer::OfPlayer(OfPlayer const& of_player) = default; // NOLINT (false positive cppcoreguidelines-pro-type-member-init)

/** destructor
 **/
CardsInformation::OfPlayer::~OfPlayer() = default;

/** resets all information
 **/
void
CardsInformation::OfPlayer::reset()
{
  // cards
  played_.clear();
  must_have_.clear();
  cards_weighting_.clear();
  for (auto const& c : game().rule().cards()) {
    can_have_.set(c, game().rule(Rule::Type::number_of_same_cards));
    cards_weighting_[c] = 0;
  }

  // tcolors
  tcolor_played_.clear();
  tcolor_must_have_.clear();
  for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
    tcolor_can_have_.set(static_cast<Card::TColor>(c),
                               min(game().rule(Rule::Type::number_of_cards_on_hand),
                                   game().cards().count(static_cast<Card::TColor>(c))));
}

/** writes 'of_player' in 'ostr'
 **
 ** @param    ostr   output stream
 ** @param    of_player   object to write into 'ostr'
 **
 ** @return   the output stream
 **/
ostream&
operator<<(ostream& ostr, CardsInformation::OfPlayer const& of_player)
{
  of_player.write(ostr);
  return ostr;
}

/** writes 'of_player' in 'ostr'
 **
 ** @param    ostr   output stream
 **/
void
CardsInformation::OfPlayer::write(ostream& ostr) const
{
  ostr << "player = " << playerno() << " (" << player().name() << ")\n";

  ostr << "{\n";

  ostr << "  played\n"
    << "  {\n"
    << played_
    << "  }\n";

  ostr << "  must_have\n"
    << "  {\n"
    << must_have_
    << "  }\n";

  ostr << "  can_have\n"
    << "  {\n"
    << "cardno = " << can_have_.cards_no() << '\n';
  for (auto const& c : game().rule().cards()) {
    if (can_have(c) > 0) {
      ostr << setw(14)
        << c << " = "
        << can_have(c) << ' '
        << "(" << weighting(c, false) << ")\n";
    }
  }
  ostr << "  }\n";

  ostr << "  tcolors played\n"
    << "  {\n"
    << tcolor_played_
    << "  }\n";

  ostr << "  tcolors must_have\n"
    << "  {\n"
    << tcolor_must_have_
    << "  }\n";

  ostr << "  tcolors can_have\n"
    << "  {\n"
    << tcolor_can_have_
    << "  }\n";

  ostr << '\n';
  ostr << "estimated hand\n"
    << estimated_hand() << '\n';
  ostr << "}\n";
}

/** @return   the corresponding cards information
 **/
CardsInformation const&
CardsInformation::OfPlayer::cards_information() const
{ return *cards_information_; }

/** @return   the corresponding cards information
 **/
CardsInformation&
CardsInformation::OfPlayer::cards_information()
{ return *cards_information_; }

/** @return   the number of the player
 **/
unsigned
CardsInformation::OfPlayer::playerno() const
{ return playerno_; }

/** @return   the corresponding game
 **/
Game const&
CardsInformation::OfPlayer::game() const
{
  return cards_information().game();
}

/** @return   the corresponding player
 **/
Player const&
CardsInformation::OfPlayer::player() const
{
  return game().player(playerno());
}

/** sets the corresponding cards information
 **
 ** @param    cards_information   new cards information
 **/
void
CardsInformation::OfPlayer::set_cards_information(CardsInformation&
                                                  cards_information)
{ cards_information_ = &cards_information; }

/** @return   whether all cards are known
 **/
bool
CardsInformation::OfPlayer::all_known() const
{
  return (can_have_.cards_no() == must_have_.cards_no());
}

/** -> result
 **
 ** @param    card   card
 **
 ** @return   how many of 'card' the player has played
 **/
unsigned
CardsInformation::OfPlayer::played(Card const card) const
{
  return played_[card];
}

/** -> result
 **
 ** @param    tcolor   tcolor
 **
 ** @return   how many cards of the tcolor 'tcolor' the player has played
 **/
unsigned
CardsInformation::OfPlayer::played(Card::TColor const tcolor) const
{
  DEBUG_ASSERTION(tcolor != Card::unknowncardcolor,
                  "CardsInformation::OfPlayer::played(" << tcolor << ")\n"
                  "  tcolor 'unknown' is forbidden");

  return tcolor_played_[tcolor];
}

/** -> result
 **
 ** @param    card   card
 **
 ** @return   how many cards of 'card' the player must still have on the hand
 **/
unsigned
CardsInformation::OfPlayer::must_have(Card const card) const
{
  return must_have_[card];
}

/** -> result
 **
 ** @param    tcolor    tcolor
 **
 ** @return   how many cards of 'tcolor' the player must still have on the hand
 **/
unsigned
CardsInformation::OfPlayer::must_have(Card::TColor const tcolor) const
{
  DEBUG_ASSERTION(tcolor != Card::unknowncardcolor,
                  "CardsInformation::OfPlayer::must_have(" << tcolor << ")\n"
                  "  tcolor 'unknown' is forbidden");
  return tcolor_must_have_[tcolor];
}

/** -> result
 **
 ** @param    card   card
 **
 ** @return   how many cards of 'card' the player can still have on the hand
 **/
unsigned
CardsInformation::OfPlayer::can_have(Card const card) const
{
  return can_have_[card];
}

/** -> result
 **
 ** @param    tcolor    tcolor
 **
 ** @return   how many cards of 'tcolor' the player can still have on the hand
 **/
unsigned
CardsInformation::OfPlayer::can_have(Card::TColor const tcolor) const
{
  DEBUG_ASSERTION(tcolor != Card::unknowncardcolor,
                  "CardsInformation::OfPlayer::can_have(" << tcolor << ")\n"
                  "  tcolor 'unknown' is forbidden");
  return tcolor_can_have_[tcolor];
}

/** -> result
 **
 ** @param    card   card
 **
 ** @return   whether the player cannot have the card still on the hand
 **/
bool
CardsInformation::OfPlayer::cannot_have(Card const card) const
{
  return (can_have(card) == 0);
}

/** -> result
 **
 ** @param    tcolor   tcolor to check
 **
 ** @return   whether the player does not have anymore the tcolor
 **/
bool
CardsInformation::OfPlayer::does_not_have(Card::TColor const tcolor) const
{
  return (can_have(tcolor) == 0);
}

/** -> result
 **
 ** @param    card   card
 **
 ** @return   the number of unkown cards 'card'
 **/
unsigned
CardsInformation::OfPlayer::unknown(Card const card) const
{
  return (can_have(card) - must_have(card));
}

/** -> result
 **
 ** @param    tcolor    tcolor
 **
 ** @return   the number of unknown cards of 'tcolor'
 **/
unsigned
CardsInformation::OfPlayer::unknown(Card::TColor const tcolor) const
{
  return (can_have(tcolor) - must_have(tcolor));
}

/** -> result
 **
 ** @param    card     card
 ** @param    modify   whether to modify the weighting (t.i. of the club queen) (default: true)
 **
 ** @return   weighting for the card
 **             positive means more probability for having the card
 **/
int
CardsInformation::OfPlayer::weighting(Card const card, bool const modify) const
{
  if (cannot_have(card))
    return 0;

  int modifier = 0;

  // take the announcement into account:
  // if the player has announced, he will likely have less color cards
  if (   !card.istrump(game())
      && !(   card.value() == Card::ace
           && cards_information().color_runs(card.color()) == 0)) {
    switch (game().announcements().last(player()).announcement) {
    case Announcement::no120:
      modifier -= 15;
      break;
    case Announcement::no90:
      modifier -= 30;
      break;
    case Announcement::no60:
      modifier -= 100;
      break;
    case Announcement::no30:
      modifier -= 500;
      break;
    case Announcement::no0:
      modifier -= 1000;
      break;
    default:
      break;
    }
  }

  if (modify) {
    if (   (card == Card::club_queen)
        && (game().type() == GameType::normal) ) {
      modifier += (10 // *Value*
                   * cards_information().player().team_information().team_value(playerno()));
      if (cards_information().player().teaminfo(playerno()) >= GuessedTeam::maybe_re) {
        if (played(Card::club_queen))
          modifier -= 500;
        else
          modifier += 1000;
      } else if (cards_information().player().teaminfo(playerno()) == GuessedTeam::maybe_contra)
        modifier -= 1000;
    }
  }

  return (cards_weighting_[card] + modifier);
}

/** @return   the number of played cards
 **/
unsigned
CardsInformation::OfPlayer::played_cards_no() const
{
  return (  game().tricks().tricks_in_trickpiles()
          + (game().tricks().current().has_played(player()) ? 1 : 0));
}

/** @return   the number of cards still to be played
 **/
unsigned
CardsInformation::OfPlayer::remaining_cards_no() const
{
  return (game().rule(Rule::Type::number_of_tricks_in_game) - played_cards_no());
}

/** @return   the cards the player has played
 **/
Hand
CardsInformation::OfPlayer::played_cards() const
{
  Hand cards(player());

  for (auto const& c : played_.cards())
    cards.add(c);

  return cards;
}

/** @return   the cards the player must have
 **/
Hand
CardsInformation::OfPlayer::must_have_cards() const
{
  Hand cards(player());

  for (auto const& c : must_have_.cards())
    cards.add(c);

  return cards;
}

/** @return   the cards the player cannot have
 **/
Hand
CardsInformation::OfPlayer::cannot_have_cards() const
{
  Hand cards(player());

  for (auto const& c : game().rule().cards()) {
    for (unsigned i = 0;
         i < game().rule(Rule::Type::number_of_same_cards) - can_have(c);
         ++i) {
      cards.add(c);
    }
  }

  return cards;
}

/** @return   the possible hand of the player
 **
 ** @todo   check whether the cards the player must have are as many as the
 **   player can only have
 ** @todo   add the played cards
 **/
Hand
CardsInformation::OfPlayer::possible_hand() const
{
  DEBUG_ASSERTION((can_have_.cards_no()
                   >= player().cards_to_play()),
                  "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::possible_hand():\n"
                  "  can_have.cards_no() = " << can_have_.cards_no()
                  << " < " << player().cards_to_play()
                  << " = player().cards_to_play()");

  DEBUG_ASSERTION((can_have_.cards().size()
                   == can_have_.cards_no()),
                  "CardsInformation::OfPlayer::possible_hand():\n"
                  "  the number of cards is not valid");

  return Hand(player(),
              can_have_.cards(), played_.cards());
}

/** -> result
 ** the estimation is based by the weighting
 **
 ** @return   estimated hand for the player
 **
 ** @todo       check tcolor (can have, must have)
 ** @todo       better alogrithm for the estimation
 ** @todo       ensure, that all cards are distributed
 ** @todo       ensure, that each player has enough cards
 ** @todo       maybe contra -> no club queen
 ** @todo       two re -> both one club queen
 **/
Hand
CardsInformation::OfPlayer::estimated_hand() const
{
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
  if (CONDITION_INFORMATION_ESTIMATED_HAND) {
    clog << __FILE__ << '#' << __LINE__ << "  " << "start CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << player().no() << ")::estimate_hand()\n";
  }
#endif

  try {
    if (dynamic_cast<Ai const&>(cards_information().player()).value(Aiconfig::Type::hands_known))
      return player().hand();
  } catch (std::bad_cast const&) {
  }

  Hand hand(player());

  // start with the played cards
  hand.add_played(played_.cards());

#ifdef WORKAROUND
  // all cards known
  if (can_have_.cards_no() == player().cards_to_play()) {
    hand.add(can_have_.cards());
    return hand;
  }
#endif

  auto const& game = this->game();
  auto const& rule = game.rule();

  { // 1) add all 'must have' cards
    // ToDo: use must_have for the cards directly
    for (auto const& c : rule.cards()) {
      hand.add(c, must_have(c));
    }
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
    if (CONDITION_INFORMATION_ESTIMATED_HAND) {
      //clog << __FILE__ << '#' << __LINE__ << "  " << "hand after 1) must have\n" << hand << '\n';
    }
#endif
  } // 1) add all 'must have' cards

  // all cards of the player are known
  if (hand.cardsnumber() == player().cards_to_play())
    return hand;

  // the cards to distribute
  WeightedCardList cards(game);
  { // Put all remaining can_have-cards in the list 'cards'.
    for (auto const& c : rule.cards()) {
      if (cannot_have(c))
        continue;
      int w = INT_MIN; // the weighting from the other players
      // take the weighting of the other players into account
      for (unsigned p = 0; p < game.playerno(); ++p) {
        if (   (p != playerno())
            && cards_information().of_player(p).unknown(c)) {
          w = max(w, cards_information().of_player(p).weighting(c));
        }
      }

      // '2 * i' reflects the probability to have two of the same cards
      for (int i = 0;
#ifdef WORKAROUND
           // ToDo: remaining(*c) should always be >= can_have(*c)
           i < static_cast<int>(min(can_have(c) - must_have(c),
                                    cards_information().remaining_unknown(c)));
#else
           i < static_cast<int>(can_have(*c) - must_have(*c));
#endif
           ++i) {
        if (w == INT_MIN) {
          cards.add(c, INT_MAX);
          continue;
        }
        if (   c == Card::club_queen
            && game.type() == GameType::normal) {
          // in a normal game, a player will most surely not have two club queens
          cards.add(c,
                    weighting(c)
                    - w / static_cast<int>(cards_information().remaining_unknown(c))
                    - 10000 * (i + must_have(c))); // *Value*
        } else {
          cards.add(c,
                    weighting(c)
                    - w / static_cast<int>(cards_information().remaining_unknown(c))
                    - 10 * i); // *Value*
        }
      }
    }
  } // Put all remaining can_have-cards in the list 'cards'.
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
  if (CONDITION_INFORMATION_ESTIMATED_HAND) {
    clog << __FILE__ << '#' << __LINE__ << "  " << "weighted cards\n" << cards << '\n';
  }
#endif

  { // 2) add to each player the cards with the greates weighting
    // (a card can be distributed multible times)
    // Now fill up the hand in the order of weighting.
    while (   !cards.empty()
           // '- 1' here, so that in the next block 'w' is the right weighting
           // (the next card is always added)
           && hand.cardsnumber() + 1 < player().cards_to_play()) {
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
      if (CONDITION_INFORMATION_ESTIMATED_HAND) {
        clog << __FILE__ << '#' << __LINE__ << "  " << "2a) add card " << cards.highest() << '\n';
      }
#endif
      hand.add(cards.highest());
      cards.pop_highest();
    }

    // Also add the cards with a weighting nearly the same as the lowest so far.
    int const w = cards.highest_weighting();
    while (   !cards.empty()
           && cards.highest_weighting() > w - 40) { // *Value*
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
      if (CONDITION_INFORMATION_ESTIMATED_HAND) {
        clog << __FILE__ << '#' << __LINE__ << "  " << "2b) add card " << cards.highest() << '\n';
      }
#endif
      hand.add(cards.highest());
      cards.pop_highest();
    }
  } // 2) add each player the cards with the greates weighting

  { // 3) for each color add enough cards according to 'tcolor_must_have' and 'tcolor_can_have'
    // single tcolors
    for (unsigned tc = 0; tc < Card::number_of_tcolors; ++tc) {
      auto tcolor = static_cast<Card::TColor>(tc);
      unsigned n = hand.count(tcolor);
      unsigned const min = must_have(tcolor);
      if (n >= min)
        continue;
      // add at least 'min' cards of 'tcolor'
      while (!cards.highest(tcolor).is_empty()
             && (n + 1 < min)) {
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
        if (CONDITION_INFORMATION_ESTIMATED_HAND) {
          clog << __FILE__ << '#' << __LINE__ << "  " << "3) add card " << cards.highest(tcolor) << '\n';
        }
#endif
        hand.add(cards.highest(tcolor));
        cards.pop_highest(tcolor);
        n += 1;
      }

      // Also add the cards with a weighting nearly the same as the lowest so far.
      int const w = cards.highest_weighting(tcolor);
      while (   !cards.highest(tcolor).is_empty()
             && (cards.highest_weighting(tcolor) > w - 40)) { // *Value*
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
        if (CONDITION_INFORMATION_ESTIMATED_HAND) {
          clog << __FILE__ << '#' << __LINE__ << "  " << "3) add card " << cards.highest(tcolor) << '\n';
        }
#endif
        hand.add(cards.highest(tcolor));
        cards.pop_highest(tcolor);
      }
    }

    // single tcolors inverted
    for (unsigned tc = 0; tc < Card::number_of_tcolors; ++tc) {
      auto tcolor = static_cast<Card::TColor>(tc);
      if (player().cards_to_play() <= can_have(tcolor))
        continue;
      unsigned n = hand.cardsnumber() - hand.count(tcolor);
      unsigned const min = (player().cards_to_play()
                            - can_have(tcolor));
      if (n >= min)
        continue;
      THROW_INVALID_GAME_IF(cards.cards_no() < min - n,
                            "estimated_hand: cards.cards_no() = " << cards.cards_no() << " >= " << min << " - " << n << " = min - n");

      // add at least 'min' cards of not 'tcolor'
      while (!cards.highest_not_of(tcolor).is_empty()
             && (n + 1 < min)) {
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
        if (CONDITION_INFORMATION_ESTIMATED_HAND) {
          clog << __FILE__ << '#' << __LINE__ << "  " << "3) add card " << cards.highest_not_of(tcolor) << '\n';
        }
#endif
        hand.add(cards.highest_not_of(tcolor));
        cards.pop_highest_not_of(tcolor);
        n += 1;
      }

      // Also add the cards with a weighting nearly the same as the lowest so far.
      int const w = cards.highest_weighting(tcolor);
      while (   !cards.highest_not_of(tcolor).is_empty()
             && (cards.highest_weighting_not_of(tcolor) > w - 40)) { // *Value*
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
        if (CONDITION_INFORMATION_ESTIMATED_HAND) {
          clog << __FILE__ << '#' << __LINE__ << "  " << "3) add card " << cards.highest_not_of(tcolor) << '\n';
        }
#endif
        hand.add(cards.highest_not_of(tcolor));
        cards.pop_highest_not_of(tcolor);
      }
    }
  } // 3) for each color add enough cards according to 'tcolor_must_have' and 'tcolor_can_have'

  { // 4) add the cards this player has the greates weighting of
    // this must be the last part to make use of 'cards', since 'cards' is empty after this part

    while (!cards.empty()) {
      Card const card = cards.highest();
      // player with the maximal weighting
      int weighting_max = INT_MIN;
      for (unsigned p = 0; p < game.playerno(); ++p) {
        if (   !cards_information().of_player(p).all_known()
            && cards_information().of_player(p).can_have(card)
            && (cards_information().of_player(p).weighting(card)
                > weighting_max) )
          weighting_max
            = cards_information().of_player(p).weighting(card);
      }

#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
      if (CONDITION_INFORMATION_ESTIMATED_HAND) {
        clog << __FILE__ << '#' << __LINE__ << "  " << "4) " << setw(5) << cards.highest_weighting() << " -- "
          << setw(5) << weighting_max << " - 40: " << card << '\n';
      }
#endif
      if (cards.highest_weighting() >= weighting_max - 40) { // *Value* (same below, 2 times)
#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
        if (CONDITION_INFORMATION_ESTIMATED_HAND) {
          clog << __FILE__ << '#' << __LINE__ << "  " << "4) add card " << card << '\n';
        }
#endif
        hand.add(card);
      }

      cards.pop_highest();
    } // while (!cards.empty())
  } // 4) add the cards this player has the greates weighting of

#ifdef CONDITION_INFORMATION_ESTIMATED_HAND
  if (CONDITION_INFORMATION_ESTIMATED_HAND) {
    clog << __FILE__ << '#' << __LINE__ << "  " << "end CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << player().no() << ")::estimate_hand()\n";
  }
#endif

  return hand;
}

/** @return   whether the hand is valid
 **/
bool
CardsInformation::OfPlayer::is_valid(Hand const& hand) const
{
  map<Card, unsigned> const counter = hand.counted_cards();

  for (auto const& c : counter) {
    Card const card = c.first;
    unsigned const n = c.second;
    if (!(   must_have(card) <= n
          && can_have(card)  >= n) )
      return false;
  }

  for (unsigned c = 0; c < Card::number_of_tcolors; ++c) {
    auto const tcolor = static_cast<Card::TColor>(c);
    unsigned const n = hand.count(tcolor);
    if (!(   must_have(tcolor) <= n
          && can_have(tcolor) >= n) )
      return false;
  }

  return true;
}

/** @return   the weighting of the hand
 **/
int
CardsInformation::OfPlayer::weighting(Hand const& hand) const
{
  int min_weighting = INT_MAX;
  int sum_weighting = 0;
  for (auto const& card : hand) {
    if (must_have(card))
      continue;
    min_weighting = std::min(min_weighting, weighting(card));
    sum_weighting += weighting(card);
  }
  return min_weighting + sum_weighting / 10;
}

/** the game starts
 **/
void
CardsInformation::OfPlayer::game_start()
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::game_start()\n";
#endif

  switch (game().type()) {
  case GameType::marriage:
    if (player() == game().players().soloplayer()) {
      add_must_have(Card::club_queen, 2);
      // tcolor is set in CardsInformation::do_update()
    }
    break;
  case GameType::poverty:
    /// the player who takes the poverty
#ifndef WITH_DOKOLOUNGE
    // In der Doko-Lounge wird nur bekannt gegeben, ob die Armut überhaupt Trumpf erhalten hat, aber nicht wie viele Trumpf.
    if (player() == game().players().poverty_partner())
      CardsInformationHeuristics::poverty_returned
        (player(),
         cards_information().player(),
         cards_weighting_,
         game().poverty().returned_trumpno());
    if (player() == game().players().soloplayer())
      if (game().poverty().returned_trumpno() == 0)
        add_must_exactly_have(Card::trump, game().poverty().returned_trumpno());
#endif
    break;
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
    CardsInformationHeuristics::game_start_picture_solo(player(),
                                                        cards_information().player(),
                                                        cards_weighting_);
    break;
  default:
    break;
  }
}

/** the player has played 'card'
 **
 ** @param    card   card that has been played
 ** @param    trick   current trick
 **/
void
CardsInformation::OfPlayer::card_played(HandCard const card,
                                        Trick const& trick)
{
  if (card.is_unknown()) {
    played_.inc(card);
    tcolor_played_.inc(card.tcolor());
    vector<Card> cards_to_dec;
    for (auto const card : must_have_)
      if (card.second > 0)
        cards_to_dec.push_back(card.first);
    for (auto const card : cards_to_dec)
      must_have_.dec(card);
    for (auto& counter : tcolor_must_have_)
      if (counter > 0)
        counter -= 1;
    return ;
  }

#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "card_played(card = " << card << ", trick)\n";
#endif
  THROW_INVALID_GAME_IF(!can_have(card),
                        "card_played(" << card << ", trick" << ")\n"
                        << "Cards information says the player cannot have " << card << "\n"
                        << trick);

  played_.inc(card);
  if (must_have(card))
    must_have_.dec(card);
  can_have_.dec(card);
  Card::TColor const tcolor = card.tcolor();
  tcolor_played_.inc(tcolor);
  if (must_have(tcolor))
    tcolor_must_have_.dec(tcolor);
  tcolor_can_have_.dec(tcolor);

  if (!cards_information().in_recalcing)
    update_remaining_cards();
}

/** change the weightings according to the played card
 ** call some heuristics which
 **
 ** @param    card    played_card
 ** @param    trick   current trick
 **
 ** @todo      better name: all cards can be weighted
 **/
void
CardsInformation::OfPlayer::weight_played_card(HandCard const card,
                                               Trick const& trick)
{
  // no information from unknown cards
  if (card.is_unknown())
    return ;
  // I do not get more information from my own cardplay
  if (card.player() == cards_information().player())
    return ;

#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "weight_played_card(card = " << card << ", trick)\n";
#endif

  CardsInformationHeuristics::card_played(card, trick,
                                          cards_information().player(),
                                          cards_weighting_);
}

/** adds the information that the player must have the card 'no' times
 **
 ** @param    card   card that the player must have
 ** @param    no   how many times the player must have the card
 **   default: 1
 **/
void
CardsInformation::OfPlayer::add_must_have(Card const card, unsigned const no)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW) {
    if (must_have(card) < no) {
      cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
        << "add_must_have(card = " << card << ", no = " << no << ")\n";
    }
  }
#endif
  THROW_INVALID_GAME_IF(no > can_have(card),
                        "add_must_have(" << card << ", " << no << ")\n"
                        << "no = " << no << " > " << can_have(card) << " = can_have(" << card << ")");

  if (must_have_.min_set(card, no))
    cards_information().queue_update(card);
}

/** adds the information that the player must have the cards
 **
 ** @param    cards   cards the player must have
 **/
void
CardsInformation::OfPlayer::add_must_have(vector<Card> const& cards)
{
  map<Card, unsigned> number;
  for (auto const& card : cards)
    number[card] += 1;

  for (auto const& n : number) {
    add_must_have(n.first, n.second);
  }
}

/** adds the information that the player must have the tcolor 'no' times
 **
 ** @param    tcolor   tcolor that the player must have
 ** @param    no       how many times the player must have the tcolor
 **/
void
CardsInformation::OfPlayer::add_must_have(Card::TColor const tcolor,
                                          unsigned const no)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW) {
    if (must_have(tcolor) < no) {
      cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
        << "add_must_have(tcolor = " << tcolor << ", no = " << no << ")\n";
    }
  }
#endif

  THROW_INVALID_GAME_IF(no > can_have(tcolor),
                        "add_must_have(" << tcolor << ", " << no << "):\n"
                        "no = " << no << " < " << can_have(tcolor) << " = can have");


  THROW_INVALID_GAME_IF(tcolor_must_have_.cards_no()
                        - must_have(tcolor)
                        + no
                        > player().cards_to_play(),
                        "add_must_have(" << tcolor << ", " << no << "):\n"
                        "  no too great:\n"
                        "tcolor_must_have_.cards_no() - must_have(tcolor) + no = "
                        << tcolor_must_have_.cards_no()
                        << " - " << must_have(tcolor)
                        << " + " << no
                        << " > " << player().cards_to_play()
                        << " = player().cards_to_play()");

  if (tcolor_must_have_.min_set(tcolor, no)) {
    cards_information().queue_update(tcolor);
  }
}

/** adds the information that the player must have the tcolor exactly 'no' times
 **
 ** @param    tcolor   tcolor that the player must have
 ** @param    no       how many times exactly the player must have the tcolor
 **/
void
CardsInformation::OfPlayer::add_must_exactly_have(Card::TColor const tcolor,
                                                  unsigned const no)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    if (   (must_have(tcolor) < no)
        || (can_have(tcolor) > no) )
      cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
        << "add_must_exactly_have(tcolor = " << tcolor << ", no = " << no << ")\n";
#endif

  add_can_have(tcolor, no);
  add_must_have(tcolor, no);
}

/** adds the information that the player can have the card at max 'no' times
 **
 ** @param    card   card that the player can have
 ** @param    no   how many times the player can have the card at max
 **/
void
CardsInformation::OfPlayer::add_can_have(Card const card, unsigned const no)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    if (can_have(card) > no)
      cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
        << "add_can_have(card = " << card << ", no = " << no << ")\n";
#endif
  THROW_INVALID_GAME_IF(no < must_have(card),
                        "add_can_have(" << card << ", " << no << "):\n"
                        "  no < " << must_have(card) << " = must have");

  if (can_have_.max_set(card, no))
    cards_information().queue_update(card);

  // now erase the 'can_have' for all cards of 'tcolor'
}

/** adds the information that the player can have the cards at max 'no' times
 **
 ** @param    cards   cards that the player can have
 ** @param    no   how many times the player can have the card at max
 **/
void
CardsInformation::OfPlayer::add_can_have(vector<Card> const& cards,
                                         unsigned const no)
{
  for (auto const& card : cards)
    add_can_have(card, no);
}

/** adds the information that the player can only have the cards
 **
 ** @param    cards   cards the player can only have
 **/
void
CardsInformation::OfPlayer::add_can_only_have(vector<Card> const& cards)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW) {
#if 0
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "add_can_only_have(cards)\n";
#else
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "add_can_only_have(cards = ";
    for (auto c = cards.begin(); c != cards.end(); ++c)
      cout << ((c == cards.begin()) ? "" : ", ") << *c;
    cout << ")" << endl;
#endif
  }
#endif
  map<Card, unsigned> number;
  for (auto const& card : cards) {
    number[card] += 1;
  }

  auto const unknown_no = std::count(cards.begin(), cards.end(), Card::unknown);
  if (unknown_no == 0) {
    // remove all entries in 'can_have' which have the value '0'
    // count the number of cards the player can have
    vector<Card> to_remove;
    for (auto x = can_have_.begin();
         x != can_have_.end();
         ++x) {
      auto n = number.find(x->first);
      if (n == number.end()) {
        THROW_INVALID_GAME_IF(must_have(x->first),
                              "add_can_only_have():\n"
                              "  card '" << x->first << "':\n"
                              "    new number = 0 < "
                              << must_have(x->first) << " = must_have");
        to_remove.push_back(x->first);
        continue;
      }

      THROW_INVALID_GAME_IF(n->second < must_have(x->first),
                            "add_can_only_have():\n"
                            "  card '" << x->first << "':\n"
                            "    new number = " << n->second << " < "
                            << must_have(x->first) << " = must_have");
    }

    // remove the cards with value '0' from the list
    if (!to_remove.empty()) {
      for (auto const& card : to_remove) {
        THROW_INVALID_GAME_IF(must_have(card),
                              "  card '" << card << "':\n"
                              "    new number = 0 < "
                              << must_have(card) << " = must_have");

        add_cannot_have(card);
      }
    }
  }

  // set the maximal number of the cards
  for (auto const& n : number) {
    if (can_have_.max_set(n.first, n.second + unknown_no))
      cards_information().queue_update(n.first);
  }

  THROW_INVALID_GAME_IF(   game().status() != Game::Status::redistribute
                        && can_have_.cards_no() < player().cards_to_play(),
                        "add_can_only_have(cards)\n"
                        "  can_have.cards_no() = " << can_have_.cards_no()
                        << " < "
                        << player().cards_to_play() << " = cards to play");
}

/** adds the information that the player cannot have the card
 **
 ** @param    card   card the player cannot have
 **/
void
CardsInformation::OfPlayer::add_cannot_have(Card const card)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    if (can_have(card) > 0)
      cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
        << "add_cannot_have(card = " << card << ")\n";
#endif
  THROW_INVALID_GAME_IF(must_have(card),
                        "add_cannot_have(" << card << ")\n"
                        "  must_have(card) = " << must_have(card) << " > 0");

  if (can_have_.erase(card)) {
    THROW_INVALID_GAME_IF(must_have(card) > 0,
                          "add_cannot_have(" << card << "):\n"
                          << "  must_have = " << must_have(card));
    THROW_INVALID_GAME_IF(can_have_.cards_no() < player().cards_to_play(),
                          "add_cannot_have(" << card << ")\n"
                          "  player = " << playerno() << "\n"
                          "  can_have.cards_no() = " << can_have_.cards_no()
                          << " < "
                          << player().cards_to_play() << " = cards to play");

#ifdef WORKAROUND
    // if only one player can have the card, he must have it (see 'update_card')
    { // check whether all cards of 'card' are either played,
      // one player has it or it can be forgotten
      unsigned distributed = cards_information().played(card);
      for (unsigned p = 0; p < this->game().playerno(); ++p)
        distributed += cards_information().of_player(p).can_have(card);
      distributed += cards_information().forgotten_cards_no();

      THROW_INVALID_GAME_IF(distributed < game().rule(Rule::Type::number_of_same_cards),
                            "add_cannot_have"
                            "(" << card << ")\n"
                            "  player = " << playerno() << "\n"
                            "  the card is not distributed");
    } // check whether all cards of 'card' are somewhere
#endif
    cards_information().queue_update(card);
  }
}

/** adds the information that the player can have the tcolor at max 'no' times
 **
 ** @param    tcolor   tcolor that the player can have
 ** @param    no       how many times the player can have the tcolor at max
 **/
void
CardsInformation::OfPlayer::add_can_have(Card::TColor const tcolor,
                                         unsigned const no)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    if (can_have(tcolor) > no)
      cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
        << "add_can_have(tcolor = " << tcolor << ", no = " << no << ")\n";
#endif
  DEBUG_ASSERTION(no <= game().cards().count(),
                  "CardsInformation"
                  << "(" << cards_information().player().no() << ")"
                  << "::OfPlayer(" << playerno() << ")"
                  << "::add_can_have(" << tcolor << ", " << no << "):\n"
                  "  no = " << no << " too great");
  THROW_INVALID_GAME_IF(no < must_have(tcolor),
                        "add_can_have(" << tcolor << ", " << no << "):\n"
                        "  no = " << no << " < " << must_have(tcolor) << " = must have");

  if (tcolor_can_have_.max_set(tcolor, no)) {
    cards_information().queue_update(tcolor);
  }
}

/** adds the information that the player cannot have the cards
 **
 ** @param    cards   cards the player cannot have
 **/
void
CardsInformation::OfPlayer::add_cannot_have(vector<Card> const& cards)
{
  for (auto const& card : cards)
    add_cannot_have(card);
}

/** adds the information that the player cannot have the tcolor
 **
 ** @param    tcolor   tcolor the player cannot have
 **/
void
CardsInformation::OfPlayer::add_cannot_have(Card::TColor const tcolor)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW) {
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "add_does_not_have(tcolor = " << tcolor << ")\n";
    cout << player().hand() << endl;
  }
#endif

  add_must_exactly_have(tcolor, 0);
}

/** updates the information of the card
 ** throws InvalidGameException
 **
 ** @param    card   card to update the information of
 **/
void
CardsInformation::OfPlayer::update_information(Card const card)
{
  if (card.is_unknown())
    return;

#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW) {
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "update_information(card = " << card << ")\n";
  }
#endif
  if (cards_information().in_recalcing)
    return ;

  // how many times this card was played
  unsigned const played = cards_information().played(card);
  // the number of forgotten cards
  unsigned const forgotten_cards
    = cards_information().forgotten_cards_no();
  // count how many of the card the other player must have
  unsigned must_have = 0;
  // count how many of the card the other player can have
  unsigned can_have = 0;
  for (unsigned p = 0; p < game().playerno(); ++p) {
    if (p != playerno()) {
      must_have += cards_information().of_player(p).must_have(card);
      can_have += cards_information().of_player(p).can_have(card);
    }
  }

#ifdef WORKAROUND
  // see bug report 232744
  // the cards distribution shall be sufficient
  // so that this case does not happen
  THROW_INVALID_GAME_IF(   played + must_have > game().rule(Rule::Type::number_of_same_cards)
                        && !card.is_unknown()
                        && cards_information().is_virtual(),
                        "throw in update_information(" << card << ")");
#endif

  THROW_INVALID_GAME_IF(played + must_have > game().rule(Rule::Type::number_of_same_cards),
                        "update_information(" << card << ")\n"
                        "  played + must_have > max number:\n"
                        "  " << played
                        << " + " << must_have << " > "
                        << game().rule(Rule::Type::number_of_same_cards));

  // the number of cards this player can at most have
  unsigned const n = min(game().rule(Rule::Type::number_of_same_cards)
                         - played
                         - must_have,
                         this->can_have(card.tcolor(game()))
                         - must_have_(card.tcolor(game()),
                                      game())
                         + this->must_have(card)
                        );
  THROW_INVALID_GAME_IF(n < this->must_have(card),
                        "update_information(" << card << ")\n"
                        "  player = " << playerno() << "\n"
                        "  card = " << card << "\n"
                        "  number of same cards - played - must_have total"
                        " < must_have(card):\n"
                        << game().rule(Rule::Type::number_of_same_cards)
                        << " - " << played
                        << " - " << must_have
                        << " < " << this->must_have(card));

  if (can_have_.max_set(card, n))
    cards_information().queue_update(card);

  // check whether a player must have the remaining cards of 'card'
  if (can_have + played + this->must_have(card) + forgotten_cards
      < game().rule(Rule::Type::number_of_same_cards)) {
    add_must_have(card,
                  game().rule(Rule::Type::number_of_same_cards)
                  - can_have - played - forgotten_cards);
  }

  THROW_INVALID_GAME_IF(can_have_.cards_no() < player().cards_to_play(),
                        "update_information(" << card << ")\n"
                        << "  can_have.cards_no() = "
                        << can_have_.cards_no()
                        << " < "
                        << player().cards_to_play() << " = cards to play");

  THROW_INVALID_GAME_IF(must_have_.cards_no() > player().cards_to_play(),
                        "update_information(" << card << ")\n"
                        << "  must_have.cards_no() = "
                        << must_have_.cards_no()
                        << " > "
                        << player().cards_to_play() << " = cards to play");
}

/** updates the information of the tcolor
 **
 ** @param    tcolor   tcolor to update the information of
 **/
void
CardsInformation::OfPlayer::update_information(Card::TColor const tcolor)
{
  if (tcolor == Card::unknowncardcolor)
    return ;

#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "update_information(" << tcolor << ")\n";
#endif

  // the number of forgotten cards
  unsigned const forgotten_cards = cards_information().forgotten_cards_no();

  { // maximum can have
    // the number of cards still to be played minus must have of other colors
    THROW_INVALID_GAME_IF(player().cards_to_play()
                          + must_have(tcolor)
                          < tcolor_must_have_.cards_no(),
                          "update_information(" << tcolor << ")\n"
                          << " cards to play + must have(" << tcolor << ") = "
                          << player().cards_to_play()
                          << " + " << must_have(tcolor)
                          << " < " << tcolor_must_have_.cards_no() << " = "
                          << tcolor_must_have_.cards_no());

    add_can_have(tcolor,
                 player().cards_to_play()
                 - tcolor_must_have_.cards_no()
                 + must_have(tcolor));
  } // maximum can have
  { // minimum must have
    // the number of cards still to be played minus can have of other colors
    if (player().cards_to_play()
        > tcolor_can_have_.cards_no()
        - can_have(tcolor)) {
      add_must_have(tcolor,
                    player().cards_to_play()
                    - (tcolor_can_have_.cards_no()
                       - can_have(tcolor)));
    }
  } // minimum must have
  { // maximum can have
    // number of cards still in game but not must have of the other players
    unsigned sum_must_have = 0;
    for (unsigned p = 0; p < game().playerno(); ++p)
      if (p != playerno())
        sum_must_have += cards_information().of_player(p).must_have(tcolor);
    THROW_INVALID_GAME_IF(game().cards().count(tcolor)
                          < cards_information().played(tcolor)
                          + sum_must_have,
                          "update_information(" << tcolor << ")\n"
                          << "  numberof = " << game().cards().count(tcolor)
                          << " > " << cards_information().played(tcolor)
                          << " + " << sum_must_have
                          << " = played + sum_must_have");
    add_can_have(tcolor,
                 game().cards().count(tcolor)
                 - cards_information().played(tcolor)
                 - sum_must_have);
  } // maximum can have
  { // minimum must have
    // number of cards of the color - played - can have of others
    unsigned sum_can_have = 0;
    for (unsigned p = 0; p < game().playerno(); ++p)
      if (p != playerno())
        sum_can_have += cards_information().of_player(p).can_have(tcolor);
    if (game().cards().count(tcolor)
        > (forgotten_cards
           + cards_information().played(tcolor)
           + sum_can_have) ) {

      add_must_have(tcolor,
                    game().cards().count(tcolor)
                    - forgotten_cards
                    - cards_information().played(tcolor)
                    - sum_can_have);
    }
  } // minimum must have
  // set the can have and must have according to the information of the cards
  add_can_have(tcolor, can_have_(tcolor, game()));
  add_must_have(tcolor, must_have_(tcolor, game()));
  check_can_is_must(tcolor);
}

/** updates the information of the tcolors
 **/
void
CardsInformation::OfPlayer::update_tcolor_information()
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "update_tcolor_information()\n";
#endif

  for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
    update_information(static_cast<Card::TColor>(c));

  if (!all_known()) {
    { // check can is must
      if (tcolor_can_have_.cards_no()
          == player().cards_to_play()) {
        for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
          add_must_have(static_cast<Card::TColor>(c),
                        can_have(static_cast<Card::TColor>(c)));
      }
    } // check can is must

    { // check must is can
      if (tcolor_must_have_.cards_no()
          == player().cards_to_play()) {
        for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
          add_can_have(static_cast<Card::TColor>(c),
                       must_have(static_cast<Card::TColor>(c)));
      }
    } // check must is can
  } // if (!all_known())

  // ToDo: if the player has only one free of a color, then he cannot have two of a card (p.e. three trumps, two swines so he cannot have two club kings)
}

/** updates 'can have' according to 'remaining cards'
 **/
void
CardsInformation::OfPlayer::update_remaining_cards()
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "update_remaining_cards()\n";
#endif
  unsigned const remaining_cards_no
    = (player().cards_to_play() - must_have_.cards_no());

  if ( !(remaining_cards_no
         < game().rule(Rule::Type::number_of_same_cards)) )
    return ;
  if ( !(must_have_.cards_no() != can_have_.cards_no()) )
    return ;

  vector<Card> cards;
  for (auto const& c : can_have_) {
    if (can_have(c.first)
        > remaining_cards_no - must_have(c.first))
      cards.push_back(c.first);
  } // for (c \in can_have_)
  for (auto const& card : cards) {
    add_can_have(card, remaining_cards_no + must_have(card));
  }
}

/** checks whether the cards which the player can have are the ones
 ** he has to have
 **/
void
CardsInformation::OfPlayer::check_can_is_must()
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "check_can_is_must()\n";
#endif
  if (can_have_.cards_no() == must_have_.cards_no())
    return ;
  if (must_have_.cards_no() == player().cards_to_play())
    return ;

  // update the tcolor information
  for (unsigned c = 0; c < Card::number_of_tcolors; ++c)
    check_can_is_must(static_cast<Card::TColor>(c));

  if (can_have_.cards_no() != player().cards_to_play()) {
    if (can_have_.cards_no() <
        (player().cards_to_play()
         + game().rule(Rule::Type::number_of_same_cards)) ) {
      unsigned const cards_surplus
        = can_have_.cards_no() - player().cards_to_play();
      for (auto const& c : can_have_) {
        if (c.second > cards_surplus)
          if (must_have_.min_set(c.first, c.second - cards_surplus))
            cards_information().queue_update(c.first);
      } // for (c \in can_have)
    } // if (surplus less than number of cards)
    { // only one card unknown

      // Check whether there is only one card in 'can have' unknown.
      // Then the remaining number must be the difference between
      // 'cards to play' and 'must have'.

      if (can_have_.cards_no()
          >= (must_have_.cards_no()
              + game().rule(Rule::Type::number_of_same_cards)))
        return ;

      Card unknown_card;
      for (auto const& c : can_have_) {
        if (must_have_[c.first] != c.second) {
          // found a second unknown card
          if (!unknown_card.is_empty())
            return ;

          unknown_card = c.first;
        } // if (must_have != can_have)
      } // for (c \in can_have_)

      can_have_.max_set(unknown_card,
                        player().cards_to_play()
                        - must_have_.cards_no()
                        + must_have_[unknown_card]);
    } // only one card unknown
  } // if (can_have != cards_to_play)

  // so all 'can_have' are 'must_have'

  for (auto const& c : can_have_) {
    if (must_have_[c.first] != c.second) {
      if (must_have_.min_set(c.first, c.second))
        cards_information().queue_update(c.first);
    }
  } // for (c \in can_have_)
}

/** checks whether the cards which the player can have are the ones
 ** he has to have
 **
 ** @param    tcolor   tcolor to check
 **
 ** @todo       information of only one remaining unknown card (see below)
 **/
void
CardsInformation::OfPlayer::check_can_is_must(Card::TColor const tcolor)
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "check_can_is_must(" << tcolor << ")\n";
#endif
  if (can_have(tcolor) > must_have(tcolor))
    return ;
  unsigned const can_have_tcolor = can_have_(tcolor, game());
  unsigned const must_have_tcolor = must_have_(tcolor, game());
  if (can_have_tcolor == must_have_tcolor)
    return ;
#ifdef POSTPONED
  // ToDo: check, whether only one card is unknown:
  // either set 'can have' or 'must have' according to the free
  // can_have(tcolor) - must_have_(tcolor, game()) or
  // can_have_(tcolor, game()) - must_have(tcolor)
  if (can_have_tcolor > must_have(tcolor)) {
    if (can_have_tcolor - must_have(tcolor) <
        game().rule(Rule::Type::number_of_same_cards)) {
      // check whether there is only one unknown card
      for (CardCounter::const_iterator c = can_have_.begin();
           c != can_have_.end();
           ++c) {
        if (c->first.tcolor(game()) == tcolor) {
          if (must_have_[c->first] != c->second) {
            must_have_[c->first].min_set(c->first,
                                         c->second
                                         - (can_have_tcolor
                                            - must_have(tcolor)));
          } // if (must_have != can_have)
        }
      } // for (c \in can_have_)

    }
  } // if (can_have_tcolor > must_have(tcolor))
#endif

  if (can_have_tcolor == must_have(tcolor)) {
    // all cards of tcolor are must have
    for (auto const& c : can_have_) {
      if (c.first.tcolor(game()) == tcolor) {
        if (must_have_.min_set(c.first, c.second))
          cards_information().queue_update(c.first);
      } // if (c->tcolor(game()) == tcolor)
    } // for (c \in can_have_)
  } // if (can_have_(tcolor, game()) == must_have(tcolor))

  if (must_have_tcolor == can_have(tcolor)) {
    // all cards of tcolor are must have
    // can_have_ is changed in the loop!
    CardCounter const can_have = can_have_;
    for (auto const& c : can_have) {
      if (c.first.tcolor(game()) == tcolor) {
        if (can_have_.max_set(c.first, must_have(c.first))) {
          cards_information().queue_update(c.first);
        }
      } // if (c->tcolor(game()) == tcolor)
    } // for (c \in can_have_)
  } // if (can_have_(tcolor, game()) == must_have(tcolor))
}

/** check whether the cards the player has to have are the only ones
 ** he can have
 **/
void
CardsInformation::OfPlayer::check_must_is_can()
{
#ifdef CONDITION_INFORMATION_FLOW
  if (CONDITION_INFORMATION_FLOW)
    cout << "CardsInformation(" << cards_information().player().no() << ")::OfPlayer(" << playerno() << ")::"
      << "check_must_is_can()\n";
#endif

  // the number of unknown cards of the player
  unsigned const unknown_cards_no
    = (player().cards_to_play() - must_have_.cards_no());

  if (unknown_cards_no >= game().rule(Rule::Type::number_of_same_cards))
    return ;

  // Example:
  // We know now that there is only one unknown card.
  // So the player can only have one 'can have' more than 'must have'.

  vector<Card> update_cards;
  for (auto const& c : can_have_) {
    unsigned const must_have = this->must_have(c.first);
    if (c.second > must_have + unknown_cards_no) {
      //if (can_have_.max_set(c->first, must_have))
      update_cards.push_back(c.first);
    }
  }

  // limit the can have
  for (auto const& card : update_cards) {
    if (can_have_.max_set(card, must_have(card) + unknown_cards_no))
      cards_information().queue_update(card);
  }

  // special case: poverty
  if (   (game().type() == GameType::poverty)
      && (playerno() == game().players().soloplayer().no()) ) {
    // check whether all trumps of the player are known
    if (   !does_not_have(Card::trump)
        && (can_have(Card::trump) > must_have(Card::trump))
        && (played(Card::trump) + must_have(Card::trump)
            == game().poverty().returned_trumpno())) {
      // search the trumps
      vector<Card> update_cards;
      for (auto const& c : can_have_) {
        if (   (c.first.istrump(game()))
            && (c.second > must_have(c.first)) )
          update_cards.push_back(c.first);
      } // for (c \in can_have_)
      // limit the can have
      for (auto const& card : update_cards) {
        if (can_have_.max_set(card, must_have(card)))
          cards_information().queue_update(card);
      }
    } // if (all trumps known)

  } // if (poverty player)
} // void CardsInformation::OfPlayer::check_must_is_can()

/** -> result
 **
 ** @param    lhs   first object
 ** @param    rhs   second object
 **
 ** @return   whether the two objects are equal
 **   (the correspondign cards information may differ)
 **/
bool
operator==(CardsInformation::OfPlayer const& lhs,
           CardsInformation::OfPlayer const& rhs)
{
  return ( (lhs.playerno_
            == rhs.playerno_)
          && (lhs.played_
              == rhs.played_)
          && (lhs.must_have_
              == rhs.must_have_)
          && (lhs.can_have_
              == rhs.can_have_)
          && (lhs.tcolor_played_
              == rhs.tcolor_played_)
          && (lhs.tcolor_must_have_
              == rhs.tcolor_must_have_)
          && (lhs.tcolor_can_have_
              == rhs.tcolor_can_have_)
         );
}

/** -> result
 **
 ** @param    lhs   first object
 ** @param    rhs   second object
 **
 ** @return   whether the two objects are different
 **/
bool
operator!=(CardsInformation::OfPlayer const& lhs,
           CardsInformation::OfPlayer const& rhs)
{
  return !(lhs == rhs);
}

/** self check
 ** when an error is found, an ASSERTION is created
 **
 ** @return   whether the self-check was successful (no error)
 **/
bool
CardsInformation::OfPlayer::self_check() const
{
  if (player().cards_to_play() == UINT_MAX)
    return true;

  { // cards
    // the player has to have at least as many cards as he has to play
    if ( !(can_have_.cards_no() >= player().cards_to_play())) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  can_have < cards to play:\n"
                      << "  " << can_have_.cards_no()
                      << " < " << player().cards_to_play());
      return false;
    }

    if (   !cards_information().is_virtual()
        && !game().isvirtual()) {
      { // the player has all cards of his hand in 'can_have'
        if (!can_have_.contains(player().hand().counted_cards())) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  the cards of the hand are not contained in the can have cards.\n");
          return false;
        }
      } // the player has all cards of his hand in 'can_have'
      { // the player has all 'must_have' cards in his hand
        if (!must_have_.is_contained(player().hand().counted_cards())) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  the must have cards are not contained in the cards of the hand.\n");
          return false;
        }
      } // the player has all 'must_have' cards in his hand
    } // if (!cards_information().is_virtual() && !game().isvirtual())


    // the player has to be able to play all 'must' cards
    if ( !(must_have_.cards_no() <= player().cards_to_play())) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  must_have > cards to play:\n"
                      << "  " << must_have_.cards_no()
                      << " > " << player().cards_to_play());
      return false;
    }

    // check 'can have == cards to play' ==> 'can have == must have'
    if ( !(   (can_have_.cards_no() != player().cards_to_play())
           || (can_have_.cards_no() == must_have_.cards_no()) ) ) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  cards_to_play == can_have != must_have:\n"
                      << "  " << player().cards_to_play()
                      << " == " << can_have_.cards_no()
                      << " != " << must_have_.cards_no()
                     );
      return false;
    }

    // for each card check:
    for (auto
         c = game().rule().cards().begin();
         c != game().rule().cards().end();
         ++c) {
      unsigned played = cards_information().played(*c);
      { // played(player) <= played(all)
        if (played < this->played(*c)) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  card '" << *c << "' has been played "
                          << played << " times, but by the player "
                          << playerno() << " '" << this->played(*c)
                          << "' times");
          return false;
        }
      } // played(player) <= played(all)

      unsigned const can_have = this->can_have(*c);
      { // check 'can have < must have'
        if ( !(can_have >= must_have(*c))) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  card '" << *c << "': "
                          "can_have = " << can_have << " < "
                          "must_have = " << must_have_[*c]
                         );
          return false;
        }
      } // check 'can have < must have'

      { // check 'can have < cards to play'
        if ( !(can_have <= player().cards_to_play())) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  card '" << *c << "': "
                          "can_have = " << can_have << " > "
                          "cards to play = " << player().cards_to_play()
                         );
          return false;
        }
      } // check 'can have < cards to play'

      { // check 'can have + played + sum must have <= #Cards'
        unsigned sum = 0;
        for (unsigned p = 0; p < game().playerno(); ++p)
          if (p != playerno())
            sum += cards_information().of_player(p).must_have(*c);

        if (can_have + played + sum
            > game().rule(Rule::Type::number_of_same_cards) ) {
#ifdef DEBUG_ASSERT
          for (unsigned p = 0; p < game().playerno(); ++p) {
            if (p != playerno())
              if (cards_information().of_player(p).must_have(*c) > 0)
                cerr << p << ": must have = " << cards_information().of_player(p).must_have(*c) << endl;
          }
#endif
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  card '" << *c << "':\n"
                          "  can_have + played + Sum must_have > number_of_cards:\n"
                          "  "
                          << can_have
                          << " + " << played << " + " << sum << " > "
                          << game().rule(Rule::Type::number_of_same_cards)
                          //<< "\ngame:\n{\n" << game() << "}\n"
                          //<< "\n" << cards_information()
                         );

          return false;
        }
      } // check 'can have + played + sum must have <= #Cards'
      { // check 'can have(tcolor) >= can have'
        if (can_have > this->can_have(c->tcolor(game()))) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  card '" << *c << "':\n"
                          "  can_have > can_have(tcolor):\n"
                          "  "
                          << can_have << " > "
                          << this->can_have(c->tcolor(game()))
                          //<< "\ngame:\n{\n" << game() << "}\n"
                          //<< "\n" << cards_information()
                         );

          return false;
        } // if (can_have < can_have(c->tcolor(game())))
      } // check 'can have(tcolor) >= can have'
    } // for (c \in cards)

    { // checked '#played intern <= #played by player'
      unsigned played_cardno = 0;
      for (auto const& p : played_)
        played_cardno += p.second;
      unsigned const played_cardno_by_player
        = (game().tricks().max_size() - player().cards_to_play());
      if (played_cardno > played_cardno_by_player) {
        DEBUG_ASSERTION(false,
                        "CardsInformation"
                        << "(" << cards_information().player().no() << ")"
                        << "::OfPlayer(" << playerno() << ")"
                        << "::self_check()\n"
                        "  " << played_cardno << " cards are marked as played, "
                        "but the player has played "
                        << played_cardno_by_player
                        << "\n" << game());
        return false;
      }
    } // checked '#played intern <= #played by player'

    if (   !cards_information().is_virtual()
        && (&cards_information()
            == &cards_information().player().cards_information())
       ) {
      // check that all cards on the hand of the player are 'can_be'
      for (auto const& card : player().hand().cards()) {
        if (card != Card::unknown) {
          DEBUG_ASSERTION((can_have(card)
                           >= player().hand().count(card)),
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  card '" << card << "' is "
                          << player().hand().count(card)
                          << " times on the hand, but 'can have' = "
                          << can_have(card));
        }
      } // for (card \in player().hand().cards())
    } // if (!is_virtual_game)

  } // cards

  { // tcolor

    // played in tcolor is as in cards
    if (!(tcolor_played_.cards_no() == played_.cards_no())) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  tcolor played != played (cards):\n"
                      << "  " << tcolor_played_.cards_no()
                      << " != " << played_.cards_no());
      return false;
    }

    // the player has to be able to play all 'must' cards
    if (!(tcolor_must_have_.cards_no()
          <= player().cards_to_play())) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  tcolor must_have > cards to play:\n"
                      << "  " << tcolor_must_have_.cards_no()
                      << " > " << player().cards_to_play());
      return false;
    }

    // the player has to have at least as many cards as he has to play
    if (!(tcolor_can_have_.cards_no()
          >= player().cards_to_play())) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  tcolor can_have < cards to play:\n"
                      << "  " << tcolor_can_have_.cards_no()
                      << " < " << player().cards_to_play());
      return false;
    }
    // the player has to be able to play all 'must' cards
    if (!(tcolor_must_have_.cards_no()
          <= player().cards_to_play())) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  tcolor must_have > cards to play:\n"
                      << "  " << tcolor_must_have_.cards_no()
                      << " > " << player().cards_to_play());
      return false;
    }

    // check 'can have == cards to play' ==> 'can have == must have'
    if ( !(   (tcolor_can_have_.cards_no()
               != player().cards_to_play())
           || (tcolor_can_have_.cards_no()
               == tcolor_must_have_.cards_no()) ) ) {
      DEBUG_ASSERTION(false,
                      "CardsInformation"
                      << "(" << cards_information().player().no() << ")"
                      << "::OfPlayer(" << playerno() << ")"
                      << "::self_check()\n"
                      "  playerno = " << playerno() << "\n"
                      "  cards_to_play == tcolor can_have != tcolor must_have:\n"
                      << "  " << player().cards_to_play()
                      << " == " << tcolor_can_have_.cards_no()
                      << " != " << tcolor_must_have_.cards_no()
                     );
      return false;
    }

    // for each tcolor check:
    for (unsigned c = 0; c < Card::number_of_tcolors; ++c) {
      auto const tcolor = static_cast<Card::TColor>(c);
      unsigned const played = cards_information().played(tcolor);
      { // played(player) <= played(all)
        if (played < this->played(tcolor)) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  tcolor '" << tcolor << "' has been played "
                          << played << " times, but by the player "
                          << playerno() << " '" << this->played(tcolor)
                          << "' times");
          return false;
        }
      } // played(player) <= played(all)
      unsigned const can_have = this->can_have(tcolor);
      { // check 'can have < must have'
        if ( !(can_have >= must_have(tcolor))) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  tcolor '" << tcolor << "': "
                          "can_have = " << can_have << " < "
                          "must_have = " << must_have(tcolor)
                         );
          return false;
        }
      } // check 'can have < must have'

      { // check 'can have < cards to play'
        if ( !(can_have <= player().cards_to_play())) {
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  tcolor '" << tcolor << "': "
                          "can_have = " << can_have << " > "
                          "cards to play = " << player().cards_to_play()
                         );
          return false;
        }
      } // check 'can have < cards to play'

      { // check 'can have + played + sum must have <= #Cards'
        unsigned sum = 0;
        for (unsigned p = 0; p < game().playerno(); ++p)
          if (p != playerno())
            sum += cards_information().of_player(p).must_have(tcolor);

        if (can_have + played + sum > game().cards().count(tcolor) ) {
#ifdef DEBUG_ASSERT
          for (unsigned p = 0; p < game().playerno(); ++p) {
            if (p != playerno())
              if (cards_information().of_player(p).must_have(tcolor) > 0)
                cerr << p << ": tcolor must have = " << cards_information().of_player(p).must_have(tcolor) << endl;
          }
#endif
          DEBUG_ASSERTION(false,
                          "CardsInformation"
                          << "(" << cards_information().player().no() << ")"
                          << "::OfPlayer(" << playerno() << ")"
                          << "::self_check()\n"
                          "  playerno = " << playerno() << "\n"
                          "  tcolor '" << tcolor << "':\n"
                          "  can_have + played + sum must_have > possible number:\n"
                          "  "
                          << can_have
                          << " + " << played << " + " << sum << " > "
                          << game().cards().count(tcolor)
                          //<< "\ngame:\n{\n" << game() << "}\n"
                          << "\n" << *this
                          //<< "\n" << cards_information()
                         );

          return false;
        }
      } // check 'can have + played + sum must have <= #Cards'
    } // for (c \in tcolors)

  } // tcolor

  return true;
} // bool CardsInformation::OfPlayer::self_check() const
