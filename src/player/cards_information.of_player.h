/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "cards_information.h"

/**
 ** Information of the cards a specific player can have
 **
 ** @author   Diether Knof
 **
 ** @todo     join the vector<Card> and vector<Card> functions (by begin, end iterators?)
 ** @todo     open shifted fox
 **/
class CardsInformation::OfPlayer {
  friend bool operator==(OfPlayer const& lhs, OfPlayer const& rhs);
  friend class CardsInformation;

  public:
  OfPlayer(CardsInformation& cards_information, unsigned playerno);
  OfPlayer(OfPlayer const& of_player);
  ~OfPlayer();

  // reset all informations
  void reset();


  // writes the information in 'ostr'
  void write(ostream& ostr) const;

  CardsInformation const& cards_information() const;
  CardsInformation& cards_information();
  unsigned playerno() const;

  // the corresponding game
  Game const& game() const;
  // the corresponding player
  Player const& player() const;

  // changes the corresponding cards information
  void set_cards_information(CardsInformation& cards_information);

  // whether all cards are known
  bool all_known() const;

  // how many cards 'card' the player has played
  unsigned played(Card card) const;
  // how many cards of the tcolor 'tcolor' the player has played
  unsigned played(Card::TColor tcolor) const;
  // how many cards the player must still have on the hand
  unsigned must_have(Card card) const;
  // how many cards the player must still have on the hand
  unsigned must_have(Card::TColor tcolor) const;
  // how many cards the player can still have on the hand
  unsigned can_have(Card card) const;
  // how many cards the player can still have on the hand
  unsigned can_have(Card::TColor tcolor) const;
  // whether the player cannot have the card
  bool cannot_have(Card card) const;
  // whether the player has not the color
  bool does_not_have(Card::TColor tcolor) const;
  // how the unkown number of 'card' (can have - must have)
  unsigned unknown(Card card) const;
  // how the unkown number of 'tcolor' (can have - must have)
  unsigned unknown(Card::TColor tcolor) const;


  // the weighting for the card
  int weighting(Card card, bool modify = true) const;


  unsigned played_cards_no() const;
  unsigned remaining_cards_no() const;
  // the cards the player has played
  Hand played_cards() const;
  // the cards the player must have
  Hand must_have_cards() const;
  // the cards the player cannot have
  Hand cannot_have_cards() const;
  // hand with all possible cards of the player
  Hand possible_hand() const;
  // the estimated hand for the player
  Hand estimated_hand() const;

  // checks, whether the hand is valid
  bool is_valid(Hand const& hand) const;
  int weighting(Hand const& hand) const;

  // the game starts
  void game_start();
  // the player has played 'card'
  void card_played(HandCard card, Trick const& trick);

  // change the weightings according to the played card
  void weight_played_card(HandCard card, Trick const& trick);

  // adds the information that the player must have the card 'no' times
  void add_must_have(Card card, unsigned no = 1);
  // adds the information that the player must have the cards
  void add_must_have(vector<Card> const& cards);
  // adds the information that the player must have the tcolor cards 'no' times
  void add_must_have(Card::TColor tcolor, unsigned no);

  // adds the information that the player can have the card at max 'no' times
  void add_can_have(Card card, unsigned no);
  // adds the information that the player can have the cards at max 'no' times
  void add_can_have(vector<Card> const& cards, unsigned no);
  // adds the information that the player can only have the cards
  void add_can_only_have(vector<Card> const& cards);
  // adds the information that the player can have the tcolor cards at max 'no' times
  void add_can_have(Card::TColor tcolor, unsigned no);
  // adds the information that the player must have the tcolor cards exactly 'no' times
  void add_must_exactly_have(Card::TColor tcolor, unsigned no);

  // adds the information that the player cannot have the card
  void add_cannot_have(Card card);
  // adds the information that the player cannot have the cards
  void add_cannot_have(vector<Card> const& cards);
  // whether the player has not the color
  void add_cannot_have(Card::TColor tcolor);

  // updates the information of the card
  void update_information(Card card);
  // updates the information of the tcolor
  void update_information(Card::TColor tcolor);
  // updates the information of the tcolors
  void update_tcolor_information();
  // updates 'can have' according to 'remaining cards'
  void update_remaining_cards();

  private:
  // check whether the cards the player can have are the ones
  // he has to have
  void check_can_is_must();
  // check whether the cards the player can have are the ones
  // he has to have
  void check_can_is_must(Card::TColor tcolor);
  // check whether the cards the player has to have are the only ones
  // he can have
  void check_must_is_can();
  // checks the data for error
  bool self_check() const;

  private:
  // the cards information
  CardsInformation* cards_information_;
  // the playerno
  unsigned playerno_ = UINT_MAX;

  // number of played cards of the player
  CardCounter played_;
  // number of cards, the player must have
  CardCounter must_have_;
  // number of cards, the player can have
  CardCounter can_have_;

  // number of played tcolors of the player
  TColorCounter tcolor_played_;
  // number of cards, the tcolors must have
  TColorCounter tcolor_must_have_;
  // number of cards, the tcolors can have
  TColorCounter tcolor_can_have_;

  // weighting for the cards for estimations
  // -100
  mutable map<Card, int> cards_weighting_;
}; // class CardsInformation::OfPlayer

// writes the cards information about a player into the output stream
ostream& operator<<(ostream& ostr,
		    CardsInformation::OfPlayer const& of_player);
// compares two cards informations about a player
bool operator==(CardsInformation::OfPlayer const& lhs,
		CardsInformation::OfPlayer const& rhs);
// compares two cards informations about a player
bool operator!=(CardsInformation::OfPlayer const& lhs,
		CardsInformation::OfPlayer const& rhs);
