/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/team.h"

enum class GuessedTeam {
  noteam,
  re,
  contra,
  surely_re,
  surely_contra,
  maybe_re,
  maybe_contra,
  unknown,
};
constexpr auto guessed_team_list = array<GuessedTeam, 8>{
  GuessedTeam::noteam,
  GuessedTeam::re,
  GuessedTeam::contra,
  GuessedTeam::surely_re,
  GuessedTeam::surely_contra,
  GuessedTeam::maybe_re,
  GuessedTeam::maybe_contra,
  GuessedTeam::unknown,
};

auto to_guessed(Team team) -> GuessedTeam;
auto surely(Team team)     -> GuessedTeam;
auto maybe(Team team)      -> GuessedTeam;
auto to_real(GuessedTeam guessed_team)        -> Team;
auto maybe_to_real(GuessedTeam guessed_team)  -> Team;

auto to_string(GuessedTeam team)                 -> string;
auto short_string(GuessedTeam team)              -> string;
auto gettext(GuessedTeam team)                   -> string;
auto operator<<(ostream& ostr, GuessedTeam team) -> ostream&;

auto operator>=(GuessedTeam lhs, GuessedTeam rhs) -> bool;
auto operator==(Team        lhs, GuessedTeam rhs) -> bool;
auto operator==(GuessedTeam lhs, Team        rhs) -> bool;
auto operator& (GuessedTeam lhs, GuessedTeam rhs) -> bool;
auto operator& (GuessedTeam lhs, Team        rhs) -> bool;
auto operator& (Team        lhs, GuessedTeam rhs) -> bool;
auto surely_equal(GuessedTeam lhs, GuessedTeam rhs) -> bool;
auto maybe_equal (GuessedTeam lhs, GuessedTeam rhs) -> bool;
auto is_real(GuessedTeam guessed_team)            -> bool;
auto is_surely(GuessedTeam guessed_team)          -> bool;
auto is_maybe(GuessedTeam guessed_team)           -> bool;
