/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "heuristicsmap.h"
#include "player.h"
#include "../game/game.h"
#include "../party/rule.h"

#include "../misc/bug_report.h"


auto HeuristicsMap::group(Game const& game) -> HeuristicsMap::GameTypeGroup
{
  switch (game.type()) {
  case GameType::normal:
    return GameTypeGroup::normal;
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
    DEBUG_ASSERT(false);
    break;
  case GameType::poverty:
    return GameTypeGroup::poverty;
  case GameType::marriage:
    if (game.marriage().selector() == MarriageSelector::team_set)
      return GameTypeGroup::normal;
    else
      return GameTypeGroup::marriage_undetermined;
  case GameType::marriage_solo:
    return GameTypeGroup::soli_color;
  case GameType::marriage_silent:
    return GameTypeGroup::marriage_silent;
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
    return GameTypeGroup::soli_single_picture;
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
    return GameTypeGroup::soli_double_picture;
  case GameType::solo_koehler:
    return GameTypeGroup::solo_koehler;
  case GameType::solo_club:
  case GameType::solo_spade:
  case GameType::solo_heart:
  case GameType::solo_diamond:
    return GameTypeGroup::soli_color;
  case GameType::solo_meatless:
    return GameTypeGroup::solo_meatless;
  } // switch(game.type())

  DEBUG_ASSERT(false);

  return GameTypeGroup::normal;
} // HeuristicsMap::GameTypeGroup HeuristicsMap::group(Game game)


auto HeuristicsMap::group(Player const& player) -> HeuristicsMap::PlayerTypeGroup
{
  PlayerTypeGroup group = PlayerTypeGroup::contra;

  switch(player.team()) {
  case Team::re:
    group = PlayerTypeGroup::re;
    break;
  case Team::contra:
    group = PlayerTypeGroup::contra;
    break;
  case Team::noteam:
  case Team::unknown:
    if (   player.game().type() == GameType::marriage
        && player.game().marriage().selector() != MarriageSelector::team_set) {
      group = PlayerTypeGroup::contra;
      break;
    }

#ifndef RELEASE
    cerr << "HeuristicsMap::group(player):\n"
      <<"  wrong team " << player.team()
      << endl;
#if 0
    ::bug_report.report("HeuristicsMap::group(player):\n"
                        "  wrong team " + ::to_string(player.team()));
#endif
    group = PlayerTypeGroup::contra;
    break;
#endif
#ifdef POSTPONED
    DEBUG_ASSERTION(false,
                    "HeuristicsMap::group(player):\n"
                    "  wrong team " << player.team());
#else
    group = PlayerTypeGroup::contra;
#endif
    break;
  } // switch(gametype)

  // special types

  // poverty
  if (    player.game().type() == GameType::poverty
      && player == player.game().players().soloplayer() )
    group = PlayerTypeGroup::special;

  return group;
} // HeuristicsMap::PlayerTypeGroup HeuristicsMap::group(Player const& player)


HeuristicsMap::Key::Key(Player const& player) :
  gametype_group(HeuristicsMap::group(player.game())),
  playertype_group(HeuristicsMap::group(player))
{ }


auto HeuristicsMap::operator==(HeuristicsMap::Key const lhs,
                               HeuristicsMap::Key const rhs) -> bool
{
  return (   lhs.gametype_group   == rhs.gametype_group
          && lhs.playertype_group == rhs.playertype_group);
}


auto HeuristicsMap::operator!=(HeuristicsMap::Key const lhs,
                               HeuristicsMap::Key const rhs) -> bool
{
  return !(lhs == rhs);
}


auto HeuristicsMap::operator<(HeuristicsMap::Key const lhs,
                              HeuristicsMap::Key const rhs) -> bool
{
  return (   lhs.gametype_group < rhs.gametype_group
          || (   lhs.gametype_group == rhs.gametype_group
              && lhs.playertype_group < rhs.playertype_group));
}


auto is_solo(GameTypeGroup const game_type) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
  case GameTypeGroup::marriage_undetermined:
    return false;
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::solo_meatless:
  case GameTypeGroup::soli_color:
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return true;
  }
  return false;
}


auto is_picture_solo(GameTypeGroup const game_type) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
  case GameTypeGroup::marriage_undetermined:
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::solo_meatless:
  case GameTypeGroup::soli_color:
    return false;
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return true;
  }
  return false;
}


auto is_color_solo(GameTypeGroup const game_type) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
  case GameTypeGroup::marriage_undetermined:
  case GameTypeGroup::solo_meatless:
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return false;
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::soli_color:
    return true;
  }
  return false;
}


auto is_with_trump(GameTypeGroup const game_type) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
  case GameTypeGroup::marriage_undetermined:
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::soli_color:
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return true;
  case GameTypeGroup::solo_meatless:
    return false;
  }
  return false;
}


auto is_with_trump_color(GameTypeGroup const game_type) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
  case GameTypeGroup::marriage_undetermined:
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::soli_color:
    return true;
  case GameTypeGroup::solo_meatless:
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return false;
  }
  return false;
}


auto is_with_partner(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
    return true;
  case GameTypeGroup::marriage_undetermined:
    return false;
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::soli_color:
  case GameTypeGroup::solo_meatless:
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return player_group == PlayerTypeGroup::contra;
  }
  return false;
}


auto is_with_special_points(GameTypeGroup const game_type) -> bool
{
  switch (game_type) {
  case GameTypeGroup::normal:
  case GameTypeGroup::poverty:
  case GameTypeGroup::marriage_undetermined:
    return true;
  case GameTypeGroup::marriage_silent:
  case GameTypeGroup::soli_color:
  case GameTypeGroup::solo_meatless:
  case GameTypeGroup::soli_single_picture:
  case GameTypeGroup::soli_double_picture:
  case GameTypeGroup::solo_koehler:
    return false;
  }
  return false;
}


auto to_string(HeuristicsMap::GameTypeGroup const group) -> string
{
  switch (group) {
  case HeuristicsMap::GameTypeGroup::normal:
    (void)_("HeuristicsMap::GameTypeGroup::normal");
    return "normal";
  case HeuristicsMap::GameTypeGroup::poverty:
    (void)_("HeuristicsMap::GameTypeGroup::poverty");
    return "poverty";
  case HeuristicsMap::GameTypeGroup::marriage_undetermined:
    (void)_("HeuristicsMap::GameTypeGroup::undetermined marriage");
    return "undetermined marriage";
  case HeuristicsMap::GameTypeGroup::marriage_silent:
    (void)_("HeuristicsMap::GameTypeGroup::silent marriage");
    return "silent marriage";
  case HeuristicsMap::GameTypeGroup::solo_meatless:
    (void)_("HeuristicsMap::GameTypeGroup::solo meatless");
    return "solo meatless";
  case HeuristicsMap::GameTypeGroup::soli_color:
    (void)_("HeuristicsMap::GameTypeGroup::soli color");
    return "soli color";
  case HeuristicsMap::GameTypeGroup::soli_single_picture:
    (void)_("HeuristicsMap::GameTypeGroup::soli single picture");
    return "soli single picture";
  case HeuristicsMap::GameTypeGroup::soli_double_picture:
    (void)_("HeuristicsMap::GameTypeGroup::soli double picture");
    return "soli double picture";
  case HeuristicsMap::GameTypeGroup::solo_koehler:
    (void)_("HeuristicsMap::GameTypeGroup::solo koehler");
    return "solo koehler";
  } // switch (group)

  return {};
}


auto HeuristicsMap::GameTypeGroup_from_string(string const& name) -> HeuristicsMap::GameTypeGroup
{
  for (auto g : game_type_group_all) {
    if (name == ::to_string(g)) {
      return g;
    }
  }
#ifdef DEPRECATED
  // 2018-01-23
  if (name == "default")
    return GameTypeGroup::normal;
#endif

  DEBUG_ASSERTION(false,
                  "HeuristicsMap::GameTypeGroup_from_name(" << name << ")\n"
                  "  did not find gametype group '" << name << "'");
  return GameTypeGroup::normal;
}


auto HeuristicsMap::PlayerTypeGroup_from_string(string const& name) -> HeuristicsMap::PlayerTypeGroup
{
  for (auto g : player_type_group_all) {
    if (name == ::to_string(g)) {
      return g;
    }
  }

  DEBUG_ASSERTION(false,
                  "HeuristicsMap::PlayerTypeGroup_from_name(\"" << name << "\")\n"
                  "  did not find playertype group '" << name << "'");
  return PlayerTypeGroup::re;
}


auto to_string(HeuristicsMap::PlayerTypeGroup group) -> string
{
  switch (group) {
  case HeuristicsMap::PlayerTypeGroup::contra:
    (void)_("HeuristicsMap::PlayerTypeGroup::contra");
    return "contra";
  case HeuristicsMap::PlayerTypeGroup::re:
    (void)_("HeuristicsMap::PlayerTypeGroup::re");
    return "re";
  case HeuristicsMap::PlayerTypeGroup::special:
    (void)_("HeuristicsMap::PlayerTypeGroup::special");
    return "special";
  } // switch (group)

  return {};
}


auto operator<<(ostream& ostr, HeuristicsMap::GameTypeGroup const group) -> ostream&
{
  ostr << ::to_string(group);
  return ostr;
}


auto operator<<(ostream& ostr, HeuristicsMap::PlayerTypeGroup const group) -> ostream&
{
  ostr << ::to_string(group);
  return ostr;
}


auto operator<<(ostream& ostr, HeuristicsMap::Key key) -> ostream&
{
  ostr << key.gametype_group << " / " << key.playertype_group;
  return ostr;
}


auto gettext(HeuristicsMap::GameTypeGroup const gametype_group) -> string
{
  return gettext("HeuristicsMap::GameTypeGroup::"
                 + ::to_string(gametype_group));
}


auto gettext(HeuristicsMap::PlayerTypeGroup const playertype_group) -> string
{
  return gettext("HeuristicsMap::PlayerTypeGroup::"
                 + ::to_string(playertype_group));
}
