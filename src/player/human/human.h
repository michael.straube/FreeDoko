/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../ai/ai.h"

class Human : public Ai {
public:
  Human();
  explicit Human(istream& istr);
  explicit Human(Name name);
  Human(Player const& player);
  Human(Player const& player, Aiconfig const& aiconfig);
  Human(Ai const& ai);
  Human(Human const& human);
  auto operator=(const Human& player) = delete;
  auto clone() const -> unique_ptr<Player> override;

  ~Human() override = default;

  auto get_reservation()            -> Reservation const& override;
  void game_start()                                       override;
  auto announcement_request() const -> Announcement       override;
  auto card_get()                   -> HandCard           override;

  void teaminfo_update() override;

  auto poverty_shift()                                   -> HandCards override;
  auto poverty_take_accept(unsigned cardno)              -> bool      override;
  auto poverty_cards_change(vector<Card> const& cards)   -> HandCards override;
  void poverty_cards_get_back(vector<Card> const& cards)              override;

  void swines_announced(Player const& player) override;
};
