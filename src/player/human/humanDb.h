/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */


#pragma once

#ifdef POSTPHONED

#include "../playersDb.h"

class HumanDb : public PlayersDb
{
public:
  HumanDb() :
    PlayersDb(),
    description_(),
    private_( false ),
    pixmap_( "" )  
  {
  } 

  HumanDb( HumanDb& h ) :
    PlayersDb( h ),
    description_( h.description_ ),
    private_( h.private_ ),
    pixmap_( h.pixmap_ )  
  {
  } 

  auto description() -> std::string
  { return description_; }

  auto isPrivate() -> bool
  { return private_; }

  auto pixmap() -> char*
  { return pixmap_; }

private:
  std::string description_;    
  bool private_;
  char* pixmap_;
}; // class HumanDb : public PlayersDb

#endif // #ifdef POSTPHONED
