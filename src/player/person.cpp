/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "person.h"
#include "../party/party.h"
#include "../misc/preferences.h"
#include "../ui/ui.h"

auto Person::default_name(Type const type, unsigned const playerno) -> Name
{
  switch(type) {
  case Type::human:
    return ::preferences(Preferences::Type::name);
  default:
    switch (playerno) {
    case 0:
      return "Mara"s;
    case 1:
      return "Gerd"s;
    case 2:
      return "Erika"s;
    case 3:
      return "Sven"s;
    case 4:
      return "Tina"s;
    case 5:
      return "Alex"s;
    case UINT_MAX:
      return "Kai"s;
    default:
      return {};
    }
  }

  return to_string(type);
}


auto Person::default_voice(Type const type, unsigned const playerno, Name const name) -> Voice
{
  switch(type) {
  case Type::human:
#ifdef POSTPONED
    // todo: add check, that the voice exists
    return ::preferences(Preferences::Type::name);
#endif
  default:
    switch (playerno) {
    case 0:
      return "female/mbrola"s;
    case 1:
      return "male/mbrola"s;
    case 2:
      return "female/mbrola"s;
    case 3:
      return "male/mbrola"s;
    case 4:
      return "female/mbrola"s;
    case 5:
      return "man/mbrola"s;
    default:
      return ""s;
    }
  }

  return to_string(type);
}

Person::Person(Type const type, Name const name) :
  type_(type),
  name_(name)
{ }

Person::Person(Person const& person) = default;

Person& Person::operator=(Person const& person) = default;

auto Person::clone() const -> unique_ptr<Person>
{
  return make_unique<Person>(*this);
}

Person::~Person() = default;

auto Person::type() const noexcept -> Type
{
  return type_;
}

auto Person::read(Config const& config) -> bool
{
#ifndef OUTDATED
  // 0.7.3
  if (   config.name == "name"
      || config.name == "Name")
#else
    if (config.name == "name")
#endif
    {
      name_ = config.value;
#ifndef RELEASE
      if (name_.length() > 8 && name_.substr(0, 8) == "FreeDoko")
        name_.erase(0, 8);
#endif
      return true;
    } else if (config.name == "voice") {
      voice_ = config.value;
      return true;
    } else if (config.name == "color") {
      color_ = config.value;
      return true;
#ifndef OUTDATED
      // 0.7.3
    } else if (   config.name == "type"
               || config.name == "Type") {
#else
    } else if (config.name == "Type") {
#endif
      if (type_ != Type::unset) {
        cerr << "Type already set, ignoring further one" << endl;
        return true;
      }
      if (config.value == to_string(Type::human)) {
        type_ = Type::human;
      } else if (config.value == to_string(Type::ai)) {
        type_ = Type::ai;
      } else if (config.value == to_string(Type::ai_dummy)) {
        type_ = Type::ai_dummy;
      } else if (config.value == to_string(Type::ai_random)) {
        type_ = Type::ai_random;
      } else { // if (config.value == 'type')
        DEBUG_ASSERTION(false,
                        "Person::read(config):\n"
                        "  player type '" << config.value << "' not known");
      }
      return true;
    }
  return false;
}


void Person::set_type(Type const type)
{
  type_ = type;
}

auto Person::name() const -> Name
{
  if (name_.empty()) {
    if (::party)
      return Person::default_name(type(), ::party->players().no(*this));
    else
      return Person::default_name(type(), 0);
  }

  return name_;
}

auto Person::name_value() const noexcept -> Name
{
  return name_;
}

void Person::set_default_name()
{
  if (::party)
    set_name(Person::default_name(type_, ::party->players().no(*this)));
}

void Person::set_name(Name const name)
{
  name_ = name;

  if (::party && ::party->players().no(*this) != UINT_MAX) {
    if (::ui)
      ::ui->name_changed(*this);
  }
}

auto Person::voice() const -> Voice
{
  if (voice_.empty()) {
    if (::party)
      return Person::default_voice(type_,
                                   ::party->players().no(*this),
                                   name());
  }

  return voice_;
}

void Person::set_default_voice()
{
  if (::party)
    set_voice(Person::default_voice(type_,
                                    ::party->players().no(*this),
                                    name()));
}

void Person::set_voice(string const voice)
{
  voice_ = voice;
  ::ui->voice_changed(*this);
}

auto Person::voice_value() const noexcept -> Voice
{
  return voice_;
}

auto Person::color() const noexcept -> Color
{
  return color_;
}

void Person::set_color(Color const color)
{
  color_ = color;
}

bool operator==(Person const& lhs, Person const& rhs)
{
  return (&lhs == &rhs);
}

bool operator!=(Person const& lhs, Person const& rhs)
{
  return !(lhs == rhs);
}

auto operator<<(ostream& ostr, Person const& person) -> ostream&
{
  ostr << "name = " << person.name_value() << '\n';
  if (person.name_value().empty())
    ostr << "# name = " << person.name() << '\n';
  ostr << "voice = " << person.voice() << '\n'
    << "type = "     << person.type()  << '\n'
    << "color = "    << person.color() << '\n';

  return ostr;
}

auto to_string(Person::Type const type) -> string
{
  switch(type) {
  case Person::Type::unset:
    (void)_("Person::Type::player type unset");
    return "player type unset";
  case Person::Type::human:
    (void)_("Person::Type::human");
    return "human";
  case Person::Type::ai:
    (void)_("Person::Type::ai");
    return "ai";
  case Person::Type::ai_dummy:
    (void)_("Person::Type::dummy ai");
    return "dummy ai";
  case Person::Type::ai_random:
    (void)_("Person::Type::random ai");
    return "random ai";
  }

  return "";
}

auto gettext(Person::Type const type) -> string
{
  return gettext("Person::Type::" + to_string(type));
}

auto operator<<(ostream& ostr, Person::Type const type) -> ostream&
{
  ostr << to_string(type);
  return ostr;
}
