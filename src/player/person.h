/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

/** the base class for a person (human or ai)
 **/
class Person {
public:
  enum class Type {
    unset     = 0,
    human     = 0x010,
    ai        = 0x100,
    ai_dummy  = 0x101,
    ai_random = 0x102
  }; // enum Type

  using Name  = string;
  using Voice = string;
  using Color = string;

public:
  static auto default_name(Type type, unsigned playerno) -> Name;
  static auto default_voice(Type type, unsigned playerno, Name name) -> Voice;

public:
  Person() = delete;
  Person(Type type, Name name);
  Person(Person const& person);
  auto operator=(Person const& person) -> Person&;

  auto clone() const -> unique_ptr<Person>;

  virtual ~Person();

  auto read(Config const& config) -> bool;

  auto type()                 const noexcept -> Type;
  void set_type(Type type);

  auto name()                 const          -> Name;
  auto name_value()           const noexcept -> Name;
  void set_default_name();
  void set_name(Name name);

  auto voice()                const          -> Voice;
  auto voice_value()          const noexcept -> Voice;
  void set_default_voice();
  void set_voice(Voice voice);

  auto color()                const noexcept -> Color;
  void set_color(Color color);

private:
  Type type_ = Type::unset;
  Name name_;
  Voice voice_;
  Color color_;
};

auto operator<<(ostream& ostr, Person const& player) -> ostream&;

auto to_string(Person::Type type) -> string;
auto gettext(Person::Type type)   -> string;
auto operator<<(ostream& ostr, Person::Type type) -> ostream&;

auto operator==(Person const& lhs, Person const& rhs) -> bool;
auto operator!=(Person const& lhs, Person const& rhs) -> bool;

