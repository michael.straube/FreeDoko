/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "playersDb.h"
#include "../game/game.h"
#include "../game/game_summary.h"

#include "../basetypes/program_flow_exception.h"
#include <sstream>
using std::istringstream;


void TLWcount::write(ostream& ostr) const
{
  ostr << total_ << '\t'
    << won_ << '\t'
    << lost_;
}


void TLWcount::read(istream& istr)
{
  istr >> total_
    >> won_
    >> lost_;
}


void TLWcount::hasLost()
{
  total_ += 1;
  lost_  += 1;
};


void TLWcount::hasWon()
{
  total_ += 1;
  won_   += 1;
};


auto TLWcount::total() const -> Count
{
  return total_;
}


auto TLWcount::lost() const -> Count
{
  return lost_;
}


auto TLWcount::won() const -> Count
{
  return won_;
}


void TLWcount::clear()
{
  total_ = 0;
  lost_  = 0;
  won_   = 0;
}

auto TLWcount::operator+=(TLWcount const& tlwcount) -> TLWcount&
{
  total_ += tlwcount.total_;
  lost_  += tlwcount.lost_;
  won_   += tlwcount.won_;
  return *this;
}


auto operator+(TLWcount const& lhs, TLWcount const& rhs) -> TLWcount 
{
  TLWcount result = lhs;
  result += rhs;
  return result;
}


auto operator<<(ostream& ostr, TLWcount const& tlwcount) -> ostream&
{
  tlwcount.write(ostr);
  return ostr;
}


auto operator>>(istream& istr, TLWcount& tlwcount) -> istream&
{
  tlwcount.read(istr);
  return istr;
}


void Ranking::write(ostream& ostr) const
{ 
  ostr << value_;
}


void Ranking::read(istream& istr)
{
  istr >> value_;
}


void Ranking::add(Ranking const& r)
{
  value_ = ( 0.9 * value_ + r.value_ ) / 1.9;
}


auto Ranking::value() const -> double
{
  return value_;
}


auto Ranking::operator+=(Ranking const& b) -> Ranking&
{
  add(b);
  return *this;
};


void Ranking::clear()
{
  value_ = 0;
}


auto operator<<(ostream& ostr, Ranking const& ranking) -> ostream&
{
  ranking.write(ostr);
  return ostr;
}


auto operator>>(istream& istr, Ranking& ranking) -> istream&
{
  ranking.read(istr);
  return istr;
}


auto operator<<(ostream& ostr, PlayersDb const& players_db) -> ostream&
{
  players_db.write(ostr);
  return ostr;
}


auto operator>>(istream& istr, PlayersDb& players_db) -> istream&
{
  players_db.read(istr);
  return istr;
}


PlayersDb::PlayersDb(istream& istr)
{
  read(istr);
}


void PlayersDb::write(ostream& ostr) const
{
  ostr << setw(24) << "ranking" << " =\t"
    << rank << '\n';
  ostr << setw(24) << "total games" << " =\t"
    << games_all() << '\n';
  ostr << setw(24) << "total special points" << " =\t"
    << specialpoints_all() << '\n';

  ostr << setw(24) << "average game points" << " =\t"
    << averageGamePoints() << '\n';
  ostr << setw(24) << "average game trick points" << " =\t"
    << averageGameTrickPoints() << '\n';

  ostr << '\n';

  ostr << "games\n"
    << "{\n";
  for (auto gt : game_type_list)
    ostr << setw(32) << to_string(gt) << " =\t"
      << games(gt) << '\n';
  ostr <<"}\n";

  ostr << '\n';

  ostr << "specialpoints\n"
    << "{\n";
  for (auto const sp : Specialpoint::types)
    ostr << setw(32) << to_string(sp) << " =\t"
      << specialpoints(sp) << '\n';
  ostr <<"}\n";
}


void PlayersDb::read(istream& istr)
{
  // first clear all
  averageGamePoints_ = 0;
  averageGameTrickPoints_ = 0;
  games_all_ = TLWcount();
  games_.clear();
  specialpoints_all_ = TLWcount();
  specialpoints_.clear();

  unsigned depth = 0;
  while (istr.good()) {
    Config config;
    istr >> config;
    if (istr.fail() || istr.eof())
      break;

    istringstream istr_value(config.value);
    if (!config.separator
        && (config.name == "{")) {
      depth += 1;
    } else if (!config.separator
               && (config.name == "}")) {
      if (depth == 0) {
        cerr << "players database: "
          << "found a '}' without a '{' before.\n"
          << "Finish reading the the file."
          << endl;
        break;
      } // if (depth == 0)
      depth -= 1;
      if (depth == 0)
        break;
    } else if (config.name == "ranking")
      rank.read(istr_value);
    else if (config.name == "total games")
      games_all_.read(istr_value);
    else if (config.name == "total special points")
      specialpoints_all_.read(istr_value);
    else if (config.name == "average game points")
      istr_value >> averageGamePoints_;
    else if (config.name == "average game trick points")
      istr_value >> averageGameTrickPoints_;
    else if (!config.separator
             && read_group(config.name, istr)) // NOLINT
      // this complex reading is necessary to easily add groups in child
      // classes
      ;
    else {
      cerr << "Reading the player database:\n"
        << "found following unknown line:\n"
        << (config.separator
            ? (config.name + " = " + config.value)
            : config.name + config.value)
        << '\n'
        << "ignoring it."
        << endl;
      SEGFAULT;
      std::exit(EXIT_FAILURE);
    }
  }; // while (istr.good())
}


auto PlayersDb::read_group(string const& group, istream& istr) -> bool
{
  if (group == "games")
    read_group_games(istr);
  else if (group == "specialpoints")
    read_group_specialpoints(istr);
  else
    return false;

  return true;
} // bool PlayersDb::read_group(string const& group, istream& istr)


void PlayersDb::read_group_games(istream& istr)
{
  unsigned depth = 0;

  // load the configuration
  while (istr.good()) {
    Config config;
    istr >> config;

    if (config.separator) {
      istringstream istr_value(config.value);
      istr_value >> games_[game_type_from_string(config.name)];
    } else { // if (config.separator)
      if (config.name == "{") {
        depth += 1;
      } else if (config.name == "}") {
        if (depth == 0) {
          cerr << "players database: group 'games': "
            << "found a '}' without a '{' before.\n"
            << "Finish reading the the file."
            << endl;
          break;
        } // if (depth == 0)
        depth -= 1;
        if (depth == 0)
          break;
      } else if (config.name.empty()) {
        cerr << "players database: group 'games': "
          << "Ignoring line \'" << config.value << "\'.\n";
      } else {
        cerr << "players database: group 'games': "
          << "type '" << config.name << "' unknown.\n"
          << "Ignoring it.\n";
      } // if (config.name == .)
    } // config.separator
  } // while (istr.good())
}


void PlayersDb::read_group_specialpoints(istream& istr)
{
  unsigned depth = 0;

  // load the configuration
  while (istr.good()) {
    Config config;
    istr >> config;

    if (config.separator) {
      try {
        istringstream istr_value(config.value);
        istr_value >> specialpoints_[specialpoint_type_from_string(config.name)];
      } catch (...) {
        cerr << "players database: group 'specialpoints': "
          << "unknown type '" << config.name << "'."
          << endl;
      }
    } else { // if (config.separator)
      if (config.name == "{") {
        depth += 1;
      } else if (config.name == "}") {
        if (depth == 0) {
          cerr << "players database: group 'specialpoints': "
            << "found a '}' without a '{' before.\n"
            << "Finish reading the the file."
            << endl;
          break;
        } // if (depth == 0)
        depth -= 1;
        if (depth == 0)
          break;
      } else if (config.name.empty()) {
        cerr << "players database: group 'specialpoints': "
          << "Ignoring line \'" << config.value << "\'.\n";
      } else {
        cerr << "players database: group 'specialpoints': "
          << "type '" << config.name << "' unknown.\n"
          << "Ignoring it.\n";
      } // if (config.name == .)
    } // config.separator
  } // while (istr.good())
} // void PlayersDb::read_group_specialpoints(istream& istr)


void PlayersDb::evaluate(Player const& p, GameSummary const& g)
{
  if ( g.winnerteam() == p.team() )
  {

    games_[g.game_type()].hasWon();
    rank.add( Ranking( 1.0 ) );
    averageGamePoints_ = (( averageGamePoints_ * games_all_.total() ) + g.points() )
      / ( games_all_.total() + 1 );
    games_all_.hasWon();
  } else
  {
    games_[g.game_type()].hasLost();
    rank.add( Ranking( -1.0 ) );
    games_all_.hasLost();
  }

  averageGameTrickPoints_ =
    ( ( averageGameTrickPoints_ * games_all_.total() ) + g.points(p) )
    / (games_all_.total()+1);

  auto spv = g.specialpoints();

  for (auto const& i : spv) {
    if (   i.player_get_no == p.no()
        || i.player_get_no == UINT_MAX)
      if (i.counting_team == p.team()) {
        specialpoints_[i.type].hasWon();
        specialpoints_all_.hasWon();
      }
    if (   i.player_of_no == p.no()
        || i.player_of_no == UINT_MAX)
      if (i.counting_team != p.team()) {
        specialpoints_[i.type].hasLost();
        specialpoints_all_.hasLost();
      }
  }
} // virtual void PlayerDb::evaluate(Player p, GamePoints g )


void PlayersDb::clear()
{
  rank.clear();

  games_.clear();
  specialpoints_.clear();

  averageGamePoints_ = 0;
  averageGameTrickPoints_ = 0;

  games_all_.clear();
  specialpoints_all_.clear();
}


auto PlayersDb::games(GameType const game_type) const -> TLWcount const&
{
  return games_[game_type];
}	

/// number of special points made or given away
auto PlayersDb::specialpoints(Specialpoint::Type const type) const -> TLWcount const&
{
  return specialpoints_[type];
}	

auto PlayersDb::games_all() const -> TLWcount const&
{
  return games_all_;
}

auto PlayersDb::games_group_marriage() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_marriage);
}


auto PlayersDb::games_group_poverty() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_poverty);
}


auto PlayersDb::games_group_solo() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_solo);
}


auto PlayersDb::games_group_solo_color() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_color_solo);
}


auto PlayersDb::games_group_solo_picture() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_picture_solo);
}


auto PlayersDb::games_group_solo_picture_single() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_single_picture_solo);
}


auto PlayersDb::games_group_solo_picture_double() const -> TLWcount
{
  auto const num_games = [this](GameType const gt) {
    return games(gt);
  };
  return sum_if(game_type_list, num_games, is_double_picture_solo);
}


auto PlayersDb::specialpoints_all() const -> TLWcount const&
{
  return specialpoints_all_;
}


auto PlayersDb::specialpoints_group_winning() const -> TLWcount
{
  auto const num_specialpoints = [this](Specialpoint::Type const sp) {
    return specialpoints(sp);
  };
  return sum_if(Specialpoint::types, num_specialpoints, is_winning);
}


auto PlayersDb::specialpoints_group_announcement() const -> TLWcount
{
  auto const num_specialpoints = [this](Specialpoint::Type const sp) {
    return specialpoints(sp);
  };
  return sum_if(Specialpoint::types, num_specialpoints, is_announcement);
}


auto PlayersDb::averageGamePoints() const -> double
{ return averageGamePoints_; }

/// average point made with trikcs in each game
auto PlayersDb::averageGameTrickPoints() const -> double
{ return averageGameTrickPoints_; }
