/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../game/specialpoint.h"
#include "player.h"
class GameSummary;

/// [T]otal[L]ost[W]on count
class TLWcount {
public:
  using Count = unsigned long long;
public:
  TLWcount()                                   = default;
  TLWcount(TLWcount const&)                    = default;
  auto operator=(TLWcount const&) -> TLWcount& = default;
  ~TLWcount()                                  = default;

  void write(ostream& ostr) const;
  void read(istream& istr);

  void hasLost();
  void hasWon();

  auto total() const -> Count;
  auto lost()  const -> Count;
  auto won()   const -> Count;

  void clear();

  auto operator+=(TLWcount const& tlwcount) -> TLWcount&;

private:
  Count total_ = 0;
  Count lost_  = 0;
  Count won_   = 0;
}; // class TLWcount

auto operator+(TLWcount const& lhs, TLWcount const& rhs) -> TLWcount;

auto operator<<(ostream& ostr, TLWcount const& tlwcount) -> ostream&;
auto operator>>(istream& istr, TLWcount& tlwcount)       -> istream&;


class Ranking {
public:
  Ranking(double const d = 0) : value_(d) {};
  auto operator=(Ranking const& r) -> Ranking& = default;
  ~Ranking() = default;

  void write(ostream& ostr) const;
  void read(istream& istr);

  void add(Ranking const& r);

  auto value() const -> double;

  auto operator+=(Ranking const& b) -> Ranking&;

  void clear();

private:
  double value_ = 0;
}; // class Ranking

auto operator<<(ostream& ostr, Ranking const& ranking) -> ostream&;
auto operator>>(istream& istr, Ranking& ranking)       -> istream&;


class PlayersDb {
public:
  PlayersDb() = default;
  explicit PlayersDb(istream& istr);
  PlayersDb(PlayersDb const& p) = default;

  virtual ~PlayersDb() = default;

  virtual void write(ostream& ostr) const;
  void read(istream& istr);
protected:
  virtual auto read_group(string const& name, istream& istr) -> bool;
private:
  void read_group_games(istream& istr);
  void read_group_specialpoints(istream& istr);
public:

  virtual void evaluate(Player const& p, GameSummary const& game_summary );

  auto games(GameType game_type)             const -> TLWcount const&;
  auto specialpoints(Specialpoint::Type type) const -> TLWcount const&;

  auto games_all()                       const -> TLWcount const&;
  auto games_group_marriage()            const -> TLWcount;
  auto games_group_poverty()             const -> TLWcount;
  auto games_group_solo()                const -> TLWcount;
  auto games_group_solo_color()          const -> TLWcount;
  auto games_group_solo_picture()        const -> TLWcount;
  auto games_group_solo_picture_single() const -> TLWcount;
  auto games_group_solo_picture_double() const -> TLWcount;

  auto specialpoints_all()                const -> TLWcount const&;
  auto specialpoints_group_winning()      const -> TLWcount;
  auto specialpoints_group_announcement() const -> TLWcount;

  auto averageGamePoints()      const -> double;
  auto averageGameTrickPoints() const -> double;

  virtual void clear();

  Ranking rank;

private:
  double averageGamePoints_ = 0;
  double averageGameTrickPoints_ = 0;

  TLWcount games_all_;
  mutable std::map<GameType, TLWcount> games_;

  TLWcount specialpoints_all_;
  mutable std::map<Specialpoint::Type, TLWcount> specialpoints_;
};

auto operator<<(ostream& ostr, PlayersDb const& players_db) -> ostream&;
auto operator>>(istream& istr, PlayersDb& players_db)       -> istream&;
