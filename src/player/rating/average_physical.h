/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "rating.h"

/**
 ** average rater
 ** Returns the average value of the given values,
 ** skipping the maximal and the minimal value
 **/
class Rating::AveragePhysical : public Rating
{
  public:
    AveragePhysical() : Rating(Type::average_physical) { }
    ~AveragePhysical() override = default;

    auto value() const -> int override
    {
      if (this->values.empty())
	return INT_MIN;
      if (this->values.size() == 1)
        return *this->values.begin();
      if (this->values.size() == 2)
        return (*this->values.begin() + *this->values.rbegin()) / 2;

      this->sort();
      int sum = 0;
      for (auto v = this->values.begin() + 1;
           v + 1 != this->values.end();
           ++v)
        sum += *v;

      return sum / ((static_cast<int>(this->values.size()) - 2) / 2);
    }
}; // class Rating::AveragePhysical : public Rating
