/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "rating.h"

/**
 ** reverse linear rater
 ** Returns the reverse linear value calculated by the given values.
 ** 2/(n*(n+1)) * \\Sum_i=1^n (n - i) * value[i]
 **/
class Rating::LinearReverse : public Rating {
  public:
    LinearReverse() : Rating(Type::linear_reverse) { }
    ~LinearReverse() override = default;

    auto value() const -> int override
    {
      if (this->values.empty())
	return INT_MIN;
      this->sort();
      int v = 0;
      for (int i = 0; i < static_cast<int>(this->values.size()); ++i)
	v += (this->values.size() - i) * this->values[i];
      return v / static_cast<int>( (this->values.size()
				    * (this->values.size() + 1)
				   ) / 2);
    }
}; // class Rating::LinearReverse : public Rating
