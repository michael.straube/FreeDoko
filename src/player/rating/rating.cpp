/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "rating.h"
#include "ratings.h"

Rating::Rating(Type const type) :
  type_(type)
{ }


auto Rating::create(Type const type) -> unique_ptr<Rating>
{
  switch(type) {
  case Type::max:
    return make_unique<Max>();
  case Type::min:
    return make_unique<Min>();
  case Type::second_max:
    return make_unique<SecondMax>();
  case Type::second_min:
    return make_unique<SecondMin>();
  case Type::median:
    return make_unique<Median>();
  case Type::average:
    return make_unique<Average>();
  case Type::average_physical:
    return make_unique<AveragePhysical>();
  case Type::linear:
    return make_unique<Linear>();
  case Type::linear_reverse:
    return make_unique<LinearReverse>();
  } // switch(type)
  return {};
}


auto operator<<(ostream& ostr, Rating const& rating) -> ostream&
{
  rating.write(ostr);
  return ostr;
}


void Rating::write(ostream& ostr) const
{
  ostr << to_string(type_) << ": " << value() << '\n';
  for (auto const i : values)
    ostr << "  " << i << '\n';
}


void Rating::add(int const value)
{
  values.push_back(value);
}


void Rating::delete_worst(size_t const n)
{
  if (values.size() <= n) {
    values.clear();
    return ;
  }
  sort();
  values.erase(values.begin(), values.begin() + n);
}


void Rating::sort() const
{
  std::sort(values.begin(), values.end());
}


auto to_string(Rating::Type const type) -> string
{
  switch(type) {
  case Rating::Type::max:
    (void)_("Rating::maximum");
    return "maximum";
  case Rating::Type::min:
    (void)_("Rating::minimum");
    return "minimum";
  case Rating::Type::second_max:
    (void)_("Rating::second maximum");
    return "second maximum";
  case Rating::Type::second_min:
    (void)_("Rating::second minimum");
    return "second minimum";
  case Rating::Type::median:
    (void)_("Rating::median");
    return "median";
  case Rating::Type::average:
    (void)_("Rating::average");
    return "average";
  case Rating::Type::average_physical:
    (void)_("Rating::physical average");
    return "physical average";
  case Rating::Type::linear:
    (void)_("Rating::linear");
    return "linear";
  case Rating::Type::linear_reverse:
    (void)_("Rating::linear reverse");
    return "linear reverse";
  } // switch(type)
  return {};
}


auto gettext(Rating::Type const type) -> string
{
  return gettext("Rating::" + to_string(type));
}


auto operator<<(ostream& ostr, Rating::Type const type) -> ostream&
{ return (ostr << to_string(type)); }


auto Rating::type_from_string(string const& name) -> Rating::Type
{
  for (auto r : type_list)
    if (to_string(r) == name)
      return r;
  throw name;
  return Type::max;
}
