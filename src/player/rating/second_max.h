/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "rating.h"

/**
 ** second maximum rater
 ** returns the second maximal value of the given values
 **/
class Rating::SecondMax : public Rating
{
  public:
    SecondMax() : Rating(Type::second_max) { }
    ~SecondMax() override = default;

    auto value() const -> int override
    { 
      if (this->values.empty())
        return INT_MIN;
      if (this->values.size() == 1)
        return *this->values.begin();
      this->sort();
      return *(this->values.rbegin() + 1);
    }
}; // class Rating::SecondMax : public Rating
