/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "team_information.h"
#include "team_information.heuristics.h"

#include "player.h"
#include "cards_information.of_player.h"
#include "ai/ai.h"

#include "../card/trick.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"
#include "../game/gameplay.h"
#include "../misc/preferences.h"
#include "../ui/ui.h"

#ifdef CHECK_RUNTIME
#include "../../runtime.h"
#endif

// For configuration values search for '*Value*'

/*
 * TeamInformation handles the information a player has which team the other
 * players are / can be.
 * If the team of a player is not known, the actions of the player are rated
 * (pro/contra 're'/'contra'). The rating of the unknown players are compared
 * and if they do not lie close together the teams are set to the 'maybe' team.
 *
 * Certain information:
 * the certain information is (mostly) taken from 'game', but I will lay down
 * here the ideas.
 * The only interesting case is a normal game, in every other case the team
 * information is known (marriage is a special case till the determination).
 * - announcement 're'/'contra'
 *   the team of the player is 're'/'contra'
 * - playing of a club queen
 *   the team of the player is 're'
 * - not serving trump
 *   the team of the player is 'contra' if it is not already 're'
 * - information from 'CardsInformation'
 *   if 'CardsInformation' can say something about having / not having the club
 *   queen we add this information.
 * - counting 're's and 'contra's
 *   if there are two re players, the remaining players must be contra
 *   if there are three contra players, the remaining player must be re
 *     (remember the silent marriage!)
 *   if there are two contra players, the player itself is re and plays no
 *     silent marriage, the remaining player is re
 *
 * Uncertain information:
 * According to some heuristics (see 'TeamInformationHeuristic') the actions
 * of the players are rated (here we assume the other players do play
 * intelligently). Positive means pro re, negative means pro contra.
 * When the rating of the unknown players differ enough the player(s) with the
 * higher value is set to 'maybe re', the other(s) to 'maybe contra'
 * (see 'TeamInformation::update_team()' for more explicit information).
 * I do also check for a silent marriage:
 *   If there are already two contra and one re player, the remaining player is
 *   generally set to 'maybe re', but if he has a strong negative value he is
 *   also set to 'maybe contra'.
 */

TeamInformation::TeamInformation(Player const& player) :
  player_(&player),
  team_values_(4, 0), // ToDo: player.game().playerno(),
  teams_(4, GuessedTeam::unknown) // ToDo: player.game().playerno(),
{
  reset();
}


TeamInformation::TeamInformation(TeamInformation const& team_information) = default;


auto TeamInformation::operator=(TeamInformation const& team_information) -> TeamInformation& = default;


TeamInformation::~TeamInformation() = default;


auto operator<<(ostream& ostr, TeamInformation const& team_information) -> ostream&
{
  team_information.write(ostr);
  return ostr;
}

void TeamInformation::write(ostream& ostr) const
{
  ostr << "team information:\n"
    << "{\n";

  for (unsigned p = 0; p < game().playerno(); ++p)
    ostr << p << ": "
      << setw(5)
      << team_value(p) << ' '
      << team(p)
      << '\n';

  ostr << "}\n";
}

auto TeamInformation::player() const -> Player const&
{
  return *player_;
}

auto TeamInformation::teams() const -> vector<GuessedTeam> const&
{
  return teams_;
}

auto TeamInformation::known_teams_no() const -> unsigned
{
  return known_teams_no_;
}

void TeamInformation::set_player(Player const& player)
{
  player_ = &player;
}

auto TeamInformation::game() const -> Game const&
{
  return player().game();
}

auto TeamInformation::team_value(Player const& player) const -> int
{
  return team_value(player.no());
}

auto TeamInformation::team_value(unsigned const playerno) const -> int
{
  if (game().status() >= Game::Status::play)
    return (team_values_[playerno]
            + player().cards_information().of_player(playerno).weighting(Card::club_queen, false) / 10);
  else
    return team_values_[playerno];
}

auto TeamInformation::team(Player const& player) const -> GuessedTeam
{
  return teams_[player.no()];
}

auto TeamInformation::team(unsigned const playerno) const -> GuessedTeam
{
  return teams_[playerno];
}

auto TeamInformation::partner(Player const& player) const -> Player const*
{
  if (!is_real(team(player)))
    return {};
  auto const team = to_real(this->team(player));
  Player const* partner = nullptr;
  // search partner
  unsigned p = 0;
  for (; p < game().playerno(); ++p) {
    if (   p != player.no()
        && this->team(p) == team) {
      partner = &game().player(p);
      break;
    }
  }

  // check it is the only possible partner
  for (p += 1; p < game().playerno(); ++p) {
    if (   p != player.no()
        && this->team(p) & team) {
      return nullptr;
    }
  }

  return partner;
}

auto TeamInformation::partner() const -> Player const*
{
  if (game().is_solo())
    return nullptr;

  auto const team = player().team();
  if (!is_real(team))
    return nullptr;

  Player const* partner = nullptr;
  // search possible partner
  unsigned p = 0;
  for (; p < game().playerno(); ++p) {
    if (   p != player().no()
        && this->team(p) == team) {
      if (partner) // Mehr als einen möglichen Partner gefunden
        return nullptr;
      partner = &game().player(p);
    }
  }
  return partner;
}

auto TeamInformation::guessed_partner() const -> Player const*
{
  Player const* partner = nullptr;
  // search possible partner
  unsigned p = 0;
  for (; p < game().playerno(); ++p) {
    if (   p != player().no()
        && team(p) >= surely(player().team())) {
      if (partner) // Mehr als einen möglichen Partner gefunden
        return nullptr;
      partner = &game().player(p);
    }
  }
  return partner;
}

auto TeamInformation::maybe_partner() const -> Player const*
{
  Player const* partner = guessed_partner();
  if (partner)
    return partner;
  // search possible partner
  unsigned p = 0;
  for (; p < game().playerno(); ++p) {
    if (   p != player().no()
        && team(p) >= maybe(player().team())) {
      if (partner) // Mehr als einen möglichen Partner gefunden
        return nullptr;
      partner = &game().player(p);
    }
  }
  return partner;
}

void TeamInformation::set_teams(vector<Team> const& teams)
{
  DEBUG_ASSERTION(teams.size() == teams_.size(),
                  "TeamInformation::set_teams(teams)\n"
                  "  size of parameter = " << teams.size() << " != " << teams_.size() << " = size of interal teams vector");
  auto const teams_bak = teams_;

  for (size_t p = 0; p < teams.size(); ++p) {
    teams_[p] = to_guessed(teams[p]);
  }

  // recalculate the number of known teams
  known_teams_no_ = 0;
  for (auto const& t : this->teams())
    if (::is_real(t))
      known_teams_no_ += 1;

  // update the ai teams information in the ui
  if (!game().isvirtual()
      && ::preferences(Preferences::Type::show_ai_information_teams)
      && (player().type() == Player::Type::human)) {
    for (auto const& player : game().players()) {
      if (team(player) != teams_bak[player.no()]) {
        ::ui->teaminfo_changed(player);
      }
    }
  } // if (show ai teams information)
}

bool TeamInformation::all_known() const
{
  return (known_teams_no() == game().playerno());
}

void TeamInformation::game_start()
{
  reset();

  // set the own team
  if (game().type() == GameType::normal) {
    teams_[player().no()]
      = ( (player().hand().count(Card::club_queen) > 0)
         ? GuessedTeam::re
         : GuessedTeam::contra);
  }

  update_teams();
}

void TeamInformation::trick_full(Trick const& trick)
{
}

void TeamInformation::card_played(HandCard const card)
{
  // ToDo: all: analyse the card and check for help/bother another player
  using namespace TeamInformationHeuristic;

  // No information from unknown cards
  if (card.is_unknown())
    return ;

  if (game().type() != GameType::normal)
    return ;

#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai team information"].start_stop_proxy();
#endif

  // take the cards information to check
  // whether the player can still have a club queen
  if (   (game().type() == GameType::normal)
      && !::is_real(team(card.player()))
      && (card != Card::club_queen)
      && (player().cards_information().of_player(card.player()).can_have(Card::club_queen) == 0) ) {
    team_values_[card.player().no()] = -100;
  }

  team_values_[card.player().no()]
    += TeamInformationHeuristic::card_played(card, game().tricks().current(),
                                             player());

  update_teams();
  recalc_team_values();
}

void TeamInformation::announcement_made(Announcement const announcement, Player const& player)
{
  update_teams();
}

void TeamInformation::marriage(Player const& bridegroom, Player const& bride)
{
  for (unsigned p = 0; p < game().playerno(); ++p)
    teams_[p] = (   p == bridegroom.no()
                 || p == bride.no()
                 ? GuessedTeam::re
                 : GuessedTeam::contra );

  known_teams_no_ = game().playerno();
}

void TeamInformation::reset()
{
  team_values_ = vector<int>(4, 0); // ToDo: game().playerno(),
  teams_ = vector<GuessedTeam>(4, GuessedTeam::unknown); // ToDo: game().playerno(),
  known_teams_no_ = 0;
}

void TeamInformation::update_teams()
{
  if (   all_known()
      && !game().isvirtual())
    // all teams are known
    return ;

#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ai team information"].start_stop_proxy();
#endif


  // the old value of the teams
  auto const teams_bak = teams();

  // in an undetermined marriage set the contra players to 'maybe contra'
  if (game().marriage().is_undetermined()) {
    for (auto const& player : game().players()) {
      if (game().players().is_soloplayer(player))
        teams_[player.no()] = GuessedTeam::re;
      else if (player == this->player())
        teams_[player.no()] = GuessedTeam::maybe_contra;
      else
        teams_[player.no()] = GuessedTeam::maybe_re;
    }
    return ;
  } // if (undetermined marriage)

  // Update with the teaminfo of 'game'
  // and add information from 'CardsInformation'.
  // Also count the 're's and 'contra's
  unsigned re_no = 0; // number of re
  unsigned contra_no = 0; // number of re
  for (auto const& player : game().players()) {
    auto& team = teams_[player.no()];

    // information from the game
    team = to_guessed(game().teaminfo().get(player));

    // the player knows his own team
    if (   !::is_real(team)
        && player == this->player())
      team = to_guessed(this->player().team());

    // use also the cards information
    if (!::is_real(team)) {
      try {
        if (dynamic_cast<Ai const&>(this->player()).value(Aiconfig::Type::hands_known)) {
          // If the hands are known, the checks are made against trump, not against the club queen. Else, the team would be known from the start on.
          if (this->player().cards_information().of_player(player).played(Card::club_queen))
            team = GuessedTeam::re;
          else if (   this->player().cards_information().of_player(player).does_not_have(Card::trump)
                   && !this->player().cards_information().of_player(player).played(Card::club_queen))
            // Bug: as soon as the player has played his last trump, his team is known. But without the hands known, it would be the first time, he has not serverd trump.
            team = GuessedTeam::contra;
        } else { // if !(player().value(Aiconfig::Type::hands_known))
          if (   this->player().cards_information().of_player(player).must_have(Card::club_queen)
              || this->player().cards_information().of_player(player).played(Card::club_queen))
            team = GuessedTeam::re;
          else if (   this->player().cards_information().of_player(player).cannot_have(Card::club_queen)
                   && !this->player().cards_information().of_player(player).played(Card::club_queen))
            team = GuessedTeam::contra;
        } // if !(player().value(Aiconfig::Type::hands_known))
      } catch (std::bad_cast const&) {
      }
    } // if (!::is_real(team(**player)))

    switch (team) {
    case GuessedTeam::re:
      re_no += 1;
      break;
    case GuessedTeam::contra:
      contra_no += 1;
      break;
    default:
      break;
    } // switch (team(**player)
  } // for (player : game().players())

  // check whether all teams are known
  known_teams_no_ = re_no + contra_no;

  if (known_teams_no() < game().playerno()) {
    // all not re players must be contra
    if (re_no == game().rule(Rule::Type::number_of_players_per_team)) {
      for (unsigned p = 0; p < game().playerno(); ++p)
        if (team(p) != GuessedTeam::re)
          teams_[p] = GuessedTeam::contra;
      known_teams_no_ = game().playerno();
    } // if (re_no == game().rule(Rule::Type::number_of_players_per_team))
  } // all not re players must be contra

  if (known_teams_no() < game().playerno()) {
    // there must be at least one re player
    if (contra_no == game().playerno() - 1) {
      for (unsigned p = 0; p < game().playerno(); ++p)
        if (team(p) != GuessedTeam::contra)
          teams_[p] = GuessedTeam::re;
      known_teams_no_ = game().playerno();
    } // if (contra_no == game().playerno() - 1)
  } // there must be at least one re player

  if (known_teams_no() < game().playerno()) {
    // there must be another re player but me
    if (   player_
        && contra_no == game().playerno() - 2
        && team(player()) == Team::re
        && (player().hand().count_all(Card::club_queen)
            < game().rule(Rule::Type::number_of_same_cards))
       ) {
      for (unsigned p = 0; p < game().playerno(); ++p)
        if (team(p) != GuessedTeam::contra)
          teams_[p] = GuessedTeam::re;
      known_teams_no_ = game().playerno();
    } // if (last unknown player must be re)
  } // there must be another re player but me

  // set the team of the players to maybe according to the team value
  if (known_teams_no() < game().playerno()) {
    switch (known_teams_no()) {
    case 0:
    case 1: { // (almost) no information
      // Idea:
      // |  re  | unknown | contra |
      // with enough distance between the two extreme values

      int min_value = INT_MAX;
      int max_value = INT_MIN;
      for (unsigned p = 0; p < game().playerno(); ++p) {
        if (!::is_real(team(p))) {
          if (team_value(p) < min_value)
            min_value = team_value(p);
          if (team_value(p) > max_value)
            max_value = team_value(p);
        }
      } // for (p)

      if (max_value - min_value < 15) // *Value*
        break;

      // count how many possible contra and re there are
      for (unsigned p = 0; p < game().playerno(); ++p) {
        if (!::is_real(team(p))) {
          if      (team_value(p) >= max_value - (max_value - min_value) / 3)
            re_no += 1;
          else if (team_value(p) <= min_value + (max_value - min_value) / 3)
            contra_no += 1;
        }
      } // for (p)

      if (   contra_no <= game().rule(Rule::Type::number_of_players_per_team)
          && re_no     <= game().rule(Rule::Type::number_of_players_per_team) ) {
        for (unsigned p = 0; p < game().playerno(); ++p) {
          if (!::is_real(team(p))) {
            if      (team_value(p) >= max_value - (max_value - min_value) * 1 / 6)
              teams_[p] = GuessedTeam::surely_re;
            else if (team_value(p) >= max_value - (max_value - min_value) * 1 / 3)
              teams_[p] = GuessedTeam::maybe_re;
            else if (team_value(p) <= min_value + (max_value - min_value) * 1 / 3)
              teams_[p] = GuessedTeam::maybe_contra;
            else if (team_value(p) <= min_value + (max_value - min_value) * 1 / 6)
              teams_[p] = GuessedTeam::surely_contra;
          }
        } // for (p)
      } // if (contra_no, re_no <= 2)

      break;
    } // case 0, 1

    case 2: {
      switch (contra_no) {
      case 0: // contra_no
        DEBUG_ASSERTION(false,
                        "TeamInformation::update_teams()\n"
                        "  contra no = 0");
        break;
      case 1: { // contra_no
        // one re and one contra player are known
        int min_value = INT_MAX;
        int max_value = INT_MIN;
        for (unsigned p = 0; p < game().playerno(); ++p) {
          if (!::is_real(team(p))) {
            if (team_value(p) < min_value)
              min_value = team_value(p);
            if (team_value(p) > max_value)
              max_value = team_value(p);
          }
        } // for (p)

        // enough difference to set the teams
        try {
          if (max_value - min_value < (dynamic_cast<Ai const&>(player()).value(Aiconfig::Type::trusting)
                                       ? 10
                                       : 20) ) // *Value*
            break;
        } catch (std::bad_cast const&) {
          if (max_value - min_value < 20)
            break;
        }

        for (unsigned p = 0; p < game().playerno(); ++p) {
          if (is_real(team(p)))
            continue;
          if (max_value - min_value >= 40) {
            if (team_value(p) == max_value)
              teams_[p] = GuessedTeam::surely_re;
            else // == min_value
              teams_[p] = GuessedTeam::surely_contra;
          } else {
            if (team_value(p) == max_value)
              teams_[p] = GuessedTeam::maybe_re;
            else // == min_value
              teams_[p] = GuessedTeam::maybe_contra;
          }
        }
        break;
      } // one re and one contra player are known
      case 2: // contra_no
        // both other players are re
        // but in the case that someone plays a silent marriage
        for (unsigned p = 0; p < game().playerno(); ++p)
          if (!::is_real(team(p))) {
            // I think here we can be very strict
            if      (team_value(p) >=   5) // *Value*
              teams_[p] = GuessedTeam::surely_re;
            else if (team_value(p) >=  -5) // *Value*
              teams_[p] = GuessedTeam::maybe_re;
            else if (team_value(p) <= -35) // *Value*
              teams_[p] = GuessedTeam::surely_contra;
            else if (team_value(p) <= -25) // *Value*
              teams_[p] = GuessedTeam::maybe_contra;
          } // switch(contra_no)

        break;
      default:
        break;
      } // switch (contra_no)

      break;
    } // case 2

    case 3: {
      // this case can only be when there are two contra and one re,
      // so the question is whether the re player plays a silent marriage.

      Player const* re_player = nullptr;
      for (unsigned p = 0; p < game().playerno(); ++p) {
        if (team(p) == Team::re) {
          re_player = &game().player(p);
          break;
        }
      }
      DEBUG_ASSERTION(re_player != nullptr,
                      "TeamInformation::update_team()\n"
                      "  three known players but no re player\n");
      for (unsigned p = 0; p < game().playerno(); ++p)
        if (!::is_real(team(p))) {
          // I think here we can be very strict
          if (   team_value(p) >= -20 // *Value*
              || game().points_of_player(*re_player) < game().played_points() / 2)
            teams_[p] = GuessedTeam::maybe_re;
          else
            teams_[p] = GuessedTeam::surely_contra;
          break;
        }
      break;
    } // case 3

    case 4:
      DEBUG_ASSERTION(false,
                      "TeamInformation::update_teams()\n"
                      "  should have left the method earlier because all teams "
                      "are known:\n"
                      "  known teams no = " << known_teams_no()
                      << " == " << game().playerno()
                      << " = player number in game");
      break;

    default:
      DEBUG_ASSERTION(false,
                      "TeamInformation::update_teams()\n"
                      "  known teams number is '" << known_teams_no()
                      << "' > 4 that is the maximum (players in game = "
                      << game().playerno());
      break;
    } // switch (known_teams_no())
  } // if (known_teams_no() < game().playerno())

  { // check for two re players

    unsigned re_no = 0;
    unsigned contra_no = 0;
    // count the players of the team
    for (auto const& player : game().players()) {
      if (team(player) & Team::re)
        re_no += 1;
      else if (team(player) & Team::contra)
        contra_no += 1;
    } // for (player : game().players)

    if (   re_no + contra_no < game().playerno()
        && re_no == game().rule(Rule::Type::number_of_players_per_team)) {
      // all unknown players must be contra
      for (auto const& player : game().players()) {
        if (team(player) == Team::unknown)
          teams_[player.no()] = GuessedTeam::maybe_contra;
      }
    } // if (all re players known
  } // check for two re players
  // update the ai teams information in the ui
  if (!game().isvirtual()
      && ::preferences(Preferences::Type::show_ai_information_teams)
      && player().type() == Player::Type::human) {
    for (auto const& player : game().players()) {
      if (team(player) != teams_bak[player.no()]) {
        ::ui->teaminfo_changed(player);
      }
    }
  } // if (show ai teams information)

} // void TeamInformation::update_teams()

/** @todo      work in progress
 **/
// NOLINTNEXTLINE(readability-make-member-function-const)
void TeamInformation::recalc_team_values()
{
  if (all_known())
    return ;

  if (in_recalcing_)
    return ;

  return ; // NOLINT ToDo

#ifdef POSTPONED
  auto const lv = set_local_value(in_recalcing_, true);

#ifdef CHECK_RUNTIME
  auto const ssp = (  (this == &player().team_information())
                    ? ::runtime["ai recalc team weightings"].start_stop_proxy()
                    : nullptr);
#endif
  // overview
  // * Rebuild a new team information for the start of the game
  // * Replay the game and update the weighting (using the information of further played cards)

  // create a virtual game

  // virtual players
  vector<Player*> player_virt;
  {
    // Create new players.
    // The hand is set during the recursion.
    for (auto const& player : game().players()) {
      player_virt.push_back(player().clone());
      player_virt.back()->set_name(player.name());
      player_virt.back()->set_team(player().team_information().guessed_team(player_virt.size() - 1));
    } // for (player)

  } // create a virtual game
  Game virt_game(game(), player_virt);
  virt_game.reset_to_first_trick();
  // set the cards information
  auto const self = player_virt[player().no()];
  self->cards_information().reset();
  for (unsigned p = 0; p < virt_game.playerno(); ++p) {
    self->cards_information().of_player(p).add_must_have(player().cards_information().of_player(p).played_cards());
  }

  // build a new cards information for the start of the game
  TeamInformation team_information(*player_virt[game().no(player())]);

  // replay the game (with announcements!)
  virt_game.play(game().gameplay());

  team_values_ = self->team_information().team_values_;
  for (auto& p : player_virt)
    delete p;
#endif
} // void TeamInformation::recalc_team_values()

bool operator==(TeamInformation const& lhs, TeamInformation const& rhs)
{
  return (   lhs.team_values_ == rhs.team_values_
          && lhs.teams_ == rhs.teams_);
}

bool operator!=(TeamInformation const& lhs, TeamInformation const& rhs)
{
  return !(lhs == rhs);
}
