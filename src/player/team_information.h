/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "guessed_team.h"
#include "../basetypes/team.h"
#include "../basetypes/announcement.h"
#include "../card/card.h"

class HandCard;
class Player;
class Game;
class Trick;

/** Contains and analyses the information, which team a player has
 **/
class TeamInformation {
  friend auto operator==(TeamInformation const& lhs, TeamInformation const& rhs) -> bool;
public:
  TeamInformation() = delete;
  TeamInformation(Player const& player);
  TeamInformation(TeamInformation const& team_information);
  auto operator=(TeamInformation const& team_information) -> TeamInformation&;
  ~TeamInformation();

  void write(ostream& ostr) const;

  auto player() const -> Player const&;

  auto teams()          const -> vector<GuessedTeam> const&;
  auto known_teams_no() const -> unsigned;

  void set_player(Player const& player);

  auto game() const -> Game const&;

  auto team_value(Player const& player)   const -> int;
  auto team_value(unsigned playerno)      const -> int;
  auto team(Player const& player)         const -> GuessedTeam;
  auto team(unsigned playerno)            const -> GuessedTeam;
  auto partner(Player const& player)      const -> Player const*;
  auto partner()                          const -> Player const*;
  auto guessed_partner()                  const -> Player const*;
  auto maybe_partner()                    const -> Player const*;

  void set_teams(vector<Team> const& teams);

  auto all_known() const -> bool;

  // the information from the game

  void game_start();
  void card_played(HandCard card);
  void announcement_made(Announcement announcement, Player const& player);
  void trick_full(Trick const& trick);
  void marriage(Player const& bridegroom, Player const& bride);

  void reset();

  void update_teams();
  void recalc_team_values();

private:
  Player const* player_ = nullptr;

  vector<int> team_values_; // positive for re, negative for contra
  vector<GuessedTeam> teams_;
  unsigned known_teams_no_ = 0;

  bool in_recalcing_ = false;
};

auto operator<<(ostream& ostr, TeamInformation const& team_information) -> ostream&;

auto operator==(TeamInformation const& lhs, TeamInformation const& rhs) -> bool;
auto operator!=(TeamInformation const& lhs, TeamInformation const& rhs) -> bool;
