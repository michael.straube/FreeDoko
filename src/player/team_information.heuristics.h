/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once


class Player;
class HandCard;
class Trick;

/**
 ** heuristics for the team information
 ** CardsInformations cannot be used, since the teaminfo is updated before the cards information and hence CardsInformation::self_check() will find errors (one card lacks to be played)
 **
 ** @todo       make use of CardsInformation possible
 **/
namespace TeamInformationHeuristic {
  int card_played(HandCard played_card,
                  Trick const& trick,
                  Player const& self);

} // namespace TeamInformationHeuristic 
