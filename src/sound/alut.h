/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef USE_SOUND_ALUT

#include "sound.h"

#include <AL/alut.h>

/**
 ** @brief	the sound class using the program 'aplay'
 **
 ** @author	Diether Knof
 **/
class Sound::Alut : public Sound {
  private:
    // number of instances
    static unsigned instances_no;

  public:
    Alut();
    ~Alut() override;

    Alut(Alut const&) = delete;
    Alut& operator=(Alut const&) = delete;

    void play(Position position,
              string const& soundgroup,
              Type const type) override;
    bool playing() const override;

  private:
    void check_current_sound();
    void play_next_sound();

  private:
    AutoDisconnector disconnector_;
    ALuint current_source_ = 0;
    queue<Path> file_queue_;
}; // class Alut : public Sound

#endif // #ifdef USE_SOUND_ALUT
