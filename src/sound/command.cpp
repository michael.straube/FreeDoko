/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_SOUND_COMMAND

#include "command.h"
#include "../misc/preferences.h"

#ifdef OSX
constexpr auto play_sound_command = "afplay";
#else
constexpr auto play_sound_command = "aplay";
#endif
//constexpr string play_sound_command = "ogg123";

/** Constructor
 **/
Sound::Command::Command() = default;

/** Destructor
 **/
Sound::Command::~Command() = default;

/** play the sound (global group)
 **
 ** @param    position   position of the sound
 ** @param    voice      voice (directory to take the sound from)
 ** @param    type       sound to play
 **/
void
Sound::Command::play(Position const position,
                     string const& voice,
                     Type const type)
{
  auto const path = Sound::path(position, voice, type);
  if (path.empty())
    return ;

  system((play_sound_command // NOLINT(cert-env33-c)
          + (" " + Sound::path(position, voice, type).string())
          + " >/dev/null &").c_str());
} // void Sound::Command::play(Position position, string voice, Type text)

/** @return   whether the sound is still playing
 **/
bool
Sound::Command::playing() const
{
  return false;
} // bool Sound::Command::playing() const

#endif // #ifdef USE_SOUND_COMMAND
