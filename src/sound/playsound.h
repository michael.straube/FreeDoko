/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef USE_SOUND_PLAYSOUND

#include "sound.h"

/**
 ** the sound class using the windows command 'PlaySound'
 **/
class Sound::WinPlaySound : public Sound {
  public:
    WinPlaySound();
    ~WinPlaySound() override;

    WinPlaySound(WinPlaySound const&) = delete;
    WinPlaySound& operator=(WinPlaySound const&) = delete;


    void play(Position position,
              string const& voice,
              Type type) override;
    bool playing() const override;

  private:
    void check_current_sound();
    void play_next_sound();

  private:
    AutoDisconnector disconnector_;
    queue<Path> file_queue_;
}; // class WinPlaySound : public Sound

#endif // #ifdef USE_SOUND_PLAYSOUND
