/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_SOUND

#pragma once

#include "../basetypes/game_type.h"
#include "../basetypes/marriage_selector.h"
#include "../basetypes/announcement.h"
#include "../basetypes/position.h"
class Player;
class Game;
class Trick;
class HandCard;

/**
 ** the base sound class
 **/
class Sound {
  public:
#ifdef USE_SOUND_ALUT
    class Alut;
#endif
#ifdef USE_SOUND_COMMAND
    class Command;
#endif
#ifdef USE_SOUND_PLAYSOUND
    class WinPlaySound;
#endif

  public:
    enum class Type {
      first,
      gametype_normal = first,
      gametype_thrown_nines,
      gametype_thrown_kings,
      gametype_thrown_nines_and_kings,
      gametype_thrown_richness,
      gametype_redistribute,
      gametype_fox_highest_trump,
      gametype_poverty,
      gametype_marriage,
      gametype_marriage_solo,
      gametype_marriage_silent,
      gametype_solo_meatless,
      gametype_solo_jack,
      gametype_solo_queen,
      gametype_solo_king,
      gametype_solo_queen_jack,
      gametype_solo_king_jack,
      gametype_solo_king_queen,
      gametype_solo_koehler,
      gametype_solo_club,
      gametype_solo_heart,
      gametype_solo_spade,
      gametype_solo_diamond,
      marriage_selector_team_set,
      marriage_selector_first_foreign,
      marriage_selector_first_trump,
      marriage_selector_first_color,
      marriage_selector_first_club,
      marriage_selector_first_spade,
      marriage_selector_first_heart,
      marriage_selector_silent,
      cards_distributed,
      trick_move_in_pile,
      card_played,
      poverty,
      poverty_denied,
      poverty_accepted,
      marriage,
      announcement_noannouncement,
      announcement_re,
      announcement_contra,
      announcement_no120,
      announcement_no90,
      announcement_no60,
      announcement_no30,
      announcement_no0,
      swines,
      hyperswines,
      no_winner,
      soloplayer_won,
      soloplayer_lost,
      re_won,
      contra_won,
      last = contra_won
    }; // enum Type

    // returns the name of the sound
    static string name(Sound::Type type);
    // returns the text of the sound
    static string text(Sound::Type type);
    // returns the file name for the sound
    static string filename(Sound::Type type);
    // returns the path of the soundfile
    static Path path(Position position,
                     string const& voice,
                     Type type);

    // returns the type for the gametype
    static Type type(GameType gametype);
    // returns the type for the gametype
    static Type type(MarriageSelector marriage_selector);
    // returns the type for the announcement
    static Type type(Announcement announcement);

  public:
    // create a sound class
    static unique_ptr<Sound> create();

  public:
    Sound();
    virtual ~Sound() = default;

    Sound(Sound const&) = delete;
    Sound& operator=(Sound const&) = delete;

    // play the sound of the voice
    virtual void play(Position position,
                      string const& voice,
                      Type type) = 0;

    // play the sound (global group)
    virtual void play(Type type);
    // play the sound (group of the player)
    virtual void play(Player const& player, Type type);

    // whether the sound is still playing
    virtual bool playing() const = 0;


    void game_open(Game& game);

    // card sounds

    // the cards are distributed
    void game_cards_distributed();
    // the trick is moved into a pile
    void trick_move_in_pile(Trick const& trick);
    // the card is played
    void card_played(HandCard card);


    // reservations

    // the game is started
    void game_start();
    // the game is finished
    void game_finish();


    // special games

    // the marriage partner has found a bride
    void marriage(Player const& bridegroom, Player const& bride);


    // poverty

    // 'player' shifts 'cardno' cards
    void poverty_shift(Player const& player, unsigned cardno);
    // the player 'player' has denied the poverty trumps
    void poverty_take_denied(Player const& player);
    // the player 'player' has accepted the poverty trumps
    // and has returned 'cardno' cards with 'trumpno' trumps
    void poverty_take_accepted(Player const& player,
                               unsigned cardno,
                               unsigned trumpno);


    // announcements

    // an announcement is made
    void announcement_made(Announcement announcement,
                           Player const& player);
    // the player has swines
    void swines_announced(Player const& player);
    // the player has hyperswines
    void hyperswines_announced(Player const& player);

  private:
    AutoDisconnector disconnector_;
}; // class Sound

#endif // #ifdef USE_SOUND
