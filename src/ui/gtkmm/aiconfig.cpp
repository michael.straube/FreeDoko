/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "aiconfig.h"
#include "ui.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../player/aiconfig.h"
#include "../../player/ai/heuristic.h"
#include "../../misc/preferences.h"
#include "../../utils/string.h"

#include "widgets/labelscalespinbutton.h"
#include "widgets/label_card_selector.h"
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/grid.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/combobox.h>
#include <gtkmm/notebook.h>
#include <gtkmm/frame.h>
#include <gtkmm/textview.h>
#include <gtkmm/main.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    player   the corresponding (Gtk-)Player
 **/
Players::Player::AiConfig::AiConfig(Player* const player) :
  Base(player),
  player(player)
{
  init();
} // Players::Player::AiConfig::AiConfig(Player* player)

/** destruktor
 **/
Players::Player::AiConfig::~AiConfig()
{
  for (auto& i : type_bool)
    delete static_cast<Aiconfig::Type::Bool*>(i->steal_data("type"));

  for (auto& i : type_int)
    delete static_cast<Aiconfig::Type::Int*>(i->steal_data("type"));

  for (auto& i : type_card)
    delete static_cast<Aiconfig::Type::Card*>(i->steal_data("type"));
} // Players::Player::AiConfig::~AiConfig()

/** creates all subelements
 **/
void
Players::Player::AiConfig::init()
{
  { // create the widgets
    { // buttons
      difficulty_label
        = Gtk::manage(new Gtk::Label(_("Label::difficulty")));
      difficulty_button
        = Gtk::manage(new Gtk::Button(_("Button::hardcoded")));
      difficulty_button->set_image_from_icon_name("edit-clear-all");
      difficulty_button->set_always_show_image();
      difficulty_selector
        = Gtk::manage(new Gtk::LabelTypeSelector< ::Aiconfig::Difficulty>());
      difficulty_selector->get_label()->set_label(_("AiConfig::difficulty") + ": ");
      for (auto const d : aiconfig_difficulty_list) {
        difficulty_selector->add(d);
      }
    } // buttons


    for (auto const t : Aiconfig::Type::bool_list) {
      type_bool.push_back(Gtk::manage(new Gtk::CheckButton(_(t))));
      type_bool.back()->set_data("type", new Aiconfig::Type::Bool(t));
      type_bool.back()->signal_toggled().connect(sigc::bind<int>(sigc::mem_fun(*this, &AiConfig::change), static_cast<int>(t)));
    }
    for (auto const t : Aiconfig::Type::int_list) {
      switch (t) {
      case Aiconfig::Type::last_tricks_without_heuristics:
      case Aiconfig::Type::first_trick_for_trump_points_optimization:
        type_int.push_back(Gtk::manage(new Gtk::LabelScaleSpinButton(_(t) + ":")));
        break;
      case Aiconfig::Type::future_limit:
      case Aiconfig::Type::limit_throw_fehl:
      case Aiconfig::Type::limitqueen:
      case Aiconfig::Type::limitdulle:
      case Aiconfig::Type::announcelimit:
      case Aiconfig::Type::announcelimitdec:
      case Aiconfig::Type::announceconfig:
      case Aiconfig::Type::announcelimitreply:
      case Aiconfig::Type::announceconfigreply:
        type_int.push_back(Gtk::manage(new Gtk::LabelSpinButton(_(t) + ":")));
        break;
      case Aiconfig::Type::max_sec_wait_for_gametree:
        type_int.push_back(Gtk::manage(new Gtk::LabelSpinButton(_(t) + ":", _("seconds"))));
        break;
      } // switch(t)
      type_int.back()->set_data("type", new Aiconfig::Type::Int(t));
      type_int.back()->signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &AiConfig::change), static_cast<int>(t)));
    }
    for (auto const t : Aiconfig::Type::card_list) {
      switch (t) {
      case Aiconfig::Type::limitthrowing:
      case Aiconfig::Type::limithigh:
      case Aiconfig::Type::trumplimit_normal:
      case Aiconfig::Type::lowest_trumplimit_normal:
        type_card.push_back(Gtk::manage(new Gtk::LabelCardSelector(_(t))));
        break;
      case Aiconfig::Type::trumplimit_solocolor:
      case Aiconfig::Type::lowest_trumplimit_solocolor:
      case Aiconfig::Type::trumplimit_solojack:
      case Aiconfig::Type::lowest_trumplimit_solojack:
      case Aiconfig::Type::trumplimit_soloqueen:
      case Aiconfig::Type::lowest_trumplimit_soloqueen:
      case Aiconfig::Type::trumplimit_soloking:
      case Aiconfig::Type::lowest_trumplimit_soloking:
      case Aiconfig::Type::trumplimit_solojackking:
      case Aiconfig::Type::lowest_trumplimit_solojackking:
      case Aiconfig::Type::trumplimit_solojackqueen:
      case Aiconfig::Type::lowest_trumplimit_solojackqueen:
      case Aiconfig::Type::trumplimit_soloqueenking:
      case Aiconfig::Type::lowest_trumplimit_soloqueenking:
      case Aiconfig::Type::trumplimit_solokoehler:
      case Aiconfig::Type::lowest_trumplimit_solokoehler:
      case Aiconfig::Type::trumplimit_meatless:
      case Aiconfig::Type::lowest_trumplimit_meatless:
        // the translation is added by a separate label
        type_card.push_back(Gtk::manage(new Gtk::LabelCardSelector()));
        break;
      default:
        DEBUG_ASSERTION(false,
                        "UI_GTKMM::AiConfig::init()\n"
                        "  Aiconfig::Type::Card '" << t << "' "
                        "not taken into account.");
        break;
      }
      type_card.back()->set_data("type", new Aiconfig::Type::Card(t));
      for (auto c : aiconfig().valid_cards(t))
        type_card.back()->add(c);
      type_card.back()->signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &AiConfig::change), static_cast<int>(t)) );
    }
    rating = Gtk::manage(new Gtk::ComboBoxText());
    for (auto const r : Rating::type_list) {
      rating->append(to_string(r), _(r));
    }
  } // create the widgets

  create_container();

  { // the signals
    difficulty_button->signal_clicked().connect(sigc::mem_fun(*this, &AiConfig::hardcoded));
    difficulty_selector->signal_value_changed().connect(sigc::mem_fun(*this, &AiConfig::difficulty_changed));
    rating->signal_changed().connect(sigc::mem_fun(*this, &AiConfig::rating_changed));
  } // the signals

  update();

  heuristics_signals_active = true;
} // void Players::Player::AiConfig::init()

/** @return   a container with the configuration (in notebooks)
 **/
void
Players::Player::AiConfig::create_container()
{
  container
    = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));

  { // the difficulty
    auto box = Gtk::manage(new Gtk::Box());

    box->add(*difficulty_label);
    box->add(*difficulty_selector);

    difficulty_container = box;
  } // the difficulty

  { // the settings
    notebook = Gtk::manage(new Gtk::Notebook());
    notebook->set_scrollable(true);

    { // the notebook pages
      { // general settings
        auto label = Gtk::manage(new Gtk::Label(_("AiConfig::Group::general")));

        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EM));
        vbox->set_halign(Gtk::ALIGN_CENTER);
        vbox->set_valign(Gtk::ALIGN_CENTER);

        vbox->add(*type_bool[Aiconfig::Type::teams_known   - Aiconfig::Type::bool_first]);
        vbox->add(*type_bool[Aiconfig::Type::hands_known   - Aiconfig::Type::bool_first]);
        vbox->add(*type_bool[Aiconfig::Type::trusting      - Aiconfig::Type::bool_first]);
        vbox->add(*type_bool[Aiconfig::Type::aggressive    - Aiconfig::Type::bool_first]);
        vbox->add(*type_bool[Aiconfig::Type::cautious      - Aiconfig::Type::bool_first]);
        vbox->add(*type_bool[Aiconfig::Type::chancy        - Aiconfig::Type::bool_first]);
        vbox->add(*type_int[Aiconfig::Type::future_limit   - Aiconfig::Type::int_first]);
        vbox->add(*type_int[Aiconfig::Type::max_sec_wait_for_gametree - Aiconfig::Type::int_first]);

        notebook->append_page(*vbox, *label);
      } // general settings
      { // announcement
        auto label = Gtk::manage(new Gtk::Label(_("AiConfig::Group::announcement")));

        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EM));
        vbox->set_halign(Gtk::ALIGN_CENTER);
        vbox->set_valign(Gtk::ALIGN_CENTER);

        vbox->add(*(type_int[Aiconfig::Type::announcelimit       - Aiconfig::Type::int_first]));
        vbox->add(*(type_int[Aiconfig::Type::announcelimitdec    - Aiconfig::Type::int_first]));
        vbox->add(*(type_int[Aiconfig::Type::announceconfig      - Aiconfig::Type::int_first]));
        vbox->add(*(type_int[Aiconfig::Type::announcelimitreply  - Aiconfig::Type::int_first]));
        vbox->add(*(type_int[Aiconfig::Type::announceconfigreply - Aiconfig::Type::int_first]));

        notebook->append_page(*vbox, *label);
      } // announcement
      { // trick decision
        auto label = Gtk::manage(new Gtk::Label(_("AiConfig::Group::trick decision")));

        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EM));
        vbox->set_halign(Gtk::ALIGN_CENTER);
        vbox->set_valign(Gtk::ALIGN_CENTER);

        vbox->add(*type_int[Aiconfig::Type::last_tricks_without_heuristics - Aiconfig::Type::int_first]);
        vbox->add(*type_int[Aiconfig::Type::first_trick_for_trump_points_optimization - Aiconfig::Type::int_first]);
        vbox->add(*type_int[Aiconfig::Type::limit_throw_fehl - Aiconfig::Type::int_first]);
        vbox->add(*type_int[Aiconfig::Type::limitqueen       - Aiconfig::Type::int_first]);
        vbox->add(*type_int[Aiconfig::Type::limitdulle       - Aiconfig::Type::int_first]);

        {
          auto label = Gtk::manage(new Gtk::Label(_("Label::AiConfig::rating") + ":"));
          auto hbox = Gtk::manage(new Gtk::Box());
          hbox->set_spacing(1 EX);
          hbox->add(*label);
          hbox->add(*rating);
          vbox->add(*hbox);
        }

        notebook->append_page(*vbox, *label);
      } // trick decision
      { // card limits
        auto label = Gtk::manage(new Gtk::Label(_("AiConfig::Group::card limits")));

        auto card_limits_notebook = Gtk::manage(new Gtk::Notebook());
        notebooks.push_back(card_limits_notebook);

        { // normal game
          auto label = Gtk::manage(new Gtk::Label(_("AiConfig::CardLimit::Group::normal game")));

          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox->set_halign(Gtk::ALIGN_CENTER);
          vbox->set_valign(Gtk::ALIGN_CENTER);

          vbox->add(*type_card[Aiconfig::Type::limitthrowing - Aiconfig::Type::card_first]);
          vbox->add(*type_card[Aiconfig::Type::trumplimit_normal - Aiconfig::Type::card_first]);
          vbox->add(*type_card[Aiconfig::Type::lowest_trumplimit_normal - Aiconfig::Type::card_first]);
          vbox->add(*type_card[Aiconfig::Type::limithigh - Aiconfig::Type::card_first]);

          card_limits_notebook->append_page(*vbox, *label);
        } // normal game
        { // other solo
          auto label = Gtk::manage(new Gtk::Label(_("AiConfig::CardLimit::Group::solo")));

          auto grid = Gtk::manage(new Gtk::Grid());
          grid->set_row_spacing(1 EX / 2);
          grid->set_column_spacing(1 EM / 2);
          grid->set_border_width(1 EX);
          grid->set_halign(Gtk::ALIGN_CENTER);
          grid->set_valign(Gtk::ALIGN_CENTER);
          { // the grid
            Gtk::Label* label = nullptr;
            int row = 0;

            // title
            label = Gtk::manage(new Gtk::Label(_("AiConfig::CardLimit::Group::trump limit")));
            grid->attach(*label, 1, row, 1, 1);
            label = Gtk::manage(new Gtk::Label(_("AiConfig::CardLimit::Group::lowest trump limit")));
            grid->attach(*label, 2, row, 1, 1);
            row += 1;
            grid->set_row_spacing(3 EX / 2);
            // color solo
            label = Gtk::manage(new Gtk::Label(_("GameType::Group::Solo::color") + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_solocolor - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_solocolor - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // jack solo
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_jack) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_solojack - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_solojack - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // queen solo
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_queen) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_soloqueen - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_soloqueen - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // king solo
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_king) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_soloking - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_soloking - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // queen-jack solo
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_queen_jack) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_solojackqueen - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_solojackqueen - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // king-jack solo
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_king_jack) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_solojackking - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_solojackking - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // king-queen solo
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_king_queen) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_soloqueenking - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_soloqueenking - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // koehler
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_koehler) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_solokoehler - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_solokoehler - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;
            // meatless
            label = Gtk::manage(new Gtk::Label(_(GameType::solo_meatless) + ": "));
            grid->attach(*label, 0, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::trumplimit_meatless - ::Aiconfig::Type::card_first],
                         1, row, 1, 1);
            grid->attach(*type_card[::Aiconfig::Type::lowest_trumplimit_meatless - ::Aiconfig::Type::card_first],
                         2, row, 1, 1);
            row += 1;

            (void) row;
          } // the table
          card_limits_notebook->append_page(*grid, *label);

        } // other solo

        notebook->append_page(*card_limits_notebook, *label);
      } // card limits
      { // heuristics
        auto label = Gtk::manage(new Gtk::Label(_("AiConfig::Group::heuristics")));

        auto heuristics_notebook = Gtk::manage(new Gtk::Notebook());
        notebooks.push_back(heuristics_notebook);
        heuristics_model = new HeuristicsModel();


        { // normal game
#if 0
          Gtk::Label* label
            = Gtk::manage(new Gtk::Label(_((HeuristicsMap::normal))));


          heuristics_notebook->append_page(*create_heuristic_treeview(HeuristicsMap::DEFAULT, HeuristicsMap::re),
                                           *label);
#else
          auto normal_notebook = Gtk::manage(new Gtk::Notebook());
          notebooks.push_back(normal_notebook);
          add_heuristic_treeviews_re_contra(heuristics_notebook,
                                                  HeuristicsMap::GameTypeGroup::normal);
#endif
        } // normal game
        { // marriage
          auto label = Gtk::manage(new Gtk::Label(_("HeuristicsMap::GameTypeGroup::marriage")));

          auto marriage_notebook = Gtk::manage(new Gtk::Notebook());
          notebooks.push_back(marriage_notebook);

          add_heuristic_treeviews_re_contra(marriage_notebook,
                                                  HeuristicsMap::GameTypeGroup::marriage_undetermined);
          add_heuristic_treeviews_re_contra(marriage_notebook,
                                                  HeuristicsMap::GameTypeGroup::marriage_silent);

          heuristics_notebook->append_page(*marriage_notebook,
                                           *label);
        } // marriage
        { // poverty
          add_heuristic_treeviews_special_re_contra(heuristics_notebook,
                                                          HeuristicsMap::GameTypeGroup::poverty);
        } // poverty
        { // soli
          auto label = Gtk::manage(new Gtk::Label(_("HeuristicsMap::GameTypeGroup::soli")));

          auto soli_notebook = Gtk::manage(new Gtk::Notebook());
          notebooks.push_back(soli_notebook);

          add_heuristic_treeviews_re_contra(soli_notebook,
                                                  HeuristicsMap::GameTypeGroup::soli_color);
          add_heuristic_treeviews_re_contra(soli_notebook,
                                                  HeuristicsMap::GameTypeGroup::soli_single_picture);
          add_heuristic_treeviews_re_contra(soli_notebook,
                                                  HeuristicsMap::GameTypeGroup::soli_double_picture);
          add_heuristic_treeviews_re_contra(soli_notebook,
                                                  HeuristicsMap::GameTypeGroup::solo_koehler);
          add_heuristic_treeviews_re_contra(soli_notebook,
                                                  HeuristicsMap::GameTypeGroup::solo_meatless);

          heuristics_notebook->append_page(*soli_notebook,
                                           *label);
        } // soli

        notebook->append_page(*heuristics_notebook, *label);
      } // heuristics
    } // the notebook pages

    container->pack_start(*notebook, true, true);
  } // the settings

#ifndef RELEASE
#ifdef DKNOF
  { // test, whether all configuration buttons are packed in a container
    for (auto const type : Aiconfig::Type::bool_list) {
      if (!type_bool[type - Aiconfig::Type::bool_first]->get_parent()) {
        cerr << "UI_GTKMM::Players::Player::AiConfig::create_container()\n"
          << "  config '" << type << "' not packed\n";
      }
    }
    for (auto const type : Aiconfig::Type::int_list) {
      if (!type_int[type - Aiconfig::Type::int_first]->get_parent()) {
        cerr << "UI_GTKMM::Players::Player::AiConfig::create_container()\n"
          << "  config '" << type << "' not packed\n";
      }
    }
    for (auto const type : Aiconfig::Type::card_list) {
      if (!type_card[type - Aiconfig::Type::card_first]->get_parent()) {
        cerr << "UI_GTKMM::Players::Player::AiConfig::create_container()\n"
          << "  config '" << type << "' not packed\n";
      }
    }
  } // test, whether all configuration buttons are packed in a container
#endif
#endif
} // void Players::Player::AiConfig::create_container()

/** adds a treeview for the heuristic group to the notbook
 **
 ** @param    notebook           notebook to add the treeview to
 ** @param    gametype_group     gametype group
 ** @param    playertype_group   playertype group
 ** @param    label_text         label translation
 **/
void
Players::Player::AiConfig::add_heuristic_treeview(Gtk::Notebook* const notebook,
                                                  HeuristicsMap::GameTypeGroup const gametype_group,
                                                  HeuristicsMap::PlayerTypeGroup const playertype_group,
                                                  string const& label_text)
{
  auto label = Gtk::manage(new Gtk::Label(label_text));

  notebook->append_page(*create_heuristic_treeview(gametype_group,
                                                         playertype_group),
                        *label);
}

/** adds a treeview for the heuristic group to the notbook
 ** a subpage for the re player and the contra player
 **
 ** @param    notebook           notebook to add the treeview to
 ** @param    gametype_group     gametype group
 **/
void
Players::Player::AiConfig::add_heuristic_treeviews_re_contra(Gtk::Notebook* const notebook,
                                                             HeuristicsMap::GameTypeGroup const gametype_group)
{
#ifdef WORKAROUND
  auto text = _("HeuristicsMap::GameTypeGroup::" + ::to_string(gametype_group));
  auto label = Gtk::manage(new Gtk::Label(text));
#else
  auto label = Gtk::manage(new Gtk::Label(_(gametype_group)));
#endif

  auto subnotebook = Gtk::manage(new Gtk::Notebook());
  notebooks.push_back(subnotebook);

  add_heuristic_treeview(subnotebook,
                               gametype_group,
                               HeuristicsMap::PlayerTypeGroup::re,
#ifdef WORKAROUND
                               _("HeuristicsMap::PlayerTypeGroup::" + ::to_string(HeuristicsMap::PlayerTypeGroup::re))
#else
                               _(HeuristicsMap::PlayerTypeGroup::re)
#endif
                              );
  add_heuristic_treeview(subnotebook,
                               gametype_group,
                               HeuristicsMap::PlayerTypeGroup::contra,
#ifdef WORKAROUND
                               _("HeuristicsMap::PlayerTypeGroup::" + ::to_string(HeuristicsMap::PlayerTypeGroup::contra))
#else
                               _(HeuristicsMap::PlayerTypeGroup::contra)
#endif
                              );

  notebook->append_page(*subnotebook, *label);
} // void Players::Player::AiConfig::add_heuristic_treeviews_re_contra(Gtk::Notebook* notebook, HeuristicsMap::GameTypeGroup gametype_group)

/** adds a treeview for the heuristic group to the notbook
 ** a subpage for the special player, the re player and the contra player
 **
 ** @param    notebook           notebook to add the treeview to
 ** @param    gametype_group     gametype group
 **/
void
Players::Player::AiConfig::add_heuristic_treeviews_special_re_contra(Gtk::Notebook* const notebook,
                                                                     HeuristicsMap::GameTypeGroup const gametype_group)
{
#ifdef WORKAROUND
  auto text = _("HeuristicsMap::GameTypeGroup::" + ::to_string(gametype_group));
  auto label = Gtk::manage(new Gtk::Label(text));
#else
  auto label = Gtk::manage(new Gtk::Label(_(gametype_group)));
#endif

  auto subnotebook = Gtk::manage(new Gtk::Notebook());
  notebooks.push_back(subnotebook);

  add_heuristic_treeview(subnotebook,
                               gametype_group,
                               HeuristicsMap::PlayerTypeGroup::special,
#ifdef WORKAROUND
                               _("HeuristicsMap::PlayerTypeGroup::" + ::to_string(HeuristicsMap::PlayerTypeGroup::special))
#else
                               _(HeuristicsMap::PlayerTypeGroup::special)
#endif
                              );
  add_heuristic_treeview(subnotebook,
                               gametype_group,
                               HeuristicsMap::PlayerTypeGroup::re,
#ifdef WORKAROUND
                               _("HeuristicsMap::PlayerTypeGroup::" + ::to_string(HeuristicsMap::PlayerTypeGroup::re))
#else
                               _(HeuristicsMap::PlayerTypeGroup::re)
#endif
                              );
  add_heuristic_treeview(subnotebook,
                               gametype_group,
                               HeuristicsMap::PlayerTypeGroup::contra,
#ifdef WORKAROUND
                               _("HeuristicsMap::PlayerTypeGroup::" + ::to_string(HeuristicsMap::PlayerTypeGroup::contra))
#else
                               _(HeuristicsMap::PlayerTypeGroup::contra)
#endif
                              );

  notebook->append_page(*subnotebook, *label);
}

/** creates a treeview for the given gametype/playertype combination
 **
 ** @param    gametype_group     gametype group
 ** @param    playertype_group   playertype group
 **
 ** @return   a scrolled window containing the treeview
 **/
Gtk::Widget*
Players::Player::AiConfig::create_heuristic_treeview(HeuristicsMap::GameTypeGroup const gametype_group,
                                                     HeuristicsMap::PlayerTypeGroup const playertype_group)
{
  auto box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  box->signal_realize().connect(sigc::bind<Gtk::Box*, HeuristicsMap::GameTypeGroup, HeuristicsMap::PlayerTypeGroup>(sigc::mem_fun(*this, &Players::Player::AiConfig::init_heuristic_treeview), box, gametype_group, playertype_group));

  return box;
}

/** creates a treeview for the given gametype/playertype combination
 **
 ** @param    gametype_group     gametype group
 ** @param    playertype_group   playertype group
 **
 ** @return   a scrolled window containing the treeview
 **/
void
Players::Player::AiConfig::init_heuristic_treeview(Gtk::Box* const box,
                                                   HeuristicsMap::GameTypeGroup const gametype_group,
                                                   HeuristicsMap::PlayerTypeGroup const playertype_group)
{
  HeuristicsMap::Key key(gametype_group, playertype_group);

  { // table
    auto box_treeview = Gtk::manage(new Gtk::Box());
    { // treeview
      auto& model = heuristics_model;
      auto& store = heuristics_list[key];
      auto& treeview = heuristics_treeview[key];

      auto const& states = aiconfig_const().heuristic_states(key);

      store = Gtk::ListStore::create(*model);
      treeview = Gtk::manage(new Gtk::TreeView(store));
      treeview->set_property("reorderable", true);
      treeview->set_property("headers_visible", false);

      treeview->append_column_editable("active", model->active);
      treeview->get_column_cell_renderer(0)->set_property("xalign", 0.5);
      treeview->append_column("heuristic", model->heuristic_name);
      treeview->get_column_cell_renderer(1)->set_property("xalign", 0);

      { // create the rows
        for (auto const s : states) {
          auto const heuristic = s.heuristic;
          if (!is_real(heuristic))
            continue;
          if (!Heuristic::is_valid(heuristic, gametype_group, playertype_group))
            continue;

          auto row = *store->append();

          row[model->active] = aiconfig().value(key, s.heuristic);
          row[model->heuristic] = heuristic;
          row[model->heuristic_name] = _(heuristic);
          row[model->gametype_group] = gametype_group;
          row[model->playertype_group] = playertype_group;
        }
      } // create the rows

      { // events
        store->signal_row_changed().connect(sigc::mem_fun(*this, &AiConfig::heuristics_row_changed_event));
        store->signal_row_deleted().connect(sigc::bind<Glib::RefPtr<Gtk::ListStore> >(sigc::mem_fun(*this, &AiConfig::heuristics_row_deleted_event), store));
        treeview->get_selection()->signal_changed().connect(sigc::bind<HeuristicsMap::Key>(sigc::mem_fun(*this, &AiConfig::update_heuristic_up_down_buttons), key));
        treeview->get_selection()->signal_changed().connect(sigc::bind<HeuristicsMap::Key>(sigc::mem_fun(*this, &AiConfig::update_heuristic_description), key));
      } // events

      treeview->set_valign(Gtk::ALIGN_FILL);
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
      scrolled_window->set_policy(Gtk::POLICY_NEVER,
                                  Gtk::POLICY_AUTOMATIC);
      scrolled_window->add(*treeview);

      scrolled_window->set_valign(Gtk::ALIGN_FILL);
      box_treeview->pack_start(*scrolled_window, Gtk::PACK_SHRINK);
    } // treeview
    { // right column
      auto box3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
      box3->set_border_width(1 EX);
      { // move up/down buttons
        auto button_box = Gtk::manage(new Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL));
        button_box->set_layout(Gtk::BUTTONBOX_SPREAD);
        button_box->set_spacing(1 EX);

        heuristic_up_button[key]
          = Gtk::manage(new Gtk::Button(_("Button::move up")));
        heuristic_up_button[key]->set_image_from_icon_name("go-up");
        heuristic_up_button[key]->set_always_show_image();
        heuristic_down_button[key]
          = Gtk::manage(new Gtk::Button(_("Button::move down")));
        heuristic_down_button[key]->set_image_from_icon_name("go-down");
        heuristic_down_button[key]->set_always_show_image();

        button_box->add(*heuristic_up_button[key]);
        button_box->add(*heuristic_down_button[key]);

        box3->pack_start(*button_box, Gtk::PACK_SHRINK);

        update_heuristic_up_down_buttons(key);
        heuristic_up_button[key]->signal_clicked().connect(sigc::bind<HeuristicsMap::Key>(sigc::mem_fun(*this, &AiConfig::heuristic_move_up_event), key));
        heuristic_down_button[key]->signal_clicked().connect(sigc::bind<HeuristicsMap::Key>(sigc::mem_fun(*this, &AiConfig::heuristic_move_down_event), key));
      } // move up/down buttons
      { // description
        auto description_view = Gtk::manage(new Gtk::TextView());
        heuristic_description[key] = description_view;

        description_view->set_editable(false);
        description_view->set_wrap_mode(Gtk::WRAP_WORD);

        description_view->get_buffer()->set_text("");

        auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
        scrolled_window->set_policy(Gtk::POLICY_NEVER,
                                    Gtk::POLICY_AUTOMATIC);
        scrolled_window->add(*description_view);
        box3->pack_end(*scrolled_window, Gtk::PACK_EXPAND_WIDGET);
      } // description
      box_treeview->pack_end(*box3, Gtk::PACK_EXPAND_WIDGET);
    } // right column
    box->pack_end(*box_treeview, Gtk::PACK_EXPAND_WIDGET);
  } // table
  box->show_all();
}

/** @return   the ai configuration
 **/
::Aiconfig&
Players::Player::AiConfig::aiconfig()
{
  return dynamic_cast<Aiconfig&>(player->player());
} // ::Aiconfig& Players::Player::AiConfig::aiconfig()

/** @return   the ai configuration (const version)
 **/
::Aiconfig const&
Players::Player::AiConfig::aiconfig_const() const
{
  return dynamic_cast<Aiconfig&>(player->player());
} // ::Aiconfig const& Players::Player::AiConfig::aiconfig_const() const

/** sets the aiconfig to the selected difficulty
 **/
void
Players::Player::AiConfig::difficulty_changed()
{
  auto const difficulty = difficulty_selector->type();
  if (difficulty == ::Aiconfig::Difficulty::custom)
    return ;

  if (difficulty == aiconfig().difficulty())
    return ;

  aiconfig().set_to_difficulty(difficulty);
  update();
}

/** the rating has changed
 **
 ** @param    trickno      the trick number of the changed type
 **/
void
Players::Player::AiConfig::rating_changed()
{
  aiconfig().enter_update();

  aiconfig().set_rating(Rating::type_from_string(rating->get_active_id()));

  aiconfig().leave_update();
}

/** a aiconfig has been changed by the user
 **
 ** @param    type   the type of the aiconfig
 **/
void
Players::Player::AiConfig::change(int const type)
{
  if ((type >= Aiconfig::Type::bool_first)
      && (type <= Aiconfig::Type::bool_last))
    aiconfig().set(Aiconfig::Type::Bool(type),
                         type_bool[type - Aiconfig::Type::bool_first]->get_active());
  else if ((type >= Aiconfig::Type::int_first)
           && (type <= Aiconfig::Type::int_last)) {
    if (type_int[type - Aiconfig::Type::int_first]->get_realized())
      aiconfig().set(Aiconfig::Type::Int(type),
                           type_int[type - Aiconfig::Type::int_first]->get_value_as_int());
  }
  else if ((type >= Aiconfig::Type::card_first)
           && (type <= Aiconfig::Type::card_last)) {
    if (type_card[type - Aiconfig::Type::card_first]->get_realized())
      aiconfig().set(Aiconfig::Type::Card(type),
                           type_card[type - Aiconfig::Type::card_first]->card());
  }
  else
    DEBUG_ASSERTION(false,
                    "AiConfig::change(type):\n"
                    "  type '" << type << "' unknown.");

  difficulty_selector->set_type(aiconfig().difficulty());
} // void Players::Player::AiConfig::change(int type)

/** a row has changed
 **
 ** @param    path    path to the changed row
 ** @param    row     changed row
 **/
void
Players::Player::AiConfig::heuristics_row_changed_event(Gtk::TreeModel::Path const& path,
                                                        Gtk::TreeModel::iterator const& row)

{
  if (!heuristics_signals_active)
    return ;

  auto& model = *heuristics_model;

  HeuristicsMap::Key const key((*row)[model.gametype_group],
                               (*row)[model.playertype_group]);

  aiconfig().set(key,
                       (*row)[model.heuristic],
                       (*row)[model.active]);
}

/** a row has been deleted
 ** This is used for recalculating the order of the rows.
 ** Assumptions are:
 ** * this is function is called last in the reordering
 ** * the indices of 'TreeModel::Path::get_indices()' are so that the first
 **   one indicates the row number (counted from 0 on)
 **
 ** @param    path    path to the changed row
 ** @param    store   corresponding store
 **
 ** @bug      too many uncertain assumptions (see description)
 **/
void
Players::Player::AiConfig::heuristics_row_deleted_event(Gtk::TreeModel::Path const& path,
                                                        Glib::RefPtr<Gtk::ListStore> store)
{
  if (!heuristics_signals_active)
    return ;

  auto& model = *heuristics_model;

  auto& row = *store->children().begin();
  HeuristicsMap::Key const key((*row)[model.gametype_group],
                               (*row)[model.playertype_group]);
  for (auto& row : store->children()) {
    aiconfig().move(key,
                          (*row)[model.heuristic],
                          store->get_path(row).front()
                         );
  }
} // void Players::Player::AiConfig::heuristics_row_deleted_event(Gtk::TreeModel::Path path, Glib::RefPtr<Gtk::ListStore> store)

/** update the sensitivity of the up and down buttons
 **
 ** @param    key      key of the heuristic
 **/
void
Players::Player::AiConfig::update_heuristic_up_down_buttons(HeuristicsMap::Key const& key)
{
  auto selection = heuristics_treeview[key]->get_selection();
  auto store = heuristics_list[key];

  DEBUG_ASSERTION((selection->count_selected_rows() <= 1),
                  "UI_GTKMM::Players::Player::AiConfig::update_heuristic_up_down_buttons()\n"
                  "  more than one line selected: (" << selection->count_selected_rows() << ") for key " << key.gametype_group << "-" << key.playertype_group);

  if (selection->count_selected_rows() == 0) {
    heuristic_up_button[key]->set_sensitive(false);
    heuristic_down_button[key]->set_sensitive(false);
    return ;
  } // if (selection->count_selected_rows() == 0)

  //auto const& row = *selection->get_selected();
  auto const& path = store->get_path(selection->get_selected());
  size_t const selected_row_no = path.front();
  heuristic_up_button[key]->set_sensitive(selected_row_no > 0);
  heuristic_down_button[key]->set_sensitive(selected_row_no <
                                                  store->children().size()
                                                  - 1);
} // void Players::Player::AiConfig::update_heuristic_up_down_buttons(HeuristicsMap::Key key)

/** update the description of the active heuristic
 **
 ** @param    key      key of the heuristic
 **/
void
Players::Player::AiConfig::update_heuristic_description(HeuristicsMap::Key const& key)
{
  auto selection = heuristics_treeview[key]->get_selection();
  auto store = heuristics_list[key];

  DEBUG_ASSERTION((selection->count_selected_rows() <= 1),
                  "UI_GTKMM::Players::Player::AiConfig::update_heuristic_description()\n"
                  "  more than one line selected: (" << selection->count_selected_rows() << ") for key " << key.gametype_group << "-" << key.playertype_group);

  if (selection->count_selected_rows() == 0) {
    heuristic_description[key]->get_buffer()->set_text("");
    return ;
  } // if (selection->count_selected_rows() == 0)

  auto const& row = *selection->get_selected();
  heuristic_description[key]->get_buffer()->set_text(gettext_description(row[heuristics_model->heuristic]));
} // void Players::Player::AiConfig::update_heuristic_description(HeuristicsMap::Key key)

/** move the selected heuristic up
 **
 ** @param    key      key of the heuristic
 **/
void
Players::Player::AiConfig::heuristic_move_up_event(HeuristicsMap::Key const& key)
{
  auto& treeview = *heuristics_treeview[key];
  auto selection = treeview.get_selection();

  DEBUG_ASSERTION((selection->count_selected_rows() == 1),
                  "UI_GTKMM::Players::Player::AiConfig::heuristic_move_up_event()\n"
                  "  not one line selected but '" << selection->count_selected_rows() << "' for key " << key.gametype_group << "-" << key.playertype_group);
  auto store = heuristics_list[key];
  auto const& path = store->get_path(selection->get_selected());
  auto const selected_row_no = path.front();
  DEBUG_ASSERTION((selected_row_no > 0),
                  "UI_GTKMM::Players::Player::AiConfig::heuristic_move_up_event()\n"
                  "  selected row is '0'");

  store->iter_swap(selection->get_selected(),
                   --selection->get_selected());
#ifdef WORKAROUND
  // call this so that the aiconfig gets noted about the change
  heuristics_row_deleted_event(path, store);
  // and update the sensitivity of the buttons
  update_heuristic_up_down_buttons(key);
#endif
  // make the selected row be visible
  treeview.scroll_to_row(store->get_path(treeview.get_selection()->get_selected()));
} // void Players::Player::AiConfig::heuristic_move_up_event(HeuristicsMap::Key key)

/** move the selected heuristic down
 **
 ** @param    key      key of the heuristic
 **/
void
Players::Player::AiConfig::heuristic_move_down_event(HeuristicsMap::Key const& key)
{
  auto& treeview = *heuristics_treeview[key];
  auto selection = treeview.get_selection();

  DEBUG_ASSERTION((selection->count_selected_rows() == 1),
                  "UI_GTKMM::Players::Player::AiConfig::heuristic_move_up_event()\n"
                  "  not one line selected but '" << selection->count_selected_rows() << "' for key " << key.gametype_group << "-" << key.playertype_group);

  auto store = heuristics_list[key];
  auto const& path = store->get_path(selection->get_selected());
  size_t const selected_row_no = path.front();
  DEBUG_ASSERTION((selected_row_no < store->children().size() - 1),
                  "UI_GTKMM::Players::Player::AiConfig::heuristic_move_up_event()\n"
                  "  selected row is too great: " << selected_row_no
                  << " >= " << store->children().size() << " = num rows");

  store->iter_swap(selection->get_selected(),
                   ++selection->get_selected());
#ifdef WORKAROUND
  // call this so that the aiconfig gets noted about the change
  heuristics_row_deleted_event(path, store);
  // and update the sensitivity of the buttons
  update_heuristic_up_down_buttons(key);
#endif
  // make the selected row be visible
  treeview.scroll_to_row(store->get_path(treeview.get_selection()->get_selected()));
} // void Players::Player::AiConfig::heuristic_move_down_event(HeuristicsMap::Key key)

/** sets the ai to the hardcoded
 **/
void
Players::Player::AiConfig::hardcoded()
{
  aiconfig().reset_to_hardcoded();
  update();
}

/** update all widgets
 **/
void
Players::Player::AiConfig::update()
{
  difficulty_selector->set_type(aiconfig().difficulty());
  difficulty_description_update();

  rating->set_active_text(_(aiconfig().rating()));

  for (auto widget : type_bool)
    widget->set_active(aiconfig().value(*static_cast<Aiconfig::Type::Bool*>(widget->get_data("type"))));
  for (auto widget : type_int) {
    auto const type
      = *static_cast<Aiconfig::Type::Int*>(widget->get_data("type"));
    widget->set_range(aiconfig().min(type),
                      aiconfig().max(type));
    widget->set_value(aiconfig().value(type));
  } // for (auto widget = type_int)
  type_int[Aiconfig::Type::future_limit - Aiconfig::Type::int_first]->set_increments(1, 1000);
  type_int[Aiconfig::Type::max_sec_wait_for_gametree - Aiconfig::Type::int_first]->set_increments(1, 10);
  for (auto widget : type_card) {
    auto const type
      = *static_cast<Aiconfig::Type::Card*>(widget->get_data("type"));
    widget->set_card(aiconfig().value(type));
  } // for (widget \in type_card)

  { // heuristics
    heuristics_signals_active = false;
    auto& model = *heuristics_model;
    for (auto const& m : heuristics_list) {
      auto const& key = m.first;
      auto const& states = aiconfig_const().heuristic_states(key);
      auto& store = m.second;

      { // create the rows
        store->clear();
        for (auto s : states) {
          auto row = *store->append();

          row[model.active] = aiconfig().value(key, s.heuristic);
          row[model.heuristic] = s.heuristic;
          row[model.heuristic_name] = _(s.heuristic);
          row[model.gametype_group] = key.gametype_group;
          row[model.playertype_group] = key.playertype_group;

        } // for (auto s : states)
      } // create the rows

    } // for (m \in heuristics_list)
    heuristics_signals_active = true;
  } // heuristics
} // void Players::Player::AiConfig::update()

/** update the description of the difficulty according to the player type
 **
 ** @bug       the un-sensitivity is not accepted when called before the container has been drawn (gtkmm bug?)
 **/
void
Players::Player::AiConfig::difficulty_description_update()
{
  // change the name of the difficulty button
  switch (player->player().type()) {
  case ::Player::Type::unset:
    DEBUG_ASSERTION(false,
                    "UI_GTKMM::Players::Player::AiConfig::type_change(type)\n"
                    "  type is 'unset'");
    break;
  case ::Player::Type::human:
  case ::Player::Type::ai:
    difficulty_selector->set_sensitive(true);
    container->set_sensitive(true);
    switch (player->player().type()) {
    case ::Player::Type::human:
      difficulty_label->set_label(_("card suggestion") + ": ");
      break;
    case ::Player::Type::ai:
      difficulty_label->set_label(_("difficulty") + ": ");
      break;
    default:
      break;
    } // switch (player->player().type())
    break;
  case ::Player::Type::ai_dummy:
  case ::Player::Type::ai_random:
    difficulty_label->set_label(_("no ai configuration") + ": ");
    difficulty_selector->set_sensitive(false);
    container->set_sensitive(false);
    break;
  } // switch (player->player().type())
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
