/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef USE_UI_GTKMM

#include "base.h"
#include "player.h"
#include "../../player/aiconfig.h"

#include "widgets/label_type_selector.h"
#include <sigc++/trackable.h>
#include <gtkmm/liststore.h>
namespace Gtk {
class Box;
class Notebook;
class Button;
class Label;
class CheckButton;
class LabelSpinButton;
class SpinButton;
class ComboBoxText;
class MenuItem;
class TreeView;
class TextView;
class Widget;
class LabelCardSelector;
} // namespace Gtk

class Player;
class Aiconfig;
namespace UI_GTKMM_NS {

/**
 ** the ai configuration of one player
 **/
class Players::Player::AiConfig : public Base, public sigc::trackable {
  public:
    friend class Players;
    friend class Player;

    /**
     ** The Model for a heuristic
     **/
    struct HeuristicsModel : public Gtk::TreeModel::ColumnRecord {
        HeuristicsModel()
        {
          this->add(this->active);
          this->add(this->heuristic);
          this->add(this->heuristic_name);
          this->add(this->gametype_group);
          this->add(this->playertype_group);
        }

        Gtk::TreeModelColumn<bool> active;
        Gtk::TreeModelColumn<::Aiconfig::Heuristic> heuristic;
        Gtk::TreeModelColumn<Glib::ustring> heuristic_name;
        Gtk::TreeModelColumn<HeuristicsMap::GameTypeGroup> gametype_group;
        Gtk::TreeModelColumn<HeuristicsMap::PlayerTypeGroup> playertype_group;
    }; // struct HeuristicsModel : public Gtk::TreeModel::ColumnRecord

  public:
    explicit AiConfig(Player* player);
    ~AiConfig() override;

    ::Aiconfig& aiconfig();
    ::Aiconfig const& aiconfig_const() const;

    void change(int type);

    void hardcoded();

    void update();
    void difficulty_description_update();

  public:
    void init();

  private:
    void create_container();
    void add_heuristic_treeview(Gtk::Notebook* notebook,
                                HeuristicsMap::GameTypeGroup gametype_group,
                                HeuristicsMap::PlayerTypeGroup playertype_group,
                                string const& label_text);
    void add_heuristic_treeviews_re_contra(Gtk::Notebook* notebook,
                                           HeuristicsMap::GameTypeGroup gametype_group);
    void add_heuristic_treeviews_special_re_contra(Gtk::Notebook* notebook,
                                                   HeuristicsMap::GameTypeGroup gametype_group);
    Gtk::Widget* create_heuristic_treeview(HeuristicsMap::GameTypeGroup gametype_group,
                                           HeuristicsMap::PlayerTypeGroup playertype_group);
    void init_heuristic_treeview(Gtk::Box* box,
                                 HeuristicsMap::GameTypeGroup gametype_group,
                                 HeuristicsMap::PlayerTypeGroup playertype_group);


    void difficulty_changed();
    void rating_changed();

    void heuristics_row_changed_event(Gtk::TreeModel::Path const& path,
                                      Gtk::TreeModel::iterator const& row);
    void heuristics_row_deleted_event(Gtk::TreeModel::Path const& path,
                                      Glib::RefPtr<Gtk::ListStore> store);

    void update_heuristic_up_down_buttons(HeuristicsMap::Key const& key);
    void update_heuristic_description(HeuristicsMap::Key const& key);

    void heuristic_move_up_event(HeuristicsMap::Key const& key);
    void heuristic_move_down_event(HeuristicsMap::Key const& key);

  private:
    Player* const player = nullptr;

  protected:
    Gtk::Box* container = nullptr;
    Gtk::Notebook* notebook = nullptr;
    vector<Gtk::Notebook*> notebooks;

    Gtk::Container* difficulty_container = nullptr;
    Gtk::Label* difficulty_label = nullptr;
    Gtk::Button* difficulty_button = nullptr;
    Gtk::LabelTypeSelector< ::Aiconfig::Difficulty>* difficulty_selector = nullptr;

    vector<Gtk::CheckButton*> type_bool;
    vector<Gtk::LabelSpinButton*> type_int;
    vector<Gtk::LabelCardSelector*> type_card;
    Gtk::ComboBoxText* rating = nullptr;

    HeuristicsModel* heuristics_model = nullptr;
    std::map<HeuristicsMap::Key, Glib::RefPtr<Gtk::ListStore> > heuristics_list;
    std::map<HeuristicsMap::Key, Gtk::TreeView*> heuristics_treeview;
    std::map<HeuristicsMap::Key, Gtk::Button*> heuristic_up_button;
    std::map<HeuristicsMap::Key, Gtk::Button*> heuristic_down_button;
    std::map<HeuristicsMap::Key, Gtk::TextView*> heuristic_description;
    bool heuristics_signals_active = false;

  private: // unused
    AiConfig() = delete;
    AiConfig(AiConfig const&) = delete;
    AiConfig& operator=(AiConfig const&) = delete;
}; // class Players::Player::AiConfig : public Base, public Gtk::Object

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
