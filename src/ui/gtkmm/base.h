/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef USE_UI_GTKMM

namespace UI_GTKMM_NS {
class UI_GTKMM;

/**
 ** the base class
 **/
class Base {
  public:
    explicit Base(Base* parent);
    Base(Base* parent, UI_GTKMM* ui);
    virtual ~Base();

    Base() = delete;
    Base(Base const&) = delete;
    Base& operator=(Base const&) = delete;

  public:
    UI_GTKMM* const ui;
    Base* const parent;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
