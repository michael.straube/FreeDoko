/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"

#include "widgets/sticky_dialog.h"
#include "widgets/file_chooser_dialog_wrapper.h"

namespace Gtk {
class TextView;
class CheckButton;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the bug report dialog and selector
 **/
class BugReport : public Base, public Gtk::StickyDialog {
  public:
    BugReport() = delete;
    BugReport(Base* parent, string message = "");
    BugReport(BugReport const&) = delete;
    BugReport& operator=(BugReport const&) = delete;
    ~BugReport() override;

    bool load(string filename);

    // sets the widget as destination for the drop of a bug report
    void set_dnd_destination(Gtk::Widget& widget);

    void create_report();
    void save();

  private:
    void init();

    void save_on_desktop_toggled_event();

    // drag-and-drop: a bug report was dropped
    void on_bug_report_dropped(Glib::RefPtr<Gdk::DragContext> const& context,
                               int x, int y,
                               Gtk::SelectionData const& selection_data,
                               guint info, guint time);
  private:
    AutoDisconnector disconnector_;
    Gtk::TextView* description = nullptr;
    Gtk::Label* file_label = nullptr;
    Gtk::CheckButton* save_on_desktop_button = nullptr;
    Gtk::TextView* message = nullptr;
    Gtk::Button* dismiss_button = nullptr;
    Gtk::Button* save_button = nullptr;

  public:
    unique_ptr<Gtk::FileChooserDialogWrapper> load_file_chooser;
}; // class BugReport : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
