/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "bug_report_replay.h"
#include "gameplay_actions.h"

#include "ui.h"
#include "thrower.h"
#include "bug_report.h"
#include "main_window.h"
#include "cards.h"

#include "../../basetypes/fast_play.h"
#include "../../os/bug_report_replay.h"
#include "../../game/game.h"
#include "../../player/player.h"
#include "../../player/aiconfig.h"

#include "../../utils/string.h"

#include "game_summary.h"
#include <gtkmm/notebook.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>

namespace UI_GTKMM_NS {

/** Constructor
 **
 ** @param    parent   the parent widget
 **/
BugReportReplay::BugReportReplay(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::Bug report replay"), false)
{
  ui->add_window(*this);

  actions       = Gtk::manage(new GameplayActions(this));
  human_actions = Gtk::manage(new GameplayActions(this));
  game_summary  = Gtk::manage(new GameSummary(this));

  signal_realize().connect(sigc::mem_fun(*this, &BugReportReplay::init));

  ui->bug_report->set_dnd_destination(*this);

  Party::signal_open.connect_back([this](Party& party) {
                                  party.signal_get_settings.connect_back(*this, &BugReportReplay::update_info, disconnector_);
                                  },
                                  disconnector_);
  Game::signal_open.connect_back([this](Game& game) {
                                 update_info();
                                 game.signal_redistribute.connect_back(*this, &BugReportReplay::update_info, disconnector_);
                                 game.signal_gameplay_action.connect_back(*this, &BugReportReplay::gameplay_action, disconnector_);
                                 },
                                 disconnector_);
  OS_NS::BugReportReplay::signal_open.connect_back(*this,
                                                   &UI_GTKMM_NS::BugReportReplay::bug_report_replay_open,
                                                   disconnector_);
} // BugReportReplay::BugReportReplay(Base* parent)

/** destructor
 **/
BugReportReplay::~BugReportReplay() = default;

/** create all subelements
 **/
void
BugReportReplay::init()
{
  set_icon(ui->icon);

  { // action area
    auto end_button
      = Gtk::manage(new Gtk::Button(_("Button::end bug report")));

    end_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                       &BugReportReplay::end_bug_report));

    add_close_button(*this);
  } // action area

  { // informations
    version = Gtk::manage(new Gtk::Label(""));
    version->set_justify(Gtk::JUSTIFY_LEFT);
    compiled = Gtk::manage(new Gtk::Label(""));
    compiled->set_justify(Gtk::JUSTIFY_LEFT);
    system = Gtk::manage(new Gtk::Label(""));
    system->set_justify(Gtk::JUSTIFY_LEFT);
    time = Gtk::manage(new Gtk::Label(""));
    time->set_justify(Gtk::JUSTIFY_LEFT);
    language = Gtk::manage(new Gtk::Label(""));
    language->set_justify(Gtk::JUSTIFY_LEFT);
    trick = Gtk::manage(new Gtk::Label(""));
    trick->set_justify(Gtk::JUSTIFY_LEFT);
    reset_ais_button = Gtk::manage(new Gtk::Button(_("Button::reset ais")));
    reset_ais_button->set_image_from_icon_name("edit-clear-all");
    reset_ais_button->set_always_show_image();
    message = Gtk::manage(new Gtk::TextView());
    message->get_buffer()->set_text("");
    message->set_wrap_mode(Gtk::WRAP_WORD);
#ifdef RELEASE
    message->set_editable(false);
    message->set_cursor_visible(false);
#else
    // Note: the changes in the message box are not saved.
#endif

    seed = Gtk::manage(new Gtk::Label(""));
    seed->set_justify(Gtk::JUSTIFY_LEFT);

    startplayer = Gtk::manage(new Gtk::Label(""));
    startplayer->set_justify(Gtk::JUSTIFY_LEFT);

    soloplayer = Gtk::manage(new Gtk::Label(""));
    soloplayer->set_justify(Gtk::JUSTIFY_LEFT);

    // hide the 'player' column
    human_actions->get_column(1)->set_visible(false);
  } // informations
  { // vbox
    notebook = Gtk::manage(new Gtk::Notebook());
    { // general
      auto label = Gtk::manage(new Gtk::Label(_("BugReportReplay::Group::general")));

      auto general_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EM));
      general_box->set_border_width(1 EM);

      { // game box
        auto game_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));

        game_box->pack_start(*version, Gtk::PACK_EXPAND_WIDGET);
        game_box->pack_start(*system, Gtk::PACK_EXPAND_WIDGET);
        game_box->pack_start(*seed, Gtk::PACK_EXPAND_WIDGET);
        game_box->pack_start(*startplayer, Gtk::PACK_EXPAND_WIDGET);
        game_box->pack_start(*soloplayer, Gtk::PACK_EXPAND_WIDGET);
        game_box->pack_start(*trick, Gtk::PACK_EXPAND_WIDGET);
        reset_ais_button->set_halign(Gtk::ALIGN_CENTER);
        game_box->pack_start(*reset_ais_button, Gtk::PACK_EXPAND_WIDGET);

        general_box->pack_start(*game_box, false, true);
      } // game box
      { // message
        general_box->add(*message);
      } // message

      notebook->append_page(*general_box, *label);
    } // general
    { // actions
      auto label = Gtk::manage(new Gtk::Label(_("BugReportReplay::Group::actions")));

      auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));

      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
      scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC,
                                  Gtk::POLICY_AUTOMATIC);

      actions->get_selection()->set_mode(Gtk::SELECTION_SINGLE);
      scrolled_window->add(*actions);

      vbox->pack_start(*scrolled_window, Gtk::PACK_EXPAND_WIDGET);

      auto_actions_button
        = Gtk::manage(new Gtk::Button(_("Button::auto actions")));
      vbox->pack_end(*auto_actions_button, Gtk::PACK_SHRINK);

      notebook->append_page(*vbox, *label);
    } // actions
    { // human actions
      auto label = Gtk::manage(new Gtk::Label(_("BugReportReplay::Group::human actions")));
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
      scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC,
                                  Gtk::POLICY_AUTOMATIC);

      scrolled_window->add(*human_actions);

      notebook->append_page(*scrolled_window, *label);
    } // human actions
    { // game summary
      auto label = Gtk::manage(new Gtk::Label(_("BugReportReplay::Group::game summary")));

      notebook->append_page(*game_summary, *label);
    } // game summary

    get_content_area()->pack_start(*notebook, true, true);
  } // vbox

  { // signals
    reset_ais_button->signal_clicked().connect(sigc::mem_fun(*this, &BugReportReplay::reset_ais));
    actions->get_selection()->signal_changed().connect(sigc::mem_fun(*this, &BugReportReplay::update_auto_actions_button));
    auto_actions_button->signal_clicked().connect(sigc::mem_fun(*this, &BugReportReplay::set_auto_actions));
    update_auto_actions_button();
  } // signals

  show_all_children();


  {
    Gdk::Geometry geometry;
    geometry.min_width = 3 * ui->cards->height();
    geometry.min_height = 4 * ui->cards->height();

    set_geometry_hints(*this, geometry, Gdk::HINT_MIN_SIZE);
  }
} // void BugReportReplay::init()

/** a bug report replay is opened
 **
 ** @param    bug_report_replay   the bug report replay that is opened
 **/
void
BugReportReplay::bug_report_replay_open(OS_NS::BugReportReplay&
                                        bug_report_replay)
{
  DEBUG_ASSERTION((&bug_report_replay == ::bug_report_replay.get()),
                  "BugReportReplay::bug_report_replay_open(bug_report_replay)\n"
                  "  the bug report replay "
                  " = " << &bug_report_replay
                  << " != " << ::bug_report_replay.get()
                  << " = global one");
  bug_report_replay_ = &bug_report_replay;
  bug_report_replay_->signal_close.connect_back(*this,
                                                      &UI_GTKMM_NS::BugReportReplay::bug_report_replay_close,
                                                      disconnector_);

  if (!get_realized())
    realize();

  actions->set_actions(bug_report_replay.actions());
  actions->set_discrepancies(bug_report_replay.actions_discrepancies());
  human_actions->set_actions(bug_report_replay.human_actions());
  human_actions->set_discrepancies(bug_report_replay.human_actions_discrepancies());
  if (bug_report_replay.game_summary()) {
    game_summary->set_game_summary(*bug_report_replay.game_summary());
    game_summary->show();
  } else {
    game_summary->hide();
  }
  // ToDo: hide the last page if there is no game summary in the bug report

  update_info();
  notebook->set_current_page(0);

  if (!(::fast_play & FastPlay::hide_bug_report_window)) {
    show();
  }
} // void BugReportReplay::bug_report_replay_open(OS_NS::BugReportReplay bug_report_replay)

/** the bug report replay is closed
 **/
void
BugReportReplay::bug_report_replay_close()
{
  actions->remove_actions();
  human_actions->remove_actions();
  if (   bug_report_replay_
      && ::bug_report_replay
      && bug_report_replay_ == ::bug_report_replay.get()) {
    // 'if' is workaround: first hide then show leaves the window hidden
    bug_report_replay_ = nullptr;
    hide();
  }
} // BugReportReplay::bug_report_replay_close()

/** there happened a gameplay action
 **
 ** @param    action   gameplay action
 **/
void
BugReportReplay::gameplay_action(GameplayAction const& action)
{
  if (!bug_report_replay_)
    return ;

  actions->set_current_action_no(bug_report_replay_->current_action_no());
  human_actions->set_current_action_no(bug_report_replay_->current_human_action_no());
  update_auto_actions_button();
} // void BugReportReplay::gameplay_action(GameplayAction action)

/** updates the information
 **/
void
BugReportReplay::update_info()
{
  if (!bug_report_replay_)
    return ;

  DEBUG_ASSERTION((bug_report_replay_ == ::bug_report_replay.get()),
                  "BugReportReplay::update_info():\n"
                  "  bug_report_replay_ != ::bug_report_replay:\n"
                  "  " << bug_report_replay_
                  << " != " << ::bug_report_replay.get());

  if (ui->party().status() == Party::Status::programstart)
    return ;
#if 0
  if (!&party())
    return ;
#endif


#ifdef WORKAROUND
  // This function can be called before this party is set
  // (by ::bug_report_replay). So we take a party here that always exists.
  // An alternitive would be to check, whether this party is set, but
  // 'UI::party_' is private.
  Party const& party = *::party;
#else
  Party const& party = ui->party();
#endif

  version->set_label(_("Version: %s",
                             to_string(bug_report_replay_->version())));
  compiled->set_label(_("Compiled: %s",
                              bug_report_replay_->compiled()));
  system->set_label(_("System: %s",
                            bug_report_replay_->system()));
  time->set_label(_("Time: %s",
                          bug_report_replay_->time()));
  language->set_label(_("Language: %s",
                              bug_report_replay_->language()));
  trick->set_label(_("Trick: %u",
                           bug_report_replay_->trickno()));

#ifdef WORKAROUND
  // message: change iso to utf8
  message->get_buffer()->set_text(UI_GTKMM::to_utf8(bug_report_replay_->message()));
#else
  message->get_buffer()->set_text(bug_report_replay_->message());
#endif


  seed->set_label(String::to_string(bug_report_replay_->seed()));

  startplayer->set_label(String::to_string(bug_report_replay_->startplayer_no()));
  seed->set_label(_("Seed: %u", bug_report_replay_->seed()));

  if (bug_report_replay_->startplayer_no() != UINT_MAX)
    startplayer->set_label(_("Startplayer: %u",
                                   bug_report_replay_->startplayer_no())
                                 + " (" + party.players()[bug_report_replay_->startplayer_no()].name() + ")");
  else
    startplayer->set_label(_("Startplayer: %u",
                                   bug_report_replay_->startplayer_no())
                                 + " (-)"
                                );

  if (bug_report_replay_->soloplayer_no() != UINT_MAX)
    soloplayer->set_label(_("Soloplayer: %u",
                                  bug_report_replay_->soloplayer_no())
                                + " (" + party.players()[bug_report_replay_->soloplayer_no()].name() + ")"
                               );
  else
    soloplayer->set_label(_("Soloplayer: -"));

  actions->update();
  human_actions->update();

  if (bug_report_replay_->game_summary())
    game_summary->update();

  update_actions_past();
} // void BugReportReplay::update_info()

/** update the showing of the actions already made
 **
 ** @todo      all
 **/
void
BugReportReplay::update_actions_past()
{
  if (!get_realized())
    return ;

#if 0
  for (auto& row : actions->get_model()->children()) {
    for (auto col : actions->get_columns()) {
      // ToDo
    } // for (col \in actsion->get_columns())
  } // for (row \in actions)
#endif
} // void BugReportReplay::update_actions_past()

/** update the auto actions button
 **/
void
BugReportReplay::update_auto_actions_button()
{
  if (!get_realized())
    return ;

  auto selection = actions->get_selection();

  if (selection->get_selected()) {
    auto row = *(selection->get_selected());

    unsigned const actionno = row[actions->model.no];

    if (actionno > bug_report_replay_->current_action_no()) {
      auto_actions_button->set_sensitive(true);
    } else {
      auto_actions_button->set_sensitive(false);
    }
  } else {	// if !(selection->get_selected())
    auto_actions_button->set_sensitive(false);
  }	// if !(selection->get_selected())
} // void BugReportReplay::update_auto_actions_button()

/** automatic execute till the selected action
 **/
void
BugReportReplay::set_auto_actions()
{
  auto selection = actions->get_selection();

  DEBUG_ASSERTION(selection->get_selected(),
                  "BugReportReplay::set_auto_actions()\n"
                  "  no action selected");
  auto row = *(selection->get_selected());

  unsigned const actionno = row[actions->model.no];

  bug_report_replay_->set_auto_action_end(actionno);
  if (   ui->party().in_game()
      && ui->game().status() == Game::Status::play
      && ui->game().players().current_player().type() == Player::Type::human) {
    auto& player = ui->game().players().current_player();
    player.hand().request_card(player.card_get());
    ui->thrower(player.hand().requested_card(), __FILE__, __LINE__);
  } // if (human has to play a card)
} // void BugReportReplay::set_auto_actions()

/** end the bug report
 **/
void
BugReportReplay::end_bug_report()
{
  bug_report_replay_ = nullptr;
} // void BugReportReplay::end_bug_report()

/** reset the ais
 **/
void
BugReportReplay::reset_ais()
{
  int n = 0;
  for (auto& p : ui->party().players()) {
    if (dynamic_cast<Aiconfig*>(&p))
      dynamic_cast<Aiconfig&>(p).reset_to_hardcoded(n);
    n += 1;
  }
} // void BugReportReplay::reset_ais()

/** the name of 'player' has changed
 **
 ** @param    player   player whose name has changed
 **/
void
BugReportReplay::name_changed(Player const& player)
{
  if (!get_realized())
    return ;

  if (!bug_report_replay_)
    return ;

  if (ui->party().in_game()) {
    if (bug_report_replay_->startplayer_no()
        == player.no())
      startplayer->set_label(_("Startplayer: %u",
                                     player.no())
                                   + " (" + player.name() + ")"
                                  );

    if (bug_report_replay_->soloplayer_no() == player.no())
      soloplayer->set_label(_("Soloplayer: %u",
                                    player.no())
                                  + " (" + player.name() + ")"
                                 );
  } // if (ui->party().in_game())

  actions->name_changed(player);
  human_actions->name_changed(player);
  game_summary->name_changed(player);
} // void BugReportReplay::name_changed(Player player)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
