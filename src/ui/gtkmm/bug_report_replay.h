/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "widgets/sticky_dialog.h"
#include "../../os/bug_report_replay.h"

#include "../../party/party.h"
#include "../../party/rule.h"

#include <stack>
using std::stack;

#include <gtkmm/treemodel.h>
namespace Gtk {
class Notebook;
class Label;
class Button;
class TextView;
class CellRenderer;
} // namespace Gtk

class Trick;

namespace UI_GTKMM_NS {
class GameplayActions;
class GameSummary;

/**
 ** the bug report that is replayed
 **
 ** Note, this class gets the information directly over the OS,
 ** the functions must not be called by UI_GTKMM.
 ** In 'Ui_Wrap' this class must be inserted behind '::bug_report_replay'.
 **
 ** @todo   save changes in the message box
 **/
class BugReportReplay : public Base, public Gtk::StickyDialog {
  public:
    explicit BugReportReplay(Base* parent);
    BugReportReplay() = delete;
    BugReportReplay(BugReportReplay const&) = delete;
    BugReportReplay& operator=(BugReportReplay const&) = delete;
    ~BugReportReplay() override;


    void gameplay_action(GameplayAction const& action);

    void bug_report_replay_open(OS_NS::BugReportReplay& bug_report_replay);
    void bug_report_replay_close();

  private:
    void init();

    void update_info();
    void update_actions_past();
    void update_auto_actions_button();

    void set_auto_actions();
    void end_bug_report();

    void reset_ais();

  public:
    void name_changed(Player const& player);

  private:
    AutoDisconnector disconnector_;

    OS_NS::BugReportReplay* bug_report_replay_ = nullptr;
    Gtk::Notebook* notebook = nullptr;

    Gtk::Label* version = nullptr;
    Gtk::Label* compiled = nullptr;
    Gtk::Label* system = nullptr;
    Gtk::Label* time = nullptr;
    Gtk::Label* language = nullptr;
    Gtk::Label* trick = nullptr;
    Gtk::Button* reset_ais_button = nullptr;
    Gtk::TextView* message = nullptr;

    Gtk::Label* seed = nullptr;
    Gtk::Label* startplayer = nullptr;
    Gtk::Label* soloplayer = nullptr;
    // rules
    // players
    // game_replay (tricks)
    // hands
    // gamepoints

    GameplayActions* actions = nullptr;
    Gtk::Button*     auto_actions_button = nullptr;
    GameplayActions* human_actions = nullptr;
    GameSummary*     game_summary = nullptr;

  private: // unused
}; // class BugReportReplay : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
