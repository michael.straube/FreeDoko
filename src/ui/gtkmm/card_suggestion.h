/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "widgets/sticky_dialog.h"

#include "../../card/card.h"
class Player;

#include <gdk/gdkevents.h>
namespace Gtk {
class EventBox;
class Image;
class Label;
class TextView;
}; // namespace Gtk

namespace UI_GTKMM_NS {
class Table;

/**
 ** card suggestion dialog
 **/
class CardSuggestion : public Base, public Gtk::StickyDialog {
public:
  explicit CardSuggestion(Table* table);
  ~CardSuggestion() override;

  auto player() const -> ::Player const&;

  void show_information(bool show_window);
  void play_card();

  void card_played();

private:
  void init();
  void get_suggestion();
  auto play_card_signal(GdkEventButton* event) -> bool;

public:
  virtual void mouse_click_on_table();

private:
  Gtk::EventBox* card_box = nullptr;
  Gtk::Image* card_image = nullptr;
  Gtk::Label* heuristics_text = nullptr;
  Gtk::TextView* heuristics_description = nullptr;

  Gtk::Button* play_button = nullptr;

public:
  Card suggested_card;

private:
  bool thinking = false;

private: // unused
  CardSuggestion() = delete;
  CardSuggestion(CardSuggestion const&) = delete;
  CardSuggestion& operator=(CardSuggestion const&) = delete;
}; // class CardSuggestion : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
