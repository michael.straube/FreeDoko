/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"

#include "widgets/scaled_pixbuf.h"

#include "../../basetypes/rotation.h"
#include "../../card/card.h"

#include <glibmm/refptr.h>
namespace Gtk {
class Image;
}; // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the cards
 **/
class Cards : public Base, public sigc::trackable {
  public:
    explicit Cards(Base* base);
    Cards() = delete;
    Cards(Cards const&) = delete;
    Cards& operator=(Cards const&) = delete;

    ~Cards() override;

    double scaling() const;
    void update_scaling();

    // the original width of the cards
    int width_original() const;
    // the original height of the cards
    int height_original() const;
    // the width of the scaled cards
    int width(Rotation rotation = Rotation::up) const;
    // the width of the scaled cards
    int height(Rotation rotation = Rotation::up) const;

    // the back picture (scaled)
    Gdk::ScaledPixbuf& back();
    // the card picture (scaled)
    Gdk::ScaledPixbuf& card(Card card);
    // the back picture (original)
    Glib::RefPtr<Gdk::Pixbuf> back_original();
    // the card picture (original)
    Glib::RefPtr<Gdk::Pixbuf> card_original(Card card);

    // managed images
    Gtk::Image* new_managed_image(Card card = Card());
    void change_managed(Gtk::Image* image, Card card);
    void remove_managed(Gtk::Image const* image);
    void update_managed();

    void load_all();
    void load_back();
    void load_cards();
    auto path(Card card) -> string;

  private:
    AutoDisconnector disconnector_;
    double scaling_ = 1;

    // the back picture
    Gdk::ScaledPixbuf back_;
    // the card pictures
    vector<vector<Gdk::ScaledPixbuf>> card_;

    // the images that are managed by 'this'
    vector<Gtk::Image*> managed_image_;
}; // class Cards : public Base

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
