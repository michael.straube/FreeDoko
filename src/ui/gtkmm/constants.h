/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

// ui/gtkmm/constants.h
#pragma once

#include "../constants.h"

#ifdef USE_UI_GTKMM

//#define TEST_LAYOUT

#include <gtkmm/base.h>
#include "utils.h"

// version checking
// Copied from gtkmm/base.h (GTK_VERSION_GE).
#ifndef GTKMM_VERSION_GE
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GTKMM_VERSION_GE(major,minor)  ((GTKMM_MAJOR_VERSION>major)||(GTKMM_MAJOR_VERSION==major)&&(GTKMM_MINOR_VERSION>=minor))
#endif


#include <glibmm.h>
// these are not defined p.e. for the internet tablet
#ifndef GLIBMM_EXCEPTIONS_ENABLED
#define GLIBMM_EXCEPTIONS_ENABLED
#endif
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
#define GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
#endif
#ifndef GLIBMM_PROPERTIES_ENABLED
#define GLIBMM_PROPERTIES_ENABLED
#endif


// for relative spaces
//
// Dynamische Ermittlung, siehe
// https://git.gnome.org/browse/glom/tree/glom/utils_ui.cc#n296
// (Version von Ende 2014)
// 
// used for:
// * border in notebooks
#define EM * 18 // NOLINT(cppcoreguidelines-macro-usage)
// used for:
// * spacing in boxes
#define EX * 12 // NOLINT(cppcoreguidelines-macro-usage)


namespace UI_GTKMM_NS {
constexpr array<char const*, 4> supported_graphic_formats = {".png", ".jpg", ".jpeg", ".gif"};
} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
