/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "full_trick.h"
#include "trick_summary.h"
#include "ui.h"
#include "table.h"
#include "cards.h"

#include "../../party/party.h"
#include "../../game/game.h"
#include "../../card/trick.h"
#include "../../misc/preferences.h"

#include <gtkmm/image.h>
#include <gtkmm/main.h>

namespace UI_GTKMM_NS {

FullTrick::FullTrick(Table* const table) :
  Base(table),
  Gtk::StickyDialog(_("Window::full trick"), *table->ui->main_window, false)
{
  set_gravity(Gdk::GRAVITY_STATIC);

  signal_realize().connect(sigc::mem_fun(*this, &FullTrick::init));
}


FullTrick::~FullTrick() = default;


void FullTrick::init()
{
  auto close_button = add_close_button(*this);
  close_button->signal_clicked().connect(sigc::mem_fun(*this, &FullTrick::close));

  { // vbox
    get_content_area()->set_spacing(1 EX);

    trick_summary = Gtk::manage(new TrickSummary(this, false));

    get_content_area()->add(*trick_summary);

    winnercard = ui->cards->new_managed_image();
    winnercard->set_padding(2 EM, 2 EX);
    trick_summary->pack_start(*winnercard);
  } // vbox

  show_all_children();
  trick_summary->update();

  { // move to the top left corner of the window
    int x = 0;
    int y = 0;
    dynamic_cast<Table*>(parent)->get_window()->get_origin(x, y);
    move(x, y);
  } // move to the top left conrner of the window
}


void FullTrick::show_information(::Trick const& trick)
{
  if (!get_realized())
    realize();
  auto const& game = trick.game();

  bool show = ::preferences(::Preferences::Type::show_full_trick_window);
  if (!show
      && ::preferences(::Preferences::Type::show_full_trick_window_if_special_points)) {
    // test whether the teams of the player of and for are different
    auto const specialpoints = trick.specialpoints();
    auto const& teaminfo = game.teaminfo();
    for (auto const& sp : specialpoints) {
      if (sp.player_of_no == UINT_MAX) {
        show = true;
        break;
      }
      auto const& player_get = game.player(sp.player_get_no);
      auto const& player_of = game.player(sp.player_of_no);
      auto const teaminfo_get = teaminfo.get_human_teaminfo(player_get);
      auto const teaminfo_of = teaminfo.get_human_teaminfo(player_of);
      if (   !is_real(teaminfo_get)
          || (teaminfo_get != teaminfo_of) ) {
        show = true;
        break;
      } // if (trick shall be shown)
    } // for (sp)
  } // if (!show)

  if (show) {
    trick_summary->set_trick(trick);
    ui->cards->change_managed(winnercard,
                                    trick.winnercard());

    present();
  }

  sigc::connection timeout_connection;
  if (::preferences(::Preferences::Type::close_full_trick_automatically))
    timeout_connection
      = Glib::signal_timeout().connect(sigc::slot0<bool>(sigc::mem_fun(*this, &FullTrick::on_timeout)),
                                       ::preferences(::Preferences::Type::full_trick_close_delay));

  ui->table->mouse_cursor_update();

  try {
    while (game.status() == Game::Status::full_trick)
      ::ui->wait();
  } catch (Game::Status const status) {
    if (status == Game::Status::trick_taken) {
      ::party->game().set_trick_taken();
    } else {
      hide();
      throw;
    }
  }

  if (::preferences(::Preferences::Type::close_full_trick_automatically))
    timeout_connection.disconnect();

  if (show)
    hide();

  ui->cards->change_managed(winnercard, Card());
}


void FullTrick::close()
{
#ifdef WORKAROUND
  // Manchmal wird die Funktion wohl doppelt aufgerufen
  if (ui->game().tricks().current().isempty())
    return ;
#endif
  DEBUG_ASSERTION(ui->game().tricks().current().isfull(),
                  "FullTrick::close()\n"
                  "  the current trick is not full.");
  DEBUG_ASSERTION(ui->game().status() == Game::Status::full_trick,
                  "FullTrick::close()\n"
                  "  the game status is not game full trick but " << ui->game().status());

#ifdef WORKAROUND
  // problem: this exception is thrown two times
  // It seems that that one time is from the automatic closing
  // and the other from manual closing.
  if (ui->thrower.exception_type() == Thrower::Type::party_status)
    return ;
  if (ui->thrower.exception_type() == Thrower::Type::game_status)
    return ;
#endif
  ui->thrower(Game::Status::trick_taken, __FILE__, __LINE__);
}


void FullTrick::name_changed(Player const& player)
{
  if (!is_visible())
    return ;

  trick_summary->update();
}


auto FullTrick::on_timeout() -> bool
{
  close();

  return false;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
