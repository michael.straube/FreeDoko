/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "widgets/sticky_dialog.h"

class Trick;
class Player;

namespace Gtk {
class Image;
}; // namespace Gtk

namespace UI_GTKMM_NS {
class Table;
class TrickSummary;

/**
 ** the full trick window
 **/
class FullTrick : public Base, public Gtk::StickyDialog {
public:
  explicit FullTrick(Table* table);
  FullTrick() = delete;
  FullTrick(FullTrick const&) = delete;
  auto operator=(FullTrick const&) -> FullTrick& = delete;
  ~FullTrick() override;

  void show_information(::Trick const& trick);
  void close();

  void name_changed(Player const& player);

private:
  void init();

  auto on_timeout() -> bool;

private:
  TrickSummary* trick_summary = nullptr;
  Gtk::Image*   winnercard = nullptr;
}; // class FullTrick : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
