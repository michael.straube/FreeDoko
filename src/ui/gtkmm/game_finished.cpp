/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "game_finished.h"
#include "game_summary.h"
#include "game_review.h"

#include "ui.h"
#include "bug_report.h"
#include "main_window.h"
#include "table.h"
#include "party_points.h"

#include "../../party/party.h"
#include "../../game/game_summary.h"
#include "../../misc/preferences.h"

#include <gtkmm/checkbutton.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent widget
 **/
GameFinished::GameFinished(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::Game finished"),
               *parent->ui->main_window, false)
{
  ui->add_window(*this);

  // the game summary is created here, because else I would have first
  // to show this window and afterwards set the gameno.
  game_summary = Gtk::manage(new GameSummary(this));

  signal_realize().connect(sigc::mem_fun(*this, &GameFinished::init));
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_hide().connect(sigc::mem_fun(*this, &GameFinished::on_hide));
#endif
} // GameFinished::GameFinished(Base* parent)

/** destructor
 **/
GameFinished::~GameFinished() = default;

/** create all subelements
 **/
void
GameFinished::init()
{
  set_icon(ui->icon);

  set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

  get_content_area()->set_spacing(10);

  get_content_area()->pack_start(*game_summary);

  { // show party points
    show_party_points
      = Gtk::manage(new Gtk::Button(_("Button::show party points")));

    get_content_area()->pack_end(*show_party_points, Gtk::PACK_SHRINK);
    show_party_points->set_halign(Gtk::ALIGN_CENTER);
    show_party_points->signal_clicked().connect(sigc::mem_fun0(ui->table->party_points(), &Gtk::Window::present));
  } // show party points

  { // show game review
    show_game_review
      = Gtk::manage(new Gtk::CheckButton(_("Button::show game review")));

    get_content_area()->pack_end(*show_game_review, Gtk::PACK_SHRINK);
    show_game_review->set_halign(Gtk::ALIGN_CENTER);
    show_game_review->signal_clicked().connect(sigc::mem_fun(*this, &GameFinished::toggle_game_review));
  } // show game review

  { // action area
    create_bug_report_button
      = Gtk::manage(new Gtk::Button(_("Button::create bug report")));
    //add_action_widget(*create_bug_report_button, Gtk::RESPONSE_NONE);
    get_action_area()->add(*create_bug_report_button);
    create_bug_report_button->signal_clicked().connect(sigc::mem_fun(*(ui->bug_report),
                                                                           &BugReport::create_report));

    replay_game_button = Gtk::manage(new Gtk::Button(_("Button::replay game")));
    replay_game_button->set_image_from_icon_name("edit-redo");
    replay_game_button->set_always_show_image();
    add_action_widget(*replay_game_button, Gtk::RESPONSE_CLOSE);
    replay_game_button->signal_clicked().connect(sigc::mem_fun(*this, &GameFinished::replay_game));

    next_game_button = Gtk::manage(new Gtk::Button(_("Button::next game")));
    next_game_button->set_image_from_icon_name("go-next");
    next_game_button->set_always_show_image();
    add_action_widget(*next_game_button, Gtk::RESPONSE_CLOSE);
    next_game_button->signal_clicked().connect(sigc::mem_fun(*this, &GameFinished::hide));

    next_game_button->set_can_default();
    next_game_button->grab_default();
    next_game_button->grab_focus();
  } // action area

  show_all_children();

  ::preferences.signal_changed(::Preferences::Type::show_bug_report_button_in_game_finished_window).connect_back([this](auto value) {update_bug_report_button();}, disconnector_);
  update_bug_report_button();
} // void GameFinished::init()

/** the game has finished:
 ** show the summary and let the player review the game
 **/
void
GameFinished::game_finished()
{
  realize();

  auto const game_summary = ::GameSummary(ui->game());
  if (ui->party().is_replayed_game()) {
    this->game_summary->set_game_summary(game_summary);
  } else {
    this->game_summary->set_gameno(ui->party().gameno());
  }
  if (ui->party().is_last_game()) {
    next_game_button->set_label(_("Button::end of party"));
    next_game_button->set_image_from_icon_name("go-last");
  } else {
    next_game_button->set_label(_("Button::next game"));
    next_game_button->set_image_from_icon_name("go-next");
  }

  present();

  while (   !ui->thrower
         && ui->game().status() == Game::Status::finished
         && is_visible())
    ::ui->wait();

  show_game_review->set_active(false);
  if (ui->party().is_replayed_game())
    this->game_summary->remove_game_summary();

  hide();
} // void GameFinished::game_finished()

/** a setting has changed
 ** show_bug_report_button_in_game_finished_window:
 **   update whether to show the 'create bug report' button
 **
 ** @param    type   the type that has changed
 **/
void
GameFinished::update_bug_report_button()
{
  if (!get_realized())
    return ;

  if (::preferences(::Preferences::Type::show_bug_report_button_in_game_finished_window))
    create_bug_report_button->show();
  else
    create_bug_report_button->hide();
}

/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
GameFinished::name_changed(::Player const& player)
{
  if (!get_realized())
    return ;

  if (game_summary)
    game_summary->name_changed(player);
} // void GameFinished::name_changed(Player const& player)

/** event: hide the window
 **/
void
GameFinished::on_hide()
{
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Gtk::StickyDialog::on_hide();
#endif
  ui->main_quit();
  game_summary->set_gameno(UINT_MAX);
} // void GameFinished::on_hide()

/** toggles the game review
 **/
void
GameFinished::toggle_game_review()
{
  if (show_game_review->get_active()) {
    DEBUG_ASSERTION(!game_review,
                    "GameFinished::toggle_game_review:\n"
                    "  already in game review");
    game_review = std::make_unique<GameReview>(this);
  } else {
    DEBUG_ASSERTION(game_review,
                    "GameFinished::toggle_game_review:\n"
                    "  not in game review");
    game_review.reset();
  }
  ui->table->force_redraw_all();
} // void GameFinished::toggle_game_review()

/** replay the game
 **/
void
GameFinished::replay_game()
{
  ui->party().set_replay_game();
  hide();
} // void GameFinished::replay_game()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
