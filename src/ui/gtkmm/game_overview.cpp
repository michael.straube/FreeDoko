/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "game_overview.h"
#include "game_summary.h"
#include "main_window.h"
#include "ui.h"

#include "../../party/party.h"
#include "../../game/game_summary.h"
#include "../../player/player.h"

#include <gtkmm/label.h>
#include <gtkmm/checkbutton.h>
#include <gdk/gdkkeysyms.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    party_points    the parent widget
 **/
PartyPoints::GameOverview::GameOverview(PartyPoints* const party_points) :
  Base(party_points),
  // gettext: %u = game number
  StickyDialog(_("Game overview: %u", 0U),
               *ui->main_window,
               // *dynamic_cast<PartyPoints*>(parent),
               false)
{
  ui->add_window(*this);

  set_transient_for(*dynamic_cast<PartyPoints*>(parent));

  // the game summary is created here, because else I would have first
  // to show this window and afterwards set the gameno.
  game_summary = Gtk::manage(new GameSummary(this));

  signal_realize().connect(sigc::mem_fun(*this, &GameOverview::init));

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &GameOverview::on_key_press_event));
#endif
} // PartyPoints::GameOverview::GameOverview(PartyPoints* party_points)

/** destructor
 **/
PartyPoints::GameOverview::~GameOverview() = default;

/** create all subelements
 **/
void
PartyPoints::GameOverview::init()
{
  set_icon(ui->icon);
  set_skip_taskbar_hint();

  get_content_area()->set_spacing(10);

  get_content_area()->pack_start(*game_summary);

  add_close_button(*this);

  show_all_children();

  auto const gameno = gameno_;
  gameno_ = UINT_MAX;
  set_gameno(gameno);
} // void GameOverview::init()

/** @return   the gameno
 **/
unsigned
PartyPoints::GameOverview::gameno() const
{
  return gameno_;
} // unsigned GameOverview::gameno() const

/** the game has finished:
 ** show the summary and let the player review the game
 **
 ** @param    gameno   new gameno
 **   (UINT_MAX for no gameno)
 **/
void
PartyPoints::GameOverview::set_gameno(unsigned const gameno)
{
  if (this->gameno() == gameno)
    return ;

  gameno_ = gameno;

  if (!get_realized())
    return ;

  if (gameno == UINT_MAX) {
    hide();
    return ;
  }

  DEBUG_ASSERTION((gameno <= ::party->gameno()),
                  "GameOverview::set_gameno():\n"
                  "  gameno = " << gameno
                  << " > "
                  << ::party->gameno() << " = ::party->gameno()");

  game_summary->set_gameno(gameno);
  // gettext: %u = game number
  set_title(_("Game overview: %u",
                    gameno + 1));
} // void GameOverview::set_gameno(unsigned gameno)

/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
PartyPoints::GameOverview::name_changed(::Player const& player)
{
  if (!get_realized())
    return ;

  if (game_summary)
    game_summary->name_changed(player);
} // void PartyPoints::GameOverview::name_changed(Player const& player)

/** a key has been pressed
 ** C-o: output of the game summary on 'stdout'
 **
 ** @param    key   the key
 **
 ** @return   whether the key was managed
 **/
bool
PartyPoints::GameOverview::on_key_press_event(GdkEventKey* const key)
{
  bool managed = false;

  if ((key->state & ~GDK_SHIFT_MASK) == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_o: // ouput of the game summary
      cout << ui->party().game_summary(gameno());
      managed = true;
      break;
    } // switch (key->keyval)
  } // if (key->state == GDK_CONTROL_MASK)

  return (managed
          || StickyDialog::on_key_press_event(key)
          || ui->key_press(key));
} // bool PartyPoints::GameOverview::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
