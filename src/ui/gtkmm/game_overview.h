/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "party_points.h"
#include "widgets/sticky_dialog.h"
class Player;

namespace Gtk {
class Button;
} // namespace Gtk

namespace UI_GTKMM_NS {
class GameSummary;

/**
 ** the game overwiew
 **/
class PartyPoints::GameOverview : public Base, public Gtk::StickyDialog {
public:
explicit GameOverview(PartyPoints* party_points);
GameOverview(GameOverview const&)            = delete;
GameOverview& operator=(GameOverview const&) = delete;
~GameOverview() override;

unsigned gameno() const;
void set_gameno(unsigned gameno);

void name_changed(::Player const& player);

private:
void init();

bool on_key_press_event(GdkEventKey* key) override;

private:
unsigned gameno_ = UINT_MAX;
GameSummary* game_summary = nullptr;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
