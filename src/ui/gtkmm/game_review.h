/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"

class Game;
class Trick;

#include <gdk/gdk.h>

namespace UI_GTKMM_NS {
class GameFinished;

/**
 ** window for reviewing the game (trickwise)
 **/
class GameReview : public Base {
  public:
    explicit GameReview(GameFinished* game_finished);
    ~GameReview() override;

    unsigned trickno() const;
    ::Trick const& trick() const;
    bool trick_visible() const;

    void previous_trick();
    void following_trick();
    void set_trickno(unsigned trickno);

    bool key_press(GdkEventKey const* key);

  private:
    unsigned trickno_ = 0;

  private:
    GameReview() = delete;
    GameReview(GameReview const&) = delete;
    GameReview& operator=(GameReview const&) = delete;
}; // class GameReview : public Base

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
