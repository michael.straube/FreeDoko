/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "game_summary.h"

#include "ui.h"
#include "bug_report.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game_summary.h"
#include "../../utils/string.h"

#include <gtkmm/button.h>
#include <gtkmm/separator.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gtkmm/treeview.h>
#include <gtkmm/scrolledwindow.h>

namespace UI_GTKMM_NS {

/** Constructor
 **
 ** @param    parent   parent object
 **/
GameSummary::GameSummary(Base* const parent) :
  Base(parent)
{
  signal_realize().connect(sigc::mem_fun(*this, &GameSummary::init));
}

/** destructor
 **/
GameSummary::~GameSummary() = default;

/** create all subelements
 **/
void
GameSummary::init()
{
  set_border_width(1 EM);
  set_spacing(1 EM);

  { // left column
    auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));

    vbox->set_border_width(1 EX);
    vbox->set_spacing(1 EX);

    gamenumber = Gtk::manage(new Gtk::Label(_("Game: %u", 0U)));
    vbox->pack_start(*gamenumber, false, true);

    startplayer = Gtk::manage(new Gtk::Label(_("Startplayer: %s")));
    vbox->pack_start(*startplayer, false, true);

    winner = Gtk::manage(new Gtk::Label(_("Winner: no team")));
    vbox->pack_start(*winner, false, true);


    { // trickpoints
      trickpoints_list
        = Gtk::ListStore::create(trickpoints_model);
      trickpoints_treeview
        = Gtk::manage(new Gtk::TreeView(trickpoints_list));
      trickpoints_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

      trickpoints_treeview->append_column(_("name"),
                                                trickpoints_model.name);
      trickpoints_treeview->append_column(_("trick points"),
                                                trickpoints_model.points);
      trickpoints_treeview->get_column_cell_renderer(1)->set_property("xalign", 0.5);
      trickpoints_treeview->append_column(_("team"),
                                                trickpoints_model.team);
      trickpoints_treeview->append_column(_("game points"),
                                                trickpoints_model.game_points);
      trickpoints_treeview->get_column_cell_renderer(3)->set_property("xalign", 0.5);
      trickpoints_treeview->append_column(_("party points"),
                                                trickpoints_model.party_points);
      trickpoints_treeview->get_column_cell_renderer(4)->set_property("xalign", 0.5);

      vbox->pack_start(*trickpoints_treeview, false, true);

      for (unsigned c = 0;
           c < trickpoints_treeview->get_columns().size();
           ++c) {
        auto column = trickpoints_treeview->get_column(c);
        column->set_cell_data_func(*column->get_first_cell(), sigc::mem_fun(*this, &GameSummary::set_points_cell_color));
      }
    } // trickpoints
    { // bock triggers
      bock_triggers_list
        = Gtk::ListStore::create(bock_triggers_model);
      bock_triggers_treeview
        = Gtk::manage(new Gtk::TreeView(bock_triggers_list));
      bock_triggers_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

      bock_triggers_treeview->append_column(_("bock trigger"),
                                                  bock_triggers_model.name);

      vbox->pack_start(*bock_triggers_treeview, false, true);
    } // bock triggers

    pack_start(*vbox, Gtk::PACK_SHRINK);
  } // left column

  { // right column
    auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));

    vbox->set_border_width(1 EX);
    vbox->set_spacing(1 EX);

    { // game type
      type = Gtk::manage(new Gtk::Label(_("gametype: %t", _(GameType::normal))));

      vbox->pack_start(*type, false, true);
    } // game type


    { // points description
      points_description_list
        = Gtk::ListStore::create(points_description_model);
      points_description_treeview
        = Gtk::manage(new Gtk::TreeView(points_description_list));

      // ToDo: selecting a special point shows the corresponding trick
      // in the game review
      points_description_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);
      points_description_treeview->append_column(_("team"),
                                                       points_description_model.team);
      // ToDo: right-align the sign
      points_description_treeview->append_column(_("player"),
                                                       points_description_model.player);
      points_description_treeview->append_column(_("points"),
                                                       points_description_model.points);
      points_description_treeview->get_column_cell_renderer(2)->set_property("xalign", 0.5);
      points_description_treeview->append_column(_("description"),
                                                       points_description_model.description);

#ifdef POSTPONED
      // ToDo: make the scrolled window show the first four lines of the table
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
      scrolled_window->set_policy(Gtk::POLICY_NEVER,
                                  Gtk::POLICY_AUTOMATIC);
      scrolled_window->add(*(points_description_treeview));
      vbox->pack_start(*scrolled_window, true, true);
#else
      vbox->pack_start(*points_description_treeview, true, true);
      //pack_start(*(points_description_treeview), true, true);
#endif
    } // point description
    //pack_start(*(points_description));

    pack_start(*vbox);
  } // right column

  show_all_children();
  if (gameno != UINT_MAX)
    show();

  update();
} // void GameSummary::init()

/** set the gameno
 **
 ** @param    gameno   the new gameno
 **/
void
GameSummary::set_gameno(unsigned const gameno)
{
  this->gameno = gameno;
  if (gameno == UINT_MAX)
    game_summary = nullptr;
  else
    game_summary = &ui->party().game_summary(gameno);
  update();
}

/** set the game summary
 **
 ** @param    game_summary   new game summary
 **/
void
GameSummary::set_game_summary(::GameSummary const& game_summary)
{
  gameno = UINT_MAX;
  this->game_summary = &game_summary;
  update();
}

/** removes the game summary
 **/
void
GameSummary::remove_game_summary()
{
  game_summary = nullptr;
}

/** update (rewrite) all information
 **
 ** @todo   mark the winner gray
 ** @todo   if there is no 'player_of' don't show the column
 **/
void
GameSummary::update()
{
  if (!get_realized())
    return ;

  if (ui->party().status() == Party::Status::programstart)
    return ;
  if (!game_summary) {
    hide();
    return ;
  }

#ifdef WORKAROUND
  // why?
  show();
#endif

  Party const& party = ui->party(); // cppcheck-suppress shadowVariable

  if (game_summary->is_replayed_game()) {
    gamenumber->set_label(_("Game: %u (replayed)", gameno + 1));
  } else {
    gamenumber->set_label(_("Game: %u", gameno + 1));
  }
  startplayer->set_label(_("Startplayer: %s",
                                 ui->party().players()[game_summary->party_player(game_summary->startplayer())].name()));

  if (game_summary->is_duty_solo()) {
    type->set_label(_("gametype: %t (duty solo)"s,
                            _(game_summary->game_type())));
  } else {
    type->set_label(_("gametype: %t"s,
                            _(game_summary->game_type())));
  }

  if (game_summary->winnerteam() == Team::noteam) {
    winner->set_label(_("Winner: no team"));
  } else {
    // gettext: %t = winnerteam, %u = points of the winnerteam, %t = looserteam, %u = points of the looserteam
    winner->set_label(_("Winner: team %t with %u points (%t: %u points)",

                              _(game_summary->winnerteam()),
                              game_summary->trick_points(game_summary->winnerteam()),
                              _(::opposite(game_summary->winnerteam())),
                              game_summary->trick_points(::opposite(game_summary->winnerteam()))
                             )
                           );
  }

  { // trickpoints
#ifdef WORKAROUND
    //if (!trickpoints_list->children().empty())
    //trickpoints_list->erase(trickpoints_list->get_iter("0"));
#else
    // after clearing the list, no row is added anymore
    trickpoints_list->clear();
#endif

    for (unsigned p = 0;
         p < game_summary->rule()(Rule::Type::number_of_players_in_game);
         p++) {
      auto const& player = party.players()[game_summary->party_player(p)];
      Gtk::ListStore::iterator i;
      if (p < trickpoints_list->children().size())
        i = trickpoints_list->get_iter(String::to_string(p));
      else
        i = trickpoints_list->append();
      auto row = *i;
      row[trickpoints_model.name] = player.name();
      if (game_summary->winnerteam() == Team::noteam)
        row[trickpoints_model.winner] = 0; // no winner
      else if (game_summary->team(player) == game_summary->winnerteam())
        row[trickpoints_model.winner] = 1; // winner
      else
        row[trickpoints_model.winner] = -1; // looser

      row[trickpoints_model.points] = game_summary->trick_points(player);
      row[trickpoints_model.team]
        = _(game_summary->team(player));
      if (game_summary->get_points(player))
        if (game_summary->points(player) > 0)
          row[trickpoints_model.game_points]
            = "+" + String::to_string(game_summary->points(player));
        else
          row[trickpoints_model.game_points]
            = String::to_string(game_summary->points(player));
      else
        row[trickpoints_model.game_points]
          = "";

      row[trickpoints_model.party_points]
        = (gameno == UINT_MAX
           ? party.pointsum(game_summary->party_player(p))
           : party.pointsum_till_game(gameno, game_summary->party_player(p)));
    } // for (p < game_summary->rule(Rule::Type::number_of_players_in_game)

    if (game_summary->is_replayed_game()) {
      trickpoints_treeview->get_column(4)->set_visible(false);
    } else {
      trickpoints_treeview->get_column(4)->set_visible(true);
    }
  } // trickpoints

  { // points description
    if (game_summary->specialpoints().empty()) {
      points_description_treeview->hide();
    } else { // if !(game_summary->specialpoints().empty())
      points_description_treeview->show();

      points_description_list->clear();

      for (const auto & i : game_summary->specialpoints()) {
        auto const& specialpoint
          = i;
        auto row = *points_description_list->append();
        row[points_description_model.team]
          = (  (specialpoint.team == Team::noteam)
             ? ""s
             : _(specialpoint.team));

        if (specialpoint.player_get_no == UINT_MAX) {
          if (specialpoint.player_of_no == UINT_MAX)
            row[points_description_model.player] = string();
          else
            row[points_description_model.player]
              = party.players()[game_summary->party_player(specialpoint.player_of_no)].name();
        } else {
          row[points_description_model.player]
            = party.players()[game_summary->party_player(specialpoint.player_get_no)].name();
        }

        if (   !party.rule()(Rule::Type::knocking)
            && (specialpoint.type == Specialpoint::Type::no120_said) ) {
          if (specialpoint.team == Team::re)
            row[points_description_model.description]
              = _("Specialpoint::re said");
          else if (specialpoint.team == Team::contra)
            row[points_description_model.description]
              = _("Specialpoint::contra said");
        } else {
          row[points_description_model.description]
            = _(specialpoint);
        }


        { // show whether the points are added or subtracted
          bool add = true;
          if ((game_summary->winnerteam() == Team::re)
              || (game_summary->winnerteam() == Team::contra)) {
            add = (i.counting_team
                   == game_summary->winnerteam());
          } else { // if !(game_summary->winnerteam() == re, contra)
            if (specialpoint.counting_team == Team::re)
              add = true;
            else if (specialpoint.counting_team == Team::contra)
              add = false;
          } // if !(game_summary->winnerteam() != re, contra)
          if (add)
            row[points_description_model.points] = "+";
          else
            row[points_description_model.points] = "-";
        } // show whether the points are added or subtracted

        if (    (game_summary->winnerteam()
                 == Team::noteam)
            && (specialpoint.counting_team
                == Team::noteam))
          row[points_description_model.points] = " ";
        if (   (   specialpoint.type == Specialpoint::Type::no120_said
                || specialpoint.type == Specialpoint::Type::announcement_reply)
            && game_summary->rule()(Rule::Type::announcement_re_doubles)
           ) {
          row[points_description_model.points]
            = "* " + String::to_string(specialpoint.value);
        } else if (specialpoint.type == Specialpoint::Type::solo) {
          // a solo
          row[points_description_model.points]
            = "* " + String::to_string(specialpoint.value);
        } else if (specialpoint.type == Specialpoint::Type::bock) {
          // a bock multiplicator
          row[points_description_model.points]
            = "* " + String::to_string(specialpoint.value);
        } else {
          // all other special points
          row[points_description_model.points]
            = (row[points_description_model.points]
               + " " + String::to_string(value(specialpoint.type))
              );

        }
      } // for (i < game_summary->specialpoints().size())
    } // if !(game_summary->specialpoints().empty())

  } // points description

  if (game_summary->is_replayed_game()) {
    bock_triggers_treeview->hide();
  } else if (game_summary->bock_triggers().empty()) {
    bock_triggers_treeview->hide();
  } else {
    bock_triggers_treeview->show();

    bock_triggers_list->clear();

    for (auto i : game_summary->bock_triggers()) {
      auto row = *(bock_triggers_list->append());
      row[bock_triggers_model.name]
        = _(i);
    } // for (i < game_summary->specialpoints().size())
  }
}

/** changes the color of the cell at 'iterator'
 ** mark the winner green and the loosers red
 **
 ** @param    cell_renderer   cell renderer to change
 ** @param    iterator   row
 **/
void
GameSummary::set_points_cell_color(Gtk::CellRenderer* const cell_renderer,
                                   Gtk::TreeModel::iterator const& iterator)
{
  if (!get_realized())
    return ;

  auto const row = *iterator;

  int const winner = row[trickpoints_model.winner];
  if (winner > 0) // winner
    cell_renderer->property_cell_background() = "#AFA"; // helles grün
  else if (winner < 0) // looser
    cell_renderer->property_cell_background() = "#FBB"; // helles rot
  else // no winner
    cell_renderer->property_cell_background() = "white";
} // void GameSummary::set_points_cell_color(Gtk::CellRenderer* cell_renderer, Gtk::TreeModel::iterator iterator)


/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
GameSummary::name_changed(Player const& player)
{
  if (!get_realized())
    return ;

  update();
} // void GameSummary::name_changed(Player const& player)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
