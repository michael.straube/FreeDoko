/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include <gtkmm/box.h>
#include <gtkmm/liststore.h>

class Player;
class GameSummary;

namespace Gtk {
class Label;
class TreeView;
class CellRenderer;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the summary of a game
 **/
class GameSummary : public Base, public Gtk::Box {

  /**
   ** the columns of the trickpoints table
   **/
  struct TrickpointsModel : public Gtk::TreeModel::ColumnRecord {
    TrickpointsModel()
    {
      this->add(this->name);
      this->add(this->winner);
      this->add(this->points);
      this->add(this->team);
      this->add(this->game_points);
      this->add(this->party_points);
    }

    Gtk::TreeModelColumn<Glib::ustring> name;
    Gtk::TreeModelColumn<int> winner;
    Gtk::TreeModelColumn<unsigned> points;
    Gtk::TreeModelColumn<Glib::ustring> team;
    Gtk::TreeModelColumn<Glib::ustring> game_points;
    Gtk::TreeModelColumn<int> party_points;
  }; // struct TrickpointsModel : public Gtk::TreeModel::ColumnRecord

  /**
   ** the columns of the special points table
   **/
  struct PointsDescriptionModel : public Gtk::TreeModel::ColumnRecord {
    PointsDescriptionModel()
    {
      this->add(this->team);
      this->add(this->player);
      this->add(this->points);
      this->add(this->description);
    }

    Gtk::TreeModelColumn<Glib::ustring> team;
    Gtk::TreeModelColumn<Glib::ustring> player;
    Gtk::TreeModelColumn<Glib::ustring> points;
    Gtk::TreeModelColumn<Glib::ustring> description;
  }; // struct PointsDescriptionModel : public Gtk::TreeModel::ColumnRecord

  /**
   ** the columns of the bock triggers table
   **/
  struct BockTriggersModel : public Gtk::TreeModel::ColumnRecord {
    BockTriggersModel()
    { this->add(this->name); }

    Gtk::TreeModelColumn<Glib::ustring> name;
  }; // struct BockTriggersModel : public Gtk::TreeModel::ColumnRecord

  public:
  explicit GameSummary(Base* parent);
  ~GameSummary() override;

  // set the game number
  void set_gameno(unsigned gameno_a);
  // set the game summary
  void set_game_summary(::GameSummary const& game_summary);
  void remove_game_summary();
  // update the window
  void update();

  // the name has changed
  void name_changed(Player const& player);

  private:
  // initialize the window
  void init();
  // set the color of a cell
  void set_points_cell_color(Gtk::CellRenderer* cell_renderer,
                             Gtk::TreeModel::iterator const& iterator);

  private:
  unsigned gameno = UINT_MAX;
  ::GameSummary const* game_summary = nullptr;

  Gtk::Label* gamenumber = nullptr;
  Gtk::Label* startplayer = nullptr;
  Gtk::Label* type = nullptr;
  Gtk::Label* winner = nullptr;
  TrickpointsModel trickpoints_model;
  Glib::RefPtr<Gtk::ListStore> trickpoints_list;
  Gtk::TreeView* trickpoints_treeview = nullptr;
  PointsDescriptionModel points_description_model;
  Glib::RefPtr<Gtk::ListStore> points_description_list;
  Gtk::TreeView* points_description_treeview = nullptr;
  BockTriggersModel bock_triggers_model;
  Glib::RefPtr<Gtk::ListStore> bock_triggers_list;
  Gtk::TreeView* bock_triggers_treeview = nullptr;

  private: // unused
  GameSummary() = delete;
  GameSummary(GameSummary const&) = delete;
  GameSummary& operator=(GameSummary const&) = delete;
}; // class GameSummary : public Base, public Gtk::Box

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
