/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "gametype.h"

#include "../ui.h"
#include "../icons.h"

#include "../../../party/party.h"
#include "../../../party/rule.h"
#include "../../../game/game.h"
#include "../../../player/human/human.h"

#include "../../../misc/preferences.h"
#include "../../../utils/string.h"

#include <gtkmm/image.h>
#include <gtkmm/label.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 ** @param    game     the corresponding game
 **/
GameInfoDialog::Information::GameType::GameType(Base* const parent,
                                                Game const& game) :
  // gettext: %t = game.type()
  GameInfoDialog::Information(parent, _("Window::GameInfo::%t",
                                        _(game.type()))),
  game(&game)
{
  ui->icons->change_managed(icon, game.type());

  swines_announcement = Gtk::manage(new Gtk::Button(_("Menu::Item::swines")));
  pack_end(*swines_announcement);
  swines_announcement->signal_clicked().connect([this](){ announce_swines(); });

  init();
}

/** destructor
 **/
GameInfoDialog::Information::GameType::~GameType() = default;

/** update all texts
 **/
void GameInfoDialog::Information::GameType::update_texts()
{
  if (!ui->party().in_game())
    return ;

  if (   game->status() != Game::Status::reservation
      && game->status() != Game::Status::redistribute
      && game->status() != Game::Status::start)
    if (game->type() != ::GameType::normal)
      return ;

  switch (game->type()) {
  case ::GameType::normal:
    label->set_label(_(game->type()));
    break;
  case ::GameType::thrown_nines:
    label->set_label(_("%s has %u nines.",
                       game->players().soloplayer().name(),
                       game->players().soloplayer().hand().count(Card::nine))
                    );
    break;
  case ::GameType::thrown_kings:
    label->set_label(_("%s has %u kings.",
                       game->players().soloplayer().name(),
                       game->players().soloplayer().hand().count(Card::king))
                    );
    break;
  case ::GameType::thrown_nines_and_kings:
    label->set_label(_("%s has %u nines and kings.",
                       game->players().soloplayer().name(),
                       game->players().soloplayer().hand().count(Card::nine) + game->players().soloplayer().hand().count(Card::king))
                    );
    break;
  case ::GameType::thrown_richness:
    label->set_label(_("%s has %u richness.",
                       game->players().soloplayer().name(),
                       game->players().soloplayer().hand().points())
                    );
    break;
  case ::GameType::fox_highest_trump:
    // gettext: %t = gametype (fox highest trump)
    label->set_label(_("%s has %t.",
                       game->players().soloplayer().name(),
                       _(game->type()))
                    );
    break;
  case ::GameType::redistribute:
    label->set_label(_("%s forces a redistribution.",
                       game->players().soloplayer().name())
                    );
    break;
  case ::GameType::poverty: {
    // error
    // gettext: %t = gametype (poverty)
    string text = _("%s has a %t.",
                    game->players().soloplayer().name(),
                    _(game->type()));
    if (game->status() != Game::Status::redistribute) {
      for (auto const& player : game->players()) {
        if (   (player != game->players().soloplayer())
            && (player.team() == Team::re)) {
#ifdef POSTPONED
          text +=  ("        \n        "
                    + _("Partner is %s.",
                        player.name()));
#endif
          if (game->players().soloplayer().hand().count(Card::trump) == 1) {
            text +=  ("        \n        "
                      + _("%s has returned %u trump.",
                          player.name(),
                          game->players().soloplayer().hand().count(Card::trump)
                         ));
          } else {
            text +=  ("        \n        "
                      + _("%s has returned %u trumps.",
                          player.name(),
                          game->players().soloplayer().hand().count(Card::trump)
                         ));
          }

        } // if (player is the partner)
      } // for (player)
    } // if (game().status() != game_redistribute)
    label->set_label(text);
  } break;
  case ::GameType::marriage:
  case ::GameType::marriage_solo:
    // gettext: %1s = marriage soloplayer, %t = type (marriage), %t = marriage selector
    label->set_label(_("%s has a %t: %t.",
                       game->players().soloplayer().name(),
                       _(game->type()),
                       _(game->marriage().selector()))
                    );
    break;
  case ::GameType::marriage_silent:
    // cannot happen
    break;
  case ::GameType::solo_meatless:
  case ::GameType::solo_jack:
  case ::GameType::solo_queen:
  case ::GameType::solo_king:
  case ::GameType::solo_queen_jack:
  case ::GameType::solo_king_jack:
  case ::GameType::solo_king_queen:
  case ::GameType::solo_koehler:
  case ::GameType::solo_club:
  case ::GameType::solo_spade:
  case ::GameType::solo_heart:
  case ::GameType::solo_diamond:
    // gettext: %s = player, %s = gametyp
    label->set_label(_("%s plays a %t.",
                       game->players().soloplayer().name(),
                       _(game->type()))
                    );
    break;
  } // switch (game->type())

  if (   !::preferences(::Preferences::Type::announce_swines_automatically)
      && game->rule()(Rule::Type::swines_announcement_begin)
      && game->players().count_humans() == 1
      && game->swines().swines_announcement_valid(game->players().human())) {
    swines_announcement->show();
  } else {
    swines_announcement->hide();
  }
}

/** @return   whether the information is blocking the gameplay
 **/
bool GameInfoDialog::Information::GameType::is_blocking() const
{
  return (   Information::is_blocking()
          || game->status() == Game::Status::redistribute);
}

void GameInfoDialog::Information::GameType::announce_swines()
{
  if (game->players().count_humans() == 1)
    ui->game().swines().swines_announce(ui->game().players().human());
  swines_announcement->hide();
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
