/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "../gameinfo.h"

class Game;

namespace UI_GTKMM_NS {

/**
 ** gametype information
 **/
class GameInfoDialog::Information::GameType : public GameInfoDialog::Information {
  public:
    GameType(Base* parent, Game const& game);
    ~GameType() override;

  private:
    // update the text
    void update_texts() override;

    // whether the information is blocking the gameplay
    bool is_blocking() const override;

    void announce_swines();

  private:
    // the corresponding game
    Game const* game;

    Gtk::Button* swines_announcement = nullptr;

  private:
    GameType() = delete;
    GameType(GameType const&) = delete;
    GameType& operator=(GameType const&) = delete;
}; // class GameInfoDialog::Information::GameType : public GameInfoDialog::Information

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
