/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "marriage.h"

#include "../ui.h"
#include "../icons.h"

#include "../../../game/game.h"
#include "../../../player/player.h"

#include <gtkmm/image.h>
#include <gtkmm/label.h>

namespace UI_GTKMM_NS {

GameInfoDialog::Information::Marriage::Marriage(Base* const parent,
                                                Player const& bridegroom,
                                                Player const& bride) :
  GameInfoDialog::Information(parent, _("Window::GameInfo::marriage")),
  bridegroom(&bridegroom),
  bride(&bride)
{
  init();
  ui->icons->change_managed(icon, bridegroom.game().marriage().selector());
}


GameInfoDialog::Information::Marriage::~Marriage() = default;


void GameInfoDialog::Information::Marriage::update_texts()
{
  if (*bridegroom == *bride)
    label->set_label(_("%s has found no bride.",
                             bridegroom->name()
                            ));
  else
    label->set_label(_("%s marries %s.",
                             bridegroom->name(),
                             bride->name()
                            ));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
