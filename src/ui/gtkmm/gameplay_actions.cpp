/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "gameplay_actions.h"

#include "ui.h"

#include "../../party/party.h"

#include "../../utils/string.h"

#include <gtkmm/treeview.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   parent object
 **/
GameplayActions::GameplayActions(Base* const parent) :
  Base(parent)
{
  init();
}

/** destruktor
 **/
GameplayActions::~GameplayActions() = default;

/** create all subelements
 **/
void
GameplayActions::init()
{
  list = Gtk::ListStore::create(model);
  set_model(list);

  get_selection()->set_mode(Gtk::SELECTION_NONE);

  append_column(_("type"), model.type);
  append_column(_("player"), model.player);
  get_column_cell_renderer(1)->set_property("xalign", 0.5);
  append_column(_("data"), model.data);

  for (auto c : get_columns()) {
    c->set_cell_data_func(*c->get_first_cell(),
                          sigc::mem_fun(*this, &GameplayActions::set_cell_color));
  }
  show_all_children();

  if (get_realized())
    update();
  else
    signal_realize().connect(sigc::mem_fun(*this, &GameplayActions::update));
} // void GameplayActions::init()

/** update (rewrite) all information
 **
 ** @todo   mark the winner gray
 ** @todo   if there is no 'player_of' don't show the column
 **/
void
GameplayActions::update()
{
  if (!get_realized())
    return ;

  list->clear();

  if (!actions)
    return;

  unsigned trickno = 0;
  for (unsigned a = 0; a < actions->size(); ++a) {
    auto const& action = *((*actions)[a]);
    auto row = *list->append();
    row[model.type] = gettext(action.type());
    unsigned const player = ::GameplayActions::player(action);
    if (player == UINT_MAX)
      row[model.player] = "";
    else
#ifdef WORKAROUND
      // in 'party_open()' 'BugReportReplay' first gets the party and then the ui
      row[model.player]
        = ::party->players()[player].name();
#else
    row[model.player]
      = ui->party().players()[player].name();
#endif
    if (action.type() == ::GameplayActions::Type::trick_full) {
      trickno += 1;
      row[model.data]
        = (String::to_string(trickno) + ": "
           + action.data_translation());
    } else {
      row[model.data] = action.data_translation();
    }
    row[model.action] = &action;
    row[model.no] = a;
  } // for (a < actsion->size())
} // void GameplayActions::update()

/** sets the actions
 **
 ** @param    actions   actions to display
 **/
void
GameplayActions::set_actions(vector<unique_ptr<::GameplayAction>> const& actions)
{
  this->actions = &actions;
  update();
} // void GameplayActions::set_actions(vector<unique_ptr<::GameplayAction>> actions)

/** sets the discrepancy
 **
 ** @param    discrepancies   discrepancies of the actions to the gameplay
 **/
void
GameplayActions::set_discrepancies(vector<::GameplayActions::Discrepancy>
                                   const& discrepancies)
{
  this->discrepancies = &discrepancies;
  update();
} // void GameplayActions::set_discrepancies(vector<GameplayAction::Discrepancy> discrepancies)

/** removes the actions
 **/
void
GameplayActions::remove_actions()
{
  actions = nullptr;
  discrepancies = nullptr;
  update();
} // void GameplayActions::remove_actions()

/** sets the number of the current action
 **
 ** @param    current_action_no   new number
 **/
void
GameplayActions::set_current_action_no(unsigned const current_action_no)
{
  this->current_action_no = current_action_no;

  if (!get_realized())
    return ;

  // the redrawing is needed, so that the colors are updated
  queue_draw();

  // just scroll to the current row (I do not know a simplier way)
  if (current_action_no < get_model()->children().size())
    scroll_to_row(get_model()->get_path(*(get_model()->get_iter(String::to_string(current_action_no)))));
} // void GameplayActions::set_current_action_no(unsigned current_action_no)

/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
GameplayActions::name_changed(Player const& player)
{
  if (!get_realized())
    return ;

  update();
} // void GameplayActions::name_changed(Player player)

/** changes the color of the cell at 'iterator'
 **
 ** @param    cell_renderer   cell renderer to change
 ** @param    iterator   row
 **/
void
GameplayActions::set_cell_color(Gtk::CellRenderer* const cell_renderer,
                                Gtk::TreeModel::iterator const& iterator)
{
  auto const row = *iterator;
  GameplayAction const* const action = row[model.action];
  if (!action)
    return ;
  auto const action_no = row[model.no];

  pair<string, string> colors;
  auto const discrepancy = (!discrepancies
                            ? ::GameplayActions::Discrepancy::none
                            : action_no < discrepancies->size()
                            ?  (*discrepancies)[action_no]
                            : ::GameplayActions::Discrepancy::future);
  colors = GameplayActions::colors(action->type(), discrepancy);

  cell_renderer->set_property("foreground", colors.first);
  cell_renderer->set_property("background", colors.second);
  cell_renderer->set_property("cell_background", colors.second);
} // void GameplayActions::set_cell_color(Gtk::CellRenderer* cell_renderer, Gtk::TreeModel::iterator iterator)

/** -> result
 **
 ** @param    action_type   the action type
 ** @param    discrepancy   the discrepancy to the action
 **
 ** @return   pair of colors (foreground, background)
 **/
pair<string, string>
GameplayActions::colors(::GameplayActions::Type const action_type,
                        ::GameplayActions::Discrepancy const discrepancy)
{
  switch (action_type) {
  case ::GameplayActions::Type::poverty_shift:
  case ::GameplayActions::Type::poverty_accepted:
  case ::GameplayActions::Type::poverty_returned:
  case ::GameplayActions::Type::poverty_denied:
  case ::GameplayActions::Type::poverty_denied_by_all:
  case ::GameplayActions::Type::announcement:
  case ::GameplayActions::Type::swines:
  case ::GameplayActions::Type::hyperswines:
  case ::GameplayActions::Type::marriage:
    switch (discrepancy) {
    case ::GameplayActions::Discrepancy::future:
      return std::make_pair("black", "white");
    case ::GameplayActions::Discrepancy::none:
      return std::make_pair("black", "lightgreen");
    case ::GameplayActions::Discrepancy::skipped:
    case ::GameplayActions::Discrepancy::player:
    case ::GameplayActions::Discrepancy::card:
    case ::GameplayActions::Discrepancy::other:
      return std::make_pair("black", "red");
    } // switch (discrepancy)
    break;

  case ::GameplayActions::Type::reservation:
  case ::GameplayActions::Type::trick_open:
  case ::GameplayActions::Type::trick_full:
  case ::GameplayActions::Type::trick_taken:
    switch (discrepancy) {
    case ::GameplayActions::Discrepancy::future:
      return std::make_pair("black", "lightgray");
    case ::GameplayActions::Discrepancy::none:
      return std::make_pair("white", "darkgreen");
    case ::GameplayActions::Discrepancy::skipped:
    case ::GameplayActions::Discrepancy::player:
    case ::GameplayActions::Discrepancy::card:
    case ::GameplayActions::Discrepancy::other:
      return std::make_pair("white", "darkred");
    } // switch (discrepancy)
    break;

  case ::GameplayActions::Type::card_played:
    switch (discrepancy) {
    case ::GameplayActions::Discrepancy::future:
      return std::make_pair("black", "white");
    case ::GameplayActions::Discrepancy::none:
      return std::make_pair("black", "green");
    case ::GameplayActions::Discrepancy::skipped:
    case ::GameplayActions::Discrepancy::player:
    case ::GameplayActions::Discrepancy::card:
    case ::GameplayActions::Discrepancy::other:
      return std::make_pair("black", "red");
    } // switch (discrepancy)
    break;

  case ::GameplayActions::Type::check:
    return std::make_pair("red", "blue");
  case ::GameplayActions::Type::print:
    return std::make_pair("black", "blue");
  case ::GameplayActions::Type::quit:
    return std::make_pair("red", "white");
  }; // switch (action_type)

  return std::make_pair("black", "white");
} // static pair<string, string> GameplayActions::colors(GameplayActions::Type action_type, GameplayActions::Discrepancy discrepancy)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
