/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"

#include "../../game/gameplay_action.h"
#include "../../player/player.h"

#include <gtkmm/liststore.h>
#include <gtkmm/treeview.h>

namespace UI_GTKMM_NS {

/**
 ** A list store containing the gameplay actions from a vector
 **/
class GameplayActions : public Base, public Gtk::TreeView {
  public:
    // the colors for the action type
    static pair<string, string>
      colors(::GameplayActions::Type action_type,
             ::GameplayActions::Discrepancy discrepancy);

  public:
    /**
     ** the columns of game actions
     **/
    struct GameplayActionModel : public Gtk::TreeModel::ColumnRecord {
      GameplayActionModel()
      {
        this->add(this->type);
        this->add(this->player);
        this->add(this->data);
        this->add(this->action);
        this->add(this->no);
      }

      Gtk::TreeModelColumn<Glib::ustring> type;
      Gtk::TreeModelColumn<Glib::ustring> player;
      Gtk::TreeModelColumn<Glib::ustring> data;
      Gtk::TreeModelColumn<GameplayAction const*> action;
      Gtk::TreeModelColumn<unsigned> no;
    }; // struct GameplayActionModel : public Gtk::TreeModel::ColumnRecord

  public:
    explicit GameplayActions(Base* parent);
    ~GameplayActions() override;

    // update the gameplay actions
    void update();

    // set the actions
    void set_actions(vector<unique_ptr<::GameplayAction>> const& actions);
    // show the discrepancies
    void set_discrepancies(vector<::GameplayActions::Discrepancy> const&
                           discrepancies);
    // remove all actions
    void remove_actions();
    // set the current action
    void set_current_action_no(unsigned current_action_no);

    // the name of the player has changed
    void name_changed(Player const& player);

    // set the cell color
    void set_cell_color(Gtk::CellRenderer* cell_renderer,
                        Gtk::TreeModel::iterator const& row);
  private:
    // initialize the list
    void init();

  private:
    // the actions
    vector<unique_ptr<::GameplayAction>> const* actions = nullptr;
    // the discrepancy
    vector<::GameplayActions::Discrepancy> const* discrepancies = nullptr;
    // the number of the current action
    unsigned current_action_no = 0;

  public:
    // the model
    GameplayActionModel model;
  private:
    // the list
    Glib::RefPtr<Gtk::ListStore> list;
}; // class GameplayActions

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
