/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include <cstring>
#include "help.h"
#include "../help.h"

#include "ui.h"

#include <gtkmm/image.h>
#include <gtkmm/button.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
Help::Help(Base* const parent) :
  Base(parent),
  Gtk::MessageDialog("FreeDoko – " + _("Window::help"),
                     false,
                     Gtk::MESSAGE_WARNING, Gtk::BUTTONS_NONE,
                     false
                    )
{
  ui->add_window(*this);
  init();
} // Help::Help(Base* parent)

/** destruktor
 **/
Help::~Help() = default;

/** create all subelements
 **/
void
Help::init()
{
  set_icon(ui->icon);

  set_skip_taskbar_hint();

  { // buttons
    add_close_button(*this);

    auto online_button = Gtk::manage(new Gtk::Button(_("online help")));
    online_button->set_image_from_icon_name("help-contents");
    online_button->set_always_show_image();
    add_action_widget(*online_button, Gtk::RESPONSE_NONE);
    online_button->show_all();
    online_button->set_can_default();
    online_button->grab_default();
    online_button->signal_clicked().connect(sigc::mem_fun(*this, &Help::show_online_help));
  } // buttons

  set_message(_("Help::not found"));

  set_default_size(ui->logo->get_width(),
                         ui->logo->get_height() * 2);
#ifdef POSTPONED
  get_window()->set_decorations(Gdk::DECOR_BORDER
                                      | Gdk::DECOR_RESIZEH
                                      | Gdk::DECOR_TITLE
                                      | Gdk::DECOR_MENU);
#endif

  show_all_children();
} // void Help::init()

/** show the homepage
 **/
void
Help::show_homepage()
{
  show_internet_page(::Help::homepage_location());
} // void Help::show_homepage()

/** show the homepage
 **/
void
Help::show_cardsets_download()
{
  show_internet_page(::Help::cardsets_download_location());
} // void Help::show_cardsets_download()

/** show the manual
 **/
void
Help::show_manual()
{
  show_help(::Help::manual_location());
} // void Help::show_manual()

/** show the help at the location
 **
 ** @param    location   location of the help
 **/
void
Help::show_help(string const& location)
{
  if (   (location[0] == '/')
      || (string(location, 0, strlen("./")) == "./")
      || (string(location, 0, strlen("file://")) == "file://") ) {
    // local adress
    ::Help::show(location);
    return ;
  }

  if (online_accepted_) {
    ::Help::show(location);
    return ;
  }

  set_location(location);

  present();
} // void Help::show_help(string location)

/** show the internet page at the location
 **
 ** @param    location   internet address
 **/
void
Help::show_internet_page(string const& location)
{
  ::Help::show(location);
} // void Help::show_internet_page(string location)

/** show the help at the online location
 **/
void
Help::show_online_help()
{
  online_accepted_ = true;
  ::Help::show(location_);
  hide();
} // void Help::show_online_help()

/** sets the location
 **
 ** @param    location   online location of the help
 **/
void
Help::set_location(string const& location)
{
  location_ = location;
  // gettext: %s = location
  set_secondary_text(_("Help::not found: %s", location));
} // void Help::set_location(string location)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
