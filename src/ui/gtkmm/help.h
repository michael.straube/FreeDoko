/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include <gtkmm/messagedialog.h>

namespace UI_GTKMM_NS {

/**
 ** the about dialog
 **/
class Help : public Base, public Gtk::MessageDialog {
  public:
    explicit Help(Base* parent);
    ~Help() override;

    // show the homepage
    void show_homepage();
    // show the homepage for loading cardsets
    void show_cardsets_download();
    // show the manual
    void show_manual();

    // show the help at the location
    void show_help(string const& location);
    // show the internet page at the location
    void show_internet_page(string const& location);

  private:
    // initialize the window
    void init();
    // sets the location
    void set_location(string const& location);

    // show the help at the online location
    void show_online_help();

  private:
    // the location
    string location_;
    // whether 'online' is accepted
    bool online_accepted_ = false;

  private: // unused
    Help() = delete;
    Help(Help const&) = delete;
    Help& operator=(Help const&) = delete;
}; // class Help : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
