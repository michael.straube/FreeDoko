/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "last_trick.h"
#include "table.h"
#include "trick_summary.h"

#include "ui.h"
#include "main_window.h"

#include "../../game/game.h"
#include "../../party/rule.h"
#include "../../card/trick.h"
#include "../../player/player.h"

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gdk/gdkkeysyms.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table   the table
 **/
LastTrick::LastTrick(Table* const table) :
  Base(table),
  Gtk::StickyDialog("FreeDoko – " + _("Window::last trick"))
{
  signal_realize().connect(sigc::mem_fun(*this, &LastTrick::init));
} // LastTrick::LastTrick(Table* table)

/** destructor
 **/
LastTrick::~LastTrick() = default;

/** create all subelements
 **/
void
LastTrick::init()
{
  add_close_button(*this);

  get_content_area()->set_spacing(1 EX);

  navigation_container = Gtk::manage(new Gtk::Box());
  previous_trick_button = Gtk::manage(new Gtk::Button());
  following_trick_button = Gtk::manage(new Gtk::Button());
  {
    auto previous_image = Gtk::manage(new Gtk::Image());
    previous_image->set_from_icon_name("go-previous", Gtk::ICON_SIZE_BUTTON);
    previous_trick_button->add(*previous_image);
  }
  {
    auto next_image = Gtk::manage(new Gtk::Image());
    next_image->set_from_icon_name("go-next", Gtk::ICON_SIZE_BUTTON);
    following_trick_button->add(*next_image);
  }
  previous_trick_button->set_halign(Gtk::ALIGN_START);
  navigation_container->pack_start(*previous_trick_button,
                                         Gtk::PACK_SHRINK);
  previous_trick_button->set_halign(Gtk::ALIGN_END);
  navigation_container->pack_end(*following_trick_button,
                                       Gtk::PACK_SHRINK);


  previous_trick_button->signal_clicked().connect(sigc::mem_fun(*this, &LastTrick::previous_trick));
  following_trick_button->signal_clicked().connect(sigc::mem_fun(*this, &LastTrick::following_trick));

  get_content_area()->pack_start(*navigation_container, Gtk::PACK_SHRINK);

  trick_summary = make_unique<TrickSummary>(this);
  //trick_summary->set_trick(*last_trick);

  get_content_area()->pack_start(*trick_summary);

  show_all_children();
  set_trickno(trickno());
  trick_summary->update();

  signal_key_press_event().connect(sigc::mem_fun(*this,
                                                       &LastTrick::key_press));
} // void LastTrick::init()

/** show the last trick
 **
 ** @param    last_trick   the last trick
 **/
void
LastTrick::show_last_trick(::Trick const& last_trick)
{
  last_trick_no = last_trick.no();
  auto const& game = last_trick.game();
  if (game.tricks().tricks_in_trickpiles() == 0)
    return ;

  realize();

  set_trickno(last_trick_no);
  present();
} // void LastTrick::show_last_trick(::Trick last_trick)

/** hide the window
 **/
void
LastTrick::close()
{
  hide();
} // void LastTrick::close()

/** @return   number of the current trick
 **/
unsigned
LastTrick::trickno() const
{
  return last_trick_no;
} // unsigned LastTrick::trickno() const

/** set the trickno
 **
 ** @param    trickno   new trickno
 **/
void
LastTrick::set_trickno(unsigned const trickno)
{
  last_trick_no = trickno;
  auto const& game = ui->game();
  auto const& trick = game.tricks().trick(trickno);
  auto const tricks_in_trickpiles = game.tricks().tricks_in_trickpiles();
  trick_summary->set_trick(trick);
  previous_trick_button->set_sensitive(trickno > 0);
  following_trick_button->set_sensitive(trickno
                                              < tricks_in_trickpiles - 1);

  if (this->trickno() == tricks_in_trickpiles - 1)
    set_title(_("Window::last trick"));
  else
    set_title(_("Window::%u. trick", trickno + 1));
  if (!::preferences(::Preferences::Type::show_all_tricks))
    navigation_container->hide();
  else if (tricks_in_trickpiles >= 2)
    navigation_container->show();
  else
    navigation_container->hide();

#ifdef WINDOWS
#ifdef WORKAROUND
  // Sometimes the window is not updated
  queue_draw();
#endif
#endif
} // void LastTrick::set_trickno(unsigned const trickno)

/** show the previous trickno
 **/
void
LastTrick::previous_trick()
{
  if (!::preferences(::Preferences::Type::show_all_tricks))
    return ;
  if (trickno() > 0)
    set_trickno(trickno() - 1);
} // void LastTrick::previous_trick()

/** show the following trickno
 **/
void
LastTrick::following_trick()
{
  if (!::preferences(::Preferences::Type::show_all_tricks))
    return ;
  auto const& game = ui->game();
  auto const trickno = last_trick_no;
  if (trickno < game.tricks().tricks_in_trickpiles() - 1)
    set_trickno(trickno + 1);
} // void LastTrick::following_trick()

/** the window is hidden
 **/
void
LastTrick::on_hide()
{
  Gtk::StickyDialog::on_hide();
  last_trick_no = UINT_MAX;
  trick_summary->remove_trick();
} // void LastTrick::on_hide()

/** the name of 'player' has changed
 **
 ** @param    player   the player whose name has changed
 **/
void
LastTrick::name_changed(::Player const& player)
{
  if (last_trick_no != UINT_MAX)
    trick_summary->update();
} // void LastTrick::name_changed(Player const& player)

/** a key has been pressed
 **
 ** @param    key   the key
 **
 ** @return   from 'ui->key_press(key)'
 **
 ** @bug   signal does not get here for '->', '<-' and '\<Esc\>'
 **/
bool
LastTrick::key_press(GdkEventKey* const key)
{
  // whether the key was accepted
  bool accepted = false;

  if ((key->state & GDK_SHIFT_MASK) == 0) {
    switch (key->keyval) {
    case GDK_KEY_Left: // show previous trick
      previous_trick();
      accepted = true;
      break;
    case GDK_KEY_Right: // show following trick
      following_trick();
      accepted = true;
      break;
    case GDK_KEY_Escape: // close the window
      hide();
      accepted = true;
      break;
    } // switch (key->keyval)
  } // if (key->state == 0)

  if (accepted)
    return false;

  return ui->key_press(key);
} // bool LastTrick::key_press(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
