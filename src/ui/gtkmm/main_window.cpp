/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "main_window.h"
#include "menu.h"

#include "ui.h"
#include "cards.h"

#include "first_run.h"
#include "program_updated.h"
#include "bug_report.h"

#include "../../versions.h"
#include "../../misc/preferences.h"

#include <gtkmm/box.h>
#include <gtkmm/grid.h>
#include <gtkmm/entry.h>
namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   parent object (the ui)
 **/
MainWindow::MainWindow(Base* const parent) :
  Base(parent),
  menu(std::make_unique<Menu>(this)),
  container(Gtk::manage(new Gtk::HBox()))
{
  set_icon(ui->icon);

  set_minimal_size();

  set_title(program_name + "   "s
                  + to_string(*::version));

  auto box = Gtk::manage(new Gtk::VBox(false, 0));
  add(*box);
  box->pack_start(*menu, false, true);

  box->add(*container);
  container->show();
  box->show();

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_window_state_event().connect(sigc::mem_fun(*this, &MainWindow::on_window_state_event));
  signal_key_press_event().connect(sigc::mem_fun(*this, &MainWindow::on_key_press_event));
  signal_show().connect(sigc::mem_fun(*this, &MainWindow::on_show));
  signal_hide().connect(sigc::mem_fun(*this, &MainWindow::on_hide));
#endif
  signal_key_press_event().connect(sigc::mem_fun(*this, &MainWindow::on_key_press_event));

  ui->bug_report->set_dnd_destination(*this);
} // MainWindow::MainWindow(Base* const parent)

/** destruktor
 **/
MainWindow::~MainWindow() = default;

/** start a new party
 **/
void
MainWindow::start_new_party_event()
{
  ui->start_new_party();
} // void MainWindow::start_new_party_event()

/** end a new party
 **/
void
MainWindow::end_party_event()
{
  ui->end_party();
} // void MainWindow::end_party_event()

/** quits the program.
 ** 'UI_GTKMM' cannot be used by the signals sinc it is no Gtk object. So the MainWindow is used instead
 **/
void
MainWindow::quit_program_event()
{
  ui->quit_program();
} // void MainWindow::quit_program_event()

/** generates an error
 **/
void
MainWindow::generate_error_event()
{
  //ui->generate_error(_("BugReport::generated error"));
  ui->error(_("BugReport::generated error"), backtrace_string());
}

/** a key has been pressed
 **
 ** @param    key   the key
 **
 ** @return   from 'ui->key_press(key)'
 **/
bool
MainWindow::on_key_press_event(GdkEventKey* const key)
{
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  if (Gtk::Window::on_key_press_event(key))
    return true;
#endif
  return ui->key_press(key);
} // bool MainWindow::on_key_press_event(GdkEventKey* key)

/** the window is shown -- make sure that the start windows stay on top
 **/
void
MainWindow::on_show()
{
  if (::preferences(::Preferences::Type::ui_main_window_width)) {
    set_default_size(::preferences(::Preferences::Type::ui_main_window_width),
                           ::preferences(::Preferences::Type::ui_main_window_height));
    move(::preferences(::Preferences::Type::ui_main_window_pos_x),
               ::preferences(::Preferences::Type::ui_main_window_pos_y));
  }
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Gtk::Window::on_show();
#endif
  if (::preferences(::Preferences::Type::ui_main_window_width)) {
    move(::preferences(::Preferences::Type::ui_main_window_pos_x),
               ::preferences(::Preferences::Type::ui_main_window_pos_y));
  }
  if (::preferences(::Preferences::Type::ui_main_window_fullscreen))
    fullscreen();

  if (ui->first_run_window)
    ui->first_run_window->raise();
  if (ui->program_updated_window)
    ui->program_updated_window->raise();
} // void MainWindow::on_show()

/** the window is hidden -- quit the program
 **/
void
MainWindow::on_hide()
{
  ui->quit_program();
} // void MainWindow::on_hide()

/** sets the minimal size of the window (depends on the size of the cards)
 **/
void
MainWindow::set_minimal_size()
{
  Gdk::Geometry geometry;
  geometry.min_width = 5 * ui->cards->height();
  geometry.min_height = 4 * ui->cards->height();

  set_geometry_hints(*this, geometry, Gdk::HINT_MIN_SIZE);
} // void MainWindow::set_minimal_size()


/** a change in the window state (p.e. iconify)
 ** track this event in order to iconify/deiconify all windows with the main
 ** window
 **
 ** @param    state    the (new) state
 **
 ** @return   false (let the Gtk::Window class handle the iconification)
 **/
bool
MainWindow::on_window_state_event(GdkEventWindowState* const state)
{
  if (state->new_window_state & GDK_WINDOW_STATE_ICONIFIED)
    ui->iconify_all_windows();
  else
    ui->deiconify_all_windows();

  ::preferences.set(::Preferences::Type::ui_main_window_fullscreen,
                (get_window()->get_state() & Gdk::WINDOW_STATE_FULLSCREEN));

  Gtk::Window::on_window_state_event(state);

  return false;
} // bool MainWindow::on_window_state_event(GdkEventWindowState* state)

/** a change in the window size/position
 ** track this event in order to iconify/deiconify all windows with the main
 ** window
 **
 ** @param    state    the (new) state
 **
 ** @return   false;
 **/
bool
MainWindow::on_configure_event(GdkEventConfigure* const event)
{
  if (!(get_window()->get_state() & Gdk::WINDOW_STATE_FULLSCREEN)) {
    // Im Vollbild geben get_size() und get_position() die Werte vom Vollbild-Fenster zurück, daher werden die Werte nicht im Vollbild-Modus gespeichert.
    int width = 0, height = 0;
    int pos_x = 0, pos_y = 0;
    get_size(width, height);
    get_position(pos_x, pos_y);
    ::preferences.set(::Preferences::Type::ui_main_window_pos_x, pos_x);
    ::preferences.set(::Preferences::Type::ui_main_window_pos_y, pos_y);
    ::preferences.set(::Preferences::Type::ui_main_window_width, width);
    ::preferences.set(::Preferences::Type::ui_main_window_height, height);
  }
  Gtk::Window::on_configure_event(event);
  return false;
} // bool MainWindow::on_configure_event(GdkEventConfigure* event)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
