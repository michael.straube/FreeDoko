/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef USE_UI_GTKMM

#include "base.h"
#include <gtkmm/window.h>
namespace Gtk {
class Box;
} // namespace Gtk

namespace UI_GTKMM_NS {
class Menu;

/**
 ** the main window
 **/
class MainWindow : public Base, public Gtk::Window {
  public:
    explicit MainWindow(Base* parent);
    MainWindow() = delete;
    MainWindow(MainWindow const&) = delete;
    MainWindow& operator=(MainWindow const&) = delete;
    ~MainWindow() override;

    // a new party shall be started
    void start_new_party_event();
    // the party shall be ended
    void end_party_event();
    // the program shall be quit
    void quit_program_event();
    // en error shall be generated
    void generate_error_event();

    // set the window to its minimal size
    void set_minimal_size();

    // a key press
    bool on_key_press_event(GdkEventKey* key) override;

  private:
    void on_show() override;
    void on_hide() override;

    // this is called p.e. by iconfy/deiconify
    bool on_window_state_event(GdkEventWindowState* state) override;
    // p.e. move, resize
    bool on_configure_event(GdkEventConfigure* state) override;

  public:
    std::unique_ptr<Menu> menu;
    Gtk::Container* container = nullptr;
}; // class MainWindow : public Base, public Gtk::Window

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
