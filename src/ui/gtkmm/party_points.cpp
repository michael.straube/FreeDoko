/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "party_points.h"
#include "game_overview.h"
#include "party_points.graph.h"

#include "ui.h"
#include "cards.h"
#include "icons.h"
#include "bug_report.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game_summary.h"
#include "../../misc/preferences.h"

#include <gtkmm/notebook.h>
#include <gtkmm/treeview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gdk/gdkkeysyms.h>


#define OFFSET_PLAYER	1 // NOLINT(cppcoreguidelines-macro-usage)
#ifdef POSTPONED
#define OFFSET_GAMEPOINTS	(OFFSET_PLAYER + party().players().size()) // NOLINT(cppcoreguidelines-macro-usage)
#else
#define OFFSET_GAMEPOINTS	(OFFSET_PLAYER + ::party->players().size()) // NOLINT(cppcoreguidelines-macro-usage)
#endif
#define OFFSET_BOCK_MULTIPLIER	(OFFSET_GAMEPOINTS + 1) // NOLINT(cppcoreguidelines-macro-usage)
#define OFFSET_GAMETYPE	        (OFFSET_BOCK_MULTIPLIER + 1) // NOLINT(cppcoreguidelines-macro-usage)
namespace UI_GTKMM_NS {

PartyPoints::PartyPointsModel::PartyPointsModel(unsigned const playerno) :
  playerpoints(playerno)
{
  add(empty);
  add(round);
  add(round_str);
  add(round_color);
  add(gameno);
  add(gameno_str);
  for (auto& p : playerpoints)
    add(p);
  add(gamepoints_str);
  add(bock_multiplier_str);
  add(gametype);
}


PartyPoints::PartyPoints(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::party points"), false),
  party_points_model(4)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this, &PartyPoints::init));
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &PartyPoints::on_key_press_event));
#endif

#ifdef WORKAROUND
  // remove when 'GamePoints' doesn't need a pointer to Game any more
  realize();
#endif
}


PartyPoints::~PartyPoints() = default;


void PartyPoints::init()
{
  set_icon(ui->icon);

  game_overview = std::make_unique<GameOverview>(this);

  { // notebook
    notebook = Gtk::manage(new Gtk::Notebook());

    { // table
      auto table_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
      table_vbox->set_border_width(1 EX);
      table_vbox->set_spacing(1 EX);

      { // the points table
        // of the sum treeview only the header is neeeded
        // (workaround since there exists no foot for a treeview)
        duty_soli_list = Gtk::TreeStore::create(party_points_model);
        party_points_sum_treeview = Gtk::manage(new Gtk::TreeView(duty_soli_list));
        duty_soli_treeview = party_points_sum_treeview;

        party_points_list = Gtk::TreeStore::create(party_points_model);
        party_points_treeview = Gtk::manage(new Gtk::TreeView(party_points_list));
        party_points_treeview->get_selection()->set_mode(Gtk::SELECTION_SINGLE);

        party_points_treeview->append_column("", party_points_model.empty);
        party_points_sum_treeview->append_column("", party_points_model.empty);
        party_points_sum_treeview->get_column(0)->set_alignment(Gtk::ALIGN_END);

        auto const& party = *::party;
        for (unsigned p = 0; p < party.players().size(); p++) {
          party_points_treeview->append_column(party.players()[p].name(), party_points_model.playerpoints[p]);
          party_points_treeview->get_column_cell_renderer(p + OFFSET_PLAYER)->set_property("xalign", 0.5);
          party_points_treeview->get_column(p + OFFSET_PLAYER)->set_alignment(Gtk::ALIGN_CENTER);
          party_points_sum_treeview->append_column(party.players()[p].name(), party_points_model.playerpoints[p]);
          party_points_sum_treeview->get_column_cell_renderer(p + OFFSET_PLAYER)->set_property("xalign", 0.5);
          party_points_sum_treeview->get_column(p + OFFSET_PLAYER)->set_alignment(Gtk::ALIGN_CENTER);
          party_points_sum_treeview->get_column(p + OFFSET_PLAYER)->set_resizable(false);
          duty_soli_treeview->get_column(p + OFFSET_PLAYER)->set_alignment(Gtk::ALIGN_CENTER);
        } // for (p < party.players().size())
        party_points_treeview->append_column(_("game points"), party_points_model.gamepoints_str);
        party_points_treeview->get_column_cell_renderer(OFFSET_GAMEPOINTS)->set_property("xalign", 0.5);
        party_points_sum_treeview->append_column("", party_points_model.gamepoints_str);
        party_points_sum_treeview->get_column(OFFSET_GAMEPOINTS)->set_alignment(Gtk::ALIGN_CENTER);
        party_points_sum_treeview->get_column_cell_renderer(OFFSET_GAMEPOINTS)->set_property("xalign", 0.5);

        party_points_treeview->append_column(_("bock"), party_points_model.bock_multiplier_str);
        party_points_treeview->get_column_cell_renderer(OFFSET_BOCK_MULTIPLIER)->set_property("xalign", 0.5);
        party_points_sum_treeview->append_column("", party_points_model.bock_multiplier_str);
        party_points_sum_treeview->get_column(OFFSET_BOCK_MULTIPLIER)->set_alignment(Gtk::ALIGN_CENTER);
        party_points_sum_treeview->get_column_cell_renderer(OFFSET_BOCK_MULTIPLIER)->set_property("xalign", 0.5);

        party_points_treeview->append_column(_("game type"), party_points_model.gametype);
        party_points_sum_treeview->append_column("", party_points_model.gametype);


#ifdef LINUX
        party_points_treeview->set_rules_hint(true);
#endif
        for (unsigned c = 0;
             c < party_points_treeview->get_columns().size();
             ++c) {
          auto column = party_points_treeview->get_column(c);
#ifdef GLIBMM_PROPERTIES_ENABLED
          column->property_width().signal_changed().connect(sigc::mem_fun(*this, &PartyPoints::update_player_columns_size));
          //column->property_width().signal_changed().connect(sigc::mem_fun(*this, &PartyPoints::sum_columns_size_update));
#else
          column->connect_property_changed("width", sigc::mem_fun(*this, &PartyPoints::update_player_columns_size));
#endif
#ifdef WINDOWS
          column->add_attribute(*column->get_first_cell(),
                                "background", 3);
#endif
          column->set_cell_data_func(*column->get_first_cell(),
                                     sigc::bind<unsigned>(sigc::mem_fun(*this, &PartyPoints::set_cell_color), c));
        }

      } // the points table

      { // point list
        auto table_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
        { // the scrolled window
          auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
          scrolled_window->set_policy(Gtk::POLICY_NEVER,
                                      Gtk::POLICY_AUTOMATIC);
          scrolled_window->add(*party_points_treeview);
          scrolled_window->set_hadjustment(party_points_treeview->get_hadjustment());
          scrolled_window->set_vadjustment(party_points_treeview->get_vadjustment());

          table_box->pack_start(*scrolled_window, true, true);
        } // the scrolled window
        table_box->pack_end(*party_points_sum_treeview, false, true);
        table_vbox->pack_start(*table_box);
      } // point list

      {
        sum_row = *duty_soli_list->append();
        sum_row[party_points_model.gametype] = _("sum");
      }
      { // duty soli
        // add lines for duty soli
        {
          duty_free_soli_row = *duty_soli_list->append();
          duty_free_soli_row[party_points_model.gametype] = _("duty free soli");
        }
        {
          duty_color_soli_row = *duty_soli_list->append();
          duty_color_soli_row[party_points_model.gametype] = _("duty color soli");
        }
        {
          duty_picture_soli_row = *duty_soli_list->append();
          duty_picture_soli_row[party_points_model.gametype] = _("duty picture soli");
        }
      } // duty soli

      { // the buttons
        { // show game overview
          show_game_overview_button = Gtk::manage(new Gtk::Button(_("Button::show game overview")));
          show_game_overview_button->set_halign(Gtk::ALIGN_CENTER);
          table_vbox->pack_end(*show_game_overview_button, Gtk::PACK_SHRINK);
          show_game_overview_button->signal_clicked().connect(sigc::mem_fun(*this, &PartyPoints::show_game_overview));
        } // show game overview
        { // add up points
          add_up_points
            = Gtk::manage(new Gtk::CheckButton(_("Button::add up points")));
          add_up_points->set_halign(Gtk::ALIGN_CENTER);
          table_vbox->pack_end(*add_up_points, Gtk::PACK_SHRINK);
          add_up_points->signal_toggled().connect(sigc::mem_fun(*this, &PartyPoints::recalc_all));
          add_up_points->set_active();
        } // add up points

        { // collapse rounds
          auto box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 2 EM));
          box->set_homogeneous();

          Gtk::RadioButton::Group group;
          {
            collapse_rounds
              = Gtk::manage(new Gtk::RadioButton(group, _("Button::collapse rounds")));
            box->add(*collapse_rounds);
            collapse_rounds->signal_toggled().connect(sigc::mem_fun(*this, &PartyPoints::collaps_setting_changed));
          }
          {
            expand_rounds
              = Gtk::manage(new Gtk::RadioButton(group, _("Button::expand rounds")));
            box->add(*expand_rounds);
            expand_rounds->signal_toggled().connect(sigc::mem_fun(*this, &PartyPoints::collaps_setting_changed));
          }
          box->set_halign(Gtk::ALIGN_CENTER);
          table_vbox->pack_end(*box, Gtk::PACK_SHRINK);
        } // collapse rounds
      } // the buttons

      auto label = Gtk::manage(new Gtk::Label(_("Label::table")));
      notebook->append_page(*table_vbox, *label);
    } // table
    { // graph
      graph = Gtk::manage(new Graph(this));

      auto label = Gtk::manage(new Gtk::Label(_("Label::graph")));
      notebook->append_page(*graph, *label);
    } // graph

    get_content_area()->pack_start(*notebook);
  } // notebook

  { // remaining rounds / points
    remaining_rounds_label
      = Gtk::manage(new Gtk::Label(_("remaining rounds: %u", 0U)));
    remaining_points_label
      = Gtk::manage(new Gtk::Label(_("remaining points: %i", 0)));

    auto box = Gtk::manage(new Gtk::Box());
    box->set_homogeneous();
    box->pack_start(*remaining_rounds_label, Gtk::PACK_SHRINK, 1 EM);
    box->pack_end(*remaining_points_label, Gtk::PACK_SHRINK, 1 EM);

    get_content_area()->pack_start(*box, false, true);
  } // remaining rounds / points

  add_close_button(*this);

  show_all_children();

  { // signals
    party_points_treeview->get_selection()->signal_changed().connect(sigc::mem_fun(*this, &PartyPoints::row_selection_changed));

    signal_show().connect(sigc::mem_fun(*this,
                                        &PartyPoints::update_player_columns_size));
    signal_show().connect(sigc::mem_fun(*this, &PartyPoints::update));
    party_points_treeview->signal_row_collapsed().connect(sigc::mem_fun(*this, &PartyPoints::row_collapsed_or_expanded));
    party_points_treeview->signal_row_expanded().connect(sigc::mem_fun(*this, &PartyPoints::row_collapsed_or_expanded));
  } // signals

  update_player_columns_size();

  set_default_size(0, 4 * ui->cards->height());

  show_game_overview_button->set_sensitive(false);

  if (::party->status() > Party::Status::init)
    recreate_all();

  Party::signal_open         .connect(*this, &PartyPoints::party_open,        disconnector_);
  ::party->signal_loaded     .connect(*this, &PartyPoints::recreate_all,      disconnector_);
  ::party->signal_start_round.connect(*this, &PartyPoints::party_start_round, disconnector_);
  ::party->signal_finish     .connect(*this, &PartyPoints::party_finish,      disconnector_);
  ::party->signal_close      .connect(*this, &PartyPoints::party_close,       disconnector_);


  ::signal_update.connect(*this, &PartyPoints::lazy_recreate_all_step, disconnector_);
}


void PartyPoints::on_show()
{
  StickyDialog::on_show();

  if (lazy_recreate_all_)
    return ;

  recreate_all_preprocessing();
  lazy_recreate_all_ = false;
  for (unsigned g = 0; g < party().finished_games(); ++g) {
    add_game(g, false);
  }

  recreate_all_postprocessing();
}


auto PartyPoints::party() -> Party&
{
  DEBUG_ASSERTION(party_,
                  "PartyPoints::party()\n"
                  "  party is not set");
  return *party_;
}


void PartyPoints::party_open(Party& party)
{
  if (!get_realized())
    return ;
  party_ = &party;

  party_points_list->clear();
  round_rows.clear();
  game_rows.clear();

  auto const& rule = party.rule();
  if (rule(Rule::Type::number_of_rounds_limited))
    remaining_rounds_label->show();
  else
    remaining_rounds_label->hide();
  if (rule(Rule::Type::points_limited))
    remaining_points_label->show();
  else
    remaining_points_label->hide();

  party_points_treeview->get_column(OFFSET_BOCK_MULTIPLIER)->set_visible(rule(Rule::Type::bock));
  party_points_sum_treeview->get_column(OFFSET_BOCK_MULTIPLIER)->set_visible(rule(Rule::Type::bock));
}


void PartyPoints::party_start_round(unsigned const round)
{
  if (!get_realized())
    return ;
  if (round == 0)
    return ;
  if (round_rows.empty())
    return ;
#ifndef DKNOF
  if (round >= round_rows.size())
    return ;
#endif

  if (collapse_rounds->get_active()) {
    party_points_treeview->collapse_row(party_points_list->get_path(round_rows[round - 1]));
  }
}


void PartyPoints::party_finish()
{
  if (!get_realized())
    return ;

  if (graph)
    graph->draw_all();
  update_sum();
  update_duty_soli();

  if (!round_rows.empty())
    party_points_treeview->collapse_row(party_points_list->get_path(round_rows.back()));
}


void PartyPoints::party_close()
{
  if (!get_realized())
    return ;
  party_points_list->clear();
  round_rows.clear();
  game_rows.clear();
  if (graph)
    graph->draw_all();
  game_overview->set_gameno(UINT_MAX);

  hide();
  party_ = nullptr;
}


void PartyPoints::game_start()
{
  if (!get_realized())
    return ;

  if (graph)
    graph->draw_all();
}


void PartyPoints::game_finished()
{
  if (!get_realized())
    return ;

  if (party().is_replayed_game())
    return ;

  add_game(party().gameno());
  add_future_bock_multipliers();
  party_points_treeview->scroll_to_row(Gtk::TreePath(game_rows[party().gameno()]));
}


void PartyPoints::name_changed(Player const& player)
{
  if (!get_realized())
    return ;

  party_points_treeview->get_column(::party->players().no(player) + OFFSET_PLAYER)->set_title(" " + player.name() + " ");
  party_points_sum_treeview->get_column(::party->players().no(player) + OFFSET_PLAYER)->set_title(" " + player.name() + " ");
  game_overview->name_changed(player);

  party_points_treeview->columns_autosize();
}


void PartyPoints::recalc_all()
{
  if (!get_realized())
    return ;

  if (::party->status() <= Party::Status::init)
    return ;

  for (unsigned g = 0; g < game_rows.size(); g++)
    set_points(g);

  update_sum();
  update_duty_soli();

  if (graph)
    graph->draw_all();
}


void PartyPoints::recreate_all()
{
  // @todo    round limits
  // @todo    added bock rounds
  // @todo    recreate the columns
#if 1
  if (ui->party().status() < Party::Status::play
      && (   !is_visible()
          || party().finished_games() < 40) ) {
    lazy_recreate_all_ = true;
    return ;
  }
#endif

  lazy_recreate_all_ = false;
  for (unsigned g = 0; g < party().finished_games(); ++g) {
    add_game(g, false);
  }

  recreate_all_postprocessing();
}


void PartyPoints::recreate_all_preprocessing()
{
  if (!get_realized())
    return ;
  if (ui->party().status() < Party::Status::init)
    return ;

  party_points_list->clear();
  round_rows.clear();
  game_rows.clear();
}


void PartyPoints::lazy_recreate_all_step()
{
  if (!lazy_recreate_all_)
    return ;
  if (   party().status() == Party::Status::programstart
      || party().status() == Party::Status::init)
    return ;

  auto const g = game_rows.size();
  add_game(g, false);

  if (g + 1 >= party().finished_games()) {
    lazy_recreate_all_ = false;
    recreate_all_postprocessing();
  }
}


void PartyPoints::recreate_all_postprocessing()
{
  add_future_bock_multipliers();

  auto const& rule = party().rule();
  if (duty_free_soli_row)
    duty_soli_list->erase(duty_free_soli_row);
  if (duty_color_soli_row)
    duty_soli_list->erase(duty_color_soli_row);
  if (duty_picture_soli_row)
    duty_soli_list->erase(duty_picture_soli_row);
  if (rule(Rule::Type::number_of_duty_soli) > 0)
    duty_free_soli_row = *duty_soli_list->append();
  else
    duty_free_soli_row = Gtk::TreeRow();
  if (rule(Rule::Type::number_of_duty_color_soli) > 0)
    duty_color_soli_row = *duty_soli_list->append();
  else
    duty_color_soli_row = Gtk::TreeRow();
  if (rule(Rule::Type::number_of_duty_picture_soli) > 0)
    duty_picture_soli_row = *duty_soli_list->append();
  else
    duty_picture_soli_row = Gtk::TreeRow();

  update_sum();
  update_duty_soli();

  party_points_treeview->get_column(OFFSET_BOCK_MULTIPLIER)->set_visible(rule(Rule::Type::bock));
  party_points_sum_treeview->get_column(OFFSET_BOCK_MULTIPLIER)->set_visible(rule(Rule::Type::bock));

  update();

  if (graph)
    graph->draw_all();
}


void PartyPoints::player_added(Player const& player)
{
  auto const p = party().players().no(player);
  //party_points_model.playerpoints.resize(party().players().size());
  party_points_treeview->insert_column(player.name(),
                                       party_points_model.playerpoints[p],
                                       p + OFFSET_PLAYER);
  party_points_treeview->get_column_cell_renderer(p + OFFSET_PLAYER)->set_property("xalign", 0.5);
  party_points_treeview->get_column(p + OFFSET_PLAYER)->set_alignment(Gtk::ALIGN_CENTER);
  party_points_sum_treeview->insert_column(player.name(),
                                           party_points_model.playerpoints[p],
                                           p);
  party_points_sum_treeview->get_column(p + OFFSET_PLAYER)->set_resizable(false);
  duty_soli_treeview->get_column(p + OFFSET_PLAYER)->set_alignment(Gtk::ALIGN_CENTER);
}


void PartyPoints::add_game(unsigned const gameno, bool const update)
{
  if (!get_realized())
    return ;

  if (party().starts_new_round(gameno)) {
    party_start_round(party().round_of_game(gameno));
  }

  add_row(gameno);
  set_points(gameno);
  if (update) {
    party_points_treeview->expand_to_path(party_points_list->get_path(game_rows[gameno]));
    update_sum();
    update_duty_soli();
    if (graph)
      graph->draw_all();
  }
}


void PartyPoints::set_points(unsigned const gameno)
{
  if (!get_realized())
    return ;

  DEBUG_ASSERTION((gameno < game_rows.size()),
                  "PartyPoints::set_points(gameno)\n"
                  "  gameno = " << gameno << " >= "
                  << game_rows.size() << " = number of rows");


  auto const& party = this->party();
  auto& row = game_rows[gameno];

  if (gameno >= party.gameno()
      + (party.in_game() && party.game().status() == Game::Status::finished ? 1 : 0)) {
    row[party_points_model.bock_multiplier_str]
      = (  (party.bock_multiplier(gameno) != 1)
         ? std::to_string(party.bock_multiplier(gameno))
         : "");
    if (party.rule()(Rule::Type::number_of_rounds_limited)
        && (gameno == party.round_startgame(party.rule()(Rule::Type::number_of_rounds))) )
      row[party_points_model.gametype] = _("duty soli round");
    else
      row[party_points_model.gametype] = "";
    return ;
  }

  auto const& game_summary = party.game_summary(gameno);

  if (   party.starts_new_round(gameno)
      && row.children()) {
    if (!party_points_treeview->row_expanded(party_points_list->get_path(row))) {
      update_round(row[party_points_model.round]);
      return ;
    }
  }

  row[party_points_model.round] = party.round_of_game(gameno);
  if (party.round_of_game(gameno) % 2 == 0)
    row[party_points_model.round_color] = "#F8F8F8";
  else
    row[party_points_model.round_color] = "#F0F0F0";
  row[party_points_model.gameno] = gameno;
  row[party_points_model.gameno_str]
    = std::to_string(gameno);
  row[party_points_model.bock_multiplier_str]
    = (   (game_summary.bock_multiplier() != 1)
       ? std::to_string(game_summary.bock_multiplier())
       : "");
  row[party_points_model.gamepoints_str]
    = std::to_string(game_summary.points());
#ifdef POSTPONED
  if (game_summary.game_type() == GameType::normal)
    ;
  //row[party_points_model.gametype] = nullptr;
  else {
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = ui->icons->icon(game_points.game_type());
    int const height = 20;
    row[party_points_model.gametype] = pixbuf->scale_simple(pixbuf->get_width() * height / pixbuf->get_height(), height, Gdk::INTERP_TILES);
  }
#else
  if (game_summary.game_type() == GameType::normal) {
    row[party_points_model.gametype] = "";
  } else {
    row[party_points_model.gametype] = (_(game_summary.game_type()) + (game_summary.is_duty_solo() ? " *" : ""));
  }
#endif
  for (unsigned p = 0; p < party.players().size(); p++) {
    if (game_summary.get_points(party.players()[p])) {
      if (add_up_points->get_active())
        row[party_points_model.playerpoints[p]] = std::to_string(party.pointsum_till_game(gameno, p));
      else
        row[party_points_model.playerpoints[p]] = std::to_string(party.game_summary(gameno).points(party.players()[p]));
    } else {
      row[party_points_model.playerpoints[p]] = "";
    }
  } // for (p < party.players().size())
} // void PartyPoints::set_points(unsigned const gameno)


void PartyPoints::add_future_bock_multipliers()
{
  if (!get_realized())
    return ;

  auto const& party = this->party();
  if (party.current_bock_multiplier() == 1)
    return ;

  // first add the missing rows
  for (unsigned g = 0;
       g < party.bock_multipliers().size();
       ++g) {
    add_row(party.finished_games() + g);
  }

  // second set the multipliers
  for (unsigned g = 0;
       g < party.bock_multipliers().size();
       ++g) {
    auto const gameno = party.finished_games() + g;
    auto& row = game_rows[gameno];
    row[party_points_model.round] = party.round_of_game(gameno);
    if (   party.rule()(Rule::Type::number_of_rounds_limited)
        && (gameno == party.round_startgame(party.rule()(Rule::Type::number_of_rounds))) )
      row[party_points_model.gametype] = _("duty soli round");
    row[party_points_model.gameno]
      = gameno;
    row[party_points_model.bock_multiplier_str]
      = (   (party.bock_multiplier(gameno) == 1)
         ? ""
         : std::to_string(party.bock_multiplier(gameno)));
  } // for (g)
}


void PartyPoints::add_row(unsigned const gameno)
{
  if (!get_realized())
    return ;
  auto const& party = this->party();
  if (game_rows.size() <= gameno) {
    if (party.starts_new_round(gameno)) {
      round_rows.push_back(*party_points_list->append());
      game_rows.push_back(round_rows.back());
    } else {
      game_rows.push_back(*party_points_list->append(round_rows.back().children()));
      if (round_rows.back()[party_points_model.gameno] + 1 >= gameno) {
        party_points_treeview->expand_to_path(party_points_list->get_path(round_rows.back()));
      }
    }
  } else if (   party.finished_games() > gameno
             && party.game_summary(gameno).startplayer_stays()
            ) {
    // when a solo game has been played, add a row
    if (party_points_list->iter_depth(game_rows[gameno]) == 0) {
      if (game_rows[gameno].children().empty()) {
        game_rows.insert(game_rows.begin() + gameno + 1,
                         *party_points_list->append(game_rows[gameno].children()));
      } else {
        game_rows.insert(game_rows.begin() + gameno + 1,
                         *party_points_list->insert(game_rows[gameno+1]));
      }
    } else {
      game_rows.insert(game_rows.begin() + gameno,
                       *party_points_list->insert(game_rows[gameno]));
    }
    // update the future rows
    for (unsigned g = gameno; g < game_rows.size(); ++g)
      game_rows[g][party_points_model.gameno] = g;
  }
  game_rows[gameno][party_points_model.round] = party.round_of_game(gameno);

#ifndef RELEASE
#ifdef DKNOF
  if (   (party_points_list->iter_depth(game_rows[gameno]) == 0)
      != party.starts_new_round(gameno) ) {
    DEBUG_ASSERTION(false,
                    "PartyPoints::add_row(" << gameno << ")\n"
                    "  depth of row = " << party_points_list->iter_depth(game_rows[gameno]) << " but game " << gameno << " starts new round = " << party.starts_new_round(gameno) << '\n'
                    << "  roundno(" << gameno << ") = " << party.round_of_game(gameno) << '\n'
                    << "  startgame of round = " << party.round_startgame(party.round_of_game(gameno)));
  }
#endif
#endif
}


void PartyPoints::row_collapsed_or_expanded(Gtk::TreeModel::iterator const& iterator,
                                            Gtk::TreeModel::Path const& path)
{
  if (!get_realized())
    return ;

  // ToDo: verify that it is a round-row
  auto row = *party_points_treeview->get_model()->get_iter(path);

  if (party_points_treeview->row_expanded(path))
    set_points(row[party_points_model.gameno]);
  else
    update_round(row[party_points_model.round]);
}


void PartyPoints::update_round(unsigned const round)
{
  if (!get_realized())
    return ;

  DEBUG_ASSERTION((round_rows.size() > round),
                  "PartyPoints::update_round(round):\n"
                  "  round too great: round = " << round
                  << " >= round_rows.size() = "
                  << round_rows.size());
  if (ui->party().status() <= Party::Status::init)
    return ;

  auto const& party = this->party();
  auto& row = round_rows[round];

  if (   party_points_treeview->row_expanded(party_points_list->get_path(row))
      || row.children().empty())
    return ;

  row[party_points_model.gameno_str] = "";
  if (party.bock_multiplier(party.round_startgame(round)) > 1)
    row[party_points_model.bock_multiplier_str] = std::to_string(party.bock_multiplier(party.round_startgame(round)));
  else
    row[party_points_model.bock_multiplier_str] = "";
  row[party_points_model.gametype] = (  party.is_duty_soli_round(round) ? _("duty soli round") : "");

  if (round > party.round_of_game(party.finished_games() - 1)) {
    for (unsigned p = 0; p < party.players().size(); ++p)
      row[party_points_model.playerpoints[p]] = "";
    row[party_points_model.gamepoints_str] = "";
  } else { // if !(round > party.roundno())
    if (add_up_points->get_active()) {
      for (unsigned p = 0; p < party.players().size(); ++p)
        row[party_points_model.playerpoints[p]] = std::to_string(party.pointsum_till_round(round, p));
      row[party_points_model.gamepoints_str] = std::to_string(party.pointsum_till_round(round));
    } else {
      for (unsigned p = 0; p < party.players().size(); ++p)
        row[party_points_model.playerpoints[p]] = std::to_string(party.pointsum_in_round(round, p));
      row[party_points_model.gamepoints_str] = std::to_string(party.pointsum_in_round(round));
    }
  }
}


void PartyPoints::update_sum()
{
  if (!get_realized())
    return ;

  auto const& party = this->party();

  // udpate the sum
  for (unsigned p = 0; p < party.players().size(); ++p) {
    sum_row[party_points_model.playerpoints[p]] = std::to_string(party.pointsum(p));
  }
  sum_row[party_points_model.gamepoints_str] = std::to_string(party.pointsum());
  // ToDo: write the number of bock triggers
  //sum_row->get_column(0)->set_title(" " + std::to_string(party.gameno() + 1) + " ");

  sum_columns_size_update();


  if (party.rule()(Rule::Type::number_of_rounds_limited))
    remaining_rounds_label->set_label(_("remaining rounds: %u",
                                        party.remaining_rounds())
                                     );
  if (party.rule()(Rule::Type::points_limited))
    remaining_points_label->set_label(_("remaining points: %i",
                                        ( (party.remaining_points() > 0)
                                         ? party.remaining_points()
                                         : 0) ) );
  if (   party.rule()(Rule::Type::number_of_rounds_limited)
      || party.rule()(Rule::Type::points_limited))
    remaining_rounds_label->get_parent()->set_border_width(1 EX);
  else
    remaining_rounds_label->get_parent()->set_border_width(0);
}


void PartyPoints::update_duty_soli()
{
  if (!get_realized())
    return ;

  auto const& party = this->party();

  for (unsigned p = 0; p < party.players().size(); ++p) {
    if (duty_free_soli_row)
      duty_free_soli_row[party_points_model.playerpoints[p]] = std::to_string(party.remaining_duty_free_soli(p));
    if (duty_color_soli_row)
      duty_color_soli_row[party_points_model.playerpoints[p]] = std::to_string(party.remaining_duty_color_soli(p));
    if (duty_picture_soli_row)
      duty_picture_soli_row[party_points_model.playerpoints[p]] = std::to_string(party.remaining_duty_picture_soli(p));
  } // for (p < players().size())

  // ToDo: show/hide the rows according to the rule
}


void PartyPoints::update_player_columns_size()
{
  if (!is_visible())
    return ;

  auto const& party = this->party();

  for (unsigned c = OFFSET_PLAYER;
       c < OFFSET_PLAYER + party.players().size();
       ++c) {
    party_points_treeview->get_column(c)->set_min_width(-1);
  }
  {
    int width = -1;
    for (unsigned c = OFFSET_PLAYER; c < OFFSET_PLAYER + party.players().size(); ++c) {
      width = max(width, party_points_treeview->get_column(c)->get_width());
    }
    for (unsigned c = OFFSET_PLAYER; c < OFFSET_PLAYER + party.players().size(); ++c) {
      if (party_points_treeview->get_column(c)->get_min_width() < width)
        party_points_treeview->get_column(c)->set_min_width(width);
    }
  }

  sum_columns_size_update();
}


void PartyPoints::row_selection_changed()
{
  // show the belonging game summary
#ifdef WORKAROUND
  // beim Aufruf von party_points_list->clear() aus party_close() wird das Signal row_selection_changed erzeugt. selection->get_selected() wird wahr, aber party gibt es gar nicht mehr.
  if (party_ == nullptr)
    return ;
#endif

  auto selection = party_points_treeview->get_selection();

  show_game_overview_button->set_sensitive(selection->get_selected_rows().size() == 1);

  if (selection->get_selected()) {
    auto row = *selection->get_selected();

    auto const gameno = row[party_points_model.gameno];

    // ToDo: check, if the current game is finished
    if (gameno < party().finished_games()) {
      game_overview->set_gameno(gameno);
      show_game_overview_button->set_sensitive(true);
    } else {
      game_overview->set_gameno(UINT_MAX);
      game_overview->hide();
      show_game_overview_button->set_sensitive(false);
    }
  } else {	// if !(selection->get_selected())
    game_overview->set_gameno(UINT_MAX);
    game_overview->hide();
    show_game_overview_button->set_sensitive(false);
  }	// if !(selection->get_selected())
}


void PartyPoints::sum_columns_size_update()
{
  // update the column size of the sum treeview
  if (!get_realized())
    return ;

  for (unsigned c = 0; c < party_points_treeview->get_columns().size() - 1; ++c)
    if (party_points_treeview->get_column(c)->get_visible()) {
      auto const width
        = party_points_treeview->get_column(c)->get_width();
#ifdef WORKAROUND
      party_points_sum_treeview->get_column(c)->set_min_width(width);
      party_points_sum_treeview->get_column(c)->set_max_width(width);
#else
      // does not work
      party_points_sum_treeview->get_column(c)->set_fixed_width(width);
      party_points_sum_treeview->get_column(c)->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
#endif
    }
}


void PartyPoints::clear_selection()
{
  if (!get_realized())
    return ;

  party_points_treeview->get_selection()->unselect_all();
}


void PartyPoints::show_game_overview()
{
  if (!get_realized())
    return ;

  if (!game_overview)
    return ;

  row_selection_changed();

  if (game_overview->gameno() < party().finished_games())
    game_overview->present();
#ifdef WORKAROUND
  // 'game_overview' cannot get the gameno in the initialisation
  if (game_overview->gameno() == UINT_MAX)
    row_selection_changed();
#endif
}


void PartyPoints::collaps_setting_changed()
{
  if (!get_realized())
    return ;

  if (collapse_rounds->get_active())
    party_points_treeview->collapse_all();
  else
    party_points_treeview->expand_all();
}


void PartyPoints::update()
{
  if (!get_realized())
    return ;

  if (ui->party().status() <= Party::Status::init)
    return ;

  auto const& party = this->party();
  auto const& rule = party.rule();
  for (auto const& row : game_rows) {
    auto const gameno = row[party_points_model.gameno];
    // ToDo: check, whether the current game is finished
    if (gameno >= party.finished_games()) {
      if (   rule(Rule::Type::number_of_rounds_limited)
          && (gameno == party.round_startgame(rule(Rule::Type::number_of_rounds))) )
        row[party_points_model.gametype]
          = _("duty soli round");
      else
        row[party_points_model.gametype] = "";
    } else { // if !(gameno >= party.finished_games())
      auto const& game_summary = party.game_summary(gameno);
      if (game_summary.game_type() == GameType::normal)
        row[party_points_model.gametype] = "";
      else
        row[party_points_model.gametype]
          = (_(game_summary.game_type())
             + (game_summary.is_duty_solo() ? " *" : ""));
    } // if !(gameno >= party.finished_games())
  }
  for (auto const& row : round_rows) {
    update_round(row[party_points_model.round]);
  }
}


void PartyPoints::set_cell_color(Gtk::CellRenderer* cell_renderer,
                                 Gtk::TreeModel::iterator const& iterator,
                                 unsigned const column)
{
  if (!get_realized())
    return ;

  auto const row = *iterator;

  auto const gameno = row[party_points_model.gameno];
  auto const& party = this->party();
  if (gameno >= party.finished_games())
    return ;

  auto const& game_summary = party.game_summary(gameno);
  auto cell_renderer_text = dynamic_cast<Gtk::CellRendererText*>(cell_renderer);
  if (!cell_renderer_text)
    return ;

#ifdef POSTPONED
  if (party_points_treeview->row_expanded(Gtk::TreePath(iterator)))
    ;
#endif
  if (   game_summary.soloplayer() != UINT_MAX
      && (   column == OFFSET_GAMETYPE
          || column == OFFSET_PLAYER + game_summary.party_player(game_summary.soloplayer()) )
     ) {
    cell_renderer_text->property_foreground() = color(game_summary);
    cell_renderer_text->property_weight() = (  column == OFFSET_GAMETYPE
                                             ? Pango::WEIGHT_NORMAL
                                             : Pango::WEIGHT_BOLD);
  } else {
    cell_renderer_text->property_foreground() = "black";
    cell_renderer_text->property_weight() = Pango::WEIGHT_NORMAL;
  }


#ifdef POSTPONED
  // the whole column is made italic

  // make the name of the startplayer bold
  if (   (column >= OFFSET_PLAYER)
      && (column < OFFSET_PLAYER + party.players().size())
      && (game_summary.soloplayer() != UINT_MAX)
      && (column - OFFSET_PLAYER == game_summary.party_player(game_summary.soloplayer()))) {
    cell_renderer->set_property("style", Pango::STYLE_ITALIC);
  }
#endif
}


auto PartyPoints::color(::GameSummary const& game_summary) -> string
{
  switch (game_summary.game_type()) {
  case GameType::normal:
    return "black";
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
    return "#ffff00";
  case GameType::poverty:
    return "#B8860B";
  case GameType::marriage:
    return "#1E90FF";
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    return "#00BFFF";
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::solo_club:
  case GameType::solo_spade:
  case GameType::solo_heart:
  case GameType::solo_diamond:
  case GameType::solo_meatless:
    if (game_summary.is_duty_solo())
      return "#FF0000";
    else
      return "#ffaf00";
  }

  return "black";
}

/** a key has been pressed
 ** C-o: output of the points on 'stdout'
 **
 ** @param    key   the key
 **
 ** @return   whether the key was managed
 **
 ** @todo     print the party points
 **/
bool
PartyPoints::on_key_press_event(GdkEventKey* const key)
{
  bool managed = false;

  if ((key->state & ~GDK_SHIFT_MASK) == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_o: // output of the party points
      // ToDo
      managed = true;
      break;
    }
  }

  return (managed
          || StickyDialog::on_key_press_event(key)
          || ui->key_press(key));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
