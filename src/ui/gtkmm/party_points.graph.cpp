/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "party_points.graph.h"

#include "ui.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game.h"

namespace UI_GTKMM_NS {

PartyPoints::Graph::Graph(PartyPoints* const party_points) :
  Base(party_points)
{ }


PartyPoints::Graph::~Graph() = default;


void PartyPoints::Graph::draw_all()
{
  queue_draw();
}

auto PartyPoints::Graph::on_draw(::Cairo::RefPtr<::Cairo::Context> const& cr) -> bool
{
  DrawingArea::on_draw(cr);
  auto const width = get_width();
  auto const height = get_height();

  cr->push_group();

  { // clear the whole area
    cr->save();
    cr->set_source_rgb(1, 1, 1);
    cr->scale(width, height);
    cr->rectangle(0, 0, width, height);
    cr->fill();
    cr->restore();
  } // clear the whole area

  auto const& party = ui->party(); // cppcheck-suppress shadowVariable
  auto const& rule = party.rule();


  Counting const counting = rule(Rule::Type::counting);

  // last game to draw + 1
  unsigned const played_gamesno
    = (party.gameno()
       + ( party.in_game() && party.game().status() == Game::Status::finished
          ? 1
          : 0) );

  // calculate the minimum and maximum points made by the players
  int min_points = 0; // minimal points made by the players
  int max_points = 0; // maximal points made by the players

  if (played_gamesno == 0) {
    if (rule(Rule::Type::points_limited))
      max_points = rule(Rule::Type::points) / 2;
    if (max_points == 0)
      max_points = 10;

    switch (rule(Rule::Type::counting)) {
    case Counting::plus:
      break;
    case Counting::minus:
      min_points = -max_points;
      max_points = 0;
      break;
    case Counting::plusminus:
      min_points = -max_points;
      break;
    } // switch (rule(Rule::Type::counting))
  } // if (played_gamesno == 0)

  for (unsigned g = 0; g < played_gamesno; ++g) {
    for (unsigned p = 0; p < party.players().size(); ++p) {
      int const points = party.pointsum_till_game(g, p);
      if (points < min_points)
        min_points = points;
      if (points > max_points)
        max_points = points;
    } // for (p < party.players().size())
  } // for (g < played_gamesno)
  if (max_points == min_points)
    max_points += 1;

  // how many games to display
  unsigned gamesno = UINT_MAX;
  if (party.status() == Party::Status::finished) {
    gamesno = party.finished_games();
  } else if (party.is_last_game()) {
    gamesno = played_gamesno;
    if (!(party.in_game() && party.game().status() == Game::Status::finished))
      gamesno += 1;
  } else {
    if (rule(Rule::Type::number_of_rounds_limited)) {
      gamesno = max(played_gamesno,
                    party.round_startgame(rule(Rule::Type::number_of_rounds)));
    }
    if (   rule(Rule::Type::points_limited)
        && party.points() > 0) {
      gamesno = min(gamesno,
                    max( (  (rule(Rule::Type::points)
                             * (played_gamesno + 1))
                          / static_cast<int>(party.points())),
                        played_gamesno + 1));
    }
    if (gamesno < UINT_MAX) {
      gamesno += party.remaining_duty_soli();
      if (   party.in_game()
          && party.game().status() == Game::Status::finished
          && party.game().is_duty_solo()) {
        gamesno += 1;
      }
    }
  } // if (party.is_last_game())

  if (gamesno == UINT_MAX)
    gamesno = played_gamesno;
  if (gamesno < party.round_startgame(1))
    gamesno = party.round_startgame(1);
  // how many rounds to display
  auto roundsno = party.round_of_game(gamesno);
#ifdef WORKAROUND
  // beim Laden eines beendeten Turniers ist der Status noch nicht Status::finished, daher ist roundsno um 1 zu klein
  if (party.is_finished() && party.status() != Party::Status::finished)
    roundsno += 1;
#endif


  // layout:
  // +-----------------------------+ border_y
  // | 10+       /                 |
  // |   |     _/                  |
  // |   |   _/  _                 |
  // |  5+  /   /                  | height_graph
  // |   | / __/                   |
  // |   |/ /                      |
  // |  0+----+----+----+----+----+| border_roundno
  // |   0    5   10   15   20   25| height_roundno, border_names
  // |    player1 player2 p3  p4   | height_names
  // +-----------------------------+ border_x
  // border_x width_points border_points width_graph border_x

  // the layout values
  int border_y = 0;
  // cppcheck-suppress variableScope
  int height_graph = 0;
  int border_roundno = 0;
  // cppcheck-suppress variableScope
  int height_roundno = 0;
  // cppcheck-suppress variableScope
  int border_names = 0;
  int height_names = 0;

  int border_x = 0;
  int width_points = 0;
  // cppcheck-suppress variableScope
  int border_points = 0;
  int width_graph = 0;

  int zero_x = 0;           // the x position of the zero point of the graph
  int zero_y = 0;           // the y position of the zero point of the graph
  double scale_games = 0;   // the distance beween two games
  double scale_points = 0;  // the distance between two points
  // note: the 'scale_points' distance is negative, so that the points go up
  int points_step = 0; // the steps beween two points markers

  // whether the player names are shown on the bottom (else on the top)
  bool const names_on_bottom = (counting != Counting::minus);

  // the layout for the name and the description
  auto text_layout = create_pango_layout("");
  text_layout->set_font_description(Pango::FontDescription("Sans 10"));

  // size of the markers (in each direction)
  int marker_pixel_size = 10;

  { // marker size
    int layout_width = 0, layout_height = 0;
    text_layout->set_text("0");
    text_layout->get_pixel_size(layout_width, layout_height);
    marker_pixel_size = layout_height / 2;
  } // marker size

  { // set the layout values
    { // y
      border_y = max((height / 50), 2);
      border_names = border_y / 2;
      { // height name
        height_names = 0;
        int layout_width = 0, layout_height = 0;
        for (auto const& player : party.players()) {
          text_layout->set_text(player.name());
          text_layout->get_pixel_size(layout_width, layout_height);
          if (layout_height > height_names)
            height_names = layout_height;
        }
      } // height name
      border_roundno = border_y / 2;
      if (counting == Counting::plusminus) {
        height_roundno = 0;
#ifdef WORKAROUND
        scale_points = 0;
#endif
        // ensure that there is enough place for the roundno below the graph
        if (min_points * scale_points
            < height_names + border_names + marker_pixel_size)
          height_roundno = ((height_names + border_names
                             + marker_pixel_size)
                            - static_cast<int>(min_points * scale_points));
      } else { // if !(counting = plusminus)
        height_roundno = height_names;
      } // if !(counting = plusminus)
      if (height < (2 * border_y
                    + height_names + border_names
                    + height_roundno + border_roundno)) {
        return true;
      }
      height_graph = (height - (2 * border_y
                                + height_names + border_names
                                + height_roundno + border_roundno));
      scale_points = - (height_graph
                        / static_cast<double>(max_points - min_points));
      zero_y = (border_y - static_cast<int>(max_points * scale_points));
      if (!names_on_bottom)
        zero_y += (height_names + border_names
                   + height_roundno + border_roundno);
    } // y

    { // x
      border_x = max((width / 50), 2);
      border_points = border_x / 2;
      { // width points
        vector<int> steps = {1, 2, 5};

        int layout_width = 0, layout_height = 0;
        text_layout->set_text("0");
        text_layout->get_pixel_size(layout_width, layout_height);

        auto const min_pixel_distance
          = (static_cast<int>(layout_height) * 3) / 2;
        points_step = 0;

        for (size_t i = 0; i < steps.size(); ++i) {
          auto const s = steps[i];
          if (height_graph / ((max_points - min_points) / s)
              >= min_pixel_distance) {
            points_step = s;
            break;
          }
          steps.push_back(10 * s);
        } // for (i < steps.size())
        if (points_step == 0) {
          return true;
        }

        width_points = 0;
        for (auto p = -((-min_points / points_step) * points_step);
             p <= max_points;
             p += points_step)  {
          text_layout->set_text(std::to_string(p));
          text_layout->get_pixel_size(layout_width, layout_height);
          if (width_points < layout_width)
            width_points = layout_width;
        }
      } // width points
      zero_x = border_x + width_points + border_points;
      if (width < zero_x + border_x) {
        return true;
      }
      width_graph = width - zero_x - border_x;

      scale_games = (width_graph / static_cast<double>(gamesno));
    } // x
  } // set the layout values

  { // draw the curves of the points of the players
    // reorder the players so that the human players are drawn last
    vector<unsigned> players;
    for (unsigned p = 0; p < party.players().size(); ++p)
      if (party.players()[p].type() != Player::Type::human)
        players.push_back(p);
    for (unsigned p = 0; p < party.players().size(); ++p)
      if (party.players()[p].type() == Player::Type::human)
        players.push_back(p);

    text_layout->set_font_description(Pango::FontDescription("Sans Bold 10"));
    for (auto const p : players) {
      // the y offset for the line of the player
      auto const player_offset_y = ( ((static_cast<int>(p) - 1) * 2 - 1)
                                    / (static_cast<int>(party.roundno())
                                       / 5 + 1) );
      string color = party.players()[p].color();
#ifdef WORKAROUND
      // set a default color if the player has none
      if (color.empty()) {
        static vector<string> const colors = {"blue", "red", "green", "gold"};
        if (p < colors.size())
          color = colors[p];
        else
          color = "black";
      } // if (color == "")
#endif
      cr->save();
      {
        auto const c = Gdk::RGBA(color);
        cr->set_source_rgb(c.get_red(), c.get_green(), c.get_blue());
      }

      // the name
      text_layout->set_text(party.players()[p].name());
      int layout_width = 0, layout_height = 0;
      text_layout->get_pixel_size(layout_width, layout_height);
      cr->move_to(zero_x
                  + (width_graph * p / party.players().size()) // NOLINT(bugprone-integer-division)
                  + (width_graph / party.players().size() // NOLINT(bugprone-integer-division)
                     - layout_width) / 2, // NOLINT(bugprone-integer-division)
                  ( names_on_bottom
                   ? (height
                      - border_y
                      - height_names)
                   : border_y
                  ));
      text_layout->show_in_cairo_context(cr);

      // the points
      cr->set_line_width(2);
      cr->translate(zero_x, zero_y + player_offset_y);
      cr->move_to(0, 0);
      for (unsigned g = 0; g < played_gamesno; ++g) {
        cr->line_to(scale_games * (g + 1),
                    scale_points * party.pointsum_till_game(g, p));
      } // for (g <= party.gameno())
      cr->stroke();

      cr->restore();
    } // for (p < party.players().size())
  } // draw the curves of the points of the players

  cr->set_line_width(0.5);
  { // zero lines
    cr->save();
    cr->set_source_rgb(0, 0, 0);
    cr->move_to(zero_x + width_graph, zero_y);
    cr->line_to(zero_x, zero_y);
    cr->line_to(zero_x, zero_y + static_cast<int>(scale_points * min_points));
    cr->stroke();
    cr->move_to(zero_x + width_graph, zero_y);
    cr->line_to(zero_x, zero_y);
    cr->line_to(zero_x, zero_y + static_cast<int>(scale_points * max_points));
    cr->stroke();
    cr->restore();
  } // zero lines
  { // round markers
    int round_step = 0;

    auto steps = vector<int>{1, 2, 5};

    int layout_width = 0, layout_height = 0;
    text_layout->set_font_description(Pango::FontDescription("Sans 10"));
    text_layout->set_text(std::to_string(roundsno));
    text_layout->get_pixel_size(layout_width, layout_height);

    // minimal pixel distance between two markers
    unsigned const min_pixel_distance = 2 * layout_width;
    (void) min_pixel_distance;

    if (roundsno == 0) {
      round_step = 2 * width_graph;
    } else { // if !(roundsno == 0)
      for (size_t i = 0; i < steps.size(); ++i) {
        auto const s = steps[i];
        if (width_graph / (roundsno / s) >= min_pixel_distance) {
          round_step = s;
          break;
        }
        steps.push_back(10 * s);
      } // for (i < steps.size())
    } // if !(roundsno == 0)
    if (round_step == 0) {
      return true;
    }

    // how many pixel the marker goes up/down
    int const marker_y_up = ( (counting == Counting::minus)
                             ? 0
                             : marker_pixel_size);

    int const marker_y_down = ( (counting == Counting::plus)
                               ? 0
                               : marker_pixel_size);

    for (unsigned r = 0; r <= roundsno; r += round_step) {
      int const x = zero_x + ((party.remaining_duty_soli() || party.status() == Party::Status::finished) && r == roundsno
                               ? width_graph
                               : static_cast<int>(scale_games * party.round_startgame(r)));
      cr->move_to(x, zero_y - marker_y_down);
      cr->rel_line_to(0, marker_y_up + marker_y_down);
      cr->stroke();
      int layout_width = 0, layout_height = 0; // cppcheck-suppress shadowVariable
      text_layout->set_text(std::to_string(r));
      text_layout->get_pixel_size(layout_width, layout_height);
      if (counting == Counting::minus) {
        cr->move_to(x  - (layout_width / 2), // NOLINT(bugprone-integer-division)
                    zero_y - marker_y_up - border_roundno - layout_height);
      } else { // if (counting == Counting::minus)
        cr->move_to(x - (layout_width / 2), // NOLINT(bugprone-integer-division)
                    zero_y + marker_y_down + border_roundno);
      } // if (counting == Counting::minus)
      text_layout->show_in_cairo_context(cr);
    }
  } // round markers
  { // point markers
    int layout_width = 0, layout_height = 0;
    for (int p = -((-min_points / points_step) * points_step);
         p <= max_points;
         p += points_step)  {
      text_layout->set_text(std::to_string(p));
      text_layout->get_pixel_size(layout_width, layout_height);
      cr->move_to(border_x + width_points
                  - layout_width,
                  zero_y + static_cast<int>(scale_points * p)
                  - layout_height / 2); // NOLINT(bugprone-integer-division)
      text_layout->show_in_cairo_context(cr);
      cr->move_to(zero_x,
                  zero_y + static_cast<int>(scale_points * p));
      cr->rel_line_to(marker_pixel_size, 0);
      cr->stroke();
    }
  } // point markers
  cr->pop_group_to_source();
  cr->paint();

  return true;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
