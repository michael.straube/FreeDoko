/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "party_summary.h"

#include "ui.h"
#include "main_window.h"
#include "program_updated.h"
#include "bug_report_replay.h"

#include "../../basetypes/fast_play.h"
#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../os/bug_report_replay.h"

#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gtkmm/treeview.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    playerno   number of players
 **/
PartySummary::PlayersSummaryModel::PlayersSummaryModel(unsigned const playerno) :
  player(playerno)
{
  add(name);
  for (unsigned p = 0; p < playerno; ++p)
    add(player[p]);
  add(visible);
}

/** constructor
 **
 ** @param    parent   the parent object
 **/
PartySummary::PartySummary(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::party summary"), *parent->ui->main_window, false)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this, &PartySummary::init));
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_show().connect(sigc::mem_fun(*this, &PartySummary::on_show));
#endif
} // PartySummary::PartySummary(Base* parent)

/** destructor
 **/
PartySummary::~PartySummary() = default;

/** create all subelements
 **/
void
PartySummary::init()
{
  auto& party = ui->party(); // cppcheck-suppress shadowVariable

  set_icon(ui->icon);

  set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

  get_content_area()->set_spacing(1 EX);

  { // action area
    new_party_button = Gtk::manage(new Gtk::Button(_("Button::new party")));
    add_action_widget(*new_party_button, Gtk::RESPONSE_CLOSE);
    new_party_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                                   &PartySummary::new_party_event)
                                                    );

    continue_party_button = Gtk::manage(new Gtk::Button(_("Button::continue party")));
    add_action_widget(*continue_party_button, Gtk::RESPONSE_CLOSE);
    continue_party_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                                        &PartySummary::continue_party_event)
                                                         );
    continue_party_button->set_can_default();
    continue_party_button->grab_default();
    continue_party_button->grab_focus();
  } // action area

  {
    roundnumber = Gtk::manage(new Gtk::Label(_("Roundnumber: %u", 0U)));
    get_content_area()->pack_start(*roundnumber, false, true);
  }
  {
    gamenumber = Gtk::manage(new Gtk::Label(_("Gamenumber: %u", 0U)));
    get_content_area()->pack_start(*gamenumber, false, true);
  }
  {
    remaining_rounds = Gtk::manage(new Gtk::Label(_("Remaining rounds: %u", 0U)));
    get_content_area()->pack_start(*remaining_rounds, false, true);
  }
  {
    remaining_points = Gtk::manage(new Gtk::Label(_("Remaining points: %i", 0)));
    get_content_area()->pack_start(*remaining_points, false, true);
  }
  {
    next_startplayer = Gtk::manage(new Gtk::Label(_("Next startplayer: %s", "Name")));
    get_content_area()->pack_end(*next_startplayer, false, true);
  }

  { // player
    player_model
      = std::make_unique<PlayersSummaryModel>(party.players().size());
    player_list
      = Gtk::ListStore::create(*player_model);
    points_row = *player_list->append();
    points_row[player_model->name] = _("points");
    duty_soli_row = *player_list->append();
    duty_soli_row[player_model->name] = _("duty soli");
    player_treeview
      = Gtk::manage(new Gtk::TreeView(player_list));
    player_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

    player_treeview->append_column("", player_model->name);
    for (unsigned p = 0; p < party.players().size(); ++p) {
      player_treeview->append_column("Name", player_model->player[p]);
      player_treeview->get_column_cell_renderer(1 + p)->set_property("xalign", 0.5);
    }

    get_content_area()->pack_start(*player_treeview, Gtk::PACK_EXPAND_WIDGET);
  } // player

  show_all_children();
  update();
  { // Umgehen der folgenden Warnung:
    // "FreeDoko – Turnierzusammenfassung" has broken size hints (inconsistent with current size).
    // fvwm is ignoring those hints.    hint override = 0, flags = 310
    // min_width = 320, min_height = 208, max_width = 0, max_height = 0
    int x_min = 0, x = 0;
    get_preferred_width(x_min, x);
  }
} // void PartySummary::init()

/** A party is loaded.
 ** Show this window.
 **/
void
PartySummary::party_loaded()
{
  if (::fast_play & FastPlay::party_start)
    return ;

  if (!get_realized())
    realize();

  update();

  if (ui->party().status() == Party::Status::initial_loaded)
    new_party_button->show();
  else
    new_party_button->hide();

  present();
  if (ui->bug_report_replay->is_visible())
    ui->bug_report_replay->raise();

  while (   is_visible()
         && !ui->thrower) {
    ::ui->wait();
    if (::bug_report_replay
        && ::bug_report_replay->auto_start_party())
      break;
  }

  hide();
} // void PartySummary::party_loaded()

/** show the window.
 ** Shows/hides the action buttons
 **/
void
PartySummary::on_show()
{
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  StickyDialog::on_show();
#endif

  if (ui->program_updated_window)
    ui->program_updated_window->raise();

  update();
} // void PartySummary::show()

/** update (rewrite) all information
 **
 ** @todo   remaining points
 ** @todo   duty soli
 **/
void
PartySummary::update()
{
  if (!get_realized())
    return ;

  if (   ui->party().status() != Party::Status::initial_loaded
      && ui->party().status() != Party::Status::loaded)
    return ;

  Party const& party = ui->party(); // cppcheck-suppress shadowVariable

  roundnumber->set_label(_("Roundnumber: %u",
                                 party.roundno() + 1));
  gamenumber->set_label(_("Gamenumber: %u",
                                party.gameno() + 1));
  if (party.rule()(Rule::Type::number_of_rounds_limited))
    remaining_rounds->set_label(_("Remaining rounds: %u",
                                        party.remaining_rounds()));
  else
    remaining_rounds->hide();
  if (party.rule()(Rule::Type::points_limited))
    remaining_points->set_label(_("Remaining points: %i",
                                        party.remaining_points()));
  else
    remaining_points->hide();

  next_startplayer->set_label(_("Next startplayer: %s",
                                      party.players()[party.startplayer()].name()));

#ifdef WORKAROUND
  // Wenn der Text neu gesetzt wird, ändert sich die Größe vom Fenster mehrmals schrittweise.
  if (continue_party_button->get_label()
      != (party.is_finished()
          ? _("Button::finish party")
          : _("Button::continue party")))
#endif
  {
    continue_party_button->set_label(party.is_finished()
                                           ? _("Button::finish party")
                                           : _("Button::continue party"));
  }
  duty_soli_row[player_model->visible]
    = (party.rule()(Rule::Type::number_of_duty_soli) > 0);
  for (unsigned p = 0; p < party.players().size(); ++p) {
    player_treeview->get_column(1 + p)->set_title(party.players()[p].name());
    points_row[player_model->player[p]] = party.pointsum(p);
    duty_soli_row[player_model->player[p]]
      = party.remaining_duty_soli(p);
  }
} // void PartySummary::update()

/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
PartySummary::name_update(Player const& player)
{
  if (!get_realized())
    return ;

  auto const p = ui->party().players().no(player);
  if (p + 1 >= player_treeview->get_columns().size())
    return ;

  player_treeview->get_column(p + 1)->set_title(player.name());
}

/** event, that the party shall be continued
 **/
void
PartySummary::continue_party_event()
{
  hide();
  ui->main_quit();
} // void Party

/** event, that a new party shall be started
 **/
void
PartySummary::new_party_event()
{
  ui->thrower(Party::Status::init, __FILE__, __LINE__);

  hide();
  ui->main_quit();
} // void PartySummary::new_party_event()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
