/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "widgets/sticky_dialog.h"
#include <gtkmm/liststore.h>

class Player;

namespace Gtk {
class TreeView;
class Button;
class Label;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the summary of a party
 ** The summary is shown, when a party has been loaded.
 ** The summary contains:
 **   players
 **     points
 **     duty soli
 **   remaining points
 **   remaining rounds
 **   gamenumber
 **   next startplayer
 **/
class PartySummary : public Base, public Gtk::StickyDialog {
  /**
   ** the columns of the players table
   **/
  struct PlayersSummaryModel : public Gtk::TreeModel::ColumnRecord {
    PlayersSummaryModel(unsigned playerno);

    Gtk::TreeModelColumn<Glib::ustring> name;
    vector<Gtk::TreeModelColumn<int> > player;
    Gtk::TreeModelColumn<bool> visible;
  }; // struct PlayersSummartyTable : public Gtk::TreeModel::ColumnRecord

  public:
  explicit PartySummary(Base* parent);
  ~PartySummary() override;

  PartySummary() = delete;
  PartySummary(PartySummary const&) = delete;
  PartySummary& operator=(PartySummary const&) = delete;

  // the party is loaded
  void party_loaded();
  // the window is shown
  void on_show() override;

  // update the name of the player
  void name_update(Player const& player);

  private:
  // initialize the window
  void init();
  // update the window
  void update();

  // the party shall be continued
  void continue_party_event();
  // a new party shall be started
  void new_party_event();

  private:
  AutoDisconnector disconnector_;

  std::unique_ptr<PlayersSummaryModel> player_model = nullptr;
  Glib::RefPtr<Gtk::ListStore> player_list;
  Gtk::TreeRow points_row;
  Gtk::TreeRow duty_soli_row;
  Gtk::TreeView* player_treeview = nullptr;

  Gtk::Label* roundnumber = nullptr;
  Gtk::Label* gamenumber = nullptr;
  Gtk::Label* remaining_rounds = nullptr;
  Gtk::Label* remaining_points = nullptr;
  Gtk::Label* next_startplayer = nullptr;

  Gtk::Button* continue_party_button = nullptr;
  Gtk::Button* new_party_button = nullptr;
}; // class PartySummary : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
