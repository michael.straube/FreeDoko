/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "player.h"
#include "aiconfig.h"
#include "ui.h"

#include "../../party/party.h"
#include "../../player/player.h"
#include "../../player/aiconfig.h"
#include "../../player/ai/ai.h"
#include "../../misc/preferences.h"
#include "../../utils/random.h"

#include "widgets/filemenu_extension.h"
#include "widgets/filemenu_file.h"
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/box.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    players    parent object
 ** @param    playerno   number of the player
 **/
Players::Player::Player(Players* const players, unsigned const playerno) :
  Base(players),
  players(players),
  playerno_(playerno),
  player_bak(player().clone())
{
  init();
} // Players::Player::Player(Players* players, unsigned playerno)

/** Destruktor
 **/
Players::Player::~Player()
{
  for (auto type : type_button)
    delete static_cast<PlayerType*>(type->steal_data("type"));
} // Players::Player::~Player()

/** init all subelements
 **/
void
Players::Player::init()
{
  aiconfig_ = std::make_unique<AiConfig>(this);

  name_label = Gtk::manage(new Gtk::Label("name"));
  name_entry = Gtk::manage(new Gtk::Entry());

  random_name_button = Gtk::manage(new Gtk::Button(_("Button::random name")));

  voice_button = Gtk::manage(new Gtk::Button("voice"));
  voice_menu
    = Gtk::manage(new Gtk::FileMenuFilterFile(sigc::mem_fun(*this, &Players::Player::voice_selected), "re.wav"));

  reset_button = Gtk::manage(new Gtk::Button(_("Button::reset")));
  reset_button->set_image_from_icon_name("edit-undo");
  reset_button->set_always_show_image();

  { // the player types
    Gtk::RadioButton::Group group;
    // the type is 'int', because there are compilation errors with
    // '::Player::Type'
    // Human
    type_button.push_back(Gtk::manage(new Gtk::RadioButton(group,
                                                                 _(PlayerType(::Player::Type::human)))));
    type_button.back()->set_data("type",
                                       new PlayerType(::Player::Type::human));
    // Ai
    type_button.push_back(Gtk::manage(new Gtk::RadioButton(group,
                                                                 _(PlayerType(::Player::Type::ai)))));
    type_button.back()->set_data("type",
                                       new PlayerType(::Player::Type::ai));
#ifndef RELEASE
    // AiDummy
    type_button.push_back(Gtk::manage(new Gtk::RadioButton(group,
                                                                 _(PlayerType(::Player::Type::ai_dummy)))));
    type_button.back()->set_data("type",
                                       new PlayerType(::Player::Type::ai_dummy));
    // AiRandom
    type_button.push_back(Gtk::manage(new Gtk::RadioButton(group,
                                                                 _(PlayerType(::Player::Type::ai_random)))));
    type_button.back()->set_data("type",
                                       new PlayerType(::Player::Type::ai_random));
#endif

  } // the player types

  { // signals
    //name_entry->signal_changed().connect(sigc::mem_fun(*this, &Player::name_change));
    name_entry->signal_focus_out_event().connect(sigc::mem_fun(*this, &Player::name_changed_event));
    random_name_button->signal_clicked().connect(sigc::mem_fun(*this, &Player::random_name));

    voice_button->signal_clicked().connect(sigc::mem_fun(*voice_menu, &Gtk::FileMenu::show));
    for (auto const& d : ::preferences.data_directories())
      voice_menu->add_directory(d / "sounds" / ::preferences(::Preferences::Type::language));

    for (auto type : type_button)
      type->signal_toggled().connect(sigc::bind<PlayerType const>(sigc::mem_fun(*this, &Player::type_change),
                                                                  *static_cast<PlayerType*>(type->get_data("type"))));

    reset_button->signal_clicked().connect(sigc::mem_fun(*this, &Player::reset));
  } // signals
} // void Players::Player::init()

/** @return   the player
 **/
::Player const&
Players::Player::player() const
{
  return ui->party().players()[playerno()];
} // ::Player const& Players::Player::player() const

/** @return   the player
 **/
::Player&
Players::Player::player()
{
  return ui->party().players()[playerno()];
} // ::Player& Players::Player::player()

/** @return   the aiconfig
 **/
Players::Player::AiConfig&
Players::Player::aiconfig()
{
  return *aiconfig_;
} // Players::Player::AiConfig& Players::Player::aiconfig()

/** @return   the playernumber
 **/
unsigned
Players::Player::playerno() const
{
  return playerno_;
} // unsigned Players::Player::playerno() const

/** creates a backup
 **/
void
Players::Player::create_backup()
{
  player_bak = std::unique_ptr<::Player>(player().clone());
} // void Players::Player::create_backup()

/** resets the settings
 **/
void
Players::Player::reset()
{
  if (player_bak)
    ui->party().players().set(playerno(), player_bak->clone());
  update();
} // void Players::Player::reset()

/** the name has changed
 **
 ** @param    focus   ignored
 **/
bool
Players::Player::name_changed_event(GdkEventFocus* focus)
{
  if (player().name()
      != name_entry->get_text())
    player().person().set_name(name_entry->get_text());

  return true;
} // bool Players::Player::name_changed_event(GdkEventFocus* focus)

/** create a random name
 **/
void
Players::Player::random_name()
{
#include "namen.h"
  unsigned p = 0;
  auto name = namensliste[::random_value(namensliste.size())];
  do {
    for (p = 0; p < ui->party().players().size(); ++p)
      if (ui->party().players()[p].name() == name)
        break;
    name = namensliste[::random_value(namensliste.size())];
  } while (p < ui->party().players().size()) ;
  player().person().set_name(name);
} // void Players::Player::random_name()

/** a voice is selected
 **
 ** @param    voice   the selected voice
 **/
void
Players::Player::voice_selected(string const& voice)
{
  player().person().set_voice(voice);
} // void Players::Player::voice_selected(string voice)

/** the type has changed
 **
 ** @param    type   new type
 **/
void
Players::Player::type_change(PlayerType const type)
{
  for (auto typei : type_button) {
    if (*static_cast<PlayerType*>(typei->get_data("type")) == type) {
      if (typei->get_active())
        ui->party().players().set_type(playerno(), type);
      break;
    }
  } // for (typei : type_button)
} // void Players::Player::type_change(PlayerType type)

/** update the aiconfig
 **/
void
Players::Player::aiconfig_update()
{
  aiconfig().update();
} // void Players::Player::aiconfig_update()

/** update the sensitivity of all widgets
 **/
void
Players::Player::sensitivity_update()
{
  bool const sensitive = (ui->party().status() == Party::Status::init);

  for (auto t : type_button) {
    t->set_sensitive(sensitive);
  } // for (auto t : type_button)
  reset_button->set_sensitive(sensitive);
}

/** update all widgets
 **/
void
Players::Player::update()
{
  sensitivity_update();
  name_update();
  voice_update();
  for (auto type : type_button)
    if (*static_cast<PlayerType*>(type->get_data("type"))
        == player().type()) {
      type->set_active();
      break;
    }

  aiconfig().update();
} // void Players::Player::update()

/** update the name
 **
 ** @todo   block 'Entry::change_signal()'
 **/
void
Players::Player::name_update()
{
  // ToDo: block 'Entry::change_signal()'
  // Now, the entry is first cleared (that leads to an inserting of the
  // default name), afterwards the new name is inserted.
  // So in the end the name is the old name followed by the default name.

  // workaround
  static bool in_change = false;
  if (!in_change) {
    in_change = true;
    name_entry->set_text(player().name());
    name_label->set_text(player().name());
    in_change = false;
  } // if (!in_change)
} // void Players::Player::name_update()

/** update the voice
 **/
void
Players::Player::voice_update()
{
  voice_button->set_label(player().voice());
} // void Players::Player::voice_update()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
