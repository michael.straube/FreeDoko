/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "widgets/sticky_dialog.h"

#include "../../player/player.h"

class Player;
class Aiconfig;
#include <gtkmm/notebook.h>

namespace Gtk {
class Grid;
class ToggleButton;
class Entry;
}; // namespace Gtk
namespace UI_GTKMM_NS {

/**
 ** the window with all players
 **
 ** @todo   export into a window
 **/
class Players : public Base, public Gtk::StickyDialog {
  public:
    class Player;

  public:
    explicit Players(Base* parent);
    ~Players() override;

    Players() = delete;
    Players(Players const&) = delete;
    Players& operator=(Players const&) = delete;

    // the player settings
    Player& player(unsigned playerno);
    // the player settings
    Player& player(::Player const& player);

    // create a backup for all players
    void create_backup();
    // reset all players
    void reset();

    // update the sensitivity
    void sensitivity_update();
    // update all players
    void update();
    // two players have swapped
    void players_swapped(::Player const& player_a, ::Player const& player_b);
    // update a player
    void player_update(::Player const& player);
    // update a player name
    void name_update(::Player const& player);
    // update the voice of a player
    void voice_update(::Player const& player);
    // update the aiconfig of a player
    void aiconfig_update(::Aiconfig const& aiconfig);

  private:
    // initialize all
    void init();

    // signal: page switched
    void notebook_switch_page_signal(Widget* widget, guint pageno,
                                     Gtk::Notebook* notebook);

    // a key was pressed
    bool on_key_press_event(GdkEventKey* key) override;

  private:
    AutoDisconnector disconnector_;

    vector<std::unique_ptr<Player>> player_;
    Gtk::Notebook* players_notebook = nullptr;
}; // class Players : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
