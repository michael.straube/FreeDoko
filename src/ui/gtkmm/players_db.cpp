/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "players_db.h"
#include "ui.h"
#include "cards.h"

#include "../../party/party.h"
#include "../../player/player.h"
#include "../../player/playersDb.h"
#include "../../player/ai/aiDb.h"

#include "../../utils/string.h"


#include <gtkmm/treeview.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/scrolledwindow.h>
#include <gdk/gdkkeysyms.h>
namespace UI_GTKMM_NS {

/** -> result
 **
 ** @param    statistic   statistic
 **
 ** @return   string of 'statistic'
 **/
string
  PlayersDB::to_string(Statistic const statistic)
  {
    switch(statistic) {
    case Statistic::total:
      (void)_("Statistic::total");
      return "total";
    case Statistic::won:
      (void)_("Statistic::won");
      return "won";
    case Statistic::lost:
      (void)_("Statistic::lost");
      return "lost";
    case Statistic::percent_won:
      (void)_("Statistic::percent won");
      return "percent won";
    }

    return "";
  }

/** translation
 **
 ** @param    statistic   statistic
 **
 ** @return   translation
 **/
string
  PlayersDB::gettext(Statistic const statistic)
  {
    return ::gettext("Statistic::" + to_string(statistic));
  }

/** Constructor for the model
 **
 ** @param    playerno   number of players in the party
 **/
PlayersDB::PlayersDBModel::PlayersDBModel(unsigned const playerno) :
  statistic(playerno)
{
  add(type);
  for (unsigned p = 0; p < playerno; p++)
    add(statistic[p]);
}

/** constructor
 **
 ** @param    parent   the parent object
 **/
PlayersDB::PlayersDB(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::players database"), false)
{
  ui->add_window(*this);

  set_default_size(0, 3 * ui->cards->height());
  signal_realize().connect(sigc::mem_fun(*this, &PlayersDB::init));

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &PlayersDB::on_key_press_event));
#endif
}

/** destructor
 **/
PlayersDB::~PlayersDB() = default;

/** create all subelements
 **/
void
PlayersDB::init()
{
  set_icon(ui->icon);

  { // hbox
    auto hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 1 EM));
    { // statistic type
      auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));

      Gtk::RadioButton::Group statistic_group;
      for (auto statistic : statistic_all) {
        auto const radio_button
          = Gtk::manage(new Gtk::RadioButton(statistic_group,
                                             gettext(statistic)));
        vbox->add(*radio_button);
        radio_button->signal_clicked().connect(sigc::bind<Statistic const>(sigc::mem_fun(*this, &PlayersDB::set_statistic), statistic));
        if (statistic == Statistic::percent_won)
          radio_button->set_active(true);

      }
      vbox->set_valign(Gtk::ALIGN_START);
      hbox->add(*vbox);
    } // statistic type

    { // the treeview

      // the points table
      players_db_model = std::make_unique<PlayersDBModel>(ui->party().players().size());

      players_db_list
        = Gtk::TreeStore::create(*players_db_model);
      players_db_treeview
        = Gtk::manage(new Gtk::TreeView(players_db_list));

      players_db_treeview->append_column(_("players database data type"),
                                               players_db_model->type);
      players_db_treeview->get_column_cell_renderer(0)->set_property("xalign", 1);
      for (unsigned p = 0; p < ui->party().players().size(); p++) {
        players_db_treeview->append_column(ui->party().players()[p].name(),
                                                 players_db_model->statistic[p]);
        players_db_treeview->get_column_cell_renderer(p + 1)->set_property("xalign", 0.5);
      }

      { // the scrolled window
        auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
        scrolled_window->set_policy(Gtk::POLICY_NEVER,
                                    Gtk::POLICY_AUTOMATIC);
        scrolled_window->add(*(players_db_treeview));

        hbox->pack_start(*scrolled_window);
      } // the scrolled window
    } // the treeview
    get_content_area()->pack_start(*hbox);
  } // hbox

  { // action area
    { // clear button
      auto clear_button = Gtk::manage(new Gtk::Button(_("Button::clear database")));
      clear_button->set_image_from_icon_name("edit-clear-all");
      clear_button->set_always_show_image();
      add_action_widget(*clear_button, Gtk::RESPONSE_NONE);
      clear_button->signal_clicked().connect(sigc::mem_fun(*this, &PlayersDB::clear_db));
    } // clear button

    add_close_button(*this);
  } // action area

  show_all_children();

  signal_show().connect(sigc::mem_fun(*this,
                                            &PlayersDB::recreate_db));
  recreate_db();

  Party::signal_open.connect_back([this](Party&) { recreate_db(); }, disconnector_);
  party->signal_close.connect_back(*this, &PlayersDB::hide, disconnector_);
}

/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
PlayersDB::name_changed(Player const& player)
{
  if (!get_realized())
    return ;

  players_db_treeview->get_column(ui->party().players().no(player) + 1
                                       )->set_title(" " + player.name() + " ");
}

/** recreates the table
 **/
void
PlayersDB::recreate_db()
{
  if (!get_realized())
    return ;

  if (!players_db_list)
    return ;

  players_db_list->clear();

  { // ranking
    ranking = *players_db_list->append();
    ranking[players_db_model->type]
      = _("ranking");
  } // ranking

  { // average game points
    average_game_points = *players_db_list->append();
    average_game_points[players_db_model->type]
      = _("average game points");
  } // average game points

  { // average game trick points
    average_game_trick_points = *players_db_list->append();
    average_game_trick_points[players_db_model->type]
      = _("average game trick points");
  } // average game trick points

  {
    for (unsigned p = 0; p < 4; ++p) {
      ranking[players_db_model->statistic[p]]
        = statistic_data(TLWcount());
    }
  }

  { // game type
    games = *players_db_list->append();
    games[players_db_model->type]
      = _("games");
    games_marriage
      = *(players_db_list->append(games.children()));
    games_marriage[players_db_model->type]
      = _("GameType::Group::marriage");
    games_poverty
      = *(players_db_list->append(games.children()));
    games_poverty[players_db_model->type]
      = _("GameType::Group::poverty");
    games_solo
      = *(players_db_list->append(games.children()));
    games_solo[players_db_model->type]
      = _("GameType::Group::solo");
    games_solo_color
      = *(players_db_list->append(games_solo.children()));
    games_solo_color[players_db_model->type]
      = _("GameType::Group::Solo::color");
    games_solo_picture
      = *(players_db_list->append(games_solo.children()));
    games_solo_picture[players_db_model->type]
      = _("GameType::Group::Solo::picture");
    games_solo_picture_single
      = *(players_db_list->append(games_solo_picture.children()));
    games_solo_picture_single[players_db_model->type]
      = _("GameType::Group::Solo::Picture::single");
    games_solo_picture_double
      = *(players_db_list->append(games_solo_picture.children()));
    games_solo_picture_double[players_db_model->type]
      = _("GameType::Group::Solo::Picture::double");

    game.clear();
    for (auto const gt : game_type_list) {
      switch (gt) {
      case GameType::normal:
        game.push_back(*players_db_list->prepend(games.children()));
        break;
      case GameType::thrown_nines:
      case GameType::thrown_kings:
      case GameType::thrown_nines_and_kings:
      case GameType::thrown_richness:
      case GameType::fox_highest_trump:
      case GameType::redistribute:
      case GameType::poverty:
        game.push_back(*players_db_list->append(games_poverty.children()));
        break;
      case GameType::marriage:
      case GameType::marriage_solo:
      case GameType::marriage_silent:
        game.push_back(*players_db_list->append(games_marriage.children()));
        break;
      case GameType::solo_jack:
      case GameType::solo_queen:
        game.push_back(*players_db_list->append(games_solo_picture_single.children()));
        break;
      case GameType::solo_king:
      case GameType::solo_queen_jack:
      case GameType::solo_king_jack:
      case GameType::solo_king_queen:
        game.push_back(*players_db_list->append(games_solo_picture_double.children()));
        break;
      case GameType::solo_koehler:
        game.push_back(*players_db_list->append(games_solo_picture.children()));
        break;
      case GameType::solo_club:
      case GameType::solo_heart:
      case GameType::solo_spade:
      case GameType::solo_diamond:
        game.push_back(*players_db_list->append(games_solo_color.children()));
        break;
      case GameType::solo_meatless:
        game.push_back(*players_db_list->append(games_solo.children()));
        break;
#if 0
      default:
        DEBUG_ASSERTION(false,
                        "UI_GTKMM::PlayersDB::recreate_db()\n"
                        "  game type '" << static_cast<GameType>(gt) << "' is not considered");
        break;
#endif
      }
      game.back()[players_db_model->type] = _(gt);
    }
  } // game type

  { // special points
    specialpoints = *players_db_list->append();
    specialpoints[players_db_model->type]
      = _("Specialpoint::specialpoints");
    specialpoints_winning
      = *players_db_list->append(specialpoints.children());
    specialpoints_winning[players_db_model->type]
      = _("Specialpoint::Group::winning");
    specialpoints_announcement
      = *players_db_list->append(specialpoints.children());
    specialpoints_announcement[players_db_model->type]
      = _("Specialpoint::Group::announcement");

    specialpoint.clear();
    for (auto const sp : Specialpoint::types) {
      switch (sp) {
      case Specialpoint::Type::nospecialpoint:
      case Specialpoint::Type::caught_fox:
      case Specialpoint::Type::caught_fox_last_trick:
      case Specialpoint::Type::fox_last_trick:
      case Specialpoint::Type::charlie:
      case Specialpoint::Type::caught_charlie:
      case Specialpoint::Type::dulle_caught_dulle:
      case Specialpoint::Type::heart_trick:
      case Specialpoint::Type::doppelkopf:
        specialpoint[sp] = *players_db_list->prepend(specialpoints.children());
        break;
      case Specialpoint::Type::won:
      case Specialpoint::Type::no90:
      case Specialpoint::Type::no60:
      case Specialpoint::Type::no30:
      case Specialpoint::Type::no0:
      case Specialpoint::Type::contra_won:
      case Specialpoint::Type::solo:
      case Specialpoint::Type::bock:
        specialpoint[sp] = *players_db_list->append(specialpoints_winning.children());
        break;
      case Specialpoint::Type::no120_said:
      case Specialpoint::Type::no90_said:
      case Specialpoint::Type::no60_said:
      case Specialpoint::Type::no30_said:
      case Specialpoint::Type::no0_said:
      case Specialpoint::Type::no90_said_120_got:
      case Specialpoint::Type::no60_said_90_got:
      case Specialpoint::Type::no30_said_60_got:
      case Specialpoint::Type::no0_said_30_got:
      case Specialpoint::Type::announcement_reply:
        specialpoint[sp] = *players_db_list->append(specialpoints_announcement.children());
        break;
      }
      specialpoint[sp][players_db_model->type] = _(sp);
    }
  } // special points

  { // heuristics
    heuristics = *players_db_list->append();
    heuristics[players_db_model->type]
      = _("AiConfig::Group::heuristics");
    heuristics_general
      = *players_db_list->append(heuristics.children());
    heuristics_general[players_db_model->type]
      = _("AiConfig::Heuristic::Group::normal game");
    heuristics_marriage
      = *players_db_list->append(heuristics.children());
    heuristics_marriage[players_db_model->type]
      = _("AiConfig::Heuristic::Group::marriage");
    heuristics_poverty
      = *players_db_list->append(heuristics.children());
    heuristics_poverty[players_db_model->type]
      = _("AiConfig::Heuristic::Group::poverty");
    heuristics_solo
      = *players_db_list->append(heuristics.children());
    heuristics_solo[players_db_model->type]
      = _("AiConfig::Heuristic::Group::solo");
    heuristics_solo_color
      = *players_db_list->append(heuristics_solo.children());
    heuristics_solo_color[players_db_model->type]
      = _("AiConfig::Heuristic::Group::Solo::color");
    heuristics_solo_picture
      = *players_db_list->append(heuristics_solo.children());
    heuristics_solo_picture[players_db_model->type]
      = _("AiConfig::Heuristic::Group::Solo::picture");
    heuristics_solo_meatless
      = *players_db_list->append(heuristics_solo.children());
    heuristics_solo_meatless[players_db_model->type]
      = _("AiConfig::Heuristic::Group::Solo::meatless");

    heuristic.clear();
    for (auto const heuristic : aiconfig_heuristic_list) {
      switch (heuristic) {
      case Aiconfig::Heuristic::error:
      case Aiconfig::Heuristic::no_heuristic:
      case Aiconfig::Heuristic::manual:
      case Aiconfig::Heuristic::bug_report:
        this->heuristic[heuristic] = heuristics;
        break;
      case Aiconfig::Heuristic::only_one_valid_card:
      case Aiconfig::Heuristic::valid_card:
      case Aiconfig::Heuristic::gametree:
        this->heuristic[heuristic] = *players_db_list->insert(heuristics_general);
        break;
      default:
        if (is_general(heuristic)) {
          this->heuristic[heuristic] = *players_db_list->append(heuristics_general.children());
        } else if (for_marriage(heuristic)) {
          this->heuristic[heuristic] = *players_db_list->append(heuristics_marriage.children());
        } else if (for_poverty(heuristic)) {
          this->heuristic[heuristic] = *players_db_list->append(heuristics_poverty.children());
        } else if (for_color_solo(heuristic)) {
          this->heuristic[heuristic] = *players_db_list->append(heuristics_solo_color.children());
        } else if (for_picture_solo(heuristic)) {
          this->heuristic[heuristic] = *players_db_list->append(heuristics_solo_picture.children());
        } else if (for_meatless_solo(heuristic)) {
          this->heuristic[heuristic] = *players_db_list->append(heuristics_solo_meatless.children());
        } else {
          DEBUG_ASSERTION(false,
                          "PlayersDB::recreate_db()\n"
                          "heuristic " << heuristic << " not allocated.");
        }
        break;
      }
      this->heuristic[heuristic][players_db_model->type] = _(heuristic);
    }
  } // heuristics

  update_db();

  players_db_treeview->show_all();
}

/** updates the statistics
 **/
void
PlayersDB::update_db()
{
  if (!get_realized())
    return ;

  if (!players_db_list)
    return ;

  Party const& party = ui->party();

  for (unsigned p = 0; p < party.players().size(); ++p) {
    auto const& player = ui->party().players()[p];
    auto const& db = player.db();

    ranking[players_db_model->statistic[p]]
      = String::to_string(static_cast<int>(db.rank.value() * 10) / 10.0);

    average_game_points[players_db_model->statistic[p]]
      = String::to_string(static_cast<int>(db.averageGamePoints() * 10) / 10.0);

    average_game_trick_points[players_db_model->statistic[p]]
      = String::to_string(static_cast<int>(db.averageGameTrickPoints() * 10) / 10.0);

    { // game type
      games[players_db_model->statistic[p]]
        = statistic_data(db.games_all());
      games_marriage[players_db_model->statistic[p]]
        = statistic_data(db.games_group_marriage());
      games_marriage[players_db_model->statistic[p]]
        = statistic_data(db.games_group_marriage());
      games_poverty[players_db_model->statistic[p]]
        = statistic_data(db.games_group_poverty());
      games_solo[players_db_model->statistic[p]]
        = statistic_data(db.games_group_solo());
      games_solo_color[players_db_model->statistic[p]]
        = statistic_data(db.games_group_solo_color());
      games_solo_picture[players_db_model->statistic[p]]
        = statistic_data(db.games_group_solo_picture());
      games_solo_picture_single[players_db_model->statistic[p]]
        = statistic_data(db.games_group_solo_picture_single());
      games_solo_picture_double[players_db_model->statistic[p]]
        = statistic_data(db.games_group_solo_picture_double());
      for (auto gt : game_type_list) {
        game[static_cast<int>(gt)][players_db_model->statistic[p]]
          = statistic_data(db.games(gt));
      }
    } // game type

    { // special points
      specialpoints[players_db_model->statistic[p]]
        = statistic_data(db.specialpoints_all());
      specialpoints_winning[players_db_model->statistic[p]]
        = statistic_data(db.specialpoints_group_winning());
      specialpoints_announcement[players_db_model->statistic[p]]
        = statistic_data(db.specialpoints_group_announcement());
      for (auto sp : Specialpoint::types) {
        specialpoint[sp][players_db_model->statistic[p]]
          = statistic_data(db.specialpoints(sp));
      }
    } // special points
    { // heuristics
      if (dynamic_cast<AiDb const*>(&db) == nullptr)
        continue;
      auto const& aidb = dynamic_cast<AiDb const&>(db);
      auto const total = aidb.heuristic(Aiconfig::Heuristic::no_heuristic);

      heuristics[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, total);
      heuristics_general[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_general());
      heuristics_marriage[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_marriage());
      heuristics_poverty[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_poverty());
      heuristics_solo[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_solo());
      heuristics_solo_color[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_solo_color());
      heuristics_solo_picture[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_solo_picture());
      heuristics_solo_meatless[players_db_model->statistic[p]]
        = heuristic_statistic_data(total, aidb.heuristics_group_solo_meatless());
      for (auto const heuristic : aiconfig_heuristic_list) {
        if (this->heuristic[heuristic] == heuristics)
          continue;
        this->heuristic[heuristic][players_db_model->statistic[p]]
          = heuristic_statistic_data(total, aidb.heuristic(heuristic));
      }
    } // heuristics
  }
}

/** sets the statistic
 **
 ** @param    statistic   new statistic
 **/
void
PlayersDB::set_statistic(Statistic const statistic)
{
  this->statistic = statistic;
  update_db();
}

/** clears the database
 **
 ** @todo    remove 'const cast'
 **/
void
PlayersDB::clear_db()
{
  for (auto& p : ui->party().players())
    p.db().clear();

  update_db();
}

/** -> result
 **
 ** @param    tlwcount   counter
 **
 ** @return   string of the data analysed by 'statistic'
 **/
Glib::ustring
PlayersDB::statistic_data(TLWcount const& tlwcount) const
{
  switch(statistic) {
  case Statistic::total:
    return String::to_string(tlwcount.total());
  case Statistic::won:
    if (tlwcount.total() > 0)
      return String::to_string(tlwcount.won());
    else
      return "-";
  case Statistic::lost:
    if (tlwcount.total() > 0)
      return String::to_string(tlwcount.lost());
    else
      return "-";
  case Statistic::percent_won:
    if (tlwcount.total() > 0)
      return (String::to_string(tlwcount.won() * 100
                                / tlwcount.total())
              + "%");
    else
      return "-";
  }

  return "-";
}

/** -> result
 **
 ** @param    total   total number of heuristics
 ** @param    num     number of the active heuristics
 **
 ** @return   string of the data analysed by 'statistic'
 **/
Glib::ustring
PlayersDB::heuristic_statistic_data(unsigned const total,
                                    unsigned const num) const
{
  if (num == 0)
    return "-";

  switch(statistic) {
  case Statistic::total:
  case Statistic::won:
  case Statistic::lost:
    return String::to_string(num);
  case Statistic::percent_won:
    return (String::to_string((num * 100 / total))
            + "."
            + String::to_string((num * 1000 / total) % 10)
            + "%");
  }

  return "-";
}

/** a key has been pressed
 ** C-o: output of the statistics on 'stdout'
 **
 ** @param    key   the key
 **
 ** @return   whether the key was managed
 **/
bool
PlayersDB::on_key_press_event(GdkEventKey* const key)
{
  bool managed = false;

  if ((key->state & ~GDK_SHIFT_MASK) == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_o: // ouput of the preferences
      for (auto& p : ui->party().players()) {
        cout << p.name() << '\n'
          << "{\n"
          <<   p.db()
          << "}\n";
      }
      managed = true;
      break;
    }
  }

  return (managed
          || StickyDialog::on_key_press_event(key)
          || ui->key_press(key));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
