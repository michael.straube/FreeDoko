/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "widgets/sticky_dialog.h"
#include <gtkmm/notebook.h>
namespace Gtk {
class Widget;
class Notebook;
class Label;
class Alignment;
class Button;
class CheckButton;
class RadioButton;
class LabelSpinButton;
class Entry;
class Image;
class FontChooserDialog;
class ColorChooserDialog;
class Menu;
class Grid;
} // namespace Gtk

#include "../../misc/preferences.cardsorder.h"
#include "../../misc/preferences_type.h"
class Preferences;

namespace UI_GTKMM_NS {

/**
 ** the preferences dialog
 **
 ** @todo   update of the cardset in 'cards order'
 ** @todo   change the style of the fonts and colors buttons
 ** @todo   alpha-channel for the colors
 ** @todo   on-the-fly changing of the fonts
 **/
class Preferences : public Base, public Gtk::StickyDialog {
  public:
    class CardsOrder;
    class Selection; // base class
    class Cardset;
    class CardsBack;
    class Iconset;
    class Background;
  public:
    explicit Preferences(Base* parent);
    ~Preferences() override;

    void create_backup();
    ::Preferences const& backup() const;

    void reset();
    void save();

    void sensitivity_update();
    void update_all();
    void update(int type, bool update_sensitivity = true);
    void changed(int type);
    bool focus_out_event(GdkEventFocus* event, int type);

    void open_cards_order();

    void language_menu_create(bool popup);
    void language_selected(string language);
    void cardset_selected(string cardset);

    void font_chooser_response(int response_id,
                               Gtk::FontChooserDialog* font_chooser);
    void color_chooser_response(int response_id,
                                Gtk::ColorChooserDialog* color_chooser);

  private:
    void init();
    void init_cardsorder();

    Gtk::Box* add_group_box(string const& group_name, string const& icon_name);
    Gtk::Notebook* add_group_notebook(string const& group_name, string const& icon_name);
    void add_group(string const& group_name, string const& icon_name, Gtk::Widget& widget);
    Gtk::Box* add_subgroup_box(Gtk::Notebook& subgroup_notebook,
                               string const& subgroup_name);

    bool on_key_press_event(GdkEventKey* key) override;

  public:
    Gtk::Button* reset_button = nullptr;

    Gtk::Notebook* group_notebook = nullptr;

    std::map<::Preferences::Type::Bool, Gtk::CheckButton*> type_bool;
    std::map<::Preferences::Type::Unsigned, Gtk::Widget*> type_unsigned;
    std::map<::Preferences::Type::String, Gtk::Widget*> type_string;
    std::map<::Preferences::Type::String, Gtk::Label*> type_string_label;

    std::unique_ptr<CardsOrder> cards_order;
    std::unique_ptr<Cardset> cardset;
    std::unique_ptr<CardsBack> cards_back;
    std::unique_ptr<Iconset> iconset;
    std::unique_ptr<Background> background;

    Gtk::Menu* language_menu = nullptr;
    std::map<::Preferences::Type::String, Gtk::FontChooserDialog*> font_chooser;
    std::map<::Preferences::Type::String, Gtk::ColorChooserDialog*> color_chooser;

  private:
    unique_ptr<::Preferences> backup_;
    AutoDisconnector disconnector_;

  private: // unused
    Preferences() = delete;
    Preferences(Preferences const&) = delete;
    Preferences& operator=(Preferences const&) = delete;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
