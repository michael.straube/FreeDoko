/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "background.h"

#include "../ui.h"
#include "../cards.h"

#include <gtkmm/scrolledwindow.h>
#include <gtkmm/image.h>
#include <gdkmm/general.h>

namespace UI_GTKMM_NS {

namespace {
double constexpr dest_width = 100.0;
double constexpr dest_height = 100.0;
Glib::RefPtr<Gdk::Pixbuf> load_background_image(std::filesystem::path path);
Glib::RefPtr<Gdk::Pixbuf> scale_if_too_big(Glib::RefPtr<Gdk::Pixbuf> pixbuf);
Glib::RefPtr<Gdk::Pixbuf> repeat_if_too_small(Glib::RefPtr<Gdk::Pixbuf> pixbuf);
} // namespace


/** constructor
 **/
Preferences::Background::Background(Preferences& preferences) :
  Selection(preferences, ::Preferences::Type::background)
{
  Selection::default_entries = {"table.png"};
}

/** destructor
 **/
Preferences::Background::~Background()
  = default;

/** creates all subelements for the cardsorder
 **/
void
Preferences::Background::init()
{
  Selection::init();
  container->set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
}

/** @return   whether the path is valid
 **/
bool
Preferences::Background::path_valid(std::filesystem::path const path) const
{
  return is_supported_graphic_file(path);
}

/** add the images to the box
 **/
Gtk::Widget*
Preferences::Background::create_entry(Entry const entry)
{
  auto pixbuf = load_background_image(entry.path);
  if (!pixbuf) {
    return {};
  }
  auto image = Gtk::manage(new Gtk::Image(pixbuf));
  return image;
}

namespace {

/** @return  pixbuf or nullptr
 ** If the size is too big, it will be scaled
 ** If the size is too small, it will be repeated
 **/
Glib::RefPtr<Gdk::Pixbuf>
  load_background_image(std::filesystem::path const path)
  {
    try {
      auto pixbuf = Gdk::Pixbuf::create_from_file(path.string());
      pixbuf = scale_if_too_big(pixbuf);
      pixbuf = repeat_if_too_small(pixbuf);
      return pixbuf;
    } catch (...) {
      return {};
    }
  }

/** @return   pixbuf scaled down, if it is too big
 **/
Glib::RefPtr<Gdk::Pixbuf>
  scale_if_too_big(Glib::RefPtr<Gdk::Pixbuf> pixbuf)
  {
    if (   pixbuf->get_width() <= dest_width
        && pixbuf->get_height() <= dest_height) {
      return pixbuf;
    }
    double const scale = min(dest_width / pixbuf->get_width(),
                             dest_height / pixbuf->get_height());
    pixbuf = pixbuf->scale_simple(scale * pixbuf->get_width(),
                                  scale * pixbuf->get_height(),
                                  Gdk::INTERP_BILINEAR);
    return pixbuf;
  }

/** @return   pixbuf repeated, if it is too small
 **/
Glib::RefPtr<Gdk::Pixbuf>
  repeat_if_too_small(Glib::RefPtr<Gdk::Pixbuf> pixbuf)
  {
    if (   pixbuf->get_width() >= 0.6 * dest_width
        && pixbuf->get_height() >= 0.6 * dest_height) {
      return pixbuf;
    }
    auto image = Cairo::ImageSurface::create(Cairo::FORMAT_RGB24,
                                             pixbuf->get_width(),
                                             pixbuf->get_height());
    {
      auto cr = Cairo::Context::create(image);
      Gdk::Cairo::set_source_pixbuf(cr, pixbuf, 0, 0);
      cr->paint();
    }
    auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_RGB24,
                                               dest_width, dest_height);
    {
      auto cr = Cairo::Context::create(surface);
      auto background = Cairo::SurfacePattern::create(image);
      background->set_extend(Cairo::EXTEND_REPEAT);
      cr->set_source(background);
      cr->rectangle(0, 0, dest_width, dest_height);
      cr->fill();
    }

    return Gdk::Pixbuf::create(surface, 0, 0, dest_width, dest_height);
  }
} // namespace

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
