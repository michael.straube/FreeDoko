/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "selection.h"

namespace UI_GTKMM_NS {

/** background preference
 **/
class Preferences::Background : public Selection {
  public:
    explicit Background(Preferences& preferences);
    ~Background() override;

  private:
    void init() override;

    bool path_valid(std::filesystem::path path) const override;
    Gtk::Widget* create_entry(Entry entry) override;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
