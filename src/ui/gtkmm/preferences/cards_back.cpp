/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "cards_back.h"

#include "../ui.h"
#include "../cards.h"

#include "../../../utils/file.h"

#include <gtkmm/image.h>

namespace UI_GTKMM_NS {

/** constructor
 **/
Preferences::CardsBack::CardsBack(Preferences& preferences) :
  Selection(preferences, ::Preferences::Type::cards_back)
{
  Selection::default_entries = {"penguin.png"};
}

/** destructor
 **/
Preferences::CardsBack::~CardsBack()
  = default;

/** creates all subelements for the cardsorder
 **/
void
Preferences::CardsBack::init()
{
  Selection::init();
  ::preferences.signal_changed(::Preferences::Type::cardset).connect([this](auto type) {update();});
}

/** @return   list of all backs (graphic files) in the (backs)-directory
 **
 ** @todo   Unterverzeichnisse unterstützen
 **/
bool
Preferences::CardsBack::path_valid(std::filesystem::path const path) const
{
  if (!is_supported_graphic_file(path))
    return false;
  if (::preferences.cardset_format() == CardsetFormat::pysol) {
    return (string(path.stem().string(), 0, 4) == "back");
  }

  return true;
}

/** add the images to the box
 **/
Gtk::Widget*
Preferences::CardsBack::create_entry(Entry const entry)
{
  try {
    auto pixbuf = Gdk::Pixbuf::create_from_file(entry.path.string());
    auto image = Gtk::manage(new Gtk::Image(pixbuf));
    return image;
  } catch (...) {
    return {};
  }
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
