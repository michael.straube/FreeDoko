/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "selection.h"

#include <regex>
#include <list>
using std::list;

#include <gtkmm/scrolledwindow.h>
#include <gtkmm/flowbox.h>

namespace UI_GTKMM_NS {

namespace {
unsigned constexpr subdirs_upper_limit = 2000;
} // namespace

/** constructor
 **/
Preferences::Selection::Entry::Entry(string const name_, std::filesystem::path const path_) :
  name(name_), path(path_)
{}

/** constructor
 **/
Preferences::Selection::Selection(Preferences& preferences,
                                  ::Preferences::Type::String const type) :
  Base(&preferences),
  type_(type),
  container(Gtk::manage(new Gtk::ScrolledWindow())),
  box(Gtk::manage(new Gtk::FlowBox()))
{
  container->show();
  container->signal_realize().connect(sigc::mem_fun(*this, &Selection::init));
}

/** destructor
 **/
Preferences::Selection::~Selection()
{
  clear_box();
}

/** @return   the setting type
 **/
::Preferences::Type::String
Preferences::Selection::type() const
{ return type_; }

/** use the cards order as a widget
 **/
Gtk::Widget*
Preferences::Selection::widget()
{
  return container;
}

/** use the cards order as a widget
 **/
Preferences::Selection::operator Gtk::Widget&()
{
  return *container;
}

/** creates all subelements for the cardsorder
 **/
void
Preferences::Selection::init()
{
  container->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  container->add(*box);
  container->show_all_children();
  box->set_valign(Gtk::ALIGN_START);
  box->set_selection_mode(Gtk::SELECTION_BROWSE);
  box->set_activate_on_single_click();
  box->signal_child_activated().connect(sigc::mem_fun(*this, &Selection::entry_selected));
  box->signal_set_focus_child().connect(sigc::mem_fun(*this, &Selection::entry_has_focus));
  ::preferences.signal_changed(type()).connect(*this, &Selection::set_focus);
  container->signal_show().connect(sigc::mem_fun(*this, &Preferences::Selection::update));
  update();
}

/** update the selection
 **/
void
Preferences::Selection::update()
{
  auto entries = entries_list();
  container_algorithm::sort(entries);
  auto const& value = ::preferences.value(type());
  {
    clear_box();
    add_entries(entries);
  }
  set_focus(value);
}

/** @return   whether the file is of a supported graphic type
 **/
bool
Preferences::Selection::is_supported_graphic_file(std::filesystem::path const path)
{
  return (   is_regular_file(path)
          && contains(supported_graphic_formats, path.extension()));
}

/** update the selection (also consider default values)
 **/
void
Preferences::Selection::set_focus(string const value)
{
  if (set_focus_to_child(value))
    return ;
  for (auto const& entry : default_entries) {
    if (set_focus_to_child(entry))
      return ;
  }
#if 0
  // Funktioniert nicht zuverlässig
  if (!box->get_children().empty()) {
    box->select_child(*box->get_child_at_index(0));
    box->set_focus_child(*box->get_child_at_index(0));
    return ;
  }
#endif
}

/** update the selection (also consider default values)
 **/
void
Preferences::Selection::update_focus()
{
  set_focus(::preferences.value(type()));
}

/** update the selection
 **/
bool
Preferences::Selection::set_focus_to_child(string value)
{
  return false;
  for (auto child : box->get_children()) {
    auto container = dynamic_cast<Gtk::FlowBoxChild*>(child);
    auto const name = *static_cast<string*>(container->get_child()->get_data("name"));
    if (name == value) {
      box->select_child(*container);
      box->set_focus_child(*container);
      return true;
    }
  }
  return false;
}

/** remove all children from the box
 **/
void
Preferences::Selection::clear_box()
{
  for (auto child : box->get_children()) {
    auto container = dynamic_cast<Gtk::FlowBoxChild*>(child);
    delete static_cast<string*>(container->get_child()->get_data("name"));
    box->remove(*child);
  }
}

/** @return   list of all backs (graphic files) in the (backs)-directory
 **/
vector<Preferences::Selection::Entry>
Preferences::Selection::entries_list() const
{
  unsigned n = 0;
  vector<Entry> entries_list;
  for (auto const& directory : ::preferences.directories(type())) {
    if (!is_directory(directory))
      continue;
    list<string> subdirs = {""s};
    for (auto const& subdir : subdirs) {
      for (auto const& entry : std::filesystem::directory_iterator(directory / subdir)) {
        auto const& path = entry.path();
        if (path_valid(path)) {
          entries_list.emplace_back(subdir + path.filename().string(), path);
          continue;
        } else if (   is_directory(path)
                   && n < subdirs_upper_limit) {
          ++n;
          subdirs.emplace_back(subdir + path.filename().string() + "/");
          continue;
        }
      }
    }
  }
  return entries_list;
}

/** add the entries to the box
 **/
void
Preferences::Selection::add_entries(vector<Entry> const& entries_list)
{
  for (auto const& entry : entries_list) {
    auto widget = create_entry(entry);
    if (!widget)
      continue;
    box->add(*widget);
    widget->set_data("name", new string(entry.name));
    widget->set_data("path", new string(entry.path.string()));
    widget->set_tooltip_text(entry.path.string());
    widget->show();
  }
}

/** a child has been selected
 **/
void
Preferences::Selection::entry_selected(Gtk::FlowBoxChild* const widget)
{
  if (!widget)
    return ;
  auto const value = *static_cast<string*>(widget->get_child()->get_data("name"));
  ::preferences.set(type(), value);
}

/** a child has got the focus
 **/
void
Preferences::Selection::entry_has_focus(Gtk::Widget* const widget)
{
  if (!widget)
    return ;

#ifdef WORKAROUND
  // rausnehmen, wenn zuverlässig auch beim Wechsel der Reiter der Fokus auf dem zuletzt ausgewählten Element (z.B. Hintergrund) bleibt
  auto const value = *static_cast<string*>(dynamic_cast<Gtk::FlowBoxChild*>(widget)->get_child()->get_data("name"));
  //CLOG << "focus: " << type() << " = " << value << "  (" << ::preferences.value(type()) << ")\n";
  return ;
#endif

  entry_selected(dynamic_cast<Gtk::FlowBoxChild*>(widget));
}

/** comparison
 **/
bool
operator<(Preferences::Selection::Entry const lhs,
          Preferences::Selection::Entry const rhs)
{
  return (   lhs.name < rhs.name
          || (   lhs.name == rhs.name
              && lhs.path < rhs.path));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
