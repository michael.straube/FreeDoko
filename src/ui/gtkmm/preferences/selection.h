/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "../preferences.h"

namespace Gtk {
class ScrolledWindow;
class FlowBox;
class FlowBoxChild;
} // namespace Gtk

namespace UI_GTKMM_NS {

/** preference for a string: select one element of an flow box
 **/
class Preferences::Selection : public Base {
  public:
    struct Entry {
      Entry(string name_, std::filesystem::path path_);
      string name;
      std::filesystem::path path;
    };
  public:
    Selection(Preferences& preferences, ::Preferences::Type::String type);
    ~Selection() override;

    ::Preferences::Type::String type() const;
    Gtk::Widget* widget();
    operator Gtk::Widget&();

    void update();
    void update_focus();

  protected:
    virtual void init();
    virtual bool path_valid(std::filesystem::path path) const = 0;
    virtual Gtk::Widget* create_entry(Entry entry) = 0;

    static bool is_supported_graphic_file(std::filesystem::path path);

  private:
    void set_focus(string value);
    bool set_focus_to_child(string value);
    void clear_box();
  protected:
    virtual vector<Entry> entries_list() const;
  private:
    void add_entries(vector<Entry> const& entries_list);
    void entry_selected(Gtk::FlowBoxChild* widget);
    void entry_has_focus(Gtk::Widget* widget);

  protected:
    ::Preferences::Type::String const type_;
    Gtk::ScrolledWindow* container = nullptr;
    Gtk::FlowBox* box = nullptr;
    vector<string> default_entries;
};

bool operator<(Preferences::Selection::Entry lhs,
               Preferences::Selection::Entry rhs);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
