/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"

#include "../../utils/version.h"
#include <gtkmm/dialog.h>
#include <gtkmm/textbuffer.h>

namespace UI_GTKMM_NS {

/**
 ** message about a program update
 **/
class ProgramUpdated : public Base, public Gtk::Dialog {
  public:
    ProgramUpdated(Base* parent, Version old_version);
    ProgramUpdated(ProgramUpdated const&)           = delete;
    ~ProgramUpdated() override;

  private:
    // reset the ais
    void reset_ais();

  private:
    // the last version
    Version const old_version;

    Glib::RefPtr<Gtk::TextBuffer> text_buffer;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
