/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "reservation.h"
#include "ui.h"
#include "main_window.h"
#include "table.h"
#include "table/cards_distribution.h"
#include "table/name.h"
#include "table/icongroup.h"

#include "../../party/rule.h"
#include "../../game/game.h"
#include "../../player/player.h"

#include "../../os/bug_report_replay.h"

#include "../../utils/string.h"

#include <gtkmm/notebook.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/grid.h>
#include <gtkmm/main.h>
namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table      the table
 ** @param    position   the corresponding position
 **/
Reservation::Reservation(Table& table, Position const position) :
  HTIN(table, position),
  Gtk::StickyDialog("Window::reservation", *parent->ui->main_window, false)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this,
                                               &Reservation::init));
}

/** destructor
 **/
Reservation::~Reservation() = default;

/** @return   the corresponding player
 **/
Player&
Reservation::player()
{
  return ui->game().player(table().player(position()).no());
} // Player& Reservation::player()

/** @return   the corresponding game
 **/
Game&
Reservation::game()
{
  return ui->game();
} // Game& Reservation::game()

/** create all subelements
 **/
void
Reservation::init()
{
  set_icon(ui->icon);
  if (game().players().count_humans() <= 1)
    set_title(_("Window::reservation"));
  else
    set_title(_("Window::reservation (%s)",
                      player().name()));

  set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

  announce_button = Gtk::manage(new Gtk::Button(_("Button::reservation announce")));
  add_action_widget(*announce_button, Gtk::RESPONSE_ACCEPT);

  bock_label = Gtk::manage(new Gtk::Label(_("bock: %u", 1U)));
#ifdef POSTPONED
  // does not work reliable
  {
    Pango::AttrList attributes;
    auto attribute
      = Pango::AttrShape::create_attr_weight(Pango::WEIGHT_BOLD);
    attributes.insert(attribute);
    bock_label->set_attributes(attributes);
  }
#endif

  notebook = Gtk::manage(new Gtk::Notebook());
  notebook->set_tab_pos(Gtk::POS_LEFT);
  notebook->signal_switch_page().connect(sigc::mem_fun(*this,
                                                             &Reservation::switch_page_event));

  swines_button = Gtk::manage(new Gtk::CheckButton(_("swines")));
  hyperswines_button = Gtk::manage(new Gtk::CheckButton(_("hyperswines")));
  solo_swines_button = Gtk::manage(new Gtk::CheckButton(_("swines")));
  solo_hyperswines_button = Gtk::manage(new Gtk::CheckButton(_("hyperswines")));

  announce_button->set_can_default();
  announce_button->grab_default();
  announce_button->grab_focus();

  { // bock information
    get_content_area()->pack_start(*bock_label, false, true);
  } // bock information
  { // the notebook
    get_content_area()->pack_start(*notebook, true, true);

    { // create the buttons
      Gtk::RadioButton::Group gametype_group;
      for (auto gt : game_type_list) {
        auto& button = gametype_buttons[gt];
        button = Gtk::manage(new Gtk::RadioButton(gametype_group, _(gt)));
        button->set_data("gametype", new GameType(gt));
        button->signal_toggled().connect(sigc::bind<GameType>(sigc::mem_fun(*this, &Reservation::gametype_changed), gt));
      } // for (gt : game_type_list)

      Gtk::RadioButton::Group marriage_selector_group;
      for (auto m : marriage_selector_list) {
        auto& button = marriage_selector_buttons[m];
        button = Gtk::manage(new Gtk::RadioButton(marriage_selector_group, _(m)));
        button->set_data("selector", new MarriageSelector(MarriageSelector(m)));
        button->signal_toggled().connect(sigc::bind<MarriageSelector>(sigc::mem_fun(*this, &Reservation::marriage_selector_changed), m));
      } // for (m : marriage_selector_list)
    } // create the buttons

    { // set the pages of the notebook
      { // General
        auto label = Gtk::manage(new Gtk::Label(_("GameType::Group::general")));
        auto hbox = Gtk::manage(new Gtk::Box());
        hbox->set_spacing(1 EX);
        hbox->set_border_width(1 EX);

        { // gametypes
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
          vbox->set_valign(Gtk::ALIGN_START);
          vbox->add(*gametype_buttons[GameType::normal]);
          vbox->add(*gametype_buttons[GameType::thrown_nines]);
          vbox->add(*gametype_buttons[GameType::thrown_kings]);
          vbox->add(*gametype_buttons[GameType::thrown_nines_and_kings]);
          vbox->add(*gametype_buttons[GameType::thrown_richness]);
          vbox->add(*gametype_buttons[GameType::fox_highest_trump]);
          vbox->add(*gametype_buttons[GameType::poverty]);
          hbox->add(*vbox);
        } // gametypes
        { // marriage selectors
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
          vbox->set_valign(Gtk::ALIGN_START);
          vbox->add(*gametype_buttons[GameType::marriage]);
          marriage_selector_buttons[MarriageSelector::first_foreign]->set_margin_left(1 EM);
          vbox->add(*marriage_selector_buttons[MarriageSelector::first_foreign]);
          marriage_selector_buttons[MarriageSelector::first_trump]->set_margin_left(1 EM);
          vbox->add(*marriage_selector_buttons[MarriageSelector::first_trump]);
          marriage_selector_buttons[MarriageSelector::first_color]->set_margin_left(1 EM);
          vbox->add(*marriage_selector_buttons[MarriageSelector::first_color]);
          marriage_selector_buttons[MarriageSelector::first_club]->set_margin_left(1 EM);
          vbox->add(*marriage_selector_buttons[MarriageSelector::first_club]);
          marriage_selector_buttons[MarriageSelector::first_spade]->set_margin_left(1 EM);
          vbox->add(*marriage_selector_buttons[MarriageSelector::first_spade]);
          marriage_selector_buttons[MarriageSelector::first_heart]->set_margin_left(1 EM);
          vbox->add(*marriage_selector_buttons[MarriageSelector::first_heart]);
          hbox->add(*vbox);
        } // marriage selectors
        { // the swines
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
          vbox->set_valign(Gtk::ALIGN_START);
          vbox->add(*swines_button);
          vbox->add(*hyperswines_button);
          hbox->add(*vbox);
        } // the swines
        notebook->append_page(*hbox, *label);
      } // General
      { // Solo
        auto label = Gtk::manage(new Gtk::Label(_("GameType::Group::solo")));

        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
        vbox->set_halign(Gtk::ALIGN_CENTER);
        vbox->set_valign(Gtk::ALIGN_START);

        {
          auto grid = Gtk::manage(new Gtk::Grid());
          grid->set_border_width(1 EX);
          grid->set_row_spacing(0 EX);
          grid->set_column_spacing(1 EM);
          grid->set_valign(Gtk::ALIGN_CENTER);
          grid->set_halign(Gtk::ALIGN_CENTER);

          grid->attach(*gametype_buttons[GameType::solo_club],
                       0, 0, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_spade],
                       0, 1, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_heart],
                       0, 2, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_diamond],
                       0, 3, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_meatless],
                       1, 0, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_jack],
                       1, 1, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_queen],
                       1, 2, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_king],
                       1, 3, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_queen_jack],
                       2, 0, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_king_jack],
                       2, 1, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_king_queen],
                       2, 2, 1, 1);
          grid->attach(*gametype_buttons[GameType::solo_koehler],
                       2, 3, 1, 1);
          vbox->add(*grid);
        }
        {
          auto hbox = Gtk::manage(new Gtk::Box());
          hbox->set_homogeneous();
          hbox->set_spacing(1 EM);
          hbox->set_halign(Gtk::ALIGN_CENTER);
          hbox->add(*solo_swines_button);
          hbox->add(*solo_hyperswines_button);
          vbox->add(*hbox);
        }

        {
          offer_duty_solo_button
            = Gtk::manage(new Gtk::CheckButton(_("offer duty solo")));

          vbox->add(*offer_duty_solo_button);
        }

        notebook->append_page(*vbox, *label);
      } // Solo
    } // set the pages of the notebook
  } // the notebook
  {
    auto box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 5 EM));
    box->set_halign(Gtk::ALIGN_CENTER);
    box->set_margin_top(1 EX);
    box->set_margin_bottom(1 EX);
    {
      auto manual_distribution = Gtk::manage(new Gtk::Button(_("distribute manually")));
      auto manual_distribution_actions =
        [this]() {
          if (position() == Position::south) {
            ui->table->cards_distribution().save_hand_of_human_player(player().hand());
          }
          ui->thrower(Game::Status::manual_cards_distribution,
                            __FILE__, __LINE__);
        };
      manual_distribution->signal_clicked().connect(manual_distribution_actions);
      box->add(*manual_distribution);
    }
    {
      auto& redistribute = *gametype_buttons[GameType::redistribute];
      box->add(redistribute);
    }
    get_content_area()->add(*box);
  }
  { // tournament info
    auto grid = Gtk::manage(new Gtk::Grid());
    grid->set_column_spacing(2 EM);
    { // remaining games/rounds/points
      auto grid2 = Gtk::manage(new Gtk::Grid());

      remaining_rounds_label
        = Gtk::manage(new Gtk::Label(_("remaining rounds") + ":"s));
      remaining_rounds_number
        = Gtk::manage(new Gtk::Label);
      remaining_rounds_number->set_label("0");
      remaining_games_label
        = Gtk::manage(new Gtk::Label(_("remaining games") + ":"s));
      remaining_games_number
        = Gtk::manage(new Gtk::Label);
      remaining_games_number->set_label("0");
      remaining_points_label
        = Gtk::manage(new Gtk::Label(_("remaining points") + ":"s));
      remaining_points_number
        = Gtk::manage(new Gtk::Label);
      remaining_points_number->set_label("0");

      remaining_rounds_label->set_halign(Gtk::ALIGN_START);
      remaining_rounds_number->set_halign(Gtk::ALIGN_END);
      remaining_games_label->set_halign(Gtk::ALIGN_START);
      remaining_games_number->set_halign(Gtk::ALIGN_END);
      remaining_points_label->set_halign(Gtk::ALIGN_START);
      remaining_points_number->set_halign(Gtk::ALIGN_END);
      grid2->attach(*remaining_rounds_label,
                    0, 0, 1, 1);
      grid2->attach(*remaining_rounds_number,
                    1, 0, 1, 1);
      grid2->attach(*remaining_games_label,
                    0, 1, 1, 1);
      grid2->attach(*remaining_games_number,
                    1, 1, 1, 1);
      grid2->attach(*remaining_points_label,
                    0, 2, 1, 1);
      grid2->attach(*remaining_points_number,
                    1, 2, 1, 1);

      grid2->set_row_spacing(1 EX);
      grid2->set_column_spacing(static_cast<int>(0.5 EM));

      grid->add(*grid2);
    } // remaining games/rounds/points
    { // duty soli
      auto grid2 = Gtk::manage(new Gtk::Grid());

      duty_free_soli_label
        = Gtk::manage(new Gtk::Label(_("duty free soli") + ":"s));
      duty_free_soli_number
        = Gtk::manage(new Gtk::Label);
      duty_free_soli_number->set_label("0");
      duty_color_soli_label
        = Gtk::manage(new Gtk::Label(_("duty color soli") + ":"s));
      duty_color_soli_number
        = Gtk::manage(new Gtk::Label);
      duty_color_soli_number->set_label("0");
      duty_picture_soli_label
        = Gtk::manage(new Gtk::Label(_("duty picture soli") + ":"s));
      duty_picture_soli_number
        = Gtk::manage(new Gtk::Label);
      duty_picture_soli_number->set_label("0");

      duty_free_soli_label->set_halign(Gtk::ALIGN_START);
      duty_free_soli_number->set_halign(Gtk::ALIGN_END);
      duty_color_soli_label->set_halign(Gtk::ALIGN_START);
      duty_color_soli_number->set_halign(Gtk::ALIGN_END);
      duty_picture_soli_label->set_halign(Gtk::ALIGN_START);
      duty_picture_soli_number->set_halign(Gtk::ALIGN_END);
      grid2->attach(*duty_free_soli_label,
                    0, 0, 1, 1);
      grid2->attach(*duty_free_soli_number,
                    1, 0, 1, 1);
      grid2->attach(*duty_color_soli_label,
                    0, 1, 1, 1);
      grid2->attach(*duty_color_soli_number,
                    1, 1, 1, 1);
      grid2->attach(*duty_picture_soli_label,
                    0, 2, 1, 1);
      grid2->attach(*duty_picture_soli_number,
                    1, 2, 1, 1);

      grid2->set_row_spacing(1 EX);
      grid2->set_column_spacing(static_cast<int>(0.5 EM));

      grid->add(*grid2);
    } // duty soli
    grid->set_border_width(1 EX);
    grid->set_halign(Gtk::ALIGN_CENTER);
    get_content_area()->add(*grid);
  } // tournament info

  { // signals
    announce_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                                  &Reservation::announce)
                                                   );
    swines_button->signal_toggled().connect(sigc::mem_fun(*this, &Reservation::swines_changed));
    solo_swines_button->signal_toggled().connect(sigc::mem_fun(*this, &Reservation::swines_changed));
    hyperswines_button->signal_toggled().connect(sigc::mem_fun(*this, &Reservation::swines_changed));
    solo_hyperswines_button->signal_toggled().connect(sigc::mem_fun(*this, &Reservation::swines_changed));
  } // signals

  show_all_children();
} // void Reservation::init()

/** show the reservation for selecting before it is the turn of the player
 **/
void
Reservation::show_for_selection()
{
  if (announced)
    return ;

  realize();

  notebook->set_current_page(0);

  set_default();
  sensitivity_update();

  { // adjust some texts because of changed points
    remaining_rounds_number->set_label(std::to_string(ui->party().remaining_rounds()));
    remaining_games_number->set_label(std::to_string(ui->party().remaining_games()));
    remaining_points_number->set_label(std::to_string(ui->party().remaining_points()));

    duty_free_soli_number->set_label(std::to_string(player().remaining_duty_free_soli()));
    duty_color_soli_number->set_label(std::to_string(player().remaining_duty_color_soli()));
    duty_picture_soli_number->set_label(std::to_string(player().remaining_duty_picture_soli()));


    bock_label->set_label(_("bock: %u",
                                  ui->party().current_bock_multiplier()));
    gametype_buttons[GameType::thrown_nines]->set_label(_("GameType::%u nines",
                                                                game().rule(Rule::Type::min_number_of_throwing_nines)));
    gametype_buttons[GameType::thrown_kings]->set_label(_("GameType::%u kings",
                                                                game().rule(Rule::Type::min_number_of_throwing_kings)));
    gametype_buttons[GameType::thrown_nines_and_kings]->set_label(_("GameType::%u nines and kings",
                                                                          game().rule(Rule::Type::min_number_of_throwing_nines_and_kings)));
    gametype_buttons[GameType::thrown_richness]->set_label(_("GameType::%u richness",
                                                                   game().rule(Rule::Type::min_richness_for_throwing)));

    if (player().hand().count(Card::club_queen)
        == game().rule(Rule::Type::number_of_same_cards)) {
      gametype_buttons[GameType::normal]->set_label(_(::GameType::marriage_silent));
    } else {
      gametype_buttons[GameType::normal]->set_label(_(::GameType::normal));
    }
  } // adjust some texts
  if (ui->party().current_bock_multiplier() != 1)
    bock_label->show();
  else
    bock_label->hide();

  show();
} // void Reservation::show_for_selection()

/** dummy function
 **
 ** @return   false
 **/
bool
Reservation::changed() const
{
  return false;
} // bool Reservation::changed() const

/** nothing (dummy function)
 **
 ** @param    cr       drawing context
 **/
void
Reservation::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  // this function is needed because it is virtual in base class 'HTIN'
} // void Reservation::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** dummy function
 **
 ** @return   the height of the drawing
 **/
Reservation::Outline
Reservation::outline() const
{
  return {};
} // Outline Reservation::outline() const

/** gets a reservation
 **
 ** @return   reservation
 **/
::Reservation
Reservation::get()
{
  if (!announced) {

    if (!is_visible())
      show_for_selection();

    do {
      if (!is_visible())
        present();

      while (!ui->thrower
             && is_visible())
        ::ui->wait();
    } while (!announce_button->is_sensitive()
             && !ui->thrower) ;
  } // if (!announced)

  game().set_type(GameType::normal);

  if (!announced)
    announce();

  announced = false;

  hide();
  return player().reservation();
} // ::Reservation Reservation::get()

/** set to default values
 **/
void
Reservation::set_default()
{
  in_update = true;

  auto const& reservation = player().get_default_reservation();

  swines_button->set_active(reservation.swines);
  hyperswines_button->set_active(reservation.hyperswines);
  solo_swines_button->set_active(false);
  solo_hyperswines_button->set_active(false);
  offer_duty_solo_button->set_active(false);

  // special case: with swines announced the player does not have a poverty
  if (   (reservation.game_type == GameType::poverty)
      && (player().hand().count_poverty_cards()
          > game().rule(Rule::Type::max_number_of_poverty_trumps)) )
    swines_button->set_active(false);

  marriage_selector_buttons[reservation.marriage_selector]->set_active();
  gametype_buttons[reservation.game_type]->set_active();

  name().force_redraw();
  table().name(game().startplayer()).force_redraw();

  in_update = false;

  gametype_changed(reservation.game_type);

  { // switch to the game type from the bug report replay
    if (   ::bug_report_replay
        && !::bug_report_replay->finished()
        && (::bug_report_replay->soloplayer_no() == player().no() )) {

      gametype_buttons[::bug_report_replay->game_type()]->set_active();
      if (::bug_report_replay->game_type() == GameType::marriage) {
        marriage_selector_buttons[::bug_report_replay->marriage_selector()]->set_active();
      }

      if (is_solo(::bug_report_replay->game_type()))
        notebook->set_current_page(1);
    }
  } // switch to the game type from the bug report replay

  swines_changed();
} // void Reservation::set_default()

/** update the sensitivity
 **/
void
Reservation::sensitivity_update()
{
  if (!get_realized())
    return ;

  if (in_update)
    return ;

  if (!ui->party().in_game())
    return ;

  auto const& player = this->player();
  auto const& game = player.game();
  if (game.status() != Game::Status::reservation)
    return ;

  auto const& rule = game.rule();
  auto const& hand = player.hand();

  if (   rule(Rule::Type::swines)
      && !rule(Rule::Type::swine_only_second)) {
    swines_button->show();
    solo_swines_button->show();
  } else {
    swines_button->hide();
    solo_swines_button->hide();
  }
  if (!rule(Rule::Type::swines_in_solo))
    solo_swines_button->hide();
  swines_button->set_sensitive(game.swines().swines_announcement_valid(player));
  solo_swines_button->set_sensitive(is_color_solo(game.type())
                                    && game.swines().swines_announcement_valid(player));

  if (rule(Rule::Type::hyperswines)) {
    hyperswines_button->show();
    solo_hyperswines_button->show();
  } else {
    hyperswines_button->hide();
    solo_hyperswines_button->hide();
  }
  if (!rule(Rule::Type::hyperswines_in_solo))
    solo_hyperswines_button->hide();
  hyperswines_button->set_sensitive(game.swines().hyperswines_announcement_valid(player)
                                    && swines_button->get_active());
  solo_hyperswines_button->set_sensitive(is_color_solo(game.type())
                                         && game.swines().hyperswines_announcement_valid(player)
                                         && solo_swines_button->get_active());
  if (::preferences(::Preferences::Type::announce_swines_automatically)) {
    swines_button->set_sensitive(false);
    hyperswines_button->set_sensitive(false);
    solo_swines_button->set_sensitive(false);
    solo_hyperswines_button->set_sensitive(false);
  } // if (::preferences(::Preferences::Type::announce_swines_automatically))

  for (auto& widget : gametype_buttons) {
    if (rule(widget.first))
      widget.second->show();
    else
      widget.second->hide();
  } // for (widget)

  if (rule(Rule::Type::solo))
    notebook->get_nth_page(1)->show();
  else
    notebook->get_nth_page(1)->hide();

  for (auto& widget : marriage_selector_buttons) {
    if (rule(widget.first))
      widget.second->show();
    else
      widget.second->hide();
  } // for (widget)

  gametype_buttons[GameType::thrown_richness]->set_sensitive((hand.points() >= rule(Rule::Type::min_richness_for_throwing))
                                                             && rule(GameType::thrown_richness));
  gametype_buttons[GameType::thrown_nines_and_kings]->set_sensitive((hand.count(Card::nine) + hand.count(Card::king) >= rule(Rule::Type::min_number_of_throwing_nines_and_kings))
                                                                    && rule(GameType::thrown_nines_and_kings));
  gametype_buttons[GameType::thrown_kings]->set_sensitive((hand.count(Card::king) >= rule(Rule::Type::min_number_of_throwing_kings))
                                                          && rule(GameType::thrown_kings));
  gametype_buttons[GameType::thrown_nines]->set_sensitive((hand.count(Card::nine) >= rule(Rule::Type::min_number_of_throwing_nines))
                                                          && rule(GameType::thrown_nines));
  gametype_buttons[GameType::poverty]->set_sensitive(hand.has_poverty());

  { // look, whether there is a card that is higher than the fox
    auto const fox_highest_trump = [&player]() {
      for (auto const& c : player.hand()) {
        if (c.istrump() && !c.is_at_max_fox())
          return false;
      }
      return true;
    }();
    gametype_buttons[GameType::fox_highest_trump]->set_sensitive(rule(Rule::Type::throw_when_fox_highest_trump)
                                                                 && fox_highest_trump);

  } // look, whether there is a card that is higher than the fox
  gametype_buttons[GameType::marriage]->set_sensitive((hand.count(Card::club_queen) == rule(Rule::Type::number_of_same_cards)));

  for (auto widget : marriage_selector_buttons) {
    auto const selected = gametype_buttons[GameType::marriage]->get_active();
    widget.second->set_sensitive(selected);
    //gametype_buttons[GameType::marriage]->sensitive());
  }

  { // duty soli
    if (   game.is_duty_solo()
        && (game.startplayer() == player)) {
      announce_button->set_sensitive(false);
      notebook->set_current_page(1);
      notebook->get_nth_page(0)->set_sensitive(false);
      for (auto widget : gametype_buttons) {
        auto const game_type = widget.first;
        widget.second->set_sensitive(   (   is_solo(game_type)
                                         && player.remaining_duty_free_soli())
                                     || (   is_color_solo(game_type)
                                         && player.remaining_duty_color_soli())
                                     || (   is_picture_solo(game_type)
                                         && player.remaining_duty_picture_soli())
                                     || (game_type == GameType::redistribute)
                                    );
        if (   widget.second->is_sensitive()
            && widget.second->get_active())
          announce_button->set_sensitive(true);

      } // for (widget)
    } // if (duty solo)

    if (rule(Rule::Type::offer_duty_solo)
        && game.is_duty_solo()
        && (game.startplayer() == player)) {
      offer_duty_solo_button->show();
    } else {
      offer_duty_solo_button->hide();
    }

    if (rule(Rule::Type::number_of_rounds_limited)) {
      remaining_rounds_label->show();
      remaining_rounds_number->show();
      remaining_games_label->show();
      remaining_games_number->show();
    } else {
      remaining_rounds_label->hide();
      remaining_rounds_number->hide();
      remaining_games_label->hide();
      remaining_games_number->hide();
    }
    if (rule(Rule::Type::points_limited)) {
      remaining_points_label->show();
      remaining_points_number->show();
    } else {
      remaining_points_label->hide();
      remaining_points_number->hide();
    }
    if (  rule(Rule::Type::number_of_duty_soli)
        - rule(Rule::Type::number_of_duty_color_soli)
        - rule(Rule::Type::number_of_duty_picture_soli)
        > 0) {
      duty_free_soli_label->show();
      duty_free_soli_number->show();
    } else {
      duty_free_soli_label->hide();
      duty_free_soli_number->hide();
    }
    if (rule(Rule::Type::number_of_duty_color_soli)) {
      duty_color_soli_label->show();
      duty_color_soli_number->show();
    } else {
      duty_color_soli_label->hide();
      duty_color_soli_number->hide();
    }
    if (rule(Rule::Type::number_of_duty_picture_soli)) {
      duty_picture_soli_label->show();
      duty_picture_soli_number->show();
    } else {
      duty_picture_soli_label->hide();
      duty_picture_soli_number->hide();
    }
  } // duty soli
} // void Reservation::sensitivity_update()

/** the gametype has changed
 **
 ** @param    gametype_   changed game type
 **/
void
Reservation::gametype_changed(GameType const gametype)
{
  if (!get_realized())
    return ;

  if (!gametype_buttons[gametype]->get_active())
    return ;
  if (in_update)
    return ;

  in_update = true;

  auto const& player = this->player();
  auto const& game = player.game();
  auto const& rule = game.rule();

  if (gametype == GameType::marriage) {
    auto const selected = gametype_buttons[gametype]->get_active();
    for (auto widget : marriage_selector_buttons)
      widget.second->set_sensitive(selected);
  } // if (gametype == GameType::marriage)

  this->game().set_type(static_cast<GameType>(gametype));

  { // update the swines
    auto const& hand = player.hand();

    swines_button->set_sensitive(game.swines().swines_announcement_valid(player));
    swines_button->set_active(swines_button->is_sensitive()
                              && rule(Rule::Type::swines_announcement_begin)
                              && ((game.type() != GameType::poverty)
                                  || (hand.count(Card::trump) <= rule(Rule::Type::max_number_of_poverty_trumps)))
                             );
    hyperswines_button->set_sensitive(game.swines().hyperswines_announcement_valid(player)
                                      && swines_button->get_active());
    hyperswines_button->set_active(hyperswines_button->is_sensitive()
                                   && rule(Rule::Type::hyperswines_announcement_begin));
    solo_swines_button->set_sensitive(game.swines().swines_announcement_valid(player));
    solo_swines_button->set_active(solo_swines_button->is_sensitive()
                                   && rule(Rule::Type::swines_announcement_begin)
                                   && ((game.type() != GameType::poverty)
                                       || (hand.count(Card::trump) <= rule(Rule::Type::max_number_of_poverty_trumps)))
                                  );
    solo_hyperswines_button->set_sensitive(game.swines().hyperswines_announcement_valid(player)
                                           && solo_swines_button->get_active());
    solo_hyperswines_button->set_active(solo_hyperswines_button->is_sensitive()
                                        && rule(Rule::Type::hyperswines_announcement_begin));

    if (::preferences(::Preferences::Type::announce_swines_automatically)) {
      swines_button->set_sensitive(false);
      hyperswines_button->set_sensitive(false);
      solo_swines_button->set_sensitive(false);
      solo_hyperswines_button->set_sensitive(false);
    } // if (::preferences(::Preferences::Type::announce_swines_automatically))
  } // update the swines

  { // update the reservation of the player
    ::Reservation& reservation = this->player().reservation();

    reservation.game_type = static_cast<GameType>(gametype);
    if (   (player.hand().count(Card::club_queen)
            == game.rule(Rule::Type::number_of_same_cards))
        && (gametype == GameType::normal)) {
      reservation.game_type = GameType::marriage_silent;
    }
    for (auto const& widget : marriage_selector_buttons) {
      if (widget.second->get_active()) {
        reservation.marriage_selector = widget.first;
      }
    }
    // swines are updated in the following 'swines_changed()'
  } // update the reservation of the player

  name().force_redraw();
  table().name(game.startplayer()).force_redraw();

  if (    game.is_duty_solo()
      && (game.startplayer() == player)) {
    announce_button->set_sensitive(is_real_solo(gametype)
                                   || (gametype == GameType::redistribute));
  }

  in_update = false;

  table().update_hands();
  table().draw_all();
  swines_changed();
} // void Reservation::gametype_changed(int gametype)

/** the marriage selector has changed
 **
 ** @param    marriage_selector   changed marriage selector
 **/
void
Reservation::marriage_selector_changed(MarriageSelector const marriage_selector)
{
  if (!get_realized())
    return ;

  player().reservation().marriage_selector
    = marriage_selector;
  table().draw_all();
} // void Reservation::marriage_selector_changed(int marriage_selector_)

/** the selection of swines/hyperswines has changed
 **
 ** @bug       the cards order is not updated (problem: information flow)
 **/
void
Reservation::swines_changed()
{
  if (!get_realized())
    return ;

  if (in_update)
    return ;

  ::Reservation& reservation = player().reservation();

  bool differ = (   (reservation.swines
                     != (is_solo(game().type())
                         ? solo_swines_button->get_active()
                         : swines_button->get_active()))
                 || (reservation.hyperswines
                     != (is_solo(game().type())
                         ? solo_hyperswines_button->get_active()
                         : hyperswines_button->get_active())));
  if (!differ)
    return ;

  reservation.swines
    = (is_solo(game().type())
       ? solo_swines_button->get_active()
       : swines_button->get_active());
  reservation.hyperswines
    = (is_solo(game().type())
       ? solo_hyperswines_button->get_active()
       : hyperswines_button->get_active());

  sensitivity_update();
  table().update_hands();
  table().draw_all();
} // void Reservation::swines_changed()

/** update in game: gametype, swines, hyperswines according to the selected reservation
 ** Used for the 'early' reservation, when the human can select a reservation before it is his turn. When a player before has announced, the game is resetted (gametype normal, no swines). So we have to adjust here, in order to have the cards order not changed.
 **/
void
Reservation::update_for_reservation()
{
  game().set_type(player().reservation().game_type);
  swines_changed();
} // void Reservation::update_for_reservation()

/** update the reservation of the player
 **/
void
Reservation::update_player_reservation()
{
  if (!get_realized())
    return ;

  ::Reservation& reservation = player().reservation();

  for (auto const& widget : gametype_buttons)
    if (widget.second->get_active())
      reservation.game_type = widget.first;

  //reservation.game_type = static_cast<GameType>(gametype);
  for (auto const& widget : marriage_selector_buttons)
    if (widget.second->get_active())
      reservation.marriage_selector = widget.first;

  reservation.swines
    = (is_solo(game().type())
       ? solo_swines_button->get_active()
       : swines_button->get_active());
  reservation.hyperswines
    = (is_solo(game().type())
       ? solo_hyperswines_button->get_active()
       : hyperswines_button->get_active());

  reservation.offer_duty_solo
    = offer_duty_solo_button->get_active();
} // void Reservation::update_player_reservation()


/** page change
 ** if the user switches to the first page, activate the default reservation
 **
 ** @param    page     the new page
 ** @param    pageno   the number of the page
 **/
void
Reservation::switch_page_event(Widget* widget, guint const pageno)
{
#ifdef WORKAROUND
  // else the 'is_visible()' segfaults when starting a new tournament
  if (   !ui->party().in_game()
      || ui->game().status() != Game::Status::reservation)
    return ;
#endif
  if (!is_visible())
    return ;

  if (::bug_report_replay) {
    if (   (pageno == 0)
        && is_solo(game().type())
        && !is_solo(::bug_report_replay->game_type()))
      set_default();
    if (   (pageno == 1)
        && !is_solo(game().type())
        && is_solo(::bug_report_replay->game_type()))
      set_default();
  } // if (::bug_report_replay)

  if (!::bug_report_replay) {
    if (pageno == 0) {
      if (is_solo(game().type())) {
        // reset the reservation
        game().set_type(GameType::normal);
        if (!is_solo(player().get_default_reservation().game_type))
          set_default();
      }
    } // if (pageno == 0)
  } // if (!::bug_report_replay)
} // void Reservation::switch_page_event(Widget*, guint pageno)

/** announce the reservation
 **/
void
Reservation::announce()
{
  if (!announce_button->is_sensitive())
    return ;

  update_player_reservation();

  hide();
#ifdef WINDOWS
#ifdef WORKAROUND
  ui->main_window->raise();
#endif
#endif

  announced = true;
} // void Reservation::announce()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
