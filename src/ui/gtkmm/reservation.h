/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "table/htin.h"

#include "../../game/reservation.h"

#include "widgets/sticky_dialog.h"
namespace Gtk {
class CheckButton;
class RadioButton;
class Notebook;
class Label;
} // namespace Gtk

namespace UI_GTKMM_NS {
class Table;

/**
 ** the reservation
 **
 ** @todo   ordering of the swines
 **/
class Reservation : public HTIN, public Gtk::StickyDialog {
  public:
    Reservation(Table& table, Position position);
    ~Reservation() override;

    // the corresponding player
    Player& player();
    // the corresponding game
    Game& game();

    bool changed() const override;
    auto outline() const -> Outline override;

    void draw(Cairo::RefPtr<::Cairo::Context> cr) override;

    // show the reservation for selecting before it is the turn of the player
    void show_for_selection();

    // get a reservation
    ::Reservation get();

    // dummy function
    void draw(Cairo::RefPtr<::Cairo::Context> cr, bool update);

    // set the default reservation
    void set_default();
    // the gametype has changed (by the user)
    void gametype_changed(GameType gametype);
    // the marriage selector has changed (by the user)
    void marriage_selector_changed(MarriageSelector marriage_selector);
    // the swines butten has changed (by the user)
    void swines_changed();

    // update the game/swines announcement, ... according to the selected reservation
    void update_for_reservation();

    // update the sensitivity (i.e. swines, marriage, poverty)
    void sensitivity_update();

    // update the selected reservation (when the page was changed)
    void update_player_reservation();

  private:
    // initialize the window
    void init();

    // the selected page has changed
    void switch_page_event(Widget* widget, guint pageno);

    // close the window and announce the reservation
    void announce();

  private:
    Gtk::Button* announce_button = nullptr;

    Gtk::Label* bock_label = nullptr;

    Gtk::Notebook* notebook = nullptr;
    Gtk::CheckButton* swines_button = nullptr;
    Gtk::CheckButton* hyperswines_button = nullptr;
    Gtk::CheckButton* solo_swines_button = nullptr;
    Gtk::CheckButton* solo_hyperswines_button = nullptr;
    std::map<GameType, Gtk::RadioButton*> gametype_buttons;
    std::map<MarriageSelector, Gtk::RadioButton*> marriage_selector_buttons;

    Gtk::Label* remaining_rounds_label = nullptr;
    Gtk::Label* remaining_rounds_number = nullptr;
    Gtk::Label* remaining_games_label = nullptr;
    Gtk::Label* remaining_games_number = nullptr;
    Gtk::Label* remaining_points_label = nullptr;
    Gtk::Label* remaining_points_number = nullptr;
    Gtk::Label* duty_free_soli_label = nullptr;
    Gtk::Label* duty_free_soli_number = nullptr;
    Gtk::Label* duty_color_soli_label = nullptr;
    Gtk::Label* duty_color_soli_number = nullptr;
    Gtk::Label* duty_picture_soli_label = nullptr;
    Gtk::Label* duty_picture_soli_number = nullptr;

    Gtk::CheckButton* offer_duty_solo_button = nullptr;

    // whether this reservation is already announced
    bool announced = false;
    // whether the window is in an update mode
    bool in_update = false;

  private: // unused
    Reservation() = delete;
    Reservation(Reservation const&) = delete;
    Reservation& operator=(Reservation const&) = delete;
}; // class Reservation : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
