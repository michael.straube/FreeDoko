/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "rules.h"
#include "ui.h"
#include "icons.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game.h"
#include "../../misc/preferences.h"
#include "../../utils/string.h"

#include "widgets/labelscalespinbutton.h"
#include <gtkmm/main.h>
#include <gtkmm/label.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/fixed.h>
#include <gtkmm/box.h>
#include <gtkmm/grid.h>
#include <gtkmm/notebook.h>
#include <gtkmm/sizegroup.h>
#include <gtkmm/frame.h>
#include <gtkmm/grid.h>
#include <gtkmm/messagedialog.h>

namespace {
// additional scaling for the icons of the notebook
auto constexpr iconscaling = 0.7;
} // namespace

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
Rules::Rules(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::Rules"), false)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this, &Rules::init));
  create_backup();
  signal_show().connect(sigc::mem_fun(*this, &Rules::create_backup));

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &Rules::on_key_press_event));
#endif
} // Rules::Rules(Base* parent)

/** destructor
 **/
Rules::~Rules()
{
  for (auto widget : type_bool)
    delete static_cast<Rule::Type::Bool*>(widget->steal_data("type"));

  for (auto widget : type_unsigned)
    delete static_cast<Rule::Type::Unsigned*>(widget->steal_data("type"));
} // Rules::~Rules()

/** creates all subelements
 **
 ** @todo   make the horizontal size always great enough
 ** @todo   visual difference between the groups (two background colours)
 **/
void
Rules::init()
{
  set_icon(ui->icon);

  reset_button = Gtk::manage(new Gtk::Button(_("Button::reset")));
  reset_button->set_image_from_icon_name("edit-undo");
  reset_button->set_always_show_image();

  tournament_button = Gtk::manage(new Gtk::Button(_("Button::tournament rules")));
  tournament_button->set_image_from_icon_name("games-highscores");
  tournament_button->set_always_show_image();

  { // create the buttons
    for (int t = Rule::Type::bool_first; t <= Rule::Type::bool_last; t++) {
      type_bool.push_back(Gtk::manage(new Gtk::CheckButton(_(Rule::Type::Bool(t)))));
      type_bool.back()->set_data("type", new Rule::Type::Bool(Rule::Type::Bool(t)));
      type_bool.back()->signal_toggled().connect(sigc::bind<int const>(sigc::mem_fun(*this, &Rules::changed),
                                                                             t)
                                                      );
    }
    for (int t = Rule::Type::unsigned_first; t <= Rule::Type::unsigned_last; t++) {
      auto const type = Rule::Type::Unsigned(t);
      switch (type) {
      case Rule::Type::marriage_determination:
      case Rule::Type::announcement_no_120:
      case Rule::Type::announcement_no_90:
      case Rule::Type::announcement_no_60:
      case Rule::Type::announcement_no_30:
      case Rule::Type::announcement_no_0: {
        auto button = Gtk::manage(new Gtk::LabelScaleSpinButton(_(type)));
        button->signal_value_changed().connect(sigc::bind<int const>(sigc::mem_fun(*this, &Rules::changed), t));
        type_unsigned.push_back(button);
        break;
      }
      default: {
        auto button = Gtk::manage(new Gtk::LabelSpinButton(_(type)));
        type_unsigned.push_back(button);
        button->signal_value_changed().connect(sigc::bind<int const>(sigc::mem_fun(*this, &Rules::changed), t));
        break;
      }
      } // switch(type)
      type_unsigned.back()->set_data("type", new Rule::Type::Unsigned(type));
    }
    { // counting
      Gtk::RadioButton::Group counting_group;
      for (auto c : {Counting::plus, Counting::minus, Counting::plusminus}) {
        counting[c] = Gtk::manage(new Gtk::RadioButton(counting_group, _(c)));
        counting[c]->signal_toggled().connect(sigc::bind<int>(sigc::mem_fun(*this, &Rules::changed), static_cast<int>(Rule::Type::counting)));
      }
    } // counting
    { // same size of some widgets
      static auto announcement_scale_size_group
        = Gtk::SizeGroup::create(Gtk::SIZE_GROUP_BOTH);
      announcement_scale_size_group->add_widget(*dynamic_cast<Gtk::LabelScaleSpinButton*>(type_unsigned[Rule::Type::announcement_no_120 - Rule::Type::unsigned_first])->get_scale());
      announcement_scale_size_group->add_widget(*dynamic_cast<Gtk::LabelScaleSpinButton*>(type_unsigned[Rule::Type::announcement_no_90  - Rule::Type::unsigned_first])->get_scale());
      announcement_scale_size_group->add_widget(*dynamic_cast<Gtk::LabelScaleSpinButton*>(type_unsigned[Rule::Type::announcement_no_60  - Rule::Type::unsigned_first])->get_scale());
      announcement_scale_size_group->add_widget(*dynamic_cast<Gtk::LabelScaleSpinButton*>(type_unsigned[Rule::Type::announcement_no_30  - Rule::Type::unsigned_first])->get_scale());
      announcement_scale_size_group->add_widget(*dynamic_cast<Gtk::LabelScaleSpinButton*>(type_unsigned[Rule::Type::announcement_no_0   - Rule::Type::unsigned_first])->get_scale());

    } // same size of some widgets
  } // create the buttons


  { // layout
    add_close_button(*this);

    { // layout for the rules
      group_notebook = Gtk::manage(new Gtk::Notebook());

      group_notebook->set_tab_pos(Gtk::POS_LEFT);
      //group_notebook->set_scrollable(true);
      group_notebook->popup_enable();
#ifdef TODO
      group_notebook->set_property("homogeneous", true);
#endif
      { // Tournament
        auto tournament_box = add_group("tournament",
                                              ui->create_theme_icon_image("games-highscores", 48));
        (void)_("Rule::Group::tournament");
        tournament_box->set_spacing(3 EX);

        { // with nines
          auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox2->set_halign(Gtk::ALIGN_CENTER);
          tournament_box->add(*vbox2);

          vbox2->add(*type_bool[Rule::Type::with_nines - Rule::Type::bool_first]);
        } // with nines
        { // lower box
          auto hbox = Gtk::manage(new Gtk::Box());
          hbox->set_spacing(2 EM);
          { // limits
            auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2 EX));
            {
              auto vbox3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
              vbox3->add(*type_bool[Rule::Type::number_of_rounds_limited - Rule::Type::bool_first]);
              vbox3->add(*type_unsigned[Rule::Type::number_of_rounds - Rule::Type::unsigned_first]);
              vbox2->add(*vbox3);
            }
            {
              auto vbox3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
              vbox3->add(*type_bool[Rule::Type::points_limited - Rule::Type::bool_first]);
              vbox3->add(*type_unsigned[Rule::Type::points - Rule::Type::unsigned_first]);
              vbox2->add(*vbox3);
            }
            hbox->add(*vbox2);
          } // limits
          { // duty soli
            auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2 EX));
            {
              auto vbox3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
              vbox3->add(*type_unsigned[Rule::Type::number_of_duty_soli - Rule::Type::unsigned_first]);
              vbox3->add(*type_unsigned[Rule::Type::number_of_duty_color_soli - Rule::Type::unsigned_first]);
              vbox3->add(*type_unsigned[Rule::Type::number_of_duty_picture_soli - Rule::Type::unsigned_first]);
              vbox2->add(*vbox3);
            }
            {
              auto vbox3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
              vbox3->add(*type_bool[Rule::Type::offer_duty_solo - Rule::Type::bool_first]);
              vbox2->add(*vbox3);
            }
            hbox->pack_end(*vbox2, Gtk::PACK_SHRINK, 2 EX);
          } // duty soli
          tournament_box->add(*hbox);
        } // lower box
      } // Tournament
      { // counting
        auto vbox = add_group("counting",
                                    ui->create_theme_icon_image("view-statistics", 48));
        (void)_("Rule::Group::counting");
        auto counting_box = Gtk::manage(new Gtk::Box());
        counting_box->set_spacing(4 EM);
        { // counting type
          auto vbox_count = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2 EX));
          {
            auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));

            auto label = Gtk::manage(new Gtk::Label(_("Label::counting")));
            label->set_halign(Gtk::ALIGN_START);
            vbox->add(*label);

            for (auto widget : counting)
              vbox->add(*widget.second);

            vbox_count->add(*vbox);
          }
          {
            type_bool[Rule::Type::solo_always_counts_triple - Rule::Type::bool_first]->set_margin_top(1 EX / 2);
            vbox_count->add(*type_bool[Rule::Type::solo_always_counts_triple - Rule::Type::bool_first]);
          }
          counting_box->add(*vbox_count);
        } // counting type
        { // Bock
          auto bock_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
          bock_box->set_spacing(2 EX);
          counting_box->add(*bock_box);

          bock_box->add(*type_bool[Rule::Type::bock - Rule::Type::bool_first]);
          bock_box->add(*type_bool[Rule::Type::bock_append - Rule::Type::bool_first]);
          { // conditions
            auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
            vbox->set_homogeneous();
            bock_box->add(*vbox);

            vbox->add(*type_bool[Rule::Type::bock_120 - Rule::Type::bool_first]);
            vbox->add(*type_bool[Rule::Type::bock_solo_lost - Rule::Type::bool_first]);
            vbox->add(*type_bool[Rule::Type::bock_re_lost - Rule::Type::bool_first]);
            vbox->add(*type_bool[Rule::Type::bock_heart_trick - Rule::Type::bool_first]);
            vbox->add(*type_bool[Rule::Type::bock_black - Rule::Type::bool_first]);
          } // conditions
        } // Bock
        counting_box->set_halign(Gtk::ALIGN_CENTER);
        counting_box->set_valign(Gtk::ALIGN_CENTER);
        vbox->add(*counting_box);
      } // counting
      { // Solo
        auto solo_box = add_group("solo",
                                        ui->icons->new_managed_image(Icons::Type::solo_koehler, iconscaling));
        (void)_("Rule::Group::solo");
        solo_box->set_spacing(2 EX);

        solo_box->add(*type_bool[Rule::Type::solo - Rule::Type::bool_first]);
        { // solo types
          auto grid = Gtk::manage(new Gtk::Grid());
          grid->set_column_spacing(1 EM);
          grid->attach(*type_bool[Rule::Type::solo_color - Rule::Type::bool_first],
                       0, 0, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_koehler - Rule::Type::bool_first],
                       0, 1, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_meatless - Rule::Type::bool_first],
                       0, 2, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_jack - Rule::Type::bool_first],
                       1, 0, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_queen - Rule::Type::bool_first],
                       1, 1, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_king - Rule::Type::bool_first],
                       1, 2, 1, 1);

          grid->attach(*type_bool[Rule::Type::solo_queen_jack - Rule::Type::bool_first],
                       2, 0, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_king_jack - Rule::Type::bool_first],
                       2, 1, 1, 1);
          grid->attach(*type_bool[Rule::Type::solo_king_queen - Rule::Type::bool_first],
                       2, 2, 1, 1);
          solo_box->add(*grid);
        } // solo types
        solo_box->add(*type_bool[Rule::Type::throwing_before_solo - Rule::Type::bool_first]);
        solo_box->add(*type_bool[Rule::Type::lustsolo_player_leads - Rule::Type::bool_first]);
      } // Solo
      { // Poverty
        auto poverty_box
          = add_group("poverty",
                            ui->icons->new_managed_image(Icons::Type::poverty, iconscaling));
        (void)_("Rule::Group::poverty");

        {
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox->add(*type_bool[Rule::Type::poverty - Rule::Type::bool_first]);
          vbox->add(*type_bool[Rule::Type::poverty_shift - Rule::Type::bool_first]);
          type_bool[Rule::Type::poverty_shift_only_trump - Rule::Type::bool_first]->set_margin_left(1 EM);
          vbox->add(*type_bool[Rule::Type::poverty_shift_only_trump - Rule::Type::bool_first]);
          type_bool[Rule::Type::poverty_fox_do_not_count - Rule::Type::bool_first]->set_margin_left(1 EM);
          vbox->add(*type_bool[Rule::Type::poverty_fox_do_not_count - Rule::Type::bool_first]);
          type_bool[Rule::Type::poverty_fox_shift_extra - Rule::Type::bool_first]->set_margin_left(2 EM);
          vbox->add(*type_bool[Rule::Type::poverty_fox_shift_extra - Rule::Type::bool_first]);
          type_bool[Rule::Type::poverty_fox_shift_open - Rule::Type::bool_first]->set_margin_left(1 EM);
          vbox->add(*type_bool[Rule::Type::poverty_fox_shift_open - Rule::Type::bool_first]);
          type_bool[Rule::Type::throw_with_one_trump - Rule::Type::bool_first]->set_margin_left(1 EM);
          vbox->add(*type_bool[Rule::Type::throw_with_one_trump - Rule::Type::bool_first]);
          poverty_box->add(*vbox);
        }
        {
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox->add(*type_bool[Rule::Type::throw_when_fox_highest_trump - Rule::Type::bool_first]);
          vbox->add(*type_bool[Rule::Type::throw_with_nines - Rule::Type::bool_first]);
          vbox->add(*type_unsigned[Rule::Type::min_number_of_throwing_nines - Rule::Type::unsigned_first]);
          vbox->add(*type_bool[Rule::Type::throw_with_kings - Rule::Type::bool_first]);
          vbox->add(*type_unsigned[Rule::Type::min_number_of_throwing_kings - Rule::Type::unsigned_first]);
          vbox->add(*type_bool[Rule::Type::throw_with_nines_and_kings - Rule::Type::bool_first]);
          vbox->add(*type_unsigned[Rule::Type::min_number_of_throwing_nines_and_kings - Rule::Type::unsigned_first]);
          vbox->add(*type_bool[Rule::Type::throw_with_richness - Rule::Type::bool_first]);
          vbox->add(*type_unsigned[Rule::Type::min_richness_for_throwing - Rule::Type::unsigned_first]);
          poverty_box->add(*vbox);
        }
      } // Poverty
      { // Marriage
        auto marriage_box = add_group("marriage",
                                            ui->icons->new_managed_image(Icons::Type::marriage, iconscaling));
        (void)_("Rule::Group::marriage");

        { // marriage
          marriage_box->add(*type_bool[Rule::Type::marriage_before_poverty - Rule::Type::bool_first]);
          {
            auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
            vbox2->add(*type_bool[Rule::Type::marriage_first_foreign - Rule::Type::bool_first]);
            vbox2->add(*type_bool[Rule::Type::marriage_first_color - Rule::Type::bool_first]);
            vbox2->add(*type_bool[Rule::Type::marriage_first_single_color - Rule::Type::bool_first]);
            vbox2->add(*type_bool[Rule::Type::marriage_first_trump - Rule::Type::bool_first]);
            marriage_box->add(*vbox2);
          }
          marriage_box->add(*type_bool[Rule::Type::marriage_first_one_decides - Rule::Type::bool_first]);
          marriage_box->add(*type_unsigned[Rule::Type::marriage_determination - Rule::Type::unsigned_first]);
        } // marriage
      } // Marriage
      { // Announcements
        auto announcements_box
          = add_group("announcements",
                            ui->icons->new_managed_image(Icons::Type::no_120, iconscaling));
        (void)_("Rule::Group::announcements");
        {
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox->pack_start(*type_bool[Rule::Type::knocking - Rule::Type::bool_first]);
          announcements_box->add(*vbox);
        }

        {
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox->pack_start(*type_bool[Rule::Type::announcement_individual_limits]);

          vbox->pack_start(*type_unsigned[Rule::Type::announcement_no_120 - Rule::Type::unsigned_first]);
          vbox->pack_start(*type_unsigned[Rule::Type::announcement_no_90 - Rule::Type::unsigned_first]);
          vbox->pack_start(*type_unsigned[Rule::Type::announcement_no_60 - Rule::Type::unsigned_first]);
          vbox->pack_start(*type_unsigned[Rule::Type::announcement_no_30 - Rule::Type::unsigned_first]);
          vbox->pack_start(*type_unsigned[Rule::Type::announcement_no_0 - Rule::Type::unsigned_first]);
          announcements_box->add(*vbox);
        }
        {
          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));

          vbox->pack_start(*type_bool[Rule::Type::announcement_limit_only_for_current - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_till_full_trick - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_re_doubles - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_contra_doubles_against_re - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_first_trick_thirty_points - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_first_trick_thirty_points_only_first - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_first_trick_thirty_points_only_re_contra - Rule::Type::bool_first]);
          vbox->pack_start(*type_bool[Rule::Type::announcement_first_trick_thirty_points_in_marriage - Rule::Type::bool_first]);
          type_bool[Rule::Type::announcement_contra_doubles_against_re - Rule::Type::bool_first]->set_margin_left(1 EM);
          type_bool[Rule::Type::announcement_first_trick_thirty_points - Rule::Type::bool_first];
          type_bool[Rule::Type::announcement_first_trick_thirty_points_only_first - Rule::Type::bool_first]->set_margin_left(1 EM);
          type_bool[Rule::Type::announcement_first_trick_thirty_points_only_re_contra - Rule::Type::bool_first]->set_margin_left(1 EM);
          type_bool[Rule::Type::announcement_first_trick_thirty_points_in_marriage - Rule::Type::bool_first]->set_margin_left(1 EM);
          announcements_box->add(*vbox);
        }
      } // Announcements
      { // Dullen
        auto vbox = add_group("dullen",
                                    ui->icons->new_managed_image(Icons::Type::dullen, iconscaling));
        (void)_("Rule::Group::dullen");
        vbox->set_spacing(1 EX / 2);

        {
          vbox->add(*type_bool[Rule::Type::dullen - Rule::Type::bool_first]);
          vbox->add(*type_bool[Rule::Type::dullen_second_over_first - Rule::Type::bool_first]);
          vbox->add(*type_bool[Rule::Type::dullen_contrary_in_last_trick - Rule::Type::bool_first]);
          vbox->add(*type_bool[Rule::Type::dullen_first_over_second_with_swines - Rule::Type::bool_first]);
          type_bool[Rule::Type::dullen_second_over_first - Rule::Type::bool_first]->set_margin_left(1 EM);
          type_bool[Rule::Type::dullen_contrary_in_last_trick - Rule::Type::bool_first]->set_margin_left(2 EM);
          type_bool[Rule::Type::dullen_first_over_second_with_swines - Rule::Type::bool_first]->set_margin_left(2 EM);
        }
      } // Dullen
      { // Swines
        auto grid = Gtk::manage(new Gtk::Grid());
        add_group("swines",
                        ui->icons->new_managed_image(Icons::Type::swines, iconscaling)
                       )->add(*grid);
        (void)_("Rule::Group::swines");

        grid->set_row_homogeneous(true);
        grid->set_column_homogeneous(true);
        grid->set_column_spacing(2 EM);
        grid->set_row_spacing(1 EX / 2);

        {
          grid->attach(*type_bool[Rule::Type::swines - Rule::Type::bool_first],
                       0, 0, 1, 1);
          type_bool[Rule::Type::swines_in_solo - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::swines_in_solo - Rule::Type::bool_first],
                       0, 1, 1, 1);
          type_bool[Rule::Type::swines_in_poverty - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::swines_in_poverty - Rule::Type::bool_first],
                       0, 2, 1, 1);
          type_bool[Rule::Type::swines_announcement_begin - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::swines_announcement_begin - Rule::Type::bool_first],
                       0, 3, 1, 1);
          type_bool[Rule::Type::swine_only_second - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::swine_only_second - Rule::Type::bool_first],
                       0, 4, 1, 1);
          grid->attach(*type_bool[Rule::Type::hyperswines - Rule::Type::bool_first],
                       1, 0, 1, 1);
          type_bool[Rule::Type::hyperswines_in_solo - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::hyperswines_in_solo - Rule::Type::bool_first],
                       1, 1, 1, 1);
          type_bool[Rule::Type::hyperswines_in_poverty - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::hyperswines_in_poverty - Rule::Type::bool_first],
                       1, 2, 1, 1);
          type_bool[Rule::Type::hyperswines_announcement_begin - Rule::Type::bool_first]->set_margin_left(1 EM);
          grid->attach(*type_bool[Rule::Type::hyperswines_announcement_begin - Rule::Type::bool_first],
                       1, 3, 1, 1);
          grid->attach(*type_bool[Rule::Type::swines_and_hyperswines_joint - Rule::Type::bool_first],
                       0, 5, 2, 1);
        }
      } // Swines
      { // Extrapoints
        auto vbox = add_group("extrapoints",
                                    ui->icons->new_managed_image(Icons::Type::doppelkopf, iconscaling));
        (void)_("Rule::Group::extrapoints");

        {
          auto hbox = Gtk::manage(new Gtk::Box());
          hbox->set_spacing(1 EM);
          hbox->set_spacing(2 EM);
          vbox->add(*hbox);
          {
            auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
            vbox2->add(*type_bool[Rule::Type::extrapoint_catch_fox - Rule::Type::bool_first]);
            type_bool[Rule::Type::extrapoint_catch_fox_last_trick - Rule::Type::bool_first]->set_margin_left(1 EM);
            vbox2->add(*type_bool[Rule::Type::extrapoint_catch_fox_last_trick - Rule::Type::bool_first]);
            vbox2->add(*type_bool[Rule::Type::extrapoint_fox_last_trick - Rule::Type::bool_first]);
            type_bool[Rule::Type::extrapoint_double_fox_last_trick - Rule::Type::bool_first]->set_margin_left(1 EM);
            vbox2->add(*type_bool[Rule::Type::extrapoint_double_fox_last_trick - Rule::Type::bool_first]);
            hbox->add(*vbox2);
          }
          {
            auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
            vbox2->add(*type_bool[Rule::Type::extrapoint_charlie - Rule::Type::bool_first]);
            type_bool[Rule::Type::extrapoint_catch_charlie - Rule::Type::bool_first]->set_margin_left(1 EM);
            vbox2->add(*type_bool[Rule::Type::extrapoint_catch_charlie - Rule::Type::bool_first]);
            type_bool[Rule::Type::extrapoint_double_charlie - Rule::Type::bool_first]->set_margin_left(1 EM);
            vbox2->add(*type_bool[Rule::Type::extrapoint_double_charlie - Rule::Type::bool_first]);
            type_bool[Rule::Type::extrapoint_catch_double_charlie - Rule::Type::bool_first]->set_margin_left(1 EM);
            vbox2->add(*type_bool[Rule::Type::extrapoint_catch_double_charlie - Rule::Type::bool_first]);
            type_bool[Rule::Type::extrapoint_catch_charlie_only_with_diamond_queen - Rule::Type::bool_first]->set_margin_left(1 EM);
            vbox2->add(*type_bool[Rule::Type::extrapoint_catch_charlie_only_with_diamond_queen - Rule::Type::bool_first]);
            hbox->add(*vbox2);
          }
        }
        {
          auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
          vbox2->add(*type_bool[Rule::Type::extrapoint_dulle_jabs_dulle - Rule::Type::bool_first]);
          vbox2->add(*type_bool[Rule::Type::extrapoint_heart_trick - Rule::Type::bool_first]);
          vbox->add(*vbox2);
        }
      } // Extrapoints
    } // layout for the rules
    get_content_area()->pack_start(*group_notebook);


    { // put the buttons in the container
      auto file_box = Gtk::manage(new Gtk::ButtonBox());
      file_box->set_border_width(1 EX / 2);
      file_box->set_spacing(2 EM);
      file_box->set_halign(Gtk::ALIGN_CENTER);
      file_box->set_homogeneous();

      file_box->add(*tournament_button);
      file_box->add(*reset_button);

      get_content_area()->pack_end(*file_box, Gtk::PACK_SHRINK, 1 EX);
    } // put the buttons in the container

  } // layout

#ifndef RELEASE
#ifdef DKNOF
  { // test, whether all rules-buttons are packed in a container
    for (int t = Rule::Type::bool_first; t <= Rule::Type::bool_last; t++) {
      if (t == Rule::Type::mutate)
        continue;
      if (!type_bool[t - Rule::Type::bool_first]->get_parent())
        cerr << "UI_GTKMM::Rules::Rules():\n"
          << "  rule '" << Rule::Type::Bool(t) << "' not packed\n";
    }
    for (int t = Rule::Type::unsigned_first; t <= Rule::Type::unsigned_last; t++) {
      if (   (t == Rule::Type::number_of_players)
          || (t == Rule::Type::number_of_players_in_game)
          || (t == Rule::Type::bock_multiplier) )
        continue;
      if (!type_unsigned[t - Rule::Type::unsigned_first]->get_parent())
        cerr << "UI_GTKMM::Rules::Rules():\n"
          << "  rule '" << Rule::Type::Unsigned(t) << "' not packed\n";
    }
  } // test, whether all rule-buttons are packed in a container
#endif
#endif

  { // signals
    tournament_button->signal_clicked().connect(sigc::mem_fun(*this, &Rules::set_to_tournament));
    reset_button->signal_clicked().connect(sigc::mem_fun(*this, &Rules::reset));
  } // signals

  update_all();

  show_all_children();

  ui->party().signal_start.connect_back(*this, &Rules::sensitivity_update, disconnector_);
} // void Rules::init()

/** creates a backup
 **/
void
Rules::create_backup()
{
#ifdef WORKAROUND
  backup_ = std::make_unique<Rule>(::party->rule());
#else
  backup_ = std::make_unique<Rule>(ui->party().rule());
#endif

  sensitivity_update();
} // void Rules::create_backup()

/** @return   the backup
 **/
Rule const&
Rules::backup() const
{
  DEBUG_ASSERTION(backup_,
                  "Rule::Type::backup():\n"
                  "  'backup_' == nullptr");

  return *backup_;
} // Rule const& Rules::backup() const

/** resets the rules to the backup
 **/
void
Rules::reset()
{
  ::party->rule() = backup();
  update_all();
}

/** set the rules to the tournament rules
 **/
void
Rules::set_to_tournament()
{
  ::party->rule().reset_to_tournament();
  update_all();
}

/** update the sensitivity of all widgets
 **/
void
Rules::sensitivity_update()
{
  if (!get_realized())
    return ;

  auto const& rule = ui->party().rule();

  for (auto widget : type_bool) {
    auto const type = *static_cast<Rule::Type::Bool*>(widget->get_data("type"));
    widget->set_sensitive(rule.can_be_changed(type) && rule.dependencies(type));
  }
  for (auto widget : type_unsigned) {
    auto const type = *static_cast<Rule::Type::Unsigned*>(widget->get_data("type"));
    widget->set_sensitive(rule.can_be_changed(type) && rule.dependencies(type));
    widget->set_range(rule.min(type), rule.max(type));
  }
  for (auto widget : counting) {
    auto const type = Rule::Type::counting;
    widget.second->set_sensitive(rule.can_be_changed(type));
  }

  auto const outside_game = (   !ui->party().in_game()
                             || ui->game().status() == Game::Status::finished);

  reset_button->set_sensitive(outside_game && backup() != rule);
  tournament_button->set_sensitive(outside_game);
}


/** update the rule 'type'
 **
 ** @param    type                 the type of the rule
 ** @param    update_sensitivity   whether the sensitivity shall be updated
 **                (default: true)
 **/
void
Rules::update(int const type, bool const update_sensitivity)
{
  if (!get_realized())
    return ;

  if (update_sensitivity)
    sensitivity_update();

  Rule const& rule = ui->party().rule();

  if (   type >= Rule::Type::bool_first
      && type <= Rule::Type::bool_last) {
    type_bool[type - Rule::Type::bool_first]->set_active(rule.value(Rule::Type::Bool(type)));
  } else if (   type >= Rule::Type::unsigned_first
             && type <= Rule::Type::unsigned_last) {
    auto const t = Rule::Type::Unsigned(type);
    auto widget = type_unsigned[t - Rule::Type::unsigned_first];
    widget->set_range(rule.min(t), rule.max(t));
    widget->set_value(rule.value(t));
  } else if (type == Rule::Type::counting) {
    counting[rule(Rule::Type::counting)]->set_active(true);
  } else  {
    DEBUG_ASSERTION(false,
                    "Rules::update(type, update_sensitivity):\n"
                    "  type '" << type << "' unknown.");
  }

  if (update_sensitivity)
    reset_button->set_sensitive(backup() != rule);
} // void Rules::update(int type, bool update_sensitivity = true)

/** a rule has been changed by the user
 **
 ** @param    type   the type of the rule
 **/
void
Rules::changed(int const type)
{
  auto& rule = ::party->rule();

  if (   type == Rule::Type::with_nines
      && type_bool[type - Rule::Type::bool_first]->get_active() != rule(Rule::Type::with_nines)
      && rule(Rule::Type::announcement_individual_limits)
      && without_nines_info_shown == false
     ) {
    // show an information window for rule problems
    // @bug    memory bug: dialog is not destroyed (but is only created at most once)
    auto information
      = new Gtk::MessageDialog(!rule(Rule::Type::with_nines)
                               ? _("Rule::Information::with nines")
                               : _("Rule::Information::without nines"),
                               false,
                               Gtk::MESSAGE_INFO,
                               Gtk::BUTTONS_NONE,
                               false);
    information->set_icon(ui->icon);
    information->set_title(!rule(Rule::Type::with_nines)
                           ? _("Rule::Information::Title::with nines")
                           : _("Rule::Information::Title::without nines"));

    information->set_transient_for(*this);

    information->present();

    without_nines_info_shown = true;
  } // if ('with nines' changed)

  if (   type >= Rule::Type::bool_first
      && type <= Rule::Type::bool_last) {
    auto widget = type_bool[type - Rule::Type::bool_first];
    rule.set(Rule::Type::Bool(type), widget->get_active());
  } else if (   type >= Rule::Type::unsigned_first
             && type <= Rule::Type::unsigned_last) {
    auto const t = Rule::Type::Unsigned(type);
    auto widget = type_unsigned[t - Rule::Type::unsigned_first];
    if (widget->get_realized()) {
      rule.set(t, widget->get_value_as_int());
    }
  } else if (type == Rule::Type::counting) {
    for (auto& c : counting)
      if (c.second->get_active())
        rule.set(Rule::Type::counting, c.first);
  } else {
    DEBUG_ASSERTION(false,
                    "Rules::changed(type):\n"
                    "  type '" << type << "' unknown.");
  }
} // void Rules::changed(int type)

/** updates all widgets
 **/
void
Rules::update_all()
{
  if (!get_realized())
    return ;

  sensitivity_update();
  for (int t = Rule::Type::bool_first; t <= Rule::Type::bool_last; t++)
    update(t, false);
  for (int t = Rule::Type::unsigned_first; t <= Rule::Type::unsigned_last; t++)
    update(t, false);
  update(Rule::Type::counting, false);

  reset_button->set_sensitive(backup() != ui->party().rule());
} // void Rules::update_all()

/** adds to the group notebook a page for group 'name'.
 **
 ** @param    name   name of the group
 ** @param    image   image before the label
 **         (default: nullptr)
 **
 ** @return   created vbox for the rules of the group
 **/
Gtk::Box*
Rules::add_group(string const& name, Gtk::Image* image)
{
  auto box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2 EX));
  box->set_border_width(1 EM);
  box->set_halign(Gtk::ALIGN_CENTER);
  box->set_valign(Gtk::ALIGN_CENTER);

  auto bookmark = Gtk::manage(new Gtk::Box());
  bookmark->set_spacing(1 EM / 2);
  if (image)
    bookmark->add(*image);
  if (!name.empty()) {
    auto label = Gtk::manage(new Gtk::Label(_("Rule::Group::" + name)));
    bookmark->add(*label);
  } // if (name)
  bookmark->show_all_children();

  auto menu_label = Gtk::manage(new Gtk::Label(_("Rule::Group::" + name)));
  group_notebook->append_page(*box, *bookmark, *menu_label);

  return box;
} // Gtk::Box* Rules::add_group(string name, Gtk::Image* image = nullptr)

/** a key has been pressed
 ** C-o: output of the rules on 'stdout'
 **
 ** @param    key   the key
 **
 ** @return   whether the key was managed
 **/
bool
Rules::on_key_press_event(GdkEventKey* const key)
{
  bool managed = false;

  if ((key->state & ~GDK_SHIFT_MASK) == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_o: // ouput of the rules
      cout << ui->party().rule() << endl;
      managed = true;
      break;
    case GDK_KEY_u: // update the rules
      update_all();
      managed = true;
      break;
    } // switch (key->keyval)
  } // if (key->state == GDK_CONTROL_MASK)

  return (managed
          || StickyDialog::on_key_press_event(key)
          || ui->key_press(key));
} // bool Rules::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
