/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include "../../party/rule.h"

#include "widgets/sticky_dialog.h"
#include <gtkmm/notebook.h>
namespace Gtk {
class Label;
class Button;
class RadioButton;
class CheckButton;
class LabelSpinButton;
class Box;
class Notebook;
class Image;
} // namespace Gtk

class Rule;

namespace UI_GTKMM_NS {

/**
 ** the rules dialog
 **
 ** @todo   use a treeview
 **/
class Rules : public Base, public Gtk::StickyDialog {
  public:
    explicit Rules(Base* parent);
    ~Rules() override;

    Rules() = delete;
    Rules(Rules const&) = delete;
    Rules& operator=(Rules const&) = delete;

    void create_backup();
    Rule const& backup() const;

    void reset();
    void set_to_tournament();

    void sensitivity_update();
    void update(int rule_type, bool update_sensitivity = true);
    void changed(int rule_type);

    void update_all();

  private:
    void init();

    Gtk::Box* add_group(string const& name, Gtk::Image* image = nullptr);

    bool on_key_press_event(GdkEventKey* key) override;

  public:
    vector<Gtk::CheckButton*> type_bool;
    vector<Gtk::LabelSpinButton*> type_unsigned;
    std::map<Counting, Gtk::RadioButton*> counting;

  private:
    Gtk::Button* reset_button = nullptr;
    Gtk::Button* tournament_button = nullptr;

    Gtk::Notebook* group_notebook = nullptr;

  private:
    AutoDisconnector disconnector_;

    std::unique_ptr<Rule> backup_;
    // whether the info dialog changes for the rule 'with nines' <-> 'without nines' was shown
    bool without_nines_info_shown = false;
}; // class Rules : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
