/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"


#include <gtkmm/window.h>
namespace Gtk {
class Image;
class ProgressBar;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the splash screen
 **/
class SplashScreen : public Base, public Gtk::Window {
  friend class UI_GTKMM;
  public:
  explicit SplashScreen(Base* parent);
  ~SplashScreen() override;

  // the status message has changed
  void status_message_changed(string const& status_message);
  // the progress has changed
  void progress_changed(double progress_max);

  private:
  // initialize all elements
  void init();

  private:
  Gtk::Image* image = nullptr;
  Gtk::ProgressBar* progress_bar = nullptr;

  private: // unused
  SplashScreen() = delete;
  SplashScreen(SplashScreen const&) = delete;
  SplashScreen& operator=(SplashScreen const&) = delete;
}; // class SplashScreen : public Base, public Gtk::Window

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
