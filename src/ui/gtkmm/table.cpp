/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "table.h"
#include "reservation.h"
#include "game_finished.h"
#include "game_review.h"
#include "party_points.h"
#include "party_finished.h"
#include "full_trick.h"
#include "last_trick.h"

#include "table/cards_distribution.h"
#include "table/trick.h"
#include "table/hand.h"
#include "table/trickpile.h"
#include "table/icongroup.h"
#include "table/name.h"
#include "table/poverty.h"

#include "ui.h"
#include "main_window.h"
#include "card_suggestion.h"
#include "gameinfo_dialog.h"
#include "gameinfos.h"

#include "../../basetypes/fast_play.h"

#include "../../party/party.h"
#include "../../game/game.h"
#include "../../game/gameplay_actions/trick.h"
#include "../../player/player.h"
#include "../../player/ai/ai.h"
#include "../../card/hand.h"
#include "../../card/trick.h"
#include "../../os/bug_report_replay.h"
#include "../../utils/file.h"

#include <gtkmm/drawingarea.h>
#include <gtkmm/main.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   parent object
 **/
Table::Table(Base* const parent) :
  Base(parent),
  party_points_(make_unique<PartyPoints>(this)),
  gameinfo_(make_unique<GameInfoDialog>(this)),
  surface_(Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, 1, 1))
{
  ui->main_window->container->add(*this);

  show();

  for (auto p : vector<Position>({Position::south,
                                 Position::west,
                                 Position::north,
                                 Position::east})) {
    hand_[p] = std::make_unique<Hand>(*this, p);
    trickpile_[p] = std::make_unique<TrickPile>(*this, p);
    icongroup_[p] = std::make_unique<Icongroup>(*this, p);
    name_[p] = std::make_unique<Name>(*this, p);
    reservation_[p] = std::make_unique<Reservation>(*this, p);
  }
  trick_ = std::make_unique<Trick>(*this);
  poverty_ = std::make_unique<Poverty>(*this);
  cards_distribution_  = std::make_unique<CardsDistribution>(*this);

  for (auto& t : trickpile_)
    part_.push_back(t.second.get());
  for (auto& n : name_)
    part_.push_back(n.second.get());
  for (auto& i : icongroup_)
    part_.push_back(i.second.get());
  for (auto& h : hand_)
    part_.push_back(h.second.get());
  part_.push_back(trick_.get());
  part_.push_back(poverty_.get());
  part_.push_back(cards_distribution_.get());

  add_events(Gdk::BUTTON_PRESS_MASK);
  add_events(Gdk::SCROLL_MASK);
  add_events(Gdk::POINTER_MOTION_MASK);

  load_background();

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_expose_event()       .connect(sigc::mem_fun(*this, &Table::on_expose_event));
  signal_configure_event()    .connect(sigc::mem_fun(*this, &Table::on_configure_event));
  signal_button_press_event() .connect(sigc::mem_fun(*this, &Table::on_button_press_event));
  signal_event()              .connect(sigc::mem_fun(*this, &Table::on_event));
  signal_motion_notify_event().connect(sigc::mem_fun(*this, &Table::on_motion_notify_event));
#endif

  Party::signal_open.connect_back([this](Party& party){ draw_all(); }, disconnector_);
  ::party->signal_finish.connect_back(*this, &Table::party_finish, disconnector_);
  ::preferences.signal_changed().connect_back([this](auto type, auto* value) {preference_changed(type);}, disconnector_);
} // Table::Table(Base* parent)

/** destruktor
 **/
Table::~Table() = default;

/** show the party finished window
 **/
void
Table::party_finish()
{
  if (!party_finished_) {
    party_finished_ = std::make_unique<PartyFinished>(this);
  }

  draw_all();

  if (!(::fast_play & FastPlay::party_finished))
    party_finished_->party_finished();
} // void Table::party_finish()

/** a gameplay action has happened
 **
 ** @param    action   gameplay action
 **/
void
Table::gameplay_action(GameplayAction const& action)
{
  if (gameinfo_)
    gameinfo_->update_announcement_reply();
} // void Table::gameplay_action(GameplayAction action)

/** create the htin-objects
 **/
void
Table::game_open()
{
  if (::debug("pdf")) {
    open_pdf();
  }
}

/** distribute the cards manually
 **/
void
Table::game_distribute_cards_manually()
{
  cards_distribution().reset();
  force_redraw_all();
  force_redraw_all(); // two times to get all positions right
  while (!ui->thrower
         && !cards_distribution().finished()) {
    ::ui->wait();
  }
}

/** the cards are distributed in the game
 **
 ** @todo     distribute in 3er packs (animation)
 **/
void
Table::game_cards_distributed()
{
  force_redraw_all();
}

/** the game is redistributed
 **/
void
Table::game_redistribute()
{
  force_redraw_all();
}

/** ask 'player' for a reservation
 **
 ** @param    player   player who is asked
 **/
void
Table::reservation_ask(Player const& player)
{
  // redraw the names so that the current player is highlighted
  draw_all();

  // if this is the first reservation to ask, show the reservation window for the next human player
  if (!(   player.game().party().is_duty_soli_round()
        && !player.game().rule(Rule::Type::offer_duty_solo))) {
    for (Player const* p = &player.game().players().following(player);
         *p != player;
         p = &player.game().players().following(*p)) {
      if (p->type() == Player::Type::human) {
        if (player == player.game().startplayer())
          reservation(*p).show_for_selection();
        else
          reservation(*p).update_for_reservation();
        break;
      } // if (p->type() == Player::Type::human)
    } // for (p)

  } // if (player is startplayer and not human
} // void Table::reservation_ask(Player player)

/** gets the reservation of the given player:
 **
 ** @param    player   player, whose reservation schould be get
 **
 ** @return   reservation of the player
 **/
::Reservation
Table::reservation_get(Player const& player)
{
  draw_all();
  return reservation_[position(player)]->get();
} // ::Reservation Table::reservation_get(player)

/** the game starts
 **/
void
Table::game_start()
{
  force_redraw_all();
  party_points_->game_start();

  for (auto& r : reservation_)
    r.second->hide();

  // if we have a solo player show the info window
  if (ui->game().type() != GameType::normal) {

    if (   !(::fast_play & FastPlay::gameplay_information)
        && !(   ::bug_report_replay
             && ::bug_report_replay->auto_action())
        && ::preferences(::Preferences::Type::show_gametype_window))
      gameinfo_->show(new GameInfoDialog::Information::GameType(this, ui->game()),
                            ::preferences(::Preferences::Type::close_gametype_window_automatically),
                            ::preferences(::Preferences::Type::gametype_window_close_delay));
  } // if (ui->game().type() != GameType::normal)

  force_redraw_all();
} // void Table::game_start()

/** the player has to play a card
 **
 ** @param    player   the active player
 **/
void
Table::card_get(Player const& player)
{
  if (   card_suggestion_
      && card_suggestion_->is_visible())
    show_card_suggestion();

  get_card_ = true;
  mouse_cursor_update();
  hand(player).force_redraw();
  draw_all();
} // void Table::card_get(Player player)

/** the player has played a card
 **/
void
Table::card_got()
{
  get_card_ = false;
} // void Table::card_got()

/** the player has played a card
 **
 ** @param    player   the active player
 **/
void
Table::card_played(Player const& player)
{
  if (card_suggestion_)
    card_suggestion_->card_played();

  draw_all();

  if (::debug("screenshots")) {
    if (!ui->game().tricks().current().isfull()) {
      save_screenshot();
    }
  }
} // void Table::card_played(Player player)

/** the trick is full
 **/
void
Table::trick_open()
{
  draw_all();
  if (   last_trick_
      && last_trick_->is_visible()) {
    if (   ::preferences(::Preferences::Type::show_all_tricks)
        && last_trick_->trickno() + 1 < ui->game().tricks().real_current_no() - 1) {
      last_trick_->set_trickno(last_trick_->trickno());
    } else {
      show_last_trick();
    }
  }
  if (::debug("screenshots")) {
    save_screenshot();
  }
}

/** the trick is full
 **/
void
Table::trick_full()
{
  draw_all();
  if (::debug("screenshots")) {
    save_screenshot();
  }
  if (::debug("pdf")) {
    save_trick_in_pdf();
    if (ui->game().tricks().remaining_no() == 0)
      close_pdf();
  }

  if (::fast_play & FastPlay::gameplay_information)
    return ;
  if (::bug_report_replay) {
    if (::bug_report_replay->auto_action())
      return ;
    if (::bug_report_replay->auto_action_end() > 0) {
      auto const& last_auto_action = *::bug_report_replay->actions()[::bug_report_replay->auto_action_end() - 1];
      if (   !::bug_report_replay->actions().empty()
          && ::bug_report_replay->current_action_no() == ::bug_report_replay->auto_action_end()
          && last_auto_action.type() == GameplayActions::Type::trick_full) {
        auto const& trick_full = dynamic_cast<GameplayActions::TrickFull const&>(last_auto_action);
        // Ein 'stop' direkt nach einem vollen Stich soll diesen auch schließen.
        // Allerdings ist ::bug_report_relpy->finished() bereits true, wenn die UI aufgerufen wird.
        if (trick_full.trick == ui->game().tricks().current())
          return;
      }
    }
  }

  if (!full_trick_)
    full_trick_ = std::make_unique<FullTrick>(this);
  full_trick_->show_information(ui->game().tricks().current());
}

/** move the trick in the trickpile
 **
 ** @todo   animation
 **/
void
Table::trick_move_in_pile()
{
  draw_all();
} // void Table::trick_move_in_pile()

/** show the last trick
 **/
void
Table::show_last_trick()
{
  if (!ui->party().in_game())
    return ;

  auto const& game = ui->game();

  if (!(   game.status() >= Game::Status::play
        && game.status() < Game::Status::finished))
    return ;

  if (game.tricks().real_current_no() == 0)
    return ;

  show_trick(game.tricks().trick(game.tricks().real_current_no() - 1));
} // void Table::show_last_trick()

/** show the given trick
 **
 ** @param    trick   trick to show
 **/
void
Table::show_trick(::Trick const& trick)
{
  if (!(   ::preferences(::Preferences::Type::show_all_tricks)
        || trick.no() == trick.game().tricks().real_current_no() - 1))
    return ;

  if (!last_trick_)
    last_trick_ = std::make_unique<LastTrick>(this);

  if (last_trick_->is_visible()) {
    last_trick_->set_trickno(trick.no());
    last_trick_->present();
    return ;
  } // if (last_trick_->is_visible())

  last_trick_->show_last_trick(trick);

  mouse_cursor_update();
} // void Table::show_trick(::Trick trick)

/** show the card suggestion
 **
 ** @param    show_window   whether to show the window
 **/
void
Table::show_card_suggestion(bool const show_window)
{
  DEBUG_ASSERTION((ui->game().players().current_player().type()
                   == Player::Type::human),
                  "Table::show_card_suggestion():\n"
                  "  current player is not human.");

  if (!card_suggestion_)
    card_suggestion_ = std::make_unique<CardSuggestion>(this);

  card_suggestion_->show_information(show_window);
} // void Table::show_card_suggestion(bool show_window)

/** show the game finished window
 **/
void
Table::game_finished()
{
  if (!game_finished_)
    game_finished_ = std::make_unique<GameFinished>(this);

  force_redraw_all();

  party_points_->game_finished();

  if (!(::fast_play & FastPlay::game_finished))
    game_finished_->game_finished();
} // void Table::game_finished()

/** destroy the htin-objects
 **/
void
Table::game_close()
{
  if (last_trick_)
    last_trick_->close();

  if (card_suggestion_)
    card_suggestion_->card_played();

  if (gameinfo_)
    gameinfo_->close();
} // void Table::game_close()

/** @return   whether the game is reviewed
 **/
bool
Table::in_game_review() const
{
  return (   ui->party().in_game()
          && ui->game().status() == Game::Status::finished
          && game_finished_
          && game_finished_->game_review);
} // bool Table::in_game_review() const

/** @return   current tricknumber in the game review
 **/
unsigned
Table::game_review_trickno() const
{
  DEBUG_ASSERTION(in_game_review(),
                  "Table::game_review_trickno():\n"
                  "  not in game review");
  return game_finished_->game_review->trickno();
} // unsigned Table::game_review_trickno() const

/** @return   current trick in the game review
 **/
::Trick const&
Table::game_review_trick() const
{
  DEBUG_ASSERTION(in_game_review(),
                  "Table::game_review_trickno():\n"
                  "  not in game review");
  return game_finished_->game_review->trick();
} // ::Trick const& Table::game_review_trick() const

/** @return   whether a trick is visible in the game review
 **/
bool
Table::game_review_trick_visible() const
{
  DEBUG_ASSERTION(in_game_review(),
                  "Table::game_review_trickno():\n"
                  "  not in game review");
  return game_finished_->game_review->trick_visible();
} // bool Table::game_review_trick_visible() const

/** 'player' has announced 'announcement'
 **
 ** @param    announcement   the announcement that has been made
 ** @param    player      the player that has announced swines
 **/
void
Table::announcement_made(Announcement const announcement,
                         Player const& player)
{
  force_redraw_all();

  if (   !(::fast_play & FastPlay::gameplay_information)
      && !(   ::bug_report_replay
           && ::bug_report_replay->auto_action())
      && ::preferences(::Preferences::Type::show_announcement_window)) {
    gameinfo_->show(new GameInfoDialog::Information::Announcement(this, player, announcement),
                    ::preferences(::Preferences::Type::close_announcement_window_automatically),
                    ::preferences(::Preferences::Type::announcement_window_close_delay));
  }
} // void Table::announcement_made(Announcement const announcement, Player const& player)

/** swines have been announced
 **
 ** @param    player   the player that has announced swines
 **/
void
Table::swines_announced(Player const& player)
{
  force_redraw_all();

  if (player.type() != Player::Type::human) {

    if (   !(::fast_play & FastPlay::gameplay_information)
        && !(   ::bug_report_replay
             && ::bug_report_replay->auto_action())
        && ::preferences(::Preferences::Type::show_swines_window))
      gameinfo_->show(new GameInfoDialog::Information::Swines(this, player),
                      ::preferences(::Preferences::Type::close_swines_window_automatically),
                      ::preferences(::Preferences::Type::swines_window_close_delay));

  } // if (player.type() != Player::Type::human)
} // void Table::swines_announced(Player const& player)

/** hyperswines have been announced
 **
 ** @param    player   the player that has announced hyperswines
 **/
void
Table::hyperswines_announced(Player const& player)
{
  force_redraw_all();

  if (player.type() != Player::Type::human) {

    if (   !(::fast_play & FastPlay::gameplay_information)
        && !(   ::bug_report_replay
             && ::bug_report_replay->auto_action())
        && ::preferences(::Preferences::Type::show_swines_window)) {
      gameinfo_->show(new GameInfoDialog::Information::Hyperswines(this, player),
                      ::preferences(::Preferences::Type::close_swines_window_automatically),
                      ::preferences(::Preferences::Type::swines_window_close_delay));
    } // if (!(::fast_play & FastPlay::gameplay_information))
  } // if (player.type() != Player::Type::human)
} // void Table::hyperswines_announced(Player const& player)

/** marriage: Information of the new team
 ** if the bride is the bridegroom, the bridegroom must play a solo
 **
 ** @param    bridegroom   the player with the marriage
 ** @param    bride      the bride
 **/
void
Table::marriage(Player const& bridegroom,
                Player const& bride)
{
  force_redraw_all();

  if (   !(::fast_play & FastPlay::gameplay_information)
      && !(   ::bug_report_replay
           && ::bug_report_replay->auto_action())
      && ::preferences(::Preferences::Type::show_marriage_window)) {
    gameinfo_->show(new GameInfoDialog::Information::Marriage(this, bridegroom, bride),
                    ::preferences(::Preferences::Type::close_marriage_window_automatically),
                    ::preferences(::Preferences::Type::marriage_window_close_delay));
  }
} // void Table::marriage(Player const& bridegroom, Player const& bride)

/** save the table as screenshot
 **/
void
Table::save_screenshot() const
{
  auto pixbuf = Gdk::Pixbuf::create(surface_, 0, 0, width(), height());
  auto directory = (File::desktop_directory() / "FreeDoko Screenshots");
  if (!is_directory(directory)) {
    create_directory(directory);
  }
  if (!is_directory(directory)) {
    directory = File::desktop_directory();
  }
  if (!is_directory(directory)) {
    return ;
  }
  string filename;
  if (ui->party().in_game()) {
    auto const& game = ui->game();
    if (   game.status() >= Game::Status::play
        && game.status() < Game::Status::finished) {
      auto const& trick = game.tricks().current();
      auto const trickno = game.tricks().real_current_no() + 1;
      filename = ("FreeDoko_Screenshot_"
                  + std::to_string(game.seed()) + "_"
                  + (trickno < 10 ? "0" : "")
                  + std::to_string(trickno)
                  + "-"
                  + std::to_string(trick.actcardno()));
    } else if (game.status() == Game::Status::finished) {
      if (in_game_review()) {
        auto const trickno = game_review_trickno() + 1;
        filename = ("FreeDoko_Screenshot_"
                    + std::to_string(game.seed()) + "_"
                    + (trickno < 10 ? "0" : "")
                    + std::to_string(trickno));
      } else {
        filename =  ("FreeDoko_Screenshot_"
                     + std::to_string(game.seed()) + "_"
                     + "finished");
      }

    } else {
      filename = ("FreeDoko_Screenshot_"
                  + std::to_string(game.seed()));
    }
  } else {
    filename = "FreeDoko_Screenshot.png";
  }
  pixbuf->save((directory / (filename + ".png")).string(), "png");
}

void Table::open_pdf()
{
  pdf_surface_ = Cairo::PdfSurface::create("Spielverlauf.pdf", width(), height());
}

void Table::save_trick_in_pdf()
{
  auto cr = Cairo::Context::create(pdf_surface_);

  draw_background(cr);

  for (auto& p : part_) {
    auto const outline = p->outline();
    cr->set_source(p->surface(), outline.x, outline.y);
    cr->paint();
  }

  cr->show_page();
}

void Table::close_pdf()
{
  pdf_surface_.clear();
}

/** event: mouse click
 **
 ** @param    button_event   the event of the mouse click
 **
 ** @return   whether there was a reaction
 **/
bool
Table::on_button_press_event(GdkEventButton* button_event)
{
  if (!ui->party().in_game())
    return false;

  int const x = static_cast<int>(button_event->x);
  int const y = static_cast<int>(button_event->y);

  if (in_game_review()) {
    switch (button_event->button) {
    case 3:
    case 4:
      game_finished_->game_review->previous_trick();
      return true;
    case 2:
      game_finished_->game_review->set_trickno(0);
      return true;
    case 1:
    case 5:
      game_finished_->game_review->following_trick();
      return true;
    default:
      break;
    }
  } // if (in_game_review

  if (cards_distribution_->on_button_press_event(button_event))
    return true;

  // shifting cards in a poverty
  if (ui->game().status() == Game::Status::poverty_shift)
    if (poverty().button_press_event(button_event)) {
      mouse_cursor_update();
      return true;
    }

  { // test, whether the last trick shall be shown
    if (ui->party().in_game()) {
      auto const& game = ui->game();
      for (auto const& p : game.players()) {
        auto const trick = trickpile(p).over_trick(x, y);
        if (trick) {
          switch (button_event->button) {
          case 1: // left mouse button
            show_trick(*trick);
            mouse_cursor_update();
            return true;
          default:
            break;
          } // switch (button_event->button)
        } // if (trick)
      } // for (p \in game.player)
    } // if (ui->game().status() in game)
  } // test, whether the last trick shall be shown

  { // test whether an announcement shall be made
    if (button_event->button == 1) {
      if (ui->party().in_game()) {
        for (auto& p : ui->game().players()) {
          if (   p.type() == Player::Type::human
              && icongroup(p).mouse_over_next_announcement()) {
            ui->game().announcements().make_announcement(p.next_announcement(), p);
            mouse_cursor_update();
            return true;
          }
        } // for (p)
      } // if (::in_running_game())
    } // if (button_event->button == 3)
  } // test whether an announcement shall be made

  { // test whether the full trick shall be closed
    if (button_event->button == 1) {
      if (   ui->party().in_game()
          && ui->game().status() == Game::Status::full_trick
          && ui->game().tricks().current().isfull()
          && full_trick_
          && trick().mouse_over_cards()) {
        full_trick_->close();
        return true;
      } // if (::in_running_game() && trick full && ...)
    } // if (button_event->button == 1)
  } // test whether the full trick shall be closed

  { // close the gameinfo dialogs
    if (button_event->button == 3) {
      gameinfo_->close();
      if (   full_trick_
          && ui->game().status() == Game::Status::full_trick
          && ui->game().tricks().current().isfull()) {
        full_trick_->close();
      } else if (last_trick_) {
        last_trick_->hide();
      } else if (card_suggestion_) {
        card_suggestion_->hide();
      }
    } // if (button_event->button == 3)
  } // close the gameinfo dialogs

  { // test, whether a card shall be played
    if (get_card()) {
      Player& player = ui->game().players().current_player();
      if (player.type() == Player::Type::human) {
        auto const cardno = hand(player).cardno_at_position(x, y);

        if (cardno) {
          switch (button_event->button) {
          case 1: { // left mouse button
            auto sorted_hand = hand(player).sorted_hand();
            sorted_hand.request_position_all(*cardno);
            if (   !card_suggestion_
                || (sorted_hand.card_all(*cardno)
                    != card_suggestion_->suggested_card) )
              dynamic_cast<Ai&>(player).set_last_heuristic_to_manual();
            // ToDo: only do this, if the card is in the hand
            //       (for a bug report replay)
            ui->thrower(sorted_hand.requested_card(),
                        __FILE__, __LINE__);

            mouse_cursor_update();
            return true;
          }
          case 3: // right mouse button
            if (card_suggestion_)
              card_suggestion_->play_card();
            mouse_cursor_update();
            return true;
          default:
            break;
          } // switch (button_event->button)
        } // if (cardno)
      } // if (player is human)
    } // if (ui->game().status() == Game::Status::play)
  } // test, whether a card shall be played


  switch (button_event->button) {
  case 1: // left mouse button
    DEBUG("mouse") << "mouse: " << x << '+' << y << '\n';
  } // switch (button_event->button)

#ifdef WINDOWS
#ifdef WORKAROUND
  // Under MS-Windows the information windows can get in the background of the main window, although they should stay in the front.
  // So, when a mouse click is made on the table which is not otherwise interpreted, the information windows are put in the foreground.
  { // put a gameinfo dialog, ... in the foreground
    if (game_finished_
        && game_finished_->is_visible())
      game_finished_->raise();
    if (party_finished_
        && party_finished_->is_visible())
      party_finished_->raise();
    if (card_suggestion_
        && card_suggestion_->is_visible())
      card_suggestion_->raise();
    if (gameinfo_
        && gameinfo_->is_visible())
      gameinfo_->raise();
    if (full_trick_
        && full_trick_->is_visible())
      full_trick_->raise();
    if (last_trick_
        && last_trick_->is_visible())
      last_trick_->raise();
  } // put gameinfo dialog, ... in the foreground
#endif
#endif

  mouse_cursor_update();
  return false;
} // bool Table::on_button_press_event(GdkEventButton* event)

/** event: mouse scroll
 **
 ** @param    event   the event of the scrolling
 **
 ** @return   whether there was a reaction
 **/
bool
Table::on_scroll_event(GdkEventScroll* const event)
{
  if (in_game_review()) {
    switch (event->direction) {
    case GDK_SCROLL_UP:
      game_finished_->game_review->previous_trick();
      break;
    case GDK_SCROLL_DOWN:
      game_finished_->game_review->following_trick();
      break;
    default:
      break;
    } // switch (event->direction)
  } // if (in_game_review)

  return true;
} // bool Table::on_scroll_event(GdkEventScroll* event)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
