/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "table.h"
#include "table/cards_distribution.h"
#include "table/poverty.h"
#include "table/trick.h"
#include "table/hand.h"
#include "table/trickpile.h"
#include "table/icongroup.h"
#include "table/name.h"

#include "ui.h"
#include "cards.h"

#include "../../player/player.h"
#include "../../player/human/human.h"
#include "../../party/party.h"
#include "../../game/game.h"
#include "../../misc/preferences.h"

#include <gtkmm/drawingarea.h>

namespace UI_GTKMM_NS {

auto Table::party_points() -> PartyPoints&
{
  return *party_points_;
}

auto Table::cards_distribution() const -> CardsDistribution const&
{
  return *cards_distribution_;
}

auto Table::cards_distribution() -> CardsDistribution&
{
  return *cards_distribution_;
}

auto Table::poverty() const -> Poverty const&
{
  return *poverty_;
}

auto Table::poverty() -> Poverty&
{
  return *poverty_;
}

auto Table::trick() const -> Trick const&
{
  return *trick_;
}

auto Table::trick() -> Trick&
{
  return *trick_;
}

auto Table::hand(Player const& player) -> Hand&
{
  return hand(position(player));
}

auto Table::hand(Player const& player) const -> Hand const&
{
  return hand(position(player));
}

auto Table::hand(Position const position) -> Hand&
{
  //return *hand_[position]; // needs write access
  return *hand_.find(position)->second;
}

auto Table::hand(Position const position) const -> Hand const&
{
  //return *hand_[position]; // needs write access
  return *hand_.find(position)->second;
}

auto Table::trickpile(Player const& player) -> TrickPile&
{
  return trickpile(position(player));
}

auto Table::trickpile(Player const& player) const -> TrickPile const&
{
  return trickpile(position(player));
}

auto Table::trickpile(Position const position) -> TrickPile&
{
  //return *trickpile_[position]; // needs write access
  return *trickpile_.find(position)->second;
}

auto Table::trickpile(Position const position) const -> TrickPile const&
{
  //return *trickpile_[position]; // needs write access
  return *trickpile_.find(position)->second;
}

auto Table::icongroup(Player const& player) -> Icongroup&
{
  return icongroup(position(player));
}

auto Table::icongroup(Player const& player) const -> Icongroup const&
{
  return icongroup(position(player));
}

auto Table::icongroup(Position const position) -> Icongroup&
{
  //return *icongroup_[position]; // needs write access
  return *icongroup_.find(position)->second;
}

auto Table::icongroup(Position const position) const -> Icongroup const&
{
  //return *icongroup_[position]; // needs write access
  return *icongroup_.find(position)->second;
}

auto Table::name(Player const& player) -> Name&
{
  return name(position(player));
}

auto Table::name(Player const& player) const -> Name const&
{
  return name(position(player));
}

auto Table::name(Position const position) -> Name&
{
  //return *name_[position]; // needs write access
  return *name_.find(position)->second;
}

auto Table::name(Position const position) const -> Name const&
{
  //return *name_[position]; // needs write access
  return *name_.find(position)->second;
}

auto Table::reservation(Player const& player) -> Reservation&
{
  return reservation(position(player));
}

auto Table::reservation(Player const& player) const -> Reservation const&
{
  return reservation(position(player));
}

auto Table::reservation(Position const position) -> Reservation&
{
  //return *reservation_[position]; // needs write access
  return *reservation_.find(position)->second;
}

auto Table::reservation(Position const position) const -> Reservation const&
{
  //return *reservation_[position]; // needs write access
  return *reservation_.find(position)->second;
}

bool Table::get_card() const
{ return get_card_; }


auto Table::position(Player const& player) const -> Position
{
  return ::preferences.position(player.no());
}

auto Table::player(Position const position) const -> Player const&
{
  // ToDo: remove for-loop
  for (auto const& p : ui->game().players())
    if (::preferences.position(p.no()) == position)
      return p;

  DEBUG_ASSERTION(false,
                  "Table::player(position)\n"
                  "  could not find player at position "
                  << "'" << position << "'");

  return ui->game().player(0);
}

auto Table::rotation(Player const& player) const -> Rotation
{
  return rotation(position(player));
}

auto Table::rotation(Position const position) const -> Rotation
{
  switch (position) {
  case Position::north:
    return Rotation::down;
  case Position::south:
    return Rotation::up;
  case Position::west:
    return Rotation::right;
  case Position::east:
    return Rotation::left;
  case Position::center:
    DEBUG_ASSERTION(false,
                    "Table::rotation(position):\n"
                    "  wrong position center (" << position << ")");
  } // switch(position())

  return Rotation::up;
}

auto Table::create_cairo_context() -> Cairo::RefPtr<Cairo::Context>
{
  return Cairo::Context::create(surface_);
}

auto Table::width() const -> int
{
  return get_width();
}

auto Table::height() const -> int
{
  return get_height();
}

auto Table::layout() const -> Table::Layout const&
{
  return layout_;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
