/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "part.h"

#include "../../../card/card.h"
#include "../../../card/hand.h"
#include "../../../card/sorted_hand.h"

class Ai;

namespace UI_GTKMM_NS {

/** the cards for the manual distribution
 **/
class CardsDistribution : public Table::Part {
public:
  /**
   ** The status for checking changes (for redrawing)
   ** @todo: card suggestion
   **/
  struct Status {
    ::Hand hand;
  };

  struct Geometry : public Table::Part::Geometry {
    int rows               = 0;
    int columns            = 0;
    int cards_step_x       = 0;
    int cards_small_step_x = 0;
    int cards_step_y       = 0;

    Outline outline() const override;
  }; // struct Geometry : public Table::Part::Geometry

public:
  explicit CardsDistribution(Table& table);
  ~CardsDistribution() override;

  CardsDistribution(CardsDistribution const&) = delete;
  CardsDistribution& operator=(CardsDistribution const&) = delete;

  void reset();
  bool finished() const;
  SortedHand cards();
  SortedHandConst cards() const;

  bool changed()  const override;
  auto geometry() const -> Geometry;
  auto outline()  const -> Outline         override;

  void draw(Cairo::RefPtr<::Cairo::Context> cr) override;
  void draw_tricks(Cairo::RefPtr<::Cairo::Context> cr);

private:
  optional<unsigned> cardno_at_position(int x, int y) const;
  Card card_at_position(int x, int y) const;

  void move_card_in_hand(unsigned c);
  void move_card_in_hand(Card card);
  void remove_card_from_hand(Player& player, unsigned c);
  void remove_all_cards_from_hand(Player& player);
public:
  void save_hand_of_human_player(::Hand const& hand);
private:
  void set_hand_of_human_player();

  void rotate_to_next_hand();
  bool check_all_cards_distributed();
  void finish();

public:
  bool on_button_press_event(GdkEventButton* event);

public:
  //private:
  Status current_status() const;
  Status last_status_;

private:
  bool finished_ = false;
  ::Hand cards_;
  ::Hand saved_hand_of_human_player_;
}; // class CardsDistribution : public HTIN

bool operator!=(CardsDistribution::Status const& lhs, CardsDistribution::Status const& rhs);
ostream& operator<<(ostream& ostr, CardsDistribution::Status const& status);
ostream& operator<<(ostream& ostr, CardsDistribution::Geometry const& geometry);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
