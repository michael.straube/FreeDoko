/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "part.h"
#include "../../../basetypes/position.h"

class Player;
class Game;

#include <cairomm/context.h>
#include <gdkmm/pixbuf.h>

namespace UI_GTKMM_NS {
class Hand;
class TrickPile;
class Icongroup;
class Name;

/**
 ** base class for 'Hand', 'TrickPile', 'Icongroup' and 'Name'
 **/
class HTIN : public Table::Part {
  public:
    HTIN(Table& table, Position position);
    ~HTIN() override;

    Position position() const;
    Rotation rotation() const;
    Rotation rotation(Position position) const;

    // the corresponding player
    Player const& player() const;

    // the corresponding hand
    Hand& hand();
    Hand const& hand() const;
    Hand& hand(Position position);
    Hand const& hand(Position position) const;
    // the corresponding trickpile
    TrickPile& trickpile();
    TrickPile const& trickpile() const;
    TrickPile& trickpile(Position position);
    TrickPile const& trickpile(Position position) const;
    // the corresponding icongroup
    Icongroup& icongroup();
    Icongroup const& icongroup() const;
    Icongroup& icongroup(Position position);
    Icongroup const& icongroup(Position position) const;
    // the corresponding name
    Name& name();
    Name const& name() const;
    Name& name(Position position);
    Name const& name(Position position) const;

  private:
    Position position_;

  private:
    HTIN() = delete;
    HTIN(HTIN const&) = delete;
    HTIN& operator=(HTIN const&) = delete;
}; // class HTIN : public Base

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
