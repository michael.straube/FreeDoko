/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "layout.h"
#include "../table.h"
#include "name.h"
#include "icongroup.h"
#include "trick.h"
#include "../ui.h"
#include "../cards.h"
#include "../icons.h"

namespace UI_GTKMM_NS {

auto TableLayoutGeometry::outline() const -> Outline
{
  return transform({0, -height, width, height});
}

void TableLayoutGeometry::transform(Cairo::RefPtr<::Cairo::Context> cr) const
{
  switch (rotation) {
  case Rotation::up:
    cr->transform(Cairo::Matrix(1, 0, 0, 1, x, y));
    break;
  case Rotation::down:
    cr->transform(Cairo::Matrix(-1, 0, 0, -1, x, y));
    break;
  case Rotation::right:
    cr->transform(Cairo::Matrix(0, 1, -1, 0, x, y));
    break;
  case Rotation::left:
    cr->transform(Cairo::Matrix(0, -1, 1, 0, x, y));
    break;
  } // switch (rotation)
}

auto TableLayoutGeometry::transform(Outline const outline) const -> Outline
{
  switch (rotation) {
  case Rotation::up:
    return {x + outline.x,
      y + outline.y,
      outline.width,
      outline.height};
  case Rotation::down:
    return {x - outline.x - outline.width,
      y - outline.y - outline.height,
      outline.width,
      outline.height};
  case Rotation::right:
    return {x - outline.y - outline.height,
      y + outline.x,
      outline.height,
      outline.width};
  case Rotation::left:
    return {x + outline.y,
      y - outline.x - outline.width,
      outline.height,
      outline.width};
  } // switch (rotation)
  return {};
}

void TableLayoutGeometry::retransform(int& x, int& y) const
{
  x -= this->x;
  y -= this->y;
  switch (rotation) {
  case Rotation::up:
    return ;
  case Rotation::down: {
    x = -x;
    y = -y;
    return ;
  }
  case Rotation::right: {
    int const t = x;
    x = y;
    y = -t;
  }
    return ;
  case Rotation::left: {
    int const t = x;
    x = -y;
    y = t;
    return ;
  }
  } // switch (rotation)
}

void TableLayout::recalc(Table const& table)
{
  width  = table.get_width();
  height = table.get_height();

  // update the layout
  if (width / 3 < height / 2)
    recalc_standard(table);
  else
    recalc_widescreen(table);
}

void TableLayout::recalc_standard(Table const& table)
{
  type = Type::standard;

  // Kürzere Schreibweise
  auto const cards_width  = table.ui->cards->width();
  auto const cards_height = table.ui->cards->height();
  auto const icons_max_height = table.ui->icons->max_height();

  auto const north = Position::north;
  auto const south = Position::south;
  auto const east  = Position::east;
  auto const west  = Position::west;

  border_x = cards_height / 10;
  border_y = cards_height / 10;

  trick.width  = table.trick().TrickDrawing::width();
  trick.height = table.trick().TrickDrawing::height();
  trick.x = width  / 2;
  trick.y = height / 2;

  // Es werden erst alle Werte in neue Variablen geschrieben, anschließend in die Elemente der Struktur.
  // Grund sind die vielen Abhängigkeiten der verschiedenen Größen voneinander.
  // Dadurch können die Werte nicht einfach nacheinander ermittelt werden sondern die Reihenfolge muss durcheinander sein.
  // Der Compiler gibt dann einen Fehler auf, wenn ein Wert verwendet wird, der noch nicht ermittelt (= definiert) wurde.

  int const hand_south_y = height - border_y;
  int const hand_south_margin_x = cards_height / 10;
  int const hand_south_margin_y = cards_height / 10;
  int const hand_south_height = cards_height;

  int const hand_north_y = border_y;
  int const hand_north_height = cards_height;
  int const hand_north_margin_x = cards_height / 10;
  int const hand_north_margin_y = cards_height / 10;

  int const hand_west_x = border_x;
  int const hand_west_margin_x = cards_height / 10;
  int const hand_west_margin_y = cards_height / 10;
  int const hand_west_height = cards_height;

  int const hand_east_x = width - border_x;
  int const hand_east_margin_x = cards_height / 10;
  int const hand_east_margin_y = cards_height / 10;
  int const hand_east_height = cards_height;

  // width und height wird beim Zeichnen von Name gesetzt.
  int const name_south_width  = table.name(south).width();
  int const name_south_height = table.name(south).height();
  int const name_south_y = (  hand_south_y
                            - hand_south_height
                            - hand_south_margin_y);

  int const name_north_width  = table.name(north).width();
  int const name_north_height = table.name(north).height();
  int const name_north_y = (  hand_north_y
                            + hand_north_height
                            + hand_north_margin_y
                            + name_north_height);

  int const name_west_width  = table.name(west).width();
  int const name_west_height = table.name(west).height();
  int const name_west_x = hand_west_x;

  int const name_east_width  = table.name(east).width();
  int const name_east_height = table.name(east).height();
  int const name_east_x = min(hand_east_x - hand_east_height,
                              width - border_x - name_east_width);

  int const icongroup_south_y = (  hand_south_y
                                 - hand_south_height
                                 - hand_south_margin_y);
  int const icongroup_south_height = icons_max_height;

  int const icongroup_north_y = (  hand_north_y
                                 + hand_north_height
                                 + hand_north_margin_y);
  int const icongroup_north_height = icons_max_height;

  int const icongroup_west_x = (  hand_west_x
                                + hand_west_height
                                + hand_west_margin_y);
  int const icongroup_west_height = icons_max_height;

  int const icongroup_east_x = (  hand_east_x
                                - hand_east_height
                                - hand_east_margin_y);
  int const icongroup_east_height = icons_max_height;

  int const hand_north_x = (width
                            - border_x
                            - hand_east_height
                            - hand_east_margin_y
                            - icongroup_east_height);
  int const hand_north_width = max(2 * cards_width,
                                   width
                                   - border_x
                                   - hand_west_height
                                   - hand_west_margin_y
                                   - icongroup_west_height
                                   - border_x
                                   - hand_east_height
                                   - hand_east_margin_y
                                   - icongroup_east_height);

  int const hand_south_x = (border_x
                            + hand_west_height
                            + hand_west_margin_y
                            + icongroup_west_height);
  int const hand_south_width = max(2 * cards_width,
                                   width
                                   - border_x
                                   - hand_west_height
                                   - hand_west_margin_y
                                   - icongroup_west_height
                                   - border_x
                                   - hand_east_height
                                   - hand_east_margin_y
                                   - icongroup_east_height);

  int const hand_east_y = (height
                           - border_y
                           - hand_south_height
                           - hand_south_margin_y
                           - icongroup_south_height);
  int const hand_east_width = max(2 * cards_width,
                                  height
                                  - border_y
                                  - hand_north_height
                                  - hand_north_margin_y
                                  - icongroup_north_height
                                  - border_y
                                  - hand_south_height
                                  - hand_south_margin_y
                                  - icongroup_south_height);

  int const hand_west_y = (border_y
                           + hand_north_height
                           + hand_north_margin_y
                           + icongroup_north_height);
  int const hand_west_width = max(2 * cards_width,
                                  height
                                  - border_y
                                  - hand_north_height
                                  - hand_north_margin_y
                                  - icongroup_north_height
                                  - border_y
                                  - hand_south_height
                                  - hand_south_margin_y
                                  - icongroup_south_height);

  int const trickpile_south_x = (  hand_south_x
                                 + hand_south_width
                                 + hand_south_margin_x);
  int const trickpile_south_y = hand_south_y;
  int const trickpile_south_width = max(cards_height, cards_width);
  int const trickpile_south_height = max(cards_height, cards_width);

  int const trickpile_north_x = (  hand_north_x
                                 - hand_north_width
                                 - hand_north_margin_x);
  int const trickpile_north_y = hand_north_y;
  int const trickpile_north_width  = max(cards_height, cards_width);
  int const trickpile_north_height = max(cards_height, cards_width);

  int const trickpile_west_x = hand_west_x;
  int const trickpile_west_y = (  hand_west_y
                                + hand_west_width
                                + hand_west_margin_x);
  int const trickpile_west_width  = max(cards_height, cards_width);
  int const trickpile_west_height = max(cards_height, cards_width);

  int const trickpile_east_x = hand_east_x;
  int const trickpile_east_y = (  hand_east_y
                                - hand_east_width
                                - hand_east_margin_x
                                - name_east_height
                                - hand_east_margin_x);
  int const trickpile_east_width  = max(cards_height, cards_width);
  int const trickpile_east_height = max(cards_height, cards_width);

  int const name_south_x = hand_south_x;
  int const name_north_x = (  hand_north_x
                            - hand_north_width);
  int const name_east_y = (  hand_east_y
                           - hand_east_width
                           - hand_east_margin_y);
  int const name_west_y = (  hand_west_y
                           - hand_west_margin_y);

  int const icongroup_south_x = (  hand_south_x
                                 + hand_south_width / 4);
  int const icongroup_south_width  = hand_south_width * 3 / 4;
  int const icongroup_north_x = hand_north_x;
  int const icongroup_north_width  = hand_north_width * 3 / 4;
  int const icongroup_west_y = hand_west_y;
  int const icongroup_west_width  = hand_west_width;
  int const icongroup_east_y = hand_east_y;
  int const icongroup_east_width  = hand_east_width;

  int const poverty_arrow_south_x = (hand_south_x
                                     + hand_south_width / 2);
  int const poverty_arrow_south_y = (hand_south_y
                                     - hand_south_height
                                     - hand_south_margin_y
                                     - icongroup_south_height
                                     - 2 * hand_south_margin_y);
  int const poverty_arrow_south_width  = hand_south_width * 3 / 4;
  int const poverty_arrow_south_height = cards_height * 2 / 10;
  int const poverty_arrow_north_x = (hand_north_x
                                     - hand_north_width / 2);
  int const poverty_arrow_north_y = (hand_north_y
                                     + hand_north_height
                                     + hand_north_margin_y
                                     + icongroup_north_height
                                     + 2 * hand_north_margin_y);
  int const poverty_arrow_north_width  = hand_east_width * 3 / 4;
  int const poverty_arrow_north_height = cards_height * 2 / 10;
  int const poverty_arrow_west_x = (hand_west_x
                                    + hand_west_height
                                    + hand_west_margin_y
                                    + icongroup_west_height
                                    + 2 * hand_west_margin_y);
  int const poverty_arrow_west_y = (hand_west_y
                                    + hand_west_width / 2);
  int const poverty_arrow_west_width  = hand_east_width * 3 / 4;
  int const poverty_arrow_west_height = cards_height * 2 / 10;
  int const poverty_arrow_east_x = (hand_east_x
                                    - hand_east_height
                                    - hand_east_margin_y
                                    - icongroup_east_height
                                    - 2 * hand_east_margin_y);
  int const poverty_arrow_east_y = (hand_east_y
                                    - hand_east_width / 2);
  int const poverty_arrow_east_width  = hand_east_width * 3 / 4;
  int const poverty_arrow_east_height = cards_height * 2 / 10;

  int const poverty_cards_south_x = (  hand_south_x
                                     + hand_south_width / 2);
  int const poverty_cards_south_y = (  poverty_arrow_south_y
                                     - poverty_arrow_south_height
                                     - 2 * hand_south_margin_y);
  int const poverty_cards_south_width  = hand_south_width * 3 / 4;
  int const poverty_cards_south_height = min(cards_height * 3 / 2,
                                             poverty_cards_south_y
                                             - icongroup_north_y
                                             - icongroup_north_height
                                             - hand_north_margin_y);
  int const poverty_cards_north_x = (hand_north_x
                                     - hand_north_width / 2);
  int const poverty_cards_north_y = (poverty_arrow_north_y
                                     + poverty_arrow_north_height
                                     + 2 * hand_north_margin_y);
  int const poverty_cards_north_width  = hand_east_width * 3 / 4;
  int const poverty_cards_north_height = min(cards_height * 3 / 2,
                                             icongroup_south_y
                                             - icongroup_south_height
                                             - hand_south_margin_y
                                             - poverty_cards_north_y);
  int const poverty_cards_west_x = (poverty_arrow_west_x
                                    + poverty_arrow_west_height
                                    + 2 * hand_west_margin_y);
  int const poverty_cards_west_y = (hand_west_y
                                    + hand_west_width / 2);
  int const poverty_cards_west_width  = hand_east_width * 3 / 4;
  int const poverty_cards_west_height = cards_height * 3 / 2;
  int const poverty_cards_east_x = (poverty_arrow_east_x
                                    - poverty_arrow_east_height
                                    - 2 * hand_east_margin_y);
  int const poverty_cards_east_y = (hand_east_y
                                    - hand_east_width / 2);
  int const poverty_cards_east_width  = hand_east_width * 3 / 4;
  int const poverty_cards_east_height = cards_height * 3 / 2;


  hand[south].rotation = Rotation::up;
  hand[south].x        = hand_south_x;
  hand[south].y        = hand_south_y;
  hand[south].margin_x = hand_south_margin_x;
  hand[south].margin_y = hand_south_margin_y;
  hand[south].width    = hand_south_width;
  hand[south].height   = hand_south_height;

  hand[north].rotation = Rotation::down;
  hand[north].x        = hand_north_x;
  hand[north].y        = hand_north_y;
  hand[north].margin_x = hand_north_margin_x;
  hand[north].margin_y = hand_north_margin_y;
  hand[north].width    = hand_north_width;
  hand[north].height   = hand_north_height;

  hand[west].rotation = Rotation::right;
  hand[west].x        = hand_west_x;
  hand[west].y        = hand_west_y;
  hand[west].margin_x = hand_west_margin_x;
  hand[west].margin_y = hand_west_margin_y;
  hand[west].width    = hand_west_width;
  hand[west].height   = hand_west_height;

  hand[east].rotation = Rotation::left;
  hand[east].x        = hand_east_x;
  hand[east].y        = hand_east_y;
  hand[east].margin_x = hand_east_margin_x;
  hand[east].margin_y = hand_east_margin_y;
  hand[east].width    = hand_east_width;
  hand[east].height   = hand_east_height;

  trickpile[south].rotation = Rotation::up;
  trickpile[south].x      = trickpile_south_x;
  trickpile[south].y      = trickpile_south_y;
  trickpile[south].width  = trickpile_south_width;
  trickpile[south].height = trickpile_south_height;

  trickpile[north].rotation = Rotation::down;
  trickpile[north].x      = trickpile_north_x;
  trickpile[north].y      = trickpile_north_y;
  trickpile[north].width  = trickpile_north_width;
  trickpile[north].height = trickpile_north_height;

  trickpile[west].rotation = Rotation::right;
  trickpile[west].x      = trickpile_west_x;
  trickpile[west].y      = trickpile_west_y;
  trickpile[west].width  = trickpile_west_width;
  trickpile[west].height = trickpile_west_height;

  trickpile[east].rotation = Rotation::left;
  trickpile[east].x      = trickpile_east_x;
  trickpile[east].y      = trickpile_east_y;
  trickpile[east].width  = trickpile_east_width;
  trickpile[east].height = trickpile_east_height;

  name[south].rotation = Rotation::up;
  name[south].x      = name_south_x;
  name[south].y      = name_south_y;
  name[south].width  = name_south_width;
  name[south].height = name_south_height;

  name[north].rotation = Rotation::up;
  name[north].x      = name_north_x;
  name[north].y      = name_north_y;
  name[north].width  = name_north_width;
  name[north].height = name_north_height;

  name[west].rotation = Rotation::up;
  name[west].x      = name_west_x;
  name[west].y      = name_west_y;
  name[west].width  = name_west_width;
  name[west].height = name_west_height;

  name[east].rotation = Rotation::up;
  name[east].x      = name_east_x;
  name[east].y      = name_east_y;
  name[east].width  = name_east_width;
  name[east].height = name_east_height;

  icongroup[south].rotation = Rotation::up;
  icongroup[south].x      = icongroup_south_x;
  icongroup[south].y      = icongroup_south_y;
  icongroup[south].width  = icongroup_south_width;
  icongroup[south].height = icongroup_south_height;

  icongroup[north].rotation = Rotation::down;
  icongroup[north].x      = icongroup_north_x;
  icongroup[north].y      = icongroup_north_y;
  icongroup[north].width  = icongroup_north_width;
  icongroup[north].height = icongroup_north_height;

  icongroup[west].rotation = Rotation::right;
  icongroup[west].x      = icongroup_west_x;
  icongroup[west].y      = icongroup_west_y;
  icongroup[west].width  = icongroup_west_width;
  icongroup[west].height = icongroup_west_height;

  icongroup[east].rotation = Rotation::left;
  icongroup[east].x      = icongroup_east_x;
  icongroup[east].y      = icongroup_east_y;
  icongroup[east].width  = icongroup_east_width;
  icongroup[east].height = icongroup_east_height;

  poverty_cards[south].rotation = Rotation::up;
  poverty_cards[south].x      = poverty_cards_south_x;
  poverty_cards[south].y      = poverty_cards_south_y;
  poverty_cards[south].width  = poverty_cards_south_width;
  poverty_cards[south].height = poverty_cards_south_height;
  poverty_cards[north].rotation = Rotation::down;
  poverty_cards[north].x      = poverty_cards_north_x;
  poverty_cards[north].y      = poverty_cards_north_y;
  poverty_cards[north].width  = poverty_cards_north_width;
  poverty_cards[north].height = poverty_cards_north_height;
  poverty_cards[west].rotation = Rotation::right;
  poverty_cards[west].x      = poverty_cards_west_x;
  poverty_cards[west].y      = poverty_cards_west_y;
  poverty_cards[west].width  = poverty_cards_west_width;
  poverty_cards[west].height = poverty_cards_west_height;
  poverty_cards[east].rotation = Rotation::left;
  poverty_cards[east].x      = poverty_cards_east_x;
  poverty_cards[east].y      = poverty_cards_east_y;
  poverty_cards[east].width  = poverty_cards_east_width;
  poverty_cards[east].height = poverty_cards_east_height;

  poverty_arrow[south].rotation = Rotation::up;
  poverty_arrow[south].x      = poverty_arrow_south_x;
  poverty_arrow[south].y      = poverty_arrow_south_y;
  poverty_arrow[south].width  = poverty_arrow_south_width;
  poverty_arrow[south].height = poverty_arrow_south_height;
  poverty_arrow[north].rotation = Rotation::down;
  poverty_arrow[north].x      = poverty_arrow_north_x;
  poverty_arrow[north].y      = poverty_arrow_north_y;
  poverty_arrow[north].width  = poverty_arrow_north_width;
  poverty_arrow[north].height = poverty_arrow_north_height;
  poverty_arrow[west].rotation = Rotation::right;
  poverty_arrow[west].x      = poverty_arrow_west_x;
  poverty_arrow[west].y      = poverty_arrow_west_y;
  poverty_arrow[west].width  = poverty_arrow_west_width;
  poverty_arrow[west].height = poverty_arrow_west_height;
  poverty_arrow[east].rotation = Rotation::left;
  poverty_arrow[east].x      = poverty_arrow_east_x;
  poverty_arrow[east].y      = poverty_arrow_east_y;
  poverty_arrow[east].width  = poverty_arrow_east_width;
  poverty_arrow[east].height = poverty_arrow_east_height;
}


void TableLayout::recalc_widescreen(Table const& table)
{
  type = Type::widescreen;

  // Kürzere Schreibweise
  auto const cards_width  = table.ui->cards->width();
  auto const cards_height = table.ui->cards->height();
  auto const icons_max_height = table.ui->icons->max_height();

  auto const swines_north = (   table.icongroup(Position::north).current_status().swines
                             || table.icongroup(Position::north).current_status().hyperswines);
  auto const swines_west  = (   table.icongroup(Position::west ).current_status().swines
                             || table.icongroup(Position::west ).current_status().hyperswines);
  auto const swines_east  = (   table.icongroup(Position::east ).current_status().swines
                             || table.icongroup(Position::east ).current_status().hyperswines);

  auto const north = Position::north;
  auto const south = Position::south;
  auto const east  = Position::east;
  auto const west  = Position::west;

  width  = table.width();
  height = table.height();

  border_x = cards_height / 10;
  border_y = cards_height / 10;

  trick.width  = table.trick().TrickDrawing::width();
  trick.height = table.trick().TrickDrawing::height();
  trick.x = width  / 2;
  trick.y = height / 2;

  // Es werden erst alle Werte in neue Variablen geschrieben, anschließend in die Elemente der Struktur.
  // Grund sind die vielen Abhängigkeiten der verschiedenen Größen voneinander.
  // Dadurch können die Werte nicht einfach nacheinander ermittelt werden sondern die Reihenfolge muss durcheinander sein.
  // Der Compiler gibt dann einen Fehler auf, wenn ein Wert verwendet wird, der noch nicht ermittelt (= definiert) wurde.

  int const hand_south_y = height - border_y;
  int const hand_south_margin_x = cards_height / 10;
  int const hand_south_margin_y = cards_height / 10;
  int const hand_south_height = cards_height;

  int const hand_north_y = border_y;
  int const hand_north_height = cards_height;
  int const hand_north_margin_x = cards_height / 10;
  int const hand_north_margin_y = cards_height / 10;

  int const hand_west_x = border_x;
  int const hand_west_margin_x = cards_height / 10;
  int const hand_west_margin_y = cards_height / 10;
  int const hand_west_height = cards_height;

  int const hand_east_x = width - border_x;
  int const hand_east_margin_x = cards_height / 10;
  int const hand_east_margin_y = cards_height / 10;
  int const hand_east_height = cards_height;

  // width und height wird beim Zeichnen von Name gesetzt.
  int const name_south_width  = table.name(south).width() + 3;
  int const name_south_height = table.name(south).height();
  int const name_north_width  = table.name(north).width() + 3;
  int const name_north_height = table.name(north).height();
  int const name_west_width   = table.name(west).width() + 3;
  int const name_west_height  = table.name(west).height();
  int const name_east_width   = table.name(east).width() + 3;
  int const name_east_height  = table.name(east).height();

  int const icongroup_south_y = (  hand_south_y
                                 - hand_south_height
                                 - hand_south_margin_y);
  int const icongroup_south_height = icons_max_height;

  int const icongroup_north_y = (  hand_north_y
                                 + hand_north_height
                                 + hand_north_margin_y);
  int const icongroup_north_height = icons_max_height;

  int const icongroup_west_x = (  hand_west_x
                                + hand_west_height
                                + hand_west_margin_y);
  int const icongroup_west_height = icons_max_height;

  int const icongroup_east_x = (  hand_east_x
                                - hand_east_height
                                - hand_east_margin_y);
  int const icongroup_east_height = icons_max_height;

  int name_north_x = 0;
  int hand_north_x = 0;
  int hand_north_width = 0;
  auto pos = max(icongroup_east_x
                 - icongroup_east_height
                 - 2 * cards_height,
                 border_x
                 + 3 * cards_width);
  if (name_north_width + hand_north_margin_x < cards_height) {
    // Wenn genug Platz ist, den Namen links vom Blatt setzen, ansonsten rechts
    name_north_x = max(border_x, cards_height / 2 - hand_north_margin_x - width);
    hand_north_x = max(min(pos, name_north_x + name_north_width),
                       width / 3);
    hand_north_width = (hand_north_x
                        - (  name_north_x
                           + name_north_width
                           + hand_north_margin_x));
  } else {
    hand_north_x = max(min(pos, border_x + cards_height / 2),
                       width / 3);
    name_north_x = hand_north_x + hand_north_margin_x;
    hand_north_width = hand_north_x - border_x - cards_height / 2;
  }

  int const hand_east_y = (height
                           - border_y
                           - hand_south_height
                           - hand_south_margin_y
                           - icongroup_south_height);
  int const hand_east_width = (  hand_east_y
                               - border_y
                               - name_east_height
                               - hand_east_margin_x);

  int const hand_west_y = (  icongroup_north_y
                           + icongroup_north_height
                           + name_west_height
                           + 3 * hand_west_margin_y);
  int const hand_west_width = (  height
                               - border_y
                               - hand_west_y);

  int const hand_south_x = (  border_x
                            + hand_west_height
                            + hand_west_margin_y
                            + icongroup_west_height
                            + cards_height);
  int const hand_south_width = max(2 * cards_width,
                                   icongroup_east_x
                                   - icongroup_east_height
                                   - hand_south_x
                                   - hand_east_margin_x);

  int const name_south_x = hand_south_x;
  int const name_south_y = (  hand_south_y
                            - hand_south_height
                            - hand_south_margin_y);
  int const name_west_x = hand_west_x;
  int const name_west_y = (  hand_west_y
                           - hand_west_margin_y);
  int const name_east_x = (  hand_east_x
                           - hand_east_height);
  int const name_east_y = (  hand_east_y
                           - hand_east_width
                           - hand_east_margin_y);

  int const icongroup_south_x = (  hand_south_x
                                 + hand_south_width / 4);
  int const icongroup_south_width  = hand_south_width * 3 / 4;
  int const icongroup_north_x = hand_north_x;
  int const icongroup_north_width  = hand_north_width;
  int const icongroup_west_y = hand_west_y;
  int const icongroup_west_width  = hand_west_width;
  int const icongroup_east_y = hand_east_y;
  int const icongroup_east_width  = (hand_east_width
                                     + hand_east_margin_x
                                     + name_east_height);

  int const name_north_y = (  border_y
                            + hand_north_height);

  int const trickpile_south_x = (  hand_south_x
                                 + hand_south_width
                                 + hand_south_margin_x);
  int const trickpile_south_y = hand_south_y;
  int const trickpile_south_width = max(cards_height, cards_width);
  int const trickpile_south_height = max(cards_height, cards_width);

  int const trickpile_north_x = hand_north_x;
  int const trickpile_north_y = (  hand_north_y
                                 + hand_north_height
                                 + hand_north_margin_y
                                 + (swines_north ? 1.2 * icongroup_north_height : 0));
  int const trickpile_north_width  = max(cards_height, cards_width);
  int const trickpile_north_height = max(cards_height, cards_width);

  int const trickpile_west_x = (  hand_west_x
                                + hand_west_height
                                + hand_west_margin_y
                                + (swines_west ? 1.2 * icongroup_west_height : 0));
  int const trickpile_west_y = (  hand_west_y
                                + hand_west_width
                                - cards_height);
  int const trickpile_west_width  = max(cards_height, cards_width);
  int const trickpile_west_height = max(cards_height, cards_width);

  int const trickpile_east_x = (  hand_east_x
                                - hand_east_height
                                - hand_east_margin_y
                                - (swines_east ? icongroup_east_height + hand_east_margin_y : 0));
  int const trickpile_east_y = (  border_y
                                + cards_height);
  int const trickpile_east_width  = max(cards_height, cards_width);
  int const trickpile_east_height = max(cards_height, cards_width);

  int const poverty_arrow_south_x = (hand_south_x
                                     + hand_south_width / 2);
  int const poverty_arrow_south_y = (hand_south_y
                                     - hand_south_height
                                     - hand_south_margin_y
                                     - icongroup_south_height
                                     - 2 * hand_south_margin_y);
  int const poverty_arrow_south_width  = hand_south_width * 3 / 4;
  int const poverty_arrow_south_height = cards_height * 2 / 10;
  int const poverty_arrow_north_x = max(icongroup_west_x
                                        + icongroup_west_height
                                        + hand_west_margin_y
                                        + hand_north_width / 2,
                                        hand_north_x);
  int const poverty_arrow_north_y = (icongroup_north_y
                                     + icongroup_north_height
                                     + hand_north_margin_y);
  int const poverty_arrow_north_width  = hand_east_width * 3 / 4;
  int const poverty_arrow_north_height = cards_height * 2 / 10;
  int const poverty_arrow_west_x = (icongroup_west_x
                                    + icongroup_west_height
                                    + hand_west_margin_y);
  int const poverty_arrow_west_y = ((icongroup_north_y
                                     + icongroup_north_height
                                     + hand_south_margin_y) / 2
                                    + (icongroup_south_y
                                       - icongroup_south_height
                                       - hand_south_margin_y
                                      ) / 2);
  int const poverty_arrow_west_width  = (icongroup_south_y
                                         - icongroup_south_height
                                         - hand_south_margin_y
                                         - (icongroup_north_y
                                            + icongroup_north_height
                                            + hand_north_margin_y
                                            + hand_south_margin_y));
  int const poverty_arrow_west_height = cards_height * 2 / 10;
  int const poverty_arrow_east_x = (hand_east_x
                                    - hand_east_height
                                    - hand_east_margin_y
                                    - icongroup_east_height
                                    - 2 * hand_east_margin_y);
  int const poverty_arrow_east_y = (hand_east_y
                                    - hand_east_width / 2);
  int const poverty_arrow_east_width  = hand_east_width * 3 / 4;
  int const poverty_arrow_east_height = cards_height * 2 / 10;

  int const poverty_cards_south_x = (  hand_south_x
                                     + hand_south_width / 2);
  int const poverty_cards_south_y = (  poverty_arrow_south_y
                                     - poverty_arrow_south_height
                                     - 2 * hand_south_margin_y);
  int const poverty_cards_south_width  = hand_south_width * 3 / 4;
  int const poverty_cards_south_height = min(cards_height * 3 / 2,
                                             poverty_cards_south_y
                                             - border_y);
  int const poverty_cards_north_x = poverty_arrow_north_x;
  int const poverty_cards_north_y = (poverty_arrow_north_y
                                     + poverty_arrow_north_height
                                     + 2 * hand_north_margin_y);
  int const poverty_cards_north_width  = hand_east_width * 3 / 4;
  int const poverty_cards_north_height = min(cards_height * 3 / 2,
                                             hand_south_y
                                             - (name_north_y
                                                + name_north_height
                                                + hand_north_margin_y));
  int const poverty_cards_west_x = (poverty_arrow_west_x
                                    + poverty_arrow_west_height
                                    + 2 * hand_west_margin_y);
  int const poverty_cards_west_y = poverty_arrow_west_y;
  int const poverty_cards_west_width  = hand_west_width * 3 / 4;
  int const poverty_cards_west_height = cards_height * 3 / 2;
  int const poverty_cards_east_x = (poverty_arrow_east_x
                                    - poverty_arrow_east_height
                                    - 2 * hand_east_margin_y);
  int const poverty_cards_east_y = poverty_arrow_east_y;
  int const poverty_cards_east_width  = hand_east_width * 3 / 4;
  int const poverty_cards_east_height = cards_height * 3 / 2;

  hand[south].rotation = Rotation::up;
  hand[south].x        = hand_south_x;
  hand[south].y        = hand_south_y;
  hand[south].margin_x = hand_south_margin_x;
  hand[south].margin_y = hand_south_margin_y;
  hand[south].width    = hand_south_width;
  hand[south].height   = hand_south_height;

  hand[north].rotation = Rotation::down;
  hand[north].x        = hand_north_x;
  hand[north].y        = hand_north_y;
  hand[north].margin_x = hand_north_margin_x;
  hand[north].margin_y = hand_north_margin_y;
  hand[north].width    = hand_north_width;
  hand[north].height   = hand_north_height;

  hand[west].rotation = Rotation::right;
  hand[west].x        = hand_west_x;
  hand[west].y        = hand_west_y;
  hand[west].margin_x = hand_west_margin_x;
  hand[west].margin_y = hand_west_margin_y;
  hand[west].width    = hand_west_width;
  hand[west].height   = hand_west_height;

  hand[east].rotation = Rotation::left;
  hand[east].x        = hand_east_x;
  hand[east].y        = hand_east_y;
  hand[east].margin_x = hand_east_margin_x;
  hand[east].margin_y = hand_east_margin_y;
  hand[east].width    = hand_east_width;
  hand[east].height   = hand_east_height;

  trickpile[south].rotation = Rotation::up;
  trickpile[south].x      = trickpile_south_x;
  trickpile[south].y      = trickpile_south_y;
  trickpile[south].width  = trickpile_south_width;
  trickpile[south].height = trickpile_south_height;

  trickpile[north].rotation = Rotation::down;
  trickpile[north].x      = trickpile_north_x;
  trickpile[north].y      = trickpile_north_y;
  trickpile[north].width  = trickpile_north_width;
  trickpile[north].height = trickpile_north_height;

  trickpile[west].rotation = Rotation::right;
  trickpile[west].x      = trickpile_west_x;
  trickpile[west].y      = trickpile_west_y;
  trickpile[west].width  = trickpile_west_width;
  trickpile[west].height = trickpile_west_height;

  trickpile[east].rotation = Rotation::left;
  trickpile[east].x      = trickpile_east_x;
  trickpile[east].y      = trickpile_east_y;
  trickpile[east].width  = trickpile_east_width;
  trickpile[east].height = trickpile_east_height;

  name[south].rotation = Rotation::up;
  name[south].x      = name_south_x;
  name[south].y      = name_south_y;
  name[south].width  = name_south_width;
  name[south].height = name_south_height;

  name[north].rotation = Rotation::up;
  name[north].x      = name_north_x;
  name[north].y      = name_north_y;
  name[north].width  = name_north_width;
  name[north].height = name_north_height;

  name[west].rotation = Rotation::up;
  name[west].x      = name_west_x;
  name[west].y      = name_west_y;
  name[west].width  = name_west_width;
  name[west].height = name_west_height;

  name[east].rotation = Rotation::up;
  name[east].x      = name_east_x;
  name[east].y      = name_east_y;
  name[east].width  = name_east_width;
  name[east].height = name_east_height;

  icongroup[south].rotation = Rotation::up;
  icongroup[south].x      = icongroup_south_x;
  icongroup[south].y      = icongroup_south_y;
  icongroup[south].width  = icongroup_south_width;
  icongroup[south].height = icongroup_south_height;

  icongroup[north].rotation = Rotation::down;
  icongroup[north].x      = icongroup_north_x;
  icongroup[north].y      = icongroup_north_y;
  icongroup[north].width  = icongroup_north_width;
  icongroup[north].height = icongroup_north_height;

  icongroup[west].rotation = Rotation::right;
  icongroup[west].x      = icongroup_west_x;
  icongroup[west].y      = icongroup_west_y;
  icongroup[west].width  = icongroup_west_width;
  icongroup[west].height = icongroup_west_height;

  icongroup[east].rotation = Rotation::left;
  icongroup[east].x      = icongroup_east_x;
  icongroup[east].y      = icongroup_east_y;
  icongroup[east].width  = icongroup_east_width;
  icongroup[east].height = icongroup_east_height;

  poverty_cards[south].rotation = Rotation::up;
  poverty_cards[south].x      = poverty_cards_south_x;
  poverty_cards[south].y      = poverty_cards_south_y;
  poverty_cards[south].width  = poverty_cards_south_width;
  poverty_cards[south].height = poverty_cards_south_height;
  poverty_cards[north].rotation = Rotation::down;
  poverty_cards[north].x      = poverty_cards_north_x;
  poverty_cards[north].y      = poverty_cards_north_y;
  poverty_cards[north].width  = poverty_cards_north_width;
  poverty_cards[north].height = poverty_cards_north_height;
  poverty_cards[west].rotation = Rotation::right;
  poverty_cards[west].x      = poverty_cards_west_x;
  poverty_cards[west].y      = poverty_cards_west_y;
  poverty_cards[west].width  = poverty_cards_west_width;
  poverty_cards[west].height = poverty_cards_west_height;
  poverty_cards[east].rotation = Rotation::left;
  poverty_cards[east].x      = poverty_cards_east_x;
  poverty_cards[east].y      = poverty_cards_east_y;
  poverty_cards[east].width  = poverty_cards_east_width;
  poverty_cards[east].height = poverty_cards_east_height;

  poverty_arrow[south].rotation = Rotation::up;
  poverty_arrow[south].x      = poverty_arrow_south_x;
  poverty_arrow[south].y      = poverty_arrow_south_y;
  poverty_arrow[south].width  = poverty_arrow_south_width;
  poverty_arrow[south].height = poverty_arrow_south_height;
  poverty_arrow[north].rotation = Rotation::down;
  poverty_arrow[north].x      = poverty_arrow_north_x;
  poverty_arrow[north].y      = poverty_arrow_north_y;
  poverty_arrow[north].width  = poverty_arrow_north_width;
  poverty_arrow[north].height = poverty_arrow_north_height;
  poverty_arrow[west].rotation = Rotation::right;
  poverty_arrow[west].x      = poverty_arrow_west_x;
  poverty_arrow[west].y      = poverty_arrow_west_y;
  poverty_arrow[west].width  = poverty_arrow_west_width;
  poverty_arrow[west].height = poverty_arrow_west_height;
  poverty_arrow[east].rotation = Rotation::left;
  poverty_arrow[east].x      = poverty_arrow_east_x;
  poverty_arrow[east].y      = poverty_arrow_east_y;
  poverty_arrow[east].width  = poverty_arrow_east_width;
  poverty_arrow[east].height = poverty_arrow_east_height;
}


auto to_string(Table::LayoutType const layout_type) -> string
{
  switch (layout_type) {
  case Table::LayoutType::standard:
    return "standard"s;
  case Table::LayoutType::widescreen:
    return "widescreen"s;
  } // switch (layout)
  return {};
}

auto operator<<(ostream& ostr, Table::LayoutType const layout_type) -> ostream&
{
  ostr << to_string(layout_type);
  return ostr;
}

auto operator<<(ostream& ostr, TableLayoutOutline  const& outline) -> ostream&
{
  ostr << outline.width << 'x' << outline.height << '+' << outline.x << '+' << outline.y;
  return ostr;
}

auto operator<<(ostream& ostr, TableLayoutGeometry const& geometry) -> ostream&
{
  ostr << geometry.width << 'x' << geometry.height << '+' << geometry.x << '+' << geometry.y << '(' << geometry.rotation << ')';
  return ostr;
}

auto operator<<(ostream& ostr, TableLayout const& layout_) -> ostream&
{
  // const-cast für einfachen Zugriff auf []
  auto& layout  = const_cast<TableLayout&>(layout_);

  ostr << "size:   " << layout.width << 'x' << layout.height << '\n';
  ostr << "border: " << layout.border_x << ", " << layout.border_y << '\n';

  ostr << "hand[south]:          " << layout.hand[Position::south] << '\n';
  ostr << "hand[north]:          " << layout.hand[Position::north] << '\n';
  ostr << "hand[west]:           " << layout.hand[Position::west]  << '\n';
  ostr << "hand[east]:           " << layout.hand[Position::east]  << '\n';
  ostr << "trickpile[south]:     " << layout.trickpile[Position::south] << '\n';
  ostr << "trickpile[north]:     " << layout.trickpile[Position::north] << '\n';
  ostr << "trickpile[west]:      " << layout.trickpile[Position::west]  << '\n';
  ostr << "trickpile[east]:      " << layout.trickpile[Position::east]  << '\n';
  ostr << "name[south]:          " << layout.name[Position::south] << '\n';
  ostr << "name[north]:          " << layout.name[Position::north] << '\n';
  ostr << "name[west]:           " << layout.name[Position::west]  << '\n';
  ostr << "name[east]:           " << layout.name[Position::east]  << '\n';
  ostr << "icongroup[south]:     " << layout.icongroup[Position::south] << '\n';
  ostr << "icongroup[north]:     " << layout.icongroup[Position::north] << '\n';
  ostr << "icongroup[west]:      " << layout.icongroup[Position::west]  << '\n';
  ostr << "icongroup[east]:      " << layout.icongroup[Position::east]  << '\n';
  ostr << "poverty arrow[south]: " << layout.poverty_arrow[Position::south] << '\n';
  ostr << "poverty arrow[north]: " << layout.poverty_arrow[Position::north] << '\n';
  ostr << "poverty arrow[west]:  " << layout.poverty_arrow[Position::west]  << '\n';
  ostr << "poverty arrow[east]:  " << layout.poverty_arrow[Position::east]  << '\n';
  ostr << "poverty cards[south]: " << layout.poverty_cards[Position::south] << '\n';
  ostr << "poverty cards[north]: " << layout.poverty_cards[Position::north] << '\n';
  ostr << "poverty cards[west]:  " << layout.poverty_cards[Position::west]  << '\n';
  ostr << "poverty cards[east]:  " << layout.poverty_cards[Position::east]  << '\n';

  ostr << "trick: " << layout.trick << '\n';

  return ostr;
}

auto operator+(TableLayoutOutline lhs, TableLayoutOutline rhs) -> TableLayoutOutline
{
  TableLayoutOutline res;
  res.x = min(lhs.x, rhs.x);
  res.y = min(lhs.y, rhs.y);
  res.width = (max(lhs.x + lhs.width,
                   rhs.x + rhs.width)
               - min(lhs.x, rhs.x));
  res.height = (max(lhs.y + lhs.height,
                    rhs.y + rhs.height)
                - min(lhs.y, rhs.y));
  return res;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
