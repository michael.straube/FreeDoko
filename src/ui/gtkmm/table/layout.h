/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "../../../basetypes/position.h"
#include "../../../basetypes/rotation.h"

#include <cairomm/context.h>

namespace UI_GTKMM_NS {
class Table;

enum class TableLayoutType {
  standard,
  widescreen
};

struct TableLayoutOutline {
  int x      = 0;
  int y      = 0;
  int width  = 0;
  int height = 0;

  //operator Gdk::Rectangle() const;
};

// Die grobe Ausrichtung. Die einzelnen Elemente haben spezialisierte Layouts mit mehr Informationen.
struct TableLayoutGeometry {
  using Outline = TableLayoutOutline;

  int x = 0;
  int y = 0;
  int margin_x = 0;
  int margin_y = 0;
  Rotation rotation = Rotation::up;
  // relative to the rotation
  int width  = 0;
  int height = 0;

  virtual Outline outline() const;
  void transform(Cairo::RefPtr<::Cairo::Context> cr) const;
  auto transform(Outline outline)  const -> Outline;
  void retransform(int& x, int& y) const;
};

struct TableLayout {
  using Type     = TableLayoutType;
  using Outline  = TableLayoutOutline;
  using Geometry = TableLayoutGeometry;

  Type type = Type::standard;

  int width    = 0;
  int height   = 0;
  int border_x = 0;
  int border_y = 0;

  std::map<Position, Geometry> hand;
  std::map<Position, Geometry> trickpile;
  std::map<Position, Geometry> name;
  std::map<Position, Geometry> icongroup;
  std::map<Position, Geometry> poverty_cards;
  std::map<Position, Geometry> poverty_arrow;
  Geometry trick;

  void recalc(Table const& table);

private:
  void recalc_standard(Table const& table);
  void recalc_widescreen(Table const& table);
};

auto to_string(TableLayoutType layout_type) -> string;
auto operator<<(ostream& ostr, TableLayoutType layout_type) -> ostream&;

auto operator<<(ostream& ostr, TableLayoutOutline  const& outline)  -> ostream&;
auto operator<<(ostream& ostr, TableLayoutGeometry const& geometry) -> ostream&;
auto operator<<(ostream& ostr, TableLayout         const& layout)   -> ostream&;

auto operator+(TableLayoutOutline lhs, TableLayoutOutline rhs) -> TableLayoutOutline;

} // namespace UI_GTKMM_NS

#endif
