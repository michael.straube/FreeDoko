/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "htin.h"

namespace UI_GTKMM_NS {

/**
 ** the graphic representation of a name
 **
 ** @todo   use of 'Gdk::Point' and 'Gdk::Rectangle'
 **/
class Name : public HTIN {
  public:
    /**
     ** The status for checking changes (for redrawing)
     **/
    struct Status {
      string name;
      bool is_active;
      string color;
      double progress;
    }; // struct Status

  public:
    Name(Table& table, Position position);
    ~Name() override;

    Cairo::RefPtr<Cairo::ImageSurface> surface() override;

    bool changed() const override;
    auto geometry() const -> Geometry;
    auto outline()  const -> Outline override;

    int width()  const;
    int height() const;

    void draw(Cairo::RefPtr<::Cairo::Context> cr) override;

  private:
    int width_ = 0;
    int height_ = 0;

    Status current_status() const;
    Status last_status_;

  private: // unused
    Name() = delete;
    Name(Name const&) = delete;
    Name& operator=(Name const&) = delete;
}; // class Name : public HTIN

bool operator!=(Name::Status const& lhs, Name::Status const& rhs);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
