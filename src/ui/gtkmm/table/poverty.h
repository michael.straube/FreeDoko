/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "htin.h"

#include "../../../card/card.h"
#include "../../../card/hand_cards.h"
class Game;
class Hand;
class Player;

#include <gdkmm/rectangle.h>
#include <gdkmm/types.h>
#include <cairomm/context.h>

namespace UI_GTKMM_NS {

/**
 ** the graphic representation of the poverty cards
 **/
class Poverty : public Table::Part {
  friend class Table;
  public:
  // The status while shifting a poverty
  enum class Status {
    invalid,
    shifting,
    asking,
    shifting_back,
    getting_back,
    accepted,
    denied_by_all,
    finished
  }; // enum Status
  // The actions in shifting a poverty
  enum class Action {
    none,
    shift_cards,
    accept_cards,
    take_card,
    put_card,
    fill_up,
    return_cards,
    get_cards_back
  }; // enum Action
  struct GeometryArrow : public TableLayoutGeometry {
    GeometryArrow(TableLayoutGeometry const& geometry) : TableLayoutGeometry(geometry) { }
    Outline outline() const final;
  };
  struct GeometryCards : public TableLayoutGeometry {
    GeometryCards(TableLayoutGeometry const& geometry) : TableLayoutGeometry(geometry) { }
    Outline outline() const final;
  };

  public:
  explicit Poverty(Table& table);
  ~Poverty() override;

  Position position() const;
  Hand const& hand() const;
  Hand& hand();
  Icongroup const& icongroup() const;
  Name const& name() const;

  // returns whether the middle is full with cards
  bool middle_full() const;
  // returns whether the shifting is valid
  bool shifting_valid() const;

  // 'player' shifts 'cardno' cards
  void shift(Player const& player, unsigned cardno);
  // ask 'player' whether to accept the poverty
  void ask(Player const& player, unsigned cardno);
  // the player 'player' has denied the poverty trumps
  void take_denied(Player const& player);
  // all players have denied to take the cards
  void take_denied_by_all();
  // the player 'player' has accepted the poverty trumps
  // and has returned 'cardno' cards with 'trumpno' trumps
  void take_accepted(Player const& player,
                     unsigned cardno,
                     unsigned trumpno);
  // 'player' shifts cards
  HandCards shift(Player& player);
  // returns whether 'player' accepts the poverty
  bool take_accept(Player const& player, unsigned cardno);
  // the player changes the cards of the poverty
  HandCards cards_change(Player& player,
                         vector<Card> const& cards_shifted);
  // the poverty player 'player' gets 'cards'
  void cards_get_back(Player const& player,
                      vector<Card> const& cards_returned);

  // start and end position of the card / the arrow

  vector<Gdk::Point> cards_pos() const;

  // draws the poverty elements
  bool changed() const override;
  auto geometry() const -> Geometry;
  auto outline()  const -> Outline  override;
  auto geometry_arrow() const -> GeometryArrow;
  auto geometry_cards() const -> GeometryCards;

  void draw(Cairo::RefPtr<::Cairo::Context> cr) override;
  void draw_cards(Cairo::RefPtr<::Cairo::Context> cr);
  void draw_arrow(Cairo::RefPtr<::Cairo::Context> cr);

  // a mouse click
  bool button_press_event(GdkEventButton* event);
  // removes the card from the hand and adds it in the middle
  void add_card_to_shift(unsigned cardno);

  // what action a can be made at the position (for the mouse)
  Action possible_action(int x, int y) const; 
  // which card is under the mouse
  optional<unsigned> cardno_at_position(int x, int y) const;

  private:
  bool is_drawn() const;

  private:
  Player const* player = nullptr; // the active player

  vector<Card> cards; // the cards that are shifted
  Status status = Status::invalid; // which acitivy is made
  bool shift_cards = false; // whether to shift the cards
  bool accept_cards = false; // whether to accept the cards

  private: // unused
  Poverty() = delete;
  Poverty(Poverty const&) = delete;
  Poverty& operator=(Poverty const&) = delete;
}; // class Poverty : public Table::Part 

ostream& operator<<(ostream& ostr, Poverty::Status status);
ostream& operator<<(ostream& ostr, Poverty::Action action);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
