/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "trick.h"
#include "hand.h"
#include "name.h"
#include "icongroup.h"
#include "trickpile.h"
#include "../ui.h"
#include "../cards.h"
#include "../table.h"

#include "../../../party/party.h"
#include "../../../game/game.h"
#include "../../../player/player.h"

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table   corresponding table
 **/
Trick::Trick(Table& table) :
  Table::Part(table),
  TrickDrawing(table)
{ }

/** destructor
 **/
Trick::~Trick() = default;

/**
 **/
bool
Trick::changed() const
{
  if (!surface_)
    return true;
  if (   Table::Part::ui->party().in_game()
      && game().status() >= Game::Status::play) {
    return (trick_drawed_ != game().tricks().current());
  }
  return true;
} // bool Trick::changed() const

auto Trick::geometry() const -> Geometry
{
  return table().layout().trick;
}

auto Trick::outline() const -> Outline
{
  auto const geometry = this->geometry();
  return geometry.transform({-geometry.width / 2, -geometry.height / 2,
                            geometry.width, geometry.height});
}

/** draws the trick
 **
 ** @param    cr       drawing context
 **/
void
Trick::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!Table::Part::ui->party().in_game())
    return ;
  if (!(   (game().status() >= Game::Status::play)
        && (   game().status() < Game::Status::finished
            || table().in_game_review() ) ) )
    return ;

  if (table().in_game_review()) {
    if (table().game_review_trick_visible())
      set_trick(table().game_review_trick());
    else
      remove_trick();
  } else {
    set_trick(TrickDrawing::ui->game().tricks().current());
  }

  if (!trick_set()) {
    trick_drawed_ = {};
    return;
  }

  auto const& trick = this->trick();

  // only show the trick if it is in the middle
  // (and not in the trickpile)
  if (    !trick.isfull()
      || trick.winnerplayer().trickpile().empty()
      || (trick.winnerplayer().trickpile().last_trick().no()
          < trick.no())
      || table().in_game_review()) {
    cr->push_group();
    geometry().transform(cr);
    set_center({0, 0});
    TrickDrawing::draw(cr);
    cr->pop_group_to_source();
    cr->paint();
  }

  trick_drawed_ = trick;
} // void Trick::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** @return   whether the mouse is over the trick
 **/
bool
Trick::mouse_over_cards() const
{
  if (!trick_set())
    return false;

  int x = 0, y = 0;
  table().get_pointer(x, y);
  auto const geometry = this->geometry();
  return TrickDrawing::mouse_over_cards(x - geometry.x,
                                        y - geometry.y);
} // bool Trick::mouse_over_cards() const

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
