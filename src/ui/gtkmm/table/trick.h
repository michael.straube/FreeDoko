/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "part.h"
#include "../trick_drawing.h"

#include "../../../card/trick.h"

namespace UI_GTKMM_NS {

/**
 ** the graphic representation of the current trick
 **/
class Trick : public Table::Part, public TrickDrawing {
public:
  explicit Trick(Table& table);
  Trick() = delete;
  Trick(Trick const&) = delete;
  Trick& operator=(Trick const&) = delete;

  ~Trick() override;

  bool changed() const override;
  auto geometry() const -> Geometry;
  auto outline()  const -> Outline         override;

  void draw(Cairo::RefPtr<::Cairo::Context> cr) override;
  bool mouse_over_cards() const;
private:
  ::Trick trick_drawed_;
}; // class Trick : public TrickDrawing 

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
