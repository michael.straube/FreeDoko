/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "../../party/status.h"
#include "../../game/status.h"
#include "../../card/card.h"

namespace UI_GTKMM_NS {

/**
 ** a thrower for exceptions
 **
 ** since gtkmm does not support exceptions, this class delays the throwing
 ** of exceptions
 **/
class Thrower {
public:
  enum class Type {
    none,
    party_status,
    game_status,
    card
  }; // enum Type

  union Exception {
    PartyStatus party_status;
    GameStatus game_status;
    Card const* card = nullptr;
  }; // union Exception

public:
  Thrower();

  auto exception()      const -> Exception;
  auto exception_type() const -> Type;

  auto depth() const -> unsigned;

  operator bool() const;
  auto will_throw() const -> bool;
  void throw_it();
  void clear();

  void inc_depth();
  void dec_depth();
  void set_depth(unsigned depth);

  void operator()(PartyStatus party_status,
                  string const& file,
                  unsigned line);
  void operator()(GameStatus game_status,
                  string const& file,
                  unsigned line);
  void operator()(Card card,
                  string const& file,
                  unsigned line);

private:
  Exception exception_;
  Type exception_type_ = Type::none;

  unsigned depth_ = 0;

  // the following variables are used for bug tracking (double throwing)
  // the file of the throw
  string file;
  // the line number of the throw
  unsigned line = 0;
}; // class Thrower

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
