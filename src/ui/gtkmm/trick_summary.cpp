/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "trick_summary.h"

#include "trick_drawing.h"
#include "ui.h"
#include "table.h"

#include "../../game/game.h"
#include "../../player/player.h"
#include "../../card/trick.h"

#include <gtkmm/drawingarea.h>
#include <gtkmm/label.h>
#include <gtkmm/treeview.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent               parent widget
 ** @param    show_trick_picture   whether to show the trick picture
 **/
TrickSummary::TrickSummary(Base* const parent,
                           bool const show_trick_picture) :
  Base{parent},
  show_trick_picture{show_trick_picture},
  trick_drawing{*this}
{
  signal_realize().connect(sigc::mem_fun(*this,
                                               &TrickSummary::init));
} // TrickSummary::TrickSummary(Base* parent, bool show_trick_picture = true)

/** destruktor
 **/
TrickSummary::~TrickSummary() = default;

/** create all subelements
 **/
void
TrickSummary::init()
{
  set_spacing(10);

  if (show_trick_picture) {
    if (trick)
      trick_drawing.set_trick(*trick);

    trick_picture = Gtk::manage(new Gtk::DrawingArea());
    trick_picture->set_size_request(trick_drawing.width()
                                          * 6 / 5,
                                          trick_drawing.height()
                                          * 6 / 5);
    trick_picture->signal_draw().connect(sigc::mem_fun(*this,
                                                             &TrickSummary::on_draw_trick));

    pack_start(*trick_picture, false, true);
  } // if (show_trick_picture)

  { // info box
    auto info_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
    info_box->set_homogeneous();
    info_box->set_border_width(2 EX);
    pack_end(*info_box);

    { // the labels
      auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
      vbox->set_border_width(2 EX);

      // gettext: %s = winner
      winner = Gtk::manage(new Gtk::Label(_("The trick goes to %s.",
                                                  trick->winnerplayer().name())));
      vbox->pack_start(*winner, false, true);

      points = Gtk::manage(new Gtk::Label(_("Points in the trick: %u",
                                                  trick->points())));

      vbox->pack_start(*points, false, true);

      info_box->pack_start(*vbox, false, true);
    } // the labels

    { // specialpoints
      specialpoints_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
      specialpoints_box->set_border_width(2 EX);

      specialpoints_list
        = Gtk::ListStore::create(specialpoints_model);
      specialpoints_treeview
        = Gtk::manage(new Gtk::TreeView(specialpoints_list));
      specialpoints_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

      specialpoints_treeview->append_column(_("specialpoint"),
                                            specialpoints_model.special_point);
      specialpoints_treeview->append_column(_("player"),
                                            specialpoints_model.player);

      specialpoints_box->pack_start(*specialpoints_treeview, false, true);

      info_box->pack_end(*specialpoints_box, false, true);
    } // specialpoints
  } // info box

  show_all();
  update();

  auto update_cards = [this](auto value) { force_update(); };
  ::preferences.signal_changed(::Preferences::Type::rotate_trick_cards).connect_back(update_cards, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::cardset).connect_back(update_cards, disconnector_);
} // void TrickSummary::init()

/** set the trick
 **
 ** @param    trick   the trick to show the info of
 **/
void
TrickSummary::set_trick(::Trick const& trick)
{
  this->trick = &trick;
  trick_drawing.set_trick(trick);
  update();
}

/** removes the trick
 **/
void
TrickSummary::remove_trick()
{
  trick = nullptr;
  trick_drawing.remove_trick();
} // void TrickSummary::remove_trick()

/** update (rewrite) all information
 **
 ** @todo   if there is no 'player_of' don't show the column
 **/
void
TrickSummary::update()
{
  if (!get_realized())
    return ;

  if (trick == nullptr)
    return ;

  auto const& trick = *this->trick;
  auto const& game = trick.game();

  // gettext: %s = winner
  winner->set_label(_("The trick goes to %s.",
                      trick.winnerplayer().name()));
  points->set_label(_("Points in the trick: %u",
                      trick.points()));

  update_trick_picture();

  { // special points
    specialpoints_list->clear();

    bool valid_point = false;
    for (auto sp : trick.specialpoints()) {
      // test whether to show the point
      if (sp.player_get_no == UINT_MAX)
#ifndef RELEASE
        // no 'player of' (can this happen?)
        DEBUG_ASSERTION(false,
                        "Gtkmm::TrickSummary(trick):\n"
                        "  no 'player_get_no' in specialpoint");
#else
      continue;
#endif
      else if (sp.player_of_no != UINT_MAX) {
        auto const& teaminfo = game.teaminfo();
        auto const team_get = teaminfo.get_human_teaminfo(game.player(sp.player_get_no));
        auto const team_of = teaminfo.get_human_teaminfo(game.player(sp.player_of_no));

        if (   is_real(team_get)
            && team_get == team_of)
          continue;
      } // if !(player_get_no == UINT_MAX) && (player_of_no != UINT_MAX)

      valid_point |= true;

      auto row = *(specialpoints_list->append());
      row[specialpoints_model.special_point]
        = _(sp.type);
      if (sp.player_of_no != UINT_MAX)
        row[specialpoints_model.player]
          = game.player(sp.player_of_no).name();
      else
        row[specialpoints_model.player] = "";
    } // for (sp : trick.specialpoints)

    if (valid_point) {
#ifdef WORKAROUND
      // Beim Neuerstellen der Sonderpunkte werden diese nicht gezeigt.
      // Vermutlich wird das Füllen von specialpoints_list nach dem clear() nicht richtig erkannt.
      // Daher wird hier der TreeView neu erzeugt.
      specialpoints_box->remove(*specialpoints_treeview);
      specialpoints_treeview
        = Gtk::manage(new Gtk::TreeView(specialpoints_list));
      specialpoints_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

      specialpoints_treeview->append_column(_("specialpoint"),
                                            specialpoints_model.special_point);
      specialpoints_treeview->append_column(_("player"),
                                            specialpoints_model.player);

      specialpoints_box->pack_start(*specialpoints_treeview, false, true);
#endif
      specialpoints_treeview->show_all();
    } else {
      specialpoints_treeview->hide();
    }
  } // special points
} // void TrickSummary::update()

/** update the trick in the drawing area
 **/
void
TrickSummary::update_trick_picture()
{
  if (   (trick == nullptr)
      || (trick_picture == nullptr))
    return ;

  trick_drawing.set_center({trick_picture->get_width() / 2,
                           trick_picture->get_height() / 2});
  trick_drawing.draw(trick_picture->get_window()->create_cairo_context());
} // void TrickSummary::update_trick_picture()

/** update the trick in the drawing area
 **
 ** @param    cr    cairo context
 **
 ** @return   true
 **/
bool
TrickSummary::on_draw_trick(Cairo::RefPtr<Cairo::Context> const& cr)
{
  trick_drawing.set_center({trick_picture->get_width() / 2,
                           trick_picture->get_height() / 2});
  trick_drawing.draw(cr);
  return true;
} // bool TrickSummary::on_draw_trick(Cairo::RefPtr<Cairo::Context> const& cr)

/** updates the cards
 **/
void
TrickSummary::force_update()
{
  if (!get_realized())
    return ;

  if (trick_picture) {
    trick_picture->set_size_request(trick_drawing.width()
                                    * 6 / 5,
                                    trick_drawing.height()
                                    * 6 / 5);
    update_trick_picture();
  }
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
