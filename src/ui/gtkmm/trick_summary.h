/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "base.h"
#include <gtkmm/box.h>
#include <gtkmm/liststore.h>
#include "trick_drawing.h"

class Trick;
class Player;

namespace Gtk {
class DrawingArea;
class Label;
class TreeView;
} // namespace Gtk

namespace UI_GTKMM_NS {
class TrickDrawing;

/**
 ** the summary of a trick
 **
 ** @todo   make the background as in 'table'
 ** @todo   name update
 **/
class TrickSummary : public Base, public Gtk::Box {

  /**
   ** the columns of the trickpoints table
   **/
  struct SpecialpointsModel : public Gtk::TreeModel::ColumnRecord {
      SpecialpointsModel()
      {
        this->add(this->special_point);
        this->add(this->player);
      }
      Gtk::TreeModelColumn<Glib::ustring> special_point;
      Gtk::TreeModelColumn<Glib::ustring> player;
  }; // struct SpecialpointsModel : public Gtk::TreeModel::ColumnRecord

  public:
  TrickSummary(Base* parent, bool show_trick_picture = true);
  TrickSummary(TrickSummary const&)            = delete;
  TrickSummary& operator=(TrickSummary const&) = delete;
  ~TrickSummary() override;

  // set the trick
  void set_trick(::Trick const& trick);
  // remove the trick
  void remove_trick();
  // update all
  void update();
  // update the trick picture
  void update_trick_picture();

  private:
  bool on_draw_trick(Cairo::RefPtr<Cairo::Context> const& cr);
  // update the cards
  void force_update();

  private:
  // initialize the window
  void init();

  private:
  AutoDisconnector disconnector_;
  ::Trick const* trick = nullptr;

  bool const show_trick_picture = true;
  TrickDrawing trick_drawing;
  Gtk::DrawingArea* trick_picture = nullptr;
  Gtk::Label* winner = nullptr;
  Gtk::Label* points = nullptr;
  Gtk::Box* specialpoints_box = nullptr;
  SpecialpointsModel specialpoints_model;
  Glib::RefPtr<Gtk::ListStore> specialpoints_list;
  Gtk::TreeView* specialpoints_treeview = nullptr;
}; // class TrickSummary : public Base, public Gtk::Box

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
