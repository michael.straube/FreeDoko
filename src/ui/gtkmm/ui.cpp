/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "ui.h"
#include "cards.h"
#include "icons.h"
#include "icons_window.h"
#include "first_run.h"
#include "program_updated.h"
#include "splash_screen.h"
#include "main_window.h"
#include "table.h"
#include "table/hand.h"
#include "game_finished.h"
#include "game_review.h"
#include "party_points.h"
#include "menu.h"
#include "party_summary.h"
#include "party_settings.h"
#include "players_db.h"
#include "game_debug.h"
#include "help.h"
#include "license.h"
#include "changelog.h"
#include "support.h"
#include "about.h"
#include "preferences.h"
#include "bug_report.h"
#include "bug_report_replay.h"
#include "error.h"

#include "../help.h"
#include "../status_message.h"
#include "../../basetypes/program_flow_exception.h"
#include "../../party/party.h"
#include "../../game/game.h"
#include "../../card/sorted_hand.h"
#include "../../card/trick.h"
#include "../../player/human/human.h"
#include "../../misc/preferences.h"
#include "../../game/gameplay.h"
#include "../../player/cards_information.h"
#include "../../player/team_information.h"


#include <gtkmm/main.h>
#include <gtkmm/icontheme.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/textview.h>
#include <gtkmm/imagemenuitem.h>
#include <gtkmm/checkmenuitem.h>
#include <gdk/gdkkeysyms.h>

namespace UI_GTKMM_NS {

bool UI_GTKMM::sleeping = false; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

/** constructor
 **/
UI_GTKMM::UI_GTKMM() :
  UI(UIType::gtkmm),
  Base(this, this)
{
#if defined(DKNOF) && !defined(RELEASE)
  {
    int argc = 2;
    auto  argv = new char*[2];
    argv[0] = new char[10];
    strncpy(argv[0], "FreeDoko", 10);
    argv[1] = new char[20];
    //strncpy(argv[1], "--g-fatal-warnings", 20);
    strncpy(argv[1], "", 20);
    kit = make_unique<Gtk::Main>(argc, argv);
    delete argv[1];
    delete argv[0];
    delete[] argv;
  }
#else
  kit = make_unique<Gtk::Main>();
#endif

  // count the number of objects
  base_objects_number = 31 + 1;
  base_objects_number += 5 * 4 + 2; // HTIN objects
#ifdef RELEASE
  base_objects_number += 2; // ?
#endif

#ifndef RELEASE
  base_objects_number -= 2; // ?
#endif

  DEBUG("init ui") << _("initiating the GUI (debug log)") << '\n';
  icon_theme = Gtk::IconTheme::get_default();

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  { // the logo
    try {
      logo = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::logo_file).string());
    } catch (Glib::FileError const& file_error) {
      ::ui->error(_("Error::loading logo (%s)",
                    (  ::preferences.path(::Preferences::Path::logo_file).empty()
                     ? ::preferences.value(::Preferences::Path::logo_file).string()
                     : ::preferences.path(::Preferences::Path::logo_file).string())
                   )
                  + "\n"
                  + _("searched paths:") + "\n"
                  + ::preferences.data_directories_string("  ")
                  + "\n"
                  + _("Error message: %s", file_error.what()));
    } catch (Gdk::PixbufError const& pixbuf_error) {
      ::ui->error(_("Error::loading logo (%s)",
                    (  ::preferences.path(::Preferences::Path::logo_file).empty()
                     ? ::preferences.value(::Preferences::Path::logo_file).string()
                     : ::preferences.path(::Preferences::Path::logo_file).string())
                   )
                  + "\n"
                  + _("searched paths:") + "\n"
                  + ::preferences.data_directories_string("  ")
                  + "\n"
                  + _("Error message: %s", pixbuf_error.what()));
    } // try
  } // the logo
  { // the icon
    try {
      icon = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::icon_file).string());
    } catch (Glib::FileError const& file_error) {
      ::ui->error(_("Error::loading icon (%s)",
                    (  ::preferences.path(::Preferences::Path::icon_file).empty()
                     ? ::preferences.value(::Preferences::Path::icon_file).string()
                     : ::preferences.path(::Preferences::Path::icon_file).string())
                   )
                  + "\n"
                  + _("searched paths:") + "\n"
                  + ::preferences.data_directories_string("  ")
                  + "\n"
                  + _("Error message: %s", file_error.what()));
    } catch (Gdk::PixbufError const& pixbuf_error) {
      ::ui->error(_("Error::loading icon (%s)",
                    (  ::preferences.path(::Preferences::Path::icon_file).empty()
                     ? ::preferences.value(::Preferences::Path::icon_file).string()
                     : ::preferences.path(::Preferences::Path::icon_file).string())
                   )
                  + "\n"
                  + _("searched paths:") + "\n"
                  + ::preferences.data_directories_string("  ")
                  + "\n"
                  + _("Error message: %s", pixbuf_error.what()));
    } // try
  } // the icon
#else
  std::auto_ptr<Glib::Error> error;
  logo = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::logo_file), error);
  if (error.get()) {
    cerr << "Error loading the logo: " << error.what() << '\n';
    ::ui->error(_("Error::loading logo (%s)",
                  ::preferences.path(::Preferences::Path::logo_file).string())
                + "\n"
                + _("searched paths:") + "\n"
                + ::preferences.data_directories_string("  "));
  }
  icon = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::icon_file), error);
  if (error.get()) {
    cerr << "Error loading the icon: " << error.what() << '\n';
    ::ui->error(_("Error::loading icon (%s)",
                  ::preferences.path(::Preferences::Path::icon_file).string())
                + "\n"
                + _("searched paths:") + "\n"
                + ::preferences.data_directories_string("  "));
  }
#endif
} // UI_GTKMM::UI_GTKMM()

/** destruktor
 **/
UI_GTKMM::~UI_GTKMM()
{
  if (Gtk::Main::level())
    Gtk::Main::quit();
} // UI_GTKMM::~UI_GTKMM()


/** Initialises the UI with the command line arguments.
 ** GTKMM removes the arguments it understands from the list.
 **
 ** @param    argc   number of arguments
 ** @param    argv   arguments
 **/
void
UI_GTKMM::init(int& argc, char**& argv)
{
  set_max_progress(3);

  // Diese Symbole werden für Windows für die SpinButtons benötigt.
  // Das Skript, das die benötigten Icons raussucht benötigt daher die folgenden Zeilen
  // set_image_from_icon_name("list-add")
  // set_image_from_icon_name("list-remove")

  // I have always to construct the window for 'Icons'
  // (I need one window and now I take the root window from 'splash_screen')
  Gtk::Window::set_auto_startup_notification(false);
  help = make_unique<Help>(this);
  splash_screen = make_unique<SplashScreen>(this);
  if (::preferences(::Preferences::Type::show_splash_screen)) {
    splash_screen->show();

    if (ui->first_run_window)
      ui->first_run_window->raise();
    if (ui->program_updated_window)
      ui->program_updated_window->raise();
  } // if (::preferences(::Preferences::Type::show_splash_screen))

  StatusMessages sm(*this);

  DEBUG("init ui") << _("loading the cards") << '\n';
  sm.add(_("loading the cards"));
  cards = make_unique<Cards>(this);
  DEBUG("init ui") << _("loading the icons") << '\n';
  sm.add(_("loading the icons"));
  icons = make_unique<Icons>(this);
  DEBUG("init ui") << _("initiating the GUI") << '\n';
  sm.add(_("initiating the GUI"));

  // there are some orders on initializing:
  //   bug_report < party_settings
  //   bug_report < bug_report_replay
  //   bug_report < main_window
  //   main_window < party_summary
  //   main_window < party_settings
  //   main_window < table
  DEBUG("init ui") << _("initiating the GUI (license)") << '\n';
  sm.add(_("initiating the GUI (license)"));
  license = make_unique<License>(this);
  DEBUG("init ui") << _("initiating the GUI (changelog)") << '\n';
  sm.add(_("initiating the GUI (changelog)"));
  changelog = make_unique<ChangeLog>(this);
  DEBUG("init ui") << _("initiating the GUI (support)") << '\n';
  sm.add(_("initiating the GUI (support)"));
  support = make_unique<Support>(this);
  DEBUG("init ui") << _("initiating the GUI (about)") << '\n';
  sm.add(_("initiating the GUI (about)"));
  about = make_unique<About>(this);

  DEBUG("init ui") << _("initiating the GUI (preferences)") << '\n';
  sm.add(_("initiating the GUI (preferences)"));
  preferences = make_unique<Preferences>(this);

  DEBUG("init ui") << _("initiating the GUI (bug report)") << '\n';
  sm.add(_("initiating the GUI (bug report)"));
  bug_report = make_unique<BugReport>(this);
  DEBUG("init ui") << _("initiating the GUI (bug report replay)") << '\n';
  sm.add(_("initiating the GUI (bug report replay)"));
  bug_report_replay = make_unique<BugReportReplay>(this);

  DEBUG("init ui") << _("initiating the GUI (main window)") << '\n';
  sm.add(_("initiating the GUI (main window)"));
  main_window = make_unique<MainWindow>(this);
  DEBUG("init ui") << _("initiating the GUI (table)") << '\n';
  sm.add(_("initiating the GUI (table)"));
  table = make_unique<Table>(this);

  DEBUG("init ui") << _("initiating the GUI (party summary)") << '\n';
  sm.add(_("initiating the GUI (party summary)"));
  party_summary = make_unique<PartySummary>(this);
  DEBUG("init ui") << _("initiating the GUI (party settings)") << '\n';
  sm.add(_("initiating the GUI (party settings)"));
  party_settings = make_unique<PartySettings>(this);

  DEBUG("init ui") << _("initiating the GUI (players db)") << '\n';
  sm.add(_("initiating the GUI (players db)"));
  players_db = make_unique<PlayersDB>(this);

  DEBUG("init ui") << _("initiating the GUI (game debug)") << '\n';
  sm.add(_("initiating the GUI (game debug)"));
  game_debug = make_unique<GameDebug>(this);

  DEBUG("init ui") << _("initiating the GUI (show all)") << '\n';
  sm.add(_("initiating the GUI (show all)"));
  main_window->menu->set_signals();

  { // set the size of the main window
    int const width_hint = static_cast<int>(7 * cards->height());
    int const height_hint = static_cast<int>(6 * cards->height());
    int const width = min(width_hint,
                          Gdk::Screen::get_default()->get_width() - 2 EM);
    int const height = min(height_hint,
                           Gdk::Screen::get_default()->get_height() - 2 EX);
    main_window->set_default_size(width, height);
    //main_window->set_default_size(width_hint - 1 EM, height_hint - 1 EX);
  } // set the size of the main window

  Gtk::Window::set_auto_startup_notification(true);

  main_window->show();
  splash_screen->hide();

  finish_progress();

  splash_screen.reset();

#ifdef WORKAROUND
#ifdef WINDOWS
  // Under MS-Windows there are problems with special characters (like 'ü')
  // When the language is changed, the characters are shown as they should,
  // I wonder, why.
  {
    auto const language = ::preferences(::Preferences::Type::language);
    ::preferences.set(::Preferences::Type::language, "en");
    ::preferences.set(::Preferences::Type::language, language);
  }
#endif
#endif

  ::party->rule().signal_changed.connect_back(*this, &UI_GTKMM::rule_changed, disconnector_);
  ::preferences.signal_changed().connect_back(*this, &UI_GTKMM::setting_changed, disconnector_);

  if (::debug("icons")) {
    static IconsWindow icons_window(this);
    icons_window.show();
  }

  DEBUG("init ui") << _("initialising the GUI: finished") << '\n';
} // UI_GTKMM::init(argc, argv)

/** -> result
 **
 ** @param    text   text to analys
 **
 ** @return   whether the text is an utf8 encoded text
 **/
bool
UI_GTKMM::is_utf8(string const& text)
{
  // see http://en.wikipedia.org/wiki/UTF-8
  for (size_t i = 0; i < text.size(); ++i) {
    char const& c = text[i];
    if ((c & 0x80) == 0) {
      // OK, ASCII character
    } else if ((c & 0xE0) == 0xC0 ) {
      if (i + 1 >= text.size())
        return false;
      if ((text[i + 1] & 0xC0) != 0x80)
        return false;
      i += 1;
    } else if ((c & 0xF0) == 0xE0 ) {
      if (i + 2 >= text.size())
        return false;
      if (   ((text[i + 1] & 0xC0) != 0x80)
          || ((text[i + 2] & 0xC0) != 0x80) )
        return false;
      i += 2;
    } else if ((c & 0xF8) == 0xF0 ) {
      if (i + 3 >= text.size())
        return false;
      if (   ((text[i + 1] & 0xC0) != 0x80)
          || ((text[i + 2] & 0xC0) != 0x80)
          || ((text[i + 3] & 0xC0) != 0x80) )
        return false;
      i += 3;
    } else {
      return false;
    } // if (c)
  } // for (i)

  return true;
} // static bool UI_GTKMM::is_utf8(string text)

/** -> result
 **
 ** @param    text   text to convert
 **
 ** @return   utf8 format of 'text'
 **/
Glib::ustring
UI_GTKMM::to_utf8(string const& text)
{
  // I do not know any function to identify the encoding of a string.
  // So we decide according to the german umlauts latin1-characters.

  // searches for german umlauts
  if (UI_GTKMM::is_utf8(text))
    return text;

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try {
    return Glib::locale_to_utf8(text);
  } catch (Glib::ConvertError const& error) {
    try {
      return Glib::convert(text, "utf8", "latin1");
    } catch (Glib::ConvertError const& error) {
    } // try
  } // try
#else
  std::auto_ptr<Glib::Error> error;
  auto ret = Glib::locale_to_utf8(text, error);
  if (!error.get())
    return ret;
  ret = Glib::convert(text, "utf8", "latin1", error);
  if (!error.get())
    return ret;
#endif

  return text;
} // static Glib::ustring UI_GTKMM::to_utf8(string text)


/** add a window to the list
 **
 ** @param    window   window to add
 **/
void
UI_GTKMM::add_window(Gtk::Window& window)
{
  windows.push_back(&window);
} // void UI_GTKMM::add_window(Gtk::Window& window)

/** updates the UI
 **/
void
UI_GTKMM::update()
{
  thrower.inc_depth();

  ::signal_update();

#ifdef CHECK_RUNTIME
  auto const ssp = ::runtime["ui update"].start_stop_proxy();
#endif
  while (   !thrower
         && Gtk::Main::events_pending()) {
    Gtk::Main::iteration(false);
  }

  thrower.dec_depth();
} // void UI_GTKMM::update()

/** sleeps the given time
 ** (in miliseconds, UINT_MAX for infinity)
 **
 ** @param    sleep_msec   time to sleep
 **
 ** @todo   remove variable 'sleeping'
 **/
void
UI_GTKMM::sleep(unsigned const sleep_msec)
{
  thrower.inc_depth();

  auto timeout_connection
    = Glib::signal_timeout().connect(&UI_GTKMM::stop_sleeping,
                                     //sigc::mem_fun(*(kit),
                                     //&Gtk::Main::quit),
                                     sleep_msec);

  UI_GTKMM::sleeping = true;

  ::signal_update();

  while (   !thrower
         && UI_GTKMM::sleeping) {
    Gtk::Main::iteration();
  }

  timeout_connection.disconnect();

  thrower.dec_depth();
} // void UI_GTKMM::sleep(unsigned sleep_msec)

/** quits the innermost main loop of gtk
 **/
void
UI_GTKMM::main_quit()
{
  if (Gtk::Main::level() > 0)
    Gtk::Main::quit();
} // void UI_GTKMM::main_quit()

/** quits the innermost main loop of gtk
 **
 ** @return   true
 **/
bool
UI_GTKMM::stop_sleeping()
{
  if (Gtk::Main::level() > 0)
    Gtk::Main::quit();

  UI_GTKMM::sleeping = false;

  return true;
} // static bool UI_GTKMM::stop_sleeping()

/** quits the program
 **/
void
UI_GTKMM::quit_program()
{
  thrower(Party::Status::quit, __FILE__, __LINE__);
  main_quit();
} // void UI_GTKMM::quit_program()

/** a key has been pressed
 **
 ** @param    key   the key
 **
 ** @return   true (stop other handlers from being invoked for the event)
 **
 ** @todo      some shortcuts (search 'ToDo')
 **/
bool
UI_GTKMM::key_press(GdkEventKey const* const key)
{
#if 0
  struct GdkEventKey {
    GdkEventType type;
    GdkWindow *window;
    gint8 send_event;
    guint32 time;
    guint state;
    guint keyval;
    gint length;
    gchar *string;
    guint16 hardware_keycode;
    guint8 group;
  };
#endif

  if (table
      && table->in_game_review())
    if (!table->game_finished_->game_review->key_press(key))
      return true;

  if (key->state == GDK_SHIFT_MASK) {
    switch (key->keyval) {
    case GDK_KEY_Escape: // quit the program
      quit_program();
      break;
    } // switch (key->keyval)
  } // if (key->state == GDK_SHIFT_MASK)

  if (party().in_game() && game().status() == Game::Status::play) {
    // play a card?
    auto& player = const_cast<Player&>(game().players().current_player());
    if (player.type() == Player::Type::human) {
      SortedHand hand = table->hand(player).sorted_hand();
      if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK))) {
        { // request a card to play
          unsigned requested_cardno = UINT_MAX;
          switch (key->keyval) {
          case GDK_KEY_1:
            requested_cardno = 0;
            break;
          case GDK_KEY_2:
            requested_cardno = 1;
            break;
          case GDK_KEY_3:
            requested_cardno = 2;
            break;
          case GDK_KEY_4:
            requested_cardno = 3;
            break;
          case GDK_KEY_5:
            requested_cardno = 4;
            break;
          case GDK_KEY_6:
            requested_cardno = 5;
            break;
          case GDK_KEY_7:
            requested_cardno = 6;
            break;
          case GDK_KEY_8:
            requested_cardno = 7;
            break;
          case GDK_KEY_9:
            requested_cardno = 8;
            break;
          case GDK_KEY_0:
          case GDK_KEY_a:
            requested_cardno = 9;
            break;
          case GDK_KEY_b:
            if (player.game().rule(Rule::Type::with_nines))
              requested_cardno = 10;
            break;
          case GDK_KEY_c:
            if (player.game().rule(Rule::Type::with_nines))
              requested_cardno = 11;
            break;
          } // switch (key->keyval)
          if (table->get_card()) {
            if (   (requested_cardno != UINT_MAX)
                && !hand.played(requested_cardno)
                && player.game().tricks().current().isvalid(hand.card_all(requested_cardno)) ) {
              hand.request_position_all(requested_cardno);
              thrower(hand.requested_card(), __FILE__, __LINE__);
            }
          } // if (table->card_get())
        } // request a card to play
      } // if ((key->state & ~GDK_SHIFT_MASK) == 0)

    } // if (player.type() == Player::Type::human)
  } // if (game().status() == Game::Status::play)

  if (!(key->state & GDK_CONTROL_MASK)) {
    switch (key->keyval) {
    case GDK_KEY_question: // "?": help
      cout << "key codes:\n"
        << " ?: help on key codes\n"
        << " 1-0abc: play card\n"
        << " n: next announcement\n"
        << " h: card suggestion\n"
        << " l: show last trick\n"
        << " .: toggle show valid cards\n"
        << " r: open rules window\n"
        << " p: open party points window (list page)\n"
        << " g: open party points window (graph page)\n"
        << " P: open players window\n"
        << " F1: open manual\n"
        << " F2: show preferences\n"
        << " Pause/Break: create bug report\n"
        << " C-c: exit pogram\n"
        << " S-Esc: exit program\n"
        << " C-f: toggle fullscreen\n"
        << " C-n: start new party\n"
        << " C-l: load party\n"
        << " C-s: save party\n"
        << " C-p: create a screenshot of the tabe\n"
        << " C-q: quit the program\n"
        << " C-r: redraw all\n"
        << " C-0: default card size\n"
        << " C-+: increase card size\n"
        << " C--: decrease card size\n"
        << " i: show game information\n"
        << " C-g: print gameplay\n"
        << " e: print human cards information\n"
        << " v: print cards information of the player at the bottom (short form)\n"
        << " V: print cards information of the player at the bottom (long form)\n"
        << " t: print team information of the player at the bottom\n"
        << " T: print all team information of the player at the bottom\n"
        ;
      break;
    case GDK_KEY_Clear: // close windows
      // @todo    all
      break;

    case GDK_KEY_l: // show last trick
      // -> menu.cpp
      if (main_window->menu->last_trick->is_sensitive())
        main_window->menu->last_trick->activate();
      break;

    case GDK_KEY_period: // toggle valid cards
      // -> menu.cpp
      break;

    case GDK_KEY_i: // show game information
      // -> menu.cpp
      if (main_window->menu->game_debug->is_sensitive())
        main_window->menu->game_debug->activate();
      break;

    case GDK_KEY_n: // next announcement
      if (   party().in_game()
          && game().status() >= Game::Status::play
          && game().status() < Game::Status::finished) {
        if (game().players().count_humans() == 1) {
          game().announcements().make_announcement(game().players().human().next_announcement(),
                                                         game().players().human()
                                                        );
        } // if (only one human)
      }
      break;
    case GDK_KEY_h: // card suggestion
      // -> menu.cpp
      if (main_window->menu->card_suggestion->is_sensitive())
        main_window->menu->card_suggestion->activate();
      break;
    case GDK_KEY_r: // show the rules
      // -> menu.cpp
      main_window->menu->rules->activate();
      break;
    case GDK_KEY_R: // print the rest query
      if (party().in_game())
        cout << game().rest_query() << '\n';
      break;
    case GDK_KEY_p: // show the party points
      // -> menu.cpp
      if (main_window->menu->party_points->is_sensitive()) {
        ui->table->party_points_->present();
        ui->table->party_points_->notebook->set_current_page(0);
      }
      break;
    case GDK_KEY_g: // show the party points graph
      if (main_window->menu->party_points->is_sensitive()) {
        ui->table->party_points_->present();
        ui->table->party_points_->notebook->set_current_page(1);
      }
      break;
    case GDK_KEY_P: // show the players
      // -> menu.cpp
      main_window->menu->players->activate();
      break;
    case GDK_KEY_H: // print all hands
      for (auto const& player : game().players()) {
        cout << player.name() << '\n' << player.hand().sorted() << '\n';
      }
      break;
    case GDK_KEY_e: // print human cards information
      if (   party().in_game()
          && game().status() >= Game::Status::poverty_shift
          && game().status() < Game::Status::finished
          && game().players().count_humans() == 1) {
        cout << '\n'
          << game().players().human().name() << ": "
          << game().players().human().cards_information() << '\n';
      }
      break;
    case GDK_KEY_v: // print the cards information of the player at the bottom
      if (   party().in_game()
          && game().status() >= Game::Status::poverty_shift
          && game().status() < Game::Status::finished) {
        auto ai = dynamic_cast<Ai const*>(&table->player(Position::south));
        if (ai)
          cout << '\n'
            << ai->name() << ": "
            << ai->cards_information() << '\n';
      }
      break;
    case GDK_KEY_V: // print the cards information of the player at the bottom
      if (   party().in_game()
          && game().status() >= Game::Status::poverty_shift
          && game().status() < Game::Status::finished) {
        auto ai = dynamic_cast<Ai const*>(&table->player(Position::south));
        if (ai) {
          cout << '\n'
            << ai->name() << ": ";
          ai->cards_information().write(cout);
        }
        cout << '\n';
      }
      break;
    case GDK_KEY_t: // print team information of the player at the bottom
      if (   party().in_game()
          && game().status() >= Game::Status::poverty_shift
          && game().status() < Game::Status::finished) {
        auto ai = dynamic_cast<Ai const*>(&table->player(Position::south));
        if (ai)
          cout << '\n'
            << ai->name() << ": "
            << ai->team_information() << '\n';
      }
      break;
    case GDK_KEY_T: // print all team information
      if (   party().in_game()
          && game().status() >= Game::Status::poverty_shift
          && game().status() < Game::Status::finished) {
        for (auto const& p : game().players()) {
          if (!dynamic_cast<Ai const*>(&p)) {
            cout << '\n'
              << p.name() << ": no ai\n";
            continue;
          }
          cout << '\n'
            << p.name() << ": "
            << dynamic_cast<Ai const&>(p).team_information() << '\n';
        } // for (p)
      }
      break;

    case GDK_KEY_F1: // show manual
      // -> menu.cpp
      main_window->menu->help->activate();
      break;
    case GDK_KEY_F2: // show preferences
      // -> menu.cpp
      main_window->menu->preferences->activate();
      break;
    case GDK_KEY_Pause: // create a bug report
    case GDK_KEY_Break: // create a bug report
      bug_report->create_report();
      break;
    case GDK_KEY_Escape:
      break;
    } // switch (key->keyval)
  } // if (key->state == 0)
  if (key->state == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_c: // exit the program
      thrower(Party::Status::quit, __FILE__, __LINE__);
      break;
    case GDK_KEY_f: // toggle fullscreen
      if (main_window->get_window()->get_state()
          & Gdk::WINDOW_STATE_FULLSCREEN)
        main_window->unfullscreen();
      else
        main_window->fullscreen();
      break;

    case GDK_KEY_g: // print the gameplay
      cout << game().gameplay();
      break;

    case GDK_KEY_n: // start new party
      // -> menu.cpp
      main_window->menu->new_party->activate();
      break;
    case GDK_KEY_s: // save party
      // -> menu.cpp
      main_window->menu->save_party->activate();
      break;
    case GDK_KEY_p: // create a screenshot of the table
      table->save_screenshot();
      break;
    case GDK_KEY_q: // quit the program
      // -> menu.cpp
      main_window->menu->quit->activate();
      break;
    case GDK_KEY_r: // redraw all
      redraw_all();
      break;
    case GDK_KEY_0: // default card size
      ::preferences.set(::Preferences::Type::original_cards_size, true);
      break;
    case GDK_KEY_plus: // increase card size
    case GDK_KEY_KP_Add:
      ::preferences.set(::Preferences::Type::cards_height,
                        ::preferences(::Preferences::Type::cards_height) + 1);
      ::preferences.set(::Preferences::Type::original_cards_size, false);
      break;
    case GDK_KEY_minus: // decrease card size
    case GDK_KEY_KP_Subtract:
      ::preferences.set(::Preferences::Type::cards_height,
                        ::preferences(::Preferences::Type::cards_height) - 1);
      ::preferences.set(::Preferences::Type::original_cards_size, false);
      break;
    } // switch (key->keyval)
  } // if (key->state == GDK_CONTROL_MASK)
  if (key->state == (GDK_CONTROL_MASK | GDK_SHIFT_MASK)) {
    switch (key->keyval) {
    case GDK_KEY_KP_Add: // Rotation, analog zu Sumatra PDf
      ::preferences.set(::Preferences::Type::own_hand_on_table_bottom, false);
      ::preferences.set(::Preferences::Type::table_rotation,
                        ::preferences(::Preferences::Type::table_rotation) == 0
                        ? 3
                        : ::preferences(::Preferences::Type::table_rotation) - 1);
      break;
    case GDK_KEY_KP_Subtract: // Rotation, analog zu Sumatra PDf
      ::preferences.set(::Preferences::Type::own_hand_on_table_bottom, false);
      ::preferences.set(::Preferences::Type::table_rotation,
                        ::preferences(::Preferences::Type::table_rotation) == 3
                        ? 0
                        : ::preferences(::Preferences::Type::table_rotation) + 1);
      break;
    case GDK_KEY_0: // Rotation zurücksetzen
      ::preferences.set(::Preferences::Type::own_hand_on_table_bottom, true);
      ::preferences.set(::Preferences::Type::table_rotation, 0);
      break;
    } // switch (key->keyval)
  }


  return true;
} // bool UI_GTKMM::key_press(GdkEventKey* key)

/** @param    page   the page to be loaded
 **
 ** @todo   all
 **/
void
UI_GTKMM::help_load(string const& page)
{
} // void UI_GTKMM::help_load(string page)

/** this is the first run of the program
 **
 ** @param    message      message for the user
 **/
void
UI_GTKMM::first_run(string const& message)
{
  DEBUG_ASSERTION(!first_run_window,
                  "UI_GTKMM::first_run(message)\n"
                  "  the window is already created.");

  first_run_window = make_unique<FirstRun>(this, message);

  first_run_window->show();
} // void UI_GTKMM::first_run(string message)

/** the program is updated
 **
 ** @param    old_version   the old version
 **/
void
UI_GTKMM::program_updated(Version const& old_version)
{
  DEBUG_ASSERTION(!program_updated_window,
                  "UI_GTKMM::program_updated(old_version)\n"
                  "  the window is already created.");

  program_updated_window = make_unique<ProgramUpdated>(this, old_version);

  program_updated_window->show();
} // void UI_GTKMM::program_updated(Version old_version)

/** show the message in an information window
 ** Does not shows the message, if it has already been shown and is no error message and is not forced to be shown.
 **
 ** @param    message      message to display
 ** @param    type      the type of the message
 ** @param    force_show     whether to show the message, although it was already shown (default: false)
 **
 ** @todo   remove memory leak
 ** @todo   add a parameter for a title
 ** @todo   put the windows on top of all windows
 **
 ** @bug   memory leak (the dialog is never deleted)
 **/
void
UI_GTKMM::information(string const& message,
                      InformationType const type,
                      bool const force_show)
{
  // check, whether the message was already shown
  if (   !add_to_messages_shown(message, type)
      && (type != InformationType::problem)
      && !force_show)
    return ;

  auto message_type = Gtk::MESSAGE_INFO;
  switch (type) {
  case InformationType::normal:
    message_type = Gtk::MESSAGE_INFO;
    break;
  case InformationType::warning:
    message_type = Gtk::MESSAGE_WARNING;
    break;
  case InformationType::problem:
    message_type = Gtk::MESSAGE_ERROR;
    break;
  } // switch(type)

  // memory leak: will not be destructed
  auto information
    = new Gtk::MessageDialog(message,
                             false,
                             message_type,
                             Gtk::BUTTONS_NONE,
                             false);
  add_close_button(*this, *information);

  if (   type == InformationType::problem
      || party().status() == Party::Status::programstart) {
    if (program_updated_window)
      information->set_transient_for(*program_updated_window);
    else if (main_window)
      information->set_transient_for(*main_window);
    information->set_modal();
    Gtk::Main::run(*information);
    delete information;
  } else {
    information->present();
  }
} // void UI_GTKMM::information(string message, InformationType type, bool force_show = false)

/** shown an error window
 ** and quit the program
 **
 ** @param    message      message to display
 **/
void
UI_GTKMM::error(string const& message, string const& backtrace)
{
  static bool already_running = false;

  if (already_running) {
    cerr << '\n'
      << _("Press <Return> to exit.") << endl;
    std::cin.get();
  } else { // if !(already_running)

    already_running = true;

    if (splash_screen)
      splash_screen->hide();

    Error error(message, backtrace, this);
#ifdef RELEASE
    error.run();
#else
    error.present();
    while (error.is_visible()) {
      sleep(1000);
      thrower.clear();
    }
#endif
  } // if !(already_running)

#ifdef ASSERTION_GENERATES_SEGFAULT
  cerr << _("Creating segmentation fault.") << endl;
  SEGFAULT;
#endif // #ifdef ASSERTION_GENERATES_SEGFAULT

  std::exit(EXIT_FAILURE);
}

/** shown an error window
 ** and quit the program
 **
 ** @param    message      message to display
 **/
void
UI_GTKMM::error(string const& message)
{
  error(message, "");
}

/** generate an error (for testings)
 **
 ** @param    error_message   error message
 ** @param    backtrace       backtrace
 **/
void
UI_GTKMM::generate_error(string const error_message)
{
  ::generate_error(error_message);
}

/** iconifies all windows
 **/
void
UI_GTKMM::iconify_all_windows()
{
  for (auto& w : windows)
    w->iconify();
} // void UI_GTKMM::conify_all_windows()

/** deiconifies all windows
 **/
void
UI_GTKMM::deiconify_all_windows()
{
  for (auto& w : windows)
    w->deiconify();
} // void UI_GTKMM::deiconify_all_windows()


auto UI_GTKMM::create_theme_icon_image(string const& icon_name, size_t size) -> Gtk::Image*
{
#ifdef WINDOWS
  // Das Suchen im über IconTheme funktioniert unter Windows nicht.
#ifdef RELEASE
  string basepath = "share/icons/oxygen/base";
#else
  string basepath = "../setup/dll/gtkmm-3.dll/share/icons/oxygen/base";
#endif
  basepath += "/" + std::to_string(size) + "x" + std::to_string(size);
  for (auto dir : {"actions", "apps", "categories"}) {
    auto const file = basepath + "/" + dir + "/" + icon_name + ".png";
    if (std::filesystem::is_regular_file(file)) {
      return Gtk::manage(new Gtk::Image(file));
    }
  }
  return nullptr;
#endif

  if (!icon_theme->has_icon(icon_name)) {
    return nullptr;
  }
  return Gtk::manage(new Gtk::Image(icon_theme->load_icon(icon_name, size)));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
