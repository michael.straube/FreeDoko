/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "ui.h"

#include "main_window.h"
#include "menu.h"
#include "party_summary.h"
#include "party_settings.h"
#include "players_db.h"
#include "players.h"
#include "rules.h"
#include "table.h"
#include "party_points.h"

#include "../../party/party.h"

#include <gtkmm/main.h>

namespace UI_GTKMM_NS {

/** start a new party
 **/
void
  UI_GTKMM::start_new_party()
  {
    thrower(Party::Status::init, __FILE__, __LINE__);
    main_quit();
  }

/** end a party
 **/
void
  UI_GTKMM::end_party()
  {
    thrower(Party::Status::finished, __FILE__, __LINE__);
    main_quit();
  }

/** gets and sets the settings of the party:
 **   names, types and configuration of the players,
 **   rules,
 **   starting seed
 **   startplayer
 **/
void
  UI_GTKMM::party_get_settings()
  {
    party_summary->hide();
    party_settings->get();
  }

/** the party is loaded
 **/
void
  UI_GTKMM::party_loaded()
  {
    party_settings->hide();
    party_summary->party_loaded();
  }


} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
