/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM


#include "ui.h"

#include <gtkmm/togglebutton.h>
#include <gtkmm/dialog.h>
namespace UI_GTKMM_NS {

/** -> result
 **
 ** @param    color   the color
 **
 ** @return   the name of the color
 **/
string
  colorname(Gdk::RGBA const color)
  {
    ostringstream color_str;
    color_str << '#' << std::setfill('0')
      << std::hex << setw(4) << color.get_red_u()
      << std::hex << setw(4) << color.get_green_u()
      << std::hex << setw(4) << color.get_blue_u()
      << std::ends;

    return color_str.str();
  } // string colorname(Gdk::Colo color)

/** toggles whether 'widget' is to be shown, depending on the state of
 ** 'toggle_button'
 **
 ** @param    widget      widget to toggle the view of
 ** @param    toggle_button   button with the state
 **/
void
  toggle_view(Gtk::Widget* const widget,
              Gtk::ToggleButton* const toggle_button)
  {
    if (toggle_button->get_active()
        && toggle_button->is_visible())
      widget->show();
    else
      widget->hide();
  } // void toggle_view(Gtk::Widget* const widget, Gtk::ToggleButton* const toggle_button)

/** sets the signal so that 'widget' is to be shown, depending on the state of
 ** 'toggle_button'
 **
 ** @param    widget      widget to toggle the view of
 ** @param    toggle_button   button with the state
 **/
void
  set_signal_toggle_view(Gtk::Widget* const widget,
                         Gtk::ToggleButton* const toggle_button)
  {
    toggle_button->signal_toggled().connect(sigc::bind<Gtk::Widget* const, Gtk::ToggleButton* const>(sigc::ptr_fun(&toggle_view), widget, toggle_button));

    toggle_button->signal_realize().connect(sigc::bind<Gtk::Widget* const, Gtk::ToggleButton* const>(sigc::ptr_fun(&toggle_view), widget, toggle_button));

    toggle_button->signal_show().connect(sigc::bind<Gtk::Widget* const, Gtk::ToggleButton* const>(sigc::ptr_fun(&toggle_view), widget, toggle_button));
    toggle_button->signal_hide().connect(sigc::bind<Gtk::Widget* const, Gtk::ToggleButton* const>(sigc::ptr_fun(&toggle_view), widget, toggle_button));

    toggle_button->signal_unmap().connect(sigc::bind<Gtk::Widget* const, Gtk::ToggleButton* const>(sigc::ptr_fun(&toggle_view), widget, toggle_button));
  } // void set_signal_toggle_view(Gtk::Widget* const widget, Gtk::ToggleButton* const toggle_button)

/** create a close button and pack it into the action area
 **
 ** @param    dialog   dialog to add the close button
 **
 ** @return   button
 **/
Gtk::Button*
  add_close_button(UI_GTKMM& ui, Gtk::Dialog& dialog)
  {
    auto close_button = Gtk::manage(new Gtk::Button(_("Button::close")));
    close_button->set_image_from_icon_name("window-close");
    close_button->set_always_show_image();
    dialog.add_action_widget(*close_button, Gtk::RESPONSE_CLOSE);
    close_button->signal_clicked().connect(sigc::mem_fun(dialog, &Gtk::Dialog::close));

    close_button->set_can_default();
    close_button->grab_default();
    close_button->grab_focus();
    close_button->show();

    return close_button;
  } // Gtk::Button* add_close_button(Gtk::Dialog& dialog)
} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
