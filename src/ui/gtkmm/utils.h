/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once


#include <gdkmm/pixbuf.h>
#include <gdkmm/rgba.h>
namespace Gtk {
  class Widget;
  class ToggleButton;
  class Dialog;
  class Button;
} // namespace Gtk
namespace Gdk {
using Bitmap = Pixbuf;
using Pixmap = Pixbuf;
} // namespace Gdk
namespace UI_GTKMM_NS {
class UI_GTKMM;

// returns the name of the color
string colorname(Gdk::RGBA color);

// toggle the view of 'widget' depending on the state of 'toggle_button'
void toggle_view(Gtk::Widget* widget,
                 Gtk::ToggleButton* toggle_button);
// sets the signal for 'toggle view'
void set_signal_toggle_view(Gtk::Widget* widget,
                            Gtk::ToggleButton* toggle_button);

// create a close button and pack it into the action area
template<typename T>
  Gtk::Button* add_close_button(T& dialog)
  { return add_close_button(*dialog.ui, dialog); }
Gtk::Button* add_close_button(UI_GTKMM& ui, Gtk::Dialog& dialog);
} // namespace UI_GTKMM_NS
#endif // #ifdef USE_UI_GTKMM
