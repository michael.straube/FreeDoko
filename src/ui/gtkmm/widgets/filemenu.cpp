/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "filemenu.h"

#include <gtkmm/main.h>
#include <gtkmm/separatormenuitem.h>
#include <gtkmm/image.h>

namespace Gtk {

/** constructor
 **
 ** @param    signal_slot   the slot for the change signal
 **/
FileMenu::FileMenu(sigc::slot1<void, string> signal_slot) : // NOLINT
  signal_slot_(signal_slot)
{
  realize();
  signal_show().connect(sigc::mem_fun(*this, &FileMenu::update));
}

/** constructor
 **
 ** @param    signal_slot   the slot for the change signal
 ** @param    directory   the directory to search from
 **/
FileMenu::FileMenu(sigc::slot1<void, string> signal_slot, // NOLINT
                   Directory const directory) :
  directories_(1, directory),
  signal_slot_(signal_slot)
{
  realize();
  signal_show().connect(sigc::mem_fun(*this, &FileMenu::update));
}

/** constructor
 **
 ** @param    signal_slot   the slot for the change signal
 ** @param    directories   the directories to search from
 **/
FileMenu::FileMenu(sigc::slot1<void, string> signal_slot, // NOLINT
                   vector<Directory> const directories) :
  directories_(directories),
  signal_slot_(signal_slot)
{
  realize();
  signal_show().connect(sigc::mem_fun(*this, &FileMenu::update));
}

/** destruktor
 **/
FileMenu::~FileMenu() = default;

/** sets the directory
 **
 ** @param    directory   new directory
 **/
void
FileMenu::set_directory(Directory const directory)
{
  directories_.clear();
  add_directory(directory);
} // void FileMenu::set_directory(string const& directory)

/** add the directory to the list
 **
 ** @param    directory   new directory
 **/
void
FileMenu::add_directory(Directory const directory)
{
  directories_.push_back(directory);
  //update();
}

/** sets the directories
 **
 ** @param    directories   new directories
 **/
void
FileMenu::set_directories(vector<Directory> const directories)
{
  directories_ = directories;
}

/** sets the signal slot
 **
 ** @param    signal_slot   the signal slot
 **/
void
FileMenu::set_signal_slot(sigc::slot1<void, string> signal_slot)
{
  signal_slot_ = signal_slot;
}

/** shows the menu
 **/
void
FileMenu::show()
{
  popup(0, 0);
  update();
}

/** updates the menu (recreates it)
 **
 ** @todo   transform the filename into utf8
 **/
void
FileMenu::update()
{
  { // remove all childs
    for (auto& child : get_children())
      remove(*child);
  } // remove all childs

  show_all();

  auto const subdirname_p
    = static_cast<string*>(get_data("subdirname"));
  string const subdirname = subdirname_p ? *subdirname_p : ""s;

  bool first_directory = true;
  for (auto const& directory : directories_) {
    if (!is_directory(directory))
      continue;

    if (!first_directory)
      add(*Gtk::manage(new Gtk::SeparatorMenuItem()));
    else
      first_directory = false;

    for (auto const& entry : std::filesystem::recursive_directory_iterator(directory)) {
      if (!accept(entry))
        continue;
      auto item = Gtk::manage(new Gtk::MenuItem(entry.path().string()));
      dynamic_cast<Gtk::Label*>(item->get_child())->set_text(item_label(subdirname,
                                                                              relative(entry.path(), directory).string()));
      item->signal_activate().connect(sigc::bind<string>(signal_slot_,
                                                         relative(entry.path(), directory).string()));

      add(*item);
    }
  }

  if (get_children().empty()) {
    // There is no child, so add a stop image.
    auto image = Gtk::manage(new Gtk::Image());
    image->set_from_icon_name("process-stop", Gtk::ICON_SIZE_MENU);
    auto stop = Gtk::manage(new Gtk::MenuItem(*image));
    stop->set_sensitive(false);
    add(*stop);
  } // if (get_children().size() == 1)

  show_all();

  Gtk::Main::iteration(false);
}

/** -> result
 **
 ** @param    directory   the directory
 **
 ** @return   a new file menu with the same settings
 **
 ** @todo     use smart pointer
 **/
FileMenu*
FileMenu::create_new(Directory const directory) const
{
  return (new FileMenu(signal_slot_, directory));
}

/** -> result
 **
 ** @param    filename   filename to test
 **
 ** @return   whether the filename is accepted for the menu
 **/
bool
FileMenu::accept(Path const filename) const
{
  return is_regular_file(filename);
}

/** -> result
 **
 ** @param    subdirname   the name of the subdir
 ** @param    filename   the filename
 **
 ** @return   label of the menu item
 **/
string
FileMenu::item_label(string const& subdirname, string const& filename) const
{
  return filename;
} // virtual string FileMenu::item_label(string subdirname, string filename) const

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
