/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include <gtkmm/menu.h>

namespace Gtk {
/**
 ** a menu with files till depth 'depth',
 ** the directories are separated
 **/
class FileMenu : public Menu {
  public:
    explicit FileMenu(sigc::slot1<void, string> signal_slot);
    FileMenu(sigc::slot1<void, string> signal_slot,
             Directory directory);
    FileMenu(sigc::slot1<void, string> signal_slot,
             vector<Directory> directories);
    FileMenu() = delete;
    FileMenu(FileMenu const&) = delete;
    FileMenu& operator=(FileMenu const&) = delete;
    ~FileMenu() override;

    // create a new file menu
    virtual FileMenu* create_new(Directory directory) const;	

    // set the directory
    void set_directory(Directory directory);
    // add a directory
    void add_directory(Directory directory);
    // set multible directory
    void set_directories(vector<Directory> directories);
    // set the signal slot
    void set_signal_slot(sigc::slot1<void, string> signal_slot);

    // show the menu
    void show();

    // update the menu
    void update();

    // whether a filename is accepted
    virtual bool accept(Path filename) const;
    // the label for the item
    virtual string item_label(string const& subdirname,
                              string const& filename) const;

  private:
    vector<Directory> directories_;
  protected:
    sigc::slot1<void, string> signal_slot_;
}; // class FileMenu : public Menu

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
