/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "filemenu_extension.h"

namespace Gtk {

/** constructor
 **
 ** @param    signal_slot    signal slot
 ** @param    extension      extension to filter for
 **/
FileMenuFilterExtension::FileMenuFilterExtension(sigc::slot1<void, string> signal_slot,
                                                 string extension) :
  FileMenu(signal_slot),
  extension(std::move(extension))
{  }

/** destructor
 **/
FileMenuFilterExtension::~FileMenuFilterExtension() = default;

/** create a new file menu
 **
 ** @param    directory    new directory
 **
 ** @return   new file menu
 **/
FileMenu*
FileMenuFilterExtension::create_new(Directory const directory) const
{
  auto file_menu = new FileMenuFilterExtension(signal_slot_,
                                               extension);
  file_menu->set_directory(directory);
  return file_menu;
}

/** -> result
 **
 ** @param    filename   filename to test for
 **
 ** @return   whether the filename is accepted
 **/
bool
FileMenuFilterExtension::accept(Path const filename) const
{
  return (   is_regular_file(filename)
          && filename.extension() == "." + extension);
}

/** -> result
 **
 ** @param    subdir     subdirectory
 ** @param    filename   filename to test for
 **
 ** @return   text for the label
 **/
string
FileMenuFilterExtension::item_label(string const& subdir,
                                    string const& filename) const
{
  return (string(filename,
                 0, filename.size() - 1 - extension.size()));
}

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
