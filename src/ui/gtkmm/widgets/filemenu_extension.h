/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "filemenu.h"

namespace Gtk {
/**
 ** a file menu, only with files with extension 'extension'
 **/
class FileMenuFilterExtension : public FileMenu {
  public:
    FileMenuFilterExtension(sigc::slot1<void, string> signal_slot,
                            string extension);
    FileMenuFilterExtension() = delete;
    FileMenuFilterExtension(FileMenuFilterExtension const&) = delete;
    FileMenuFilterExtension& operator=(FileMenuFilterExtension const&) = delete;
    ~FileMenuFilterExtension() override;

    // create a new file menu
    FileMenu* create_new(Directory directory) const override;

    // whether the filename is accepted
    bool accept(Path filename) const override;

    // the label for the item
    string item_label(string const& subdir,
                      string const& filename) const override;

  private:
    string extension;
}; // class FileMenuFilterExtension : public FileMenu

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
