/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "label_card_selector.h"

namespace Gtk {

/** constructor
 **/
LabelCardSelector::LabelCardSelector() = default;

/** constructor
 **
 ** @param    label    text for the label
 **/
LabelCardSelector::LabelCardSelector(Glib::ustring const& label) :
  LabelTypeSelector<Card>(label)
{  }

/** destructor
 **/
LabelCardSelector::~LabelCardSelector() = default;

/** @return   the card
 **/
Card
LabelCardSelector::card()
{
  return type();
}

/** set the card
 **
 ** @param    card   new card
 **/
void
LabelCardSelector::set_card(Card const card)
{
  set_type(card);
}

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
