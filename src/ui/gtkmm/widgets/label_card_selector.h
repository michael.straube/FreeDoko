/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "label_type_selector.h"

#include "../../../card/card.h"

namespace Gtk {
/**
 ** a selector for cards with a label
 **/
class LabelCardSelector : public LabelTypeSelector<Card> {
  public:
    LabelCardSelector();
    explicit LabelCardSelector(Glib::ustring const& label);
    LabelCardSelector(LabelCardSelector const&) = delete;
    LabelCardSelector& operator=(LabelCardSelector const&) = delete;
    ~LabelCardSelector() override;

    // returns the selected type
    Card card();
    // set the selected type
    void set_card(Card card);
}; // class LabelCardSelector : public LabelTypeSelector<Card>

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
