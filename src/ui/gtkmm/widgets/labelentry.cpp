/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "labelentry.h"

namespace Gtk {

/** constructor
 **
 ** @param    label   text for the label
 **/
LabelEntry::LabelEntry(Glib::ustring const& text) :
  label_(Gtk::manage(new Gtk::Label(text))),
  entry_(Gtk::manage(new Gtk::Entry()))
{
  set_spacing(1 EX);

  label_->set_halign(Gtk::ALIGN_CENTER);
  pack_start(*label_, true, true);
  pack_end(*entry_, false, true);
} // LabelEntry::LabelEntry(Glib::ustring text)

/** destructor
 **/
LabelEntry::~LabelEntry() = default;

/** @return   the label
 **/
Label*
LabelEntry::get_label()
{
  return label_;
}

/** set the text for the label
 **
 ** @param    text   text for the label
 **
 ** @return   the label
 **/
void
LabelEntry::set_label(Glib::ustring const& text)
{
  label_->set_text(text);
}

/** @return   the entry
 **/
Entry*
LabelEntry::get_entry()
{
  return entry_;
}

/** @return   the text of the entry
 **/
Glib::ustring
LabelEntry::get_text()
{
  return entry_->get_text();
}

/** set the text for the entry
 **
 ** @param    text   text for the entry
 **/
void
LabelEntry::set_text(Glib::ustring const& text)
{
  entry_->set_text(text);
}

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
