/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>

namespace Gtk {
/**
 ** a spin button with a label
 **/
class LabelEntry : public Box {
  public:
    LabelEntry() = delete;
    LabelEntry(Glib::ustring const& text);
    LabelEntry(LabelEntry const&) = delete;
    LabelEntry& operator=(LabelEntry const&) = delete;

    ~LabelEntry() override;

    Label* get_label();
    void set_label(Glib::ustring const& text);

    Entry* get_entry();
    Glib::ustring get_text();
    virtual void set_text(Glib::ustring const& text);

  private:
    Label* label_;
    Entry* entry_;
}; // class LabelEntry : public Box

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
