/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/spinbutton.h>

namespace Gtk {
/**
 ** a spin button with a label
 **/
class LabelSpinButton : public Box {
  public:
    LabelSpinButton();
    explicit LabelSpinButton(Glib::ustring const& label);
    LabelSpinButton(Glib::ustring const& label, Glib::ustring const& label_right);
    LabelSpinButton(LabelSpinButton const&) = delete;
    LabelSpinButton& operator=(LabelSpinButton const&) = delete;

    ~LabelSpinButton() override;

    // Label functions
    Label* get_label();
    void set_label(Glib::ustring const& str);

    // SpinButton functions
    SpinButton* get_spin_button();
    virtual void set_range(double min, double max);
    void set_increments(double step, double page);
    double get_value();
    int get_value_as_int();
    virtual void set_value(double value);
    Glib::SignalProxy0<void> signal_value_changed();

  protected:
    Label* label_            = nullptr;
    SpinButton* spin_button_ = nullptr;
    Label* label_right_      = nullptr;
};

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
