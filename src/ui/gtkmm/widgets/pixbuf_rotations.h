/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "../../../basetypes/rotation.h"
#include <gdkmm/pixbuf.h>

namespace Gdk {
/**
 ** a pixmap with rotations (up, down, left, right)
 ** the rotations are created when needed
 **/
class PixbufRotations : public Glib::RefPtr<Pixbuf> {
  public:
    PixbufRotations(string const& filename);
    PixbufRotations(Glib::RefPtr<Pixbuf> pixbuf);
    PixbufRotations() = delete;
    PixbufRotations(PixbufRotations const&) = delete;
    PixbufRotations& operator=(PixbufRotations const&) = delete;
    virtual ~PixbufRotations();

    // pixbuf according to the rotation
    Glib::RefPtr<Pixbuf> operator[](Rotation rotation);

    // width of the pixbuf
    int width(Rotation rotation = Rotation::up) const;
    // height of the pixbuf
    int height(Rotation rotation = Rotation::up) const;

  private:
    Glib::RefPtr<Pixbuf> create_rotation(Rotation rotation);
  private:
    vector<Glib::RefPtr<Pixbuf> > pixbuf;
}; // class PixbufRotations : public Glib::RefPtr<Pixbuf>

} // namespace Gdk

#endif // #ifdef USE_UI_GTKMM
