/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#ifdef USE_UI_GTKMM

#include "constants.h"

#include "type_selector.h"

namespace Gtk {

/**
 ** adds the type to the selector
 **
 ** @param	type   type to be added
 **/
template<typename Type>
void
TypeSelector<Type>::add(Type const& type)
{
  this->append(_(type));
  this->types.push_back(type);
}

/**
 ** @return	the selected type
 **/
template<typename Type>
Type
TypeSelector<Type>::type()
{
  if (this->get_active_row_number() == -1)
    return {};
  return this->types[this->get_active_row_number()];
}

/**
 ** sets the selected type
 **
 ** @param	type	new selected type
 **/
template<typename Type>
void
TypeSelector<Type>::set_type(Type const& type)
{
  for (size_t i = 0; i < this->types.size(); ++i) {
    if (this->types[i] == type) {
      this->set_active(i);
      return ;
    }
  }

  DEBUG_ASSERTION(false,
		  "TypeSelector::set_type(" << type << ")\n"
		  << "  type not found");
  this->set_active(-1);
}

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
