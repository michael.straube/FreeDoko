/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../party/rule.h"
#include "../misc/preferences.h"

// functions for the help
namespace Help {

  void show(string const& location);

  // the homepage location
  string homepage_location();
  // the location of cardsets to download
  string cardsets_download_location();

  // the location of the helpfile in the manual

  string manual_location(string const& page = "overview.html");
  string operation_location(string const& page);


  // the location of the specific help

  string location(Rule::Type::Bool type);
  string location(Rule::Type::Unsigned type);

  string location(Preferences::Type::Bool type);
  string location(Preferences::Type::Unsigned type);
  string location(Preferences::Type::String type);
  string location(Preferences::Type::CardsOrder type);

}; // namespace Help
