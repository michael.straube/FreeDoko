/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "status_message.h"

#include "ui.h"

StatusMessage::StatusMessage(UI& ui,
                             string const translation) :
  ui(&ui), t(translation)
{ this->ui->add_status_message(t); }

StatusMessage::~StatusMessage()
{ ui->remove_status_message(t); }

StatusMessages::StatusMessages(UI& ui) :
  ui(&ui)
{ }

void StatusMessages::add(string const& translation)
{
  push(make_unique<StatusMessage>(*ui, translation));
}
