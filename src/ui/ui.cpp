/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "ui.h"

#include <unistd.h>

#include "ui.dummy.h"
#ifdef USE_UI_GTKMM
#include "gtkmm/ui.h"
#endif // #ifdef USE_UI_GTKMM

#include "../basetypes/fast_play.h"

#include "../party/party.h"
#include "../card/hand.h"
#include "../player/player.h"
#include "../misc/preferences.h"
#include "../misc/bug_report.h"
#include "../os/bug_report_replay.h"

UI::UI(UIType const type) :
  type_(type)
  // (to be called from a child class)
{ }

auto UI::create(UIType const type) -> unique_ptr<UI>
{
  // It should just call the new-operator of the child class.
  switch (type) {
  case UIType::dummy:
    return make_unique<UI_Dummy>();
#ifdef USE_UI_GTKMM
  case UIType::gtkmm:
    return make_unique<UI_GTKMM_NS::UI_GTKMM>();
#endif // #ifdef USE_UI_GTKMM
  default:
    return {};
  } // switch(type)

  return {};
} // static unique_ptr<UI> UI::create(UIType const type)

auto UI::type() const -> UIType
{
  return type_;
}

auto UI::party() -> Party&
{
#ifdef WORKAROUND
  // Bei der Initialisierung ist party_ noch nicht gesetzt, daher wird auf das globale Objekt zugegriffen
  return *::party;
#else
  DEBUG_ASSERTION(party_,
                  "UI::party()\n"
                  "  party is not set");
  return *party_;
#endif
}

auto UI::party() const -> Party const&
{
#ifdef WORKAROUND
  // Bei der Initialisierung ist party_ noch nicht gesetzt, daher wird auf das globale Objekt zugegriffen
  return *::party;
#else
  DEBUG_ASSERTION(party_,
                  "UI::party()\n"
                  "  party is not set");
  return *party_;
#endif
}

auto UI::game() -> Game&
{
  DEBUG_ASSERTION(game_,
                  "UI::game()\n"
                  "  game is not set");
  return *game_;
}

auto UI::game() const -> Game const&
{
  DEBUG_ASSERTION(game_,
                  "UI::game()\n"
                  "  game is not set");
  return *game_;
}

void UI::sleep(unsigned const msec)
{
  sleep_msec(msec);
  update();
}

void UI::wait()
{
  if (!(::fast_play & FastPlay::pause))
    sleep(20);
  else
    sleep(1);
}

void UI::party_open(Party& party)
{
#if 0
#ifdef WITH_UI_INTERFACE
  DEBUG_ASSERTION(!party_,
                  "UI::party_open(party):\n"
                  "  party already set");
#endif
#endif
  party_ = &party;
}

void UI::party_close()
{
  party_ = {};
}

void UI::game_open(Game& game)
{
#ifdef WITH_UI_INTERFACE
  DEBUG_ASSERTION(!game_,
                  "UI::game_open(game):\n"
                  "  game already set");
#endif
  game_ = &game;
}

void UI::game_close()
{
  game_ = {};
}


void UI::player_changed(Player const& player)
{
  name_changed(player);
  voice_changed(player);
}

auto UI::status_message() const -> string const&
{
  if (status_message_.empty()) {
    static string const translation_empty = ""s;
    return translation_empty;
  }

  return status_message_.back();
}

void UI::add_status_message(string const& status_message)
{
  status_message_.push_back(status_message);
  status_message_changed(status_message);
}

void UI::remove_status_message(string const& status_message)
{
  auto pos = find(status_message_, status_message);

  DEBUG_ASSERTION((pos != status_message_.end()),
                  "UI::remove_status_message(status_message):\n"
                  "  message '" << status_message << "' "
                  "not found in the list");

  if (pos != status_message_.end())
    pos = status_message_.erase(pos);

  if (pos == status_message_.end())
    status_message_changed(this->status_message());
}

auto UI::busy() const -> bool
{
  return busy_;
}

void UI::set_busy()
{
  busy_ = true;
}

void UI::set_not_busy()
{
  busy_ = false;
}

// NOLINTNEXTLINE(misc-no-recursion)
auto UI::progress() const -> double
{
  if (this != ::ui.get())
    return ::ui->progress();

  if (progress_max_ == 0)
    return 1;

  return (progress_ / progress_max_);
}

// NOLINTNEXTLINE(misc-no-recursion)
auto UI::in_progress() const -> bool
{
  if (this != ::ui.get())
    return ::ui->in_progress();

  return (progress_max_ > 0);
}

// NOLINTNEXTLINE(misc-no-recursion)
void UI::set_max_progress(double const progress_max)
{
  if (this != ::ui.get()) {
    ::ui->set_max_progress(progress_max);
    return ;
  }

  DEBUG_ASSERTION((progress_max_ == 0),
                  "UI::set_max_progress(progress_max):\n"
                  "  'progress_max_' is already set: "
                  << progress_max_ << ".");

  progress_max_ = progress_max;
  progress_ = 0;
  progress_changed(progress());
}

// NOLINTNEXTLINE(misc-no-recursion)
void UI::set_progress(double const progress)
{
  if (this != ::ui.get()) {
    ::ui->add_progress(progress);
    return ;
  }

  DEBUG_ASSERTION(progress <= progress_max_,
                  "UI::set_progress(" << progress << ")\n"
                  "The progress is greater then the maximum one (" << progress_max_ << ")");
  DEBUG_ASSERTION(progress >= progress_,
                  "UI::set_progress(" << progress << ")\n"
                  "The progress is less then the current one (" << progress_ << ")");
  progress_ = progress;

  progress_changed(this->progress());
}

// NOLINTNEXTLINE(misc-no-recursion)
void UI::add_progress(double const progress)
{
  if (this != ::ui.get()) {
    ::ui->add_progress(progress);
    return ;
  }

  progress_ += progress;

#ifdef DKNOF
  DEBUG_ASSERTION((this->progress() <= 1.000001),
                  "UI::add_progress(progress):\n"
                  "  'progress' is greater than 1: "
                  << this->progress());
#else
  if (this->progress() > 1.000001)
    cerr << "UI::add_progres():\n"
      << "  progress is " << this->progress() << ", greater than 1" << endl;
  else
#endif
    if (this->progress() > 1 - 0.000001)
      progress_ = progress_max_;

  progress_changed(this->progress());
}

// NOLINTNEXTLINE(misc-no-recursion)
void UI::finish_progress(bool const set_full)
{
  if (this != ::ui.get()) {
    ::ui->finish_progress();
    return ;
  }

#ifndef RELEASE
#ifdef POSTPONED
  DEBUG_ASSERTION((progress() == 1),
                  "UI::finish_progress():\n"
                  "  'progress' is not equal to 1: "
                  << progress()
                  << ", diff: " << progress() - 1);
#else
  if (progress() != 1)
    cerr << "UI::finish_progres()\n"
      << "  progress is " << progress() << ", not 1" << endl;
#endif
#endif
  if (set_full) {
    progress_ = progress_max_;
    progress_changed(progress());
  } // if (set_full)

  progress_finished();
  progress_max_ = 0;
  progress_ = 0;
}

auto UI::add_to_messages_shown(string const& message, InformationType const type) -> bool
{
  if (contains(messages_shown_, Message(message, type)))
    return false;

  messages_shown_.emplace_back(message, type);
  return true;
}

auto to_string(UIType const type) -> string
{
  switch (type) {
  case UIType::dummy:
    return "dummy";
#ifdef USE_UI_GTKMM
  case UIType::gtkmm:
    return "gtkmm";
#endif
  } // switch (type)
  return {};
}

auto operator<<(ostream& ostr, UIType const type) -> ostream&
{
  ostr << to_string(type);
  return ostr;
}


#if 0
// pure virtual functions
// should be = 0 in the declaration
auto UI::get_reservation(Player const& player) -> Reservation // = 0
{
  DEBUG_ASSERTION(false, "Call of Ui<" + to_string(type_) + ">::get_reservation(player)");
  return ::Reservation();
}

auto UI::get_card(Player const& player) -> HandCard // = 0
{
  DEBUG_ASSERTION(false, "Call of Ui<" + to_string(type_) + ">::get_card(player)");
  return HandCard();
}

auto UI::get_poverty_cards_to_shift(Player& player) -> HandCards // = 0
{
  DEBUG_ASSERTION(false, "Call of Ui<" + to_string(type_) + ">::get_poverty_cards_to_shift(player)");
  return HandCards();
}

auto UI::get_poverty_take_accept(Player const& player, unsigned const cardno) -> bool // = 0
{
  DEBUG_ASSERTION(false, "Call of Ui<" + to_string(type_) + ">::get_poverty_take_accept(player, cardno)");
  return false;
}

auto UI::get_poverty_exchanged_cards(Player& player, HandCards const& cards) -> HandCards // = 0
{
  DEBUG_ASSERTION(false, "Call of Ui<" + to_string(type_) + ">::get_poverty_exchanged_cards(player, cards)");
  return HandCards();
}
#endif
