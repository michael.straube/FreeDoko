/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "../basetypes/information.h"
#include "../basetypes/announcement.h"

#include "../game/gameplay_action.h"
#include "../card/hand_card.h"

class Party;
class Game;
struct Reservation;
class Person;
class Player;
class Aiconfig;
class Trick;
class Hand;
class HandCards;
#include "../utils/version.h"

class UI;
extern unique_ptr<UI> ui; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

/**
 ** @brief    the type of the user interface
 **/
enum class UIType {
  dummy,
#ifdef USE_UI_GTKMM
  gtkmm,
#endif
}; // enum UIType

using Message = std::pair<string, InformationType>;

/**
 ** the base ui (user interface) class
 **/
class UI {
  friend class UI_Wrap;
public:
  static auto create(UIType type) -> unique_ptr<UI>;

public:
  explicit UI(UIType type);
  UI() = delete;
  UI(UI const&) = delete;
  auto operator=(UI const&) = delete;

  virtual ~UI() = default;

  auto type()  const -> UIType;
  auto party()       -> Party&;
  auto party() const -> Party const&;
  auto game()        -> Game&;
  auto game()  const -> Game const&;

  // initialize the ui
  virtual void init(int& argc, char**& argv) { }

  // updates the UI
  virtual void update() { }
  // sleeps 'msec' thousands of seconds
  // (< 0 for infinity)
  virtual void sleep(unsigned msec);
  // wait a bit
  void wait();

  // the parts of a party
  virtual void party_open(Party& party); // to be called from childs
  virtual void party_loaded() { }
  virtual void party_get_settings() { }
  virtual void party_close(); // to be called from childs

  virtual void gameplay_action(GameplayAction const& action) { }

  // the parts of a game
  virtual void game_open(Game& game); // to be called from childs
  virtual void game_distribute_cards_manually() { }
  virtual void game_cards_distributed() { }
  virtual void game_redistribute() { }
  virtual void reservation_ask(Player const& player) { }
  virtual auto get_reservation(Player const& player) -> Reservation = 0;
  virtual void reservation_got(Reservation const& reservation,
                               Player const& player) { }
  virtual void game_start() { }
  virtual void game_finish() { }
  virtual void game_close(); // to be called from childs

  // the parts of a trick
  virtual void trick_open(Trick const& trick) { }; // to be called from childs
  virtual void trick_full() { }
  virtual void trick_move_in_pile() { }
  virtual void trick_close() { }; // to be called from childs
  // get a card
  virtual auto get_card(Player const& player) -> HandCard = 0;
  virtual void card_played(HandCard card) { }
  virtual void announcement_made(Announcement announcement,
                                 Player const& player) { }
  virtual void swines_announced(Player const& player) { }
  virtual void hyperswines_announced(Player const& player) { }
  // ai actions
  virtual void ai_test_card(Card card, unsigned playerno) { }
  virtual void ai_card_weighting(int weighting) { }
  virtual void virtual_card_played(HandCard card) { }
  virtual void virtual_trick_full() { }

  // 'player' has a poverty and shifts
  virtual void poverty_shifting(Player const& player) { }
  // 'player' shifts 'cardno' cards
  virtual void poverty_shift(Player const& player, unsigned cardno) { }
  // ask 'player' whether to accept the poverty
  virtual void poverty_ask(Player const& player, unsigned cardno) { }
  // the player 'player' has denied the poverty trumps
  virtual void poverty_take_denied(Player const& player) { }
  // all players have denied to take the cards
  virtual void poverty_take_denied_by_all() { }
  // the player 'player' has accepted the poverty trumps
  // and has returned 'cardno' cards with 'trumpno' trumps
  virtual void poverty_take_accepted(Player const& player,
                                     unsigned cardno,
                                     unsigned trumpno) { }
  // 'player' shifts cards
  virtual auto get_poverty_cards_to_shift(Player& player) -> HandCards = 0;
  // returns whether 'player' accepts the poverty
  virtual auto get_poverty_take_accept(Player const& player, unsigned cardno) -> bool = 0;
  // the player changes the cards of the poverty
  virtual auto get_poverty_exchanged_cards(Player& player, vector<Card> const& cards) -> HandCards = 0;
  // the poverty player 'player' gets 'cards'
  virtual void poverty_cards_get_back(Player const& player,
                                      vector<Card> const& cards) { }

  // the marriage partner has found a bride
  virtual void marriage(Player const& bridegroom, Player const& bride) { }

  // redrawing
  virtual void redraw_all() { }
  virtual void gametype_changed() { }
  virtual void players_switched(Player const& player_a,
                                Player const& player_b) { }
  virtual void player_changed(Player const& player);
  virtual void player_added(Player const& player) { }
  virtual void name_changed(Person const& person) { }
  virtual void voice_changed(Person const& person) { }
  virtual void color_changed(Person const& person) { }
  virtual void hand_changed(Player const& player) { }
  virtual void teaminfo_changed(Player const& player) { }
  virtual void aiconfig_changed(Aiconfig const& aiconfig) { }

  // load the help page
  virtual void help_load(string const& page) { }

  // for status messages
  auto status_message() const -> string const&;
  void add_status_message(string const& status_message);
  void remove_status_message(string const& status_message);
  virtual void status_message_changed(string const&
                                      status_message) { }

  // whether the ui is busy
  auto busy() const -> bool;
  virtual void set_busy();
  virtual void set_not_busy();

  // for progress
  auto progress()    const -> double;
  auto in_progress() const -> bool;
  void set_max_progress(double progress_max);
  void set_progress(double progress);
  void add_progress(double progress);
  void finish_progress(bool set_full = true);
  virtual void progress_changed(double progress) { }
  virtual void progress_finished() { }

  // the first run of the program
  virtual void first_run(string const& message) { }
  // an update of the version
  virtual void program_updated(Version const& old_version) 
  { }
  // information for the user
  virtual void information(string const& message,
                           InformationType type,
                           bool force_show = false)
  { this->add_to_messages_shown(message, type); }

  // an error has occured
  virtual void error(string const& message) { }
  virtual void error(string const& message, string const& backtrace) { }

  // add the message to the shown messages set
  auto add_to_messages_shown(string const& message, InformationType type) -> bool;

  // the shown messages
  auto messages_shown() const -> vector<Message> const&
  { return this->messages_shown_; }


protected:
  UIType const type_;

private:
  Party* party_ = nullptr;
  Game* game_ = nullptr;

  vector<string> status_message_;
  bool busy_ = false;
  double progress_ = 0;
  double progress_max_ = 0;

  // set of shown messages
  vector<Message> messages_shown_;
}; // class UI

auto to_string(UIType type) -> string;
auto operator<<(ostream& ostr, UIType type) -> ostream&;
