/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "../constants.h"

namespace container_algorithm {
bool
check_max_if()
{
  vector<int> v = {1, 2, 3, 4};
  auto e = max_if(v, [](auto x) { return x > 20; },
                  [](auto l, auto r) {return l < r; });
  return (*e == 3);
}

} // namespace container_algorithm
