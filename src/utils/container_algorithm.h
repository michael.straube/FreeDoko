/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <algorithm>

namespace container_algorithm {
// standard algorithms for containers
template<typename Container, typename Value>
  auto find(Container& container, Value const value)
  {
    return std::find(std::begin(container), std::end(container), value);
  }
template<typename Container, typename Value>
  auto contains(Container const& container, Value const value)
  {
    return (std::find(std::begin(container), std::end(container), value)
            != std::end(container));
  }
template<typename Container, typename P>
  auto any_of(Container const& container, P const p)
  {
    return std::any_of(std::begin(container), std::end(container), p);
  }
template<typename Container, typename P>
  auto for_each(Container& container, P p)
  {
    return std::for_each(std::begin(container), std::end(container), p);
  }
template<typename Container, typename Value>
  auto count(Container const& container, Value const value)
  {
    return std::count(std::begin(container), std::end(container), value);
  }
template<typename Container, typename P>
  auto count_if(Container const& container, P const p)
  {
    return std::count_if(std::begin(container), std::end(container), p);
  }
template<typename Container, typename P>
  auto find_if(Container& container, P const p)
  {
    return std::find_if(std::begin(container), std::end(container), p);
  }
template<typename Container>
  auto sort(Container& container)
  {
    return std::sort(std::begin(container), std::end(container));
  }
template<typename Container, typename Compare>
  auto sort(Container& container, Compare const compare)
  {
    return std::sort(std::begin(container), std::end(container), compare);
  }
template<typename Container>
  auto unique(Container& container)
  {
    auto p = std::unique(std::begin(container), std::end(container));
    container.erase(p, std::end(container));
  }
template<typename Container, typename Value>
  void remove(Container& container, Value value)
  {
    auto p = std::remove(std::begin(container), std::end(container), value);
    container.erase(p, std::end(container));
  }
template<typename Container, typename UnaryPredicate>
  void remove_if(Container& container, UnaryPredicate v)
  {
    auto p = std::remove_if(std::begin(container), std::end(container), v);
    container.erase(p, std::end(container));
  }
template<typename Container, typename Compare>
  auto max_element(Container const& container, Compare compare)
  {
    return std::max_element(std::begin(container), std::end(container), compare);
  }
template<typename Container, typename Compare>
  auto min_element(Container const& container, Compare compare)
  {
    return std::min_element(std::begin(container), std::end(container), compare);
  }

/** @return   from all elements e with v(e) is true the maximum one
 **/
template<typename Container, typename UnaryPredicate, typename Compare>
  auto max_if(Container const& container, UnaryPredicate v, Compare comp)
  -> decltype(&*begin(container)) 
  {
    if (container.empty())
      return nullptr;
    decltype(&*begin(container)) p = nullptr;
    for (auto const& e : container) {
      if (!v(e))
        continue;
      if (!p) {
        p = &e;
      } else if (comp(*p, e)) {
        p = &e;
      }
    }
    return p;
  }

/** @return   the second max element
 **/
template<typename Container, typename UnaryPredicate, typename Compare>
  auto get_second_max_element_if(Container const& container, UnaryPredicate v, Compare comp)
  -> decltype(&*begin(container)) 
  {
    if (container.size() < 2)
      return {};
    decltype(&*begin(container)) p1 = nullptr;
    decltype(&*begin(container)) p2 = nullptr;
    for (auto const& e : container) {
      if (!v(e))
        continue;
      if (!p1) {
        p1 = &e;
      } else if (comp(*p1, e)) {
        p2 = p1;
        p1 = &e;
      } else if (!p2) {
        p2 = &e;
      } else if (comp(*p2, e)) {
        p2 = &e;
      }
    }
    return p2;
  }

template<typename Container>
auto sum(Container const& container)
{
  using element = decltype(*begin(container));
  return std::accumulate(begin(container), end(container), element{0});
}

template<typename Container, typename View>
auto sum(Container const& container, View view)
{
  using element = decltype(*begin(container));
  using number = decltype(view(*begin(container)));
  auto op = [view](number const a, element const b) {
    return a + view(b);
  }; 

  return std::accumulate(begin(container), end(container), number{0}, op);
}

template<typename Container, typename View, typename UnaryPredicate>
auto sum_if(Container const& container, View view, UnaryPredicate v)
{
  using element = decltype(*begin(container));
  using number = decltype(view(*begin(container)));
  auto op = [v, view](number const a, element const b) {
    if (!v(b))
      return a;
    return a + view(b);
  }; 

  return std::accumulate(begin(container), end(container), number{}, op);
}

template<typename Container, typename View, typename UnaryPredicate>
auto prod_if(Container const& container, View view, UnaryPredicate v)
{
  auto op = [v, view](auto const a, auto const b) {
    if (!v(b))
      return a;
    return a * view(b);
  }; 

  return std::accumulate(begin(container), end(container), 1, op);
}

template<typename Container>
  auto element_with_name(Container const& container, string const& name)
  {
    auto const has_name = [&name](auto const e) { return to_string(e) == name; };
    auto const p = find_if(container, has_name);
    if (p == end(container)) {
      throw "unknown element '" + name + "'";
    }
    return *p;
  }

} // namespace container_algorithm
