/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <iosfwd>
#include <string>
#include <memory>

/**
 ** a date class
 **/
struct Date {
public:
  static auto create(std::istream& istr)   -> std::unique_ptr<Date>;
  static auto create(std::string const& s) -> std::unique_ptr<Date>;
  using Year  = int;
  using Month = unsigned;
  using Day   = unsigned;

public:
  Date();
  Date(Year year, Month month, Day day) noexcept;
  explicit Date(std::string const& s);
  Date(Date const&)                    = default;
  Date(Date&&)                         = default;
  auto operator=(Date const&) -> Date& = default;
  auto operator=(Date&&)      -> Date& = default;
  ~Date() = default;

  // convert in a string, p.e. 2006-10-11
  operator std::string() const;

  Year  year  = 0;
  Month month = 0;
  Day   day   = 0;
}; // struct Date

auto operator==(Date const& lhs, Date const& rhs) -> bool;
auto operator!=(Date const& lhs, Date const& rhs) -> bool;
auto operator< (Date const& lhs, Date const& rhs) -> bool;
auto operator> (Date const& lhs, Date const& rhs) -> bool;
auto operator<=(Date const& lhs, Date const& rhs) -> bool;
auto operator>=(Date const& lhs, Date const& rhs) -> bool;

auto to_string(Date const& date)         -> std::string;
auto to_long_string_de(Date const& date) -> std::string;

auto operator<<(std::ostream& ostr, Date const& date) -> std::ostream&;
