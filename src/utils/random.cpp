/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "random.h"

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
using namespace std::literals::string_literals;

Random random_value; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

#include <ctime>

#define WORKAROUND

/** test the random number generator and write the results
 **/
void
RandomBase::test()
{
  constexpr int n = 49;
  constexpr int wuerfe = 1000000;

  Random r;
  std::vector<int> c(n, 0);
  for (int i = 0; i < n * wuerfe; ++i)
    c[r(n)] += 1;

  int min_c = c[0];
  int max_c = c[0];
  for (auto x : c) {
    min_c = std::min(min_c, x);
    max_c = std::max(max_c, x);
  }

  std::cout << "Min: " << min_c << '\n';
  std::cout << "Max: " << max_c << '\n';
  std::cout << "Diff: " << max_c - min_c << " = " << 100 * (max_c - min_c) / static_cast<double>(wuerfe) << " % * " << wuerfe << '\n';
} // static void RandomBase::test()

/** constructor
 **/
RandomMT19937::RandomMT19937() // NOLINT
{
  this->RandomMT19937::set_random_seed();
}

/** constructor
 **
 ** @param    seed   seed
 **/
RandomMT19937::RandomMT19937(Random::Type const seed) noexcept // NOLINT
{
  this->RandomMT19937::set_seed(seed);
}


/** destructor
 **/
RandomMT19937::~RandomMT19937() = default;

/** @return   the maximal seed value
 **/
RandomMT19937::Type
RandomMT19937::max_seed()
{
  return std::mt19937::max();
}

/** set a random seed
 **/
void
RandomMT19937::set_random_seed()
{
#ifdef WORKAROUND
  // The g++ under Windows has no random device, so he always uses the same sequence.
  // Since I do not know, whether this also applies to other systems we always take the time.
  auto seed = time(nullptr);
  if (seed < 0) {
    throw ("RandomMT19937::set_random_seed()\n"
           "  Could not initialize with time."s);
  }
  this->engine_.seed(seed);
#else
  std::random_device rd;
  this->engine_.seed(rd());
#endif
  this->seed_ = this->engine_();
} // void RandomMT19937::set_random_seed()

/** set the seed
 **
 ** @param    seed   new seed
 **/
void
RandomMT19937::set_seed(Type const seed)
{
  this->seed_ = seed;
  this->engine_.seed(seed);
} // void RandomMT19937::set_seed(Type seed)

/** @return   the current seed
 **/
RandomMT19937::Type
RandomMT19937::seed() const
{
  return this->seed_;
}

/** return a random value
 **
 ** @return   random value in [0, max_seed()]
 **/
RandomMT19937::Type
RandomMT19937::get()
{
  this->seed_ = this->engine_();
  return this->seed_;
} // int RandomMT19937::get()

/** return a random value
 **
 ** @param    end   maximal value + 1
 **
 ** @return   random value in [0, end)
 **/
int
RandomMT19937::operator()(int const end)
{
  if (end <= 0)
    return 0;
  auto const min = this->engine_.min(); // NOLINT
  auto const max = this->engine_.max(); // NOLINT
  auto const range = max - min;
  auto const scaling = range / end;
  auto const past = end * scaling;
  auto ret = this->engine_();
  while (ret >= past)
    ret = this->engine_();
  return (ret / scaling);
} // int RandomMT19937::operator()(int end) const


/** constructor
 **/
RandomPre_0_7_17::RandomPre_0_7_17() noexcept // NOLINT
{
  this->RandomPre_0_7_17::set_random_seed();
}

/** constructor
 **
 ** @param    seed   seed
 **/
RandomPre_0_7_17::RandomPre_0_7_17(Random::Type const seed) noexcept // NOLINT
{
  this->RandomPre_0_7_17::set_seed(seed);
}

/** destructor
 **/
RandomPre_0_7_17::~RandomPre_0_7_17() = default;

/** @return   the maximal seed value
 **/
RandomPre_0_7_17::Type
RandomPre_0_7_17::max_seed()
{
  return (RandomPre_0_7_17::m_ - 1);
}

/** set a random seed
 **/
void
RandomPre_0_7_17::set_random_seed()
{
  this->seed_ = ::random_value(this->max_seed() + 1);
} // void RandomPre_0_7_17::set_random_seed()

/** set the seed
 **
 ** @param    seed   new seed
 **/
void
RandomPre_0_7_17::set_seed(Type const seed)
{
  this->seed_ = seed % (this->max_seed() + 1);
} // void RandomPre_0_7_17::set_seed(Type seed)

/** @return   the current seed
 **/
RandomPre_0_7_17::Type
RandomPre_0_7_17::seed() const
{
  return this->seed_;
}

/** return a random value
 **
 ** @return   random value in [0, max_seed()]
 **/
RandomPre_0_7_17::Type
RandomPre_0_7_17::get()
{
  this->seed_ = (  (RandomPre_0_7_17::a_ * this->seed_ + RandomPre_0_7_17::c_)
                 % RandomPre_0_7_17::m_);
  return this->seed_;
} // int RandomPre_0_7_17::get()

/** return a random value
 **
 ** @param    end   maximal value + 1
 **
 ** @return   random value in [0, end)
 **/
int
RandomPre_0_7_17::operator()(int const end)
{
  return (this->get() % end);
} // int RandomPre_0_7_17::operator()(int end) const
