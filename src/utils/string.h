/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <string>
#include <vector>
#include <iosfwd>
#include <iomanip>
#include <sstream>
#include <cstring>

#include <filesystem>

namespace String {

auto contains(std::string const& s, char c) -> bool;

// remove all leading blanks
auto remove_leading_blanks(std::string& s) -> std::string&;
// remove all leading and trailing blanks
auto remove_blanks(std::string& s) -> std::string&;
// remove all leading and trailing blanks
auto remove_blanks(std::string const& s) -> std::string;

// remove all trailing newlines
auto remove_trailing_newlines(std::string s) -> std::string;
// replace double newlines with one
auto remove_double_newlines(std::string s) -> std::string;

// return the first word
auto word_first(std::string const& text) -> std::string;
// remove the first word
auto word_first_remove(std::string& text) -> std::string&;
// remove the first word and following blanks
auto word_first_remove_with_blanks(std::string& text) -> std::string&;

// replace all 's' with 'replacement' in 'text'
auto replace_all(std::string& text, std::string const& s, std::string const& replacement) -> std::string&;
auto replaced_all(std::string text, std::string const& s, std::string const& replacement) -> std::string;

// reads till an eof
auto get_till_eof(std::istream& istr) -> std::string;
// reads the whole file in the string
auto getfile(std::filesystem::path const& path) -> std::string;

// split 'line' according to 'separator'
auto split(std::string const& line, char separator) -> std::vector<std::string>;

// convert from latin1 to utf8
auto latin1_to_utf8(std::string text) -> std::string;

// converts the argument to a string, using 'strstream'
template<typename T>
  auto to_string(T const& t) -> std::string
  {
    std::ostringstream entry;

    entry << t;

    return entry.str();
  } // template<typename T> std::string to_string(T const& t)

// converts the argument to a string, using 'strstream'
template<typename T>
  auto to_string(T const& t, int width, char fill = ' ') -> std::string
  {
    std::ostringstream entry;

    entry << std::setfill(fill) << std::setw(width) << t;

    return entry.str();
  } // template<typename T> std::string to_string(T const& t, int width, char fill = ' ')

// reads the argument from a string, using 'strstream'
template<typename T>
  auto from_string(std::string const& str, T& t) -> T const&
  {
    std::istringstream istr(str);

    istr >> t;

    return t;
  } // template<typename T> T from_string(T& t)

} // namespace String

namespace std {
// missing in the standard
inline auto stou(std::string const& text) -> unsigned
{ return stoul(text); }
inline auto stob(std::string const& text) -> bool
{ return (   text == "true"
          || text == "yes"
          || text == "1");
}
} // namespace std
